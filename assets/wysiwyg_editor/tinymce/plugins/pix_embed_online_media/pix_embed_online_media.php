<!-- Include CSS -->
<link rel="stylesheet" href="includes/pix_embed_online_media.css" />
<div class="main main-media">
<div class="body-embed-online-media">
    <div class="area-item">
        <label for="provider" class="media--title"><strong>Provider:</strong> </label>
        <select class="provider media--input" id="provider" name="provider">
            <option>Youtube</option>
            <option>Vimeo</option>
            <option>Soundcloud</option>
            <option>Mixcloud</option>
            <option>Hearthis</option>
        </select>
    </div>
    <div class="area-item">
        <label for="source_url" class="media--title"><strong>URL:</strong> </label>
        <input class="source-url media--input" type="text" id="source_url" name="source_url" placeholder="Enter URL">
        <div class="clear-fix"></div>
        <span class="example media--input" id="example"></span>
    </div>
    <div class="area-item">
        <label class="media--title"><strong>Custom style</strong></label>
        <div class="area-child media--input clear-padding">
            <span class="item-child">
                <input class="input-text media--input__text" type="text" id="iframe_height" name="iframe_height" placeholder="Height">
            </span>
            <span class="item-child media--text__X">X</span>
            <span class="item-child">
                <input class="input-text media--input__text" type="text" id="iframe_width" name="iframe_width" placeholder="Width">
            </span>
        </div>
    </div>
    <!-- todo phase 2 -->
    <!-- <div class="area-item">
        <label><strong>Options</strong></label>
    </div> -->
</div>
<br/>
<div class="body-embed-online-media-action">
    <button class="button button-cancel" id="dialog_cancel_btn" class="btn-default">Cancel</button> 
    <button class="button button-insert" id="diablog_insert_btn" class="btn-primary">Insert and Close</button>
</div>
</div>
<!-- Include JS -->
<script type="text/javascript" src="includes/pix_embed_online_media.js"></script>