$(document).ready(function() {

    setTimeout(function() {
        hide_loadding();
    }, 1500);
  
    $('.autoplayslider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});

function show_loadding() {
    $('.div-loader').show();
}

var hide_loadding_allow = true;

function hide_loadding() {
    if (hide_loadding_allow) {
        $('.div-loader').hide();
    }
}
$(window).on("load", function() {
    mycarousel(".carousel-home-1", 1, 1, 1, true, false, 5000);
    mycarousel(".list-post", 3, 3, 1, true, false, 5000);
});
$(window).on("load resize", function() {

    if ($(window).width() > 767) {
        $("body").removeAttr("style");
        // $(".signup-right").removeClass("active"); // Cause error of form_signup_display() 
    }

    //câu dưới là <1050 thì chạy giao diện mobile....oke hk
    if ($(window).width() < 1050 || navigator.userAgent.match(/iPad/i) != null) { //ipad ngang tro xuong
        $(".main-content").css("margin-left", "auto");
        $(".mobile-menu").show();
        $(".top-menu").hide();
        $(".side-menu").hide();
    } else {
        //chỗ này là chạy giao diện pc
        $(".mobile-menu").hide();
        $(".top-menu").show();
        //$(".side-menu").show();
    }
    setTimeout(function() {
        $(".mobile-menu .wrap-slide-text").width($(".mobile-top-menu").width() - $(".mobile-menu .logo").outerWidth() - $(".mobile-menu .icon-show-menu-mobile").outerWidth() - $(".mobile-menu .text-logo").outerWidth() - $(".mobile-menu .mobile-menu-right-icon").outerWidth() - 50);
        $(".top-menu .wrap-slide-text").width($(".top-menu").width() - ($(".top-menu .logo").outerWidth() + $(".top-menu .text-logo").outerWidth() + $(".top-menu .wrap-login").outerWidth()) - 50);
    }, 400);
});

$(window).on("load", function() {
    /*-----show hide side menu pc*/
    /*$(".side-menu").on("mouseenter",function(){
    	if($(window).width() >= 1050 && navigator.userAgent.match(/iPad/i) == null){
    		$(this).addClass("active");
    		$(".side-menu-item").css("opacity","1");
    		$(".img-mobile-search").css("opacity","1");
    	}
    });
    $(".side-menu").on("mouseleave",function(){
    	if($(window).width() >= 1050 && navigator.userAgent.match(/iPad/i) == null){
    		$(this).removeClass("active");
    		$(".side-menu-item").css("opacity","0");
    		$(".img-mobile-search").css("opacity","0");
    	}
    });*/
    /*------show hide side menu mobile*/
    $(".mobile-top-menu .icon-show-menu-mobile").on("click", function() {
        if ($(".mobile-side-menu").hasClass("active")) {
            $(".mobile-side-menu").removeClass("active");
            $(".wrap-side-menu").removeClass("active");
        } else {
            $(".mobile-side-menu").addClass("active");
            $(".wrap-side-menu").addClass("active");
            $("li.side-menu-item").css("opacity", "1");
        }
    });
    $(".profile-title").on('click', function() {
        $(".profile-title").removeClass("active");
        $(this).addClass("active");
        $(".mobile-side-menu").removeClass("active");
        $(".wrap-side-menu").removeClass("active");
    });
    $(".wrap-side-menu").on("click", function() {
        $(".mobile-side-menu").removeClass("active");
        $(".wrap-side-menu").removeClass("active");
    });
    $(".mobile-menu-right-icon").on("click", function() {
        if ($(".mobile-right-menu").hasClass("active")) {
            $(".mobile-right-menu").removeClass("active");
        } else {
            $(".mobile-right-menu").addClass("active");
        }
    });
    /*show hide sign up*/
    // $(".signup , .login").on("click", function() {
    // form_signup_display();
    // });
    // Button Power - Show/hide form signup
	// Why $(".login").on("click",...)
    $(".power-off-login").on("click", function() {
         form_signup_display();
    });
    $(".mobile-signup , .mobile-login").on("click", function() {
        //check content active
        var contentSignupFormActive = $(".signup-right").hasClass("active");
        //check what is click button (signup or login)
        var signup = $(this).hasClass("mobile-signup");
        $(".signup-right").addClass("active");
        $('body').css("overflow", "hidden");
        //check signup has active
        $(".mobile-side-menu").removeClass("active");
        $(".wrap-side-menu").removeClass("active");
        if (signup) {
            if (contentSignupFormActive) {
                if ($("#signup-form").hasClass('active')) {
                    $(".signup-right").removeClass("active");
                    $('body').css("overflow", "auto");
                } else {
                    $(".signup-right .nav-tabs a[href='#signup-form']").click();
                }
            } else {
                $(".signup-right .nav-tabs a[href='#signup-form']").click();
            }
        } else {
            if (contentSignupFormActive) {
                if ($("#login-form").hasClass('active')) {
                    $(".signup-right").removeClass("active");
                    $('body').css("overflow", "auto");
                } else {
                    $(".signup-right .nav-tabs a[href='#login-form']").click();
                }
            } else {
                $(".signup-right .nav-tabs a[href='#login-form']").click();
            }
        }
    });
    // close (show - hide) sign up
    $(".close-signup").on("click", function() {
        if ($(".signup-right").hasClass("active")) {
            $(".signup-right").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".signup-right").addClass("active");
        }
    });
    $(".mobile-login").on("click", function() {
        if ($(".login-right").hasClass("active")) {
            $(".login-right").removeClass("active");
        } else {
            $(".login-right").addClass("active");
        }
    });
    $(".mobile-login").on("click", function() {
        $('.login-right').css("z-index", "2");
    });
    // close (show - hide) login
    $(".close-login").on("click", function() {
        if ($(".login-right").hasClass("active")) {
            $(".login-right").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".login-right").addClass("active");
        }
    });

	
	
	
	
	
	
    $('.step2 li a').on("click", function() {
        $(this).css("color", "#f39200");
    });
    $('.step2 li a').on("click", function() {
        $(this).css("color", "#f39200");
    });
    $(".icon-find img").on("click", function() {
        if ($(".search-find-icon").hasClass("active")) {
            $(".search-find-icon").removeClass("active");
        } else {
            $(".search-find-icon").addClass("active");
            if ($(".search-fire-icon").hasClass("active")) {
                $(".search-fire-icon").removeClass("active");
            }
            if ($(".search-music-icon").hasClass("active")) {
                $(".search-music-icon").removeClass("active");
            }
        }
    });
    $(".icon-fire img").on("click", function() {
        if ($(".search-fire-icon").hasClass("active")) {
            $(".search-fire-icon").removeClass("active");
        } else {
            $(".search-fire-icon").addClass("active");
            if ($(".search-find-icon").hasClass("active")) {
                $(".search-find-icon").removeClass("active");
            }
            if ($(".search-music-icon").hasClass("active")) {
                $(".search-music-icon").removeClass("active");
            }
        }
    });
    $(".icon-music img").on("click", function() {
        if ($(".search-music-icon").hasClass("active")) {
            $(".search-music-icon").removeClass("active");
        } else {
            $(".search-music-icon").addClass("active");
            if ($(".search-find-icon").hasClass("active")) {
                $(".search-find-icon").removeClass("active");
            }
            if ($(".search-fire-icon").hasClass("active")) {
                $(".search-fire-icon").removeClass("active");
            }
        }
    });
    // $(".profile-setting").on("click",function(){
    // 	//alert('a');
    // 	//console.log($("#user_type_id").val());
    // 	// var i = $("#user_type_id").val();
    // 	// 	for(i=1;i<=7;i++){
    // 	// 		$("#url_profile").attr('readonly','readonly');
    // 	// 	}
    // 	// 	$("#url_profile").removeAttr('readonly');
    // 	switch($("#user_type_id").val()){
    // 		case '1':
    // 		$("#url_profile").attr('readonly','readonly');
    // 		break;
    // 		case '2','3': 
    // 		$("#url_profile").removeAttr('readonly');
    // 	}
    // });
    $('input:radio[name="gender"]').change(function() {
        switch ($(this).val()) {
            case '1':
                $('.url').show();
                $('.founder').hide();
                $('.biiography').show();
                $('.shortbio').show();
                $('.status').show();
                $('.marketing').show();
                $('.contact').show();
                $('.distributor').hide();
                $('.facebook').show();
                $('.twitter').show();
                $('.bandcamp').show();
                $('.soundcloud').show();
                $('.mixcloud').show();
                $('.itunes').hide();
                $('.beatport').hide();
                break;
            case '2':
                $('.url').show();
                $('.founder').show();
                $('.biiography').show();
                $('.shortbio').show();
                $('.status').show();
                $('.marketing').hide();
                $('.contact').show();
                $('.distributor').hide();
                $('.facebook').show();
                $('.twitter').show();
                $('.bandcamp').show();
                $('.soundcloud').show();
                $('.mixcloud').show();
                $('.itunes').show();
                $('.beatport').show();
                break;
            case '3':
                $('.url').show();
                $('.founder').show();
                $('.biiography').show();
                $('.shortbio').show();
                $('.status').show();
                $('.marketing').show();
                $('.contact').show();
                $('.distributor').show();
                $('.facebook').show();
                $('.twitter').show();
                $('.bandcamp').show();
                $('.soundcloud').show();
                $('.mixcloud').show();
                $('.itunes').show();
                $('.beatport').show();
                break;
            case '4':
                $('.url').show();
                $('.founder').hide();
                $('.biiography').show();
                $('.shortbio').show();
                $('.status').show();
                $('.marketing').show();
                $('.contact').show();
                $('.distributor').hide();
                $('.facebook').show();
                $('.twitter').show();
                $('.bandcamp').show();
                $('.soundcloud').show();
                $('.mixcloud').show();
                $('.itunes').hide();
                $('.beatport').hide();
                break;
            case '5':
                $('.url').show();
                $('.founder').show();
                $('.biiography').show();
                $('.shortbio').show();
                $('.status').show();
                $('.marketing').show();
                $('.contact').show();
                $('.distributor').show();
                $('.facebook').show();
                $('.twitter').show();
                $('.bandcamp').show();
                $('.soundcloud').show();
                $('.mixcloud').show();
                $('.itunes').show();
                $('.beatport').show();
                break;
            case '6':
                $('.url').hide();
                $('.founder').hide();
                $('.biiography').hide();
                $('.shortbio').hide();
                $('.status').hide();
                $('.marketing').hide();
                $('.contact').hide();
                $('.distributor').hide();
                $('.facebook').hide();
                $('.twitter').hide();
                $('.bandcamp').hide();
                $('.soundcloud').hide();
                $('.mixcloud').hide();
                $('.itunes').hide();
                $('.beatport').hide();
                break;
            case '7':
                $('.url').hide();
                $('.founder').hide();
                $('.biiography').hide();
                $('.shortbio').hide();
                $('.status').hide();
                $('.marketing').hide();
                $('.contact').hide();
                $('.distributor').hide();
                $('.facebook').hide();
                $('.twitter').hide();
                $('.bandcamp').hide();
                $('.soundcloud').hide();
                $('.mixcloud').hide();
                $('.itunes').hide();
                $('.beatport').hide();
                break;
        }
    });
})
$(window).on("load resize", function() {
    //height signup wrap
    //$(".signup-right .tab-content").height($(".signup-wrap").outerHeight() -  $(".tab-menu-signup").outerHeight());
    // $(".profile--tab .tab-content").height($(".profile-wrap").outerHeight() - $(".tab-profile").outerHeight() - 100);
    // $(".signup-wrap .tab-content").height($(".profile-wrap").outerHeight() - $(".tab-profile").outerHeight() - 100);

    if ($('body').width() > 767) {
        $(".profile--tab .tab-content").height($(".profile-wrap").outerHeight() - $(".tab-profile").outerHeight() - 100);
        $(".signup-wrap .tab-content").height($(".profile-wrap").outerHeight() - $(".tab-profile").outerHeight() - 100);
    } else {
        $(".profile--tab .tab-content").height($(window).outerHeight() - 100);
        $(".signup-wrap .tab-content").height($(window).outerHeight() - 100);
    }
})

function mycarousel(id, itemdestop, itemtable, itemmobile, itemloop, dots, autoplay) {
    try {
        $(id).find(".carousel").each(function() {
            if (itemdestop == 1 && itemtable == 1 && itemmobile == 1) {
                $(this).css('display', 'block');
                var owl = $(this).find(".carousel-items").slick({
                    infinite: itemloop,
                    arrows: false,
                    dots: dots,
                    autoplay: autoplay,
                    autoplaySpeed: 5000,
                    onBeforeChange: function() {}
                });
                $(this).find(".carousel-prev").click(function() {
                    owl.slick('slickPrev');
                });
                $(this).find(".carousel-next").click(function() {
                    owl.slick('slickNext');
                });
            } else {
                var owl = $(this).find(".carousel-items").slick({
                    slidesToShow: itemdestop,
                    infinite: itemloop,
                    arrows: false,
                    dots: dots,
                    autoplay: autoplay,
                    responsive: [{
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: itemtable
                        }
                    }, {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: itemmobile
                        }
                    }]
                });
                $(this).find(".carousel-prev").click(function() {
                    owl.slick('slickPrev');
                });
                $(this).find(".carousel-next").click(function() {
                    owl.slick('slickNext');
                });
            }
        });
    } catch (err) {
        console.log(err);
    }
}
var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
var is_safari = navigator.userAgent.indexOf("Safari") > -1;
var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
if ((is_chrome) && (is_safari)) {
    is_safari = false;
}
if ((is_chrome) && (is_opera)) {
    is_chrome = false;
}
// check validate
function checkvalidate(input) {
    validate_check(input);
    input.on("keyup change", function() {
        input.next(".alert-error").remove();
        validate_check(input);
    });
}

function validate_check(input) {
    //check empty
    if (input.attr("data-required") == "true" && !input.val()) {
        var dataError = input.attr("data-error") || "Error";
        input.after("<div class='alert-error'>" + dataError + "</div>");
        input.css("border-color", "#f39200");
        return false;
    }
    //check regex
	console.log(input);
    if (input.not("input.password").attr("data-required") == "true" && input.val().length < 4) {
        var dataError = "Enter 4 or more characters";
        input.after("<div class='alert-error'>" + dataError + "</div>");
        input.css("border-color", "#f39200");
        return false;
    }
    if (input.attr("type") == "password" && input.val().length < 8) {
        var dataError = "Enter 8 or more characters";
        input.after("<div class='alert-error'>" + dataError + "</div>");
        input.css("border-color", "#f39200");
        return false;
    }
	
    var regex = /[^\w\s]/gi;
    if (input.attr("data-check") == "true" && regex.test(input.val()) == true) {
        var dataError = "No  special characters";
        input.after("<div class='alert-error'>" + dataError + "</div>");
        input.css("border-color", "#f39200");
        return false;
    }
    //check type email
    if (input.attr("type") == "email") {
        if (isValidEmailAddress(input.val()) != true) {
            input.after("<div class='alert-error'> Enter a vaild email address</div>");
            input.css("border-color", "#f39200");
            return false;
        }
    }
    //check type radio
    if (input.attr("type") == "radio") {
        var name = input.attr("name");
        if (!input.parent().find("input[name=" + name + "]:checked").val()) {
            input.parent().find("input[name=" + name + "]").next(".alert-error").remove();
            input.after("<div class='alert-error'> Please checked your type username</div>");
            input.css("border-color", "#f39200");
            return false;
        }
    }
    input.removeAttr("style");
    return true;
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
//profile
$(window).on("load", function() {
    $(".menu-profile-right").on("click", function() {
        $(".menu-profile-toggle").toggle(200);
    });
    /*show hide sign up*/
    $(".profile-tickets").on("click", function() {
        $(".profile-tickets-detail").show();
    });
    /*show hide profile tickets*/
    $(".profile-tickets").on("click", function() {
        if ($(".profile-tickets-detail").hasClass("active")) {
            $(".profile-tickets-detail").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".profile-tickets-detail").addClass("active");
            if ($(".profile-view-detail").hasClass("active")) {
                $(".profile-view-detail").removeClass("active");
            }
            if ($(".profile-setting-detail").hasClass("active")) {
                $(".profile-setting-detail").removeClass("active");
            }
            $('body').css("overflow", "hidden");
        }
    });
    /*show hide profile view*/
    $(".profile-view, .author").on("click", function() {
        if ($(".profile-view-detail").hasClass("active")) {
            $(".profile-view-detail").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".profile-view-detail").addClass("active");
            if ($(".profile-tickets-detail").hasClass("active")) {
                $(".profile-tickets-detail").removeClass("active");
            }
            if ($(".profile-setting-detail").hasClass("active")) {
                $(".profile-setting-detail").removeClass("active");
            }
            $('body').css("overflow", "hidden");
        }
    });
    /*show hide profile setting*/
    $(".profile-setting").on("click", function() {
        if ($(".profile-setting-detail").hasClass("active")) {
            $(".profile-setting-detail").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".profile-setting-detail").addClass("active");
            if ($(".profile-view-detail").hasClass("active")) {
                $(".profile-view-detail").removeClass("active");
            }
            if ($(".profile-tickets-detail").hasClass("active")) {
                $(".profile-tickets-detail").removeClass("active");
            }
            $('body').css("overflow", "hidden");
        }
    });
    // close (show - hide) profile
    $(".close-profile").on("click", function() {
        $(".profile--tab").removeClass("active");
        $(".profile--tab").removeAttr('style');
        $('body').css("overflow", "auto");
    });
    $(".remove-btn-profile").on("click", function() {
        $(".delete-success").css("display", "block");
    });
    $(".view-artists").on("click", function() {
        $(".toggle-artists").toggle(500);
    });
    $(".view-labels").on("click", function() {
        $(".toggle-labels").toggle(500);
    });
    $(".view-artists-1").on("click", function() {
        $(".toggle-artists-1").toggle(500);
    });
    $(".view-labels-1").on("click", function() {
        $(".toggle-labels-1").toggle(500);
    });
    $('.action').on('mouseenter', function() {
        $(this).find('.action-hover').html('Remove').css("color", "#ffa200");
    });
    $('.action').on('mouseleave', function() {
        $(this).find('.action-hover').html('Favourite').css("color", "#fff");
    });
    $(".button-minus").on("click", function() {
        $(this).parents(".input-group").find(".input-value").val(parseInt($(this).parents(".input-group").find(".input-value").val()) - 1);
    });
    $(".button-plus").on("click", function() {
        $(this).parents(".input-group").find(".input-value").val(parseInt($(this).parents(".input-group").find(".input-value").val()) + 1);
    });
});
//blog/{slug-post}: replace content color
// jQuery(document).ready(function($) {
// 	var a =$('.text-center-content-blog *').length;
// 	console.log(a);
// 	var content = $('.text-center-content-blog p').attr('style');
// 	var replaces = content.replace('#222222', 'white' ); // black-> white
// 	$('.redm-blog .text-center-content-blog p').attr('style',content + ';color:white');
// });
// jQuery(document).ready(function($) {
// 	$(window).load(carousel_items_description).resize(setTimeout(carousel_items_description,1000));
// 	//$(window).on("load resize",carousel_items_description);
// 	function carousel_items_description(){
// 		var max = 0;
// 		if( $(window).width() > 940 ){
// 		var ab = $('.carousel-items-p-des').length;
// 		for( var i = 0; i < ab; i++){
// 			if($('.carousel-items-p-des').eq(i).height() > max ){
// 				max = $('.carousel-items-p-des').eq(i).height();
// 			}
// 		}
// 		console.log(max);
// 		 $('.carousel-items-p-des').css('min-height', max + 'px');
// 		}
// 	}
// });
//$('.post-p-des').eq(i).[0].getBoundingClientRect().height;
jQuery(document).ready(function($) {
    $(window).load(post_p_des);
    $(window).resize(function() {
        setTimeout(post_p_des, 1000)
    });
    //$(window).on("load resize",carousel_items_description);
    function post_p_des() {
        var max = 0;
        var ab = $('.post-p-des').length;
        for (var i = 0; i < ab; i++) {
            if ($('.post-p-des').eq(i).height() > max) {
                max = $('.post-p-des').eq(i).height();
            }
        }
        // console.log(max);
        $('.post-p-des').css('min-height', max + 'px');
    }
});
// Blog: load box comment 
jQuery(document).ready(function($) {
    $("#ajax_loadcomment").load(function(e) {
        var url = $(this).attr('data-ajax-load-comment'); // /(en|cn)/ajax_comment
        $.ajax({
            type: "POST",
            url: url,
            data: 'articles_id=' + $('#ajax_loadcomment').data('ajax-load-comment-id'),
            success: function(data) { // start success
                    $('#ajax_loadcomment').html(data); // show response from the php script.
                    // Click ajax comment reply
                    $("[data-comment-child-reply]").click(function(event) {
                        var url = $(this).data('ajax-load-comment-link'); // /(en|cn)/ajax_comment_reply
                        var datas = $(this).data('comment-child-reply');
                        var href = $(this).attr('href');
                        var array = datas.split(",");
                        var data1 = array[0];
                        var data2 = array[1];
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: 'articles_id=' + data1 + '&id_comment_parent=' + data2,
                            success: function(result) {
                                if ($('* .div-comment-form-reply').hasClass('active')) {
                                    $('* .div-comment-form-reply').removeClass('active');
                                };
                                $('.span-apperance-comment').removeClass('hidden');
                                $('.div-ajax-load-reply-' + data2 + ' .div-comment-form-reply').addClass('active').html(result);
                                $('a[href="' + href + '"][data-comment-child-reply]').on('click', function(e) {
                                    e.preventDefault();
                                    $('html, body').animate({
                                        scrollTop: $(this.hash).offset().top - 200
                                    }, 300, function() { // Speed
                                        // window.location.hash = hash; // Change link url #
                                    });
                                });
                                // load commnet reply
                                $(".form-user-comment-reply").submit(function(e) {
                                    var url = $(this).attr('action');
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: $(this).serialize(),
                                    });
                                    e.preventDefault();
                                    $('.form-user-comment-reply textarea').val('');
                                    setTimeout(function() {
                                        $("#ajax_loadcomment").load();
                                    }, 10);
                                });
                            }
                        });
                        e.preventDefault();
                    }); //
                } // end success
        });
        e.preventDefault();
    });
    // Blog: Default load comment
    $("#ajax_loadcomment").load();
});
// Blog: Submit form comment and load box comment
$("#form-user-comment").submit(function(e) {
    var url = $(this).attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: $(this).serialize(),
    });
    e.preventDefault();
    $('#form-user-comment textarea').val('');
    setTimeout(function() {
        $("#ajax_loadcomment").load();
    }, 100);
});
// // new feed form up status
// $(".newfeed-form").submit(function(e) {
// 	var url = $(this).attr('action'); 
// 	$.ajax({
// 	       type: "POST",
// 	       url: url,
// 	       data: $(this).serialize(), 
// 	     });
// 	e.preventDefault();
// 	$('.newfeed-form textarea').val('');
// 	setTimeout(function(){
// 		$(".ajax-load-newfeed").load();
// 	},100);
// });
// // load all new feed profile
// jQuery(document).ready(function($) {
// 	$(".ajax-load-newfeed").load(function(e) {
// 	var url = $(this).data('load-newfeed'); 
// 	$.ajax({
// 	       type: "POST",
// 	       url: url,
// 	        data:'', 
//            success: function(data){
//            		$('.ajax-load-newfeed').html(data);
//            }
// 	});	
// 	e.preventDefault();
// 	})
// 	$(".ajax-load-newfeed").load();
// })
// setting country user
function initialize2() {
    options = {
        types: ['(regions)']
    };
    var input = document.getElementById('country-user'); //
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var address_components = autocomplete.getPlace().address_components;
        var city = '';
        var country = '';
        for (var j = 0; j < address_components.length; j++) {
            city = address_components[0].short_name;
            if (address_components[j].types[0] == 'country') {
                country = address_components[j].short_name;
                console.log(address_components[j]);
            }
        }
        //document.getElementById('data').innerHTML="City Name : <b>" + city + "</b> <br/>Country Name : <b>" + country + "</b>";
        document.getElementById('country-hide').value = country;
        document.getElementById('city-hide').value = city;
    });
}
google.maps.event.addDomListener(window, 'load', initialize2);
/* fix banner menu */
// if($('.side-menu').hasClass('active')){
// 	$('.side-menu-banner-hide').addClass('active');
// } else{
// 	$('.side-menu-banner-hide').removeClass('active');
// }
// 1040 <=
jQuery(document).ready(function($) {
    $(window).load(resize_menu).resize(function() {
        setTimeout(resize_menu, 500)
		
    });

    function resize_menu() {
        var body_width = $(window).width();
        if (body_width > 1366) {
            width = (body_width - 940) / 2 + 20;
            if (width > 350) {
                width = 350;
            }
        } else if (body_width > 1050) {
            var width = (body_width - 940) / 2;
            if (width <= 200) {
                width = 220;
            }
        } else {
            width = 300;
        }
        //$('.side-menu').css({'width':width-25 + 'px', 'overflow-x' : 'hidden'});
        //$('ul.list-side-menu a.side-menu-link').css({'width':width + 'px'});
        $('ul.list-side-menu a.side-menu-link li.side-menu-item').css({
            'width': width + 'px'
        });
        /*if(body_width < 1050){
        	$('ul.list-side-menu a.side-menu-link .mobile-search').css({'max-width':width + 'px', 'width':width + 'px', 'padding-left':'70px'});
        } else{
        	$('ul.list-side-menu a.side-menu-link .mobile-search').css({'max-width':width + 'px', 'width':width + 'px'});

        }*/
        len2 = ('ul.list-side-menu a.side-menu-link').length;
        len = $('ul.list-side-menu a.side-menu-link li.side-menu-item').length;
        height2 = [];
        height = [];
        setTimeout(function() {
            for (var i = 1; i < len; i++) {
                //$('ul.list-side-menu a.side-menu-link').eq(i).css('min-height','auto');
                //$('ul.list-side-menu a.side-menu-link li.side-menu-item').eq(i).css('min-height','auto');
                height2[i] = $('ul.list-side-menu a.side-menu-link').eq(i).height();
                height[i] = $('ul.list-side-menu a.side-menu-link li.side-menu-item').eq(i).height();
                if (body_width < 1050) {
                    $('ul.list-side-menu a.side-menu-link').eq(i).css({
                        'min-height': height[i] + 'px',
                        'padding': '0px 0px'
                    });
                    $('.mobile-side-menu .mobile-side-menu-view .side-menu-top ul.list-side-menu a.side-menu-link li.side-menu-item').css('padding', '10px 25px 10px 60px');
                } else {
                    //$('ul.list-side-menu a.side-menu-link li.side-menu-item').eq(i).css({'min-height':height[i] + 'px', 'padding':'0px 25px 0px 60px'});
                    //$('ul.list-side-menu a.side-menu-link').eq(i).css({'min-height':height[i] + 'px', 'padding':'10px 0px'});
                }
                // })
            }
        }, 500);
        /*$('.side-menu .side-menu-view').css({'width':width + 'px','overflow-x:':'hidden'});
        $('.side-menu .side-menu-top').css('width',width + 'px');
        $('.list-side-menu').css('width',width + 'px');*/
        //$('.side-menu .mobile-social').css({ 'width':width-60 + 'px'});
        //$('.mobile-side-menu .mobile-social').css({'max-width': width + 'px', 'width':width-60 + 'px'});
        //$('.mobile-side-menu .mobile-side-menu-view .side-menu-middle').css({'max-width': width + 'px', 'width':width-60 + 'px'});
        //$('.side-menu-banner').css({'max-width': width + 'px', 'width':width-60 + 'px'});
        //$('.side-menu .side-menu-middle').css({'max-width':width-60 + 'px','width':'100%'});
        //$('.side-menu .side-menu-middle .wrap-trending').css({'width':width-60 + 'px'});
        //$('.side-menu .side-menu-middle .side-menu-banner').css({'width':width-60 + 'px'});
        if (body_width < 1050) {
            //$('ul.list-side-menu a.side-menu-link').removeAttr('style');
            //$('ul.list-side-menu a.side-menu-link').css({'padding':'10px 25px 10px 60px'});
        }
    }
});
// remove overlay black wrap side menu active
$(window).resize(function() {
        setTimeout(function() {
            if ($(window).width() > 1050) {
                $('.wrap-side-menu').removeClass('active');
                $('.mobile-side-menu').removeClass('active');
            }
        }, 300);
    })
    // click_menu_setting1 = 0;
    // $('.side-menu-setting1').click(function(){
    // 	setTimeout(function(){
    // 		$(this).find(a).css('pointer-events','inherit');
    // 	},3000);
    // })
    // $('.side-menu-setting1').on("tap",function(){
    //   $(this).addClass('active');
    // });
$(document).ready(function() {
    $(".menu-profile .menu-style-social .search-social").on("click", function() {
        $(".form_search_music").css({ "display": "block" });
    });
    $(".form_search_music > .close").on("click", function() {
        $(".form_search_music").css({ "display": "none" });
    });
    if (login == 'yes') {
        $(".main-content").addClass("fixw0");
		$(".main-content.mt140.fullpagecol").addClass("container");
		$(".two-col").addClass('col-md-9');
        $(".box-content-1").addClass("fixw1");
    } else {
        $(".main-content").removeClass("fixw0");
        $(".box-content-1").removeClass("fixw1");
		$(".main-content.mt140.fullpagecol").removeClass("container");
		$(".two-col").removeClass('col-md-9');
    }
});






$('.side-menu-link-mb-click-music-player').click(function(e) {
    e.preventDefault();

    $(".icon-show-menu-mobile.pull-left").click();
    if ($('.playlists-form').is(':visible')) {
        $('.playlists').show();
        $('.playlists-form').hide();
        return;
    }


    if (!$('.playlist').hasClass('active')) {
        $('.playlist').addClass('active');

        $('.playlists').show();
        $('.playlists-form').hide();

        if (!$('.music').hasClass('active')) {
            $('.open-music').click();
        }
    } else {
        $('.playlist').removeClass('active');

        if ($('.music').hasClass('active')) {
            $('.open-music').click();
        }
    }

});




$('.search-social.pull-left.icon-music').click(function() {

    if ($('.playlists-form').is(':visible')) {
        $('.playlists').show();
        $('.playlists-form').hide();
        return;
    }

    if (!$('.playlist').hasClass('active')) {
        $('.playlist').addClass('active');

        $('.playlists').show();
        $('.playlists-form').hide();

        if (!$('.music').hasClass('active')) {
            $('.open-music').click();
        }
    } else {
        $('.playlist').removeClass('active');

        if ($('.music').hasClass('active')) {
            $('.open-music').click();
        }
    }

});


$('.search-social.pull-left.icon-find').click(function() {

    if ($('.playlists').is(':visible')) {
        $('.playlists-form').show();
        $('.playlists').hide();
        return;
    }

    if (!$('.playlist').hasClass('active')) {
        $('.playlist').addClass('active');

        $('.playlists-form').show();
        $('.playlists').hide();


        if (!$('.music').hasClass('active')) {
            $('.open-music').click();
        }
    } else {
        $('.playlist').removeClass('active');

        if ($('.music').hasClass('active')) {
            $('.open-music').click();
        }
    }

});


$('.text-center-content-blog').find('a:not(.default)').attr('target', '_blank');
$(window).on("load", function() {

    $(".mobile-login").on("click", function() {
        //form_signup_display();
    });
});

$(document).ready(function() {
   $("#addevent").on("click", function() {
	   $(".popup-fb").css("display","block");
    });
	$(".icon-fire").on("click", function() {
		if($('.inner-event').hasClass("active")){
			$('.inner-event').removeClass('active');
		}
		else{
			$('.inner-event').addClass('active');
		}
	 
    });
    $(".power-off-login").on("click", function(){
        $("#setting5").removeClass("fix-fade");
    });
    
});

window.CallParent = function(accessToken, type, fb_page_event_list_data) { // accessToken is not used
	if(type == 1){
		$('#facebook_fetch_button').hide();
		$('#facebook_fetch_message').show();
		$('#facebook_fetch_message .message_get_page').show();
		var fb_page_list_data = $.parseJSON(fb_page_list);
		// console.log(fb_page_list_data);
		var i = 0;
		for(i; i< fb_page_list_data.length; i++){
			var fb_page = fb_page_list_data[i];
			var html = '<a href="'+ path_url + 'ajax/event/'+fb_page['id'] +'" class="list-group-item listpag0" target="_blank">'+ fb_page['name'] +'</a>';
			$('#facebook_page_list').append(html);
		}
	}
	else if(type == 2){
		$('#facebook_fetch_message .message_get_page').hide();
		$('#facebook_fetch_message .message_get_event').show();
		var i = 0;

		for(i; i< fb_page_event_list_data.length; i++){
			var fb_page_event = fb_page_event_list_data[i];
			var html = '<a onclick ="return show_popup_fb(\''+ path_url + 'ajax/save_event/'+fb_page_event['event_id']+'\')" class="list-group-item listpag0" target="_blank"> event: '+ fb_page_event['name'] +'</a>';

			$('#facebook_page_event_list').append(html);
			$('#facebook_page_event_list').replaceAll('#facebook_page_list');
		}
	}
}
$(window).on("load resize", function() {
	
	if ($(window).width() >= 780) {
        var fixwidth = $( window ).width();
		var two_col = $(".two-col").width();
		var fixwith_home = $(".main-content").width();
		var total=two_col + fixwith_home;
		var width_medium= (fixwidth- (total))/2 ;
		var w_music= 350;
		$(".section-music .music").css({'width': w_music + 'px'});
		$(".section-music .playlist").css({'width': w_music + 'px'});
		$(".inner-event").css({'width': w_music + 'px'});     
    }
	else{
		$(".section-music .music").css({'width': '100%'});
		$(".section-music .playlist").css({'width': '100%'});
		$(".inner-event").css({'width': '100%'}); 
	}
	
});



var event_start = 0;
function event_ajax_load(section_event_jele){
	var see_more_jele = section_event_jele.find('.event_more_btn');
	var url = path_url + 'ajax/event';
	$.post(url, {'start': event_start}, function(response){
		if(response){
			var data = $.parseJSON(response);
			
			if(data.html){
				section_event_jele.find('.box-inner').append(data.html);
				event_start += event_per_page;
			}
			if(!data.is_more){
				see_more_jele.hide();
			}
		}
	});
	
}
function event_init(){
	var section_event_jele = $(".section-event");
	if(section_event_jele.length){
		pr('event_init - section_event_jele.length = ' + section_event_jele.length);
		section_event_jele.each(function(){
			event_ajax_load($(this));
		});
	}
}

$(document).ready(function() {
	event_init();
	$(".inner-event .close").on("click", function() {
		$(".inner-event").removeClass('active');
	});
	$(".show_cal").on("click", function() {
		$(".fixshow").removeClass("show_hide_cal");	
	});
	$(".hide_cal").on("click", function() {
		$(".fixshow").addClass("show_hide_cal");
	});
});


$('#search_event').on('keyup', function(){
    var keyword = $('#search_event').val();
    var url = path_url + 'ajax/search_event';
	$.post(url, {'keyword': keyword}, function(response){
		if(response){
			var data = $.parseJSON(response);
			if(data.html){
				$('.box-inner').html(data.html);
			}
			if(!data.is_more){
				see_more_jele.hide();
			}
		}
	}); 
});
