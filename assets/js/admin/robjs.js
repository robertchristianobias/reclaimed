$(document).on('click', '.push-to-fb', function(e) {
    e.preventDefault();

    var $rec_id = $(this).data('rec-id');

    $.post(root + 'admincp_events/push_to_fb', {
        rec_id: $rec_id
    }, function(data) {

    });
});

$(document).ready(function() {

    $('#submit_push').click(function(e) {
        e.preventDefault();

        var $form = $('#push_fb_form');
            $btn = $(this);

        //$btn.button('loading');

        $.post(root + 'admincp_events/process_push', $form.serializeArray(), function(data) {

            var $response = $.parseJSON(data);
            
            $btn.button('reset');
            if ($response.success === false) {				
				// displays individual error messages
				if ($response.errors) {
					for (var form_name in $response.errors) {
						$('#error-' + form_name).html($response.errors[form_name]);
                    }
                    alertify.error($response.message);
				}
			} else {

                alertify.alert('Success!', 'Successfully pushed the event', function() { 
    				window.location.reload(true); 
                });
            }
        });
    });

    $('#save_event_url').click(function(e) {
        e.preventDefault();

        var $form = $('#eb-event-form');
            btn  = $(this);

        btn.button('loading');
        $.post(root + 'admincp_events/process_event_url', $form.serializeArray(), function(data) {
            var $response = $.parseJSON(data);
                
            btn.button('Import');
            if ($response.success === false) {

				// reset the button
				btn.button('reset');
				
				// displays individual error messages
				if ($response.errors) {
					for (var form_name in $response.errors) {
						$('#error-' + form_name).html($response.errors[form_name]);
                    }
                    alertify.error($response.message);
				}
			} else {

                alertify.alert('Success!', 'Successfully added the event', function() { 
    				window.location.reload(true);
                    
                });

            }
        });
    });
})