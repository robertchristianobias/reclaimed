$(document).ready(function(e) {
	if ($("input[type='radio'][name='user_type'][value='7']").prop("checked",true)){
		$(".show_reader").addClass("hidden");
		$(".bt_save").removeClass("hidden");
		$(".hide_reader").addClass("hidden");
		$(".text_left").addClass("fix_text");
	}
	$("input[type='radio'][name='user_type']").change(function () {
		if($(this).val()=='7'){
			$(".show_reader").addClass("hidden");
			$(".bt_save").removeClass("hidden");
			$(".hide_reader").addClass("hidden");
			$(".text_left").addClass("fix_text");
		}
		else {
			$(".show_reader").removeClass("hidden");
			$(".bt_save").addClass("hidden");
			$(".hide_reader").removeClass("hidden");
			$(".text_left").removeClass("fix_text");
		}
	});
	$(".tag_bt").tagsinput('items');

	
	// radio button label click
	$('.step2-wrap label').on('click', function(){
		var item = $(this);
		if(item.prev()){
			item.prev().prop("checked", true);
		}
	})

	// TODO: Wrong way for copy data from previous step
	/*
	$('input#email').change(function(){
        var value = $( this ).val();
		if(login_google=="no"){
			$( ".change_email_step_1" ).val(value);
		}
    });
	$('input.username-input').change(function(){
        var value = $( this ).val();
		pr(login_google);
		if(login_google=="no"){
			$( ".change_name_step_1" ).val(value);
		}		
    });
	
	var value_country_residence = $('select#country_residence').val();
	$( "#country_step4" ).val(value_country_residence);
	var value_city_step3 = $('select.city_step3').val();
	$( "#city_step4" ).val(value_city_step3);
	
	$('select#country_residence').change(function(){
        var value = $( this ).val();
		$( "#country_step4" ).val(value);
    });
	$('select.city_step3').change(function(){
        var value = $( this ).val();
		$( "#city_step4" ).val(value);
    });
	*/
	
	// Tags
	form_signup_tags();
	
	// Image upload
	form_signup_image_upload();
	
	// Form SignUp - Handlers
	form_signup_process();
	form_signup_validate();
	form_signup_submit();
});
function form_signup_tags(){
	$('#genre_step5')
		.textext({
			plugins : 'tags autocomplete'
		})
		.bind('getSuggestions', function(e, data)
		{
			var list = genre_arr_json,
				textext = $(e.target).textext()[0],
				query = (data ? data.query : '') || ''
				;

			$(this).trigger(
				'setSuggestions',
				{ result : textext.itemManager().filter(list, query) }
			);
		});
		// $( "input[name='genre_step5']" ).attr('type', 'text');
		// $( "input[name='genre_step5']" ).attr('value', '');
		//$('#genre_step5').attr('name', 'genre_step5');
}

function form_signup_process(){
	form_signup_process_prev();
	form_signup_process_next();
}
function form_signup_process_prev(){
    //step signup pre button
    $('.signup-pre-btn').on("click", function() {
		current_tab_order--; // TODO
		step_number_displayed--;
		form_signup_process_finish = false;
		
        $(this).closest(".section-form").removeClass("active");
    });
}

var form_signup_process_finish = false;
function form_signup_process_next(){
    // step signup next button
    $('.form_signup_process_next, .form_signup_process_skip').on("click", function() {
		var flag_button_skip = $(this).hasClass('form_signup_process_skip');
		pr('form_signup_process_next - click - flag_button_skip = ' + flag_button_skip);
		
		// First: Check form's validity
		var form_multistep_valid = true;
		
		if (form_multi_step_validator && !flag_button_skip) {
			pr('form_signup_process_next - Triggers form validation - current_tab_order = ' + current_tab_order);
			// Important: Triggers form validation programmatically.
			if (!form_multi_step_validator.form()) {
				var form_check_error_step_jele = form_multi_step_jele.find('.section-form .error');
				pr('form_signup_process_next - form_check_error_step_jele');
				pr(form_check_error_step_jele);
				
				form_multistep_valid = false;
			}
		}
		
		if (form_multistep_valid) {
			if(!form_signup_process_finish){
				var step_container_current_jele = $(this).closest(".section-form");
				// pr('form_signup_process_next - step_container_current_jele');
				// pr(step_container_current_jele);
						
				var step = step_container_current_jele.attr("data-step");
				step = parseInt(step);
				
				step++;
				step_number_displayed++;
				current_tab_order = step;
				pr('form_signup_process_next - next - step++ = ' + step);
				
				var step_container_next_jele = $('.section-form[data-step="' + step + '"]');
				pr('form_signup_process_next - step_container_next_jele - will be displayed');
				pr(step_container_next_jele);
				
				pr('form_signup_process_next - display step_number_displayed = ' + step_number_displayed);
				step_container_next_jele.find('.step_number_displayed').text(step_number_displayed);
				
				role = $('[name="user_type"]:checked').val();
				// pr('form_signup_process_next - next - role = ' + role);
				role = parseInt(role);
				pr('form_signup_process_next - next - role = ' + role);
				
				if (step == 3) {
					// Normal SignUp
					if(!is_signup_special){
						// Copy from Step 1 to Step 3: email, username
						form_signup_step_to_step_copy('.section-form[data-step="1"] input[name=email]', '.section-form[data-step="3"] #email_3');
						form_signup_step_to_step_copy('.section-form[data-step="1"] input[name=username]', '.section-form[data-step="3"] #name_3');
						pr('.section-form[data-step="3"] td[name=name]');
					}
					if(role == USER_TYPE_READER){
						form_signup_process_finish = true;

						step_container_next_jele.find('.signup-next-btn').hide();
						step_container_next_jele.find('.bt_save').show();
						
						pr('form_signup_process_next - bt_save');
						pr(step_container_next_jele.find('.bt_save'));
					} else {
						step_container_next_jele.find('.signup-next-btn').show();
						step_container_next_jele.find('.bt_save').hide();
					}
				} else if (step == 4) {
					// Copy from Step 2 to Step 4: user_type
					var step_current_jele = $('.section-form[data-step="4"] .user_type_4');
					// pr('form_signup_step_to_step_copy - step_current_jele');
					// pr(step_current_jele);
					if(step_current_jele.length){
						step_current_jele.find('p').hide();
						var value_previous = $('.section-form[data-step="2"] input:radio[name=user_type]:checked').val();
						pr('form_signup_process_next - Copy from Step 2 to Step 4: user_type = ' + value_previous);
						step_current_jele.find('#' + value_previous).show();
					}
					
					// Copy from Step 3 to Step 4: country, city
					form_signup_step_to_step_copy('.section-form[data-step="3"] #country_step3', '.section-form[data-step="4"] #country_step4');
					form_signup_step_to_step_copy('.section-form[data-step="3"] #city_step3', '.section-form[data-step="4"] #city_step4');
					
				} else if (step == 5) {

					// Get user_type for
					var step_current_jele = $('.section-form[data-step="5"] .user_type_4');
					// pr('form_signup_step_to_step_copy - step_current_jele');
					// pr(step_current_jele);
					if(step_current_jele.length){
						step_current_jele.find('p').hide();
						var value_previous = $('.section-form[data-step="2"] input:radio[name=user_type]:checked').val();
						pr('form_signup_process_next - Copy from Step 2 to Step 4: user_type = ' + value_previous);
						step_current_jele.find('#' + value_previous).show();
					}
					// End Get User_type

					form_signup_process_finish = true;
					class_step3 = $('.section-form[data-step="' + step + '"]');
					if (role == USER_TYPE_VENUE || role == USER_TYPE_PROMOTER) {

						class_step3.find('.age').removeClass('hidden');
						class_step3.find('.gender').removeClass('hidden');

						class_step3.find('.founder').addClass('hidden'); //

						class_step3.find('.biiography').removeClass('hidden');
						class_step3.find('.shortbio').removeClass('hidden');
						class_step3.find('.status').removeClass('hidden');
						class_step3.find('.marketing').removeClass('hidden');
						class_step3.find('.contact').removeClass('hidden');
						class_step3.find('.distributor').addClass('hidden'); //
						class_step3.find('.labels').addClass('hidden'); //
						class_step3.find('.url').removeClass('hidden');
						class_step3.find('.genre').removeClass('hidden');

						class_step3.find('.facebook').removeClass('hidden');

						class_step3.find('.twitter').removeClass('hidden');
						class_step3.find('.bandcamp').removeClass('hidden');
						class_step3.find('.soundcloud').removeClass('hidden');
						class_step3.find('.mixcloud').removeClass('hidden');

						class_step3.find('.itunes').addClass('hidden'); //
						class_step3.find('.beatport').addClass('hidden'); //

					} else if (role == USER_TYPE_LABEL || role == USER_TYPE_MARKETER) {



						class_step3.find('.age').removeClass('hidden');
						class_step3.find('.gender').removeClass('hidden');

						class_step3.find('.founder').removeClass('hidden');
						class_step3.find('.biiography').removeClass('hidden');
						class_step3.find('.shortbio').removeClass('hidden');
						class_step3.find('.status').removeClass('hidden');
						class_step3.find('.marketing').removeClass('hidden');
						class_step3.find('.contact').removeClass('hidden');
						class_step3.find('.distributor').removeClass('hidden');

						class_step3.find('.labels').addClass('hidden'); //

						class_step3.find('.url').removeClass('hidden');
						class_step3.find('.genre').removeClass('hidden');

						class_step3.find('.facebook').removeClass('hidden');

						class_step3.find('.twitter').removeClass('hidden');
						class_step3.find('.bandcamp').removeClass('hidden');
						class_step3.find('.soundcloud').removeClass('hidden');
						class_step3.find('.mixcloud').removeClass('hidden');

						class_step3.find('.itunes').removeClass('hidden');
						class_step3.find('.beatport').removeClass('hidden');


					} else if (role == USER_TYPE_READER) {

						class_step3.find('.age').removeClass('hidden');
						class_step3.find('.gender').removeClass('hidden'); //

						class_step3.find('.founder').addClass('hidden');
						class_step3.find('.biiography').addClass('hidden');
						class_step3.find('.shortbio').addClass('hidden');
						class_step3.find('.status').addClass('hidden');
						class_step3.find('.marketing').addClass('hidden');
						class_step3.find('.contact').addClass('hidden');
						class_step3.find('.distributor').addClass('hidden');
						class_step3.find('.labels').addClass('hidden');
						class_step3.find('.url').addClass('hidden');
						class_step3.find('.genre').addClass('hidden');

						class_step3.find('.facebook').removeClass('hidden');

						class_step3.find('.twitter').addClass('hidden');
						class_step3.find('.bandcamp').addClass('hidden');
						class_step3.find('.soundcloud').addClass('hidden');
						class_step3.find('.mixcloud').addClass('hidden');

						class_step3.find('.itunes').addClass('hidden');
						class_step3.find('.beatport').addClass('hidden');
					}

				}
				
				// Show next step
				step_container_next_jele.addClass("active");
				pr('form_signup_process_next - step_container_next_jele has just been displayed');
			} else {
				pr('form_signup_process_next - Finally - Form submit');
				form_multi_step_jele.submit(); // SignUp form - submit here
			}
		}
		
		return false;
    });
}

function form_signup_step_to_step_copy(step_previous_selector, step_current_selector){
	var step_current_jele = $(step_current_selector);
	pr('form_signup_step_to_step_copy - step_current_jele');
	pr(step_current_jele);
	if(step_current_jele.length){
		var value_previous = $(step_previous_selector).val();
		pr('form_signup_step_to_step_copy - value_previous = ' + value_previous);
		step_current_jele.val(value_previous);
	}
	pr(step_current_jele);
}

function form_signup_submit(){
	$("#signup-form").submit(function(e) {
		var url = "ajax/signup"; // the script where you handle the form input.
		var root = path_url + url;   
		$.ajax({
			type: "POST",
			url: root,
			data: $("#signup-form").serialize(), // serializes the form's elements.
			success: function(data)
			{  
				if(data == 1)
				{
					$('#myModal').modal('show');
					$('.modal-body').html('You have successfully registered.');
					$('#setting5').addClass('fix-fade');
					location.href = path_url; // Refresh
				}
				else if(data == 0)
				{
					$('#myModal').modal('show');
					$('.modal-body').html('Your Email already exists!');
				}
				else if(data == 2)
				{
					$('#myModal').modal('show');
					$('.modal-body').html('Your Username already exists!');
				}
				else if(data == 3)
				{ // Validate term and conditions
					$('#myModal').modal('show');
					$('.modal-body').html('Please accept Terms and Conditions!');
				}
				else{ // TODO
					//pr(data);
					// alert(data);
				}
			}
		});
		e.preventDefault(); // avoid to execute the actual submit of the form.
	});
}



/*BEGIN: FORM VALIDATION*/
function form_signup_validate(){
	form_multi_step_jele = $('#signup-form');
	var rules_config = form_validate_movetab_build_rules();
	form_validate(rules_config);
}

var form_multi_step_jele;
// current_tab_order is defined in form_register.php
var form_multi_step_validator = null;
var step_number_displayed = 1;
function form_validate(rules_config) {
	// pr('form_validate - rules_config');
	// pr(rules_config);
	if (form_multi_step_validator) {
		form_multi_step_validator.destroy();
		pr('form_validate - form_multi_step_validator.destroy()');
	}
	form_multi_step_validator = form_multi_step_jele.validate({
		ignore: ":hidden:not(#hiddenRecaptcha)",
		rules: rules_config,
		highlight: function(element) {
			$(element).addClass('error');
			// if ($(element).attr('name') == 'file_attach') {
				// $(element).siblings('.span-click').addClass('error');
				// $(".th_bao").show();
			// }
		},
		unhighlight: function(element) {
			$(element).removeClass('error');
			// $(element).siblings('.file-enter').children('span').removeClass('error');
			// if ($(element).attr('name') == 'file_attach') {
				// $(element).siblings('.span-click').removeClass('error');
				// $(".th_bao").hide();
			// }
		},
		submitHandler: function(e) {
			// Do nothing
		}
	});
}
function form_validate_depend_tab(tab_order){
	// pr('form_validate_depend_tab - tab_order = ' + tab_order);
	// pr('form_validate_depend_tab - current_tab_order = ' + current_tab_order);
    return tab_order == current_tab_order;
}
function form_validate_movetab_build_rules() {
    pr('form_validate_movetab_build_rules - current_tab_order = ' + current_tab_order);
	
	// datepart: 'y', 'm', 'w', 'd', 'h', 'n', 's'
	Date.dateDiff = function(datepart, fromdate, todate) {
		datepart = datepart.toLowerCase();	
		var diff = todate - fromdate;	
		var divideBy = { 
			y:31536000000, 
			m:2592000000, 
			w:604800000, 
			d:86400000, 
			h:3600000, 
			n:60000, 
			s:1000 
		};	

		return Math.floor( diff/divideBy[datepart]);
	}

	jQuery.validator.addMethod("birthday_check", function(value, element) {
		var result = false;
		
		// value is real value
		pr('birthday_check - value');
		pr(value);
		
		var birthday = new Date(value);
		pr('birthday_check - birthday');
		pr(birthday);
		
		var today = new Date();
		pr('birthday_check - today');
		pr(today);
		var year_diff = Date.dateDiff('y', birthday, today); //displays 625
		pr('birthday_check - year_diff = ' + year_diff);
		pr(year_diff);
		if(year_diff >  16){
			result = true;
		}
		
		return result;
	}, "You are too young");
	
	
	jQuery.validator.addMethod("google_captcha_check", function(value, element) {
		var result = true;
		
		if (grecaptcha.getResponse() == '') {
			result = false;
		}
		
		return result;
	}, "You must complete the antispam verification");

    var rules_config = {
        // Step 1
		email: {
            required: { depends: function(element){return form_validate_depend_tab(1);} },
            email: { depends: function(element){return form_validate_depend_tab(1);} }
        },
		username: {
            required: { depends: function(element){return form_validate_depend_tab(1);} }
        },
		password: {
            required: { depends: function(element){return form_validate_depend_tab(1);} }
        },
        hiddenRecaptcha: {
			required: { depends: function(element){return form_validate_depend_tab(1);} },
			google_captcha_check: { depends: function(element){return form_validate_depend_tab(1);} }
        },
        
        // Step 2
        user_type: {
            required: {
                depends: function(element){return form_validate_depend_tab(2);}
            },
        },

        // Step 3
        name: {
            required: { depends: function(element){return form_validate_depend_tab(3);} }
        },
		email_3: {
            required: { depends: function(element){return form_validate_depend_tab(3);} },
			email: { depends: function(element){return form_validate_depend_tab(3);} }
        },
		location: {
            required: { depends: function(element){return form_validate_depend_tab(3);} }
        },
		bday: {
            required: { depends: function(element){return form_validate_depend_tab(3);} },
            birthday_check: { depends: function(element){return form_validate_depend_tab(3);} }
        },
        
		
        // Step 4
		user_type_name: {
            required: { depends: function(element){return form_validate_depend_tab(4);} }
        },
		comment: {
            required: { depends: function(element){return form_validate_depend_tab(4);} }
        },
		
        // Step 5
		genre_step5: {
            required: { depends: function(element){return form_validate_depend_tab(5);} }
        },
		key_location: {
            required: { depends: function(element){return form_validate_depend_tab(5);} }
        }
		
    };

    return rules_config;
}
/*END: FORM VALIDATION*/


// BEGIN - IMAGE UPLOAD
function form_signup_image_upload() {
	// Image 1
	var croppicHeaderOptions = {
			uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
			cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
			uploadData:{'prefix':'avatar_'},
			enableMousescroll:true,
			customUploadButtonId:'cropContainerHeaderButton',
			outputUrlId:'input_thumbnail_url',
			modal:true,
			rotateControls: false,
			doubleZoomControls:false,
			imgEyecandyOpacity:0.4,
			onBeforeImgUpload: function(){ },
			onAfterImgUpload: function(){ appendOriginImageAward(); },
			onImgDrag: function(){ },
			onImgZoom: function(){ },
			onBeforeImgCrop: function(){ },
			onAfterImgCrop:function(){appendAward(); },
			onReset:function(){ onResetCropic(); },
			onError:function(errormessage){ pr('onError:'+errormessage) }
	}
	var croppic = new Croppic('cropic_element', croppicHeaderOptions);
	function appendOriginImageAward() {
		var url_origin = $("div#croppicModalObj > img").attr('src');
		$('#input_image_url').val(url_origin);
		$("#preview_image_fancybox").attr("href", $('#input_image_url').val());
	}
	function appendAward(){
		$("#preview_image").attr("src", $('#input_thumbnail_url').val());
		$("#preview_image").show();
		$(".overclick").css({"background": "none"});
	}
	function onResetCropic(){
		$('#input_image_url').val('');
		$('#input_thumbnail_url').val('');
		$("#preview_image_fancybox").attr("href", '');
		$("#preview_image").attr("src", '');
		alert("1");
		$(".overclick").css({"background": "none"});
	}

	// Image 2
	var croppicHeaderOptions_new = {

			uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
			cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
			uploadData:{'prefix':'avatar_'},
			enableMousescroll:true,
			customUploadButtonId:'cropContainerHeaderButton_new',
			outputUrlId:'input_thumbnail_url_new',
			modal:true,
			rotateControls: false,
			doubleZoomControls:false,
			imgEyecandyOpacity:0.4,
			onBeforeImgUpload: function(){ },
			onAfterImgUpload: function(){ appendOriginImageAward_new(); },
			onImgDrag: function(){ },
			onImgZoom: function(){ },
			onBeforeImgCrop: function(){ },
			onAfterImgCrop:function(){appendAward_new(); },
			onReset:function(){ onResetCropic_new(); },
			onError:function(errormessage){ pr('onError:'+errormessage) }
	}
	var croppic = new Croppic('cropic_element_new', croppicHeaderOptions_new);
	function appendOriginImageAward_new() {
		var url_origin = $("div#croppicModalObj > img").attr('src');
		$('#input_image_url_new').val(url_origin);
		$("#preview_image_fancybox_new").attr("href", $('#input_image_url_new').val());
		$(".overclick").css({"background": "none"});
		
	}
	function appendAward_new(){
		$("#preview_image_new").attr("src", $('#input_thumbnail_url_new').val());
		$("#preview_image_new").show();
		$(".overclick").css({"background": "none"});
	}
	function onResetCropic_new(){
		$('#input_image_url_new').val('');
		$('#input_thumbnail_url_new').val('');
		$("#preview_image_fancybox_new").attr("href", '');
		$("#preview_image_new").attr("src", '');
		$(".overclick").css({"background": "none"});
		alert("2");
	}
}
// END - IMAGE UPLOAD


function form_signup_display() {
    //check content active
    var contentSignupFormActive = $(".signup-right").hasClass("active"); // .signup-right is signup form
    $(".signup-right").addClass("active");
    $('body').css("overflow", "hidden");
    //check signup has active
    if (contentSignupFormActive) {
        if ($("#signup-form").hasClass('active')) {
            $(".signup-right").removeClass("active");
            $('body').css("overflow", "auto");
        } else {
            $(".signup-right .nav-tabs a[href='#signup-form']").click();
        }
    } else {
        $(".signup-right .nav-tabs a[href='#signup-form']").click();
    }
}


// Function Edit on Step 3
function editName(){
	if($("#editName").text() === "Edit"){
		$("#editName").html('Save');
		$("#name_3").prop('readonly',false);
		$('#name_3').focus();
	}else{
		$("#editName").html('Edit');
		$("#name_3").prop('readonly',true);
	}
}

function editEmail(){
	if($("#editEmail").text() === "Edit"){
		$("#editEmail").html('Save');
		$("#email_3").prop('readonly',false);
		$('#email_3').focus();
	}else{
		$("#editEmail").html('Edit');
		$("#email_3").prop('readonly',true);
	}
}

function editCountry(){
	if($("#editCountry").text() === "Edit"){
		$("#editCountry").html('Save');
		$("#country_step3").prop('readonly',false);
		$('#country_step3').focus();
	}else{
		$("#editCountry").html('Edit');
		$("#country_step3").prop('readonly',true);
	}
}

function editCity(){
	if($("#editCity").text() === "Edit"){
		$("#editCity").html('Save');
		$("#city_step3").prop('readonly',false);
		$('#city_step3').focus();
	}else{
		$("#editCity").html('Edit');
		$("#city_step3").prop('readonly',true);
	}
}

function editBirthday(){
	if($("#editBirthday").text() === "Edit"){
		$("#editBirthday").html('Save');
		$('.hidden-sp').click();
	}else{
		$("#editBirthday").html('Edit');	
	}
}

function editGender(){
	if($("#editGender").text() === "Edit"){
		$("#editGender").html('Save');
		$("#gender").val("Female").trigger("click");
	}else{
		$("#editGender").html('Edit');	
	}
}

function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}