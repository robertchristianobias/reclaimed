var tag = document.createElement('script');
tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


var player,
time_update_interval = 0;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        //width: 600,
        //height: 400,
        videoId: videoId,
        playerVars: {
            //color: 'white',
            //playlist: 'taJ60kskkns,FG0fTKAqZ5g'
        },
        events: {
            onReady: initialize
        }
    });
}

function initialize(){

    /*// Update the controls on load
    updateTimerDisplay();
    updateProgressBar();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
        updateTimerDisplay();
        updateProgressBar();
    }, 1000);


    $('#volume-input').val(Math.round(player.getVolume()));*/
}


$(document).ready(function(){

	/*console.log(SC.Widget('soundcloud-placeholder'));

	var soundcloud ;
	soundcloud= SC.Widget("soundcloud-placeholder");*/


	$.music = function(){
		$music = $('.section-music .music');
 		$playlist  = $('.section-music .playlist');
		$all_songs = $('.section-music .music .song-order');
		$embed_contain = $music.find('.embed-song');
		$embed = $music.find('iframe');
		$play = $music.find('.play');

		$play.on('click', function(){
			$embed_contain.show();
			$song = $(this).parents('.song-order');
			$all_songs.removeClass('active');
			$song.addClass('active');

			var current = $song.index();
			var link = "";
			var type = $song.data('type');
			var code = $song.data('code');

			$playlist.find('.controls').attr('current', current);

			if(type == 'youtube'){
				link = "https://www.youtube.com/embed/" + code + "?enablejsapi=1&autoplay=1";
				player.loadVideoById(code);

			}else{
				if(type == 'soundcloud'){
					link = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" + code + "&amp;auto_play=true";
					$embed.attr('src', link);
				}
			}
			//show embed
			if(!$embed_contain.hasClass('active')){
				$embed_contain.addClass('active').fadeIn();
			}
			$('#m-play').addClass('active');
			
		});

		$('#open-music').on('click', function(){
			$(this).children().toggleClass('hide');
			$music.toggleClass('active');
		});

		$('#m-play').on('click', function () {
			$embed_contain.show();
			$this  = $(this);
			if(!$this.hasClass('active')){
				player.playVideo();
			}else{
				player.pauseVideo();
			}
			$(this).toggleClass('active');
		});

		$('#soundcloud #m-play').on('click', function () {

			$embed_contain.show();
			$this  = $(this);
			if(!$this.hasClass('active')){
				soundcloud.play();
			}else{
				soundcloud.pause();
			}
			$(this).toggleClass('active');
		});

		$('#m-backward').on('click', function () {
		//	console.log('back-yt');
			var current = $playlist.find('.controls').attr('current') * 1;
			current = current-1;

			$music.find('.song-order').eq(current - 1).find('.play').trigger('click');
		});
		$('#m-forward').on('click', function () {
			//console.log('back-for');
			var current = $playlist.find('.controls').attr('current') * 1;
			var current_max = $playlist.find('.controls').attr('current_max') * 1;

			console.log('current: ' + current + ' ' + 'current_max :' + current_max );

			if ( current == current_max ){
				current = 1;
			} else{
				current = current+1;
			}

			$music.find('.song-order').eq(current - 1).find('.play').trigger('click');
		});

		$('#m-reload').on('click', function () {
			var current = $playlist.find('.controls').attr('current') * 1;
			$music.find('.song-order').eq(current - 1).find('.play').trigger('click');
		});
	}

	$.youtube = function(){
		
	}
	$(window).on('load', function(){
		$.music();
		$.youtube();
	});
});