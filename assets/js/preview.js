"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
  _inherits(App, _React$Component);

  function App() {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this));

    _this.state = {
      data: myDataSet,
      preview: false
    };
    _this._handlePreviewClick = _this._handlePreviewClick.bind(_this);
    _this._handleEditClick = _this._handleEditClick.bind(_this);
    return _this;
  }

  App.prototype._handlePreviewClick = function _handlePreviewClick(e) {
    e.preventDefault();
    this.setState({ preview: true });
  };

  App.prototype._handleEditClick = function _handleEditClick(e) {
    e.preventDefault();
    this.setState({ preview: false });
  };

  App.prototype._renderPreview = function _renderPreview() {
    var _this2 = this;

    if (this._text) {
      var htmlText = function htmlText() {
        return { __html: _this2._text.value };
      };
      return React.createElement(
        "div",
        null,
        React.createElement(
          "div",
          { className: "preview-box" },
          React.createElement(
            "span",
            { className: "preview-title" },
            "Preview"
          ),
          React.createElement("div", { dangerouslySetInnerHTML: htmlText() })
        ),
        React.createElement(
          "button",
          { onClick: this._handleEditClick },
          "Edit"
        )
      );
    }
    return null;
  };

  App.prototype.render = function render() {
    var _this3 = this;

    var editStyle = {};
    if (!this.state.preview) editStyle.display = 'block';else editStyle.display = 'none';

    var previewStyle = {};
    if (this.state.preview) previewStyle.display = 'block';else previewStyle.display = 'none';
    return React.createElement(
      "div",
      null,
      React.createElement(
        "h1",
        null,
        this.state.data.title
      ),
      React.createElement(
        "form",
        null,
        React.createElement(
          "div",
          { style: editStyle },
          React.createElement("textarea", {
            ref: function ref(f) {
              return _this3._text = f;
            },
            defaultValue: this.state.data.text,
            className: "textarea" , name:"contentAdmincp" , rows:"8", id:"contentAdmincp",  }),

          React.createElement(
            "button",
            { onClick: this._handlePreviewClick },
            "Preview"
          )
        ),
        React.createElement(
          "div",
          { style: previewStyle },
          this._renderPreview()
        )
      )
    );
  };

  return App;
}(React.Component);

ReactDOM.render(React.createElement(App, null), document.getElementById('app'));