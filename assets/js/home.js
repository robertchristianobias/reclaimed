var articles_latest_start = article_latest_per_page_init + 1;
function get_articles_latest_more(){
	var see_more_jele = $('#list-post-see-more');
	var url = path_url + 'ajax/articles_latest';
	$.post(url, {'start': articles_latest_start}, function(response){
		if(response){
			var data = $.parseJSON(response);
			
			if(data.html){
				see_more_jele.before(data.html);
				articles_latest_start += article_latest_per_page;
			}
			if(!data.is_more){
				see_more_jele.hide();
			}
		}
	});
	
}