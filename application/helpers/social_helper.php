<?php
function social_login($type, $callback_url, $social_sdk_obj=null){
	$result = null;
	
	$user_profile = array();
	
	switch($type){
		case 'facebook':
			$result = social_login_facebook($callback_url, $social_sdk_obj);
			if(!empty($result['userinfo']['id'])){
				$userinfo = $result['userinfo'];
				$user_profile['oauth_uid'] = $userinfo['id'];
				$user_profile['name'] = $userinfo['first_name'].' '.$userinfo['last_name'];
				$user_profile['first_name'] = $userinfo['first_name'];
				$user_profile['last_name'] = $userinfo['last_name'];
				
				$user_profile['picture_url'] = !empty($userinfo['picture']['data']['url']) ? $userinfo['picture']['data']['url'] : '';
			}
			break;
			
		case 'google':
			$result = social_login_google($callback_url);
			if(!empty($result['userinfo'])){
				$userinfo = $result['userinfo'];
				$user_profile['oauth_uid'] = $userinfo['id'];
				$user_profile['name'] = $userinfo['name'];
				$user_profile['first_name'] = $userinfo['given_name'];
				$user_profile['last_name'] = $userinfo['family_name'];
				
				$user_profile['picture_url'] = $userinfo['picture'];
			}
			
			break;
	}
	
	if(!empty($user_profile)){
		$user_profile['email'] = $userinfo['email'];
		$user_profile['oauth_provider'] = $type;
		$user_profile['locale'] = !empty($userinfo['locale']) ? $userinfo['locale'] : ''; // Need "user_location" permission for Facebook
		
		$result['user_profile'] = $user_profile;
	}
	
	return $result;
}

//login google
function social_login_google($callback_url){
	$result = null;
	
	if (!empty($_GET['code'])) { // Authenticated with Google
		$gClient = social_login_google_client_get($callback_url);
		//Create google OAuth2 Service
		$google_oauthV2 = new Google_Oauth2Service($gClient);
		// pr('social_login_google - $gClient');
		// pr($gClient,1);
		$gClient->authenticate();
		$access_token = $gClient->getAccessToken(); // Not used
		$userinfo = $google_oauthV2->userinfo->get();
		
		$result['access_token'] = $access_token;
		$result['userinfo'] = $userinfo;
		// pr('social_login_google - $result');
		// pr($result,1);
	} else { // Not yet authenticated => Go to $redirect_auth_url
		$gClient = social_login_google_client_get($callback_url);
		$redirect_auth_url = $gClient->createAuthUrl();
		// pr('social_login_google - $redirect_auth_url = '.$redirect_auth_url,1);
		redirect($redirect_auth_url);
	}
	
	return $result;
}

function social_login_google_client_get($callback_url){
	$gClient = null;
	
	try {
		// Include the google api php libraries
		include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
		include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
		//$this->load->library('GoogleAPI');
		
		// Google Project API Credentials
		$clientId = GG_CLIENT_ID;
		$clientSecret = GG_CLIENT_SECRET;
		
		// Google Client Configuration
		$gClient = new Google_Client();

		$gClient->setApplicationName('Login to Redm');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		
		// Callback Url - Always setRedirectUri or get error:  
		// 		Type: Google_AuthException
		//		Message: Error fetching OAuth2 access token, message: 'invalid_grant'		
		$gClient->setRedirectUri($callback_url);
		
		$gClient->setScopes(array(
				'email', 
				'profile', 
				'https://www.googleapis.com/auth/plus.login'
				));
			
	}
	catch (Exception $e) {
		redirect(base_url());
	}
	
	return $gClient;
}

// Permissions Reference - Facebook Login: https://developers.facebook.com/docs/facebook-login/permissions/v3.0
// Basic Permissions - Default fields: id,first_name,last_name,middle_name,name,name_format,picture,short_name
// Read Permissions - User Attributes: email,user_age_range,user_birthday,user_friends,user_gender,user_hometown,user_link,user_location
function social_login_facebook($callback_url, $social_sdk_obj){
	$result = null;
	
	$access_token = $social_sdk_obj->is_authenticated();
	if (!empty($access_token)) { // Authenticated with Facebook
		// Get user facebook profile details
        $userProfile = $social_sdk_obj->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,link,picture,accounts');			
		$result['access_token'] = $access_token;
		$result['userinfo'] = $userProfile;
	} else { // Not yet authenticated => Go to $redirect_auth_url
		$redirect_auth_url = $social_sdk_obj->login_url();
		// pr('social_login_facebook - $redirect_auth_url = '.$redirect_auth_url,1);
		redirect($redirect_auth_url);
	}
	
	return $result;
}

function social_login_facebook_client_get($callback_url){
	$gClient = null;
	
	try {
		// Include the facebook api php libraries
		include_once APPPATH."libraries/facebook-api-php-client/Facebook_Client.php";
		include_once APPPATH."libraries/facebook-api-php-client/contrib/Facebook_Oauth2Service.php";
		//$this->load->library('FacebookAPI');
		
		// Facebook Project API Credentials
		$clientId = GG_CLIENT_ID;
		$clientSecret = GG_CLIENT_SECRET;
		
		// Facebook Client Configuration
		$gClient = new Facebook_Client();

		$gClient->setApplicationName('Login to Redm');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		
		// Callback Url - Always setRedirectUri or get error:  
		// 		Type: Facebook_AuthException
		//		Message: Error fetching OAuth2 access token, message: 'invalid_grant'		
		$gClient->setRedirectUri($callback_url);
		
		$gClient->setScopes(array(
				'email', 
				'profile', 
				'https://www.facebookapis.com/auth/plus.login'
				));
			
	}
	catch (Exception $e) {
		redirect(base_url());
	}
	
	return $gClient;
}

/*
 * There are many types of input:
 * 2018-06-30T01:00:00-07:00
 * 2018-05-13T01:00:00-07:00 to 2018-05-13T07:00:00-07:00
 */
function parse_string_to_datetime_period($string, $is_convert_to_utc = false) {
    $separator = ' to ';
    $format = DATE_W3C;
    $date_array = explode($separator, $string);
	if(!empty($date_array) && count($date_array) > 1){
		$from_date_string = ! empty($date_array [0]) ? $date_array [0] : false;
		$to_date_string   = ! empty($date_array [1]) ? $date_array [1] : false;
	} else {
		$from_date_string = $to_date_string = $string;
	}
    
    $from_datetime = DateTime::createFromFormat($format, $from_date_string);
    $to_datetime   = DateTime::createFromFormat($format, $to_date_string);
    if ($from_datetime && $to_datetime) {
        if ($is_convert_to_utc === true) {
            $from_datetime->setTimezone(new DateTimeZone('UTC'));
            $to_datetime->setTimezone(new DateTimeZone('UTC'));
        }
        $result = array(
                'from_datetime' => $from_datetime,
                'to_datetime' => $to_datetime,
            );
    } else {
        $result = false;
    }
    return $result;
}

function get_geocoding_by_google($api_key, $address, $return_types = false) {
	$final_result = array();
	$default_component_types = array('street_address', 'route', 'country', 'locality', 'postal_code');
	$check_types = ($return_types === false) ? $default_component_types : $return_types;
    $parameters = array(
            'key' => $api_key,
            'address' => $address,
        );
    $query_string = http_build_query($parameters);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?' . $query_string;
    // echo $url;
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT      => 'REDM, request by cURL via php',
    ));
    $response = curl_exec ($ch);
    curl_close ($ch);
    // pr($response);
    $data = @json_decode($response, TRUE);
    // $data = @json_decode($response, TRUE);

    if (empty($data ['status'])) {
    	die('cannot request to Google API');
    }
    $rs = array();
    // echo (! empty($data ['results'])) ? 'true' : 'false';
    if ($data ['status'] == 'OK' && ! empty($data ['results'])) {
    	foreach ($data ['results'] as $rs_key => $result) {
    		foreach ($result as $key => $value) {
    			if ($key == 'address_components') {
    				foreach ($value as $k_component => $item) {
    					//long_name, short_name, types
	    				$types = isset($item ['types']) ? $item ['types'] : array();
	    				foreach ($types as $k => $type) {
	    					if (in_array($type, $check_types)) {
	    						$final_result [$type] = isset($item ['long_name']) ? $item ['long_name'] : '';
	    					} else {
	    						$final_result [$type] = '';
	    					}
	    				}
    				}// end foreach address_components
    				$final_result ['address_components'] = $value;
    				$is_exists = true;
    			}
    			if ($is_exists === true) break;
    		} // end foreach result detail with key: address_components, formatted_address, geometry, place_id, types
    		if ($is_exists === true) break;
    	}; //end foreach results
    }
    return $final_result;
}