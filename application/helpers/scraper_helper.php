<?php
require_once "ultimate-web-scraper-master/support/web_browser.php";
require_once "ultimate-web-scraper-master/support/tag_filter.php";

function scrape_html_from_url($url){
	// Retrieve the standard HTML parsing array for later use.
	$htmloptions = TagFilter::GetHTMLOptions();

	// Turn on the automatic forms extraction option.  Note that Javascript is not executed.
	$web = new WebBrowser(array("extractforms" => true));
	$result = $web->Process($url);
	
	$data_return = array(
		'web' => $web,
		'result' => $result,
	);
	return $data_return;
}

/*
 * 	PHP library/toolkit - Ultimate Web Scraper Toolkit
 * 	Example HTML form extraction
 * 	TODO - Email/Pass
 */
function scrape_html_from_url_facebook($url, $flag_debug = false){
	$html = '';
	
	$data = scrape_html_from_url($url);
	// pr('scrape_html_from_url_facebook - $data');
	// pr($data, 1);
	$result = $data['result'];
	$web = $data['web'];
	
	$flag_error = empty($result) || empty($web);
	if (!$flag_error && !$result["success"])
	{
		$flag_error = true;
		if($flag_debug){
			echo "Error retrieving URL.  " . $result["error"] . "\n";
			exit();
		}
	}

	if (!$flag_error && $result["response"]["code"] != 200)
	{
		$flag_error = true;
		if($flag_debug){
			echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
			exit();
		}
	}

	if (!$flag_error && count($result["forms"]) != 1)
	{
		$flag_error = true;
		
		if($flag_debug){
			echo "Was expecting one form.  Received:  " . count($result["forms"]) . "\n";
			exit();
		}
	}
	
	if(!$flag_error){
		// Forms are extracted in the order they appear in the HTML.
		$form = $result["forms"][0];
		// pr('scrape_html_from_url_facebook() - $form');
		// pr($form,1);
			

		// Set some or all of the variables in the form.
		$form->SetFormValue("email", "tieubao.ctt@gmail.com");
		$form->SetFormValue("pass", "mxhhoangcung");

		// Submit the form.
		$result2 = $form->GenerateFormRequest();
		$result = $web->Process($result2["url"], $result2["options"]);
		$result = $web->Process($url); // Go to this $url again to pull Event's data

		if (!$result["success"])
		{
			$flag_error = true;
			if($flag_debug){
				echo "Error retrieving URL.  " . $result["error"] . "\n";
				exit();
			}
		}

		if ($result["response"]["code"] != 200)
		{
			$flag_error = true;
			if($flag_debug){
				echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
				exit();
			}
		}
		
		if(!$flag_error && !empty($result["body"])){
			$html = $result["body"];
		}
		
		// pr('scrape_html_from_url_facebook() - $result Afer form login is submitted');
		// pr($result,1);
	
	}
	
	return $html;
	
	/*
	// Use TagFilter to parse the content.
	$html = TagFilter::Explode($result["body"], $htmloptions);

	// Do something with the response here...
	*/
}