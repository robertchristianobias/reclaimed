<?php
/****************************	BEGIN: SESSION HELPER ***************************/
function session_check(){
	$ci =& get_instance();
	$session_data = $ci->session->userdata('userData');
	// pr($session_data,1);
	$result = !empty($session_data);
	
	return $result;
}

function session_get($key){
	$result = '';
	
	$ci =& get_instance();
	
	if(isset($ci->session->userdata['userlogin'][$key])){
		$result = $ci->session->userdata['userlogin'][$key];
	}
	
	return $result;
}

function session_name_get(){
	$result = '';
	
	$name = session_get('name');
	if(!empty($name)){
		$result = $name;
	} else {
		$username = session_get('username');
		$result = $username;
	}
	
	return $result;
}
/****************************	END: SESSION HELPER ***************************/
