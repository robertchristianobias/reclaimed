<?php
require "php-html-parser-master/vendor/autoload.php";
use PHPHtmlParser\Dom;
	
function load_html($html){
	// Assuming you installed from Composer:
	$dom = new Dom;
	$dom->load($html);
	
	return $dom;
}
