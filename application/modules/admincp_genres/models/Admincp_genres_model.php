<?php
class Admincp_genres_model extends CI_Model {
	private $module = 'admincp_genres';
	private $table = 'admin_nqt_genre';
	private $table_lang = 'admin_nqt_genre';
	private $field_parent_id = 'id';
	private $table_category = 'admin_nqt_category_articles';

	function getsearchContent($limit,$page){
		$this->db->select('*');
		$this->db->limit($limit,$page);
		$this->db->order_by($this->input->post('func_order_by'),$this->input->post('order_by'));
		if($this->input->post('content')!=''){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalsearchContent(){
		$this->db->select('*');
		if($this->input->post('content')!='' && $this->input->post('content')!='type here...'){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetailManagement($id){
		// $this->db->select(PREFIX.$this->table.'.*, id AS data_lang');
		$this->db->select('*');
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			$result = $query->row();
			return $result;
		}else{
			return false;
		}
	}
	
	function saveManagement($fileName=''){
		$name = $this->input->post('genres');
		$userTypeId = $this->input->post('userType');
		
		$userTypeName = $this->getUserTypeName($userTypeId);
		
		$data = array(
			'status'			=>	isset($_POST['status']) ? 1 : 0,
			'created'			=>	date('Y-m-d H:i:s'),
			'name'				=> $name,
			'user_type'			=> $userTypeId,
			'user_type_name'	=> $userTypeName->type,
			);
			
			if($this->input->post('hiddenIdAdmincp')==0){
			
				if($this->db->insert($this->table,$data)){
					modules::run('admincp/saveLog',$this->module,$this->db->insert_id(),'Add new','Add new');
					return true;
				}
			}
			else{
				$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
				
				$update_id = $this->input->post('hiddenIdAdmincp');
				$this->db->where('id', $update_id);
				if($this->db->update($this->table,$data)){
					modules::run('admincp/saveLog',$this->module,$this->db->insert_id(),'Edit','Edit');
					return true;
				}
			}
		return false;
	}


	function softDeleteData ($id) {
		$data ['is_delete'] = 1;
		$this->db->where('id', $id);

		if ($this->db->update (PREFIX.$this->table, $data)) {
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			//Soft-delete data in table language
			$this->db->where ($this->field_parent_id, $id);
			$this->db->update (PREFIX.$this->table_lang, $data);
			//end soft-delete data language

			// Delete tag relationship
			$query_tag = $this->db->query("
			SELECT post_id, tag_id FROM  admin_tag_acticles_relationships
	        WHERE post_id = $id
	        ");

	 		if ($query_tag->result()){
	 			$query_tag = $query_tag->result();

	 			foreach ($query_tag as $r_query_tag ){

	 				 $this->db->query("
	 				 	DELETE FROM `admin_tag_acticles_relationships` WHERE post_id = $id and tag_id = $r_query_tag->tag_id 
	 				 ");
	 			}

	 		}

			return TRUE;
		}
		return FALSE;
	}
	
	function checkData($title,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('title'.$lang,$title);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	function getCategories() {
		$result = FALSE;
		$all_lang = (isset($this->lang->languages)) ? $this->lang->languages : array('' => '');

		$this->db->select('*');
		$this->db->where('status',1);
		#check soft delete
		$this->db->where('is_delete', 0);
		$query = $this->db->get(PREFIX.$this->table_category);

		if($query->result()){
			$result = $query->result();
			foreach ($result as $key => $value) {
				$title_merge = '';
				foreach ($all_lang as $key => $lang) { 
					$_title = 'title_' . $key;
					$title_merge .= strtoupper($key) . ': ' . $value->$_title . ' | ';
				}
				$title_merge = substr($title_merge, 0, -3);
				// echo $title_merge;
				// $title_merge = $value->title_vi . ' | VI: ' . $value->title_en;
				$value->title_merge = $title_merge;
				$value->name = $title_merge;
			}
		}
		return $result;
	}
	
	function getUserType() {
		$result = FALSE;

		$this->db->select('*');
		$query = $this->db->get('user_type');
		$result = $query->result();
		return $result;
	}
	
	function getUserTypeName($userTypeId) {
		$result = FALSE;

		$this->db->select('type');
		$this->db->where('id',$userTypeId);
		$query = $this->db->get('user_type');
		$result = $query->row();
		return $result;
	}
}