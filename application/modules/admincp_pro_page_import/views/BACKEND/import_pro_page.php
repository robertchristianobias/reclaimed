<script src="<?=PATH_URL?>statics/import/upload/script.js"></script>
<link rel="stylesheet" href="<?=PATH_URL?>static/import/upload/main.css">
<script type="text/javascript" src="<?=PATH_URL?>static/map/js/json2.js"></script>

<script>

var import_file = false;
var form_upload_image_url = '<?=PATH_URL_ADMIN.$module?>/upload'; // For script.js
var sheet_index = -1;
var list_sheetnames = null;

/*BEGIN: DEBUG*/
function pr(message){
	if(console && console.log){
		console.log(message);
	}
}
/*END: DEBUG*/

function upload_finish_callback(json_str){ // For static\editor_custom\upload\js\script.
	pr('upload_finish_callback');
	pr('json_str='+json_str);
	var json_data = JSON.parse(json_str);
	var import_id = json_data['import_id'];
	var file_name = json_data['file_name'];
	pr('file_name='+file_name);
	pr('import_id='+import_id);
	ajax_list_sheetnames(file_name,import_id);
}
function ajax_list_sheetnames(file_name,import_id){
	$.post('<?=PATH_URL_ADMIN.$module?>/list_sheetnames',{'file_name':file_name},function(data){
		if(data){
			list_sheetnames = JSON.parse(data);//pr(list_sheetnames);
			sheet_index = 0;
			ajax_import_item_by_sheetname(file_name,import_id);
		}
	});
}

function ajax_import_item_by_sheetname(file_name,import_id){
	pr('In ajax_import_item_by_sheetname():');
	pr('sheet_index='+sheet_index);
	if(list_sheetnames && list_sheetnames.length && list_sheetnames[sheet_index]){
		var sheet_name = list_sheetnames[sheet_index];
		
		var sheetname_info = '"'+sheet_name+'"'+' ('+(sheet_index+1)+'/'+list_sheetnames.length+')';
		$('.loading_content .loading_text .sheetname').html(sheetname_info);
		$('.loading_content .loading_text').show();
		
		pr('sheet_name='+sheet_name);
		$.post('<?=PATH_URL_ADMIN.$module?>/import_item_by_sheetname/',{'file_name':file_name, 'import_id' : import_id, 'sheet_name':sheet_name},function(data){
			if(data=='ok'){
				sheet_index++;
				if(sheet_index == list_sheetnames.length){
					import_reset('sucess', '');
				} else {
					ajax_import_item_by_sheetname(file_name,import_id);
				}
			} else {
                var msg_error = '';
                
                if(data=='wrong_template'){
                    alert('Template fail!');
                    var msg_error = 'Template fail';
                }
                else if(data=='empty_template'){
                    alert('Template fail!');
                    var msg_error = '';
                }
                               
                import_reset('fail', msg_error);
            }
		});
	}
}

function import_reset(status, msg_error){
	hide_loading();

	$('#progress_info').hide();
	$('#import_response').show();
	var input_file_name = $('#image_file').val();//pr('input_file_name='+input_file_name);
    
    if (status == 'sucess')
        var message = '<h3 style="color:red">Success !</h3>';
    else if(status == 'fail')
        var message = '<h3 style="color:red">Fail: Template fail!</h3>';
    
    if (msg_error != '')
        var bug = '<h4 style="color:red; font-weight: normal;"><i>' + msg_error + '</i></h4>';
    else
        var bug = '';
        
    $('#import_response').html(message + bug + input_file_name);
	
	// Reset
	$('#store_upload_form').get(0).reset();
	list_sheetnames = null;
	sheet_index = -1;
	
	$('#upload_image_button').hide();
}

function show_loading(){
	$('.mask_loading').show();
	$('.loading_content').hide();
	$('.loading_content').show();
}
function hide_loading(){
	$('.mask_loading').hide();
	$('.loading_content').hide();
}

$(document).ready(function(){		
	$('#upload_image_section').show();		
			
	// Active button OK		
	$('#image_file').change(function(){		
		var file_name = $('#image_file').val();		
		if(file_name){		
			$('#upload_image_button').show();		
		}		
	});		
			
	// Reset form upload		
	$('#store_upload_form').get(0).reset();		
});

</script>
<style type="text/css">

	
	.new_form_title{
		width: 99%;
		float: left;
		display: block;
		height: 35px;
		line-height: 35px;
		color: #0071c1;
		background-color: #7d7d7d;
		padding-left: 1%;
		font-weight: bold;

	}
	
	.new_form_content{
		width: 98%;
		float: left;
		display: block;
		margin-top: 5px;
		padding-left: 1%;
		padding-right: 1%;
	}
	
	.send_button{
		width: 100px; 
		height: 25px; 
		padding: 10px;
		padding-bottom: 10px;
		float: left; 
		text-align: center; 
		background-color: #207fbe; 
		color: #FFF;
		cursor: pointer;
		border-radius: 0px;
		border: 0px;
		display: block;
	}

	.form_center{
		width: 500px;
		margin: 0 auto;
		margin-top: 150px;
	}
</style>

<div class="new_form" style="height: 590px;">
	<div class="new_form_title">IMPORT LIST USER PRO PAGE</div>
	<div class="new_form_content">
		<div class="form_center">
			<form id="store_upload_form" class="upload_form" enctype="multipart/form-data" method="post" action="" style="float:left; width:100%;">
				<div style="float: left; display: block;">
                	<input type="file" id="image_file" name="image_file" onchange="fileSelected();" style="font-size: 12px; width: 300px; height: 23px; border-radius: 0px;" placeholder="Please select file .xlsx"/>
                </div>
                    
				<div style="float: left; display: block;">                
                	<div class="send_button" id="upload_image_button" onclick="startUploading()" style="margin: 10px;">Import</div>
                </div>
			</form>
			<a href="<?=PATH_URL?>DOCUMENTS/import/import_propage.xlsx">Download template (xlsx)</a>

			<div style="float:left;width:100%;" id="fileinfo">
				<div id="filename"></div>
				<div id="filesize"></div>
				<div id="filetype"></div>
				<div id="filedim"></div>
			</div>
			<div style="display:block;">
				<div id="error" style="display:none;">You should select valid image files only!</div>
				<div id="error2" style="display:none;">An error occurred while uploading the file</div>
				<div id="abort" style="display:none;">The upload has been canceled by the user or the browser dropped the connection</div>
				<div id="warnsize" style="display:none;">Your file is very big. We can't accept it. Please select more small file</div>
			</div>
			<div style="float:left;width:100%;display:none;" id="progress_info">
            	<div style="float: left; display: block;">
                	<div id="progress"></div>
                </div>
				<div style="float: left; display: block; margin-left: 10px;">
                	<div id="progress_percent">&nbsp;</div>
                </div>
				<div style="float: left; display: block; margin-left: 10px;">
                	<div id="b_transfered">&nbsp;</div>
                </div>

				<div class="clear_both"></div>
				<div>
					<div id="speed" style="display: none;">&nbsp;</div>
					<div id="remaining" style="display: none;">&nbsp;</div>
					<div class="clear_both"></div>
				</div>
				<div id="upload_response"></div> <!-- NOT USED -->
			</div>
			<div style="float:left;width:100%;margin-top:10px;font-weight:bold;" id="import_response"></div>
			<img id="preview" />

		</div>
		

	</div>
</div>

