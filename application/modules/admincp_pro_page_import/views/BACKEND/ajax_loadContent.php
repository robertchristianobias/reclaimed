<script type="text/javascript">token_value = '<?=$this->security->get_csrf_hash()?>';</script>
<script type="text/javascript">
	function approve(id, email, url_of_page) {
		processing(id, true);
		$.ajax({
		type: 'POST',
		url: '<?=PATH_URL_ADMIN.$module.'/approve/'?>',
		data: { 'id': id, 'email': email, 'url_of_page': url_of_page, 'csrf_token': token_value },
		cache: false,
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.status == 0) {
				// alert(obj.message);
				processing(id, false);
				$('#txt_error').html(obj.message);
				show_perm_denied();
				return false;
			} else {
				$('#area_approve_'+obj.data.id).html('<span class="label label-sm label-warning"> Processing</span>');
				$('#txt_success').html(obj.message);
				show_perm_success();
			}
		},
		error: function(error) {
			processing(id, false);
			console.error(error);
		}
		});
	}

	function processing(id, is_show) {
		if(is_show) { $('#approve_'+id).hide(); $('#processing_'+id).show(); } 
		else { $('#approve_'+id).show(); $('#processing_'+id).hide(); }
	}
</script>

<div class="dataTables_wrapper no-footer">
	<div class="table-scrollable">
		<table class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>							
				<tr role="row">
					<th class="center sorting_disabled" width="35">No.</th>
					<th class="table-checkbox sorting_disabled" width="25"><input type="checkbox" id="selectAllItems" onclick="selectAllItems(<?=count($result)?>)"></th>																	
					<th class="center sorting" width="60" onclick="sort('email')" id="email">Email</th>
					<th class="center sorting" width="60" onclick="sort('name')" id="fristname">Name</th>
					<th class="center sorting" width="60" onclick="sort('url_of_page')" id="listname">Url of pro page</th>
					<th class="center sorting" width="60" onclick="sort('user_type_id')" id="lastname">User type</th>
					<th class="center sorting" width="60" onclick="sort('is_approve')" id="lastname">Approve</th>
					<th class="center sorting" width="60" onclick="sort('edit')" id="lastname">Edit</th>
					<th class="center sorting" width="80" onclick="sort('created')" id="created">Created</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if($result){								
						$i=0;
						foreach($result as $k=>$v){
				?>
				<tr class="item_row<?=$i?> gradeX <?php echo ($k%2==0) ?  'odd' :  'even' ?>" role="row">
					<td class="center"><?=$k+1+$start?></td>
					<td><input type="checkbox" id="item<?=$i?>" onclick="selectItem(<?=$i?>)" value="<?=$v->id?>"></td>										
					<td class="center"><a href="<?=PATH_URL_ADMIN.'admincp_user_genenal/update/'.$v->id?>"><?=$v->email?></a></td>
					<td class="center"><?=$v->name?></td>
					<td class="center"><?=$v->url_of_page?></td>
					<td class="center">
						<?=isset($list_user_type[$v->user_type_id]) ? $list_user_type[$v->user_type_id] : ''?>
					</td>
					<td class="center" id="area_approve_<?=$v->id?>">
						<span id="processing_<?=$v->id?>" class="label label-sm label-danger" style="display:none">Processing</span>
						<?php if($v->is_approve == 0) { ?>
							<button id="approve_<?=$v->id?>" class="btn btn-sm btn-info" onclick="approve('<?=$v->id?>', '<?=$v->email?>', '<?=$v->url_of_page?>')">
								Do approve
							</button>
						<?php } else if($v->is_approve == PROPAGE_STT_WAITING_USER) { ?>
							<span class="label label-sm label-warning"> Processing</span>
						<?php } else if($v->is_approve == PROPAGE_STT_APPROVED) { ?>
							<span class="label label-sm label-success"> Approved</span>
						<?php } ?>
					</td>	
					<?php
					$show_url = '';
					$hide_message = '';
					if(!empty($v->token)){
						if($v->is_approve == PROPAGE_STT_APPROVED){
							$hide_message = 'Approved token';
						} else {
							$show_url = PATH_URL.'en?t='.$v->token;
						}
					} else{
						$hide_message = 'No token';
					}
					if(!empty($show_url)){
					?>
					<td class="center"><a href="<?=$show_url?>" target="_blank">Set Info</a></td>
					<?php
					} else {
					?>
					<td class="center"><?=$hide_message?></td>
					<?php
					}
					?>
					<td class="center"><?=$v->created?></td>
				</tr>
				<?php 
					$i++;	
						}
				}
				else
				{
				?>
				<tr class="gradeX odd" role="row">
					<td class="center no-record" colspan="20">No record</td>
				</tr>
				<?php
				} 
				?>
			</tbody>
		</table>
	</div>

	<?php 
	if($result){ 
	?>
	<div class="row">
		<div class="col-md-5 col-sm-12">
			<?php 
			if(($start+$per_page)<$total){ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$start+$per_page?> of <?=$total?> entries</div>
			<?php 
			}
			else
			{ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$total?> of <?=$total?> entries</div>
			<?php 
			} 
			?>
		</div>

		<div class="col-md-7 col-sm-12">
			<div class="dataTables_paginate paging_bootstrap_full_number" style="margin-top:3px">
				<ul class="pagination" style="visibility: visible;">
					<?=$this->adminpagination->create_links();?>
				</ul>
			</div>
		</div>
	</div>
	<?php 
	} 
	?>
</div>