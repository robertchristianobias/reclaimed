<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_pro_page_import extends MX_Controller {

	private $module = 'admincp_pro_page_import';
	private $table = 'pro_page_import';
	function __construct(){
		parent::__construct();
		$this->load->model($this->module.'_model','model');
		$this->load->model('admincp_modules/admincp_modules_model');
		$this->load->model('admincp_pro_page/admincp_pro_page_model', 'pro_page');
		$this->load->helper('main_helper.php');
		if($this->uri->segment(1)==ADMINCP){
			if($this->uri->segment(2)!='login'){
				if(!$this->session->userdata('userInfo')){
					header('Location: '.PATH_URL_ADMIN.'login');
					exit;
				}
				$get_module = $this->admincp_modules_model->check_modules($this->uri->segment(2));
				$this->session->set_userdata('ID_Module',$get_module[0]->id);
				$this->session->set_userdata('Name_Module',$get_module[0]->name);
			}
			$this->template->set_template('admin');
			$this->template->write('title','Admin Control Panel');
		}
	}
	/*------------------------------------ Admin Control Panel ------------------------------------*/
	public function admincp_index(){
		permission_force_check('r');
		$default_func = 'created';
		$default_sort = 'DESC';
		$list_user_type = $this->model->getListUserType();
		$data = array(
			'module'       =>$this->module,
			'module_name'  =>$this->session->userdata('Name_Module'),
			'default_func' =>$default_func,
			'default_sort' =>$default_sort,
			'list_user_type' => $list_user_type
		);
		$this->template->write_view('content','BACKEND/index',$data);
		$this->template->render();
	}
	
	
	public function admincp_update($id=0){
		
		// redirect(PATH_URL_ADMIN.$this->module);

		if($id==0){
			permission_force_check('w');
		}else{
			permission_force_check('r');
		}
		$result[0] = array();
		if($id!=0){
			$result = $this->model->getDetailManagement($id);
		}
		$data = array(
			'result'=>$result[0],
			'module'=>$this->module,
			'id'=>$id
		);
		$this->template->write_view('content','BACKEND/ajax_editContent',$data);
		$this->template->render();
	}

	public function admincp_save(){
		// permission_force_check('w');
		// if($perm=='permission-denied'){
			// print $perm.'.'.$this->security->get_csrf_hash();
			// exit;
		// }
		if($_POST){
			//Upload Image
			$fileName = array('image'=>'');
			if($_FILES){
				foreach($fileName as $k=>$v){
					if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
						$typeFileImage = strtolower(mb_substr($_FILES['fileAdmincp']['type'][$k],0,5));
						if($typeFileImage == 'image'){
							$tmp_name[$k] = $_FILES['fileAdmincp']["tmp_name"][$k];
							$file_name[$k] = $_FILES['fileAdmincp']['name'][$k];
							$ext = strtolower(mb_substr($file_name[$k], -4, 4));
							if($ext=='jpeg'){
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-5))).'.jpg';
							}else{
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-4))).$ext;
							}
						}else{
							print 'error-image.'.$this->security->get_csrf_hash();
							exit;
						}
					}
				}
			}
			//End Upload Image

			if($this->model->saveManagement($fileName)){
				//Upload Image
				if($_FILES){
					if($_FILES){
						$upload_path = BASEFOLDER.DIR_UPLOAD_NEWS;
						check_dir_upload($upload_path);
						foreach($fileName as $k=>$v){
							if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
								move_uploaded_file($tmp_name[$k], $upload_path.$fileName[$k]);
							}
						}
					}
				}
				//End Upload Image
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_delete(){
		permission_force_check('d');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$result = $this->model->getDetailManagement($id);
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			$this->db->where('id',$id);
			if($this->db->delete($this->table)){
				//Xóa hình khi Delete
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result[0]->image);
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_import(){
		$default_func = 'created';
		$default_sort = 'DESC';
		$data = array(
			'module'=>$this->module,
			'default_func'=>$default_func,
			'default_sort'=>$default_sort,
			'is_normal_template'=>false,
		);
		$this->template->write_view('content','BACKEND/import_pro_page',$data);
		$this->template->render();
	}
	
	public function admincp_ajaxLoadContent(){
		$this->load->library('AdminPagination');
		$config['total_rows'] = $this->model->getsearchContent();
		$config['per_page'] = (int)$this->input->post('per_page');
		$config['start'] = (int)$this->input->post('start');
		$config['num_links'] = 3;
		$config['func_ajax'] = 'searchContent';
		$this->adminpagination->initialize($config);

		$result = $this->model->getsearchContent($config['per_page'],$this->input->post('start'));
		// last_query(1);

		$list_user_type = $this->model->getListUserType();
		$data = array(
			'result'=>$result,
			'per_page'=>$this->input->post('per_page'),
			'start'=>$this->input->post('start'),
			'module'=>$this->module,
			'total'=>$config['total_rows'],
			'list_user_type' => $list_user_type
		);
		$this->session->set_userdata('start',$this->input->post('start'));
		$this->load->view('BACKEND/ajax_loadContent',$data);
	}
	
	public function admincp_ajaxUpdateStatus(){
		permission_force_check('a');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}else{
			
			modules::run('admincp/saveLog',$this->module,$this->input->post('id'),'update',$this->input->post('status'),$status);
			$this->db->where('id', $this->input->post('id'));
			$this->db->update($this->table, $data);
		}
		
		$update = array(
			'status'=>$status,
			'id'=>$this->input->post('id'),
			'module'=>$this->module
		);
		$this->load->view('BACKEND/ajax_updateStatus',$update);
	}
	
	public function admincp_ajaxGetImageUpdate($id){
		$result = $this->model->getDetailManagement($id);
		print resizeImage(PATH_URL.DIR_UPLOAD_NEWS.$result[0]->image,250);exit;
	}
	/*------------------------------------ End Admin Control Panel --------------------------------*/
	
	/*------------------------------------ import Email --------------------------------*/
	
	public function admincp_upload(){
		$log_arr = array(
			'location' => __FILE__ ,
			'function' => 'admincp_upload',
		);
		debug_log_from_config($log_arr);
		try{
			if(!empty($_FILES)){
				$upload_dir = BASEFOLDER.IMPORT_PROPAGE;
				$upload_config['allowed_types'] = '*';
				$upload_config['encrypt_name'] = true;
				$upload_config['upload_path'] = $upload_dir;
				$this->load->library('upload', $upload_config);
				
				// DEBUG
				$log_arr = array(
					'location' => __FILE__ ,
					'function' => 'admincp_upload',
					'upload_config' => !empty($upload_config) ? $upload_config : '',
				);
				debug_log_from_config($log_arr);
				
				if($this->upload->do_upload("image_file")){
					$uploaded_data = $this->upload->data();
					$file_name = $uploaded_data['file_name'];
					$import_file_path = $upload_dir.$file_name;
					$import_data['type'] = 'pro_page';
					$import_data['file_name'] = $file_name;
					$import_data['created'] = getNow();
					$import_data['user_id'] = (int)$this->session->userdata('userId');
					if($this->model->insert('import',$import_data)){
						$import_id = $this->db->insert_id();
						$data['file_name'] = $file_name;
						$data['import_id'] = $import_id;
						echo json_encode($data);
					}
				} else {
					$errors = $this->upload->display_errors();
					pr($errors,1);
				}
			}
		} catch (Exception $ex) {
			$exception_error = $ex->getMessage();
		}
		
		// DEBUG
		$log_arr = array(
			'location' => __FILE__ ,
			'function' => 'admincp_upload',
			'upload_config' => !empty($upload_config) ? $upload_config : '',
			'data' => !empty($data) ? $data : '',
			// 'exception_error' => !empty($exception_error) ? $exception_error : '',
		);
		debug_log_from_config($log_arr);
	}

	function admincp_list_sheetnames(){
		$worksheetNames = array();
		$file_name = $this->input->post('file_name');
		if(!empty($file_name)){
			$file_path = BASEFOLDER.IMPORT_PROPAGE.$file_name;
			if(file_exists($file_path)){
				$objReader = phpexcel_get_obj_reader();
				if(!empty($objReader)){
					$worksheetNames = $objReader->listWorksheetNames($file_path);
					if(!empty($worksheetNames[0])){
						$worksheetNames = array(
							$worksheetNames[0]
						);
					}
				}
			}
		}
		echo json_encode($worksheetNames);
	}

	function admincp_import_item_by_sheetname(){
		
		$is_debug = false;
		// BEGIN: DEBUG
		if($is_debug){
			$start_mem = memory_get_usage();
			$start_mem = (int)($start_mem/1000);
			$start_mem = number_format($start_mem, 0, ',', '.');
			echo '$start_mem = '.$start_mem.'KB<br/>';
		}
		// END: DEBUG
        
		$file_name = $this->input->post('file_name'); 
		$import_id = (int)$this->input->post('import_id');
		$sheet_name = $this->input->post('sheet_name');
		$result = '';
		if(!empty($file_name) && !empty($sheet_name)){
			$file_path = BASEFOLDER.IMPORT_PROPAGE.$file_name;
			if(file_exists($file_path)){
				$objReader = phpexcel_get_obj_reader();
				$objReader->setLoadSheetsOnly(array($sheet_name));
            }
			
			// FOR CHECKING WHETHER THE TEMPLATE OF IMPORT FILE IS CORRECT!
			$is_correct_import_template = true;
			
			// LOAD EXCEL FILE: HEAVY TASK!
			$objPHPExcel = $objReader->load($file_path);
			$objSheet = $objPHPExcel->getActiveSheet();
			$highestRow = $objSheet->getHighestRow();
			$highestColumn = $objSheet->getHighestColumn();
			
			$list_user_type = $this->model->getListNameUserType();

			if($is_correct_import_template){
				$data = array();
				$count_empty_email = 0;
				for ($row = 1; $row <= $highestRow; $row++){
					$rowData_arr = $objSheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
													NULL,
													TRUE,
													FALSE);
					$rangeToArray_index = 0;
					if(!empty($rowData_arr[$rangeToArray_index])){
						$rowData = $rowData_arr[$rangeToArray_index];
						// Check Header row
						if($row == 1){
							$arr = array();
							if( ! (import_check_column_header('A', 'Email', $rowData) && 
								import_check_column_header('B', 'Name', $rowData) &&
								import_check_column_header('C', 'Url of pages', $rowData)&&
								import_check_column_header('D', 'User type', $rowData)&&
								import_check_column_header('E', 'Genre', $rowData)&&
                                import_check_column_header('F', '', $rowData)) ) {	
								$is_correct_import_template = false;
								break;
							}
						} 
						else { 
							$col = 0; //Start at col A
							$item = array();
							
                            $email = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $name = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $url_of_page = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $user_type_name = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $user_type_id = isset($list_user_type [$user_type_name]) ? $list_user_type [$user_type_name] : 0;
							$genre = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
							if(!empty($email) && !empty($name)&& !empty($url_of_page)){
									
                                    $item['email'] = $email;
									$item['name'] = $name;
									$item['url_of_page'] = $url_of_page;
									$item['import_id'] = $import_id;
									$item['status'] = 1;	
									$item['created'] = getNow();
									$item['user_type_name'] = $user_type_name;
									$item['user_type_id'] = $user_type_id;
									$item['genre'] = $genre;
									// $item['token']= generate_random_string(50);
									$data[] = $item;
							}
							else {
								$count_empty_email++;
								if ($count_empty_email > 5)
									break; 
							}
						}
					}
				}
				if ($is_correct_import_template){
					//do insert data
					if ( ! empty($data)) {
						// pr($data, 1);
						$this->model->insertImportData($data);
					}
					// pr($data, 1);
					$result = 'ok';
				} 
				else {
					$result = 'wrong_template';
				}
			}
			echo $result;
		}
	}

	function admincp_approve() {
		$result = array();
		$status = 0;
		$message = 'error';
		$data = array();
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$url_of_page = $this->input->post('url_of_page');
		$user_id = NULL;

		if (empty($email)) {
			$message = 'email empty';
			$this->set_result($status, $message);
		}
		if (empty($url_of_page)) {
			$message = 'url empty';
			$this->set_result($status, $message);
		}
		//verify request
		$data_check = $this->model->verifyId($id, $email, $url_of_page);
		if (empty($data_check)) {
			$message = 'data is invalid';
			$this->set_result($status, $message);
		}
		//get user id from email
		// $user_id = $this->pro_page->getUserIdByEmail($email);
		// //check email exists
		// if (empty($user_id)) {
		// 	$message = 'email not exists';
		// 	$this->set_result($status, $message);
		// }
		//check url exists
		if ($this->pro_page->checkProPageExists($url_of_page)) {
			$message = 'url exists';
			$this->set_result($status, $message);
		}

		$user_type_id = isset($data_check ['user_type_id']) ? $data_check ['user_type_id'] : '';
		$approve_status = PROPAGE_STT_WAITING_USER;//PROPAGE_STT_APPROVED; //PROPAGE_STT_WAITING_USER
		//do insert approve
		$data_insert = array();
		$data_insert ['email'] = $email; 
		$data_insert ['user_id'] = $user_id; 
		$data_insert ['url_of_page'] = $url_of_page; 
		$data_insert ['user_type_id'] = $user_type_id; 
		$data_insert ['approve_status'] = $approve_status; 
		$data_insert ['genre'] = $data_check['genre']; 
		$data_approve = $this->pro_page->insertApprovedProPage($data_insert);
		// $data_approve = $this->pro_page->insertApproveProPage($email, $user_id, $url_of_page, $user_type_id, $approve_status);
		if (empty($data_approve)) {
			$message = 'Can not approve';
			$this->set_result($status, $message);
		}
		//send email
		$send_email_result = $this->send_email_approve($email, $data_approve ['token'],$url_of_page);
		$approve_id = $data_approve ['id'];
		
		$this->model->updateDataAfterApproved($id, $approve_id, $approve_status, $send_email_result, $data_approve ['token']);
		
		$message = 'approve success';
		$status = 1;
		$data ['id'] = $id;
		// $data ['user_id'] = $user_id;
		$data ['email'] = $email;
		$data ['url_of_page'] = $url_of_page;
		$data ['send_email_approve'] = ($send_email_result) ? 'success' : 'fail';
		$this->set_result($status, $message, $data);
	}

	function send_email_approve($email, $token, $url_of_page) {
		$url_setup = PATH_URL.'en/?t='.$token;
		$subject = 'Active pro page - REDM - ' . date('Y-m-d H:i:s');
		$body = "<center>Link to your pro pages is: " . $url_of_page .
		" </center></br><center>But first you have to set up the basic information here: ".$url_setup."</center>";
		$body .= "<br><center>{$token}</center>";
		$send_email_approve = ses_send_mail($subject, $body, $email);
		
		return ($send_email_approve) ? TRUE : FALSE;
	}

	function set_result($status, $message, $data = array()) {
		$result = array();
		$result ['status'] = $status;
		$result ['message'] = $message;
		$result ['data']= $data;
		echo json_encode($result); exit;

	}
}