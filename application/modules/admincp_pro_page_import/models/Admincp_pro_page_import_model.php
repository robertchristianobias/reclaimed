<?php
class Admincp_pro_page_import_model extends MY_Model {
	private $module = 'admincp_pro_page_import';
	private $table = 'pro_page_import';
	
	function getsearchContent($limit = 0, $page = -1){
		$result = false;
		$main_table = $this->table;
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*");
        }
		
		$this->db->from($main_table);
		$this->db->join('pro_page',"{$main_table}.email = pro_page.email",'left');
		
		/*Begin: Condition*/
		// Begin - Search condition
		$content = $this->input->post('content');
		if(!empty($content)){
			$search_condition_arr = array(
				"{$main_table}.name LIKE '%{$content}%'",
				"{$main_table}.from_email LIKE '%{$content}%'",
			);
			$search_condition = implode($search_condition_arr, ' OR ');
			$search_condition = "( {$search_condition} )";
			$this->db->where($search_condition);
		}
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');
		if(!empty($dateFrom) || !empty($dateTo)){
			$datetimeFrom = date('Y-m-d 00:00:00',strtotime($dateFrom));
			$datetimeTo = date('Y-m-d 23:59:59',strtotime($dateTo));
			if(empty($dateFrom)){
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			} else if(empty($dateTo)){
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
			} else {
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			}
		}
		// End - Search condition
		
		// Begin - Filter condition
		$filter1 = (int)$this->input->post('filter1');
		if(!empty($filter1)){
			$this->db->where("{$main_table}.listname_id = '{$filter1}'");
		}
		// End - Filter condition
		
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
	}
	
	function getDetailManagement($id){
		$this->db->select('*');
		$this->db->where('admin_nqt_mailing_list_member.id',$id);
		$this->db->from('admin_nqt_mailing_list_member');
		$this->db->join('admin_nqt_mailing_list','admin_nqt_mailing_list.id = admin_nqt_mailing_list_member.listname_id');
		
		$query = $this->db->get();

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	function saveManagement($fileName=''){
		
		$this->load->helper('main_helper');
		//default data
		// $status = ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$created = $_updated = date('Y-m-d H:i:s',time());
        $name = trim($this->input->post('name'));
        $email = $this->input->post('email');
        $url_pro_page = $this->input->post('url_pro_page');
		$user_type = $this->input->post('user_type');
        $genre = $this->input->post('genre');
		
		$list_user_type = $this->model->getListNameUserType();
		
		$data = array(
			'name'=> $name,
			'email'=>$email,
			'url_of_page' =>$url_pro_page,			
			'user_type_name'=> $user_type,
			'user_type_id'=> isset($list_user_type [$user_type]) ? $list_user_type [$user_type] : 0,
			'genre' =>$genre,
			'import_id' => '', 
			'created' =>$created,
		);
		//DO INSERT DATA
		if(empty($update_id)) // ADD NEW
		{
			if($this->db->insert(PREFIX.$this->table,$data))
			{
				$insert_id = $this->db->insert_id();				
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}
		else // MODIFY
		{
			$result = $this->getDetailManagement($update_id);
			unset($data['created']);
			
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data))
			{
				return true;
			}
			return false;
		}
	}
		
	function checkData($email,$id=0, $listname_id){
		$this->db->select('id');
		$this->db->where('email',$email);
		$this->db->where('listname_id',$listname_id);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get($this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkAccess_token($access_token){
		$this->db->select('id');
		$this->db->where('access_token',$access_token);
		$this->db->limit(1);
		$query = $this->db->get($this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results($this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		//$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	public function insertImportData($data) {
		$this->db->insert_batch($this->table, $data);
	}

	public function verifyId($id, $email, $url_of_page) {
		$this->db->select('*');
		$this->db->where('id', intval($id));
		$this->db->where('email', $email);
		$this->db->where('url_of_page', $url_of_page);
		$this->db->limit(1);
		$query = $this->db->get($this->table);
		if ( ! empty($query->row_array())) {
			return $query->row_array();
		}
		return FALSE;
	}

	public function findIdByEmailAndUrlOfPage($email, $url_of_page) {
		$this->db->select('id');
		$this->db->where('email', $email);
		$this->db->where('url_of_page', $url_of_page);
		$this->db->limit(1);
		$query = $this->db->get($this->table);
		if ( ! empty($query->row('id'))) {
			return $query->row('id');
		}
		return FALSE;
	}

	public function updateSendEmailApproveSuccess($id) {
		if (empty($id)) {
			return FALSE;
		}
		$data_update = array('is_send_email_approve', 1);
		$this->db->where('id', intval($id));
		$this->db->update($this->table, $data_update);
	}

	public function updateDataAfterApproved($id, $approve_id, $approve_status, $send_email_status, $token) {
		if (empty($id)) {
			return FALSE;
		}
		$data_update = array();
		$data_update ['approve_id'] = intval($approve_id);
		$data_update ['is_approve'] = intval($approve_status);
		$data_update ['is_send_email_approve'] = intval($send_email_status);
		$data_update ['token'] = $token;

		$this->db->where('id', intval($id));
		$this->db->update($this->table, $data_update);
	}

	public function updateUserApproveProPage($approve_id) {
		$data = array();
		$data ['is_approve'] = PROPAGE_STT_APPROVED;
		$this->db->where('approve_id', intval($approve_id));
		$this->db->update($this->table, $data);
	}

	public function getListUserType() {
		$result = array();
		$this->db->select('*');
		$query = $this->db->get('user_type');
		if ( ! empty($query->result_array())) {
			$list = $query->result_array();
			foreach ($list as $key => $item) {
				$result [$item ['id']] = $item ['type'];
			}
		}
		return $result;
	}

	public function getListNameUserType() {
		$result = array();
		$this->db->select('*');
		$query = $this->db->get('user_type');
		if ( ! empty($query->result_array())) {
			$list = $query->result_array();
			foreach ($list as $key => $item) {
				$result [$item ['type']] = $item ['id'];
			}
		}
		return $result;
	}
}