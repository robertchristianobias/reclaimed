<script language="JavaScript" type="text/javascript" src="<?=PATH_URL.'asset/js/sprinkle.js'?>"></script>
<script language="JavaScript" type="text/javascript" src="<?=PATH_URL.'asset/js/jquery-1.2.6.min.js'?>"></script>
<script language="JavaScript" type="text/javascript" src="<?=PATH_URL.'asset/js/jquery-ui-personalized-1.5.2.packed.js'?>"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
});

function confirm(){
	var email = $('#emailAccessToken').val();
	var access_token = $('#hiddenAccessToken').val();
	if( email && access_token)
	{ 
		$.ajax
		(
			{
				type : 'POST',
				url: '<?=PATH_URL_ADMIN?>admincp_campaigns/ajaxCheckmailUnsubscribe',
				data:
				{
					'email': email,
					'access_token': access_token
				},
				success: function(data)
				{
					myObj = JSON.parse(data);
					if(myObj=='success'){
						alert("Success!");					
					}
				}
			}
		) 
	}
}

</script>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN FORM-->
				<form id="frmManagement" action="<?=PATH_URL_ADMIN.'update_profile/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$access_token?>" name="hiddenAccessToken" id="hiddenAccessToken" />
						<div class="form-group last">
							<label class="control-label col-md-3">Email<span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<input value="<?=$email->email?>" type="text" name="emailAccessToken" id="emailAccessToken" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button onclick="confirm()" type="button" class="btn green"><i class="fa fa-pencil"></i> Confirm</button>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->