<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovaeditor.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovamanager.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places"></script>

<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">
<?php
	if(isset($this->lang->languages)){
		$all_lang = $this->lang->languages;
	}else{
		$all_lang = array(
			'' => ''
		);
	}
?>
<script type="text/javascript">
$(document).ready( function(){
	<?php foreach($all_lang as $key=>$val){ ?>
	$("#title<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp").slugIt({
		events: 'keyup blur',
		output: '#slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp',
		map: {'!':'-'},
		space: '-'
	});
	//FOR WYSIWYG content
	// $('#content<?php echo ($key!='') ?  '_'.$key : '' ?>Admincp').liveEdit({
		// height: 350,
		// css: ['<?=PATH_URL?>assets/editor/bootstrap/css/bootstrap.min.css', '<?=PATH_URL?>assets/editor/bootstrap/bootstrap_extend.css'] /* Apply bootstrap css into the editing area */,
		// fileBrowser: '<?=PATH_URL?>assets/editor/assetmanager/asset.php',
		// returnKeyMode: 3,
		// groups: [
				// ["group1", "", ["Bold", "Italic", "Underline", "ForeColor"]],
				// ["group2", "", ["Bullets", "Numbering", "Indent", "Outdent"]],
				// ["group3", "", ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyFull"]],
				// ["group4", "", ["Paragraph", "FontSize", "FontDialog", "TextDialog"]],
				// ["group5", "", ["LinkDialog", "ImageDialog", "TableDialog"]],
				// ["group6", "", ["Undo", "Redo", "FullScreen", "SourceDialog"]]
				// ] /* Toolbar configuration */
	// });
	// $('#content<?php echo ($key!='') ? '_'.$key : '' ?>Admincp').data('liveEdit').startedit();
	//End FOR WYSIWYG content
	<?php } ?>
});

function save(){
	//console.log('save');
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}
function sendmail(){
	var subject = $('#emailSubjectAdmincp').val();
	var from_name = $('#fromNameAdmincp').val();
	// var country_id = $('#country').val();
	var campaigns = $('#campaignsAdmincp').val();
	var template_id = $('#template_idAdmincp').val();
	var listname_id = $('#listname_idAdmincp').val();
	var dateSetting = $('#date').val();
	var is_send_mail = 1;
	if(dateSetting == ''){
		var date = '';
		var time = '';
	}
	else{
		var date = $('#date').val();
		var time = $('#time').val();
	}
	var sentMail = 0;
	if(subject && from_name)
	{ 
		var txt;
		var r = confirm("You want to sendmail?");
		if (r == true) 
		{
			var sentMail = 1;
			$.ajax
			(
				{
					type : 'POST',
					url: '<?=PATH_URL_ADMIN?>admincp_campaigns/ajaxsendmail',
					data:
					{
						'subject': subject, 
						'from_name': from_name,
						// 'country_id': country_id,
						'campaigns': campaigns,
						'template_id': template_id,
						'listname_id': listname_id,
						'date': date,
						'time': time,
						'sentMail': sentMail,
						'is_send_mail': is_send_mail
					},
					success: function(data)
					{
						myObj = JSON.parse(data);
						var total = myObj.total;
						var bonus = myObj.bounced;
						if(myObj.success==true){
							alert('Total Email = ' + total + '\n' + 'Bonus Email = ' + bonus);
							setTimeout(function(){window.location.href="<?=PATH_URL_ADMIN?>admincp_contact"},500);							
						}else{
							alert("fail!");
						}
					}
				}
			)
		} 
		else{
			var sentMail = 0;
			$.ajax
			(
				{
					type : 'POST',
					url: '<?=PATH_URL_ADMIN?>admincp_campaigns/ajaxsendmail',
					data:
					{
						
						'subject': subject, 
						'from_name': from_name,
						// 'country_id': country_id,
						'campaigns': campaigns,
						'template_id': template_id,
						'listname_id': listname_id,
						'date': date,
						'time': time,
						'sentMail': sentMail,
						'is_send_mail': is_send_mail
					},
				}
			)
		}
	}
}

function showRequest(formData, jqForm, options) {
	var form = jqForm[0];
	<?php foreach($all_lang as $key=>$val){ ?>
	if (
		form.campaignsAdmincp.value == ''
	) {
		$('#txt_error').html('Please enter information.');
		show_perm_denied();
		return false;
	}
	<?php } ?>
//
}
function showRequestSendmail(formData, jqForm, options) {
	var form = jqForm[0];
	<?php foreach($all_lang as $key=>$val){ ?>
	if (
		responseText[0]=='error-email'
	) {
		$('#txt_error').html('No mail selected.');
		show_perm_denied();
		return false;
	}
	<?php } ?>
//
}

function showResponse(responseText, statusText, xhr, $form) {
	responseText = responseText.split(".");
	token_value  = responseText[1];
	$('#csrf_token').val(token_value);
	if(responseText[0]=='success'){
		<?php if($id==0){ ?>
		location.href=root+module+"/#/save";
		<?php }else{ ?>
		if($('.form-upload').val() != ''){
			$.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
				var res = data.split("src=");
				$('.fileinput-filename').html('');
				$('.fileinput').removeClass('fileinput-exists');
				$('.fileinput').addClass('fileinput-new');
			});
		}
		show_perm_success();
		<?php } ?>
	}
	
	if(responseText[0]=='error-image'){
		$('#txt_error').html('Only upload image.');
		show_perm_denied();
		return false;
	}
	
	if(responseText[0]=='error-duplicate-email'){
		$('#txt_error').html('Duplicate email in current list.');
		show_perm_denied();
		return false;
	}

	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
	if(responseText[0]=='error-email'){
		$('#txt_error').html('No mail selected.');
		show_perm_denied();
		return false;
	}
}
$(document).ready(function() {							    
							$('#time').timepicker(); 
							});
</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
		<li><a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a><i class="fa fa-angle-right"></i></li>
		<li><?php echo ($this->uri->segment(4)=='') ?  'Add new' :  'Edit' ?></li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Form Information
				</div>
			</div>
			
			<div class="portlet-body form">
				<div class="form-body notification" style="display:none">
					<div class="alert alert-success" style="display:none">
						<strong>Success!</strong> The page has been saved.
					</div>
					
					<div class="alert alert-danger" style="display:none">
						<strong>Error!</strong> <span id="txt_error"></span>
					</div>
				</div>
				
				<!-- BEGIN FORM-->
				<form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-9">
								<div class="checkbox-list">
									<label class="checkbox-inline">
										<div class="checkbox"><span><input <?=( ! empty($result->status) && ($result->status == 1) ) ? 'checked' : 'checked'?> type="checkbox" name="statusAdmincp"></span></div>
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Campaigns <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="campaignsAdmincp" 
									id="campaignsAdmincp" 
									value="<?php echo(isset($result->campaigns) ? $result->campaigns : '') ?>"
									/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Mailing list <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="listname_idAdmincp" id="listname_idAdmincp">
									<?php
									if(!empty($mailing_list)){
										foreach($mailing_list as $list){
									?>
									<option 
										<?php echo ( isset($result->listname_id) && ($result->listname_id == $list->id) ) ? 'selected' : ''; ?> 
										value="<?=$list->id?>">
											<?=$list->name?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Template<span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="template_idAdmincp" id="template_idAdmincp">
									<?php
									if(!empty($mailing_list_template)){
										foreach($mailing_list_template as $list_template){
									?>
									<option 
										<?php echo ( isset($result->template_id) && ($result->template_id == $list_template->id) ) ? 'selected' : ''; ?> 
										value="<?=$list_template->id?>">
											<?=$list_template->header?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<?php 
						if($id!=0){
							if(!empty($list_campaigns_sent)&&$checkExitCampaignsSent==1){
								$i = 0;
								foreach($list_campaigns_sent as $list_campaigns_sentmail){
									if(($result->id) == ($list_campaigns_sentmail->campaigns_id)){
						?>
						<div class="form-group">
							<label class="control-label col-md-3">
								Email subject <span class="required" aria-required="true"></span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="emailSubjectAdmincp" 
									id="emailSubjectAdmincp" 
									value="<?php echo (($result->id) == ($list_campaigns_sentmail->campaigns_id))?$list_campaigns_sentmail->subject:''?>"
								/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								From Name <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="fromNameAdmincp" 
									id="fromNameAdmincp" 
									value="<?php echo (($result->id) == ($list_campaigns_sentmail->campaigns_id)) ? $list_campaigns_sentmail->from_name:''?>"
									/>
							</div>
						</div>
						
						<!--<div class="form-group">
							<label class="control-label col-md-3">Country to send</label>
							<div class="col-md-9">
								<select name="country" id="country" class="form-control">
									<?php 
										if($list_country){
											foreach ($list_country as $country) {
									?>
									<option 
										<?php echo (($result->id) == ($list_campaigns_sentmail->campaigns_id))?(( isset($result->country_id) && ($result->country_id == $country->id) ) ? 'selected' : ''):''?> 
										value="<?=$country->id?>">
											<?=$country->name?>
									</option>
									<?php	
											}	
										}
									?>
								</select>
							</div>
						</div>-->
											
						<div class="form-group"> <h4 class = "center">Send Time</h4>
								<label class="control-label col-md-3">Date</label>
								<div class="col-md-3">
									<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
										<input value="<?php echo (($result->id) == ($list_campaigns_sentmail->campaigns_id)) ? substr($list_campaigns_sentmail->timeToSendMail,0,10) : '' ?>" type="date" name="date" id="date" class="form-control"/>
									</div>
								</div>
								<label class="control-label col-md-1">Time</label>
								<div class="col-md-3">						
									<div>
										<input value="<?php echo (($result->id) == ($list_campaigns_sentmail->campaigns_id)) ? substr($list_campaigns_sentmail->timeToSendMail,-8) : '' ?>" type="text" name="time" id ='time'  class="form-control center"/>
									</div>
								</div>
							</div>
						<?php
									}
									$i++;
								} 
							}
							else{
						?>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Email subject <span class="required" aria-required="true"></span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="emailSubjectAdmincp" 
									id="emailSubjectAdmincp" 
									value=""
								/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								From Name <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="fromNameAdmincp" 
									id="fromNameAdmincp" 
									value=""
									/>
							</div>
						</div>
						
						<!--<div class="form-group">
							<label class="control-label col-md-3">Country to send</label>
							<div class="col-md-9">
								<select name="country" id="country" class="form-control">
									<?php 
										if($list_country){
											foreach ($list_country as $country) {
									?>
									<option value = "choose" ></option>
									<option 
										<?php echo (( isset($result->country_id) && ($result->country_id == $country->id) ) ? 'selected' : '')?> 
										value="<?=$country->id?>">
											<?=$country->name?>
									</option>
									<?php	
											}	
										}
									?>
								</select>
							</div>
						</div>-->
											
						<div class="form-group"> <h4 class = "center">Send Time</h4>
								<label class="control-label col-md-3">Date</label>
								<div class="col-md-3">
									<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
										<input value="" type="date" name="date" id="date" class="form-control"/>
									</div>
								</div>
								<label class="control-label col-md-1">Time</label>
								<div class="col-md-3">						
									<div>
										<input value="" type="text" name="time" id ='time'  class="form-control center"/>
									</div>
								</div>
							</div>
							
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button onclick="sendmail()" type="button" class="btn blue"><i class="fa fa-pencil"></i> Send Email</button>
											<a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
									</div>
								</div>
							</div>
						<?php
							}
						}
						?>
				
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
							<?php
							if($id==0){
							?>
								<button onclick="save()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save</button>
								<a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
							<?php
							}
							?>
							</div>
						</div>
					</div>

				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->