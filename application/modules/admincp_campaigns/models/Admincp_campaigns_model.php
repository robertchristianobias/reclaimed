<?php
class Admincp_campaigns_model extends CI_Model {
	private $module = 'admincp_campaigns';
	private $table = 'admin_nqt_campaigns';

	function get_mailing_list(){
		$this->db->select('*');
		$this->db->from('admin_nqt_mailing_list');
		//$this->db->join('admin_nqt_mailing_list_member','admin_nqt_mailing_list_member.listname_id = admin_nqt_mailing_list.id');
		$this->db->order_by('name','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_mailing_template(){
		$this->db->select('admin_nqt_mailing_template.*');
		$this->db->from('admin_nqt_mailing_template');
		$this->db->order_by('header','asc');
		$query = $this->db->get();
		return $query->result();
	}
	function get_campaigns_sent(){
		$this->db->select('*');
		$this->db->from('admin_nqt_campaigns_sent');
		$this->db->order_by('subject','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	function getcampaigns($name){
		$this->db->select('id');
		$this->db->LIKE('campaigns',$name);
		$this->db->limit(1);
		$query = $this->db->get('admin_nqt_campaigns');

		if($query->row()){
			return $query->row();
		}else{
			return $query = '';
		}
	}
	
	function get_campaigns(){
		$this->db->select('admin_nqt_campaigns.*');
		$this->db->from('admin_nqt_campaigns');
		$this->db->order_by('campaigns','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_country(){
		$this->db->select('*');
		$query = $this->db->get('countries');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	public function nameCountry($country_id){
		$this->db->select('name');
		$this->db->where('id', $country_id);
		$query = $this->db->get('countries');

		if($query->row()){
			return $query->row();
		}else{
			return false;
		}
	}
	
	function getsearchContent($limit = 0, $page = -1){
		$result = false;
		$main_table = $this->table;
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*, admin_nqt_mailing_list.name AS mailing_list_name");
           $this->db->select("{$main_table}.*, admin_nqt_mailing_template.header AS mailing_list_template");
        }
		
		$this->db->from($main_table);
		$this->db->join('admin_nqt_mailing_list',"{$main_table}.listname_id = admin_nqt_mailing_list.id", 'left'); // Force LEFT JOIN to keep data of main table
		$this->db->join('admin_nqt_mailing_template',"{$main_table}.template_id = admin_nqt_mailing_template.id", 'left'); // Force LEFT JOIN to keep data of main table
		
		$content = $this->input->post('content');
		if(!empty($content)){
			$search_condition_arr = array(
				"{$main_table}.campaigns LIKE '%{$content}%'",
			);
			$search_condition = implode($search_condition_arr, ' OR ');
			$search_condition = "( {$search_condition} )";
			$this->db->where($search_condition);
		}
		$this->db->where('admin_nqt_campaigns.is_delete',0);
		
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');
		if(!empty($dateFrom) || !empty($dateTo)){
			$datetimeFrom = date('Y-m-d 00:00:00',strtotime($dateFrom));
			$datetimeTo = date('Y-m-d 23:59:59',strtotime($dateTo));
			if(empty($dateFrom)){
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			} else if(empty($dateTo)){
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
			} else {
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			}
		}
		// End - Search condition
		
		// Begin - Filter condition
		$filter1 = (int)$this->input->post('filter1');
		if(!empty($filter1)){
			$this->db->where("{$main_table}.listname_id = '{$filter1}'");
		}
		// End - Filter condition
		
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
	}
	
	function getDetailManagement($id){
		$this->db->select('admin_nqt_campaigns.*');
		$this->db->where('admin_nqt_campaigns.id',$id);
		$this->db->from('admin_nqt_campaigns');
		$this->db->join('admin_nqt_mailing_list','admin_nqt_mailing_list.id = admin_nqt_campaigns.listname_id');
		$this->db->join('admin_nqt_mailing_template','admin_nqt_mailing_template.id = admin_nqt_campaigns.template_id');
		
		$query = $this->db->get();

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
		
	}
	
	function saveManagement($fileName=''){
		//default data
		$status = ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$created = $_updated = date('Y-m-d H:i:s',time());
        $campaigns = $this->input->post('campaignsAdmincp');
        $listname_id = (int)$this->input->post('listname_idAdmincp');
        $template_id = (int)$this->input->post('template_idAdmincp');
		$update_id = (int)$this->input->post('hiddenIdAdmincp');
		
		//Kiểm tra đã tồn tại chưa?
			/*$checkData = $this->checkData($email,$update_id);
			if($checkData)
			{
				print 'error-duplicate-email';
				exit;
			}*/
		
		$data = array(
			'status'=> $status,
			'campaigns'=>$campaigns,
			'listname_id'=> $listname_id,
			'template_id'=> $template_id,
			'created' =>$created,
		);
		//DO INSERT DATA
		if(empty($update_id)) // ADD NEW
		{
			if($this->db->insert(PREFIX.$this->table,$data))
			{
				$insert_id = $this->db->insert_id();				
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}
		else // MODIFY
		{
			$result = $this->getDetailManagement($update_id);
			unset($data['created']);
			
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data))
			{
				return true;
			}
			return false;
		}
	}
		
	function checkData($email,$id=0){
		$this->db->select('id');
		$this->db->where('email',$email);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get($this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkExitCampaignsSent($campaigns_id){
		$this->db->select('id');
		$this->db->where('campaigns_id',$campaigns_id);
		$this->db->limit(1);
		$query = $this->db->get('admin_nqt_campaigns_sent');
		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getDataCampaigns(){
		$this->db->select('*');
		$query = $this->db->get('admin_nqt_campaigns_sent');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results($this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		//$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get($this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getDataMember($listname_id ){
		$this->db->select('admin_nqt_mailing_list_member.*');
		$this->db->from('admin_nqt_mailing_list_member');
		$this->db->leftjoin('admin_nqt_mailing_list_member','admin_nqt_mailing_list_member.listname_id = admin_nqt_mailing_list.id');
		$this->db->where('admin_nqt_mailing_list_member.id',$listname_id);	
		$this->db->order_by($this->input->post('func_order_by'),$this->input->post('order_by'));
		$query = $this->db->get();
		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getEmailtosendmail($listname_id, $country_id){
		$this->db->select('admin_nqt_mailing_list_member.*');
		$this->db->from('admin_nqt_mailing_list_member');
		$this->db->where('listname_id',$listname_id);
		// $this->db->where('country_id',$country_id);
		$this->db->where('unsubscribe',0);
		$query = $this->db->get();
		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getbonusMail($listname_id,$country_id){
		$this->db->select('id');
		$this->db->where('listname_id',$listname_id);
		$query = $this->db->count_all_results('admin_nqt_mailing_list_member');

		if($query > 0){
			return $query;
		}else{
			return 0;
		}
	}
	
	function getEmailFromToken($access_token){
		$this->db->select('admin_nqt_mailing_list_member.email');
		$this->db->from('admin_nqt_mailing_list_member');
		$this->db->where('access_token',$access_token);
		$this->db->where('unsubscribe',0);
		$query = $this->db->get();
		if($query->row()){
			return $query->row();
		}else{
			return false;
		}
	}
	
	function getBody($template_id){
		$this->db->select('admin_nqt_mailing_template.content, admin_nqt_mailing_template.footer');
		$this->db->where('id',$template_id);
		$query = $this->db->get('admin_nqt_mailing_template');
		if($query->row()){
			return $query->row();
		}else{
			return false;
		}
	}
}