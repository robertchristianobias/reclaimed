<?php
class Admincp_menu_redm_model extends CI_Model {
	private $module = 'admincp_menu_redm';
	private $table = 'admincp_menu';
	private $table_lang = 'admincp_menu_lang';
	private $field_parent_id = 'menu_id';

function getsearchContent($limit = 0, $page = -1){
		$result = false;
		$main_table = $this->table;
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*");
        }
		
		$this->db->from($main_table);

		// Force LEFT JOIN to keep data of main table
		
		if(!$is_count_total_rows){
			// Limit
			$this->db->limit($limit,$page);
			
			// Order
			$order_by = $this->input->post('func_order_by');
			$order_by = "{$main_table}.{$order_by}";
			$order_direction = $this->input->post('order_by');
			$this->db->order_by($order_by,$order_direction);
		}
		
		/*Begin: Condition*/
		// Begin - Search condition
		$content = $this->input->post('content');
		if(!empty($content)){
			$search_condition_arr = array(
				"{$main_table}.name_cn LIKE '%{$content}%'",
				"{$main_table}.name_en LIKE '%{$content}%'",
			);
			$search_condition = implode($search_condition_arr, ' OR ');
			$search_condition = "( {$search_condition} )";
			$this->db->where($search_condition);
		}
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');
		if(!empty($dateFrom) || !empty($dateTo)){
			$datetimeFrom = date('Y-m-d 00:00:00',strtotime($dateFrom));
			$datetimeTo = date('Y-m-d 23:59:59',strtotime($dateTo));
			if(empty($dateFrom)){
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			} else if(empty($dateTo)){
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
			} else {
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			}
		}
		// End - Search condition
		
		// Begin - Filter condition
		// $filter1 = (int)$this->input->post('filter1');
		// // $filter2 = (int)$this->input->post('filter2');
		// // $filter3 = (int)$this->input->post('filter3');
		// if(!empty($filter1)){
		// 	$this->db->where("{$main_table}.user_type_id = '{$filter1}'");
		// }
		// End - Filter condition
		
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
	}
	
	function getDetailManagement($id){
		// $this->db->select(PREFIX.$this->table.'.*, id AS data_lang');
		$this->db->select('*');
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			$result = $query->row();
			
			$this->db->select('*');
			$this->db->where($this->field_parent_id, $id);
			$query = $this->db->get(PREFIX.$this->table_lang);
			$temp = NULL;
			if($query->result()) {
				foreach ($query->result() as $key => $item) {
					$temp[$item->lang_code] = $item;
				}
			}
			//make data_lang property in result object. IMPORTANT!!!!
			$result->data_lang = $temp;
			return $result;

		
		}else{
			return false;
		}

		
	}
	
	function saveManagement($fileName=''){
		if(isset($this->lang->languages)){
			$all_lang = $this->lang->languages;
		}else{
			$all_lang = array(
				'' => ''
			);
		}

		//default data
		$_current_lang_code = '';
		$_status = ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$_created = $_updated = date('Y-m-d H:i:s',time());
		$_created_by = $_updated_by = ''; //TO DO
		$data_lang = $data_lang_temp = array();
		$link = $this->input->post('link');
		//$_image_url = $_thumbnail_url = '';
		
		if($this->input->post('hiddenIdAdmincp')==0){
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$keyerror = '-'.$key;
					$key = '_'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}

				$checkData = $this->checkData($this->input->post('name'.$key.'Admincp'),$key);
				if($checkData){
					print 'error-name'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
					exit;
				}
				
				// $checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key);
				// if($checkSlug){
				// 	print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
				// 	exit;
				// }
			}

			// if( ! empty($this->input->post('thumbnail_urlAdmincp')) ) {
			// 	$pre_url = $this->input->post('thumbnail_urlAdmincp');
			// 	$_thumbnail_url = move_file_from_url('admin_nqt_category_articles', $pre_url, TRUE);
			// }

			// if( ! empty($this->input->post('image_urlAdmincp')) ) {
			// 	$pre_url = $this->input->post('image_urlAdmincp');
			// 	$_image_url = move_file_from_url('admin_nqt_category_articles', $pre_url, FALSE);
			// }

			// if ( empty($_thumbnail_url) || empty($_image_url) ) {
			// 	print 'error-image.'.$this->security->get_csrf_hash();
			// 	exit;
			// }

			$data = array(
				// 'image'=> trim($fileName['image']),
				// 'thumbnail' => $_thumbnail_url,
				// 'image' => $_image_url,
				'link' =>$link,
				'status'=> $_status,
				'created'=> $_created,
				
			);
			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['name'.$key] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				//$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				//make data language
				$data_lang_temp['name'] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				//$data_lang_temp['slug'] = trim($this->input->post('slug' . $subfix));
				$data_lang_temp['created'] = $_created;
				$data_lang_temp['status'] = $_status;
				$data_lang_temp['lang_code'] = $_current_lang_code;
				//$data_lang_temp['thumbnail']  = $_thumbnail_url;
				//$data_lang_temp['image']      = $_image_url;
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			//DO INSERT DATA
			if($this->db->insert(PREFIX.$this->table,$data)){
				$insert_id = $this->db->insert_id();
				//Insert data language
				foreach ($data_lang as $key => $item) {
					$item[$this->field_parent_id] = $insert_id;
					$this->db->insert(PREFIX.$this->table_lang, $item);
				}
				//End insert data language
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}else{
			$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$keyerror = '-'.$key;
					$key = '_'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}
				$name = 'name'.$key;
				if($result->$name!=$this->input->post('name'.$key.'Admincp')){
					$checkData = $this->checkData($this->input->post('name'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
					if($checkData){
						print 'error-name'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						exit;
					}
				}
				
				// if($result->$slug!=$this->input->post('slug'.$key.'Admincp')){
				// 	$checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
				// 	if($checkSlug){
				// 		print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
				// 		exit;
				// 	}
				// }
			}
			
			//Xử lý xóa hình khi update thay đổi hình
			// if($fileName['image']==''){
			// 	$fileName['image'] = $result->image;
			// }else{
			// 	@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result->image);
			// }
		
			
			// if( ! empty($_thumbnail_url) ) {
			// 	$data['thumbnail'] = $_thumbnail_url;
			// }
			// if( ! empty($_image_url) ) {
			// 	$data['image'] = $_image_url;
			// }
			$data['status'] = $_status;
			$data['link'] =$link;
			//$data['updated'] = $_updated;

			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['name'.$key] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				//$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				//make data language
				$data_lang_temp['name'] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				//$data_lang_temp['name'] = trim($this->input->post('slug' . $subfix));
				
				$data_lang_temp['status'] = $_status;
				$data_lang_temp['lang_code'] = $_current_lang_code;
				// if( ! empty($_thumbnail_url) ) {
				// 	$data_lang_temp['thumbnail'] = $_thumbnail_url;
				// }
				// if( ! empty($_image_url) ) {
				// 	$data_lang_temp['image'] = $_image_url;
				// }
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			$update_id = $this->input->post('hiddenIdAdmincp');
			$old_value[] = $result;
			unset($data['created']	);
			modules::run('admincp/saveLog',$this->module,$this->input->post('hiddenIdAdmincp'),'','Update', $old_value, $data);
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data)){
				//Update data in table language
				foreach ($data_lang as $key => $item) {
					$lang_code = $item['lang_code'];
					unset($item['lang_code']);
					$this->db->where('lang_code', $lang_code);
					$this->db->where($this->field_parent_id, $update_id);
					$this->db->update(PREFIX.$this->table_lang, $item);
				}
				//end update data language
				return true;
			}
		}
		return false;
	}

	function softDeleteData ($id) {
		$data ['is_delete'] = 1;
		$this->db->where('id', $id);
		if ($this->db->update (PREFIX.$this->table, $data)) {
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			//Soft-delete data in table language
			$this->db->where ($this->field_parent_id, $id);
			$this->db->update (PREFIX.$this->table_lang, $data);
			//end soft-delete data language
			return TRUE;
		}
		return FALSE;
	}
	
	function checkData($title,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('name'.$lang,$title);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
		// FOR DEBUG
		$debug = true;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	function getAllData() {
		$this->db->select('id, title_en, title_vi');
		$this->db->where('status',1);
		#check soft delete
		$this->db->where('is_delete', 0);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
}