<script src="<?=get_resource_url('assets/chart/Chart.bundle.min.js')?>"></script>	
<script src="<?=get_resource_url('assets/chart/utils.js')?>"></script>	

<script>
$(document).ready(function() {           
    $('#time_from, #time_to').timepicker(); 
});	
</script>

<input type="hidden" value="<?php ($this->session->userdata('start'))? print $this->session->userdata('start') : print 0 ?>" id="start" />
<input type="hidden" value="<?=$default_func?>" id="func_sort" />
<input type="hidden" value="<?=$default_sort?>" id="type_sort" />
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="portlet-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Alert !!!</h4>
			</div>
			<div class="modal-body">
				Are you sure delete item selected?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue" onclick="deleteAll()">Delete</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$module_name?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
		<li><?=$module_name?></li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE NOTIFICATION-->
<div class="form-body notification notification-index" style="display:none">
	<div class="alert alert-success" style="display:none">
		<strong>Success!</strong> The page has been saved.
	</div>
	
	<div class="alert alert-danger" style="display:none">
		<strong>Error!</strong> <span id="txt_error"></span>
	</div>
</div>
<!-- END PAGE NOTIFICATION-->
<!-- BEGIN PAGE CONTENT-->
<!--=======================BEGIN ROW-1==============================-->
<div class="row hidden">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="info-box blue">
				<span class="info-box-icon bg-blue-blue"><i class="fa fa-tag"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Category Rank</span>
					<span class="info-box-number">Internet and Telecom &gt; Web Design # 4,410</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>		

		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="info-box green">
				<span class="info-box-icon bg-green-green"><i class="fa fa-globe"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Global Rank</span>
					<span class="info-box-number"># 4,092,347</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	

		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="info-box yellow">
				<span class="info-box-icon bg-yellow-yellow"><i class="fa fa-star"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Top Country Rank</span>
					<span class="info-box-number">India # 239,341</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>			
	</div>
<!--=======================END ROW-1==============================-->
<div class="clearfix-30"></div>



<div class="active-user">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
			<div class="div-ga-active-user">
				<div class="active-user-title">Active User</div>
				<div id="ga_active_users" class="ga-active-user margin-top-20">N/A</div>
				<button type="button" class="btn button-bg-blue margin-top-20"
					name="btn_get_active_users" 
		            id="btn_get_active_users" 
		            data-endpoint="<?=$url_get_active_user?>"
					>
					<i class="fa fa-refresh"></i> Refesh</button>
			</div>
		</div>
	</div>
</div>




<div class="clearfix-30"></div>


<!--=======================BEGIN CHART==============================-->

<div class="active-user">
	<div class="row">
		<div class="col-sm-12 text-center margin-bottom-20">
			<div class="active-user-title">Analytic</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-4">
            <div class="form-group">
 <label class="control-label control-label2 col-md-3">From date</label>
 <div class="col-md-3">
     <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
         <input value="<?=$start_date?>" type="date" name="ga_from_date" id="date_from" class="form-control"/>
     </div>
 </div>
             </div>
		</div>
		<div class="clearfix hidden-lg"></div>

		<div class="col-lg-4">
            <div class="form-group">
 <label class="control-label control-label2 col-md-3">To date</label>
 <div class="col-md-3">
     <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
         <input value="<?=$end_date?>" type="date" name="ga_to_date" id="date_to" class="form-control"/>
     </div>
 </div>
             
            </div>	
		</div>
		<div class="clearfix hidden-lg"></div>

		<div class="col-lg-4 p767-ml-15">
			<button type="button" class="btn button-bg-blue" 
            name="btn_retrieve_data" 
            id="btn_retrieve_data" 
            data-endpoint="<?=$url_retrieve_data?>"
            data-viewtype="day";
            data-startdate="<?=$start_date?>";
            data-enddate="<?=$end_date?>";
			>
				<i class="fa fa-refresh"></i> Get Chart</button>
		</div>

		<div class="clearfix"></div>
		<div class="col-sm-8">
			<!-- metrics -->
		    <?php foreach ($metrics as $key => $item) { 
		            $code = $item ['code'];
		            $name = $item ['name'];
		            $checked = ($item ['default'] == 1) ? 'checked' : '';
		            $id = 'cbx_metric_'.$key;
		        ?>
		        <input id="<?=$id?>" name="cbx_metrics" type="checkbox" value="<?=$code?>" <?=$checked?>/>
		        <label for="<?=$id?>"><?=$name?></label>
		    <?php } ?>
		    <!-- /metrics -->
		</div>
		<!-- <div class="col-sm-4 col-sm-offset-8 margin-top-30"> -->
			<!-- <select class="form-control" name="categoryAdmincp" id="groupAdmincp">
                <option value="hourly">Hourly</option>
                <option selected value="day">Day</option>
                <option value="week">Week</option>
                <option value="month">Month</option>
            </select> 
		</div>-->
		<div class="col-sm-4 margin-top-30">
            <select class="form-control" name="view_type" id="view_type" >
		        <?php foreach ($dimensions as $key => $item) {
		            $code = $item ['code'];
		            $name = $item ['name'];
		            $selected = ($item ['default'] == 1) ? 'selected' : '';
		            echo "<option value='$code' $selected>$name</option>";
		        } ?>
			</select>
		</div>
		
	</div>
</div>



<div>
    <canvas id="myChart"></canvas>
</div>
<div class="clearfix-30"></div>
<!-- <script type="text/javascript">
	var config = {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "My First dataset",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: [
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor()
                ],
                fill: false,
            }, {
                label: "My Second dataset",
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor(),
 randomScalingFactor()
                ],
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
 display: true,
 scaleLabel: {
     display: true,
     labelString: 'Month'
 }
                }],
                yAxes: [{
 display: true,
 scaleLabel: {
     display: true,
     labelString: 'Value'
 }
                }]
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("myChart").getContext("2d");
        window.myChart = new Chart(ctx, config);
    };
</script> -->
<!--=======================END CHART==============================-->

<!--=======================BEGIN ROW-2==============================-->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-key"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number">
 <span class="engagementInfo-valueNumber js-countValue" id="ga_sessions">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Sessions</b>
				</span>
			</span></div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-user"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_users">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Users</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_pageviews">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Page Views</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-percent"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_pageviews_per_session">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Pages / Sessions</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-search-plus"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_avg_session_duration">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Avg. Session Duration</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-sign-out"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_bounce_rate">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>Bounce Rate</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-percent"></i></span>
			<div class="info-box-content">
				<!-- <span class="info-box-text">Inventory</span> -->
				<span class="info-box-number" id="ga_percent_new_sessions">N/A</span>
				<div class="progress">
					<div style="width: 70%" class="progress-bar"></div>
				</div>
				<span class="progress-description">
					<b>% New Sessions</b>
				</span>
			</div><!-- /.info-box-content -->
		</div>
	</div>
</div>

<!--=======================END ROW-1==============================-->
<div class="clearfix-30"></div>

<div class="row">
	<div class="col-md-6">
      	<div class="active-user-title text-center">Country</div>
      	<table class="table table-condensed table-tr-h">
		   <thead>
		      <tr>
		         <th>
		            <h4><b>No.</b></h4>
		         </th>
		         <th>
		            <h4><b>Country</b></h4>
		         </th>
		         <th>
		            <h4><b>Total</b></h4>
		         </th>
		      </tr>
		   </thead>

		   <tbody>
		   <?php 
		  	foreach ($country as $k => $v) {
		    ?>
		      <tr>
		         <td><?=$k+1?></td>
		         <td><?=$v->country?></td>
		         <td><?=$v->total?></td>
		      </tr>
		    <?php
		    }
		    ?>
		   </tbody>
		</table>
	</div>

	<div class="col-md-6">
      	<div class="active-user-title text-center">City</div>
      	<table class="table table-condensed table-tr-h">
		   <thead>
		      <tr>
		         <th>
		            <h4><b>No.</b></h4>
		         </th>
		         <th>
		            <h4><b>City</b></h4>
		         </th>
		         <th>
		            <h4><b>Total</b></h4>
		         </th>
		      </tr>
		   </thead>
		   <tbody>
		    <?php 
		  	foreach ($city as $k => $v) {
		    ?>
		      <tr>
		         <td><?=$k+1?></td>
		         <td><?=$v->city?></td>
		         <td><?=$v->total?></td>
		      </tr>
		    <?php
		    }
		    ?>
		   </tbody>
		</table>
	</div>
</div>
<div class="clearfix-30"></div>

 

<div class="row">
	<div class="col-md-12">
      	<div class="active-user-title text-center">Blog</div>

      	<ul class="nav nav-tabs nav-blog margin-top-20">
			<li class="active"><a data-toggle="tab" href="#blog-en">EN</a></li>
			<li><a data-toggle="tab" href="#blog-cn">CN</a></li>
		</ul>

		<div class="tab-contents tab-contents-blog">
			<div id="blog-en" class="tab-pane fade in active">
			  	<table class="table table-condensed table-tr-h">
				   <thead>
				      <tr>
				         <th>
				            <h4><b>No.</b></h4>
				         </th>
				         <th>
				            <h4><b>Title</b></h4>
				         </th>
				         <th>
				            <h4><b>View</b></h4>
				         </th>
				         <th>
				            <h4><b>Comment</b></h4>
				         </th>
				      </tr>
				   </thead>

				   <tbody>
					<?php 
				  	foreach ($total_view_en as $k => $v) {
				    ?>
				     <tr>
				         <td><?php echo $k+1 ?></td>
				          <td><?php echo $v->title ?></td>
				         <td><?php echo $v->view ?></td>
				         <td>
				      		<?php
				         	
				         	$total_comment = $this->model->total_comment($v->id,'en');
				         	foreach ($total_comment as $result) {
				         ?>
				         <?php echo $result->total ?>

				       	 <?php } ?>
				          </td>
				     </tr>

				    <?php } ?>
				   </tbody>
				</table>
			</div>

			<div id="blog-cn" class="tab-pane fade">
			  	<table class="table table-condensed table-tr-h">
				   <thead>
				      <tr>
				         <th>
				            <h4><b>No.</b></h4>
				         </th>
				         <th>
				            <h4><b>Title</b></h4>
				         </th>
				         <th>
				            <h4><b>View</b></h4>
				         </th>
				         <th>
				            <h4><b>Comment</b></h4>
				         </th>
				      </tr>
				   </thead>

				   <tbody>
					<?php 
				  	foreach ($total_view_cn as $k => $v) {
				    ?>
				     <tr>
				         <td><?php echo $k+1 ?></td>
				          <td><?php echo $v->title ?></td>
				         <td><?php echo $v->view ?></td>
				         <td>
				      		<?php
				         	
				         	$total_comment = $this->model->total_comment($v->id, 'cn');
				         	foreach ($total_comment as $result) {
				         ?>
				         <?php echo $result->total ?>

				       	 <?php } ?>
				          </td>
				     </tr>

				    <?php } ?>
				   </tbody>
				</table>
			</div>
		</div>

	</div>
</div>

<div class="clearfix-30"></div>



<div class="row">
	<div class="col-md-12">
      	<div class="active-user-title text-center">User</div>

      	<ul class="nav nav-tabs nav-blog margin-top-20">
			<li class="active"><a data-toggle="tab" href="#user-age">Age</a></li>
			<li><a data-toggle="tab" href="#user-gender">Gender</a></li>
		</ul>
		<div class="tab-contents tab-contents-blog">

			<div id="user-age" class="tab-pane fade in active">
			  	<table class="table table-condensed table-tr-h">
				   <thead>
				      <tr>
				         <th>
				            <h4><b>No.</b></h4>
				         </th>
				         <th>
				            <h4><b>Age</b></h4>
				         </th>
				         <th>
				            <h4><b>Total</b></h4>
				         </th>
				         <th>
				            <h4><b>%</b></h4>
				         </th>
				      </tr>
				   </thead>

				   <tbody>
					<?php 
				  	foreach ($total_age as $k => $v) {
				    ?>
				     <tr>
				        <td><?php echo $k+1 ?></td>
				      	<td><?php echo $v->age ?></td>
				        <td><?php echo $v->total ?></td>
				      	<td>
				      		<?php foreach ($total_age_pt as $k => $v2) { ?>
				      			<?php echo round($v->total / $v2->total,3)*100 ?>
				      		<?php }?>
				      	</td>	             
				     </tr>
				    <?php } ?>
				   </tbody>
				</table>
			</div>

			<div id="user-gender" class="tab-pane fade">
			  	<table class="table table-condensed table-tr-h">
				   <thead>
				      <tr>
				         <th>
				            <h4><b>No.</b></h4>
				         </th>
				         <th>
				            <h4><b>Gender</b></h4>
				         </th>
				          <th>
				            <h4><b>Total</b></h4>
				         </th>
				          <th>
				            <h4><b>%</b></h4>
				         </th>
				      </tr>
				   </thead>

				    <tbody>
					<?php 
				  	foreach ($total_gender as $k => $v) {
				    ?>
				     <tr>
				        <td><?php echo $k+1 ?></td>
				      	
				        <td><?php 
				        		if($v->gender == '1'){
				        			echo 'Female';
				        		} else if($v->gender == '2'){
				        			echo 'Male';
				        		} else if($v->gender == '3'){
				        			echo 'Unknown';
				        		} else{
				        			echo '0';
				        		}

				        	?>

				        </td>
				        <td><?php echo $v->total ?></td>
				      	<td>
				      		<?php foreach ($total_gender_pt as $k => $v2) { ?>
				      			<?php echo round($v->total / $v2->total,3)*100 ?>
				      		<?php }?>
				      	</td>	             
				     </tr>
				    <?php } ?>
				   </tbody>
				</table>
			</div>

		</div>		
	</div>
</div>

<div class="clearfix-30"></div>
<!--=======================BEGIN ROW-3==============================-->	
<div class="row hidden">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-arrow-right"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Direct Traffic</span>
					<span class="info-box-number">13.63%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-arrow-down"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Referral Traffic</span>
					<span class="info-box-number">8.71%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-search"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Search Traffic</span>
					<span class="info-box-number">60.02%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-share-alt"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Social Traffic</span>
					<span class="info-box-number">17.64%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Mail Traffic</span>
					<span class="info-box-number">0%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-laptop"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Display Traffic</span>
					<span class="info-box-number">0%</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>	
	</div>
<!--=======================END ROW-3==============================-->
<div class="clearfix-30"></div>
<!--=======================BEGIN ROW-4==============================-->	
<div class="row hidden">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="info-box bg-blue">
				<span class="info-box-icon "><i class="fa fa-search"></i></span>
				<div class="info-box-content">
					<!-- <span class="info-box-text">Inventory</span> -->
					<span class="info-box-number">No Data</span>
					<div class="progress">
						<div style="width: 70%" class="progress-bar"></div>
					</div>
					<span class="progress-description">
						<b>Organic Search</b>
					</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="info-box bg-blue">
				<span class="info-box-icon"><i class="fa fa-usd"></i></span>
				<div class="info-box-content">
					<!-- <span class="info-box-text">Inventory</span> -->
					<span class="info-box-number">No Data</span>
					<div class="progress">
						<div style="width: 70%" class="progress-bar"></div>
					</div>
					<span class="progress-description">
						<b>Paid Search</b>
					</span>
				</div><!-- /.info-box-content -->
			</div>
		</div>
	</div>
<!--=======================END ROW-4==============================-->


<!-- END PAGE CONTENT-->

    <script>
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };
        var colorNames = Object.keys(window.chartColors);
        var datasets = [];
        var labels = [];
        var chart_title = 'ReClaimEDM Google Analytics';

        var config = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets
                },
                options: {
                    responsive: true,
                    title:{
                        display:true,
                        text: chart_title,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Dimension'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                    }
                }
            };

        window.onload = function() {
            var ctx = document.getElementById("myChart").getContext("2d");
            window.myChart = new Chart(ctx, config);
            retrieveData();
            getActiveUsers();
        };

        document.getElementById('view_type').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.viewtype = this.value.toLowerCase();
        });

        document.getElementById('date_from').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.startdate = this.value.toLowerCase();
        });

        document.getElementById('date_to').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.enddate = this.value.toLowerCase();
        });

        document.getElementById('btn_get_active_users').addEventListener('click', function() {
            getActiveUsers();
        });

        document.getElementById('btn_retrieve_data').addEventListener('click', function(){
            retrieveData();
        });

        function makeConfigLineChart(dimension, metrics, chartTitle) {
            var labels_data = eval(dimension.data);
            var labels_title = dimension.title;
            var myDatasets = [];
            var myMetricLabels = [];
            var numMetrics = metrics.length;
            for(var i=0; i < numMetrics; i++) {
                var metric = metrics[i];
                var colorName = colorNames[i % colorNames.length];
                var newColor = window.chartColors[colorName];
                var newDataset = {
                        label: metric.title,
                        backgroundColor: newColor,
                        borderColor: newColor,
                        data: eval(metric.data),
                        fill: false
                    };
                var newMetricLabel = {
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: metric.title
                        }
                    };

                myDatasets.push(newDataset);
                myMetricLabels.push(newMetricLabel);
            }

            var config = {
                type: 'line',
                data: {
                    labels: labels_data,
                    datasets: myDatasets
                }, 
                options: {
                    responsive: true,
                    title:{
                        display:true,
                        text: chartTitle
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: labels_title
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                        // yAxes: myMetricLabels
                    }
                }
            };
            return config;
        }

        function getActiveUsers() {
            var element  = document.getElementById('btn_get_active_users');
            var endpoint = element.dataset.endpoint;
            var response = doRequestXHR('post', endpoint, null);
            document.getElementById('ga_active_users').innerHTML = response.data.ga_active_users;
        }

        function retrieveData() {
            var element = document.getElementById('btn_retrieve_data');
            var endpoint = element.dataset.endpoint;
            var metrics_element = document.getElementsByName('cbx_metrics');
            var metrics_lenghth = metrics_element.length;
            var metrics = [];
            for(var i = 0; i < metrics_lenghth; i++) {
                if (metrics_element[i].checked === true) {
                    metrics.push(metrics_element[i].value);
                }
            }
            var start_date = element.dataset.startdate;
            var end_date = element.dataset.enddate;
            var view_type = element.dataset.viewtype;
            var dimension = element.dataset.viewtype;
            var data = new FormData();
            data.append('start_date', start_date);
            data.append('end_date', end_date);
            data.append('view_type', view_type);
            data.append('dimension', dimension);
            for(var i = 0; i < metrics_lenghth; i++) {
                if (metrics_element[i].checked === true) {
                    data.append('metrics[]', metrics_element[i].value);
                }
            }
            var response = doRequestXHR('post', endpoint, data);
            if(response.status == 0) {
                console.log(response);
                return;
            }
            updateStatisticalData(response.data.statistical);
            var chart_title = 'ReClaimEDM Google Analytics';
            var dimension = response.data.chart.dimensions;
            var metrics = response.data.chart.metrics;
            updateChart(dimension, metrics);
        }

        function updateChart(dimension, metrics) {
            var myDatasets = [];
            var myMetricLabels = [];
            var labelsData = eval(dimension.data);
            var numMetrics = metrics.length;
            for(var i=0; i < numMetrics; i++) {
                var metric = metrics[i];
                var colorName = colorNames[i % colorNames.length];
                var newColor = window.chartColors[colorName];
                var newDataset = {
                        label: metric.title,
                        backgroundColor: newColor,
                        borderColor: newColor,
                        data: eval(metric.data),
                        fill: false
                    };
                var newMetricLabel = {
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: metric.title
                        }
                    };

                myDatasets.push(newDataset);
                myMetricLabels.push(newMetricLabel);
            }

            window.config.data.datasets = myDatasets;
            window.config.data.labels = labelsData;
            window.config.options.scales.xAxes[0].scaleLabel.labelString = dimension.title;
            window.myChart.update();
        }

        function updateActiveUsers(active_users) {
            document.getElementById('ga_active_users').innerHTML = active_users;
        }

        function updateStatisticalData(statistical_data) {
            document.getElementById('ga_sessions').innerHTML = statistical_data.sessions;
            document.getElementById('ga_users').innerHTML = statistical_data.users;
            document.getElementById('ga_pageviews').innerHTML = statistical_data.pageviews;
            document.getElementById('ga_pageviews_per_session').innerHTML = parseFloat(statistical_data.pageviewsPerSession).toFixed(2);
            document.getElementById('ga_avg_session_duration').innerHTML = parseFloat(statistical_data.avgSessionDuration).toFixed(2);
            document.getElementById('ga_bounce_rate').innerHTML = parseFloat(statistical_data.bounceRate).toFixed(2);
            document.getElementById('ga_percent_new_sessions').innerHTML = parseFloat(statistical_data.percentNewSessions).toFixed(2);
        }

        function doRequestXHR(type, endpoint, data) {
            var response = false;
            var request = new XMLHttpRequest();
            request.open(type, endpoint , false);
            request.loadstart = function() {
            };
            request.onload = function() {
                response = JSON.parse(request.responseText);
            };

            request.error = function(error) {
            console.log(eror);
            }
            // Before send TODO
            request.send(data);
            return response;
        }
    </script>