<?php
class Admincp_pages_model extends CI_Model {
	private $module = 'admincp_pages';
	private $table = 'admin_nqt_pages';
	private $field_parent_id = 'id';
	private $table_category = 'admin_nqt_category_articles';

	function getsearchContent($limit,$page){
		$this->db->select('*');
		$this->db->limit($limit,$page);
		$this->db->order_by($this->input->post('func_order_by'),$this->input->post('order_by'));
		if($this->input->post('content')!=''){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalsearchContent(){
		$this->db->select('*');
		if($this->input->post('content')!='' && $this->input->post('content')!='type here...'){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetailManagement($id){
		// $this->db->select(PREFIX.$this->table.'.*, id AS data_lang');
		$this->db->select('*');
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			$result = $query->row();
			
			$this->db->select('*');
			$this->db->where($this->field_parent_id, $id);
			$query = $this->db->get(PREFIX.$this->table);
			$temp = NULL;
			return $result;
		}else{
			return false;
		}
	}
	
	function saveManagement($fileName=''){
		if(isset($this->lang->languages)){
			$all_lang = $this->lang->languages;
		}else{
			$all_lang = array(
				'' => ''
			);
		}

		//default data
		$_userInfo = $this->session->userdata('userInfo');
		$_status = $this->input->post('statusAdmincp');
		$_created = $_updated = date('Y-m-d H:i:s',time());
		$name = $this->input->post('name_page');
		$meta_title = $this->input->post('meta_title');
		$meta_keyword = $this->input->post('meta_keyword');
		$meta_description = $this->input->post('meta_description');

		if( ! empty($this->input->post('thumbnail_urlAdmincp')) ) {
			$pre_url = $this->input->post('thumbnail_urlAdmincp');
			$_thumbnail_url = move_file_from_url('admin_nqt_pages', $pre_url, TRUE);
		}

		if( ! empty($this->input->post('image_urlAdmincp')) ) {
			$pre_url = $this->input->post('image_urlAdmincp');
			$_image_url = move_file_from_url('admin_nqt_pages', $pre_url, FALSE);
		}

		/* change slug  */
		function normalize ($string) {
		    $table = array(
		        'Š'=>'S', 'š'=>'s', 'Đ'=>'D', 'đ'=>'d', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
		        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
		        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
		        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
		        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
		        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
		        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
		        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', ' '=>'-'
		    );
		    return strtr($string, $table);
		}

		if($this->input->post('hiddenIdAdmincp')==0){
			
			// $data['created_by'] = $_userInfo;
			$data['status']				= 1;
			$data['created'] 			= $_created;
			$data['name']     			= $name;
			$data['meta_title'] 		= $meta_title;
			$data['meta_keyword']   	= $meta_keyword;
			$data['meta_description']   = $meta_description;
			$data['image']   			= $_image_url;
			$data['thumbnail']   		= $_thumbnail_url;

			
			if($this->db->insert(PREFIX.$this->table,$data)){
				$insert_id = $this->db->insert_id();
				
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');

				$update_id = $insert_id;
				//echo 'ok 2';
				return true;
			}
		}else{
			$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
			
			//Xử lý xóa hình khi update thay đổi hình
			if($fileName['image']==''){
				$fileName['image'] = $result->image;
			}else{
				@unlink(BASEFOLDER.DIR_UPLOAD_IMGS.$result->image);
			}
			

			if( ! empty($_thumbnail_url) ) {
				$data['thumbnail'] = $_thumbnail_url;
			}
			if( ! empty($_image_url) ) {
				$data['image'] = $_image_url;
			}
			
			$data['status']				= $_status;
			$data['created'] 			= $_created;
			$data['name']     			= $name;
			$data['meta_title'] 		= $meta_title;
			$data['meta_keyword']   	= $meta_keyword;
			$data['meta_description']   = $meta_description;
			
			$update_id = $this->input->post('hiddenIdAdmincp');
			$old_value[] = $result;
			
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data)){
				return true;
			}
		}
		return false;
	}


	function softDeleteData ($id) {
		$data ['is_delete'] = 1;
		$this->db->where('id', $id);

		if ($this->db->update (PREFIX.$this->table, $data)) {
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			//Soft-delete data in table language
			$this->db->where ($this->field_parent_id, $id);
			$this->db->update (PREFIX.$this->table, $data);
			return TRUE;
		}
		return FALSE;
	}
	
	function checkData($title,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('title'.$lang,$title);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	function getCategories() {
		$result = FALSE;
		$all_lang = (isset($this->lang->languages)) ? $this->lang->languages : array('' => '');

		$this->db->select('*');
		$this->db->where('status',1);
		#check soft delete
		$this->db->where('is_delete', 0);
		$query = $this->db->get(PREFIX.$this->table_category);

		if($query->result()){
			$result = $query->result();
			foreach ($result as $key => $value) {
				$title_merge = '';
				foreach ($all_lang as $key => $lang) { 
					$_title = 'title_' . $key;
					$title_merge .= strtoupper($key) . ': ' . $value->$_title . ' | ';
				}
				$title_merge = substr($title_merge, 0, -3);
				// echo $title_merge;
				// $title_merge = $value->title_vi . ' | VI: ' . $value->title_en;
				$value->title_merge = $title_merge;
				$value->name = $title_merge;
			}
		}
		return $result;
	}
}