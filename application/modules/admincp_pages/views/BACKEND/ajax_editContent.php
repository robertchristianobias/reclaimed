<!-- <script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovaeditor.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovamanager.js'?>"></script> -->
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script> 

<!-- <script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/jquery-2.1.1.js')?>"></script> -->
<script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places&language=en"></script>

<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">


<?php
    if(isset($this->lang->languages)){
        $all_lang = $this->lang->languages;
    }else{
        $all_lang = array(
            '' => ''
        );
    }
?>
<script type="text/javascript">

function save(){
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
	};
	 $('#frmManagement').ajaxSubmit(options);
}


function showRequest(formData, jqForm, options) {
   var form = jqForm[0];
	
}

function showResponse(responseText, statusText, xhr, $form) {
    responseText = responseText.split(".");
    token_value  = responseText[1];
    $('#csrf_token').val(token_value);
    if(responseText[0]=='success'){
        <?php if($id==0){ ?>
        location.href=root+module+"/#/save";
        <?php }else{ ?>
        if($('.form-upload').val() != ''){
            $.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
                var res = data.split("src=");
                $('.fileinput-filename').html('');
                $('.fileinput').removeClass('fileinput-exists');
                $('.fileinput').addClass('fileinput-new');
            });
        }
        show_perm_success();
        <?php } ?>
    }
    
    if(responseText[0]=='error-image'){
        $('#txt_error').html('Only upload image.');
        show_perm_denied();
        return false;
    }

    if(responseText[0]=='permission-denied'){
        $('#txt_error').html('Permission denied.');
        show_perm_denied();
        return false;
    }
}

</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
        <li><a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a><i class="fa fa-angle-right"></i></li>
        <li><?php echo ($this->uri->segment(4)=='') ?  'Add new' :  'Edit' ?></li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade" style="overflow:hidden">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Form Information
                </div>
            </div>
            
            <div class="portlet-body form">
                <div class="form-body notification" style="display:none">
                    <div class="alert alert-success" style="display:none">
                        <strong>Success!</strong> The page has been saved.
                    </div>
                    
                    <div class="alert alert-danger" style="display:none">
                        <strong>Error!</strong> <span id="txt_error"></span>
                    </div>
                </div>
                
                <!-- BEGIN FORM-->
                <form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
                    <input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
                    <input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />
					<input type="hidden" value="<?php echo(isset($result->status)) ? $result->status : ''?>" id="check_draft" name="check_draft">
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <div class="checkbox-list">
                                    <label class="checkbox-inline">
                                        <div class="checkbox">
											<span>
												<input <?php if(isset($result->status)){ if($result->status==1){ ?>checked="checked"<?php }}else{ ?>checked="checked"<?php } ?> type="checkbox" name="statusAdmincp" id="statusAdmincp">
											</span>
										</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <?php /*get avatar url*/
                            $image_url = ( ! empty($result->image) ) ? get_resource_url($result->image) : '';
                            $thumbnail_url = ( ! empty($result->thumbnail) ) ? get_resource_url($result->thumbnail) : null;
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Image<span class="required" aria-required="true">*</span></label>
                            <div class="col-md-3">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="col-md-4 col-xs-12">
                                           <div>
                                                <input type="button" name="input_avatar" value="Select File" class="update-img-upload " id="cropContainerHeaderButton" />
                                                <!-- image thumbnail -->
                                                <input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
                                                <!-- image original -->
                                                <input type="hidden" id="input_image_url" name="image_urlAdmincp">

                                            </div>
                                            <div class="img-profile">
                                                <div id="cropic_element" style="display:none"></div>
                                                <a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>">
                                                    <img id="preview_image" src="<?=$thumbnail_url?>"> 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       
						<div class="form-group">
							<label class="control-label col-md-3">
								Name page
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="name_page" 
									id="name_page"
									value="<?php echo (isset($result->name) ? $result->name : '') ?>"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Meta title
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="meta_title" 
									id="meta_title"
									value="<?php echo (isset($result->meta_title) ? $result->meta_title : '') ?>"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Meta keyword
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="meta_keyword" 
									id="meta_keyword"
									value="<?php echo (isset($result->meta_keyword) ? $result->meta_keyword : '') ?>"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Meta description 
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="meta_description" 
									id="meta_description"
									value="<?php echo (isset($result->meta_description) ? $result->meta_description : '') ?>"/>
							</div>
						</div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button onclick="save()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save</button>
                                <a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->

<?php 
// Get count of tag relationships
function get_media() {
    $ci =& get_instance();

    $class = $ci->db->query("
        SELECT * from admin_upload_img order by created DESC
    ");
    if($class->result()){
       return $class->result();
     

      } else {
        return '0';
    }
}
$media = get_media();
?>


<div class="box-media-file">
    <div class="div-media-file">
       
       <?php if($media != '0') { ?> 

       <div class="div-media-img">
       <?php foreach($media as $r_media) { ?> 
        <div class="gird-media" data-url="<?php echo $r_media->id ?>" onclick="add_media(<?php echo $r_media->id ?>)"><img src="<?php echo rtrim(PATH_URL,'/') . $r_media->url ?>"></div>
      <?php }  ?>
      </div>
      <div class="clearfix"></div>
      <div class="div-media-button text-center">
         <span class="btn green btn-add-media" onclick="add_submit_media()">Add media</span>
       
      </div>

       <?php } else { ?>
          <div class="no-media">No image</div>
        <?php } ?>
    </div>
</div>

<!-- Button add img tinymce -->
<button type="button" class="btn btn-primary hidden" id="insert-asset-brief">Insert Image</button>


<style type="text/css">
    #cropic_element {
        height: 430px;
        width: 936px;
    }

    .box-media-file{
        width:100%;
        height: 100%;
         position:fixed;
        
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        z-index: 999;
        background: rgba(0,0,0,0.5);
        display: none; 
    }

    .box-media-file.active{
        display: block;
    }

    .box-media-file.active .div-media-file{
         width: calc(100% - 100px);
        height:calc(100% - 100px);
        background:white;
        position:absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        background: white;
        box-shadow:1px 1px 1px #f5f5f5;
        padding:10px;
        /* overflow-x: hidden; */
        overflow:scroll;
    }

    .no-media{
        font-size: 25px;
        color: black;
    }

    .div-media-img{
        height:calc(100% - 50px);
        /* background: red; */
    }
    .div-media-button{
        margin-top: 10px;
    }

    .gird-media{
        width: 180px;
        height: 180px;
        float: left;
        margin: 10px;
        position: relative;
    }

    .gird-media img{
        width: 100%;
        height: 100%;
        object-fit: cover;
        box-shadow: inset 0 0 15px rgba(0,0,0,.1), inset 0 0 0 1px rgba(0,0,0,.05);
        background: #eee;
    }

    .gird-media:hover{
        outline: 5px solid #488bab;
        cursor: pointer;
    }

    .gird-media.active{
        outline: 5px solid #488bab; 
        cursor: pointer;
    }

    .gird-media.active:before{
        position: absolute;
        top: -4px;
        right: -4px;
        background: #488bab;
        width: 25px;
        height: 25px;
        content: '\f00c';
        font-family:FontAwesome;
        color: white;
        text-align: center;
        line-height: 25px;
    }
    @media (max-width:767px){
        .gird-media img{
         height: 120px;
        }
    }

    @media (max-width:480px){
        .gird-media{
            width: 25%;
        }
        .gird-media img{
         height: 70px;
        }
    }

</style>

<script type="text/javascript">
function media_file(){
    $('.box-media-file').addClass('active');
}
function remove_media_file(){
    $('.box-media-file').removeClass('active');
}

function remove_media_file_active(){
    $('.gird-media').removeClass('active');
}

$('.btn-media').click(function(){
   media_file();
})

$('.box-media-file').click(function(){
    remove_media_file();
})

$('.div-media-file').click(function(e){
    e.stopPropagation();
})

$media_url = [];
var $text;
function add_media($id){
    if( !$('[data-url='+$id+']').hasClass('active') ) {
      $('[data-url='+$id+']').addClass('active');
      $media_url[$id] =  $('[data-url='+$id+']').html();  
      //  console.log($media_url);
      // console.log('Text:' + $text);
    } else{
        $('[data-url='+$id+']').removeClass('active');
        delete $media_url[$id] ;
        // console.log($media_url);
    }
}
function add_submit_media(){
    if( $('[data-url]').hasClass('active')){  
        remove_media_file();
        remove_media_file_active();
        $('#insert-asset-brief').click();
    }
}
$(document).on('click', '#insert-asset-brief', function(){
    $text = $media_url.join('');
    $text = $text.replace('undefined','');
    tinyMCE.execCommand('mceInsertContent',false,$text);
    $media_url = [];
    $text = '';

});
</script>


<script>
    var croppicHeaderOptions = {
            
            uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
            cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
            uploadData:{'prefix':'avatar_'},
            enableMousescroll:true,
            customUploadButtonId:'cropContainerHeaderButton',
            outputUrlId:'input_thumbnail_url',
            modal:true,
            rotateControls: false,
            doubleZoomControls:false,
            imgEyecandyOpacity:0.4,
            onBeforeImgUpload: function(){ },
            onAfterImgUpload: function(){ appendOriginImageAward(); },
            onImgDrag: function(){ },
            onImgZoom: function(){ },
            onBeforeImgCrop: function(){ },
            onAfterImgCrop:function(){ appendAward(); },
            onReset:function(){ onResetCropic(); },
            onError:function(errormessage){ console.log('onError:'+errormessage) }
    }   
    var croppic = new Croppic('cropic_element', croppicHeaderOptions);
    function appendOriginImageAward() {
        var url_origin = $("div#croppicModalObj > img").attr('src');
        $('#input_image_url').val(url_origin);
        $("#preview_image_fancybox").attr("href", $('#input_image_url').val());
    }
    function appendAward(){
        $("#preview_image").attr("src", $('#input_thumbnail_url').val());
        $("#preview_image").show();
    }
    function onResetCropic(){
        $('#input_image_url').val('');
        $('#input_thumbnail_url').val('');
        $("#preview_image_fancybox").attr("href", '');
        $("#preview_image").attr("src", '');
    }
</script>


