<!DOCTYPE html>
<html lang="en">
<head>
<?php
$lang = 'en';
$image_homepage = '';
$title_homepage = '';
$keyword_homepage = '';
$description_homepage = '';
if (!empty($articles_blog)) {
    $articles_blog = $articles_blog[0];
}
if (!empty($articles_blog)) {
    $title = $articles_blog->title;
    $description = $articles_blog->description;
    $meta_title = $articles_blog->meta_title;
    $meta_description = $articles_blog->meta_description;
    $meta_keywords = $articles_blog->meta_keyword;
    $image = get_resource_url($articles_blog->image);
} else if (!empty($meta_homepage)) {
    $image_homepage = $meta_homepage['image'];
    $title_homepage = $meta_homepage['meta_title'];
    $keyword_homepage = $meta_homepage['meta_keyword'];
    $description_homepage = $meta_homepage['meta_description'];
}

$title = !empty($title) ? $title : $title_homepage;
$meta_title = !empty($meta_title) ? $meta_title : $title_homepage;
$keywords = !empty($meta_keywords) ? $meta_keywords : $keyword_homepage;
$description = !empty($meta_description) ? $meta_description : $description_homepage;
$image = !empty($image) ? $image : $image_homepage;

// Check login
$login = 'no';
$is_music_player_displayed = 1; // TEST - DEBUG
if (isset($this->session->userdata['userData'])) {
    $is_music_player_displayed = 1;
    $login = 'yes';
}
?>
<title><?=$title?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>" type="image/x-icon" />
	<link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/bootstrap-combined.min.css')?>">
	<link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/croppic.css')?>">

	<link href="<?=get_resource_url('assets/css/style2.css')?>" type="text/css" rel="stylesheet">
	<script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
	<script src="<?=get_resource_url('assets/js/croppic.js')?>"></script>
	<script src="<?=get_resource_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
	<script>
	
	</script>
</head>
<body>
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker3').datetimepicker({
			pickDate: false
			});
		});
	</script>
	<style>
		.form-horizontal .control-label {
			text-align: left;
		}
		.form-horizontal .form-actions {
			padding-left: 0px;
			text-align: center;
		}
	</style>
	<div class="container">
		<form id="frmManagement" action="<?=PATH_URL.'ajax/event/save'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
			<input type="hidden" value="" id="csrf_token" name="csrf_token">
			<input type="hidden" value="0" name="hiddenIdAdmincp">
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3">Event Photo <span class="required" aria-required="true">*</span></label>
					<div class="col-md-3">
						<div class="fileinput fileinput-exists" data-provides="fileinput">
							<div class="input-group input-large">
								<div class="col-md-4 col-xs-12">
									<div>
										<input type="button" name="input_avatar" value="Select File" class="update-img-upload " id="cropContainerHeaderButton" />
										<!-- image thumbnail -->
										<input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
										<!-- image original -->
										<input type="hidden" id="input_image_url" name="image_urlAdmincp">

									</div>
									<div class="img-profile">
										<div id="cropic_element" style="display:none"></div>
										<a class="fancybox-button" id="preview_image_fancybox" href="">
											<img id="preview_image" src="">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php 
				if(!empty($data_page_event)){
					$eventtimes = json_decode($data_page_event['event_times']);
					$time = substr ( $eventtimes[0]->start_time, 0 , 10 );
				?>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="english">
						<div class="form-group">
							<label class="control-label col-md-3">Image</label>
							<div class="col-md-9">
								<div>
									<?php
									if(!empty($data_page_event['photo'])){
										echo "<img src='{$data_page_event['photo']}' style='width: 50%;' />";
									}
									?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								Event Name  
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" name="name_enAdmincp" id="name_enAdmincp" value="<?php echo (isset($data_page_event['name']))?$data_page_event['name']:''?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								Description 
								<span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<textarea data-required="1" cols="" rows="6" name="description_enAdmincp" value=""> <?php echo (isset($data_page_event['description']))?$data_page_event['description']:'' ?> </textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Date</label>
						<div class="col-md-3">
							<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
								<input value="" type="date" name="date" id="date" class="form-control">
							</div>
						</div>
						<label class="control-label col-md-1">Time</label>
						<div class="col-md-3">
							<div class="input-append" id="datetimepicker3">
								<input class="form-control" data-format="hh:mm:ss" type="text">
								<span class="add-on">
									<i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-time">
									</i>
								</span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">
						Location
						<span class="required" aria-required="true">*</span>
						</label>
						<div class="col-md-9">
							<input data-required="1" type="text" class="form-control" name="countryAdmincp" id="countryAdmincp" value="<?php echo (isset($data_page_event['place']))?$data_page_event['place']:''?>" placeholder="Location" autocomplete="off">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">
						Event time
						<span class="required" aria-required="true">*</span>
						</label>
						<div class="col-md-9">
							<input data-required="1" type="text" class="form-control" name="eventtime" id="eventtime" value="<?=$time?>" placeholder="event time" autocomplete="off">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">
						Start time
						<span class="required" aria-required="true">*</span>
						</label>
						<div class="col-md-9">
							<input data-required="1" type="text" class="form-control" name="starttime" id="starttime" value="<?php echo (isset($data_page_event['start_time']))?$data_page_event['start_time']:''?>" placeholder="start time" autocomplete="off">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">
						End time
						<span class="required" aria-required="true">*</span>
						</label>
						<div class="col-md-9">
							<input data-required="1" type="text" class="form-control" name="endtime" id="endtime" value="<?php echo (isset($data_page_event['end_time']))?$data_page_event['end_time']:''?>" placeholder="end time" autocomplete="off">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">
								Tags
								<span class="required" aria-required="true"></span>
							</label>
						<div class="col-md-9" data-name="tags-input">
							<input data-required="1" type="text" class="form-control tagit-hidden-field" name="tagsAdmincp" id="tagsAdmincp" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Link Tickets</label>
						<div class="col-md-9">
							<div class="tickets">
								<input value="" type="text" name="tickets" id="tickets" class="form-control">
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">URL Detail</label>
						<div class="col-md-9">
							<div class="url_blog_event">
								<input value="" type="text" name="url_blog_event" id="url_blog_event" class="form-control">
							</div>
						</div>
					</div>
				</div>
			</div>
				<?php } ?>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn-bt green" onclick="return save()"><i class="fa fa-pencil"></i>Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<style type="text/css">
		#cropic_element {
			height: 200px;
			width: 200px;
		}
		.form-control{
			height: 34px !important;
		}
		.input-append .add-on, .input-prepend .add-on {
   			 height: 34px;
		}
		[class^="icon-"], [class*=" icon-"] {
    		margin-top: 3px;
		}
		.tab-content {
			overflow: hidden;
		}
		textarea {
			width: 100%;
		}
		.form-actions{
			padding: 19px 20px 20px;
			margin-top: 0px;
			margin-bottom: 0px;
			background-color: transparent;
			border-top: 0px;
		}
		form#frmManagement {
			background: #ddd;
			padding: 25px;
		}
	</style>

	<script>
		function save(){
			
			var options = {
				beforeSubmit:  showRequest,  // pre-submit callback 
				success:       showResponse  // post-submit callback 
			};
			//$('#frmManagement').ajaxSubmit(options);
			$('#frmManagement').submit();
			alert('save');
			return false;
		}
		
		function showRequest(formData, jqForm, options) {
			alert("Success!");
		}
		
		function showResponse(responseText, statusText, xhr, $form) {
			responseText = responseText.split(".");
			token_value  = responseText[1];
			$('#csrf_token').val(token_value);
			if(responseText[0]=='success'){
			
			}
			
			if(responseText[0]=='error-image'){
				$('#txt_error').html('Only upload image.');
				return false;
			}
			if(responseText[0]=='permission-denied'){
				$('#txt_error').html('Permission denied.');
				return false;
			}
		}

	
		var croppicHeaderOptions = {
			uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
			cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
			uploadData:{'prefix':'avatar_'},
			enableMousescroll:true,
			customUploadButtonId:'cropContainerHeaderButton',
			outputUrlId:'input_thumbnail_url',
			modal:true,
			rotateControls: false,
			doubleZoomControls:false,
			imgEyecandyOpacity:0.4,
			onBeforeImgUpload: function(){ },
			onAfterImgUpload: function(){ appendOriginImageAward(); },
			onImgDrag: function(){ },
			onImgZoom: function(){ },
			onBeforeImgCrop: function(){ },
			onAfterImgCrop:function(){ appendAward(); },
			onReset:function(){ onResetCropic(); },
			onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var croppic = new Croppic('cropic_element', croppicHeaderOptions);
		function appendOriginImageAward() {
			var url_origin = $("div#croppicModalObj > img").attr('src');
			$('#input_image_url').val(url_origin);
			$("#preview_image_fancybox").attr("href", $('#input_image_url').val());
		}
		function appendAward(){
			$("#preview_image").attr("src", $('#input_thumbnail_url').val());
			$("#preview_image").show();
		}
		function onResetCropic(){
			$('#input_image_url').val('');
			$('#input_thumbnail_url').val('');
			$("#preview_image_fancybox").attr("href", '');
			$("#preview_image").attr("src", '');
		}
	</script>
</body>
</html>

