<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_login_facebook extends MX_Controller {

	private $module = 'admincp_login_facebook';
	private $table = 'page_item';
	private $table_lang = 'page_item_lang';
	function __construct(){
		$this->admin_module = 'admincp_login_facebook';
		parent::__construct();
		$this->load->library('session');
        $this->load->library('facebook');
		$this->load->model('admincp_login_facebook_model','model');
		
	}
	
	public function connect_facebook(){
		 $accessToken = $this->input->get('accessToken');
		 $data['accessToken'] = $accessToken;
		 $this->load->view('FRONTEND/connect_facebook',$data);
	}
	
	public function connect_event(){
		 $accessToken_event = $this->input->get('accessToken');
		 $data['accessToken_event'] = $accessToken_event;
		 
		 $this->load->view('FRONTEND/connect_event',$data);
	}
	
	public function login_fb(){
        $userData = array();
        $result_url = PATH_URL;
       
        // Check if user is logged in
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            // $userProfile = $this->facebook->request('get', '1803475339964808/events'); pr($userProfile);
            $userProfile = $this->facebook->request('get', 'me?fields=id,name,accounts'); //pr($userProfile,1);
            $data['logoutUrl'] = $this->facebook->logout_url();
			
			$fb_event_list = array();
			if(isset($userProfile['accounts']['data'])){
				foreach($userProfile['accounts']['data'] as $k => $v){
					$fb_page_list[] =  array(
						'id' => $v['id'],
						'name' => $v['name'],
					);
				}
			}
			$_SESSION['fb_page_list'] = json_encode($fb_page_list);
			$_SESSION['facebook_id'] = $userProfile['id'];
			$result_url = PATH_URL.'ajax/connect_facebook?accessToken='.$access_token;
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
		
			// pr($result_url,1);
            redirect($result_url);
    }
	
	public function get_event($page_id = ''){
        $userData = array();
        $result_url = PATH_URL.'ajax/event/';
       
        // Check if user is logged in
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $page_id.'/events'); 
            $data['logoutUrl'] = $this->facebook->logout_url();
			
			$fb_page_event_list = array();
			if(isset($userProfile['data'])){
				foreach($userProfile['data'] as $k => $v){
					$fb_page_event_list[] =  array(
						'facebook_id' => (isset($_SESSION['facebook_id'])) ? $_SESSION['facebook_id'] : '',
						'page_id' => $page_id,
						'event_id' => $v['id'],
						'description' => $v['description'],
						'end_time' => $v['end_time'],
						'name' => $v['name'],
						'place' => $v['place']['name'],
						'start_time' => $v['start_time'],
						'event_times' => json_encode($v['event_times']),
						'status' => 0,
					);
					$photo = $this->get_event_photo_id($v['id']);
					$photo_id = $photo['data'][$k]['id'];
					$image = $this->get_event_photo($photo_id);
					$fb_page_event_list[$k]['photo'] = $image['images'][$k]['source'];
					// pr($fb_page_event_list,1);
					$event_data_id = $this->model->insert_data($table='admin_nqt_get_event_from_fb',$fb_page_event_list[$k]);
					if(!empty($event_data_id)){
						$fb_page_event_list[$k]['event_data_id'] = $event_data_id;
					}
					$_SESSION['data_event'] = $fb_page_event_list;
				}
				// pr($fb_page_event_list,1);
			}
			$data = array();
			$data['fb_page_event_list'] = json_encode($fb_page_event_list);
			$data['access_token'] = $access_token;
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
            $this->load->view('FRONTEND/connect_event',$data);
    }
	
	public function save_event($event_id = ''){
		$data = array();
		$data_event_show = array();
		if(isset($_SESSION['data_event'])){
			$data_event = $_SESSION['data_event'];
			for($i = 0 ; $i < count($data_event) ; $i++){
				if( $data_event[$i]['event_id'] == $event_id ){
					$this->model->update_data($table='admin_nqt_get_event_from_fb', $data_event[$i]['event_data_id']);
					$data_event_show = $data_event[$i];					
					break;
				}
				else{
					continue;
				}
			}
		}
		$data['data_page_event'] = $data_event_show;  //pr($data,1);
		$this->load->view('FRONTEND/save_event',$data);
	}
	
	// public function load_page_event(){
		// $this->load->view('FRONTEND/save_event');
	// }

	public function get_event_photo_id($event_id = ''){
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $event_id.'/photos'); //pr($userProfile);
            
            $data['logoutUrl'] = $this->facebook->logout_url();
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
           return $userProfile;
    }
	
	public function get_event_photo($photo_id = ''){
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $photo_id.'?fields=images'); 
            
            $data['logoutUrl'] = $this->facebook->logout_url();
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
           return $userProfile;
    }
	
	public function save(){
		pr('12345678912345678',1);
		$log_arr = array(
			'location' => __FILE__ ,
			'function' => 'save event',
			'_POST' => !empty($_POST) ? $_POST : '',
		);
		debug_log_from_config($log_arr);
		$link_image = $this->input->post('url_image');  
		$name = $this->input->post('name_enAdmincp');
		$description = $this->input->post('description_enAdmincp');
		$date = $this->input->post('date');
		$time = $this->input->post('time');
		$location = $this->input->post('countryAdmincp');
		$eventtime = $this->input->post('eventtime');
		$starttime = $this->input->post('starttime');
		$endtime = $this->input->post('endtime');
		$tags = $this->input->post('tagsAdmincp');
		$tickets = $this->input->post('tickets');
		$url_blog_event = $this->input->post('url_blog_event');
		
		if( ! empty($this->input->post('thumbnail_urlAdmincp')) ) {
			$pre_url = $this->input->post('thumbnail_urlAdmincp');
			$_thumbnail_url = move_file_from_url('admin_nqt_events', $pre_url, TRUE);
		}

		if( ! empty($this->input->post('image_urlAdmincp')) ) {
			$pre_url = $this->input->post('image_urlAdmincp');
			$_image_url = move_file_from_url('admin_nqt_events', $pre_url, FALSE);
		}

		if ( empty($_thumbnail_url) || empty($_image_url) ) {
			print 'error-image.'.$this->security->get_csrf_hash();
			exit;
		}
		
		$data = array(
			'image' => $link_image,
			'name' => $name,
			'tags' => $tags,
			'location' => $location,
			'slug' => 'test_get_event_fb',
			'description' => $description,
			'date' => $date,
			'time' => $time,
			'eventtime' => $eventtime,
			'starttime' => $starttime,
			'endtime' => $endtime,
			'tickets' => $tickets,
			'url_blog_event' => $url_blog_event,
		);
		pr($data);
		
		if($this->model->insert_data($table='admin_nqt_events_lang', $data)){
			// return true;
		}
		// return false;
	}
}