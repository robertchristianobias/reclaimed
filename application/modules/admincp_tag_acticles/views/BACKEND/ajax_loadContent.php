<?php 
// Get count of tag relationships
function get_count_tag($id) {
    $ci =& get_instance();

    $class = $ci->db->query("
   		SELECT  count(admin_tag_acticles_relationships.tag_id) as count
	 	FROM admin_tag_acticles INNER JOIN admin_tag_acticles_relationships 
		ON admin_tag_acticles.id = admin_tag_acticles_relationships.tag_id
 		where admin_tag_acticles_relationships.tag_id = $id
		group by admin_tag_acticles_relationships.tag_id
    ");
    if($class->result()){
    	$class = $class->result();
    	foreach($class as $r_class){
    	  $count = $r_class->count;
    	  return $count;
    	}

      } else {
    	return '0';
    }

}

?>



					<script type="text/javascript">token_value = '<?=$this->security->get_csrf_hash()?>';</script>
					<div class="dataTables_wrapper no-footer">
						<div class="table-scrollable">
							<table class="table table-striped table-bordered table-hover dataTable no-footer">
								<thead>
									<?php
										// if(isset($this->lang->languages)){
										// 	$all_lang = $this->lang->languages;
										// }else{
										// 	$all_lang = array(
										// 		'' => ''
										// 	);
										// }
									?>
									<tr role="row">
										<th class="center sorting_disabled" width="35">No.</th>
										<th class="table-checkbox sorting_disabled" width="25">Select</th>
										
									
										
										<th class="center sorting_disabled" width="35">Tags</th>
										<th class="center sorting_disabled" width="35">Slug</th>
										<th class="center sorting_disabled" width="35">Count</th>
										<th class="center sorting_disabled" width="35">Status</th>
																				
									</tr>
								</thead>
								<tbody>
									<?php
										if($result){
											$i=0;
											foreach($result as $k=>$v){
												
												
									?>
									<tr class="item_row<?=$i?> gradeX <?php echo ($k%2==0) ?  'odd' :  'even' ?>" role="row">
										<td class="center"><?=$k+1+$start?></td>
										<td><input type="checkbox" id="item<?=$i?>" onclick="selectItem(<?=$i?>)" value=""></td>
										<td><a href=""><?=$v->name?></a></td>
										<td><a href=""><?=$v->slug?></a></td>
										<td class="center"><?php echo get_count_tag($v->id) ?></td> 
										<td class="center" id="loadStatusID_<?=$v->id?>"><a class="no_underline" href="javascript:void(0)" onclick="updateStatus(<?=$v->id?>,<?=$v->status?>,'<?=$module?>')"><?php echo ($v->status==0) ?  '<span class="label label-sm label-default">Blocked</span>' :  '<span class="label label-sm label-success">Approved</span>' ?></a>
												</td>			
									</tr>
									<?php $i++;}}else{ ?>
									<tr class="gradeX odd" role="row">
										<td class="center no-record" colspan="20">No record</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>

						<?php if($result){ ?>
						<div class="row">
							<div class="col-md-5 col-sm-12">
							<!-- 	<?php if(($start+$per_page)<$total){ ?>
								<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$start+$per_page?> of <?=$total?> entries</div>
								<?php }else{ ?>
								<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$total?> of <?=$total?> entries</div>
								<?php } ?> -->
							</div>

							<div class="col-md-7 col-sm-12">
								<div class="dataTables_paginate paging_bootstrap_full_number" style="margin-top:3px">
									<ul class="pagination" style="visibility: visible;">
										<?php //echo $this->adminpagination->create_links();?>
									</ul>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>