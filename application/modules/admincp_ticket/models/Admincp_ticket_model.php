<?php
class Admincp_ticket_model extends CI_Model {
	private $module = 'admincp_ticket';
	private $table = 'admin_nqt_ticket';
	private $table_lang = 'admin_nqt_ticket_lang';
	private $field_parent_id = 'ticket_id';

	function getsearchContent($limit = 0, $page = -1){
		$result = false;
		$main_table = $this->table;
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*, admin_nqt_events.name_cn AS event_name_cn");
           $this->db->select("{$main_table}.*, admin_nqt_events.name_en AS event_name_en");
           $this->db->select("{$main_table}.*, countries.name AS country");
           $this->db->select("{$main_table}.*, type_ticket.name AS type_ticket");
        }
		
		$this->db->from($main_table);
		$this->db->join('admin_nqt_events',"{$main_table}.event_id = admin_nqt_events.id", 'left'); // Force LEFT JOIN to keep data of main table
		$this->db->join('countries',"{$main_table}.country_id = countries.id", 'left'); // Force LEFT JOIN to keep data of main table
		$this->db->join('type_ticket',"{$main_table}.type_ticket_id = type_ticket.id", 'left'); // Force LEFT JOIN to keep data of main table
		
		$content = $this->input->post('content');
		if(!empty($content)){
			$search_condition_arr = array(
				"{$main_table}.name LIKE '%{$content}%'",
			);
			$search_condition = implode($search_condition_arr, ' OR ');
			$search_condition = "( {$search_condition} )";
			$this->db->where($search_condition);
		}
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');
		if(!empty($dateFrom) || !empty($dateTo)){
			$datetimeFrom = date('Y-m-d 00:00:00',strtotime($dateFrom));
			$datetimeTo = date('Y-m-d 23:59:59',strtotime($dateTo));
			if(empty($dateFrom)){
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			} else if(empty($dateTo)){
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
			} else {
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			}
		}
		// End - Search condition
		
		// Begin - Filter condition
		$filter1 = (int)$this->input->post('filter1');
		if(!empty($filter1)){
			$this->db->where("{$main_table}.listname_id = '{$filter1}'");
		}
		// End - Filter condition
		
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		return $result;
	}
	
	function getDetailManagement($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);
		if($query->result()){
			$result = $query->row();
			return $result;
		}else{
			return false;
		}
	}
	
	function saveManagement($fileName=''){
		//default data
		$status = ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$created = $_updated = date('Y-m-d H:i:s',time());
        $event_id = (int)$this->input->post('event_idAdmincp');
        $category_id = (int)$this->input->post('category_idAdmincp');
        $type_ticket_id = (int)$this->input->post('typeTicket_idAdmincp');
        $ticket_name = $this->input->post('nameTicketAdmincp');
        $country_id = $this->input->post('country');
		$from_time = $this->input->post('from_time');
		$to_time = $this->input->post('to_time');
		$price = (int)$this->input->post('priceAdmincp');
		$total = (int)$this->input->post('totalAdmincp');
		
		$data = array(
			'status'=> $status,
			'event_id'=>$event_id,
			'category_id'=> $category_id,
			'type_ticket_id'=> $type_ticket_id,
			'name'=> $ticket_name,
			'country_id'=> $country_id,
			'from_time'=> $from_time,
			'to_time'=> $to_time,
			'price'=> $price,
			'total'=> $total,
			'created' =>$created,
		);
		//DO INSERT DATA
		if(empty($update_id)) // ADD NEW
		{
			if($this->db->insert(PREFIX.$this->table,$data))
			{
				$insert_id = $this->db->insert_id();				
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}
		else // MODIFY
		{
			$result = $this->getDetailManagement($update_id);
			unset($data['created']);
			
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data))
			{
				return true;
			}
			return false;
		}
	}
	
	function softDeleteData ($id) {
		$data ['is_delete'] = 1;
		$this->db->where('id', $id);
		if ($this->db->update (PREFIX.$this->table, $data)) {
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			//Soft-delete data in table language
			$this->db->where ($this->field_parent_id, $id);
			$this->db->update (PREFIX.$this->table_lang, $data);
			return TRUE;
		}
		return FALSE;
	}
	
	function checkData($name,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('name'.$lang,$name);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	function getAllData(){
		$this->db->select('id, name_en, name_vi');
		$this->db->where('status',1);
		#check soft delete
		$this->db->where('is_delete', 0);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_event_list(){
		$this->db->select('*');
		$this->db->from('admin_nqt_events');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_category_list(){
		$this->db->select('*');
		$this->db->from('admin_nqt_category');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_country(){
		$this->db->select('*');
		$query = $this->db->get('countries');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	public function type_ticket_list(){
		$this->db->select('*');
		$query = $this->db->get('type_ticket');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
}