<script type="text/javascript">token_value = '<?=$this->security->get_csrf_hash()?>';</script>
<div class="dataTables_wrapper no-footer">
	<div class="table-scrollable">
		<table class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<tr role="row">
					<th class="center sorting_disabled" width="35">No.</th>
					<th class="sorting" onclick="sort('name')" id="function" width="150">Event Name</th>
					<th class="sorting" onclick="sort('price')" id="function" >Price</th>
					<th class="sorting" onclick="sort('ticket_total')" id="function" >Total</th>
					<th class="sorting" onclick="sort('ticket_stock')" id="function" >Stock</th>
					<th class="center sorting" onclick="sort('from_time')">From</th>
					<th class="center sorting" onclick="sort('to_time')">To</th>					
				</tr>
			</thead>
			<tbody>
				<?php
					if($result){
						$i=0;
						foreach($result as $k=>$v){
				?>
				<tr class="item_row<?=$i?> gradeX <?php echo($k%2==0) ? 'odd' : 'even' ?>" role="row">
					<td class="center"><?=$k+1+$start?></td>
					<td class="center"><?=$v->event_name?></td>
					<td class="center"><?=$v->price?>
					<td class="center"><?=$v->ticket_total?>
					<td class="center"><?=$v->ticket_stock?>
					<td class="center"><?=date('d-m-Y H:i:s',strtotime($v->from_time))?>
					<td class="center"><?=date('d-m-Y H:i:s',strtotime($v->to_time))?>
					</td>
				</tr>
				<?php 
					$i++;
					}
				}else{ 
				?>
				<tr class="gradeX odd" role="row">
					<td class="center no-record" colspan="20">No record</td>
				</tr>
				<?php 
				} 
				?>
			</tbody>
		</table>
	</div>

	<?php 
	if($result){ 
	?>
	<div class="row">
		<div class="col-md-5 col-sm-12">
			<?php 
			if(($start+$per_page)<$total){ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$start+$per_page?> of <?=$total?> entries</div>
			<?php 
			}else{ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$total?> of <?=$total?> entries</div>
			<?php 
			} 
			?>
		</div>

		<div class="col-md-7 col-sm-12">
			<div class="dataTables_paginate paging_bootstrap_full_number" style="margin-top:3px">
				<ul class="pagination" style="visibility: visible;">
					<?=$this->adminpagination->create_links();?>
				</ul>
			</div>
		</div>
	</div>
	<?php 
	} 
	?>
</div>