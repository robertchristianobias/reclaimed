<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places"></script>

<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">
<?php
	if(isset($this->lang->languages)){
		$all_lang = $this->lang->languages;
	}else{
		$all_lang = array(
			'' => ''
		);
	}
?>
<script type="text/javascript">

function save(){
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}

function showRequest(formData, jqForm, options) {
	var form = jqForm[0];
	<?php if(empty($id)) { ?>
	if (
		form.event_idAdmincp.value == '' || 
		form.category_idAdmincp.value == '' || 
		form.country.value == '' || 
		form.from_time.value == '' || 
		form.to_time.value == ''
	) {
		$('#txt_error').html('Please enter information.');
		show_perm_denied();
		return false;
	}
	<?php } ?>
}

function showResponse(responseText, statusText, xhr, $form) {
	responseText = responseText.split(".");
	token_value  = responseText[1];
	$('#csrf_token').val(token_value);
	if(responseText[0]=='success'){
		<?php if($id==0){ ?>
		location.href=root+module+"/#/save";
		<?php }else{ ?>
		if($('.form-upload').val() != ''){
			$.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
				var res = data.split("src=");
				$('.fileinput-filename').html('');
				$('.fileinput').removeClass('fileinput-exists');
				$('.fileinput').addClass('fileinput-new');
			});
		}
		show_perm_success();
		<?php } ?>
	}
	
	if(responseText[0]=='error-image'){
		$('#txt_error').html('Only upload image.');
		show_perm_denied();
		return false;
	}
	
	<?php foreach($all_lang as $key=>$val){ ?>
	if(responseText[0]=='error-name<?php echo ($key!='') ?  '-'.$key :  '' ?>-exists'){
		$('#txt_error').html('name<?php echo ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '' ?> already exists.');
		show_perm_denied();
		$('#name<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp').focus();
		return false;
	}
	
	if(responseText[0]=='error-slug<?php echo ($key!='') ?  '-'.$key :  '' ?>-exists'){
		$('#txt_error').html('Slug<?php echo ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '' ?> already exists.');
		show_perm_denied();
		$('#slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp').focus();
		return false;
	}
	<?php } ?>

	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
}

</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
		<li><a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a><i class="fa fa-angle-right"></i></li>
		<li><?php echo ($this->uri->segment(4)=='') ?  'Add new' :  'Edit' ?></li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Form Information
				</div>
			</div>
			
			<div class="portlet-body form">
				<div class="form-body notification" style="display:none">
					<div class="alert alert-success" style="display:none">
						<strong>Success!</strong> The page has been saved.
					</div>
					
					<div class="alert alert-danger" style="display:none">
						<strong>Error!</strong> <span id="txt_error"></span>
					</div>
				</div>
				
				<!-- BEGIN FORM-->
				<form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-9">
								<div class="checkbox-list">
									<label class="checkbox-inline">
										<div class="checkbox"><span><input <?=( ! empty($result->status) && ($result->status == 1) ) ? 'checked' : 'checked'?> type="checkbox" name="statusAdmincp"></span></div>
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Events <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="event_idAdmincp" id="groupAdmincp">
									<?php
									if(!empty($event_list)){
										foreach($event_list as $event){
									?>
									<option 
										<?php echo ( isset($result->event_id) && ($result->event_id == $event->id) ) ? 'selected' : ''; ?> 
										value="<?=$event->id?>">
											<?=$event->name_cn?><?='--'?>
											<?=$event->name_en?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Category <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="category_idAdmincp" id="groupAdmincp">
									<?php
									if(!empty($category_list)){
										foreach($category_list as $category){
									?>
									<option 
										<?php echo ( isset($result->category_id) && ($result->category_id == $category->id) ) ? 'selected' : ''; ?> 
										value="<?=$category->id?>">
											<?=$category->name_cn?><?='--'?>
											<?=$category->name_en?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Type Ticket <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="typeTicket_idAdmincp" id="groupAdmincp">
									<?php
									if(!empty($type_ticket_list)){
										foreach($type_ticket_list as $type){
									?>
									<option 
										<?php echo ( isset($result->type_ticket_id) && ($result->type_ticket_id == $type->id) ) ? 'selected' : ''; ?> 
										value="<?=$type->id?>">
											<?=$type->name?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Name Ticket <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								
								<input data-required="1" type="text" class="form-control" 
									name="nameTicketAdmincp" 
									id="nameTicketAdmincp" 
									value="<?php echo(isset($result->name) ? $result->name : '') ?>"
									/>							
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Country</label>
							<div class="col-md-9">
								<select name="country" id="country" class="form-control">
									<option value="0">--Choose--</option>
									<?php 
										if($list_country){
											foreach ($list_country as $country) {?>
												<option <?php if(isset($result->country_id)){ print $result->country_id == $country->id ? 'selected' : ''; } ?> value="<?=$country->id?>"><?=$country->name.' (+'.$country->phonecode.')'?></option>
									<?php	}	
										}
									?>
								</select>
							</div>
						</div>
												
						<div class="form-group">
							<label class="control-label col-md-3">Release dates</label>
							<div class="col-md-3">
								<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
									<input value="<?php echo(isset($result->from_time))? $result->from_time : '' ?>" type="date" name="from_time" id="from_time" class="form-control"/>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Close dates</label>
							<div class="col-md-3">
								<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
									<input value="<?php echo(isset($result->to_time))? $result->to_time : '' ?>" type="date" name="to_time" id="to_time" class="form-control"/>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Price <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="priceAdmincp" 
									id="priceAdmincp" 
									value="<?php echo(isset($result->price) ? $result->price : '') ?>"
									/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Total <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="totalAdmincp" 
									id="totalAdmincp" 
									value="<?php echo(isset($result->total) ? $result->total : '') ?>"
									/>
							</div>
						</div>
						
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button onclick="save()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save</button>
								<a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
							</div>
						</div>
					</div>

				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->