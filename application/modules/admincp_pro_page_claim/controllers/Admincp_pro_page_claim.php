<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_pro_page_claim extends MX_Controller {

	private $module = 'admincp_pro_page_claim';
	private $table = 'admin_nqt_mailing_list_member';
	function __construct(){
		parent::__construct();
		$this->load->model($this->module.'_model','model');
		$this->load->model('admincp_modules/admincp_modules_model');
		$this->load->model('admincp_pro_page/admincp_pro_page_model', 'pro_page');
		$this->load->helper('main_helper.php');
		if($this->uri->segment(1)==ADMINCP){
			if($this->uri->segment(2)!='login'){
				if(!$this->session->userdata('userInfo')){
					header('Location: '.PATH_URL_ADMIN.'login');
					exit;
				}
				$get_module = $this->admincp_modules_model->check_modules($this->uri->segment(2));
				$this->session->set_userdata('ID_Module',$get_module[0]->id);
				$this->session->set_userdata('Name_Module',$get_module[0]->name);
			}
			$this->template->set_template('admin');
			$this->template->write('title','Admin Control Panel');
		}
	}
	/*------------------------------------ Admin Control Panel ------------------------------------*/
	public function admincp_index(){
		permission_force_check('r');
		$default_func = 'created';
		$default_sort = 'DESC';
		$list_user_type = $this->model->getListUserType();
		$data = array(
			'module'       =>$this->module,
			'module_name'  =>$this->session->userdata('Name_Module'),
			'default_func' =>$default_func,
			'default_sort' =>$default_sort,
			'list_user_type' => $list_user_type
		);
		$this->template->write_view('content','BACKEND/index',$data);
		$this->template->render();
	}
	
	
	public function admincp_update($id=0){
		
		redirect(PATH_URL_ADMIN.$this->module);

		if($id==0){
			permission_force_check('w');
		}else{
			permission_force_check('r');
		}
		$result[0] = array();
		if($id!=0){
			$result = $this->model->getDetailManagement($id);
		}
		$data = array(
			'result'=>$result[0],
			'module'=>$this->module,
			'id'=>$id
		);
		$this->template->write_view('content','BACKEND/ajax_editContent',$data);
		$this->template->render();
	}

	public function admincp_save(){
		$perm=permission_force_check('w');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}
		if($_POST){
			//Upload Image
			$fileName = array('image'=>'');
			if($_FILES){
				foreach($fileName as $k=>$v){
					if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
						$typeFileImage = strtolower(mb_substr($_FILES['fileAdmincp']['type'][$k],0,5));
						if($typeFileImage == 'image'){
							$tmp_name[$k] = $_FILES['fileAdmincp']["tmp_name"][$k];
							$file_name[$k] = $_FILES['fileAdmincp']['name'][$k];
							$ext = strtolower(mb_substr($file_name[$k], -4, 4));
							if($ext=='jpeg'){
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-5))).'.jpg';
							}else{
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-4))).$ext;
							}
						}else{
							print 'error-image.'.$this->security->get_csrf_hash();
							exit;
						}
					}
				}
			}
			//End Upload Image

			if($this->model->saveManagement($fileName)){
				//Upload Image
				if($_FILES){
					if($_FILES){
						$upload_path = BASEFOLDER.DIR_UPLOAD_NEWS;
						check_dir_upload($upload_path);
						foreach($fileName as $k=>$v){
							if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
								move_uploaded_file($tmp_name[$k], $upload_path.$fileName[$k]);
							}
						}
					}
				}
				//End Upload Image
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_delete(){
		$perm=permission_force_check('d');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$result = $this->model->getDetailManagement($id);
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			$this->db->where('id',$id);
			if($this->db->delete($this->table)){
				//Xóa hình khi Delete
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result[0]->image);
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_import(){
		$default_func = 'created';
		$default_sort = 'DESC';
		$data = array(
			'module'=>$this->module,
			'default_func'=>$default_func,
			'default_sort'=>$default_sort,
			'is_normal_template'=>false,
		);
		$this->template->write_view('content','BACKEND/import_pro_page',$data);
		$this->template->render();
	}
	
	public function admincp_ajaxLoadContent(){
		$this->load->library('AdminPagination');
		$config['total_rows'] = $this->model->getsearchContent();
		$config['per_page'] = (int)$this->input->post('per_page');
		$config['start'] = (int)$this->input->post('start');
		$config['num_links'] = 3;
		$config['func_ajax'] = 'searchContent';
		$this->adminpagination->initialize($config);

		$result = $this->model->getsearchContent($config['per_page'],$this->input->post('start'));
		
		$list_user_type = $this->model->getListUserType();
		$data = array(
			'result'=>$result,
			'per_page'=>$this->input->post('per_page'),
			'start'=>$this->input->post('start'),
			'module'=>$this->module,
			'total'=>$config['total_rows'],
			'list_user_type' => $list_user_type
		);
		$this->session->set_userdata('start',$this->input->post('start'));
		$this->load->view('BACKEND/ajax_loadContent',$data);
	}
	
	public function admincp_ajaxUpdateStatus(){
		$perm=permission_force_check('a');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}else{
			
			modules::run('admincp/saveLog',$this->module,$this->input->post('id'),'update',$this->input->post('status'),$status);
			$this->db->where('id', $this->input->post('id'));
			$this->db->update($this->table, $data);
		}
		
		$update = array(
			'status'=>$status,
			'id'=>$this->input->post('id'),
			'module'=>$this->module
		);
		$this->load->view('BACKEND/ajax_updateStatus',$update);
	}
	
	public function admincp_ajaxGetImageUpdate($id){
		$result = $this->model->getDetailManagement($id);
		print resizeImage(PATH_URL.DIR_UPLOAD_NEWS.$result[0]->image,250);exit;
	}
	/*------------------------------------ End Admin Control Panel --------------------------------*/
	
	function admincp_approve() {
		$result = array();
		$status = 0;
		$message = 'error';
		$data = array();
		
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$url_of_page = $this->input->post('url_of_page');
		$action = $this->input->post('action');

		$actions = array('approve', 'reject');
		if ( ! in_array($action, $actions)) {
			$message = 'action is invalid';
			$this->set_result($status, $message);
		}
		if (empty($email)) {
			$message = 'email empty';
			$this->set_result($status, $message);
		}
		if (empty($url_of_page)) {
			$message = 'url empty';
			$this->set_result($status, $message);
		}
		//verify request
		$data_check = $this->model->verifyId($id, $email, $url_of_page);
		if (empty($data_check)) {
			$message = 'data is invalid';
			$this->set_result($status, $message);
		}
		$user_id = $this->pro_page->getUserIdByEmail($email);
		//check email exists
		if (empty($user_id)) {
			$message = 'email not exists';
			$this->set_result($status, $message);
		}

		if ($action == 'approve') {
			//check url exists
			if ($this->pro_page->checkProPageExists($url_of_page)) {
				$message = 'url exists';
				$this->set_result($status, $message);
			}
			$user_type_id = isset($data_check ['user_type_id']) ? $data_check ['user_type_id'] : '';
			$genre = isset($data_check ['genre']) ? $data_check ['genre'] : '';
			$rs = $this->do_approve($id, $email, $user_id, $url_of_page, $user_type_id, $genre);
			if($rs === TRUE) {
				$status = 1;
				$message = 'approve success';
			} 
			else {
				$message = 'Can not approve';
				$this->set_result($status, $message);
			}
		} 
		else if ($action == 'reject') {
			$this->do_reject($id, $email);
			$status = 1;
			$message = 'reject success';
		}
		// $status = 1;
		$data ['id'] = $id;
		$data ['action'] = $action;
		$data ['email'] = $email;
		$data ['url_of_page'] = $url_of_page;
		// $data ['send_email_approve'] = ($send_email_result) ? 'success' : 'fail';
		$this->set_result($status, $message, $data);
	}

	function do_approve($id, $email, $user_id, $url_of_page, $user_type_id, $genre) {
		
		$approve_status = PROPAGE_STT_APPROVED;
		//do insert approve
		$data_insert = array();
		$data_insert ['email'] = $email; 
		$data_insert ['user_id'] = $user_id; 
		$data_insert ['url_of_page'] = $url_of_page; 
		$data_insert ['user_type_id'] = $user_type_id; 
		$data_insert ['genre'] = $genre; 
		$data_insert ['approve_status'] = $approve_status; 
		$data_approve = $this->pro_page->insertApprovedProPage($data_insert);
		// $data_approve = $this->pro_page->insertApproveProPage($email, $user_id, $url_of_page, $user_type_id, $approve_status);
		if (empty($data_approve)) {
			return FALSE;
		}
		//send email
		$send_email_result = $this->send_email_approve($email, $data_approve ['token']);
		$approve_id = $data_approve ['id'];
		$approve_status = PROPAGE_STT_APPROVED;
		$this->model->updateDataAfterApproved($id, $approve_id, $approve_status, $send_email_result);
		return TRUE;
	}

	function do_reject($id, $email) {
		//send email
		$send_email_result = $this->send_email_reject($email);
		// $approve_id = $data_approve ['id'];
		$reject_status = PROPAGE_STT_REJECTED;
		$this->model->updateRejected($id, $reject_status, $send_email_result);
		return TRUE;
	}

	function send_email_approve($email, $token) {
		$subject = 'Your claim has been approved - REDM - ' . date('Y-m-d H:i:s');
		$body = "<center> " . 'Your request to create a pro page has been accepted, starting from now on you can use the pro page' . " </center>";
		
		$send_email_approve = ses_send_mail($subject, $body, $email);
		
		return ($send_email_approve) ? TRUE : FALSE;
	}

	function send_email_reject($email) {
		$subject = 'Your claim has been rejected - REDM - ' . date('Y-m-d H:i:s');
		$body = "<center> Your claim has been rejected </center>";
		
		$send_email_reject = ses_send_mail($subject, $body, $email);
		
		return ($send_email_reject) ? TRUE : FALSE;

	}

	function set_result($status, $message, $data = array()) {
		$result = array();
		$result ['status'] = $status;
		$result ['message'] = $message;
		$result ['data']= $data;
		echo json_encode($result); exit;

	}
}