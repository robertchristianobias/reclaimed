<?php
class Admincp_show_model extends CI_Model {
	private $module = 'admincp_show';
	private $table = 'admin_nqt_show';

	function getsearchContent($limit=0,$page=-1){
		$this->db->select('admin_nqt_show.*,admin_nqt_events.name as name_event');
		if($limit > 0){
			$this->db->limit($limit,$page);
			$this->db->order_by($this->input->post('func_order_by'),$this->input->post('order_by'));
		}
		if($this->input->post('content')!='' && $this->input->post('content')!='type here...'){
			$this->db->where('(`admin_nqt_show.name` LIKE "%'.$this->input->post('content').'%" OR `admin_nqt_events.name` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('admin_nqt_show.created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('admin_nqt_show.created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('admin_nqt_show.created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('admin_nqt_show.created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		$this->db->join('admin_nqt_events', 'admin_nqt_events.id = admin_nqt_show.event_id', 'left');
		//category
		$fillter1 = $this->input->post('fillter1');
		if(!empty($fillter1)){
			$this->db->where('event_id',$fillter1);
		}
		$query = $this->db->get($this->table);
		// $sql=$this->db->last_query();
		// pr($sql);
		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	function get_event(){
		$this->db->select('*');
		$this->db->where('status',1);
		$kq=$this->db->get('admin_nqt_events')->result_array();
		$cate_mapping=array();
		foreach($kq as $k){
			$cate_mapping[$k['id']]=$k['name'];
		}
		return $cate_mapping;

	}
	
	
}