<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stripe_demo_model extends MY_Model {

private $module = 'stripe_demo';
    private $token;
    //private $table_menu = '';
    //private $lang_code = '';


    function __construct()
    {
        parent::__construct();
    }
      public function stripe_public_key(){
        $api_path = 'stripe_public_key';
        $params['token'] = $this->token;
        // pr($this->token,1);
        $json_response = post_data_to_api($api_path, $params, 'json');
        $url = get_result($json_response);
        return $url;
    }

    public function stripe_connect_pro_url(){
        $api_path = 'stripe_connect_pro_url';
        $params['token'] = $this->token;
        $json_response = post_data_to_api($api_path, $params, 'json');
        $url = get_result($json_response);
        return $url;
    }
    public function checkStripeIdOfPro(){
        $api_path = 'check_stripe_user_pro';
        $params['token'] = $this->token;
        $json_response = post_data_to_api($api_path, $params, 'json');
        if(get_result($json_response) == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
    public function insertStripeToken($params){
        $api_path = 'stripe_connect_user';
        $params['token'] = $this->token;
        // pr($params,1);
        $json_response = post_data_to_api($api_path, $params, 'json');
        if(is_success($json_response)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    public function unlinkStripeCustomer($params){
        $api_path = 'unlink_stripe';
        // pr($params,1);
        $json_response = post_data_to_api($api_path, $params, 'json');
        pr($json_response, 1);
        if(is_success($json_response)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    public function load_list_user_info($id){
        $this->db->select("*");
        $this->db->where('id',$id);
        $query_list = $this->db->get("admin_nqt_user_info");
        return $query_list->result();
    }

    // public function insertStripe($userId, $req){
    // $result = false;
    // if(!empty($req['stripe_user_id'])){
    //   $stripe_user_id = $req['stripe_user_id'];
    //
    //   // First - Check duplicate user_stripe_id (Could not check duplicate bank card)
    //   $this->db->select('stripe_user_id');
    //   $this->db->where('stripe_user_id',$stripe_user_id);
    //   $query = $this->db->get('admin_nqt_user_stripe');
    //   $check_obj = $query->row_array();
    //   if(empty($check_obj)){
    //     $date = new DateTime ();
    //     $create_date = $date->format ( 'Y-m-d H:i:s' );
    //     $data = array(
    //       'user_id' => $userId,
    //       'stripe_publishable_key' => $req['stripe_publishable_key'],
    //       'stripe_user_id' => $stripe_user_id,
    //       'create_date' => $create_date,
    //     );
    //     $this->db->trans_start ();
    //     $this->db->insert( 'admin_nqt_user_stripe', $data );
    //     $this->db->trans_complete ();
    //     $result = true;
    //   }
    // }
    // return $result;
    // }
    public function insertUserStripeIdByUser($user_id, $user_stripe_id){
    $result = false;
    if(!empty($user_id) && !empty($user_stripe_id)){
      // First - Check duplicate user_stripe_id (Could not check duplicate bank card)
      $this->db->select('user_stripe_id');
      $this->db->where('user_stripe_id',$user_stripe_id);
      $query = $this->db->get('admin_nqt_user_payment');
      $check_obj = $query->row_array();
      if(empty($check_obj)){
        $date = new DateTime ();
        $create_date = $date->format ( 'Y-m-d H:i:s' );
        $data = array(
          'user_id' => $user_id,
          'user_stripe_id' => $user_stripe_id,
          'create_date' => $create_date
        );
        $this->db->trans_start ();
        $this->db->insert( 'admin_nqt_user_payment', $data );
        $this->db->trans_complete ();
        $result = true;
      }
    }
    return $result;
    }

}
