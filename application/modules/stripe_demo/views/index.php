<!DOCTYPE html>
<html>
<head>
<?php
pr($this->session->get_userdata());
    $lang= 'en';
 ?>
    <title>REDM</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="habfit">
    <meta name="keywords" content="habfit">
</head>
<body>
                        <?php
                            $now = getdate();
                            // $month = $now["mon"];
                            $month = 1;
                            $year =  $now["year"];
                            $minYear = $now["year"]-3; // Not used?
                            $maxYear = $now["year"]+20;
                        ?>
                 <div id="myModal" class="modal fade" role="dialog" action="">
                    <form action="<?=base_url(AJAX_PREFIX . '/payment/submit')?>" method="POST" id="payment-form" class="form-horizontal">
                        <section id="payment">
                            <div class="form-group">
                                <label>CARD HOLDER'S NAME</label><br/>
                                <input type="text" name="cardholdername" class="card-holder-name form-control" placeholder="Card Holder's Name" ><br/>
                            </div>
                            <div class="form-group">
                                <label>CARD EXPIRY DATE</label><br/>
                                <select name="select2" data-stripe="exp_month" class="card-expiry-month stripe-sensitive required form-control">
                                    <?php
                                        for($i=$month; $i<=12; $i++){
                                            ?>
                                                <option value="<?php echo $i;?>" ><?php echo $i;?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                                <span>/</span>
                                <select name="select2" data-stripe="exp_year" class="card-expiry-year stripe-sensitive required form-control">
                                    <?php
                                      for($i=$year; $i<=$maxYear; $i++){
                                            ?>
                                                <option value="<?php echo $i;?>" ><?php echo $i;?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>CARD NUMBER</label><br/>
                                <input type="text" name="cardnumber" placeholder="Card Number" class="card-number form-control" ><br/>
                            </div>
                            <div class="form-group" align="">
                                <label>CVV/CVV2</label><br/>
                                <input type="text" name="cvv" placeholder="CVV/CVV2" maxlength="4" value="000" class="card-cvc form-control"><br/>
                            </div>
                             <div class="payment-popup-button form-group">
                                <button type="submit"  class="btn btn-primary">Register</button>

                            </div>
                        </section>
                    </form>
                </div>


    <!-- call js page -->
<script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/slick/slick.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/nicescroll/nicescroll.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/style.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fancybox.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.mousewheel-3.0.6.pack.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/helper.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_comment.js')?>"></script>
    <script src="<?=get_resource_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.validate.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.numeric.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/bootstrap.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fitvids.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/admin/jquery.form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/ajax_submit_form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page_comment.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/croppic.css')?>">
    <script src="<?=get_resource_url('assets/js/croppic.js')?>"></script>


<!--     <script src="<?=get_resource_url('assets/js/jquery-1.11.3.min.js?r=1477453687')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/jquery.mousewheel-3.0.6.pack.js?r=1477453685')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fancybox.js?r=1477453687')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/jquery-ui.js?r=1477453687')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/jquery.validate.min.js?r=1477453687')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.numeric.min.js?r=1477453687')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/DateTimePicker.js?r=1477453687')?>" type="text/javascript"></script>
    <script src="<?=get_resource_url('assets/js/bootstrap.min.js?r=1477453685')?>"></script>
    <script src="<?=get_resource_url('assets/js/home_user.js?r=1488267759')?>"></script> -->

        <style type="text/css">
            #cropic_element {
                height: 200px;
                width: 200px;
            }
            .width-select{
                width: 150px !important;
                height: 30px !important;
                font-weight: 400;

            }

        </style>

<!--stripe-->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="<?=get_resource_url('assets/js/bootstrapValidator-min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    //return;
    $('#payment-form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
                    // createToken returns immediately - the supplied callback submits the form if there are no errors
                    Stripe.card.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val(),
            name: $('.card-holder-name').val()
                    }, stripeResponseHandler);
                    return false; // submit from callback
        },
        fields: {
            cardholdername: {
                validators: {
                    notEmpty: {
                        message: 'The card holder name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 70,
                        message: 'The card holder name must be more than 6 and less than 70 characters long'
                    }
                }
            },
            cardnumber: {
        selector: '#cardnumber',
                validators: {
                    notEmpty: {
                        message: 'The credit card number is required and can\'t be empty'
                    },
                    creditCard: {
                        message: 'The credit card number is invalid'
                    },
                }
            },
            expMonth: {
                selector: '[data-stripe="exp-month"]',
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value > currentMonth)) {
                                validator.updateStatus('expYear', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                selector: '[data-stripe="exp-year"]',
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 100) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month > currentMonth)) {
                                validator.updateStatus('expMonth', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            cvv: {
        selector: '#cvv',
                validators: {
                    notEmpty: {
                        message: 'The cvv is required and can\'t be empty'
                    },
                    cvv: {
                        message: 'The value is not a valid CVV',
                        creditCardField: 'cardnumber'
                    }
                }
            },
        }
    });
});


// iFrame - Popup Communication
if(window.postMessage) {
    if(window.addEventListener) {
        window.addEventListener("message", popup_callback, false);
    } else if(window.attachEvent) {
        window.attachEvent("onmessage", popup_callback);
    }
}
function popup_callback(event) {
    if(location.href.indexOf(event.origin) == 0){
        var data = event.data;
        if(data && data.message && data.message.indexOf('REDM_') == 0){
            if(popup_window){
                popup_window.close();
            }
            if(data.message == 'REDM_sucess'){
                alert('Link Stripe account successfully');
                location.reload();
            } else {
                alert('Link Stripe account failed!');
            }
        }
    }
}

var popup_window;
function popup_connect_with_stripe(){
    <?php
    if(!empty($stripe_connect_pro_url)){
    ?>
    popup_window = window.open("<?=$stripe_connect_pro_url?>", "Link Stripe Account Popup", "");
    <?php
    }
    ?>
    return false;
}

</script>
<script type="text/javascript">
            Stripe.setPublishableKey('<?=STRIPE_PUBLIC_KEY?>');

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    $('.submit-button').removeAttr("disabled");
                    // show hidden div
                    document.getElementById('a_x200').style.display = 'block';
                    // show the errors on the form
                    $(".payment-errors").html(response.error.message);
                } else {
                    var form$ = $("#payment-form");
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    //form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");

                    //$('#stripe_token_value').val(token);
                    var url = form$.attr('action');
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: {stripe_token: token},
                        success: function(data){
                            var obj = getJSONData(data);
                            if(obj != false){
                                if(obj.status == 1){
                                    alert('Link bank account successfully');
                                    location.reload();
                                    // $("#myModal").hide();
                                    // $("#button_account").hide();
                                } else {
                                    alert('Link bank account failed!');
                                }
                            }
                        }
                    });
//                    form$.get(0).submit();
                }
            }
</script>
</body>
</html>
