<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stripe_demo extends MX_Controller {

    private $module = 'stripe_demo';
    private $token;

public function __construct()
{
        $this->load->library('session');
         $this->load->model('Stripe_demo_model','model');
}
public function index()
{
    $this->load->view('index');
}
  private function _check_login()
    {
        if ( ! $this->session->userdata('token')) {
            redirect(base_url('/'));
        }
    }
 public function payment()
    {
        $this->_check_login();

        // Connect Pro into Customer
        $check_stripe = $this->model->checkStripeByToken();
        $data['check_stripe'] = $check_stripe;
        if(empty($check_stripe)){
            $result = $this->model->stripe_public_key();
            if(!empty($result->stripe_public_key)){
                $stripe_public_key = $result->stripe_public_key;
                $data['stripe_public_key'] = $stripe_public_key;
            }
        }

        // Connect Pro into Standalone Account
        $check_stripe_ofPro = $this->model->checkStripeIdOfPro();
        /*  if(empty($check_stripe_ofPro)){
            $result = $this->model->stripe_connect_pro_url();
            if(!empty($result->url)){
                $stripe_connect_pro_url = $result->url;
                $data['stripe_connect_pro_url'] = $stripe_connect_pro_url;
            }
        } */
        $result = $this->model->stripe_connect_pro_url();
        if(!empty($result->url)){
            $stripe_connect_pro_url = $result->url;
            $data['stripe_connect_pro_url'] = $stripe_connect_pro_url;
        }
        $data['check_stripe_ofPro'] = $check_stripe_ofPro;
        $url_insert_token = base_url('insertStripe');
        $data['url_insert_token'] = $url_insert_token;
        $data['unlink_url'] = base_url('payment/unlink-stripe');
        $this->load->view('FRONTEND/payment', $data);
    }
    public function insertStripeId(){
        $rs  = TRUE;
        $params['stripeToken'] = $this->input->post('stripe_token');
        $rs = $this->model->insertStripeToken($params);
        if ($rs !== FALSE) {
            $result['status'] = STATUS_SUCCESS;
            $result['message'] = 'Connect user successful';
        }
        else {
            $result['status'] = STATUS_FAIL;
            $result['message'] = 'Connect user failed';
        }
        return_json($result);
    }
    public function stripeConnectUser(){
      $data ['responseCd'] = NULL;
  		$data ['responseMsg'] = NULL;
        $stripetoken = $this->input->post['token'];
        $userData = $this->session->get_userdata();
        //pr($userData,1);
        $user_id = $userData['userData']['id'];
        $result = $this->model->load_list_user_info($user_id);
        if ($this->input->method ( TRUE ) === 'POST') {
            try
            {
              $this->load->library('Stripe');
              \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
              $customer = \Stripe\Customer::create(array(
                  $data['userDataStripe'] = array(
                  "source" => $stripetoken,
                  "email" => $result[0]->email,
                  "description" => $result[0]->username),
                  $this->session->set_userdata('userData',$data['userDataStripe'])
              ));
              if(!empty($customer)){
                $customer_id = $customer->id;
                if($this->model->insertUserStripeIdByUser($user_id, $customer_id)){
                  $data ["responseCd"] = 0;
                  $data ["responseMsg"] = "success";
                }
              }

            } catch (Exception $ex) {
                $exception_error = $ex->getMessage();
            }
      }
    }

}
