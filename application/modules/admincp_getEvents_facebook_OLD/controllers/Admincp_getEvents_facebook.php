<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_getEvents_facebook extends MX_Controller {

	private $module = 'admincp_getEvents_facebook';
	private $table = 'page_item';
	private $table_lang = 'page_item_lang';
	function __construct(){
		parent::__construct();
		$this->load->model($this->module.'_model','model');
		$this->load->model('admincp_modules/admincp_modules_model');
		$this->load->library('facebook');
		if($this->uri->segment(1)==ADMINCP){
			if($this->uri->segment(2)!='login'){
				if(!$this->session->userdata('userInfo')){
					header('Location: '.PATH_URL_ADMIN.'login');
					exit;
				}
				$get_module = $this->admincp_modules_model->check_modules($this->uri->segment(2));
				if(!empty($get_module)){
					$this->session->set_userdata('ID_Module',$get_module[0]->id);
					$this->session->set_userdata('Name_Module',$get_module[0]->name);
				}
			}
			$this->template->set_template('admin');
			$this->template->write('title','Admin Control Panel');
		}
	}
	/*------------------------------------ Admin Control Panel ------------------------------------*/
	public function admincp_index(){
		permission_force_check('r');
		$default_func = 'created';
		$default_sort = 'DESC';
		$data = array(
			'module'=>$this->module,
			'module_name'=>$this->session->userdata('Name_Module'),
			'default_func'=>$default_func,
			'default_sort'=>$default_sort
		);
		$this->template->write_view('content','BACKEND/index',$data);
		$this->template->render();
	}
	
	public function admincp_update($id=0){
		if($id==0){
			permission_force_check('w');
		}else{
			permission_force_check('r');
		}
		$result = array();
		if($id!=0){
			$result = $this->model->getDetailManagement($id);
		}
		$data = array(
			'result'=>$result,
			'module'=>$this->module,
			'id'=>$id
		);
		$this->template->write_view('content','BACKEND/ajax_editContent',$data);
		$this->template->render();
	}

	public function admincp_save(){
		permission_force_check('w');
		if($_POST){
			//Upload Image
			$fileName = array('image'=>'');
			if($_FILES){
				foreach($fileName as $k=>$v){
					if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
						$typeFileImage = strtolower(mb_substr($_FILES['fileAdmincp']['type'][$k],0,5));
						if($typeFileImage == 'image'){
							$tmp_name[$k] = $_FILES['fileAdmincp']["tmp_name"][$k];
							$file_name[$k] = $_FILES['fileAdmincp']['name'][$k];
							$ext = strtolower(mb_substr($file_name[$k], -4, 4));
							if($ext=='jpeg'){
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-5))).'.jpg';
							}else{
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-4))).$ext;
							}
						}else{
							print 'error-image.'.$this->security->get_csrf_hash();
							exit;
						}
					}
				}
			}
			//End Upload Image

			if($this->model->saveManagement($fileName)){
				//Upload Image
				if($_FILES){
					if($_FILES){
						$upload_path = BASEFOLDER.DIR_UPLOAD_NEWS;
						check_dir_upload($upload_path);
						foreach($fileName as $k=>$v){
							if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
								move_uploaded_file($tmp_name[$k], $upload_path.$fileName[$k]);
							}
						}
					}
				}
				//End Upload Image
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_ajaxLoadContent(){
		$this->load->library('AdminPagination');
		$config['total_rows'] = $this->model->getTotalsearchdescription();
		$config['per_page'] = $this->input->post('per_page');
		$config['num_links'] = 3;
		$config['func_ajax'] = 'searchContent';
		$config['start'] = $this->input->post('start');
		$this->adminpagination->initialize($config);

		$result = $this->model->getsearchdescription($config['per_page'],$this->input->post('start'));
		$data = array(
			'result'=>$result,
			'per_page'=>$this->input->post('per_page'),
			'start'=>$this->input->post('start'),
			'module'=>$this->module,
			'total'=>$config['total_rows']
		);
		$this->session->set_userdata('start',$this->input->post('start'));
		$this->load->view('BACKEND/ajax_loadContent',$data);
	}
	
	public function admincp_delete() {
		permission_force_check('d');
		if ( ! empty($this->input->post('id')) ) {
			$id = $this->input->post('id');
			
			if ($this->model->softDeleteData($id)){
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_ajaxGetImageUpdate($id){
		$result = $this->model->getDetailManagement($id);
		print resizeImage(PATH_URL.DIR_UPLOAD_NEWS.$result->image,250);exit;
	}
	/*------------------------------------ End Admin Control Panel --------------------------------*/
	
	/*BEGIN get events facebook*/
	public function connect_facebook(){
		 $accessToken = $this->input->get('accessToken');
		 $data['accessToken'] = $accessToken;
		 $this->load->view('FRONTEND/connect_facebook',$data);
	}
	
	public function connect_event(){
		 $accessToken_event = $this->input->get('accessToken');
		 $data['accessToken_event'] = $accessToken_event;
		 
		 $this->load->view('FRONTEND/connect_event',$data);
	}
	
	public function login_fb(){
		$this->config->set_item('facebook_login_redirect_url', 'ajax/login_fb_get_event_access_token');
        $userData = array();
        $result_url = PATH_URL;
       
        // Check if user is logged in
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '1549096118734197/events'); pr($userProfile,1);
            // $userProfile = $this->facebook->request('get', 'me?fields=id,name,accounts'); pr($userProfile,1);
            $data['logoutUrl'] = $this->facebook->logout_url();
			
			$fb_event_list = array();
			if(isset($userProfile['accounts']['data'])){
				foreach($userProfile['accounts']['data'] as $k => $v){
					$fb_page_list[] =  array(
						'id' => $v['id'],
						'name' => $v['name'],
					);
				}
			}
			$_SESSION['fb_page_list'] = json_encode($fb_page_list);
			$_SESSION['facebook_id'] = $userProfile['id'];
			$result_url = PATH_URL.'ajax/connect_facebook?accessToken='.$access_token;
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
		
			// pr($result_url,1);
            redirect($result_url);
    }
	
	public function get_event($page_id = ''){
        $userData = array();
        $result_url = PATH_URL.'ajax/event/';
       
        // Check if user is logged in
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $page_id.'/events'); 
            $data['logoutUrl'] = $this->facebook->logout_url();
			
			$fb_page_event_list = array();
			if(isset($userProfile['data'])){
				foreach($userProfile['data'] as $k => $v){
					$fb_page_event_list[] =  array(
						'facebook_id' => (isset($_SESSION['facebook_id'])) ? $_SESSION['facebook_id'] : '',
						'page_id' => $page_id,
						'event_id' => $v['id'],
						'description' => $v['description'],
						'end_time' => $v['end_time'],
						'name' => $v['name'],
						'place' => $v['place']['name'],
						'start_time' => $v['start_time'],
						'event_times' => json_encode($v['event_times']),
						'status' => 0,
					);
					$photo = $this->get_event_photo_id($v['id']);
					$photo_id = $photo['data'][$k]['id'];
					$image = $this->get_event_photo($photo_id);
					$fb_page_event_list[$k]['photo'] = $image['images'][$k]['source'];
					// pr($fb_page_event_list,1);
					$event_data_id = $this->model->insert_data($table='admin_nqt_get_event_from_fb',$fb_page_event_list[$k]);
					if(!empty($event_data_id)){
						$fb_page_event_list[$k]['event_data_id'] = $event_data_id;
					}
					$_SESSION['data_event'] = $fb_page_event_list;
				}
				// pr($fb_page_event_list,1);
			}
			$data = array();
			$data['fb_page_event_list'] = json_encode($fb_page_event_list);
			$data['access_token'] = $access_token;
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
            $this->load->view('FRONTEND/connect_event',$data);
    }
	
	public function save_event($event_id = ''){
		$data = array();
		$data_event_show = array();
		if(isset($_SESSION['data_event'])){
			$data_event = $_SESSION['data_event'];
			for($i = 0 ; $i < count($data_event) ; $i++){
				if( $data_event[$i]['event_id'] == $event_id ){
					$this->model->update_data($table='admin_nqt_get_event_from_fb', $data_event[$i]['event_data_id']);
					$data_event_show = $data_event[$i];					
					break;
				}
				else{
					continue;
				}
			}
		}
		$data['data_page_event'] = $data_event_show;  //pr($data,1);
		$this->load->view('FRONTEND/save_event',$data);
	}
	
	// public function load_page_event(){
		// $this->load->view('FRONTEND/save_event');
	// }

	public function get_event_photo_id($event_id = ''){
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $event_id.'/photos'); //pr($userProfile);
            
            $data['logoutUrl'] = $this->facebook->logout_url();
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
           return $userProfile;
    }
	
	public function get_event_photo($photo_id = ''){
		$access_token = $this->facebook->is_authenticated();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
			
            $userProfile = $this->facebook->request('get', $photo_id.'?fields=images'); 
            
            $data['logoutUrl'] = $this->facebook->logout_url();
        }
		
		else{
            $fbuser = '';
            // Get login URL
            $result_url = $this->facebook->login_url();
        }
			// pr($result_url,1);
           return $userProfile;
    }
	
	
	
	public function admincp_fb_url_pull_data(){	
		$response = array();
		$response['success'] = false;

		$url = $this->input->post('fb_url');
		$url = trim($url); // Important to scrape HTML
		if(!empty($url)){
			// $html = $this->file_get_contents_curl($url);
			$this->load->helper('scraper_helper');
			$flag_debug = true;
			$html = scrape_html_from_url_facebook($url, $flag_debug);
			$log_arr = array(
				'location' => __FILE__ ,
				'function' => 'admincp_fb_url_pull_data',
				'html' => !empty($html) ? $html : '',
			);
			debug_log_from_config($log_arr);
			
			//parsing begins here:
			$doc = new DOMDocument();
			@$doc->loadHTML($html);
			$nodes = $doc->getElementsByTagName('title');
			// pr('admincp_fb_url_pull_data - $nodes');
			// pr($nodes, 1);

			//get and display what you need:
			$title = $nodes->item(0)->nodeValue;
			$description = '';
			$image_url = '';

			$metas = $doc->getElementsByTagName('meta');
			for ($i = 0; $i < $metas->length; $i++)
			{
				$meta = $metas->item($i);
				if($meta->getAttribute('name') == 'description'){
					$description = $meta->getAttribute('content');
				} else if($meta->getAttribute('property') == 'og:description'){
					$description = $meta->getAttribute('content');
				}
			}
			
			// Parse DateTime/Location
			$event_date_start = '';
			$event_date_end = ''; // TODO
			$event_time_start = '';
			$event_time_end = '';
			$event_location = '';
			$event_location_country = '';
			
			
			$regular_expression = 'event_header_primary(.*?)\<img(.*?)src="(.*?)"'; // Parse Image
			$regular_expression .= '(.*?)_publicProdFeedInfo__timeRowTitle(.*?)content="(.*?)"\>\<span'; // Parse DateTime
			$regular_expression .= '(.*?)\<a class="_5xhk"(.*?)\>(.*?)\<\/a\>\<div class="_5xhp(.*?)"\>(.*?)\<\/div'; // Parse Location
			$regular_expression = "/{$regular_expression}/s"; //s for matching newlines
				
			// pr($html,1);
			preg_match($regular_expression, $html, $matches, PREG_OFFSET_CAPTURE);
			// pr($matches,1);
			
			if(isset($matches[3][0]) && isset($matches[6][0]) && isset($matches[9][0]) && isset($matches[11][0])){
				$this->load->helper('social_helper');
				
				$image_url = $matches[3][0];
				$image_url = htmlspecialchars_decode($image_url);

				// DateTime
				$datetime_str = $matches[6][0];
				$datetime_arr = parse_string_to_datetime_period($datetime_str);
				if(!empty($datetime_arr['from_datetime']) && !empty($datetime_arr['to_datetime'])){
					$from_datetime = $datetime_arr['from_datetime'];
					$to_datetime = $datetime_arr['to_datetime'];
					$event_date_start = $from_datetime->format('Y-m-d');
					$event_date_end = $to_datetime->format('Y-m-d');
					$event_time_start = $from_datetime->format('H:i');
					$event_time_end = $to_datetime->format('H:i');
				}
				
				// Location
				$location_city = $matches[9][0];
				$location_address = $matches[11][0];
				$event_location = !empty($location_address) ? $location_address : $location_city;
				// TODO - Parse Country
				$api_key = GG_API_KEY;
				$location_data = get_geocoding_by_google($api_key, $location_address);
				$event_location_country = ! empty($location_data ['country']) ? $location_data ['country'] : $location_city;
				
			}
			
			
			$obj = array();
			$obj['title'] = $title;
			$obj['description'] = $description;
			$obj['event_image'] = $image_url;
			$obj['url'] = $url;
			$obj['event_date_start'] = $event_date_start;
			$obj['event_date_end'] = $event_date_end;
			$obj['event_time_start'] = $event_time_start;
			$obj['event_time_end'] = $event_time_end;
			$obj['event_location'] = $event_location;
			$obj['event_location_country'] = $event_location_country;
			
			// pr($obj,1);
			
			$response['obj'] = $obj;
			$response['success'] = true;
		}
		
		echo json_encode($response);
	}
	function parse_number($content){
		$result = preg_replace('/[^0-9]/', '', $content);
		
		return $result;
	}
	
	function file_get_contents_curl($url){
		$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_URL,$url);
		$data = curl_exec($ch);

		return $data;
	}
	/* END get event from facebook*/
}