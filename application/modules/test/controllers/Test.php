<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MX_Controller {

    private $module = 'test';
    private $token;

	public function __construct()
	{
		$this->load->library('session');
	}
	public function fb()
	{
		$this->load->helper('social_helper');
		$this->load->library('facebook');
		
		
		$social_sdk_obj = $this->facebook;
		$callback_url = 'ajax/test_fb';
		$this->config->set_item('facebook_login_redirect_url', $callback_url);
		$callback_url = PATH_URL.$callback_url;
		$oauth_provider = 'facebook';
		$login_data = social_login($oauth_provider, $callback_url, $social_sdk_obj);
		// pr($login_data,1);
		$access_token = $login_data['access_token'];
		if(!empty($access_token)){
			// $data = $this->facebook->request('get', 'pixtestingpage/events', $access_token);
			$data = $this->facebook->request('get', 'me?fields=id,name,accounts');
			// $data = $this->facebook->request('get',
				// '/pixtestingpage/feed',
				// $access_token
			  // );
			pr($data,1);
			
			die('STOP');
		}
		
		
		
	}
	
	public function fb_event_parse()
	{
		$this->load->helper('social_helper');
		
		// $content = 'foobarbaz';
		// $regular_expression = '/(foo)(bar)(baz)/'
		
		require_once('fb_event_parse_content.php');
		$html = fb_event_parse_content();
		
		$regular_expression = 'event_header_primary(.*?)\<img(.*?)src="(.*?)"'; // Parse Image
		$regular_expression .= '(.*?)_publicProdFeedInfo__timeRowTitle(.*?)content="(.*?)"\>\<span'; // Parse DateTime
		$regular_expression .= '(.*?)\<a class="_5xhk"(.*?)\>(.*?)\<\/a\>\<div class="_5xhp(.*?)"\>(.*?)\<\/div'; // Parse Location
		$regular_expression = "/{$regular_expression}/s"; //s for matching newlines
		
		preg_match($regular_expression, $html, $matches, PREG_OFFSET_CAPTURE);
		// pr('fb_event_parse() - $matches');
		// pr($matches,1);
			
		if(isset($matches[3][0]) && isset($matches[6][0]) && isset($matches[9][0]) && isset($matches[11][0])){
			$image_url = $matches[3][0];
			$image_url = urldecode($image_url);
			pr('fb_event_parse() - $image_url = '.$image_url);
			$datetime_str = $matches[6][0];
			$datetime_arr = parse_string_to_datetime_period($datetime_str);
			$location_city = $matches[9][0];
			$location_address = $matches[11][0];
			$event_location = !empty($location_address) ? $location_address : $location_city;
			pr('fb_event_parse() - $datetime_arr');
			pr($datetime_arr);
			pr('$event_location = '.$event_location);
		}
		
		pr('fb_event_parse() - $matches');
		pr($matches, 1);
	}
	
	/*
	 * 	PHP library/toolkit - Ultimate Web Scraper Toolkit
	 * 	Example HTML form extraction:
	 */
	public function fb_login()
	{
		// $url = 'https://www.facebook.com/events/225933337990825';
		$url = 'https://www.facebook.com/events/167584683912997/';
		
		$this->load->helper('scraper_helper');
		$flag_debug = true;
		$html = scrape_html_from_url_facebook($url, $flag_debug);
		file_put_contents(__DIR__.'../FB_events.html', "\xEF\xBB\xBF".  $html); // Write with BOM: UTF-8
		echo $html;

	}
}













