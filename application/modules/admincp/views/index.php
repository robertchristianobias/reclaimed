<h3 class="page-title">Dashboard</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>Dashboard</li>
	</ul>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>FB Event
				</div>
			</div>
			<div class="portlet-body form">
				<!-- <div class="form-body notification" style="">
					<div class="alert alert-success" style="">
						<strong>Success!</strong> The page has been saved.
					</div>
				</div> -->
                
                <?php echo form_open(current_url(), 'id="eb-event-form" class="form-horizontal form-row-seperated"'); ?>
					<div class="form-body">
						<div class="form-group last">
							<label class="control-label col-md-3">
                                URL
                            </label>
							<div class="col-md-9">
                                <input type="text" name="fb_event_url" id="fb_event_url" class="form-control" value=""  />
                                <div id="error-fb_event_url"></div>
                            </div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn btn-success btn-margin" id="save_event_url">Add FB Event</button>
							</div>
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>