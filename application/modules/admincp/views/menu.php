<?php
if(!empty($main_menu)){
	foreach($main_menu as $key=>$val){
		$id_mainMenu = $val->id;
?>
		<li class="">
			<a>
				<i class="<?=$val->icon?>"></i>
				<span class="title"><?=$val->name?></span>
			</a>
		</li>
<?php 
		
		if($menu){
			foreach($menu as $v){
				$main_menu_id = $v->id_mainMenu;
				if($id_mainMenu==$main_menu_id){
					$permission = 'r';
					$user_permission = $perm->permission;
					//pr($user_permission,1);
					if($perm->id == 1){
						$user_permission = 'all'; // TODO
					}
					$module_id = $v->id;
					
					if(permission_check($permission, $module_id, $user_permission)){
?>
<li class="<?php if($this->uri->segment(2)==$v->name_function){ print 'active'; }?>">
	<a href="<?=PATH_URL_ADMIN.''.$v->name_function.'/'?>">
		<i class=""></i>
		<span class="title"><?=$v->name?></span>
	</a>
</li>
<?php 
					}
				}
			}
		}
	}
}
?>