<?php
class Admincp_model extends CI_Model {

	function checkLogin($user){
		$this->db->select('*');
		$this->db->where('username', $user);
		$this->db->where('status', 1);
		$query = $this->db->get('admin_nqt_users');
		
		foreach ($query->result() as $row){
			$pass = $row->password;
		}
		
		if(!empty($pass)){
			return $pass;
		}else{
			return false;
		}
	}
	function checkLogin2($user){
		$this->db->select('*');
		$this->db->where('username', $user);
		$this->db->where('status', 1);
		$query = $this->db->get('admin_nqt_users');
		return $query->row_array();
	}

	function getusernameByUserId($id){
		$this->db->select('id');
		$this->db->where('username');
        $query = $this->db->get('admin_nqt_users');
        return $query->row_array();
        //$result = $query->row_array();
        //if($query->row_array()){
			//return $query->row_array();
		//}else{
			//return false;
		//}
	}
	
	function getInfo($user){
		$this->db->select('*');
		$this->db->where('username', $user);
		$this->db->where('status', 1);
		$query = $this->db->get('admin_nqt_users');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getSetting($slug=''){
		$this->db->select('*');
		if($slug!=''){
			$this->db->where('slug', $slug);
			$this->db->limit(1);
		}
		$query = $this->db->get('admin_nqt_settings');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function checkSlug($slug){
		$this->db->select('id');
		$this->db->where('slug', $slug);
		$this->db->limit(1);
		$query = $this->db->get('admin_nqt_settings');

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	public $admin_user = NULL;
	function admin_user_set($user){
		$this->admin_user = $user;
	}

	// Section Remember me
    public function storeTokenForUser1($user_id, $token,$created_date, $expired_time){
        $data = array(
            'user_id' => $user_id,
            'expired_time' => $expired_time,
            'user_token' =>  $token,
            'created' => $created_date
            
            );
        $this->db->insert('user_session_cms',$data);

    }

    public function setSessionWhenLoginSuccessByUserId($userId) {
		// pr($userId,1);
	$userData = $this->getUserDataByUserId($userId);
	//$userPropage =  $this->getPropageByUserId($userId);
	//pr($userPropage, 1);
		if( ! empty($userData)) {
			$data = array();
			foreach($userData as $key => $value) {
				$data ['userData'][$key] = $value;
			}
			$data ['userlogin']['id'] = $userData ['id'];
			$data ['userlogin']['username'] = $userData ['username']; // TODO - Removed and replaced by name
			$data ['userlogin']['name'] = $userData ['username'];
			//$data ['userlogin']['thumbnail'] = $userData ['thumbnail'];
			//$data ['userlogin']['link_url_redm'] = $userData ['link_url_redm'];
			//$data ['userlogin']['user_bit_id'] = $userData ['user_bit_id'];
			//$data ['userlogin']['country_residence'] = $userData ['country_residence'];
			//$data ['userlogin']['propage_url'] = ( ! empty($userPropage) ) ? $userPropage['url_of_page'] : "";
			$this->session->set_userdata($data);
		}
	}

	public function getUserDataByuser_id($user_id) {
		$result = FALSE;
        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_session_cms');
        $result = $query->row_array();
        if($query->row_array()){
			return $query->row_array();
		}else{
			return false;
		}
	}

	public function deleteUser_Token($user_id,$user_token){
        $this->db->where('user_id',$user_id);
        $this->db->where('user_token',$user_token);
        $this->db->delete('user_session_cms');
    }
}