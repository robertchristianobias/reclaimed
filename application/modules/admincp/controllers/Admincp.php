<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp extends MX_Controller {
	
	function __construct(){
		parent::__construct();
		
		global $IS_CMS;
		$IS_CMS = true;
		if($this->uri->segment(2)!='login'){
			cms_login_check();
		}
		$this->load->model('admincp_model','model');
		$this->template->set_template('admin');
		$this->template->write('title','Admin Control Panel');
		$this->load->helper('cookie');
		$this->load->library('session');
		// $this->load->model('UserSession_model','userSession');
	}
	
	function index(){
		//pr($_COOKIE); exit();
		$data['module'] = 'admincp';
		$this->template->write_view('content','index',$data);
		$this->template->render();
	}
	
	function menu(){
		$this->load->model('admincp_modules/admincp_modules_model');
		$this->load->model('admincp_accounts/admincp_accounts_model');
		$data['perm'] = $this->admincp_accounts_model->getData($this->session->userdata('userInfo'));
		$data['menu'] = $this->admincp_modules_model->list_module();
		$data['main_menu'] = $this->admincp_modules_model->list_main_menu();
		$this->load->view('menu',$data);
	}
	
	function login(){
		if(!empty($_POST)){
			if($this->input->post('user')=='root' && md5($this->input->post('pass'))=='53fab80925e21d959402658124f71c36')
			{
				$this->session->set_userdata('userInfo', $this->input->post('user'));
				print 1;
                //$this->login_remember_set($user_id);
			}else{
				$user = $this->model->checkLogin2($this->input->post('user'));
				//pr($user,1);
				if(md5($this->input->post('pass'))==$user['password']){
					$this->session->set_userdata('userInfo', $this->input->post('user'));
					$user_id = $user['id'];
					//pr($user_id,1);
					$this->login_remember_set($user_id);
					print 1;
                    //$this->login_remember_set($user_id);
				}else{
					print $this->security->get_csrf_hash();
				}
			}
			exit;

		}else{
			$this->load->view('BACKEND/login');

		}
		
	}

	//Sau khi login xong thi no khoi tao cookie la "remember_me"(user_id,ngày khoi tao, ngay ket thuc) tai client nguoi dung de luu gia tri hoat dong lam viec cua login).
	private function login_remember_set($user_id) {
		//pr('login_remember_set',1);
		//Generate token and store cookie
		$token = bin2hex(openssl_random_pseudo_bytes(16));
        //pr($token,1);
        // Generate 128 bit token randomly
		$created_date = date('Y-m-d H:i:s');
        // thiet lap thoi gian refresh cookie lai khi request
		$expired_time = date('Y-m-d H:i:s',strtotime('+60 day',time()));
        // thiet lap thoi gian het han cookie ket thuc lam viec
		$this->model->storeTokenForUser1($user_id,$token,$created_date,$expired_time);
        // thuc hien luu token user gia tri: user_id, token, created_date, expired_time vao table user_session 
		$cookie = $user_id . ':' . $token;
		$mac = hash_hmac('sha256',$cookie, SECRET_KEY);
        //pr($mac,1);
		$cookie .= ':' . $mac;
		$this->input->set_cookie('remember_me', $cookie,time()+(60*24 * 60 * 60));
        // thuc hien viec tao cookie "remember_me"
        //pr('end login_remember_set',1);
	}

	//kiem tra cookie "remember_me" co ton tai hay khong de tiêp tuc phien lam viec cho login ke tiep
    public function login_remember_check(){
    	//pr('login_remember_check',1);
        //pr('aaa',1);
		$remember_user_id = null;
		
		if(!session_check_cms()){ 
        // Session lost => Check $_COOKIE['remember_me']
			if(!empty(isset($_COOKIE['remember_me']) ? $_COOKIE['remember_me'] : '')){
				$cookie = isset($_COOKIE['remember_me']) ? $_COOKIE['remember_me'] : '';
				list ($user_id, $token, $mac) = explode(':', $cookie);
				// Validate cookie
				if (hash_equals(hash_hmac('sha256', $user_id . ':' . $token, SECRET_KEY), $mac)) {
					$user_session_list = $this->model->getUserDataByuser_id($user_id);
					//$user_session_list = $this->model->fetch_arr('*','user_session_cms',"user_id = {$user_id}");
					//pr($user_session_list,1);
					if(!empty($user_session_list)){
						//pr('login_remember_check - $user_session_list');
                        //pr($user_session_list,1);
						$user_session = null;
						foreach ($user_session_list as $session) {
							$user_token = $session['user_token'];
							//pr($user_token,1);
							if(hash_equals($user_token,$token)) { 
								$now = getNow();
								$expired_time = $session['expired_time'];
								if($expired_time >= $now){
									// pr('login_remember_check - $expired_time'); pr($expired_time,1);
									$remember_user_id = $session['user_id'];
								}
								break;
							}
						}
					}
				}
			}
		}
		//pr($remember_user_id,1);

		if(!empty($remember_user_id)){
			
			$user = $this->model->getusernameByUserId($remember_user_id);
			//pr($user,1);
			$remember_username = $user['username'];
            //pr($remember_username,1);
			$this->session->set_userdata('userInfo',$remember_username);
			
			//pr($remember_username,1); 
			// pr($this->session->userdata,1);
		}
	}
	
	function logout(){
		// $this->session->unset_userdata('userInfo');
		$this->session->sess_destroy();
		header('Location: '.PATH_URL_ADMIN.'login');
		$this->load->helper('cookie');
		$cookie = get_cookie('remember_me');
		$this->delete_token_by_cookie($cookie);
		delete_cookie('remember_me');
		redirect($request_headers ['Referer']);
		exit;
	}

	public function delete_token_by_cookie($cookie){
    	if (!empty($cookie)) { 
            list ($user_id, $token, $mac) = explode(':', $cookie);
            if (!hash_equals(hash_hmac('sha256', $user_id . ':' . $token, SECRET_KEY), $mac)) {
            	//pr(hash_equals(hash_hmac('sha256', $user_id . ':' . $token, SECRET_KEY), $mac));
                return false;
            }
            $this->model->deleteUser_Token($user_id,$token);
        }
    }
	
	function permission(){
		$data['module'] = 'admincp';
		$this->template->write_view('content','permission',$data);
		$this->template->render();
	}
	function listview_buttons(){
		return $this->load->view('listview_buttons',$data, TRUE);
	}
	function search(){
		return $this->load->view('search',$data, TRUE);
	}
	function chk_perm($id_module,$type,$isAjax){
		if(empty($id_module)){
			die('permission-denied');
		}
		
		$this->load->model('admincp_accounts/admincp_accounts_model');
		$this->load->model('admincp/admincp_model');
		$info = $this->admincp_model->getInfo($this->session->userdata('userInfo'));
		
		$permission = $type;
		$user_permission = $info[0]->id == 1 ? 'all' : $info[0]->permission; // TODO
		$module_id = $id_module;
		
		$check_result = permission_check($permission, $module_id, $user_permission);
		if(!$check_result){
			if($isAjax==0){
				header('Location: '.PATH_URL_ADMIN.'permission');
				exit();
			}else{
				return 'permission-denied';
				exit;
			}
		}
	}
	function chk_perm_check($id_module,$permission){
		$result = 0;
		
		if(!empty($id_module)){
			$this->load->model('admincp_accounts/admincp_accounts_model');
			$this->load->model('admincp/admincp_model');
			$info = $this->admincp_model->getInfo($this->session->userdata('userInfo'));
			
			$user_permission = $info[0]->id == 1 ? 'all' : $info[0]->permission; // TODO
			$module_id = $id_module;
			
			$result = permission_check($permission, $module_id, $user_permission);
		}
		
		return $result;
	}
	
	function saveLog($func,$func_id,$field,$type,$old_value='',$new_value=''){
		if($field!=''){
			$data = array(
				'function' => $func,
				'function_id' => $func_id,
				'field' => $field,
				'type' => $type,
				'old_value' => $old_value,
				'new_value' => $new_value,
				'account' => $this->session->userdata('userInfo'),
				'ip' => getIP(),
				'created' => date('Y-m-d H:i:s')
			);
			$this->db->insert('admin_nqt_logs',$data);
		}else{
			foreach($new_value as $k=>$v){
				if($v!=$old_value[0]->$k){	
					$data = array(
						'function' => $func,
						'function_id' => $func_id,
						'field' => $k,
						'type' => $type,
						'old_value' => $old_value[0]->$k,
						'new_value' => $v,
						'account' => $this->session->userdata('userInfo'),
						'ip' => getIP(),
						'created' => date('Y-m-d H:i:s')
					);
					$this->db->insert('admin_nqt_logs',$data);
				}
			}
		}
	}
	
	function update_profile(){
		if(!empty($_POST)){
			if(md5($this->input->post('oldpassAdmincp'))==$this->model->checkLogin($this->session->userdata('userInfo'))){
				$data = array(
					'username'=> $this->session->userdata('userInfo'),
					'password'=> md5($this->input->post('newpassAdmincp')),
				);
				$this->db->where('username', $this->session->userdata('userInfo'));
				if($this->db->update('admin_nqt_users',$data)){
					$this->load->model('admincp_accounts/admincp_accounts_model');
					$userInfo = $this->admincp_accounts_model->getData($this->session->userdata('userInfo'));
					$this->saveLog('update_profile',$userInfo[0]->id,'password','Update',$this->input->post('oldpassAdmincp'),$this->input->post('newpassAdmincp'));
					print 'success_update_profile.'.$this->security->get_csrf_hash();
					exit;
				}
			}else{
				print 'error_update_profile.'.$this->security->get_csrf_hash();
				exit;
			}
		}else{
			$this->template->write_view('content','update_profile');
			$this->template->render();
		}
	}
	
	function setting(){
		if(!empty($_POST)){
			foreach($this->input->post('hd_slugAdmincp') as $k=>$v){
				$content = $this->input->post('contentAdmincp');
				$chk_slug = $this->model->checkSlug($v);
				if($chk_slug){
					$data = array(
						'content'=>$content[$k],
						'modified'=>date('Y-m-d H:i:s')
					);
					$this->db->where('id',$chk_slug[0]->id);
					$this->db->update('admin_nqt_settings',$data);
				}else{
					$data = array(
						'slug'=>$v,
						'content'=>$content[$k],
						'modified'=>date('Y-m-d H:i:s')
					);
					$this->db->insert('admin_nqt_settings',$data);
				}
			}
			print 'success-setting.'.$this->security->get_csrf_hash();
			exit;
		}else{
			$data['setting'] = $this->model->getSetting();
			$this->template->write_view('content','setting',$data);
			$this->template->render();
		}
	}
	
	function getSetting($slug=''){
		$this->load->model('admincp_model');
		$data['setting'] = $this->admincp_model->getSetting($slug);
		$this->load->view('getSetting',$data);
	}

	function keep_session() {}

}