<?php
$is_venue= !empty($result->user_type_id) && $result->user_type_id == 1;
$is_artist= !empty($result->user_type_id) && $result->user_type_id == 2;
$is_label= !empty($result->user_type_id) && $result->user_type_id == 3;
$is_promoter= !empty($result->user_type_id) && $result->user_type_id == 4;
$is_marketer= !empty($result->user_type_id) && $result->user_type_id == 5;
?>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovaeditor.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovamanager.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<?php
	if(isset($this->lang->languages))
	{
		$all_lang = $this->lang->languages;
	}
	else
	{
		$all_lang = array(
			'' => ''
		);
	}
?>
<script type="text/javascript">


function save()
{
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}

function showRequest(formData, jqForm, options) 
{
	var form = jqForm[0];
	<? if(empty($id)){ ?>
	if(form.email.value  == ''){
		$('#txt_error').html('Please enter information!!!');
		$('#loader').fadeOut(300);
		show_perm_denied();
		return false;
	<? } ?>
	
}

function showResponse(responseText, statusText, xhr, $form) 
{
	responseText = responseText.split(".");
	token_value  = responseText[1];
	$('#csrf_token').val(token_value);
	if(responseText[0]=='success'){
		<?php if($id==0){ ?>
		location.href=root+module+"/#/save";
		<?php }else{ ?>
		if($('.form-upload').val() != ''){
			$.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
				var res = data.split("src=");
				$('.fileinput-filename').html('');
				$('.fileinput').removeClass('fileinput-exists');
				$('.fileinput').addClass('fileinput-new');
			});
		}
		show_perm_success();
		<?php } ?>
	}
	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
	
	if(responseText[0]=='error-image'){
		$('#txt_error').html('Only upload image.');
		show_perm_denied();
		return false;
	}
	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
}
</script>
<style type="text/css">
		.btn-bg{
		margin-left: 30px;	
		background: #3498DB !important;
		color: #fff !important;
	}
	.btn-bg:hover{
		background: #3498DB99 !important;
		color: #000 !important;
		/*/font-weight: bold;*/
	}

</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
				<a href="<?=PATH_URL_ADMIN?>">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li><?php ($this->uri->segment(4)=='') ? print 'Add new' : print 'Edit' ?></li>
	</ul>
	<a class="btn-canel-back" href="<?=PATH_URL_ADMIN.$module.'/#/back'?>">
		<button type="button" class="btn default btn-bg">Cancel
		</button>
	</a>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Form Information
				</div>
			</div>
			
			<div class="portlet-body form">
				<div class="form-body notification" style="display:none">
					<div class="alert alert-success" style="display:none">
						<strong>Success!</strong> The page has been saved.
					</div>
					
					<div class="alert alert-danger" style="display:none">
						<strong>Error!</strong> 
						<span id="txt_error"></span>
					</div>
				</div>
				
				<!-- BEGIN FORM-->

				<form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />

					<div class="form-body">	

						<div class="tab-content">
					
						<style type="text/css">
							.title-user-info {
							    color: #000;
							    font-size: 18px;
							    text-align: center;
							    padding: 15px;
							    font-weight: bold;
							}
							

						</style>

						
							
						<div id="avatar_image">	
							<?php /*get avatar url*/
	                        $image_url = ( ! empty($result->image) ) ? get_resource_url($result->image) : '';
	                        $thumbnail_url = ( ! empty($result->thumbnail) ) ? get_resource_url($result->thumbnail) : null;
		                	?>
							<div class="col-md-6">
                               <div style="text-align: center;margin-top:20px;margin-bottom:10px;">
                                    <input type="button" name="input_avatar" value="Select File" class="update-img-upload " id="cropContainerHeaderButton" />
                                    <!-- image thumbnail -->
									<input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
									<!-- image original -->
									<input type="hidden" id="input_image_url" name="image_urlAdmincp">

                                </div>
                                <div class="img-profile">
                                	<div id="cropic_element" style="display:none"></div>
                                    <a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>" style="display:block;">
                                        <img id="preview_image" width=200 height=200 src="<?=$thumbnail_url?>" style="margin-left:auto;margin-right:auto;display: block;"> 
                                    </a>
                                </div>
							</div>

							<?php /*get avatar url*/
	                        $image_url_1 = ( ! empty($result->image_1) ) ? get_resource_url($result->image_1) : '';
	                        $thumbnail_url_1 = ( ! empty($result->thumbnail_1) ) ? get_resource_url($result->thumbnail_1) : null;
		                	?>
							<div class="col-md-6">
                               	<div style="text-align: center;margin-top:20px;margin-bottom:10px;">
                                    <input type="button" name="input_avatar" value="Select File" class="update-img-upload " id="crop_image_1" />
                                    <!-- image thumbnail -->
									<input type="hidden" id="input_thumbnail_url_1" name="thumbnail_urlAdmincp_1">
									<!-- image original -->
									<input type="hidden" id="input_image_url_1" name="image_urlAdmincp_1">

                                </div>
                                <div class="img-profile">
                                	<div id="cropic_element_image_1" style="display:none"></div>
                                    <a class="fancybox-button" id="preview_image_fancybox_1" href="<?=$image_url_1?>" style="display:block;">
                                        <img id="preview_image_1" width=200 height=200 src="<?=$thumbnail_url_1?>" style="margin-left:auto;margin-right:auto;display: block;"> 
                                    </a>
                                </div>
							</div>
						</div>
						

					
						<div class="col-md-12">
							<div class="form-group">
							<label class="control-label col-md-2">Status</label>
								<div class="col-md-10">
									<div class="checkbox-list">
										<label class="checkbox-inline">
											<input <?=( ! empty($result->status) && ($result->status == 1) ) ? 'checked' : 'checked'?> type="checkbox" name="status">
										</label>
									</div>
								</div>
							</div>	

							<div class="form-group">
								<label class="control-label col-md-2">Email
									<span class="required" aria-required="true">*</span>
								</label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->email)) ? $result->email : '' ?>" type="text" name="email" id="email" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2">Username 	<span class="required" aria-required="true">*</span>
								</label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->username)) ? $result->username : '' ?>" type="text" name="username" id="username" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2">Location
									<span class="required" aria-required="true">*</span>
								</label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->location)) ? $result->location : '' ?>" type="text" name="location" id="location" class="form-control" />
									<input type="hidden" name="country" value="<?php echo (isset($result->country)) ? $result->country : '' ?>" type="text" id="country-hides">
                               	  <input type="hidden" name="city" value="<?php echo (isset($result->city)) ? $result->city : '' ?>" type="text" id="city-hides">

								</div>
							</div>
						

							<div class="form-group">
								<label class="control-label col-md-2">User Type
								 	<span class="required" aria-required="true">*</span>
								 </label>
								<div class="col-md-10">
									<select onChange="getPerm(this.value)" class="form-control" name="listname_idAdmincp" id="groupAdmincp">
							         <?
							         if(!empty($user_type_list))
							         { 

							          	foreach($user_type_list as $list_type)
							          	{
							          	//pr($list_type);
							         ?>
							             <option 
								          <?php echo ( isset($result->user_type_id) && ($result->user_type_id == $list_type->id) ) ? 'selected' : ''; ?> 
								          value="<?=$list_type->type?>">
								           <?=$list_type->type?>
								         </option>
							         <?
							     		}	
							         } 

							         ?>
							        </select>			
								</div>
							</div>
						<div class="title-user-info">Additional Profile Info
						</div>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL
								 	
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->username)) ? $result->username : '' ?>" type="text" readonly="readonly" name="url" id="url" class="form-control"/>
								</div>
							</div>
						<?
						}
						?>


						<?
						if($is_artist || $is_label)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Founder
								 	
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->username)) ? $result->username : '' ?>" type="text" readonly="readonly" name="distributor" id="distributor" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Biography
								 	
								 </label>
								<div class="col-md-10">
									<textarea data-required="1" value="" type="text" name="biiography" id="biiography" class="form-control" cols="" rows="5" maxlength="800" readonly="readonly"/><?php echo (isset($result->biiography)) ? $result->biiography : '' ?></textarea>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Short bio
								 	
								 </label>
								<div class="col-md-10">
									<textarea data-required="1" value="" type="text" name="biiography" id="biiography" class="form-control" cols="" rows="4" maxlength="200" readonly="readonly"/><?php echo (isset($result->biiography)) ? $result->biiography : '' ?></textarea>
								</div>
							</div>	
						<?
						}	
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Status
								 	
								 </label>
								<div class="col-md-10">
									<textarea data-required="1" value="<?php echo (isset($result->status_genenal)) ? $result->status_genenal : '' ?>" type="text" name="status_genenal" id="status_genenal" class="form-control" cols="" rows="3" maxlength="100" readonly="readonly"/><?php echo (isset($result->status_genenal)) ? $result->status_genenal : '' ?></textarea>
								</div>
							</div>
						<?
						}
						?>	


						<?
						if($is_venue || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Marketing
								 	
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->marketing)) ? $result->marketing : '' ?>" type="text" readonly="readonly" name="marketing" id="marketing" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_venue || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Additional Contacts
								 	
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->contact)) ? $result->contact : '' ?>" type="text" readonly="readonly" name="contact" id="contact" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_artist || $is_label || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">Distributor/s
								 	
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->distributor)) ? $result->distributor : '' ?>" type="text" readonly="readonly" name="distributor" id="distributor" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL(Facebook.com)
								 
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->facebook)) ? $result->facebook : '' ?>" type="text" readonly="readonly" name="facebook" id="facebook" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL(Twitter.com)
								 
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->twitter)) ? $result->twitter : '' ?>" type="text" readonly="readonly" name="twitter" id="twitter" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>
	

						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>	
							<div class="form-group">
								<label class="control-label col-md-2">URL(Bandcamp.com)
								 
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->bandcamp)) ? $result->bandcamp : '' ?>" type="text" readonly="readonly" name="bandcamp" id="bandcamp" class="form-control"/>
								</div>
							</div>
						<?
						}
						?>


						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL(Soundcloud.com)
								 
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->soundcloud)) ? $result->soundcloud : '' ?>" type="text" readonly="readonly" name="soundcloud" id="soundcloud" class="form-control"/>
								</div>
							</div>	
						<?
						}
						?>

						
						<?
						if($is_venue || $is_artist || $is_label || $is_promoter || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL(Mixcloud.com)
								 
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->mixcloud)) ? $result->mixcloud : '' ?>" type="text" readonly="readonly" name="mixcloud" id="mixcloud" class="form-control"/>
								</div>
							</div>
						<?
						}
						?>
						

						<?
						if($is_artist || $is_marketer)
						{
						?>
							<div class="form-group">
								<label class="control-label col-md-2">URL(iTunes.com)
								
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->itunes)) ? $result->itunes : '' ?>" type="text" readonly="readonly" name="itunes" id="itunes" class="form-control"/>
								</div>
							</div>
						<?
						}
						?>
							

						<?
						if($is_artist || $is_label || $is_marketer)
						{
						?>	
							<div class="form-group">
								<label class="control-label col-md-2">URL(Beatport.com)
								
								 </label>
								<div class="col-md-10">
									<input data-required="1" value="<?php echo (isset($result->bandcamp)) ? $result->beatport : '' ?>" type="text" readonly="readonly" name="beatport" id="beatport" class="form-control"/>
								</div>
							</div>			
						</div>
						<?
						}
						?>

						
						
					</div>
					<div class="form-actions fix-bg">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<!-- <button onclick="save()" type="button" class="btn green">
									<i class="fa fa-pencil"></i>Save
								</button> -->
								
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places"></script>
<script type="text/javascript">
			function initialize() {
				var options = {
					types: ['(regions)']
				};
				var input = document.getElementById('searchTextField');
				var autocomplete = new google.maps.places.Autocomplete(input , options);
			}
			google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style type="text/css">
	#cropic_element {
		height: 200px;
		width: 200px;
	}
	#cropic_element_image_1{
		height: 200px;
		width: 200px;
	}
</style>


<script>
    var croppicHeaderOptions = {
            
            uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
            cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
            uploadData:{'prefix':'avatar_'},
            enableMousescroll:true,
            customUploadButtonId:'cropContainerHeaderButton',
            outputUrlId:'input_thumbnail_url',
            modal:true,
            rotateControls: false,
            doubleZoomControls:false,
            imgEyecandyOpacity:0.4,
            onBeforeImgUpload: function(){ },
            onAfterImgUpload: function(){ appendOriginImageAward(); },
            onImgDrag: function(){ },
            onImgZoom: function(){ },
            onBeforeImgCrop: function(){ },
            onAfterImgCrop:function(){appendAward(); },
            onReset:function(){ onResetCropic(); },
            onError:function(errormessage){ console.log('onError:'+errormessage) }
    }   
    var croppic = new Croppic('cropic_element', croppicHeaderOptions);
    function appendOriginImageAward() {
        var url_origin = $("div#croppicModalObj > img").attr('src');
        $('#input_image_url').val(url_origin);
        $("#preview_image_fancybox").attr("href", $('#input_image_url').val());
    }
    function appendAward(){
        $("#preview_image").attr("src", $('#input_thumbnail_url').val());
        $("#preview_image").show();
    }
    function onResetCropic(){
    	$('#input_image_url').val('');
    	$('#input_thumbnail_url').val('');
    	$("#preview_image_fancybox").attr("href", '');
    	$("#preview_image").attr("src", '');
    }
   
</script>
<script>
// setting country user
function initialize5() {
 options = {types: ['(regions)']};
 var input = document.getElementById('location'); //
 var autocomplete = new google.maps.places.Autocomplete(input , options);
                                
	google.maps.event.addListener(autocomplete, 'place_changed', 
	function() {
	var address_components=autocomplete.getPlace().address_components;
	var city='';
	var country='';
	for(var j =0 ;j<address_components.length;j++) {
		city =address_components[0].short_name;
	
		if(address_components[j].types[0]=='country')
		{
		   country=address_components[j].short_name;
		   console.log(address_components[j]);
		}
	}
		 //document.getElementById('data').innerHTML="City Name : <b>" + city + "</b> <br/>Country Name : <b>" + country + "</b>";
		document.getElementById('country-hides').value = country;
		document.getElementById('city-hides').value = city;

	});
}
 google.maps.event.addDomListener(window, 'load', initialize5);
</script>
<script>
    var croppicHeaderOptions_1 = {
            
            uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
            cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
            uploadData:{'prefix':'avatar_image_'},
            enableMousescroll:true,
            customUploadButtonId:'crop_image_1',
            outputUrlId:'input_thumbnail_url_1',
            modal:true,
            rotateControls: false,
            doubleZoomControls:false,
            imgEyecandyOpacity:0.4,
            onBeforeImgUpload: function(){ },
            onAfterImgUpload: function(){ appendOriginImageAwards(); },
            onImgDrag: function(){ },
            onImgZoom: function(){ },
            onBeforeImgCrop: function(){ },
            onAfterImgCrop:function(){appendAwards(); },
            onReset:function(){ onResetCropics(); },
            onError:function(errormessage){ console.log('onError:'+errormessage) }
    }   
    var croppic = new Croppic('cropic_element_image_1', croppicHeaderOptions_1);
    function appendOriginImageAwards() {
        var url_origin = $("div#croppicModalObj > img").attr('src');
        $('#input_image_url_1').val(url_origin);
        $("#preview_image_fancybox_1").attr("href", $('#input_image_url_1').val());
    }
    function appendAwards(){
        $("#preview_image_1").attr("src", $('#input_thumbnail_url_1').val());
        $("#preview_image_1").show();
    }
    function onResetCropics(){
    	$('#input_image_url_1').val('');
    	$('#input_thumbnail_url_1').val('');
    	$("#preview_image_fancybox_1").attr("href", '');
    	$("#preview_image_1").attr("src", '');
    }
   
</script>

<!-- END PAGE CONTENT-->
