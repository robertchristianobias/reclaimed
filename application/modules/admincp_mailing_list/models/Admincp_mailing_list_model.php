<?php
class Admincp_mailing_list_model extends CI_Model {
	private $module = 'admincp_mailing_list';
	private $table = 'admin_nqt_mailing_list';
	private $table_contact = 'admin_nqt_mailing_list_member';
	private $field_parent_id = 'listname_id';
	
function getsearchContent($limit = 0, $page = -1){
		$result = false;
		$main_table = $this->table;
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
        	$select =   array(
                'admin_nqt_mailing_list.*'
            );  
	     	$this->db->select($select);
	        }
		
		$this->db->from($main_table);
		$content = $this->input->post('content');
		if(!empty($content)){
			$search_condition_arr = array(
				"{$main_table}.name LIKE '%{$content}%'",
			);
			$search_condition = implode($search_condition_arr, ' OR ');
			$search_condition = "( {$search_condition} )";
			$this->db->where($search_condition);
		}
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');
		if(!empty($dateFrom) || !empty($dateTo)){
			$datetimeFrom = date('Y-m-d 00:00:00',strtotime($dateFrom));
			$datetimeTo = date('Y-m-d 23:59:59',strtotime($dateTo));
			if(empty($dateFrom)){
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			} else if(empty($dateTo)){
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
			} else {
				$this->db->where("{$main_table}.created >= '{$datetimeFrom}'");
				$this->db->where("{$main_table}.created <= '{$datetimeTo}'");
			}
		}
		//End - Search condition
		
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		//FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		return $result;
	}
	
	
	function checkData($name,$id=0){
		$this->db->select('id');
		$this->db->where('name',$name);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
		
	function saveManagement($fileName=''){
		//default data
		$_status = ($this->input->post('status')=='on') ? 1 : 0;
		$_created = $_updated = date('Y-m-d H:i:s',time());
        $_name = $this->input->post('name');
        $_from_email = $this->input->post('from_email');
        $_from_name = $this->input->post('from_name');
		if($this->input->post('hiddenIdAdmincp')==0){
			//Kiểm tra đã tồn tại chưa?
			
			$checkData = $this->checkData($_name);
			//pr(123);
			if($checkData){
				print 'error'.$this->security->get_csrf_hash();
				exit;
			}
			
		
			$data = array(
				'status'=> $_status,
				'name'=> $_name,
				'from_email'=>$_from_email,
                'from_name' =>$_from_name,
				'created' =>$_created,
			);
			
			//DO INSERT DATA
			if($this->db->insert(PREFIX.$this->table,$data)){
				$insert_id = $this->db->insert_id();				
				modules::run('admincp/saveLog',$this->module,$insert_id,'Create List','Create List');
				return true;
			}
		}
		else{
			$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
			$data['status'] = $_status;
			$data['updated'] = $_updated;
			$data['name'] = $_name;
			$data['from_email'] = $_from_email;
			$data['from_name'] = $_from_name;
			$update_id = $this->input->post('hiddenIdAdmincp');
			unset($data['created'], $data['updated']);
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data)){
				return true;
			}
			return false;
		}
	}
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getDetailManagement($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);
		if($query->result()){
			$result = $query->row();
			return $result;
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
}