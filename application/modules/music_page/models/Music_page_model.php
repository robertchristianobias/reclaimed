<?php
class Music_page_model extends CI_Model {
	private $module = 'music_page';
	private $table = 'page_item';
	private $table_lang = 'page_item_lang';
	private $field_parent_id = 'page_id';

	function getsearchContent($limit,$page){
		$this->db->select('*');
		$this->db->limit($limit,$page);
		$this->db->order_by($this->input->post('func_order_by'),$this->input->post('order_by'));
		if($this->input->post('content')!=''){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalsearchContent(){
		$this->db->select('*');
		if($this->input->post('content')!='' && $this->input->post('content')!='type here...'){
			$this->db->where('(`title_vi` LIKE "%'.$this->input->post('content').'%" OR `title_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetailManagement($id){
		$this->db->select(PREFIX.$this->table.'.*, id AS data_lang');
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			$result = $query->row();
			
			$this->db->select('*');
			$this->db->where($this->field_parent_id, $id);
			$query = $this->db->get(PREFIX.$this->table_lang);
			$temp = NULL;
			if($query->result()) {
				foreach ($query->result() as $key => $item) {
					$temp[$item->lang_code] = $item;
				}
			}
			$result->data_lang = $temp;
			// pr($result, 1);
			return $result;
		}else{
			return false;
		}
	}
	
	function saveManagement($fileName=''){
		if(isset($this->lang->languages)){
			$all_lang = $this->lang->languages;
		}else{
			$all_lang = array(
				'' => ''
			);
		}

		//default data
		$_current_lang_code = '';
		$_status = ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$_created = $_updated = date('Y-m-d H:i:s',time());
		$_created_by = $_updated_by = ''; //TO DO
		$data_lang = $data_lang_temp = array();
		
		if($this->input->post('hiddenIdAdmincp')==0){
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$key = '_'.$key;
					$keyerror = '-'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}

				$checkData = $this->checkData($this->input->post('title'.$key.'Admincp'),$key);
				if($checkData){
					print 'error-title'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
					exit;
				}
				
				$checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key);
				if($checkSlug){
					print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
					exit;
				}
			}
			$data = array(
				'image'=> trim($fileName['image']),
				'status'=> $_status,
				'created'=> $_created,
			);
			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['title'.$key] = trim(htmlspecialchars($this->input->post('title' . $subfix)));
				$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				$data['name'.$key] = trim($this->input->post('name' . $subfix));
				//make data language
				$data_lang_temp['title'] = trim(htmlspecialchars($this->input->post('title' . $subfix)));
				$data_lang_temp['slug'] = trim($this->input->post('slug' . $subfix));
				$data_lang_temp['name'] = trim($this->input->post('name' . $subfix));
				$data_lang_temp['created'] = $_created;
				$data_lang_temp['status'] = $_status;
				$data_lang_temp['lang_code'] = $_current_lang_code;
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			if($this->db->insert(PREFIX.$this->table,$data)){
				$insert_id = $this->db->insert_id();
				//Insert data language
				foreach ($data_lang as $key => $item) {
					$item[$this->field_parent_id] = $insert_id;
					$this->db->insert(PREFIX.$this->table_lang, $item);
				}
				//End insert data language
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}else{
			$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
			// pr($result);
			// pr(json_decode(json_encode($result), true), 1);
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$key = '_'.$key;
					$keyerror = '-'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}
				$title = 'title'.$key;
				$slug = 'slug'.$key;
				if($result->$title!=$this->input->post('title'.$key.'Admincp')){
					$checkData = $this->checkData($this->input->post('title'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
					if($checkData){
						print 'error-title'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						exit;
					}
				}
				
				if($result->$slug!=$this->input->post('slug'.$key.'Admincp')){
					$checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
					if($checkSlug){
						print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						exit;
					}
				}
			}
			
			//Xử lý xóa hình khi update thay đổi hình
			if($fileName['image']==''){
				$fileName['image'] = $result->image;
			}else{
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result->image);
			}
			//End xử lý xóa hình khi update thay đổi hình
			
			$data = array(
				'image'=> trim($fileName['image']),
				'status'=> ($this->input->post('statusAdmincp')=='on')?1:0,
				'updated'=> $_updated
			);
			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['title'.$key] = trim(htmlspecialchars($this->input->post('title' . $subfix)));
				$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				$data['name'.$key] = trim($this->input->post('name' . $subfix));
				//make data language
				$data_lang_temp['title'] = trim(htmlspecialchars($this->input->post('title' . $subfix)));
				$data_lang_temp['slug'] = trim($this->input->post('slug' . $subfix));
				$data_lang_temp['name'] = trim($this->input->post('name' . $subfix));
				$data_lang_temp['updated'] = $_created;
				$data_lang_temp['status'] = $_status;
				$data_lang_temp['lang_code'] = $_current_lang_code;
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			$update_id = $this->input->post('hiddenIdAdmincp');
			$old_value[] = $result;
			unset($data['created'], $data['updated']);
			modules::run('admincp/saveLog',$this->module,$this->input->post('hiddenIdAdmincp'),'','Update', $old_value, $data);
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data)){
				//Update data in table language
				foreach ($data_lang as $key => $item) {
					$lang_code = $item['lang_code'];
					unset($item['lang_code']);
					$this->db->where('lang_code', $lang_code);
					$this->db->where($this->field_parent_id, $update_id);
					$this->db->update($this->table_lang, $item);
				}
				//end update data language
				return true;
			}
		}
		return false;
	}
	
	function checkData($title,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('title'.$lang,$title);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
}