<?php
$first_video_id = '';
?>
<section class="section-music">
	<div class="music">
		<ul class="playlists-form" style="display: none">
			<form method="GET" action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/search" class="form_search_music"  style="display:none;">
			<input type="text" class="search_music" name="keyword" placeholder="Search.." required="">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			</form>
		</ul>

		<ul class="playlists" style="display:block">
			<div class="embed-song embed-responsive embed-responsive-16by9" style="display: none;">
			<div id="video-placeholder" class="embed-responsive-item embed"></div>
			</div>
			
			<?php
			foreach($playlist_song_arr as $song){
				$first_video_id = empty($first_video_id) ? $song['video_id'] : '';
			?>
			<li class="song-order clearfix" data-type="youtube" data-code="<?=$song['video_id']?>">
				<div class="left">
					<a href="#" class="play">
						<span class="icon"></span>
						<img class="img-responsive" src="<?=$song['image']?>">
					</a>
				</div>
				<div class="right">
					<p class="song-name"><a href="#" class="play"><?=$song['title']?></a></p>
				</div>
			</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="playlist">
		<div class="wrap">
			<div class="controls" current="1" current_max="<?php echo count($playlist_song_arr) ?>">
				<span id="m-play" class="play control">
					<i class="fa fa-play" aria-hidden="true"></i>
					<i class="fa fa-pause" aria-hidden="true"></i>
				</span>
			<!-- 	<span id="m-backward" class="play control">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				
				</span>

				<span id="m-forward" class="play control">
					<i class="fa fa-arrow-right" aria-hidden="true"></i>
					
				</span>

				<span id="m-reload" class="play control">
					<i class="fa fa-refresh" aria-hidden="true"></i>
				
				</span> -->


				<span id="m-backward" class="backward control"><i class="fa fa-backward" aria-hidden="true"></i></span>
				<span id="m-forward" class="forward control"><i class="fa fa-forward" aria-hidden="true"></i></span>
				<!-- <span id="m-random" class="random control"><i class="fa fa-random" aria-hidden="true"></i></span> -->
				<span id="m-reload" class="reload control"><i class="fa fa-retweet" aria-hidden="true"></i></span>

				<span id="open-music" class="open-music">
					<i class="fa fa-arrow-circle-o-right hide" aria-hidden="true"></i>
					<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
				</span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var videoId = "<?=$first_video_id?>";
	</script>
</section>