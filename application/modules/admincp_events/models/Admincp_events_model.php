<?php

class Admincp_events_model extends MY_Model {

	private $module = 'admincp_events';
	private $table = 'admin_nqt_events';
	private $table_lang = 'admin_nqt_events_lang';
	private $field_parent_id = 'event_id';

	function getsearchdescription($limit,$page){
		$this->db->select('*');
		$this->db->limit($limit,$page);
		//$this->db->order_by($this->input->post('func_order_by'), $this->input->post('order_by'));
		$this->db->order_by('id', 'DESC');
		if($this->input->post('content')!=''){
			$this->db->where('(`name_vi` LIKE "%'.$this->input->post('content').'%" OR `name_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		$this->db->where('name_en !=', '');
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalsearchdescription(){
		$this->db->select('*');
		if($this->input->post('content')!='' && $this->input->post('content')!='type here...'){
			$this->db->where('(`name_vi` LIKE "%'.$this->input->post('content').'%" OR `name_en` LIKE "%'.$this->input->post('content').'%")');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')==''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
		}
		if($this->input->post('dateFrom')=='' && $this->input->post('dateTo')!=''){
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		if($this->input->post('dateFrom')!='' && $this->input->post('dateTo')!=''){
			$this->db->where('created >= "'.date('Y-m-d 00:00:00',strtotime($this->input->post('dateFrom'))).'"');
			$this->db->where('created <= "'.date('Y-m-d 23:59:59',strtotime($this->input->post('dateTo'))).'"');
		}
		
		$this->db->where('name_en !=', '');
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetailManagement($id){
		// $this->db->select(PREFIX.$this->table.'.*, id AS data_lang');
		$this->db->select('*');
		
		#check soft delete
		$this->db->where('is_delete', 0);
		
		$this->db->where('id',$id);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			$result = $query->row();
			
			$this->db->select('*');
			$this->db->where($this->field_parent_id, $id);
			$query = $this->db->get(PREFIX.$this->table_lang);
			$temp = NULL;
			if($query->result()) {
				foreach ($query->result() as $key => $item) {
					$temp[$item->lang_code] = $item;
				}
			}
			//make data_lang property in result object. IMPORTANT!!!!
			$result->data_lang = $temp;
			return $result;
		}else{
			return false;
		}
	}

	function insert_ignore ($table, $data) {

		$keys = $values = array();
		foreach ($data as $k => $item) {
			if (empty($keys)) {
				$keys = array_keys($item);
			}
			$item_values = array_values($item);
			foreach ($item_values as $key => $value) {
				$item_values [$key] = "'{$value}'";
			}
			$values[] = "(" . implode(", ", array_values($item_values)) . ")";
		}

		
			$draw_sql = 'INSERT IGNORE INTO '.$table.' ('.implode(', ', $keys).') VALUES '.implode(', ', $values);
		
		
		// echo $draw_sql; exit;
		$this->db->query($draw_sql);
	}
	
	function saveManagement($fileName=''){
		if(isset($this->lang->languages)){
			$all_lang = $this->lang->languages;
		}else{
			$all_lang = array(
				'' => ''
			);
		}
		// pr($_POST,1);

		//default data
		$_current_lang_code = '';
		$_userInfo 		= $this->session->userdata('userInfo');
		$_status 		= ($this->input->post('statusAdmincp')=='on') ? 1 : 0;
		$_created 		= $_updated = date('Y-m-d H:i:s',time());
		$_created_by 	= $_updated_by = ''; //TO DO
		$data_lang 		= $data_lang_temp = array();
		$_image_url 	= $_thumbnail_url = '';
		$_location 		= $this->input->post('articles-country');
        $_tickets 		= $this->input->post('tickets');
		$_tickets = '';// TODO
		
        $_date_start 	= $this->input->post('date_start');
        $_date_end 		= $this->input->post('date_end');
        $_time_start	= $this->input->post('time_start'); 
        $_time_end 		= $this->input->post('time_end');
		$update_id 		= $this->input->post('hiddenIdAdmincp');//lay ID từ PartURL

		$_tagsGenre	 	= !empty($this->input->post('genre_tag')) ? json_encode($this->input->post('genre_tag')) : '[]';
		
        $_tagsArtist 	= trim($this->input->post('artists'));

        $_slug_event 	= strtolower($this->input->post('slug_enAdmincp'));

        $date_time_start = $_date_start.' '.$_time_start;
		$date_time_end = $_date_end.' '.$_time_end;
		
		$_fb_url = $this->input->post('fb_url');
		
		//set up save tags artist with id user_type
		$get_artist_id =  $this->get_pro_page_id();
		$_tagsArtist_arr = json_decode ($_tagsArtist);
		$event_id_arr = $this->get_event_tags_id($update_id);

		$tagsArtist_mapping = array();
		foreach ($event_id_arr as $k => $v){
			foreach ($get_artist_id as $a_k => $a_v) {
				if($v->tags_id == $a_v->id){
					$tagsArtist_mapping[$v->tags_id] = strtolower($a_v->name);
				}
			}
		}

		// pr( $tagsArtist_mapping);//lay tags Artist tu trong DB
		 //pr( $_tagsArtist_arr);//lay tags Artist tu view
		
		$map_artist_id = array();
		$save_id_artist = array();
	
		$map_event_name = array_diff($_tagsArtist_arr,$tagsArtist_mapping);//lay phan tu moi nhap
		//xoa tat ca phan tu trong DB co ID = update_id

		// foreach($tagsArtist_mapping as $key => $val){
		// 		if(!empty($val)){
		// 			$data['tags_id'] = $key;
		// 			$this->db->delete('admin_nqt_event_tags',$data);
		// 		}
		// 	}

		if(!empty($update_id)){
			$data['event_id'] = $update_id;
			$this->db->delete('admin_nqt_event_tags',$data);
		}
		//pr($map_event_name);

		foreach ($get_artist_id as $k => $v){
			$map_artist_id[$v->id] = strtolower($v->name);
		}
		 // pr($map_artist_id,1);
		foreach($map_artist_id as $ke =>$va){
			foreach($_tagsArtist_arr as $key => $val){
				$va = preg_replace('([\s]+)', ' ', $va);
				$val = strtolower($val);
				if($val == $va){
					// $save_id_artist[]=$ke;
					$data['tags_id'] = $ke;
					$data['slug_event'] = $_slug_event;
					$data['type'] = 'artist';
					$data['event_id'] = $update_id;
					
					$this->db->insert('admin_nqt_event_tags',$data);
				}
			}
		}

		//set up save tags artist with id user_type
		$get_pro_page_id =  $this->get_pro_page_id();
		$_tagsArtist_arr = json_decode ($_tagsArtist );
		 //pr($get_pro_page_id,1);

		$map_artist_idd = array();
		// $save_id_artistt = array();
		
		foreach ($get_pro_page_id as $k => $v){
			$map_artist_idd[$v->id] = $v->name;
		}
		// pr($_tagsArtist_arr,1);
		$flag=false;
		foreach($_tagsArtist_arr as $ke => $va){
			foreach($map_artist_idd as $key => $val){
				if($val == $va){
					unset($_tagsArtist_arr[$ke]);
					// $item = array(
					// 	'name' => $va,
					// 	'key' => $ke
					// );
					//  $list[] = $item;
					// $data['name']=$va;
					// $data['key']=$ke;
					// $save_id_artist[]=$ke;
					// $data['email']="";
					// $data['url_of_page']="";
					// $data['is_approve']="";
					// $data['is_send_email_approve']="
					// $data['token']="";
					// $data['user_type_name']="";
					// $data['user_type_id']="";
					// $data['genre']="";
					// $data['country']="";
					// $data['import_id']="";
					// $data['created']="";
					// $data['created_by']="";
					// $data['updated']="";
					// $data['updated_by']="";
					// $data['status']="";
				}
			}
		}
		foreach ($_tagsArtist_arr as $k => $val) {	
			$data['name'] = $val;
			if(!empty($data)){
					$this->db->insert('pro_page_import',$data);	
			}
		}
		$data = array();
		//pr(count($_tagsArtist_arr),1);
		// if(!empty($data))
		// {
		// 	$this->insert_ignore('pro_page_import', $data);
		// // pr($data, 1);
		// }
		
		
		// $save_tags_artist = json_encode($save_id_artist);
		//end
		
		//set up save tags genre with id genre
		$get_genre_id =  $this->get_genre_id();
		$_tagsGenre_arr = json_decode ($_tagsGenre);
		//pr($_tagsGenre_arr, 1);
		$map_genre_id = array();
		$save_id_genre = array();
		
		foreach ($get_genre_id as $k => $v){
			$map_genre_id[$v->id] = strtolower($v->name);
		}

		$dataG = array();
		foreach($map_genre_id as $ke =>$va){
			foreach($_tagsGenre_arr as $key => $val){
				if($val == $va){
					// $save_id_genre[]=$ke;
					$dataG['tags_id'] = $ke;
					$dataG['slug_event'] = $_slug_event;
					$dataG['type'] = 'genre';
					$this->db->insert('admin_nqt_event_tags',$dataG);
				}
			}
		}
		
		// $save_tags_genre = json_encode($save_id_genre);
		//end
		
		// Event - Pull metadata from FB
		$image_fb_event = $this->input->post('image_fb_event');
		

		if($this->input->post('hiddenIdAdmincp')==0){
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$keyerror = '-'.$key;
					$key = '_'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}

				$fb_url = $this->input->post('fb_url');
				if(!empty($fb_url)){
					$checkFbUrl = $this->checkFbUrl($fb_url);
					// last_query(1);
					if($checkFbUrl){
						print 'error-fb_url'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						exit;
					}
				}

				/*
				$checkData = $this->checkData($this->input->post('name'.$key.'Admincp'),$key);
				if($checkData){
					print 'error-name'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
					exit;
				}
				*/
				
				$checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key);
				if($checkSlug){
					$day="-".date("d-m-y");
						$_POST['slug'.$key.'Admincp'].= $day;
					// $day=date("dmy");
					// $_POST['slug'.$key.'Admincp'] .= $day; //ddmmyy
					
					// pr($_POST,1);
					// print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
					// exit;
				}

			}
			$pre_url = $this->input->post('thumbnail_urlAdmincp');
			if( ! empty($pre_url) ) {
				
				$_thumbnail_url = move_file_from_url('admin_nqt_events', $pre_url, TRUE);
			}
			$pre_url_1 = $this->input->post('image_urlAdmincp');
			if( ! empty($pre_url_1) ) {
				
				$_image_url = move_file_from_url('admin_nqt_events', $pre_url_1, FALSE);
			}

			if ( empty($_thumbnail_url) || empty($_image_url) ) {
				if(empty($image_fb_event)){
					print 'error-image.'.$this->security->get_csrf_hash();
					exit;
				}
			}

			$data = array(
				// 'image'=> trim($fileName['image']),
				'thumbnail' 	=> $_thumbnail_url,
				'image' 		=> $_image_url,
				'status'		=> $_status,
				'created'		=> $_created,
				'created_by'	=> $_userInfo,
				'location'		=> $_location,
                'tickets' 		=> $_tickets,
                //'date_start' 	=> $_date_start,
				//'date_end' 		=> $_date_end,
				//'time_start' 	=> $_time_start,		
				//'time_end' 		=> $_time_end,
				'date_time_start' 		=> $date_time_start,
				'date_time_end' 		=> $date_time_end,				
				'tagsGenre' 	=> $_tagsGenre,				
				'tagsArtist' 	=> $_tagsArtist,
				'fb_url'		=> $_fb_url,			
			);

			if(!empty($image_fb_event)){
				$dateNow = new DateTime();
				$file_name = md5(time()).'.jpg';
				$root_path_upload = 'assets/uploads/images';
				$path = $root_path_upload."/".$dateNow->format('Y')."/".$dateNow->format('m');
				if ( ! file_exists ( $path ) ) {
			        mkdir ( $path, 0777, true );
			    }
			    $image_name= $path."/".$file_name;
				file_put_contents($image_name, file_get_contents($image_fb_event)); //copy hinhtu url ve
				$data['thumbnail']=$image_name;
				$data['image']=$image_name;
			}
			
			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['name'.$key] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				//make data language
				$data_lang_temp['name'] 		= trim(htmlspecialchars($this->input->post('name' . $subfix)));
				$data_lang_temp['slug'] 		= trim($this->input->post('slug' . $subfix));
				$data_lang_temp['description'] 	= trim($this->input->post('description' . $subfix));
				$data_lang_temp['created'] 		= $_created;
				$data_lang_temp['status'] 		= $_status;
				$data_lang_temp['lang_code'] 	= $_current_lang_code;
				$data_lang_temp['thumbnail']  	= $_thumbnail_url;
				$data_lang_temp['image']      	= $_image_url;
				$data_lang_temp['location']     = $_location;
				$data_lang_temp['tagsGenre']    = $_tagsGenre;
				$data_lang_temp['tagsArtist']   = $_tagsArtist;
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			
			//DO INSERT DATA
			if($this->db->insert(PREFIX.$this->table,$data)){
				$insert_id = $this->db->insert_id();
				//Insert data language
				foreach ($data_lang as $key => $item) {
					$item[$this->field_parent_id] = $insert_id;
					$this->db->insert(PREFIX.$this->table_lang, $item);
				}
				//End insert data language
				modules::run('admincp/saveLog',$this->module,$insert_id,'Add new','Add new');
				return true;
			}
		}else{
			$result = $this->getDetailManagement($this->input->post('hiddenIdAdmincp'));
			//pr($result, 1);
			//Kiểm tra đã tồn tại chưa?
			foreach($all_lang as $key=>$val){
				if($key!=''){
					$_current_lang_code = $key;
					$keyerror = '-'.$key;
					$key = '_'.$key;
				}else{
					$key = '';
					$keyerror = '';
				}
				$name = 'name'.$key;
				$slug = 'slug'.$key;
				/*
				if($result->$name!=$this->input->post('name'.$key.'Admincp')){
					$checkData = $this->checkData($this->input->post('name'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
					if($checkData){
						print 'error-name'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						exit;
					}
				}
				*/
				if($result->$slug!=$this->input->post('slug'.$key.'Admincp')){
					$checkSlug = $this->checkSlug($this->input->post('slug'.$key.'Admincp'),$key,$this->input->post('hiddenIdAdmincp'));
					if($checkSlug){
						$day="-".date("d-m-y");
						$_POST['slug'.$key.'Admincp'].= $day; //ddmmyy
						// pr($_POST,1);
						// print 'error-slug'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
						// exit;
					}
				}

				// $checkFbUrl = $this->checkFbUrl($this->input->post('fb_url'));
				// if($checkFbUrl){
				// 	print 'error-fb_url'.$keyerror.'-exists.'.$this->security->get_csrf_hash();
				// 	exit;
				// }
			}
			
			//Xử lý xóa hình khi update thay đổi hình
			if($fileName['image']==''){
				$fileName['image'] = $result->image;
			}else{
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result->image);
			}
		
			
			if( ! empty($_thumbnail_url) ) {
				$data['thumbnail'] = $_thumbnail_url;
			}
			if( ! empty($_image_url) ) {
				$data['image'] = $_image_url;
			}
			//$data['date_start']= $_date_start;
			//$data['date_end']= $_date_end;
			//$data['time_start']= $_time_start;
			//$data['time_end']= $_time_end;
			$data['date_time_start']= $date_time_start;
			$data['date_time_end']= $date_time_end;
			$data['status'] = $_status;
			$data['updated'] = $_updated;
			$data['tagsGenre'] = $_tagsGenre;
			$data['location']     = $_location;
			$data['tagsArtist']   = $_tagsArtist;

			foreach($all_lang as $key=>$val){
				$_current_lang_code = $key;
				$key = ($key != '') ? '_' . $key : $key;
				$subfix = $key . 'Admincp';

				$data['name'.$key] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				$data['slug'.$key] = trim($this->input->post('slug' . $subfix));
				//make data language
				$data_lang_temp['name'] = trim(htmlspecialchars($this->input->post('name' . $subfix)));
				$data_lang_temp['slug'] = trim($this->input->post('slug' . $subfix));
				$data_lang_temp['updated'] = $_created;
				$data_lang_temp['status'] = $_status;
				$data_lang_temp['lang_code'] = $_current_lang_code;
				if( ! empty($_thumbnail_url) ) {
					$data_lang_temp['thumbnail'] = $_thumbnail_url;
				}
				if( ! empty($_image_url) ) {
					$data_lang_temp['image'] = $_image_url;
				}
				$data_lang[] = $data_lang_temp;
				unset($data_lang_temp);
			}
			$update_id = $this->input->post('hiddenIdAdmincp');
			$old_value[] = $result;
			unset($data['created'], $data['updated']);
			modules::run('admincp/saveLog',$this->module,$this->input->post('hiddenIdAdmincp'),'','Update', $old_value, $data);
			//DO UPDATE DATA
			$this->db->where('id', $update_id);
			if($this->db->update(PREFIX.$this->table,$data)){
				//last_query(1)
				//Update data in table language
				foreach ($data_lang as $key => $item) {
					$lang_code = $item['lang_code'];
					unset($item['lang_code']);
					$this->db->where('lang_code', $lang_code);
					$this->db->where($this->field_parent_id, $update_id);
					$this->db->update(PREFIX.$this->table_lang, $item);
				}
				//end update data language
				return true;
			}
		}
		return false;
	}

	function softDeleteData ($id) {
		
		$data ['is_delete'] = 1;
		$this->db->where('id', $id);
		if ($this->db->update (PREFIX.$this->table, $data)) {
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			//Soft-delete data in table language
			$this->db->where ($this->field_parent_id, $id);
			$this->db->update (PREFIX.$this->table_lang, $data);
			//end soft-delete data language
			return TRUE;
		}
		return FALSE;
	}
	
	function checkData($name,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('name'.$lang,$name);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		//$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function checkSlug($slug,$lang,$id=0){
		$this->db->select('id');
		$this->db->where('slug'.$lang,$slug);
		if($id!=0){
			$this->db->where_not_in('id',array($id));
		}
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}

	function checkFbUrl($fb_url){
		$this->db->like('fb_url', $fb_url);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($limit,$start){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->limit($limit,$start);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotal(){
		$this->db->select('id');
		$this->db->where('status',1);
		$query = $this->db->count_all_results(PREFIX.$this->table);

		if($query > 0){
			return $query;
		}else{
			return false;
		}
	}
	
	function getDetail($slug){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where('slug_'.$this->lang->lang(),$slug);
		$this->db->limit(1);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getOther($id){
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->where_not_in('id',array($id));
		$this->db->limit(5);
		$this->db->order_by('id','random');
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_artist_id(){
		$this->db->select('*');
		$query = $this->db->get('user_type');
		return $query->result();
	}
	function get_pro_page_id(){
		$this->db->select('*');
		$query = $this->db->get('pro_page_import');
		return $query->result();
	}
	function get_event_tags_id($event_id){
		$this->db->select('*');
		$this->db->where('event_id', $event_id);
		$query = $this->db->get('admin_nqt_event_tags');
		return $query->result();
	}
	
	function get_genre_id(){
		$this->db->select('*');
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('admin_nqt_genre');
		return $query->result();
	}
	function get_artist_idd(){
			$this->db->select('*');
			$query = $this->db->get('pro_page_import');
			return $query->result();
	}
	function get_propage(){
		$this->db->select('*');
		$query = $this->db->get('pro_page');
		return $query->result();
	}

	function get_propage_by_user_type($user_type_id) {
		$this->db->select('pro_page.id AS propage_id, pro_page.user_id AS propage_userid, pro_page.email AS propage_email, pro_page.user_type_id AS propage_usertypeid');
		$this->db->select('ui.id AS ui_id, ui.username AS ui_username');
		$this->db->join('admin_nqt_user_info AS ui', 'ui.id=pro_page.user_id', 'LEFT');
		$this->db->where('pro_page.user_type_id', $user_type_id);
		$query = $this->db->get('pro_page');
		return $query->result();
	}

	function getAllData() {
		$this->db->select('id, name_en, name_vi');
		$this->db->where('status',1);
		#check soft delete
		$this->db->where('is_delete', 0);
		$query = $this->db->get(PREFIX.$this->table);

		if($query->result()){
			return $query->result();
		}else{
			return false;
		}
	}

	function get_event_pro_users($event_id)
	{
		$this->db->select('*');
		$this->db->where('anepu_event_id', $event_id);

		$query = $this->db->get('admin_nqt_event_pro_users');

		return $query->result();
	}

	
	function get_event_genres($event_id)
	{
		$this->db->select('*');
		$this->db->where('aneg_event_id', $event_id);
		
		$query = $this->db->get('admin_nqt_event_genres');

		return $query->result();
	}
}