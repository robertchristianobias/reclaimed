<script type="text/javascript">token_value = '<?=$this->security->get_csrf_hash()?>';</script>

<div class="dataTables_wrapper no-footer">
	<div class="table-scrollable">
		<table class="table table-striped table-bordered table-hover dataTable no-footer">
			<thead>
				<?php
					if (isset($this->lang->languages))
					{
						$all_lang = $this->lang->languages;
					}
					else
					{
						$all_lang = array(
							'' => ''
						);
					}
				?>
				<tr role="row">
					<th class="table-checkbox sorting_disabled" width="20"><input type="checkbox" id="selectAllItems" onclick="selectAllItems(<?=count($result)?>)"></th>

					<th class="sorting_disabled" width="50">Image</th>
					<?php foreach($all_lang as $key=>$val) {  ?>
						<th class="sorting" width="200" onclick="sort('name<?php echo ($key!='') ?  '_'.$key :  '' ?>')" id="name<?php echo ($key!='') ?  '_'.$key :  '' ?>">EventName<?php echo ($key!='') ?  ' - '.mb_strtoupper($key) :  '' ?></th>
					<?php } ?>
					<th class="center sorting" width="60" onclick="sort('status')" id="status">Location</th>
					<th class="center sorting" width="20" onclick="sort('status')" id="status">Status</th>
					<th class="center sorting" width="50">Genres</th>
					<th class="center sorting" width="50">Pro User</th>
					<th class="sorting" width="80">Event Date/Time</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if($result)
					{
						$i = 0;
						foreach($result as $k=>$v) {
				?>
				<tr class="item_row<?=$i?> gradeX <?php echo ($k%2==0) ?  'odd' :  'even' ?>" role="row">
					<td><input type="checkbox" id="item<?=$i?>" onclick="selectItem(<?=$i?>)" value="<?=$v->id?>"></td>
					<td class="center">
						<a href="<?=PATH_URL_ADMIN.$module.'/update/'.$v->id?>">
							<?php  if (!empty($v->thumbnail)) { ?>
								<?php if ( file_exists($v->thumbnail)) { ?>
									<img alt="" src="<?=get_resource_url($v->thumbnail)?>" />
								<?php } else { ?>
									<img src="<?php echo $v->thumbnail; ?>" style="max-height: 60px;" />
								<?php } ?>
							<?php } else if (!empty($v->image_fb_event)) { ?>
								<img alt="" src="<?=$v->image_fb_event?>" style="max-height: 60px;" />
							<?php } ?>
						</a>
					</td>
					<?php
						foreach($all_lang as $key=>$val)
						{
							$name = ($key!='') ? 'name_'.$key : 'name';
					?>
					<td>
						<a href="<?=PATH_URL_ADMIN.$module.'/update/'.$v->id?>"><?=$v->$name?></a>
					</td>
					<?php 
					} 
					?>										
					<td class="center"><?=$v->location?></td>
					<td class="center" id="loadStatusID_<?=$v->id?>"><a class="no_underline" href="javascript:void(0)" onclick="updateStatus(<?=$v->id?>,<?=$v->status?>,'<?=$module?>')"><?php echo ($v->status==0) ?  '<span class="label label-sm label-default">Blocked</span>' :  '<span class="label label-sm label-success">Approved</span>' ?></a></td>
					<td class="center">
						<?php
							
							$tags_genre = json_decode($v->tagsGenre);
						?>
						<select name="genres[]" class="form-control select2 genre_select" multiple="multiple" style="width: 120px;" data-rec_id="<?php echo $v->id; ?>">
							<?php if ( $genres ) { ?>
								<?php foreach ( $genres as $genre ) { ?>
									<option <?php echo is_array($tags_genre) ? (in_array($genre->name, $tags_genre) ? 'selected="selected"' : '') : ''; ?> value="<?php echo $genre->name; ?>"><?php echo ucwords(strtolower($genre->name)); ?></option>
								<?php } ?>
							<?php } ?>
						</select>
						
					</td>
					<td class="center">
						<?php
							$tagged_pro_users = $this->model->get_event_pro_users($v->id);
							$tpo_arr = array();
							if ( $tagged_pro_users )
							{
								foreach ( $tagged_pro_users as $tpo )
								{
									$tpo_arr[] = $tpo->anepu_pro_user_id;
								}
							} 
						?>
						<select name="pro_user[]" class="form-control select2 pro_user_select" multiple="multiple" style="width: 120px;" data-rec_id="<?php echo $v->id; ?>">
							<?php if ( $user_types ) { ?>
								<?php foreach ( $user_types as $user_type ) { ?>
									<?php
										$pro_pages = $this->model->get_propage_by_user_type($user_type->id);
									?>
									<?php if ( $user_type->type != 'Reader') { ?>
										<optgroup label="<?php echo $user_type->type; ?>">
											<?php if ($pro_pages) { ?>
												<?php foreach ( $pro_pages as $pro_page) { ?>
													<option <?php echo (in_array($pro_page->propage_id, $tpo_arr) ? 'selected="selected"' : ''); ?> value="<?php echo $pro_page->propage_id; ?>"><?php echo ucwords(strtolower($pro_page->ui_username)); ?></option>
												<?php } ?>
											<?php } ?>
										</optgroup>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</select>

					</td>
					<td>
						<b>From: </b><?php echo date('F d, Y @ h:i a', strtotime($v->date_time_start)); ?> <br />
						<b>To: </b><?php echo date('F d, Y @ h:i a', strtotime($v->date_time_end)); ?>
					</td>
				</tr>
				<?php 
					$i++;
					}
				}else
				{ 
				?>
				<tr class="gradeX odd" role="row">
					<td class="center no-record" colspan="20">No record</td>
				</tr>
				<?php
				} 
				?>
			</tbody>
		</table>
	</div>

	<?php 
	if($result){ 
	?>
	<div class="row">
		<div class="col-md-5 col-sm-12">
			<?php 
			if(($start+$per_page)<$total){ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$start+$per_page?> of <?=$total?> entries</div>
			<?php 
			}
			else
			{ 
			?>
			<div class="dataTables_info" style="padding-left:0;margin-top:3px">Showing <?=$start+1?> to <?=$total?> of <?=$total?> entries</div>
			<?php 
			} 
			?>
		</div>

		<div class="col-md-7 col-sm-12">
			<div class="dataTables_paginate paging_bootstrap_full_number" style="margin-top:3px">
				<ul class="pagination" style="visibility: visible;">
					<?=$this->adminpagination->create_links();?>
				</ul>
			</div>
		</div>
	</div>
	<?php 
	} 
	?>
</div>

<script type="text/javascript">
	$(function() {
		$('.genre_select').select2();
		$('.genre_select').on('change', function (e) {

			var $genres = e.val;
				$rec_id = $(this).data('rec_id');

			$.post(root + 'admincp_events/tag_genres', {
				rec_id: $rec_id,
				genres: $genres
			}, function(data) {

			});
		});

		$('.pro_user_select').select2();
		$('.pro_user_select').on('change', function (e) {

			var $pro_users = e.val;
				$rec_id = $(this).data('rec_id');

			$.post(root + 'admincp_events/tag_pro_user', {
				rec_id: $rec_id,
				pro_users: $pro_users
			}, function(data) {

			});
		});
	});
</script>