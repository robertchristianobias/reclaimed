<?php
$is_testing = !empty($_GET['test']);
?>

<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>

<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovaeditor.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovamanager.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.core.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.ajax.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.arrow.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.autocomplete.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.clear.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.filter.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.focus.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.prompt.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.suggestions.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.tags.js'?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places"></script>

<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.core.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.arrow.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.autocomplete.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.clear.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.focus.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.prompt.css'?>" rel="stylesheet" type="text/css">
<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.tags.css'?>" rel="stylesheet" type="text/css">
<style>
.text-core .text-wrap .text-prompt {
	top: 1px;
}
</style>
<?php
	if(isset($this->lang->languages)){
		$all_lang = $this->lang->languages;
	}else{
		$all_lang = array(
			'' => ''
		);
	}
?>
<script type="text/javascript">
$(document).ready( function(){
	<?php foreach($all_lang as $key=>$val){ ?>
	$("#name<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp").slugIt({
		events: 'keyup blur',
		output: '#slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp',
		map: {
			'!':'-',
			'(': '-',
			')': '-'
		},
		space: '-'
	});
	
	tinymce.init({    
            selector: '#description<?php echo ($key!='') ?  '_'.$key : '' ?>Admincp',
            plugins: "code, link, image, lists, preview, textcolor, pix_embed_online_media, responsivefilemanager,table",
            toolbar: ['undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect forecolor backcolor | bullist numlist outdent indent | link image | print preview media fullpage removeformat','pix_embed_online_media | responsivefilemanager| table'],
			fontsize_formats: "8px 10px 11px 12px 14px 18px 24px 36px",
			
            external_filemanager_path:"<?=PATH_URL?>/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            relative_urls: false,
            //external_plugins: { "filemanager" : "assets/filemanager/plugin.min.js"}

              // toolbar: "anchor",
              // menubar: "insert"
        });
    //End FOR WYSIWYG content
	<?php } ?>
});
$(document).ready(function(){
  $(".btnclear").click(function(){
  	$(".event_image").hide();
	
  	document.getElementById("frmManagement").reset();
  	
  	
   	
  });
  
});

function save(){
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}

var flag_save_new = false;
function save_and_new(){
	flag_save_new = true;
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}
// function clear_all(){
// 	// document.getElementById("frmManagement").reset();
// 	$("#frmManagement").reset();
// 	// $('input[type=checkbox]').prop('checked',false); 
// 	$('.event_image').hide();
// }


function showRequest(formData, jqForm, options) {
	var form = jqForm[0];
	<?php if(empty($id)) { ?>
	if (
		form.thumbnail_urlAdmincp.value == '' || 
		form.image_urlAdmincp.value == ''
	) {
		if(is_image_fb_event_valid){
			// Do nothing
			pr('is_image_fb_event_valid = ' + is_image_fb_event_valid);
		} else {
			// $('#txt_error').html('Please Upload image.');
			// show_perm_denied();
			// return false;
		}
	}
	<?php } ?>
	<?php foreach($all_lang as $key=>$val){ ?>	
	if (
		form.name<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp.value == '' || 
		form.slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp.value == '' ||
		form.date_start.value == '' ||
		form.date_end.value == ''
	) {
		$('#txt_error').html('Please enter information.');
		show_perm_denied();
		return false;
	}
	<?php } ?>
//
}

function showResponse(responseText, statusText, xhr, $form) {
	responseText = responseText.split(".");
	token_value  = responseText[1];
	$('#csrf_token').val(token_value);
	if(responseText[0]=='success'){
		<?php if($id==0){ ?>
			// alert('showResponse');
		if(flag_save_new){
			location.reload(); // Keep the url and refresh
			flag_save_new = false; // Rest
		} else {
			location.href=root+module+"/#/save";
		}
		<?php }else{ ?>
		if($('.form-upload').val() != ''){
			$.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
				var res = data.split("src=");
				$('.fileinput-filename').html('');
				$('.fileinput').removeClass('fileinput-exists');
				$('.fileinput').addClass('fileinput-new');
			});
		}
		show_perm_success();
		<?php } ?>
	}
	
	if(responseText[0]=='error-image'){
		$('#txt_error').html('Only upload image.');
		show_perm_denied();
		return false;
	}

	if(responseText[0]=='error-fb_url<?php echo ($key!='') ?  '-'.$key :  '' ?>-exists'){
		$('#txt_error').html('Facebook event exists.');
		show_perm_denied();
		return false;
	}
	
	<?php foreach($all_lang as $key=>$val){ ?>
	if(responseText[0]=='error-name<?php echo ($key!='') ?  '-'.$key :  '' ?>-exists'){
		$('#txt_error').html('name<?php echo ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '' ?> already exists.');
		show_perm_denied();
		$('#name<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp').focus();
		return false;
	}
	
	if(responseText[0]=='error-slug<?php echo ($key!='') ?  '-'.$key :  '' ?>-exists'){
		$('#txt_error').html('Slug<?php echo ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '' ?> already exists.');
		show_perm_denied();
		$('#slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp').focus();
		return false;
	}
	<?php } ?>

	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
}
</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-name"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
		<li><a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a><i class="fa fa-angle-right"></i></li>
		<li><?php echo ($this->uri->segment(4)=='') ?  'Add new' :  'edit' ?></li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-cascade">
			<div class="portlet-name">
				<div class="caption">
					<i class="fa fa-globe"></i>Form Information
				</div>
			</div>
			
			<div class="portlet-body form">
				<div class="form-body notification" style="display:none">
					<div class="alert alert-success" style="display:none">
						<strong>Success!</strong> The page has been saved.
					</div>
					
					<div class="alert alert-danger" style="display:none">
						<strong>Error!</strong> <span id="txt_error"></span>
					</div>
				</div>
				
				<!-- BEGIN FORM-->
				<form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Facebook Event's url</label>
							<div class="col-md-3">
								<div class="tickets">
									<?php
									$fb_url = $is_testing ? 'https://www.facebook.com/events/280354142819414/' : '';
									?>
									<input value="<?=isset($result->fb_url) ? $result->fb_url : $fb_url?>" type="text" name="fb_url" id="fb_url" class="form-control"/>
								</div>
							</div>
							<div class="col-md-2">
								<button onclick="return fb_url_pull_data()" type="button" class="btn green">Auto pull data</button>
							</div>	
							<div class="col-md-2">
								<button type="button" class="btn green btnclear" id="clear-all"><i class="fa fa-pencil"></i> Clear all</button>
							</div>
							<div class="col-md-2">
								<button onclick="save_and_new()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save and new</button>
							</div>
						</div>
										 <div class="form-group">	
								<label class="control-label col-md-3">
									Tags genre
									<span class="required" aria-required="true">*</span>
								</label>
								<div class="col-md-9">
									<div>
									<?php
										$genre_list = modules::run('admincp_genres/getGenre');
										//pr($genre_list);
										$genretag = isset($result->tagsGenre) ? $result->tagsGenre : '[]';
										$tagsGenre_arr = json_decode($genretag);
										//pr($tagsGenre_arr);
										
										foreach($genre_list as $genre) {
											$checked = '';
											foreach($tagsGenre_arr as $val) {
												if($val == $genre->name) {
													$checked = 'checked';
												}
											}
									?>
										
											<div class="col-md-3">
												<input class="genre" value="<?=$genre->name?>" type="checkbox" name="genre_tag[]" <?=$checked?>>
														<?=$genre->name?>
											</div>
										<?php } ?>
									</div>
								</div>				
							</div> 
						


						<?php /*get avatar url*/
	                        $image_url = ( ! empty($result->image) ) ? get_resource_url($result->image) : '';
							//$thumbnail_url = ( ! empty($result->thumbnail) ) ? get_resource_url($result->thumbnail) : null;
							
							if ( !empty($result->image) )
							{
								if (file_exists($result->image))
								{
									$image_url = get_resource_url($result->image);
								}
								else
								{
									$image_url = $result->image;
								}
							}

							if ( !empty($result->thumbnail) )
							{
								if (file_exists($result->thumbnail))
								{
									$thumbnail_url = get_resource_url($result->thumbnail);
								}
								else
								{
									$thumbnail_url = $result->thumbnail;
								}
							}

		                ?>
						<div class="form-group">
							<label class="control-label col-md-3">Event Photo</label>
							<div class="col-md-3">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group input-large">
										<div class="col-md-4 col-xs-12">
                                           <div>
                                                <input type="button" name="input_avatar" value="Select File" class="update-img-upload " id="cropContainerHeaderButton" />
                                                <!-- image thumbnail -->
												<input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
												<!-- image original -->
												<input type="hidden" id="input_image_url" name="image_urlAdmincp">

                                            </div>
                                            <div class="img-profile">
                                            	<div id="cropic_element" style="display:none"></div>
	                                            <a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>">
	                                                <img id="preview_image" src="<?=$thumbnail_url?>"> 
	                                            </a>
                                            </div>
											
											<div class="img-profile">
	                                            <a>
	                                                <img class="event_image" src="" />
													<input type="hidden" value="" name="image_fb_event" class="image_fb_event" />
	                                            </a>
                                            </div>
											
                                        </div>
									</div>
								</div>
							</div>
						</div>

						<?php 
						if(isset($this->lang->languages)){ 
						?>
						<div class="form-group last" style="padding-bottom:0;">
							<label class="control-label col-md-3">Language</label>
							<div class="col-md-9">
								<ul class="nav nav-tabs">
									<?php $flag = false; ?>
									<?php 
									foreach ($all_lang as $key => $value) { 
									?>
										<li class="<?= $flag == false ? 'active' : ''; $flag = true; ?>"><a href="#<?=$value?>" data-toggle="tab" aria-expanded="true"><?=ucwords($value)?></a></li>
									<?php 
									} 
									?>
								</ul>
							</div>
						</div>
						<?php 
						} 
						?>
						<div class="tab-content">
							<?php $flag = false; ?>
							<?php
								foreach ($all_lang as $key => $value){
									$name = ($key!='') ? 'name_'.$key : 'name';
									$slug = ($key!='') ? 'slug_'.$key : 'slug';

									$name_lable = ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '';
									$name_id = ($key!='') ?  '_'.$key :  '';

									$slug_lable = ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '';
									$slug_id = ($key!='') ?  '_'.$key :  '';

									$description_lable = ($key!='') ?  ' ('.mb_strtoupper($key).')' :  '';
									$description_id = ($key!='') ?  '_'.$key :  '';


									//$country = ($key!='') ? 'country_'.$key : 'country';
									//description, content
							?>
							<div class="tab-pane fade <?=$flag == false ? 'active in' : ''; $flag = true; ?>" id="<?=$value?>">
								<div class="form-group">
									<label class="control-label col-md-3">
										Event Name<?php echo $name_lable ?> 
										<span class="required" aria-required="true">*</span>
									</label>
									<div class="col-md-9">
										<input data-required="1" type="text" class="form-control" 
											name="name<?php echo $name_id ?>Admincp" 
											id="name<?php echo $name_id ?>Admincp"
											value="<?php echo (isset($result->data_lang[$key]->name) ? $result->data_lang[$key]->name : '') ?>"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">
										Slug<?php echo $slug_lable ?> 
									</label>
									<div class="col-md-9">
										<input data-required="1" type="text" class="form-control" 
											name="slug<?php echo $slug_id ?>Admincp" 
											id="slug<?php  echo $slug_id ?>Admincp" 
											value="<?php echo (isset($result->data_lang[$key]->slug) ? $result->data_lang[$key]->slug : '') ?>"
											/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">
										Description<?php echo $description_lable ?> 
									</label>
									<div class="col-md-9">
										<textarea data-required="1" cols="" rows="6"
											name="description<?php echo $description_id ?>Admincp" id="description<?php echo $description_id ?>Admincp"><?php echo (isset($result->data_lang[$key]->description) ? $result->data_lang[$key]->description : '') ?></textarea>
									</div>
								</div>
							</div>
							<?php 
							} 
							?>

							<div class="form-group">
									<label class="control-label col-md-3">
										Start Date
										<span class="required" aria-required="true">*</span>
									</label>
									<div class="col-md-3">
										<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
											<input value="<?php echo(isset($result->date_start))? $result->date_start : '' ?>" name="date_start" id="date_start" class="form-control event_date_start"/>
										</div>
									</div>
									<label class="control-label col-md-3">
										End Date
										<span class="required" aria-required="true">*</span>
									</label>
									<div class="col-md-3">
										<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
											<input value="<?php echo(isset($result->date_end))? $result->date_end : '' ?>" name="date_end" id="date_end" class="form-control event_date_end"/>
										</div>
									</div>
								
							</div>
							
							<div class="form-group">
									<label class="control-label col-md-3">
										Start Time
										<span class="required" aria-required="true">*</span>
									</label>
									<div class="col-md-3">						
										<div class="input-group input-large " >
											<input value="<?php echo(!empty($result->time_start))? $result->time_start : '' ?>" type="text" name="time_start" id ='time_start'  class="form-control center event_time_start timepicker-24 "/>
										</div>
									</div>
									<label class="control-label col-md-3">
										End Time
										<span class="required" aria-required="true">*</span>
									</label>
									<div class="col-md-3">						
										<div class="input-group input-large " >
											<input value="<?php echo(!empty($result->time_end))? $result->time_end : '' ?>" type="text" name="time_end" id ='time_end' class="form-control center event_time_end timepicker-24"/>
										</div>
									</div>
							</div>
							<div class="form-group">
								        <script type="text/javascript">
                                function initialize4() {
									var options = {types: ['(regions)']}; //regions
									var input = document.getElementById('countryAdmincp');
									var autocomplete = new google.maps.places.Autocomplete(input , options);
                                    google.maps.event.addListener(autocomplete, 'place_changed', 
                                        function() {
											var address_components=autocomplete.getPlace().address_components;
                                            pr(address_components,1);
											var country='';
                                            var cities = '';
                                            var location ='';
                                            var address ='';
											for(var j =0 ;j<address_components.length;j++)
											{

                                                if(address_components[j].types[0]=='country')
												{
												   country=address_components[j].long_name;
												   country_short_name=address_components[j].short_name;
												}else if(address_components[j].types[0]=='locality')
                                                {
                                                   cities=address_components[j].long_name;
                                                   cities_short_name=address_components[j].short_name;
                                                }
                                                else if(address_components[j].types[0]=='administrative_area_level_1')
                                                {
                                                   cities=address_components[j].long_name;
                                                   cities_short_name=address_components[j].short_name;
                                                }
                                                
                                                if(cities != ''){
                                                    location=cities+"_"+country;
                                                }else{
                                                    location=country;
                                                }
											}
											//document.getElementById('data').innerHTML="City Name : <b>" + city + "</b> <br/>Country Name : <b>" + country + "</b>";
											// document.getElementById('articles-country').value = country;
											$("#articles-country").tagit("createTag", location);
											$("#countryAdmincp").val('');
                                        });
                                }
                                google.maps.event.addDomListener(window, 'load', initialize4);
                            </script>
								<label class="control-label col-md-3">
									Location
									<span class="required" aria-required="true">*</span>
								</label>
								<div class="col-md-9">
								<script type="text/javascript">
									$(document).ready(function() {
										$("#articles-country").tagit({ 
											autocomplete: {delay: 0, minLength: 2},
											showAutocompleteOnFocus: false,
											removeConfirmation: false,
											caseSensitive: true,
											allowDuplicates: false,
											allowSpaces: false,
											readOnly: false,
											tagLimit: null,
											singleField: false,
											singleFieldDelimiter: ',',
											singleFieldNode: null,
											tabIndex: null,
											placeholderText: null,

											// Events
											beforeTagAdded: function(event, ui) {
												console.log(ui.tag);
											},
											afterTagAdded: function(event, ui) {
												console.log(ui.tag);
											},
											beforeTagRemoved: function(event, ui) {
												console.log(ui.tag);
											},
											onTagExists: function(event, ui) {
												console.log(ui.tag);
											},
											onTagClicked: function(event, ui) {
												console.log(ui.tag);
											},
											onTagLimitExceeded: function(event, ui) {
												console.log(ui.tag);
											}
										});
									});
								</script>
									<input data-required="1" type="text" class="form-control event_location" 
										name="countryAdmincp" 
										id="countryAdmincp"
										value=""
										/>
									<!--<input type="hidden" name="country" id="articles-country" multiple="multiple"  value="<?php echo(isset($result->country) ? $result->country : '') ?>">
									<input type="hidden" name="city" id="articles-city" multiple="multiple"  value="<?php echo(isset($result->city) ? $result->city : '') ?>">
									<input type="hidden" name="country" id="articles-country" value="<?php echo(isset($result->country) ? $result->country : '') ?>">
									<input type="hidden" name="city" id="articles-city" value="<?php echo(isset($result->city) ? $result->city : '') ?>">-->

									<input type="text" name="articles-country" id="articles-country" value="<?php echo(isset($result->location) ? $result->location : '') ?>" class="event_location_country">
								</div>
							</div>
				
						 <div class="form-group">	
                            <label class="control-label col-md-3">
	                                Tags artist
	                                <span class="required" aria-required="true">*</span>
	                            </label>
	                            <div class="col-md-9 tags-input-auto" data-name="tags-input">
	                                <style>
	                                    .tags-input-auto .text-core{
	                                        width: 100% !important;
	                                        height: 35px !important; 
	                                    }
	                                    .tags-input-auto .text-wrap{
	                                        width: 100% !important;
	                                        height: 35px !important;
	                                    }
	                                    #artists{
	                                        width: 100% !important;
	                                        height: 35px !important;
	                                        border: 1px solid #e1e1e1;
	                                        color: #000;
	                                    }
	                                    
	                                </style>
	                                <div>
	                                    <textarea id="artists" name="artists" class="form-control" rows="1"></textarea>                                </div>
	                                <?php //pr($result, 1) //$genre = modules::run('admincp_events/get_genre'); pr($genre, 1) ?>
	                                <script type="text/javascript">
	                                    $('#artists')
	                                        .textext({
	                                            plugins : 'tags autocomplete arrow',
	                                            tagsItems:  <?=(!empty($result->tagsArtist) ? trim($result->tagsArtist) : "''")?>,
	                                            //console.log(tagsItems);
	                                        })
	                                        .bind('getSuggestions', function(e, data)
	                                        {
	                                            <?php
	                                            $artist = modules::run('admincp_events/get_artist');
	                                            $artist_arr = array();
	                                            foreach($artist as $k => $v){
	                                                $artist_arr[] = preg_replace('([\s]+)', ' ', $v->name);
	                                            }
	                                            $artist_select = json_encode($artist_arr);
	                                          //pr($artist_select,1);
	                                            ?>
	                                            var list = <?=$artist_select?>,
	                                                textext = $(e.target).textext()[0],
	                                                query = (data ? data.query : '') || ''
	                                                ;
	
	                                            $(this).trigger(
	                                                'setSuggestions',
	                                                { result : textext.itemManager().filter(list, query) }
	                                            ); 
	                                        });
	                                </script>
	                            </div>				
	                        </div> 
							<!-- <div class="form-group">	
								<label class="control-label col-md-3">
									Tags artist
								</label>
								<div class="col-md-9 tags-input-auto" data-name="tags-input">
									<style>
										.tags-input-auto .text-core{
											width: 100% !important;
											height: 35px !important; 
										}
										.tags-input-auto .text-wrap{
											width: 100% !important;
											height: 35px !important;
										}
										#artists{
											width: 100% !important;
											height: 35px !important;
											border: 1px solid #e1e1e1;
											color: #000;
										}
										
									</style>
									<div>
										<textarea id="artists" name="artists" class="example" rows="1" ></textarea>
									</div>
									<script type="text/javascript">
										$('#artists')
											.textext({
												plugins : 'tags autocomplete arrow',
						                            tagsItems: <?=(isset($result->artist_tags) ? $result->artist_tags : '[]')?>
											})
											.bind('getSuggestions', function(e, data)
											{
												var list = [
														'Artist',
														'Venue',
														'Label',
														'Promoter',
														'Marketer'
													],
													textext = $(e.target).textext()[0],
													query = (data ? data.query : '') || ''
													;

												$(this).trigger(
													'setSuggestions',
													{ result : textext.itemManager().filter(list, query) }
												);
											})
											;
									</script>
								</div>				
							</div>
							 -->
							<!--
							<div class="form-group">
								<label class="control-label col-md-3">Link Tickets</label>
								<div class="col-md-3">
									<div class="tickets">
										<input value="<?php echo(isset($result->tickets))? $result->tickets : '' ?>" type="text" name="tickets" id="tickets" class="form-control"/>
									</div>
								</div>								
							</div>
							-->
						</div>
					</div>
						<div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-9">
								<div class="checkbox-list">
									<label class="checkbox-inline">
										<div class="checkbox"><span><input <?php if(isset($result->status)){ if($result->status==1){ ?>checked="checked"<?php }}else{ ?>checked="checked"<?php } ?> type="checkbox" name="statusAdmincp"></span></div>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Post to FB Page</label>
							<div class="col-md-9">
								<div class="checkbox-list">
									<label class="checkbox-inline">
										<?php
										$attr_yes = 'checked="checked"';
										$input_attr = $attr_yes; // Default yes (Add New)
										if(isset($result->flag_fb_post)){ // Update
											if(!empty($result->flag_fb_post)){
												// Do nothing
											} else {
												$input_attr = '';
											}
										}
										?>
										<div class="checkbox"><span><input <?=$input_attr?> type="checkbox" name="statusAdmincp"></span></div>
									</label>
								</div>
							</div>
						</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button onclick="save()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save</button>
								<button onclick="save_and_new()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save and new</button>
								<a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
							</div>
						</div>
					</div>

				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->

<style type="text/css">
	#cropic_element {
		height: 200px;
		width: 200px;
	}
</style>
<script>

	$('#clear-all').click(function(event) {
		$('.checked').removeClass('checked');
		
		$(".tagit-choice").remove();
		// $(".text-core").val()="";
		$(".text-label ").remove();
		$(".text-button").remove();
	});
    var croppicHeaderOptions = {
            
            uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
            cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
            uploadData:{'prefix':'avatar_'},
            enableMousescroll:true,
            customUploadButtonId:'cropContainerHeaderButton',
            outputUrlId:'input_thumbnail_url',
            modal:true,
            rotateControls: false,
            doubleZoomControls:false,
            imgEyecandyOpacity:0.4,
            onBeforeImgUpload: function(){ },
            onAfterImgUpload: function(){ appendOriginImageAward(); },
            onImgDrag: function(){ },
            onImgZoom: function(){ },
            onBeforeImgCrop: function(){ },
            onAfterImgCrop:function(){ appendAward(); },
            onReset:function(){ onResetCropic(); },
            onError:function(errormessage){ console.log('onError:'+errormessage) }
    }   
    var croppic = new Croppic('cropic_element', croppicHeaderOptions);
    function appendOriginImageAward() {
        var url_origin = $("div#croppicModalObj > img").attr('src');
        $('#input_image_url').val(url_origin);
        $("#preview_image_fancybox").attr("href", $('#input_image_url').val());
    }
    function appendAward(){
        $("#preview_image").attr("src", $('#input_thumbnail_url').val());
        $("#preview_image").show();
    }
    function onResetCropic(){
    	$('#input_image_url').val('');
    	$('#input_thumbnail_url').val('');
    	$("#preview_image_fancybox").attr("href", '');
    	$("#preview_image").attr("src", '');
    }
   
	var is_image_fb_event_valid = false;
	var FB_URL_PULL_DATA_LIMIT = 1000;
	var fb_url_pull_data_count = 0;
	function fb_url_pull_data(){
		fb_url_pull_data_count = 0;
		fb_url_pull_data_from_url();
	}
	var fb_url_pull_data_from_url_timer;
	function fb_url_pull_data_from_url(){
		fb_url_pull_data_count++;
		if(fb_url_pull_data_count <= FB_URL_PULL_DATA_LIMIT){
			pr('fb_url_pull_data_from_url() - fb_url_pull_data_count = ' + fb_url_pull_data_count);
		} else {
			pr('fb_url_pull_data_from_url() - END - fb_url_pull_data_count = ' + fb_url_pull_data_count);
			return;
		}
		
		var url = '<?=PATH_URL.'cms/admincp_getEvents_facebook/fb_url_pull_data'?>';
		var fb_url = $('#fb_url').val();
		// pr('fb_url_pull_data - fb_url = ' + fb_url);
		if(fb_url){
			$.post(url, {'fb_url': fb_url}, function(response){
				// pr('fb_url_pull_data - response');
				// pr(response);
				
				if(response){
					var data = $.parseJSON(response);
					// pr('fb_url_pull_data - data');
					pr(data);
					
					if(data.obj && data.obj.title){
						clearTimeout(fb_url_pull_data_from_url_timer);
						
						var obj = data.obj;
						
						// Name
						$('input#name_enAdmincp').val(obj.title);
						$('input#name_enAdmincp').keyup(); // Trigger to generate slug
						
						// Description
						var description = obj.description;
						description = decodeURIComponent(escape(window.atob(description)));
						// $('textarea[name=description_enAdmincp]').val(description);
						$('textarea[name=description_enAdmincp]').val(description);
						// Sets the HTML contents of the activeEditor editor
						tinymce.activeEditor.setContent(description);
						
						if(obj.event_image){
							is_image_fb_event_valid = true;
							
							$('.event_image').attr('src', obj.event_image);
							$('input.image_fb_event').val(obj.event_image);
						}
						$('input.event_date_start').val(obj.event_date_start);
						$('input.event_date_end').val(obj.event_date_end);
						$('input.event_time_start').val(obj.event_time_start);
						$('input.event_time_end').val(obj.event_time_end);
						$('input.event_location').val(obj.event_location);
						$("#articles-country").tagit("createTag", obj.event_location_country); // Tag Country
					} else {
						clearTimeout(fb_url_pull_data_from_url_timer);
						fb_url_pull_data_from_url_timer = setTimeout(function(){
							fb_url_pull_data_from_url(); // Call Again - Recursive
						}, 3000);
					}
				}
			});
		}
		
		return false;
	}
</script>