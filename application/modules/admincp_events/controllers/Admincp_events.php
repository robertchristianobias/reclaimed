<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_events extends MX_Controller {

	private $module = 'admincp_events';
	private $table = 'page_item';
	private $table1 = 'admin_nqt_events';
	private $table_lang = 'page_item_lang';
	function __construct(){
		parent::__construct();
		$this->load->model($this->module.'_model','model');
		$this->load->model('admincp_modules/admincp_modules_model');
		if($this->uri->segment(1)==ADMINCP){
			if($this->uri->segment(2)!='login'){
				if(!$this->session->userdata('userInfo')){
					header('Location: '.PATH_URL_ADMIN.'login');
					exit;
				}
				$get_module = $this->admincp_modules_model->check_modules($this->uri->segment(2));
				$this->session->set_userdata('ID_Module',$get_module[0]->id);
				$this->session->set_userdata('Name_Module',$get_module[0]->name);
			}
			$this->template->set_template('admin');
			$this->template->write('title','Admin Control Panel');
		}
	}
	/*------------------------------------ Admin Control Panel ------------------------------------*/
	public function admincp_index(){
		// modules::run('admincp_getevents_facebook/fb_event_control_post'); // TODO - TEST
		permission_force_check('r');
		$default_func = 'created';
		$default_sort = 'DESC';
		$data = array(
			'module'=>$this->module,
			'module_name'=>$this->session->userdata('Name_Module'),
			'default_func'=>$default_func,
			'default_sort'=>$default_sort
		);
		$this->template->write_view('content','BACKEND/index',$data);
		$this->template->render();
	}
	
	public function admincp_update($id=0){
		if($id==0){
			permission_force_check('w');
		}else{
			permission_force_check('r');
		}

		$result = array();
		if($id!=0){
			$result = $this->model->getDetailManagement($id);

			$temp_date_time_start = ($result->date_time_start!="0000-00-00 00:00:00")?explode(' ',$result->date_time_start):'';
			$result->date_start = !empty($temp_date_time_start[0])?$temp_date_time_start[0]:'';
			$result->time_start = !empty($temp_date_time_start[1])?$temp_date_time_start[1]:'';

			$temp_date_time_end = ($result->date_time_end!="0000-00-00 00:00:00")?explode(' ',$result->date_time_end):'';
			$result->date_end = !empty($temp_date_time_end[0])?$temp_date_time_end[0]:'';
			$result->time_end = !empty($temp_date_time_end[1])?$temp_date_time_end[1]:'';
		}
		$data = array(
			'result'=>$result,
			'module'=>$this->module,
			'id'=>$id
		);
		// pr($data,1);
		$this->template->write_view('content','BACKEND/ajax_editContent',$data);
		$this->template->render();
	}
	// old tags artist 
	public function admincp_old_tags(){
		$flag = false;
		$get_tags_old_event_arr = $this->get_tagsArtist_arr();
		$getListProPage = $this->get_list_pro_page();

		// Debug
		$debug_duplicate = array(
			// 'first' => array(),
			// 'deleted' => array(),
		);

		// Step 1: Delete duplicates
		$pro_mapping = array();
		foreach ($getListProPage as $pro) {
			$name = $pro['name'];
			if(!empty($pro_mapping[$name])){
				// Found duplicates and delete them
				$debug_duplicate[] = array('deleted' => $pro); // TODO - Debug

				// TODO - Delete by id
				$data['id'] = $pro['id'];
				$this->db->delete('pro_page_import', $data);
			} else {
				// pr('first');
				$debug_duplicate[] = array('first' => $pro);  // TODO - Debug
				$pro_mapping[$name] = $pro;
			}
		}

		//debug_log_set_path('debug_admincp_old_tags.txt');
		$log_arr = array(
            'location' => __FILE__,
			'function' => 'admincp_old_tags',
			// '$pro_mapping' => !empty($pro_mapping) ? $pro_mapping : '',
            '$debug_duplicate' => !empty($debug_duplicate) ? $debug_duplicate : '',
        );
		//debug_log_from_config($log_arr);
		// exit();

		pr(count($pro_mapping));//Ä‘áº¿m  sá»‘ dÃ²ng bá»‹ trÃ¹ng

		//old tags -----------------------
		
		$mapping = array();
		$tags_old_event_mapping = array();
		$tagsArtist_arr = array();
		$structure_tags = array(
			'id' => array(
				'tagsArtist' => array(),
				'slug_en' => array(),
				)
			);
		$i =1;
		//pr($get_tags_old_event_arr,1);
		foreach ($get_tags_old_event_arr as $key => $tags) {
			$tags_id = $tags['id'];
			$type = 'event';
			$slug_event = $tags['slug_en'];
			$tagsArtist_list = json_decode($tags['tagsArtist']);
			$result_tags_old [$tags_id] = ['slug_event' => $slug_event,'type' => $type,'event_id' => $tags_id];
			foreach ($tagsArtist_list as $key => $tags_old) {
				$tags_old_list[] = ['tags_id' => $tags_id,'tags_old' => $tags_old];
			}
			
		}
		//pr($tags_old_list,1);
		//pr($result_tags_old,1);
		$get_list_id_tags = array();
		//láº¥y ID tags theo tab pro_page bá»Ÿi tags_old 
		foreach ($tags_old_list as $tags_old) {
			$_tags_old = strtolower($tags_old['tags_old']);
			$_tags_id = $tags_old['tags_id'];
			 foreach ($getListProPage as $k => $tags_pro) {
				$tags_name = strtolower($tags_pro['name']);
				
				if($tags_name == $_tags_old){
					$get_list_id_tags[ $_tags_id][] = ['tags_id' => $tags_pro['id']];
				}
			}
		}
		//pr($get_list_id_tags,1);
		// so sÃ¡nh id vÃ  get_list
		$result = array();
		foreach ($result_tags_old as $key => $rel) {
			//pr($key);
			foreach ($get_list_id_tags as $key_tags => $tag) {
				//pr($key_tags,1);
				if($key_tags == $key){
					foreach ($tag as $val) {
						$result[$key][] = array_merge($rel,$val);

						
					}
					
				}
			}
		}
		$i = 0;
		if(!empty($result)){
			foreach ($result as $key => $arr_insert) {
				foreach ($arr_insert as $data) {
					$this->db->insert('admin_nqt_event_tags', $data);
					$i++;
				}
				// pr(count($arr_insert));
			}
		}
		// exit;
		// pr($result,1);
		print 'ThÃ nh cÃ´ng -- Sá»‘ dÃ²ng Ä‘Æ°á»£c thÃªm: ' . $i;
	}

	function get_tagsArtist_arr()
	{
		$this->db->select('id,tagsArtist,slug_en');
		$this->db->where('tagsArtist != "" AND tagsArtist != "[]"');
		$query = $this->db->get('admin_nqt_events');
		//last_query(1);
		return $query->result_array();
	}
	function get_list_pro_page()
	{
		$this->db->select('*');
		// $this->db->where('tagsArtist != "" AND tagsArtist != "[]"');
		$this->db->order_by('id','ASC');
		$query = $this->db->get('pro_page_import');

		//last_query(1);
		return $query->result_array();
	}
	//end old tags artist 
	public function admincp_save(){
		permission_force_check('w');
		if($_POST){
			//Upload Image
			$fileName = array('image'=>'');
			if($_FILES){
				foreach($fileName as $k=>$v){
					if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
						$typeFileImage = strtolower(mb_substr($_FILES['fileAdmincp']['type'][$k],0,5));
						if($typeFileImage == 'image'){
							$tmp_name[$k] = $_FILES['fileAdmincp']["tmp_name"][$k];
							$file_name[$k] = $_FILES['fileAdmincp']['name'][$k];
							$ext = strtolower(mb_substr($file_name[$k], -4, 4));
							if($ext=='jpeg'){
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-5))).'.jpg';
							}else{
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-4))).$ext;
							}
						}else{
							// Event - Pull metadata from FB
							$image_fb_event = $this->input->post('image_fb_event');
							if(empty($image_fb_event)){
								print 'error-image.'.$this->security->get_csrf_hash();
								exit;
							}
						}
					}
				}
			}
			//End Upload Image

			if($this->model->saveManagement($fileName)){
				// Save files
				modules::run('admincp_getevents_facebook/fb_event_control_post');
				
				//Upload Image
				if($_FILES){
					if($_FILES){
						$upload_path = BASEFOLDER.DIR_UPLOAD_NEWS;
						check_dir_upload($upload_path);
						foreach($fileName as $k=>$v){
							if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
								move_uploaded_file($tmp_name[$k], $upload_path.$fileName[$k]);
							}
						}
					}
				}
				//End Upload Image
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_ajaxLoadContent(){
		$this->load->library('AdminPagination');
		$config['total_rows'] = $this->model->getTotalsearchdescription();
		$config['per_page'] = $this->input->post('per_page');
		$config['num_links'] = 3;
		$config['func_ajax'] = 'searchContent';
		$config['start'] = $this->input->post('start');
		$this->adminpagination->initialize($config);

		$result = $this->model->getsearchdescription($config['per_page'],$this->input->post('start'));

		$data = array(
			'result'	 => $result,
			'per_page'	 => $this->input->post('per_page'),
			'start'		 => $this->input->post('start'),
			'module'	 => $this->module,
			'total'		 => $config['total_rows'],
			'user_types' => $this->model->get_artist_id(),
			'genres'	 => $this->model->get_genre_id()
		);
		$this->session->set_userdata('start',$this->input->post('start'));
		$this->load->view('BACKEND/ajax_loadContent',$data);
	}
	
	/*public function admincp_delete() {
		permission_force_check('d');
		$id = $this->input->post('id');
		if ( ! empty($id )) {
			
			
			if ($this->model->softDeleteData($id)){
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}*/
	public function admincp_delete(){
		permission_force_check('d');

		if($this->input->post('id')){
			$id = $this->input->post('id');
			$result = $this->model->getDetailManagement($id);
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			$this->db->where('id',$id);
			 // pr($result,1);
			if($this->db->delete($this->table1)){
				//pr('b',1);
				//$this->db->delete($this->table_lang);
				//XÃ³a hÃ¬nh khi Delete
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result[0]->image);
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_ajaxGetImageUpdate($id){
		$result = $this->model->getDetailManagement($id);
		print resizeImage(PATH_URL.DIR_UPLOAD_NEWS.$result->image,250);exit;
	}
	
	public function get_genre(){
		$this->load->model('admincp_events_model');
		$genre = $this->admincp_events_model->get_genre_id();
		return $genre;
	}
	public function get_artist(){
		$this->load->model('admincp_events_model');
		$genre = $this->admincp_events_model->get_artist_idd();
		return $genre;
	}
	public function get_propage(){
		$propage = $this->model->get_propage();
		return $propage;
	}
	/*------------------------------------ End Admin Control Panel --------------------------------*/
	
	public function test_fb(){
		$this->create_fb_posts();
	}
	function create_fb_posts() {
		$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		$page_id = '206857356765382'; // REDM Test Page
		// $page_id = '179106292455265'; // Reclaim.EDM
		// $message = 'Test #hashtag1';
		$message = 'Hello fans';
		// $access_token = 'EAAW5VKEmfJ8BAGX6DnUEhKHRF1fZCeKkvVYeX6OUSkMUswyEMeIyRKz2i4RAlUyMjeFTnO3NKXKlVwKMxhlrKqlkZAveaSoySlKuuVteLHmPsXvOR7b1FivzO19tQr5Hv7ZBNdMIp12YqNJHY2FnOqdv3Mbgfczjl7m3pF9KwZDZD';
		$access_token = 'EAAW5VKEmfJ8BALUeH2feZAoZABltCEPV80sNi52QOKHBPwdGo724kVSXaKE49s0RWp18xyVZC09ZADZBcQydFtZAlTLyY0FZCMcd8ABWHCmdsnTfhysBIiYXfsZCWCd2zq0ygSVhZADrYTYaaFM0XZBQaSB9mZBRZAd8d4zugDO3fJSY9gZDZD'; // REDM Test Page
		// $access_token = 'EAAW5VKEmfJ8BAOA4fHUzHlOHDHnIEidC3IZBTQ9CCYpimIqvZABTzqhRjBYPKZBRmCjQ8cRa1JXJU0kQzG6B8ZAWs65fQ1sFiIIr8fS0Kkz2hcldqpkrdI8PxmWURXjc0ZBH8Hx6SuSql37CL3InJprZCrXQKhTJxZB9m55cpzGwQZDZD'; // Reclaim.EDM
		
		// $url = "https://graph.facebook.com/{$page_id}/feed?message={$message}&access_token={$access_token}";
		// $url = "https://graph.facebook.com/v3.2/page-id/feed?message={$message}&access_token={$access_token}";
		$url = "https://graph.facebook.com/v2.10/page-id/feed?access_token={$access_token}";
		// pr($url,1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_URL,$url);
		$output = curl_exec($ch);
		if (curl_error($ch)) {
			$error_msg = curl_error($ch);
		}
		curl_close($ch);
		if (isset($error_msg)) {
			echo $error_msg;
		} else {
			echo $output;
		}
		
		return $output;
	}

	public function admincp_upload_csv()
	{	
		$path = BASEFOLDER . 'assets/uploads/csv';
		$config['upload_path']    = $path;
		$config['allowed_types']  = 'csv';
		$config['max_size']       = 2048;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			$response['success'] = FALSE;
			$response['message'] = '';
			$response['error']  = $this->upload->display_errors();
		}
		else
		{
			$data = $this->upload->data();

			$response['success'] = TRUE;
			$response['message'] = '';
			$response['file'] = 'assets/uploads/csv/'. $data['file_name'];

		}

		echo json_encode($response); exit;
	}

	public function admincp_process_csv()
	{
		$this->load->library('form_validation');
		$this->load->library('csv_reader');

		$this->form_validation->set_rules('import-csv', 'Required', 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

		if ($this->form_validation->run($this) == FALSE)
		{
			$response['success'] = FALSE;
			$response['message'] = 'Some fields are required';
			$response['errors']  = array(
				'import-csv' => form_error('import-csv')
			);
		}
		else
		{
			$csv	= $this->input->post('import-csv');
			$data 	= $this->csv_reader->parse_file($csv);

			if ( $data )
			{
				set_time_limit(0);

				// Start Transaction
				$this->db->trans_start();
				
				foreach ( $data as $key => $val )
				{
					if ( ! empty($val['URL'])) 
					{
						$url = $val['URL'];
						
						// process url
						$event_data = $this->__fetch_events($url);
						$event_data = (object) $event_data;

						$insert_data = array(
							'name_en' 			=> $event_data->name_en,
							'slug_en' 			=> $event_data->slug_en,
							'thumbnail'	 		=> $event_data->thumbnail,
							'image' 			=> $event_data->image,
							'date_time_start' 	=> date('Y-m-d H:i:s', strtotime($event_data->date_time_start)),
							'date_time_end' 	=> date('Y-m-d H:i:s', strtotime($event_data->date_time_end)),
							'fb_url' 			=> $event_data->fb_url,
							'location' 			=> $event_data->location
						);

						$this->db->insert('admin_nqt_events', $insert_data);

						$this->db->trans_complete();

						if($this->db->trans_status() === FALSE)
						{
							log_message('error', 'CSV importing failed.');
						}
					}
				
				}

				// delete the csv
				if ( file_exists($csv) )
				{
					//unlink($csv);						
				}
						
				$response = array(
					'success'  			=> TRUE,
					'message'  			=> 'Successfully imported the events',
					'redirect' 			=> site_url('events'),
				);
			}
			else
			{
				$response['success'] = FALSE;
				$response['message'] = 'Error';
				$response['errors']  = array();
			}
		}

		echo json_encode($response); exit;
	}

	public function admincp_process_event_url()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('fb_event_url', 'Required', 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

		if ($this->form_validation->run($this) == FALSE)
		{
			$response['success'] = FALSE;
			$response['message'] = 'Some fields are required';
			$response['errors']  = array(
				'fb_event_url' => form_error('fb_event_url')
			);
		}
		else
		{
			$fb_event_url	= $this->input->post('fb_event_url');
			set_time_limit(0);

			// check if event url exists
			$this->db->select('*');
			$this->db->where('fb_url', $fb_event_url);
			$query = $this->db->get('admin_nqt_events');
			$result = $query->result();

			if ( count($result) > 0 ) // update
			{
				$response['success'] = FALSE;
				$response['message'] = 'FB event exists';
				$response['errors']  = array();
			}
			else // insert
			{
				$event_data = $this->__fetch_events($fb_event_url);
				$event_data = (object) $event_data;

				$insert_data = array(
					'name_en' 			=> $event_data->name_en,
					'slug_en' 			=> $event_data->slug_en,
					'thumbnail'	 		=> $event_data->thumbnail,
					'image' 			=> $event_data->image,
					'date_time_start' 	=> date('Y-m-d H:i:s', strtotime($event_data->date_time_start)),
					'date_time_end' 	=> date('Y-m-d H:i:s', strtotime($event_data->date_time_end)),
					'fb_url' 			=> $event_data->fb_url,
					'location' 			=> $event_data->location,
					'description'		=> $event_data->description
				);

				$insert = $this->db->insert('admin_nqt_events', $insert_data);
				$event_id = $this->db->insert_id();

				if ( $insert )
				{
					// insert to table lang
					$insert_lang = $this->db->insert('admin_nqt_events_lang', array(
						'name'		  => $event_data->name_en,
						'slug' 		  => $event_data->slug_en,
						'description' => $event_data->description,
						'event_id'	  => $event_id,
						'lang_code'   => 'en',
						'status' 	  => '0',
						'is_delete'   => '0'
					));
					
					$response = array(
						'success'  			=> TRUE,
						'message'  			=> 'Successfully imported the event',
						'redirect' 			=> site_url('events'),
					);
				}
				else
				{
					$response['success'] = FALSE;
					$response['message'] = 'Error';
					$response['errors']  = array();
				}
			}
		}

		echo json_encode($response); exit;
	}

	public function admincp_push_to_fb()
	{
		// $message = '
		// 	THU 8 Aug #CityName
		// 	www.reclaimedm.com/events/whateverourdbgeneratesfortheeventpgurl';

		// get events for the day
		$curr_time   = date('H:00', strtotime('+1 hour'));
		$future_time = date('H:00', strtotime('+2 hours'));
		
		$this->db->select('*');
		$this->db->where("((DATE(NOW()) BETWEEN DATE(date_time_start) AND DATE(date_time_end)))");
		$this->db->where("(date_time_start >= '".$curr_time."')" );
		#$this->db->where("(date_time_start >= '".$curr_time."' AND date_time_start < '".$future_time."')" );
		
		$query = $this->db->get('admin_nqt_events');
		$results = $query->result();

		if ( $results )
		{
			$fb = new \Facebook\Facebook([
				'app_id' 				=> '1507773589365138',
				'app_secret' 			=> 'f051d81e3c2cac4d566852a940a22c30',
				'default_graph_version' => 'v2.10'
			]);

			foreach ( $results as $result )
			{
				$msg = date('D j M', strtotime($result->date_time_start)) . ' #' . str_replace(' ', '', $result->location) . "\r\n" .
					   base_url('events/'.date('Y/m', strtotime($result->date_time_start)).'/'.$result->slug_en);

				try {
					// Returns a `FacebookFacebookResponse` object
					$response = $fb->post(
						'/101546444536468/feed',
						array (
							'message' => $msg
						),
						'EAAVbTZBUyIZAIBAC7wwycUjRCQdVJEwJnbkokqAq4aLa2hgK4ZAn1cpblHUbLlXPZB4ZBtPLducu4I33pjy8uLZBFYWXDVxKXLAJEZCanZAIlwLjSBDTAJ6ODyXXrkkMRHiCOlxhvp3vsoyQvD7BWmLbLEHRpP2V8zAjZB1XqR0hx5eZCsNW5EFZCxaMw74EXwb8juOAfWHdHFI2hVPb8Sg94ZAy'
					);
				} catch(FacebookExceptionsFacebookResponseException $e) {
					echo 'Graph returned an error: ' . $e->getMessage();
					exit;
				} catch(FacebookExceptionsFacebookSDKException $e) {
					echo 'Facebook SDK returned an error: ' . $e->getMessage();
					exit;
				}
		
				$graphNode = $response->getGraphNode();
		
				echo '<pre>';
				print_r($graphNode);
			} 
		}
		
		// get page access token
		// Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
		//   $helper = $fb->getRedirectLoginHelper();
		//   $helper = $fb->getJavaScriptHelper();
		//   $helper = $fb->getCanvasHelper();
		//   $helper = $fb->getPageTabHelper();
	}

	public function admincp_tag_genres()
	{
		$event_id = $this->input->post('rec_id');
		$genres  = $this->input->post('genres');

		if ( $_POST )
		{
			$data = array(
				'tagsGenre' => json_encode($genres)
			);

			$this->db->where('id', $event_id);
			$this->db->update('admin_nqt_events', $data);
		}
	}

	public function admincp_tag_pro_user()
	{
		$event_id = $this->input->post('rec_id');
		$pro_users_id = $this->input->post('pro_users');

		if ( $_POST )
		{
			// delete the events
			$this->db->delete('admin_nqt_event_pro_users', array(
				'anepu_event_id' => $event_id
			));

			foreach ( $pro_users_id as $user_id )
			{
				$data[] = array(
					'anepu_event_id' => $event_id,
					'anepu_pro_user_id' => $user_id
				);
			}

			$this->db->insert_batch('admin_nqt_event_pro_users', $data);

		}


	}

public function admincp_testfb()
	{
		echo 'abc';
		//$text = '[This] is a [test] string, [eat] my [shorts].';
		//preg_match_all("/\[[^\]]*\]/", $text, $matches);
		//var_dump($matches[0]);

		$url = 'https://www.facebook.com/events/475592809675162';
		$event_data = $this->__fetch_events($url);
		
		echo '<pre>';
		//var_dump($event_data);
		preg_match_all("/\[[^\]]*\]/", $event_data['description'], $matches);;
		var_dump($matches['0']);
    $find = $matches['0'];
    
    if ( $find )
    {
      foreach ($find as $val )
      {
        list($a, $b, $anchor_text) = explode(':', $val);
        
        $anchor_text_1[] = str_replace(']', '', $anchor_text);
  
      }
       
      echo '<pre>';
      print_r($anchor_text_1);
      
     // echo $desc;
    }
    
    $desc = str_replace($find, $anchor_text_1, $event_data['description']);
    $desc = str_replace('@', '', $desc);
    
    echo $desc;
    
		exit;
	}
 
	private function __fetch_events($url)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Host: www.facebook.com",
				"Postman-Token: f447c767-1fe6-491c-b723-036809ec7ef3,7978dde9-969a-4c72-bc14-dbdd2062ce4e",
				"User-Agent: PostmanRuntime/7.15.0",
				"accept-encoding: gzip, deflate",
				"cache-control: no-cache"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
			echo "cURL Error #:" . $err;
		} 
		else 
		{
			// echo $response;

			$html = new DOMDocument();
			@$html->loadHTML($response);

			$xpath 	 = new DOMXPath($html);
			$content = $xpath->query('//script[@type="application/ld+json"]');
			$images  = $xpath->query('//div[@class="uiScaledImageContainer _3ojl"]/img/@src');

			$img_arr = array();
			foreach ( $images as $img)
			{
				$img_arr[] = $img;
			}

			if ( count($content) > 0 )
			{
				$nodes = array();
				foreach ($content as $tag) {
					$nodes[] = $tag;
				}

				if ( $nodes )
				{
					if (isset($nodes['0']))
					{
						$text_content = json_decode($nodes['0']->textContent);
                            
						preg_match_all("/\[[^\]]*\]/", $text_content->description, $matches);;
						$find = $matches['0'];
						
						if ( $find )
						{
							foreach ($find as $val )
							{
								list($a, $b, $anchor_text) = explode(':', $val);
								
								$anchor_text_1[] = str_replace(']', '', $anchor_text);
							}
						}
						
						$desc = str_replace($find, $anchor_text_1, $text_content->description);
						$desc = str_replace('@', '', $desc);

						$event_data = array(
							'success'		   => TRUE,
							'name_en' 	  	   => $text_content->name,
							'slug_en'	  	   => url_title($text_content->name, '-', true),
							'thumbnail'   	   => isset($img_arr['0']->value) ? $img_arr['0']->value : $text_content->image,
							'image'		  	   => isset($img_arr['0']->value) ? $img_arr['0']->value : $text_content->image,
							'date_time_start'  => date('F d, Y h:i a', strtotime($text_content->startDate)),
							'date_time_end'    => date('F d, Y h:i a', strtotime($text_content->endDate)),
							'fb_url'           => $url,
							'location'     	   => $text_content->location->address->addressLocality,
							'description' 	   => nl2br($desc),
							//'location'       => $text_content->location->name . ' ' . $text_content->location->address->streetAddress . ' ' . $text_content->location->address->addressLocality,
							// 'photo'		   => $text_content->image
						);

						return $event_data;
					}
				}
			}
			else
			{
				$response = array(
					'success' => FALSE
				);

				return $response;
			}
		}
	}
}