<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admincp_contact extends MX_Controller {

	private $module = 'admincp_contact';
	private $table = 'admin_nqt_mailing_list_member';
	function __construct(){
		parent::__construct();
		$this->load->model($this->module.'_model','model');
		$this->load->model('admincp_modules/admincp_modules_model');
		$this->load->helper('main_helper.php');
		if($this->uri->segment(1)==ADMINCP){
			if($this->uri->segment(2)!='login'){
				if(!$this->session->userdata('userInfo')){
					header('Location: '.PATH_URL_ADMIN.'login');
					exit;
				}
				$get_module = $this->admincp_modules_model->check_modules($this->uri->segment(2));
				$this->session->set_userdata('ID_Module',$get_module[0]->id);
				$this->session->set_userdata('Name_Module',$get_module[0]->name);
			}
			$this->template->set_template('admin');
			$this->template->write('title','Admin Control Panel');
		}
	}
	/*------------------------------------ Admin Control Panel ------------------------------------*/
	public function admincp_index(){
		permission_force_check('r');
		$default_func = 'created';
		$default_sort = 'DESC';
		$mailing_list = $this->model->get_mailing_list();
		$data = array(
			'module'       =>$this->module,
			'module_name'  =>$this->session->userdata('Name_Module'),
			'default_func' =>$default_func,
			'default_sort' =>$default_sort,
			'mailing_list' =>$mailing_list,
		);
		$this->template->write_view('content','BACKEND/index',$data);
		$this->template->render();
	}
	
	
	public function admincp_update($id=0){
		if($id==0){
			permission_force_check('w');
			$mailing_list = $this->model->get_mailing_list();
			$list_country = $this->model->list_country();
		}else{
			permission_force_check('r');
		}
		$result[0] = array();
		if($id!=0){
			$result = $this->model->getDetailManagement($id);
			$mailing_list = $this->model->get_mailing_list();
			$list_country = $this->model->list_country();
		}
		$data = array(
			'result'=>$result[0],
			'mailing_list'=>$mailing_list,
			'list_country'=>$list_country,
			'module'=>$this->module,
			'id'=>$id
		);
		$this->template->write_view('content','BACKEND/ajax_editContent',$data);
		$this->template->render();
	}

	public function admincp_save(){
		permission_force_check('w');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}
		if($_POST){
			//Upload Image
			$fileName = array('image'=>'');
			if($_FILES){
				foreach($fileName as $k=>$v){
					if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
						$typeFileImage = strtolower(mb_substr($_FILES['fileAdmincp']['type'][$k],0,5));
						if($typeFileImage == 'image'){
							$tmp_name[$k] = $_FILES['fileAdmincp']["tmp_name"][$k];
							$file_name[$k] = $_FILES['fileAdmincp']['name'][$k];
							$ext = strtolower(mb_substr($file_name[$k], -4, 4));
							if($ext=='jpeg'){
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-5))).'.jpg';
							}else{
								$fileName[$k] = date('Y').'/'.date('m').'/'.md5(time().'_'.SEO(mb_substr($file_name[$k],0,-4))).$ext;
							}
						}else{
							print 'error-image.'.$this->security->get_csrf_hash();
							exit;
						}
					}
				}
			}
			//End Upload Image

			if($this->model->saveManagement($fileName)){
				//Upload Image
				if($_FILES){
					if($_FILES){
						$upload_path = BASEFOLDER.DIR_UPLOAD_NEWS;
						check_dir_upload($upload_path);
						foreach($fileName as $k=>$v){
							if(isset($_FILES['fileAdmincp']['error'][$k]) && $_FILES['fileAdmincp']['error'][$k]!=4){
								move_uploaded_file($tmp_name[$k], $upload_path.$fileName[$k]);
							}
						}
					}
				}
				//End Upload Image
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_delete(){
		permission_force_check('d');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$result = $this->model->getDetailManagement($id);
			modules::run('admincp/saveLog',$this->module,$id,'Delete','Delete');
			$this->db->where('id',$id);
			if($this->db->delete($this->table)){
				//Xóa hình khi Delete
				@unlink(BASEFOLDER.DIR_UPLOAD_NEWS.$result[0]->image);
				print 'success.'.$this->security->get_csrf_hash();
				exit;
			}
		}
	}
	
	public function admincp_import(){
		$default_func = 'created';
		$default_sort = 'DESC';
		$data = array(
			'module'=>$this->module,
			'default_func'=>$default_func,
			'default_sort'=>$default_sort,
			'is_normal_template'=>false,
		);
		$this->template->write_view('content','BACKEND/import_email',$data);
		$this->template->render();
	}
	
	public function admincp_ajaxLoadContent(){
		$this->load->library('AdminPagination');
		$config['total_rows'] = $this->model->getsearchContent();
		$config['per_page'] = (int)$this->input->post('per_page');
		$config['start'] = (int)$this->input->post('start');
		$config['num_links'] = 3;
		$config['func_ajax'] = 'searchContent';
		$this->adminpagination->initialize($config);

		$result = $this->model->getsearchContent($config['per_page'],$this->input->post('start'));
		$data = array(
			'result'=>$result,
			'per_page'=>$this->input->post('per_page'),
			'start'=>$this->input->post('start'),
			'module'=>$this->module,
			'total'=>$config['total_rows']
		);
		$this->session->set_userdata('start',$this->input->post('start'));
		$this->load->view('BACKEND/ajax_loadContent',$data);
	}
	
	public function admincp_ajaxUpdateStatus(){
		permission_force_check('a');
		if($perm=='permission-denied'){
			print $perm.'.'.$this->security->get_csrf_hash();
			exit;
		}else{
			
			modules::run('admincp/saveLog',$this->module,$this->input->post('id'),'update',$this->input->post('status'),$status);
			$this->db->where('id', $this->input->post('id'));
			$this->db->update($this->table, $data);
		}
		
		$update = array(
			'status'=>$status,
			'id'=>$this->input->post('id'),
			'module'=>$this->module
		);
		$this->load->view('BACKEND/ajax_updateStatus',$update);
	}
	
	public function admincp_ajaxGetImageUpdate($id){
		$result = $this->model->getDetailManagement($id);
		print resizeImage(PATH_URL.DIR_UPLOAD_NEWS.$result[0]->image,250);exit;
	}
	/*------------------------------------ End Admin Control Panel --------------------------------*/
	
	/*------------------------------------ import Email --------------------------------*/
	
	public function admincp_upload(){
		$log_arr = array(
			'location' => __FILE__ ,
			'function' => 'admincp_upload',
		);
		debug_log_from_config($log_arr);
		try{
			if(!empty($_FILES)){
				$upload_dir = BASEFOLDER.IMPORT_EMAIL;
				$upload_config['allowed_types'] = '*';
				$upload_config['encrypt_name'] = true;
				$upload_config['upload_path'] = $upload_dir;
				$this->load->library('upload', $upload_config);
				
				// DEBUG
				$log_arr = array(
					'location' => __FILE__ ,
					'function' => 'admincp_upload',
					'upload_config' => !empty($upload_config) ? $upload_config : '',
				);
				debug_log_from_config($log_arr);
				
				if($this->upload->do_upload("image_file")){
					$uploaded_data = $this->upload->data();
					$file_name = $uploaded_data['file_name'];
					$import_file_path = $upload_dir.$file_name;
					$import_data['type'] = 'email';
					$import_data['file_name'] = $file_name;
					$import_data['created'] = getNow();
					$import_data['user_id'] = (int)$this->session->userdata('userId');
					if($this->model->insert('import',$import_data)){
						$import_id = $this->db->insert_id();
						$data['file_name'] = $file_name;
						$data['import_id'] = $import_id;
						echo json_encode($data);
					}
				} else {
					$errors = $this->upload->display_errors();
					pr($errors,1);
				}
			}
		} catch (Exception $ex) {
			$exception_error = $ex->getMessage();
		}
		
		// DEBUG
		$log_arr = array(
			'location' => __FILE__ ,
			'function' => 'admincp_upload',
			'upload_config' => !empty($upload_config) ? $upload_config : '',
			'data' => !empty($data) ? $data : '',
			// 'exception_error' => !empty($exception_error) ? $exception_error : '',
		);
		debug_log_from_config($log_arr);
	}

	function admincp_list_sheetnames(){
		$worksheetNames = array();
		$file_name = $this->input->post('file_name');
		if(!empty($file_name)){
			$file_path = BASEFOLDER.IMPORT_EMAIL.$file_name;
			if(file_exists($file_path)){
				$objReader = phpexcel_get_obj_reader();
				if(!empty($objReader)){
					$worksheetNames = $objReader->listWorksheetNames($file_path);
				}
			}
		}
		echo json_encode($worksheetNames);
	}

	function admincp_import_item_by_sheetname(){
		
		$is_debug = false;
		// BEGIN: DEBUG
		if($is_debug){
			$start_mem = memory_get_usage();
			$start_mem = (int)($start_mem/1000);
			$start_mem = number_format($start_mem, 0, ',', '.');
			echo '$start_mem = '.$start_mem.'KB<br/>';
		}
		// END: DEBUG
        
		$file_name = $this->input->post('file_name'); 
		$import_id = (int)$this->input->post('import_id');
		$sheet_name = $this->input->post('sheet_name');
		$result = '';
		if(!empty($file_name) && !empty($sheet_name)){
			$file_path = BASEFOLDER.IMPORT_EMAIL.$file_name;
			if(file_exists($file_path)){
				$objReader = phpexcel_get_obj_reader();
				$objReader->setLoadSheetsOnly(array($sheet_name));
            }
			
			// FOR CHECKING WHETHER THE TEMPLATE OF IMPORT FILE IS CORRECT!
			$is_correct_import_template = true;
			
			// LOAD EXCEL FILE: HEAVY TASK!
			$objPHPExcel = $objReader->load($file_path);
			$objSheet = $objPHPExcel->getActiveSheet();
			$highestRow = $objSheet->getHighestRow();
			$highestColumn = $objSheet->getHighestColumn();
			
			if($is_correct_import_template){
				
				$count_empty_username = 0;
				for ($row = 1; $row <= $highestRow; $row++){
					$rowData_arr = $objSheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
													NULL,
													TRUE,
													FALSE);
					$rangeToArray_index = 0;
					if(!empty($rowData_arr[$rangeToArray_index])){
						$rowData = $rowData_arr[$rangeToArray_index];
						// Check Header row
						if($row == 1){
							if(!(import_check_column_header('B', 'List Name', $rowData) && 
								import_check_column_header('C', 'Name', $rowData) &&
								import_check_column_header('D', 'Email', $rowData)&&
								import_check_column_header('E', 'Country', $rowData)&&
                                import_check_column_header('AL', '', $rowData)))
                            {
								$is_correct_import_template = false;
								break;
							}
						} 
						else { 
							$col = 1;
							$item = array();
							
                            $listname = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $name = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $email = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
                            $country = !empty($rowData[$col]) ? trim($rowData[$col]) : ''; $col++;
							
							//tach name thanh lastname and fristname
							$a = array();
							$a = (explode(" ",$name));
							$b = count($a);
							$first_name = $a[$b-1];
							
							if($b<=1){
								$last_name = $first_name;
							}
							else{
								$last_name = trim($name, $first_name);
							}

							if(!empty($name) && !empty($email)&& !empty($country)){
								
									$listname_id = $this->model->getlistname($listname);
									
									if(!empty($listname_id)){
										$item['listname_id'] = $listname_id[0]->id;
									}
									else{
										$data['name'] = $listname;
										$data['created'] = date('Y-m-d H:i:s',time());
										$this->db->insert('admin_nqt_mailing_list',$data);
										$item['listname_id'] = $this->db->insert_id();
									}
									
									$country_id = $this->model->getcountry($country);
                                    $item['email'] = $email;
									$item['first_name'] = $first_name;
									$item['last_name'] = $last_name;
									$item['country_id'] = $country_id[0]->id;
									$item['status'] = 1;
									$item['access_token']= generate_random_string(50);
									$checkAccess_token = $this->model->checkAccess_token($item['access_token']); 
									if($checkAccess_token)
									{
										$item['access_token'] = generate_random_string(50);
									}
																	
									$item['created'] = getNow();
									//kiem tra email da tồn tại trong list
									$checkData = $this->model->checkData($email,$update_id = 0, $item['listname_id']);
									if($checkData == false)
									{
										$this->model->insert('admin_nqt_mailing_list_member',$item);
									}
							}
							else {
								$count_empty_username++;
								if($count_empty_username > 5)
									break; 
							}
						}
					}
				}
				if($is_correct_import_template){
				$result = 'ok';
				} else {
				$result = 'wrong_template';
				}
			}
			echo $result;
		}
	}
}