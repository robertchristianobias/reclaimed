<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovaeditor.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/editor/scripts/innovamanager.js'?>"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places"></script>

<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">
<?php
	if(isset($this->lang->languages)){
		$all_lang = $this->lang->languages;
	}else{
		$all_lang = array(
			'' => ''
		);
	}
?>
<script type="text/javascript">
$(document).ready( function(){
	<?php foreach($all_lang as $key=>$val){ ?>
	$("#title<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp").slugIt({
		events: 'keyup blur',
		output: '#slug<?php echo ($key!='') ?  '_'.$key :  '' ?>Admincp',
		map: {'!':'-'},
		space: '-'
	});
	//FOR WYSIWYG content
	$('#content<?php echo ($key!='') ?  '_'.$key : '' ?>Admincp').liveEdit({
		height: 350,
		css: ['<?=PATH_URL?>assets/editor/bootstrap/css/bootstrap.min.css', '<?=PATH_URL?>assets/editor/bootstrap/bootstrap_extend.css'] /* Apply bootstrap css into the editing area */,
		fileBrowser: '<?=PATH_URL?>assets/editor/assetmanager/asset.php',
		returnKeyMode: 3,
		groups: [
				["group1", "", ["Bold", "Italic", "Underline", "ForeColor"]],
				["group2", "", ["Bullets", "Numbering", "Indent", "Outdent"]],
				["group3", "", ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyFull"]],
				["group4", "", ["Paragraph", "FontSize", "FontDialog", "TextDialog"]],
				["group5", "", ["LinkDialog", "ImageDialog", "TableDialog"]],
				["group6", "", ["Undo", "Redo", "FullScreen", "SourceDialog"]]
				] /* Toolbar configuration */
	});
	$('#content<?php echo ($key!='') ? '_'.$key : '' ?>Admincp').data('liveEdit').startedit();
	//End FOR WYSIWYG content
	<?php } ?>
});

function save(){
	console.log('save');
	var options = {
		beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse  // post-submit callback 
    };
	$('#frmManagement').ajaxSubmit(options);
}

function showRequest(formData, jqForm, options) {
	var form = jqForm[0];
	<?php foreach($all_lang as $key=>$val){ ?>
	if (
		form.emailAdmincp.value == ''
	) {
		$('#txt_error').html('Please enter information.');
		show_perm_denied();
		return false;
	}
	<?php } ?>
//
}

function showResponse(responseText, statusText, xhr, $form) {
	responseText = responseText.split(".");
	token_value  = responseText[1];
	$('#csrf_token').val(token_value);
	if(responseText[0]=='success'){
		<?php if($id==0){ ?>
		location.href=root+module+"/#/save";
		<?php }else{ ?>
		if($('.form-upload').val() != ''){
			$.get('<?=PATH_URL_ADMIN.$module.'/ajaxGetImageUpdate/'.$id?>',function(data){
				var res = data.split("src=");
				$('.fileinput-filename').html('');
				$('.fileinput').removeClass('fileinput-exists');
				$('.fileinput').addClass('fileinput-new');
			});
		}
		show_perm_success();
		<?php } ?>
	}
	
	if(responseText[0]=='error-image'){
		$('#txt_error').html('Only upload image.');
		show_perm_denied();
		return false;
	}
	
	if(responseText[0]=='error-duplicate-email'){
		$('#txt_error').html('Duplicate email in current list.');
		show_perm_denied();
		return false;
	}

	if(responseText[0]=='permission-denied'){
		$('#txt_error').html('Permission denied.');
		show_perm_denied();
		return false;
	}
}
</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title"><?=$this->session->userdata('Name_Module')?></h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="fa fa-home"></i><a href="<?=PATH_URL_ADMIN?>">Home</a><i class="fa fa-angle-right"></i></li>
		<li><a href="<?=PATH_URL_ADMIN.$module?>"><?=$this->session->userdata('Name_Module')?></a><i class="fa fa-angle-right"></i></li>
		<li><?php echo ($this->uri->segment(4)=='') ?  'Add new' :  'Edit' ?></li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-cascade">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Form Information
				</div>
			</div>
			
			<div class="portlet-body form">
				<div class="form-body notification" style="display:none">
					<div class="alert alert-success" style="display:none">
						<strong>Success!</strong> The page has been saved.
					</div>
					
					<div class="alert alert-danger" style="display:none">
						<strong>Error!</strong> <span id="txt_error"></span>
					</div>
				</div>
				
				<!-- BEGIN FORM-->
				<form id="frmManagement" action="<?=PATH_URL_ADMIN.$module.'/save/'?>" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
					<input type="hidden" value="<?=$this->security->get_csrf_hash()?>" id="csrf_token" name="csrf_token" />
					<input type="hidden" value="<?=$id?>" name="hiddenIdAdmincp" />
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-9">
								<div class="checkbox-list">
									<label class="checkbox-inline">
										<div class="checkbox"><span><input <?=( ! empty($result->status) && ($result->status == 1) ) ? 'checked' : 'checked'?> type="checkbox" name="statusAdmincp"></span></div>
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Mailing list <span class="required" aria-required="true">*</span></label>
							<div class="col-md-9">
								<select onChange="getPerm(this.value)" class="form-control" name="listname_idAdmincp" id="groupAdmincp">
									<?php
									if(!empty($mailing_list)){
										foreach($mailing_list as $list){
									?>
									<option 
										<?php echo ( isset($result->listname_id) && ($result->listname_id == $list->id) ) ? 'selected' : ''; ?> 
										value="<?=$list->id?>">
											<?=$list->name?>
									</option>
									<?php 
										} 
									} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Email <span class="required" aria-required="true">*</span>
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="emailAdmincp" 
									id="emailAdmincp" 
									value="<?php echo(isset($result->email) ? $result->email : '') ?>"
									/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								First Name 
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="first_nameAdmincp" 
									id="first_nameAdmincp" 
									value="<?php echo(isset($result->first_name) ? $result->first_name : '') ?>"
									/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Last Name 
							</label>
							<div class="col-md-9">
								<input data-required="1" type="text" class="form-control" 
									name="last_nameAdmincp" 
									id="last_nameAdmincp" 
									value="<?php echo(isset($result->last_name) ? $result->last_name : '') ?>"
									/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Country</label>
							<div class="col-md-9">
								<select name="country" id="country" class="form-control">
									<option value="0">--Choose--</option>
									<?php 
										if($list_country){
											foreach ($list_country as $country) {?>
												<option <?php if(isset($result->country_id)){ print $result->country_id == $country->id ? 'selected' : ''; } ?> value="<?=$country->id?>"><?=$country->name.' (+'.$country->phonecode.')'?></option>
									<?php	}	
										}
									?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button onclick="save()" type="button" class="btn green"><i class="fa fa-pencil"></i> Save</button>
								<a href="<?=PATH_URL_ADMIN.$module.'/#/back'?>"><button type="button" class="btn default">Cancel</button></a>
							</div>
						</div>
					</div>

				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->