<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class GA_analytics extends MX_Controller {
    private $analytics;
    private $keyFileLocation;
    private $viewId;
    private $tableId;
    public function __construct() {
        parent::__construct();
        $this->load->library('GoogleAPI');
        $this->load->model('GoogleAnalytics_model', 'ga_model');
        $this->keyFileLocation = BASEFOLDER . '/include_files/pixartthemes_ga_cert.json';
        $this->viewId = '132579345';
        $this->tableId = 'ga:132579345';
        // include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
  //       include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
    }

    public function index() {
        $data = array();
        // $rs = $this->service();
        // echo json_encode($rs);
        // exit;
        $metrics = $this->ga_model->getListMetric();
        $dimensions = $this->ga_model->getListDimension();
        $data ['metrics'] = $metrics;
        $data ['dimensions'] = $dimensions;
        $this->load->view('FRONTEND/chart_view', $data);
        // $this->getTotalActiveUsers();
        // $this->test_benmark(89000);
        // exit;
        // $this->embed();
        // $this->service();
    }

    public function test_date() {
        //sunday: l; May: F; 03: d; 2017: Y; 03: H; 00: 00;
        $output = '';
        $data = array();
        $data ['ga:week'] = '21';
        $data ['ga:year'] = '2017';
        $data ['ga:month'] = '05';
        $data ['ga:yearMonth'] = '201704';
        $data ['ga:date'] = '20170521';
        $data ['ga:hour'] = '17';
        $type = 'week';
        $startDate = '2017-04-19';
        $endDate = '2017-05-22';
        $start = DateTime::createFromFormat('Y-m-d', $startDate);
        $end = DateTime::createFromFormat('Y-m-d', $endDate);
        $isFirst = $isLast = FALSE;
        $outputStart = $outputEnd = '';
        $isFirst = TRUE;
        switch (strtolower($type)) {
            case 'hourly':
                // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:date');
                // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:hour');
                $str = $data ['ga:date'] . $data ['ga:hour'];
                $str_format = 'YmdH';
                $date = DateTime::createFromFormat($str_format, $str);
                $output =  $date->format('l, F d, Y H:00');
                // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:dateHour');
                break;
            case 'day':
                // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:date');
                $str = $data ['ga:date'];
                $str_format = 'Ymd';
                $date = DateTime::createFromFormat($str_format, $str);
                $output =  $date->format('l, F d, Y');
                break;
            case 'week':
                break;
            case 'month':
                $str = $data ['ga:yearMonth'] . '01';
                $str_format = 'Ymd';
                $date = DateTime::createFromFormat($str_format, $str);
                if( $isFirst) {
                    $outputStart = $start->format('l, F d, Y');
                } else {
                    $outputStart = $date->format('l, F d, Y');
                }
                // $outputStart = ($isFirst && $start < $date) ? $date->format('l, F d, Y') : $start->format('l, F d, Y');
                $outputEnd = ($isLast) ? $end->format('l, F d, Y') : $date->format('l, F t, Y');
                $output =  $outputStart . ' - ' . $outputEnd;
                break;
        }
        echo $output; exit;
        $str = '2017052203';
        $str_format = 'Ymdh';
        $date = DateTime::createFromFormat($str_format, $str);
        echo $date->format('Y-m-d H:00');
        // // $date = date_create_from_format($str_format, $str);
        // print_r($date);

        echo ' LAST SUNDAY ', date('Y-m-d', strtotime("last sunday", strtotime("2017W10")));
        echo ' NEXT SATURDAY ', date('Y-m-d', strtotime("next saturday", strtotime("2017W10")));
    }

    public function get_active_users() {
        $result = array();
        $activeUsers = $this->getTotalActiveUsers();
        $result ['status'] = 1;
        $result ['data'] = array('ga_active_users' => $activeUsers);
        echo json_encode($result);
        exit;
    }

    public function retrieve_data() {
        $result = $data = array();
        $startDate = $this->input->post('start_date');
        $endDate = $this->input->post('end_date');
        $viewType = $this->input->post('view_type'); //DROP
        $metrics = $this->input->post('metrics');
        $dimension = $this->input->post('dimension');

        // $startDate = '2017-04-19';
        // $endDate = '2017-05-22';            
        // $dimension = 'week';
        // $metrics = ['sessions', 'pageviews'];

        // pr($this->input->post());
        //validate start date, end date
        if ($startDate > $endDate) {
            $result ['status'] = 0;
            $result ['message'] = "Invalid Date Range: end-date {$endDate} precedes start-date {$startDate}";
            $result ['data'] = $data;
            echo json_encode($result);
            exit;
        }

        //validate Metrics
        $metricsValidation = $this->ga_model->validateMetricNames($metrics);
        if($metricsValidation === NULL) {
            $result ['status'] = 0;
            $result ['message'] = "Invalid Metric field: Empty Metric field ";
            $result ['data'] = $data;
            echo json_encode($result);
            exit;
        }
        if($metricsValidation === FALSE) {
            $result ['status'] = 0;
            $result ['message'] = "Invalid Metric field: Metric name not exists";
            $result ['data'] = $data;
            echo json_encode($result);
            exit;
        }

        //validate Dimesion
        if(count($dimension) > 1) {
            $result ['status'] = 0;
            $result ['message'] = "Invalid Dimesion field: Not support multi dimensions";
            $result ['data'] = $data;
            echo json_encode($result);
            exit;
        }
        $dimensionValidation = $this->ga_model->validateDimensionNames($dimension);
        if($dimensionValidation === FALSE) {
            $result ['status'] = 0;
            $result ['message'] = "Invalid Dimesion field: Dimension name not exists";
            $result ['data'] = $data;
            echo json_encode($result);
            exit;
        }

        //Construct analytics service
        $analytics = $this->ga_model->initlializeAnalyticsReporting($this->keyFileLocation);

        //Make date Range Obj
        $dateRange = $this->ga_model->makeDateRange($startDate, $endDate);
        
        //make Chart request
        $chartMetrics = $this->ga_model->setMetricsByListName($metrics);
        $chartDimensions = $this->ga_model->setDimensionsByViewType($dimension);
        $chartRequest = $this->ga_model->makeAnalyticsReportingReportRequest($this->viewId, $dateRange, $chartMetrics, $chartDimensions);
        $chartResponse = $this->ga_model->getReportRequest($analytics, $chartRequest);
        $customChartReport = $this->ga_model->getCustomReportResult($chartResponse, $dimension, $startDate, $endDate);
        
        //make Statistical request
        $statisticalMetricsListName = array('sessions', 'users', 'pageviews', 'pageviewsPerSession', 'avgSessionDuration', 'bounceRate', 'percentNewSessions');
        $statisticalMetrics    = $this->ga_model->setMetricsByListName($statisticalMetricsListName);
        $statisticalDimensions = array();
        $statisticalRequest    = $this->ga_model->makeAnalyticsReportingReportRequest($this->viewId, $dateRange, $statisticalMetrics, $statisticalDimensions);
        $statisticalResponse = $this->ga_model->getReportRequest($analytics, $statisticalRequest);

        $customStatisticalReport = $this->ga_model->getCustomReportResult($statisticalResponse);
        
        $data = $this->generateDataResult($customChartReport, $customStatisticalReport, $dimension);
        
        $result ['status'] = 1;
        $result ['message'] = 'success';
        $result ['data'] = $data;
        echo json_encode($result);
        exit;
    }

    public function generateDataResult($customChartReport, $customStatisticalReport, $dimension) {
        $data = $dimensions_data = $metrics_data = array();
        $dimensions_data ['title'] = $this->ga_model->getNameOfDimensionByCode($dimension);
        $dimensions_data ['data'] = !empty($customChartReport[0]['dimensions'][$dimension]) ? $customChartReport[0]['dimensions'][$dimension] : array();
        $metrics = !empty($customChartReport[0]['metrics']) ? $customChartReport[0]['metrics'] : array();
        foreach ($metrics as $key => $metric) {
            $temp ['title'] = $this->ga_model->getNameOfMetricByCode($key);
            $temp ['data'] = $metric;
            $metrics_data[] = $temp;
        }

        $data ['chart']['dimensions'] = $dimensions_data;
        $data ['chart']['metrics'] = $metrics_data;
        $data ['statistical'] = isset($customStatisticalReport[0]['data'][0]) ? $customStatisticalReport[0]['data'][0] : array();

        return $data;
    }

    public function embed() {
        $this->load->view('FRONTEND/ga', NULL);
    }

    public function getTotalActiveUsers() {
        $activeUsers = FALSE;

        $analyticsRealtime = $this->ga_model->initlializeServiceAnalytics($this->keyFileLocation);
        $result = $analyticsRealtime->data_realtime->get($this->tableId, 'rt:activeUsers');
        $totals = $result->getTotalsForAllResults();
        if (isset($totals['rt:activeUsers'])) {
            $activeUsers = intval($totals['rt:activeUsers']);
        }
        return $activeUsers;
        echo 'active users: ' . $activeUsers;
        // exit;
    }

    public function service() {
        $analytics = $this->ga_model->initlializeAnalyticsReporting($this->keyFileLocation);

        $response = $this->get_ga_report($analytics);

        $rs = $this->ga_model->getCustomReportResult($response);
        // $rs = $this->printResults($response);
        // pr($response);
        $chart_rs_data = $rs[0];
        $statistical_rs_data = $rs[1];
        $dates = $users = $sessions = $pageviews = array('title' => '', 'data' => '');
        $dates ['title'] = 'Date';
        $dates ['data'] = json_encode($chart_rs_data['dimensions']['custom_date_0']);;
        
        $users ['title'] = 'User';
        $users ['data'] = json_encode($chart_rs_data['metrics']['users']);

        $sessions ['title'] = 'Session';
        $sessions ['data'] = json_encode($chart_rs_data['metrics']['sessions']);
        
        $pageviews ['title'] = 'Pageview';
        $pageviews ['data'] = json_encode($chart_rs_data['metrics']['pageviews']);
        // $dates = json_encode($chart_rs_data['dimensions']['custom_date_0']);
        // $users = json_encode($chart_rs_data['metrics']['users']);
        // $sessions = json_encode($chart_rs_data['metrics']['sessions']);
        // $pageviews = json_encode($chart_rs_data['metrics']['pageviews']);
        $data ['dates'] = $dates;
        $data ['users'] = $users;
        $data ['sessions'] = $sessions;
        $data ['pageviews'] = $pageviews;
        $data ['statistical'] = $statistical_rs_data['data'][0];
        return $data;
        // $this->load->view('FRONTEND/chart_view', $data);
    }

    public function chart_view() {
        $data = array();
        $this->load->view('FRONTEND/chart_view', $data);
    }



    public function getMetricsChartData() {

    }

    public function getMetricsStatisticalData() {
        /*ga:sessions, ga:users, ga:pageviews, ga:pageviewsPerSession, ga:avgSessionDuration, ga:bounceRate, ga:percentNewSessions,*/
        $sessions            = $this->ga_model->makeAnalyticsReportingMetric('ga:sessions', 'sessions');
        $users               = $this->ga_model->makeAnalyticsReportingMetric('ga:users', 'users');
        $pageviews           = $this->ga_model->makeAnalyticsReportingMetric('ga:pageviews', 'pageviews');
        $pageviewsPerSession = $this->ga_model->makeAnalyticsReportingMetric('ga:pageviewsPerSession', 'pageviews_per_session');
        $avgSessionDuration  = $this->ga_model->makeAnalyticsReportingMetric('ga:avgSessionDuration', 'avg_session_duration');
        $bounceRate          = $this->ga_model->makeAnalyticsReportingMetric('ga:bounceRate', 'bounce_rate');
        $percentNewSessions  = $this->ga_model->makeAnalyticsReportingMetric('ga:percentNewSessions', 'percent_new_sessions');

        $date = $this->ga_model->makeAnalyticsReportingDimension('ga:date');

        $result = array($sessions, $users, $pageviews, $pageviewsPerSession, $avgSessionDuration, $bounceRate, $percentNewSessions);
        return $result;
    }

    private function initlialize_analytics_realtime() {
        $KEY_FILE_LOCATION = BASEFOLDER . '/include_files/pixartthemes_ga_cert.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Realtime Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics', 'https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_Analytics($client);

        return $analytics;
    }



    /**
    * Initializes an Analytics Reporting API V4 service object.
    *
    * @return An authorized Analytics Reporting API V4 service object.
    */
    private function initlialize_analytics() {
        $KEY_FILE_LOCATION = BASEFOLDER . '/include_files/pixartthemes_ga_cert.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_AnalyticsReporting($client);

        return $analytics;
    }

    private function makeDateRange($startDate, $endDate) {
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDate);
        $dateRange->setEndDate($endDate);
        return $dateRange;
    }

    private function makeAnalyticsReportingMetric($expression, $alias) {
        $metric = new Google_Service_AnalyticsReporting_Metric();
        $metric->setExpression($expression);
        $metric->setAlias($alias);
        return $metric;
        // $result = ;
    }

    private function makeAnalyticsReportingDimension($name) {
        $dimension = new Google_Service_AnalyticsReporting_Dimension();
        $dimension->setName($name);
        return $dimension;
    }

    private function makeAnalyticsReportingReportRequest($viewId, $dateRange, $metrics, $dimensions) {
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($viewId);
        $request->setDateRanges($dateRange);
        $request->setMetrics($metrics);
        $request->setDimensions($dimensions);
        return $request;
    }

    private function getReportRequest($analytics, $requests) {
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests($requests);
        return $analytics->reports->batchGet($body);
    }

    function get_ga_report($analytics) {
        // Replace with your view ID, for example XXXX.
        $VIEW_ID = "132579345";

        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        // $dateRange->setStartDate("7daysAgo");
        // $dateRange->setEndDate("today");
        $dateRange->setStartDate("2017-04-01");
        $dateRange->setEndDate("2017-04-30");

        // Create the Metrics object.
        $users = new Google_Service_AnalyticsReporting_Metric();
        $users->setExpression("ga:users");
        $users->setAlias("users");

        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");
        $sessions->setAlias("sessions");

        $pageviews = new Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");
        /*ga:pageviews, */
        //Create the Dimensions object.
        $browser = new Google_Service_AnalyticsReporting_Dimension();
        $browser->setName("ga:browser");
        $date = new Google_Service_AnalyticsReporting_Dimension();
        $date->setName("ga:date");
        $hour = new Google_Service_AnalyticsReporting_Dimension();
        $hour->setName("ga:hour");
        $dateHour = new Google_Service_AnalyticsReporting_Dimension();
        $dateHour->setName('ga:dateHour');

        // Create the ReportRequest object.
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($users, $sessions, $pageviews));
        // $request->setDimensions(array($dateHour));//, $hour
        $request->setIncludeEmptyRows(TRUE); //IMPORTANT
        // $request->setDimensions(array($browser, $date));
        $test = $this->getMetricsStatisticalData();
        // pr($test, 1);
        $rq = new Google_Service_AnalyticsReporting_ReportRequest();
        $rq->setViewId($VIEW_ID);
        $rq->setDateRanges($dateRange);
        $rq->setMetrics($test);
        // $rq->setDimensions(array($date));
        // $rq = array();
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request, $rq) );
        return $analytics->reports->batchGet( $body );
    }

    function printResults($reports) {
        $result = $temp_report = array();

      for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
        $header_data = $dimensions_data = $metrics_data = array(); //new
        $report = $reports[ $reportIndex ];
        $header = $report->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows = $report->getData()->getRows();
        // pr($report, 1);
        for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
          $temp_row = array();

          $row = $rows[ $rowIndex ];
          $dimensions = $row->getDimensions();
          $metrics = $row->getMetrics();
          // pr($dimensions);
          // echo 'dimensionHeaders';
          // pr($dimensionHeaders);
          for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
            $temp_row [$dimensionHeaders [$i]] = $dimensions[$i];
            $dimensions_data [$dimensionHeaders [$i]][] = $dimensions [$i];
            // print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
            // echo "<br>";
          }
          // echo "<br>";
          for ($j = 0; $j < count($metrics); $j++) {
            $values = $metrics[$j]->getValues();
            for ($k = 0; $k < count($values); $k++) {
              $entry = $metricHeaders[$k];
              $temp_row [$entry->getName()] = $values[$k];
              $metrics_data [$entry->getName()][] = $values[$k];
              // pr($entry, 1);
              // print($entry->getName() . ": " . $values[$k] . "\n");
            }
          }
          $temp_report[] = $temp_row;
          unset($temp_row);
          // echo "<br>";
        } //end row in report
        // $result['header'] = $header_data;
        $result['data'] = $temp_report;
        $result['dimensions'] = $dimensions_data;
        $result['metrics'] = $metrics_data;
        unset($temp_report, $dimensions_data, $metrics_data);

      }//end reports
      return $result;
      // pr($result, 1);
    }
    // function printResults($reports) {
    //   for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
    //     $report = $reports[ $reportIndex ];
    //     $header = $report->getColumnHeader();
    //     $dimensionHeaders = $header->getDimensions();
    //     $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
    //     $rows = $report->getData()->getRows();

    //     for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
    //       $row = $rows[ $rowIndex ];
    //       $dimensions = $row->getDimensions();
    //       $metrics = $row->getMetrics();
    //       for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
    //         print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
    //       }

    //       for ($j = 0; $j < count($metrics); $j++) {
    //         $values = $metrics[$j]->getValues();
    //         for ($k = 0; $k < count($values); $k++) {
    //           $entry = $metricHeaders[$k];
    //           print($entry->getName() . ": " . $values[$k] . "\n");
    //         }
    //       }
    //     }
    //   }
    // }




}