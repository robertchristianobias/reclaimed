<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MX_Controller {
    
    private $module = 'payment';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;
    

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('payment_model','model');
        $this->template->set_template('default');

        //$this->lang->load($this->lang->get_name_lang());
        
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        // $this->load->library('session');
        // $this->load->library('facebook');   
    }

    public function index() { 
        $data = array();
        $data ['checkout_url'] = 'https://checkout.wirecard.com/page/init.php';
        $data ['checkout_data'] = $this->make_request();
        $this->load->view('FRONTEND/payment', $data);
        // $_data = array();
        // $_data = $this->make_request();
        // $_url = 'https://checkout.wirecard.com/page/init.php';
        // $_header = array(
        //     'Content-Type:multipart/form-data',
        //     'Location:redirect'
        //     );

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $_header);
        // curl_setopt($ch, CURLOPT_POST, TRUE);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $_data);
        // // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // curl_setopt($ch, CURLOPT_URL, $_url);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); //MAKE REDIRECT REQUEST
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        // $response = curl_exec($ch);
        // // $redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );

        // curl_close($ch);
        // header("Location: $_url");

    }

    public function make_order_detail() {
        $order = array();
        $order ['basketItems'] = 3;
        $order_detail = array();
        foreach($order_detail as $key => $detail) {
            $current_item = $key + 1;
            // $order = '';
        }
        /*
        basketItems - Numeric - Number of items in shopping basket.
        basketItem(n)ArticleNumber - Alphanumeric with special characters. - Unique ID of article n in shopping basket.
        basketItem(n)Description - Alphanumeric with special characters. - Product description of article n in shopping basket.
        basketItem(n)ImageUrl - Alphanumeric with special characters. - URL to an image of each item.
        basketItem(n)Name - Alphanumeric with special characters. - Product name of article n in shopping basket.
        basketItem(n)Quantity - Numeric - Items count of article n in shopping basket.
        basketItem(n)UnitGrossAmount - Amount - Price per unit of article n in shopping basket with taxes.
        basketItem(n)UnitNetAmount - Amount - Price per unit of article n in shopping basket without taxes.
        basketItem(n)UnitTaxAmount - Amount - Tax amount per unit of article n in shopping basket.
        basketItem(n)UnitTaxRate - Percentage of tax, e.g. 20 or 19.324. Up to 3 fractions. - Percentage of tax per unit of article n in shopping basket.
         */
    }

    public function make_request() {
        $params = array();
        $params ['customerId'] = WIRECARD_CUSTOMER_ID;
        $params ['shopId'] = WIRECARD_SHOP_ID;
        $params ['language'] = 'en';
        $params ['paymentType'] = 'SELECT'; //opt
        $params ['amount'] = '9.99';
        $params ['currency'] = 'USD';
        $params ['orderDescription'] = 'Jane Doe (33562), Order: 5343643-034';
        $params ['orderReference'] = '5343643-034'; //opt
        $params ['successUrl'] = base_url(PAYMENT_PREFIX . '/success');
        $params ['cancelUrl'] = base_url(PAYMENT_PREFIX . '/cancel'); //opt
        $params ['failureUrl'] = base_url(PAYMENT_PREFIX . '/failure'); //opt
        $params ['pendingUrl'] = base_url(PAYMENT_PREFIX . '/pending'); //opt
        $params ['confirmUrl'] = base_url(PAYMENT_PREFIX . '/confirm'); //opt
        $params ['serviceUrl'] = base_url(PAYMENT_PREFIX . '/service'); //opt
        // $params ['displayText'] = 'Thank you very much for your order in REDM.'; //opt
        // $params ['orderNumber'] = '39944'; //opt
        $params ['windowName'] = 'TEST REDM'; //opt
        $params ['basketItems'] = 1;
        $params ['basketItem1ArticleNumber'] = 'PRD001';
        $params ['basketItem1Name'] = 'Product 1';
        $params ['basketItem1Quantity'] = 1;
        $params ['basketItem1UnitGrossAmount'] = '9.99';
        $params ['basketItem1UnitNetAmount'] = '9.99';
        $params ['basketItem1UnitTaxAmount'] = '0';
        $params ['basketItem1UnitTaxRate'] = '0';

        $params ['requestFingerprintOrder'] = $this->getRequestFingerprintOrder ($params);
        $params ['requestFingerprint'] = $this->getRequestFingerprint ($params, WIRECARD_SECRET);
        return $params;
    }

    public function print_data() {
        $_data = $this->make_request();
        pr($_data);
        foreach($_data as $key => $value) {
            echo"$key:$value<br>";
        }
    }

    public function print_response() {
        echo 'HEADER:';
        pr($this->input->request_headers());
        echo 'GET:';
        pr($this->input->get());
        echo 'POST:';
        pr($this->input->post());
        // exit;
    }

    private function getRequestFingerprintOrder($params) {
        $result = "";
        foreach ($params as $key => $value) {
            $result .= "$key,";
        }
        $result .= "requestFingerprintOrder,secret";
        return $result;
    }

    public function getRequestFingerprint($params, $secret) {
        $result = "";
        foreach ($params as $key => $value) {
            $result .= "$value";
        }
        $result .= "$secret";
        return hash_hmac("sha512", $result, $secret);
    }

    public function payment_success() {
        echo 'payment payment_success';
        $this->print_response();
    }

    public function payment_failure() {
        echo 'payment payment_failure';
        $this->print_response();
    }

    public function payment_cancel() {
        echo 'payment payment_cancel';
        $this->print_response();
    }

    public function payment_pending() {
        echo 'payment payment_pending';
        $this->print_response();
    }

    public function payment_confirm() {
        echo 'payment payment_confirm';
        $this->print_response();
    }

    public function get_ga() {
        // Include the google api php libraries
        include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";

        
    }
}