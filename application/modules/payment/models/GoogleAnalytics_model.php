<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class GoogleAnalytics_model extends MY_Model {

	private $module = 'payment';
    private $analytics;
    public function __construct() {
        parent::__construct();
        $this->load->library('GoogleAPI');
    }

    public function getListDimension() {
        $result = array(
                'hourly' => [ 'code' => 'hourly', 
                                'name' => 'Hourly', 
                                'default' => 0 ],
                'day' => [ 'code' => 'day', 
                                'name' => 'Day', 
                                'default' => 1 ],
                'week' => [ 'code' => 'week', 
                                'name' => 'Week', 
                                'default' => 0 ],
                'month' => [ 'code' => 'month', 
                                'name' => 'Month', 
                                'default' => 0 ],
                'browser' => [ 'code' => 'browser', 
                                'name' => 'Browser', 
                                'default' => 0 ],
                'os' => [ 'code' => 'os', 
                                'name' => 'Operating system', 
                                'default' => 0 ],
                'country' => [ 'code' => 'country', 
                                'name' => 'Country', 
                                'default' => 0 ],
                'city' => [ 'code' => 'city', 
                                'name' => 'City', 
                                'default' => 0 ],
            );
        return $result;
    }

    public function getListMetric() {
        $result = array(
                'sessions' => [ 'code' => 'sessions', 
                                    'name' => 'Sessions', 
                                    'default' => 1 ],
                'users' => [ 'code' => 'users', 
                                    'name' => 'Users', 
                                    'default' => 0 ],
                'pageviews' => [ 'code' => 'pageviews', 
                                    'name' => 'Pageviews', 
                                    'default' => 0 ],
                'newUsers' => [ 'code' => 'newUsers', 
                                    'name' => 'New users', 
                                    'default' => 0 ],
                'pageviewsPerSession' => [ 'code' => 'pageviewsPerSession', 
                                    'name' => 'Pageviews per session', 
                                    'default' => 0 ],
                'avgSessionDuration' => [ 'code' => 'avgSessionDuration', 
                                    'name' => 'Avg. session duration', 
                                    'default' => 0 ],
                'bounceRate' => [ 'code' => 'bounceRate', 
                                    'name' => 'Bounce rate', 
                                    'default' => 0 ],
                'percentNewSessions' => [ 'code' => 'percentNewSessions', 
                                    'name' => 'Percent new sessions', 
                                    'default' => 0 ],
                'percentNewSessions' => [ 'code' => 'percentNewSessions', 
                                    'name' => 'Percent new sessions', 
                                    'default' => 0 ],
            );
        return $result;
    }

    public function getNameOfDimensionByCode($code) { 
        $listDimension = $this->getListDimension();
        return ( ! empty($listDimension[$code]['name'])) ? $listDimension[$code]['name'] : '';
    }

    public function getNameOfMetricByCode($code) {
        $listMetric = $this->getListMetric();
        return ( ! empty($listMetric[$code]['name'])) ? $listMetric[$code]['name'] : '';
    }

    public function validateMetricNames($listName) {
        if (empty($listName)) {
            return NULL;
        }
        $listName = is_array($listName) ? $listName : array($listName);
        $listValid = $this->getListMetric();
        foreach ($listName as $key => $name) {
            if( ! array_key_exists($name, $listValid)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public function validateDimensionNames($listName) {
        if (empty($listName)) {
            return NULL;
        }
        $listName = is_array($listName) ? $listName : array($listName);
        $listValid = $this->getListDimension();
        foreach ($listName as $key => $name) {
            if( ! array_key_exists($name, $listValid)) {
                return FALSE;
            }
        }
        return TRUE;
    }
    
    public function initlializeServiceAnalytics($keyFileLocation = FALSE) {
        // $keyFileLocation = BASEFOLDER . '/include_files/pixartthemes_ga_cert.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("REDM Analytics Realtime");
        $client->setAuthConfig($keyFileLocation);
        $client->setScopes(['https://www.googleapis.com/auth/analytics', 'https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_Analytics($client);

        return $analytics;
    }

    /**
    * Initializes an Analytics Reporting API V4 service object.
    *
    * @return An authorized Analytics Reporting API V4 service object.
    */
    public function initlializeAnalyticsReporting($keyFileLocation = FALSE) {
        // $keyFileLocation = BASEFOLDER . '/include_files/pixartthemes_ga_cert.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("REDM Analytics Reporting");
        $client->setAuthConfig($keyFileLocation);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new Google_Service_AnalyticsReporting($client);

        return $analytics;
    }

    public function makeDateRange($startDate, $endDate) {
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDate);
        $dateRange->setEndDate($endDate);
        return $dateRange;
    }

    public function makeAnalyticsReportingMetric($expression, $alias) {
        $metric = new Google_Service_AnalyticsReporting_Metric();
        $metric->setExpression($expression);
        $metric->setAlias($alias);
        return $metric;
    }

    public function makeAnalyticsReportingDimension($name) {
        $dimension = new Google_Service_AnalyticsReporting_Dimension();
        $dimension->setName($name);
        return $dimension;
    }

    public function setDimensionsByViewType($viewTypes) {
        $dimensions = array();
        $types = is_array($viewTypes) ? $viewTypes : array($viewTypes);
        foreach ($types as $key => $type) {
            switch (strtolower($type)) {
                case 'hourly':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:date');
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:hour');
                    // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:dateHour');
                    break;
                case 'day':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:date');
                    break;
                case 'week':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:week');
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:year');
                    // $dimensions [] = $this->makeAnalyticsReportingDimension('ga:yearWeek');
                    break;
                case 'month':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:month');
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:yearMonth');
                    break;
                case 'browser':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:browser');
                    break;
                case 'os':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:operatingSystem');
                    break;
                case 'city':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:city');
                    break;
                case 'country':
                    $dimensions [] = $this->makeAnalyticsReportingDimension('ga:country');
                    break;
                default:
                    # code...
                    break;
            }
        }
        return $dimensions;
    }

    public function setMetricsByListName($metricNames) {
        $result = array();
        $metrics = is_array($metricNames) ? $metricNames : array($metricNames);
        foreach ($metrics as $key => $metric) {
            switch ($metric) {
                case 'sessions':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'users':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'pageviews':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'pageviewsPerSession':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'avgSessionDuration':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'bounceRate':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'percentNewSessions':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'newUsers':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                case 'percentNewSessions':
                    $result [] = $this->makeAnalyticsReportingMetric('ga:'.$metric, $metric);
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        return $result;
    }

    public function makeAnalyticsReportingReportRequest($viewId, $dateRange, $metrics, $dimensions = array()) {
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($viewId);
        $request->setDateRanges($dateRange);
        $request->setMetrics($metrics);
        if(! empty($dimensions)) {
            $request->setDimensions($dimensions);
        }
        $request->setIncludeEmptyRows(TRUE); //IMPORTANT
        return $request;
    }

    public function getReportRequest($analytics, $requests) {
        $requests = is_array($requests) ? $requests : array($requests);
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests($requests);
        return $analytics->reports->batchGet($body);
    }

    public function getCustomReportResult($reports, $viewTypes = '', $startDate = '', $endDate = '') {
        $result = array();

        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $header_data = $temp_report = $temp_data = $dimensions_data = $metrics_data = array();
            
            $report                = $reports[$reportIndex];
            $header                = $report->getColumnHeader();
            $dimensionHeaders      = $header->getDimensions();
            $metricHeaders         = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows                  = $report->getData()->getRows();
            $totalRows             = count($rows);
            $totalDimensionHeaders = count($dimensionHeaders);

            for ($rowIndex = 0; $rowIndex < $totalRows; $rowIndex++) {
                $temp_row        = array();
                $row             = $rows[$rowIndex];
                $dimensions      = $row->getDimensions();
                $metrics         = $row->getMetrics();
                $totalDimensions = count($dimensions);
                $totalMetrics    = count($metrics);

                for ($i = 0; $i < $totalDimensionHeaders && $i < $totalDimensions; $i++) {
                    $temp_row [$dimensionHeaders [$i]] = $dimensions [$i];
                    $dimensions_data [$dimensionHeaders [$i]][] = $dimensions [$i];
                }
                
                for ($j = 0; $j < $totalMetrics; $j++) {
                    $values = $metrics[$j]->getValues();
                    $totalValues = count($values);
                    for ($k = 0; $k < $totalValues; $k++) {
                        $entry = $metricHeaders [$k];
                        $temp_row [$entry->getName()]       = $values [$k];
                        $metrics_data [$entry->getName()][] = $values [$k];
                    }
                }

                $temp_data[] = $temp_row;
                unset($temp_row);
            } //end row in report

            //Do custom dimension data in here
            $custom_dimension_data = $this->makeCustomDimesionByTypeView($temp_data, $viewTypes, $startDate, $endDate);
            $dimensions_data = array_merge($dimensions_data, $custom_dimension_data);
            $temp_report ['data']       = $temp_data;
            $temp_report ['dimensions'] = $dimensions_data;
            $temp_report ['metrics']    = $metrics_data;

            $result[] = $temp_report;
            unset($temp_report, $temp_data, $dimensions_data, $metrics_data);
        } //end reports
        // pr($result, 1);
        return $result;
    }

    public function makeCustomDimesionByTypeView($preData, $viewTypes = '', $startDate = '', $endDate = '') {
        if(empty($preData) || empty($viewTypes)) {
            return array();
        }
        $result = array();
        $types = is_array($viewTypes) ? $viewTypes : array($viewTypes);
        $start = DateTime::createFromFormat('Y-m-d', $startDate);
        $end = DateTime::createFromFormat('Y-m-d', $endDate);
        $isFirst = $isLast = FALSE;
        $numOfData = count($preData);
        foreach ($preData as $k => $data) {
            $output = $outputStart = $outputEnd = '';
            $isFirst = ($k === 0) ? TRUE : FALSE;
            $isLast  = ($k == $numOfData - 1) ? TRUE : FALSE;
            foreach ($types as $key => $type) {
                switch (strtolower($type)) {
                    case 'hourly':
                        $str = $data ['ga:date'] . $data ['ga:hour'];
                        $str_format = 'YmdH';
                        $date = DateTime::createFromFormat($str_format, $str);
                        $output =  $date->format('l, F d, Y H:00');
                        break;
                    case 'day':
                        $str = $data ['ga:date'];
                        $str_format = 'Ymd';
                        $date = DateTime::createFromFormat($str_format, $str);
                        $output =  $date->format('l, F d, Y');
                        break;
                    case 'week':
                        $str = strtotime($data ['ga:year'] .'W'. $data ['ga:week']);
                        $strtotime = strtotime($str);
                        if( $isFirst) {
                            $outputStart = $start->format('F d, Y');
                        } else {
                            $outputStart = date('F d, Y', strtotime("last sunday", $str));
                        }
                        if( $isLast) {
                            $outputEnd = $end->format('F d, Y');
                        } else {
                            $outputEnd = date('F d, Y', strtotime("next saturday", $str));
                        }
                        $output =  $outputStart . ' - ' . $outputEnd;
                        break;
                    case 'month':
                        $str = $data ['ga:yearMonth'] . '01';
                        $str_format = 'Ymd';
                        $date = DateTime::createFromFormat($str_format, $str);
                        if( $isFirst) {
                            $outputStart = $start->format('F d, Y');
                        } else {
                            $outputStart = $date->format('F d, Y');
                        }
                        if( $isLast) {
                            $outputEnd = $end->format('F d, Y');
                        } else {
                            $outputEnd = $date->format('F t, Y');
                        }
                        $output =  $outputStart . ' - ' . $outputEnd;
                        break;
                }
                $result [$type][] = $output;
                // unset($output);
            }
        }
        return $result;
    }
}