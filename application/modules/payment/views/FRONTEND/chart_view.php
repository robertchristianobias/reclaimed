<!--  -->

<script src="<?=PATH_URL.'assets/js/'?>jquery-1.11.3.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?=PATH_URL.'assets/css/admin/'?>datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?=PATH_URL.'assets/css/admin/'?>bootstrap-timepicker.min.css"/>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {                              
        $('#time_from, #time_to').timepicker(); 
    });
</script>
<!--  -->
<!-- <script src="<?=get_resource_url('assets/chart/chart.js')?>"></script>	
<script src="<?=get_resource_url('assets/chart/moment.js')?>"></script>	 -->
<script src="<?=get_resource_url('assets/chart/Chart.bundle.min.js')?>"></script>	
<!-- <script src="<?=get_resource_url('assets/chart/utils.js')?>"></script>	 -->

<!-- chart_view -->
<!-- <span>select date, choose type, select metrics</span> -->
	<div>From date: 
        <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
            <input value="<?php echo(isset($result->featured_from)) ? date("Y-m-d",strtotime($result->featured_from)) : '' ?>" type="date" name="featured_from_date" id="date_from" class="form-control"/>
        </div>
    </div> <!-- lastweek -->
	<div>To date: 
        <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
            <input value="<?php echo(isset($result->featured_from)) ? date("Y-m-d",strtotime($result->featured_from)) : '' ?>" type="date" name="featured_from_date" id="date_to" class="form-control"/>
        </div>
    </div> <!-- doday -->
	<select name="view_type" id="view_type" >
        <?php foreach ($dimensions as $key => $item) {
            $code = $item ['code'];
            $name = $item ['name'];
            $selected = ($item ['default'] == 1) ? 'selected' : '';
            echo "<option value='$code' $selected>$name</option>";
        } ?>
	</select>
    <br>
    <!-- metrics -->
    <?php foreach ($metrics as $key => $item) { 
            $code = $item ['code'];
            $name = $item ['name'];
            $checked = ($item ['default'] == 1) ? 'checked' : '';
            $id = 'cbx_metric_'.$key;
        ?>
        <input id="<?=$id?>" name="cbx_metrics" type="checkbox" value="<?=$code?>" <?=$checked?>/>
        <label for="<?=$id?>"><?=$name?></label>
    <?php } ?>
    <!-- /metrics -->
    <div>
        Active Users: <span id="ga_active_users"></span>
        <input type="button" 
            name="btn_get_active_users" 
            id="btn_get_active_users" 
            value="Request Active Users"
            data-endpoint="<?=PATH_URL . AJAX_PREFIX . '/get-active-user'?>">
    </div>
        <input type="button" 
            name="btn_retrieve_data" 
            id="btn_retrieve_data" 
            value="Request Retrieve Data"
            data-endpoint="<?=PATH_URL . AJAX_PREFIX . '/retrieve-data'?>"
            data-viewtype="day";
            data-startdate="2017-05-17";
            data-enddate="2017-05-18";
            >
	<div>
        
		<div>Sessions: <span id="ga_sessions"></span></div>
		<div>Users: <span id="ga_users"></span></div>
		<div>Pageviews: <span id="ga_pageviews"></span></div>
		<div>Pages / Session: <span id="ga_pageviews_per_session"></span></div>
		<div>Avg. Session Duration: <span id="ga_avg_session_duration"></span></div>
		<div>Bounce Rate: <span id="ga_bounce_rate"></span></div>
		<div>% New Sessions: <span id="ga_percent_new_sessions"></span></div>
	</div>
    <br>
    <br>
    <button id="randomizeData">Randomize Data</button>
    <button id="addDataset">Add Dataset</button>
    <button id="removeDataset">Remove Dataset</button>
    <button id="addData">Add Data</button>
    <button id="removeData">Remove Data</button>
    <div>
        <canvas id="myChart"></canvas>
    </div>
    <script>
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };
        var colorNames = Object.keys(window.chartColors);
        var datasets = [];
        var labels = [];
        var chart_title = 'ReClaimEDM Google Analytics';

        var config = {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets
                },
                options: {
                    responsive: true,
                    title:{
                        display:true,
                        text: chart_title,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Dimension'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                    }
                }
            };

        window.onload = function() {
            var ctx = document.getElementById("myChart").getContext("2d");
            window.myLine = new Chart(ctx, config);
            retrieveData();
            getActiveUsers();
        };

        document.getElementById('view_type').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.viewtype = this.value.toLowerCase();
        });

        document.getElementById('date_from').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.startdate = this.value.toLowerCase();
        });

        document.getElementById('date_to').addEventListener('change', function(){
            document.getElementById('btn_retrieve_data').dataset.enddate = this.value.toLowerCase();
        });

        document.getElementById('btn_get_active_users').addEventListener('click', function() {
            getActiveUsers();
        });

        document.getElementById('btn_retrieve_data').addEventListener('click', function(){
            retrieveData();
        });

        function makeConfigLineChart(dimension, metrics, chartTitle) {
            var labels_data = eval(dimension.data);
            var labels_title = dimension.title;
            var myDatasets = [];
            var myMetricLabels = [];
            var numMetrics = metrics.length;
            for(var i=0; i < numMetrics; i++) {
                var metric = metrics[i];
                var colorName = colorNames[i % colorNames.length];
                var newColor = window.chartColors[colorName];
                var newDataset = {
                        label: metric.title,
                        backgroundColor: newColor,
                        borderColor: newColor,
                        data: eval(metric.data),
                        fill: false
                    };
                var newMetricLabel = {
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: metric.title
                        }
                    };

                myDatasets.push(newDataset);
                myMetricLabels.push(newMetricLabel);
            }

            var config = {
                type: 'line',
                data: {
                    labels: labels_data,
                    datasets: myDatasets
                }, 
                options: {
                    responsive: true,
                    title:{
                        display:true,
                        text: chartTitle
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: labels_title
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                        // yAxes: myMetricLabels
                    }
                }
            };
            return config;
        }

        function getActiveUsers() {
            var element  = document.getElementById('btn_get_active_users');
            var endpoint = element.dataset.endpoint;
            var response = doRequestXHR('post', endpoint, null);
            document.getElementById('ga_active_users').innerHTML = response.data.ga_active_users;
        }

        function retrieveData() {
            var element = document.getElementById('btn_retrieve_data');
            var endpoint = element.dataset.endpoint;
            var metrics_element = document.getElementsByName('cbx_metrics');
            var metrics_lenghth = metrics_element.length;
            var metrics = [];
            for(var i = 0; i < metrics_lenghth; i++) {
                if (metrics_element[i].checked === true) {
                    metrics.push(metrics_element[i].value);
                }
            }
            var start_date = element.dataset.startdate;
            var end_date = element.dataset.enddate;
            var view_type = element.dataset.viewtype;
            var dimension = element.dataset.viewtype;
            var data = new FormData();
            data.append('start_date', start_date);
            data.append('end_date', end_date);
            data.append('view_type', view_type);
            data.append('dimension', dimension);
            for(var i = 0; i < metrics_lenghth; i++) {
                if (metrics_element[i].checked === true) {
                    data.append('metrics[]', metrics_element[i].value);
                }
            }
            var response = doRequestXHR('post', endpoint, data);
            if(response.status == 0) {
                console.log(response);
                return;
            }
            updateStatisticalData(response.data.statistical);
            var chart_title = 'ReClaimEDM Google Analytics';
            var dimension = response.data.chart.dimensions;
            var metrics = response.data.chart.metrics;
            updateChart(dimension, metrics);
        }

        function updateChart(dimension, metrics) {
            var myDatasets = [];
            var myMetricLabels = [];
            var labelsData = eval(dimension.data);
            var numMetrics = metrics.length;
            for(var i=0; i < numMetrics; i++) {
                var metric = metrics[i];
                var colorName = colorNames[i % colorNames.length];
                var newColor = window.chartColors[colorName];
                var newDataset = {
                        label: metric.title,
                        backgroundColor: newColor,
                        borderColor: newColor,
                        data: eval(metric.data),
                        fill: false
                    };
                var newMetricLabel = {
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: metric.title
                        }
                    };

                myDatasets.push(newDataset);
                myMetricLabels.push(newMetricLabel);
            }

            window.config.data.datasets = myDatasets;
            window.config.data.labels = labelsData;
            window.config.options.scales.xAxes[0].scaleLabel.labelString = dimension.title;
            window.myLine.update();
        }

        function updateActiveUsers(active_users) {
            document.getElementById('ga_active_users').innerHTML = active_users;
        }

        function updateStatisticalData(statistical_data) {
            document.getElementById('ga_sessions').innerHTML = statistical_data.sessions;
            document.getElementById('ga_users').innerHTML = statistical_data.users;
            document.getElementById('ga_pageviews').innerHTML = statistical_data.pageviews;
            document.getElementById('ga_pageviews_per_session').innerHTML = parseFloat(statistical_data.pageviewsPerSession).toFixed(2);
            document.getElementById('ga_avg_session_duration').innerHTML = parseFloat(statistical_data.avgSessionDuration).toFixed(2);
            document.getElementById('ga_bounce_rate').innerHTML = parseFloat(statistical_data.bounceRate).toFixed(2);
            document.getElementById('ga_percent_new_sessions').innerHTML = parseFloat(statistical_data.percentNewSessions).toFixed(2);
        }

        function doRequestXHR(type, endpoint, data) {
            var response = false;
            var request = new XMLHttpRequest();
            request.open(type, endpoint , false);
            request.loadstart = function() {
                console.log('loadstart');
            };
            request.onload = function() {
                console.log('onload');    
                response = JSON.parse(request.responseText);
            };

            request.error = function(error) {
            console.log(eror);
            }
            console.log('before Send');
            request.send(data);
            return response;
        }
    </script>