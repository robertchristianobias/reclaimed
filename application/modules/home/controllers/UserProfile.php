<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserProfile extends MX_Controller {
    
    private $module = 'home';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('home_model','model');
        $this->load->model('UserSession_model','userSession');

         $this->template->set_template('default');
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        $this->load->library('session');
        $this->load->library('facebook');

    }

   
    public function load_list_user_info($id){
        return $this->model->load_list_user_info($id);
    }

    private function _is_login(){
        if ($this->session->userdata('token')) {
            redirect(base_url());
        }
        elseif ( ! $this->input->is_ajax_request()) {
            redirect(base_url('/'));
        }
    }

    public function subscription_setting(){
        $userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
        $checkUserLocation = $this->model->checkUserLocation($user_id);
        $item = array();
        
        $local1['user_id'] = $user_id;
        $local2['user_id'] = $user_id;
        $local3['user_id'] = $user_id;
        $local4['user_id'] = $user_id;
        if(!empty($_POST)){
            $item['global_newsletter'] = $this->input->post('global-newsletter');
            $item['local_newsletter'] = $this->input->post('local-newsletter');
            $item['auto_location'] = $this->input->post('auto-location');
            $item['forum_threads'] = $this->input->post('forum-threads');
            $item['private_messages'] = $this->input->post('private-messages');
            $item['friend_request_notify'] = $this->input->post('friend-request-notify');
            $item['private_messages_notify'] = $this->input->post('private-messages-notify');
            
            $local1['local'] = $this->input->post('locationAdmincp');
            $local2['local'] = $this->input->post('locationAdmincp1');
            $local3['local'] = $this->input->post('locationAdmincp2');
            $local4['local'] = $this->input->post('locationAdmincp3');
            
            $local1['country'] = $this->input->post('countryAdmincp');
            $local2['country'] = $this->input->post('countryAdmincp1');
            $local3['country'] = $this->input->post('countryAdmincp2');
            $local4['country'] = $this->input->post('countryAdmincp3');
            
            $local1['cities'] = $this->input->post('citiesAdmincp');
            $local2['cities'] = $this->input->post('citiesAdmincp1');
            $local3['cities'] = $this->input->post('citiesAdmincp2');
            $local4['cities'] = $this->input->post('citiesAdmincp3');
			
			$local = array(
				$local1,
				$local2,
				$local3,
				$local4
			);
            if($user_id){
				$this->db->where('id',$user_id);
				$this->model->update_info($item);
                if($checkUserLocation == true){
					for($i = 0; $i<4; $i++){
						$this->model->insert_local($local[$i]);
					}
                }
                else{
                    $this->db->where('user_id', $user_id);
					$this->model->delete_local();
                    $checkUserLocation = $this->model->checkUserLocation($user_id);
                    if($checkUserLocation == true){
                       for($i = 0; $i<4; $i++){
							$this->model->insert_local($local[$i]);
						}
                    }
                }           
            }
                $result = $this->model->load_list_user_info($user_id);
                if($this->model->get_id($user_id)){
                    $data['userDataLocation'] = array(
                            'id'            => $user_id,
                            'username'      => $result[0]->username,
                            'global_newsletter' => $item['global_newsletter'],                       
                            'local_newsletter' => $item['local_newsletter'],                       
                            'auto_location' => $item['auto_location'],                       
                            'forum_threads' => $item['forum_threads'],                       
                            'private_messages' => $item['private_messages'],                       
                            'friend_request_notify' => $item['friend_request_notify'],                       
                            'private_messages_notify' => $item['private_messages_notify'],                       
                            'local1' => $local1['local'],                       
                            'local2' => $local2['local'],                       
                            'local3' => $local3['local'],                       
                            'local4' => $local4['local'],                       
                    );
                    $this->session->set_userdata('userData',$data['userDataLocation']);
                }
            $debug = false;
            if($debug){
                echo $this->db->last_query();
                exit();
            }
        
        $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }
    
    public function update_user(){
        
        $id = $this->input->post('id');
        $url = $this->input->post('url');
        $location       = $this->input->post('location');
        $country        = $this->input->post('country');
        $city           = $this->input->post('city');
        $link_url_redm  = $this->input->post('link_url_redm');  
        $age            = $this->input->post('age');
        $gender         = $this->input->post('gender'); 
        $contact        = $this->input->post('contact');
        $biography      = $this->input->post('biography');
        $status_genenal = $this->input->post('status_genenal');
        $founder        = $this->input->post('founder');
        $shortbio       = $this->input->post('shortbio');
        $marketing      = $this->input->post('marketing');
        $contact        = $this->input->post('contact');
        $distributor    = $this->input->post('distributor');
        $facebook       = $this->input->post('facebook');
        $twitter        = $this->input->post('twitter');
        $bandcamp       = $this->input->post('bandcamp');
        $soundcloud     = $this->input->post('soundcloud');
        $mixcloud       = $this->input->post('mixcloud');
        $itunes         = $this->input->post('itunes');
        $beatport       = $this->input->post('beatport');
        $_thumbnail_url = $_image_url = '';

        $result = $this->model->load_list_user_info($id);
        if($this->model->get_id($id)){
        
             $data = array(
                        'location'      => $location,
                        'country'       => $country,
                        'city'          => $city,
                        'gender'        => $gender,
                        'age'           => $age,
                        'url'           => $url,
                        'biiography'    => $biography,
                        'status_genenal'=> $status_genenal,
                        'founder'       => $founder,
                        'shortbio'      => $shortbio,
                        'marketing'     => $marketing,
                        'contact'       => $contact,
                        'distributor'   => $distributor,
                        'facebook'      => $facebook,
                        'twitter'       => $twitter,
                        'bandcamp'      => $bandcamp,
                        'soundcloud'    => $soundcloud,
                        'mixcloud'      => $mixcloud,
                        'itunes'        => $itunes,
                        'beatport'      => $beatport,
                        // 'image' => $_image_url,
                        // 'thumbnail' => $_thumbnail_url
            );

            if( ! empty($_POST['thumbnail_urlAdmincp'])) {
                    $pre_url = $_POST['thumbnail_urlAdmincp'];
                    $_thumbnail_url = move_file_from_url('thumb_avatar', $pre_url, TRUE);
                    $data['thumbnail'] = $_thumbnail_url;
            } 
            if( ! empty($_POST['image_urlAdmincp']) ) {
                    $pre_url = $_POST['image_urlAdmincp'];
                    $_image_url = move_file_from_url('image', $pre_url, FALSE);
                    $data['image'] = $_image_url;
            }

            $test['profile'] = $this->model->profile_user($link_url_redm);

            foreach ($test['profile'] as $result) {
                 $test['id_newfeed'] = $result->id;
            }

            if(!isset($test['id_newfeed'])){
                 $data['link_url_redm'] =  $this->model->link_url_redm($link_url_redm);
            }



            $this->db->where('id',$id);
             if($this->model->update_info($data)){
             }
             // FOR DEBUG
             $debug = false;
            if($debug){
            echo $this->db->last_query();
            exit();
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }
    
	public function profile($slug = ''){  
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }

        if( !$slug ) {
             redirect(PATH_URL);
        } 
        $data['profile_user'] = $this->model->profile_user($slug);
        foreach ($data['profile_user'] as $result) {
             $data['id_newfeed'] = $result->id;
             $name_profile = $result->username;
        }

        if(!$data['id_newfeed']){
          redirect(PATH_URL); 
        } 

        $data['profile'] = $this->model->profile($data['id_newfeed']);
        $this->template->write('title','Profile - '. $name_profile);
        $this->template->write_view('content','FRONTEND/profile',$data);
        $this->template->render();
        
    }
	
	public function ajax_profileSetting(){
		if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
		$this->template->write_view('FRONTEND/profile_setting',$data);
	}
}