<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    /*
    app_id = "342578"
    key = "df96490926882f24db50"
    secret = "ebdec46a5ec0433c3a71"
    cluster = "ap1"
     */
const PUSHER_APP_ID = '342578';
const PUSHER_KEY = 'df96490926882f24db50';
const PUSHER_SECRET = 'ebdec46a5ec0433c3a71';
const PUSHER_CLUSTER = 'ap1';

const EVENT_NEWFEED = 'newfeed';
const EVENT_NEWCOMMENT = 'newcomment';
const EVENT_NEWREPLY = 'newreply';
const EVENT_LIKEFEED = 'likefeed';
const EVENT_UPDATEFEED = 'updatefeed';
const EVENT_DELETEFEED = 'deletefeed';

class Feed extends MX_Controller {
    private $app_id;
    private $key;
    private $secret;
    private $cluster;
    private $pusher;
    private $options;
    private $isLogin;

    public function __construct() {
        parent::__construct();
        $this->load->library('PusherCI');
        $this->load->model('home_model','model');
        $this->load->model('User_model', 'user');
        $this->load->model('Feed_model', 'feed');
        $this->load->model('UserSession_model', 'userSession');
        $this->options = array(
            'cluster' => PUSHER_CLUSTER,//'ap1',
            'encrypted' => true
        );
        $this->pusher = new Pusher(
            PUSHER_KEY,//'df96490926882f24db50',
            PUSHER_SECRET,//'ebdec46a5ec0433c3a71',
            PUSHER_APP_ID,//'342578',
            $this->options
        );
        $this->isLogin = ( ! empty($this->userSession->getUserIdLogin()) ) ? TRUE : FALSE;
    }

    public function test() {
        $data = array(
            'firstname' => 'nhat',
            'lastname' => 'nguyen'
            );
        echo json_encode($data); exit;
        // $url = 'http://deeg.co/cbteu/update-admin-json-0c2975a9f86e0a0d393e1bbe579ced2/';
        // $result = @file_get_contents($url);
        // pr($result);
        // echo 'done';
        exit;
        $current = "2017-06-22T08:23:01+0000";
        $target = "2017-06-22T10:23:47+0200";
        $format = 'Y-m-d H:i:00 O';
        date_default_timezone_set('UCT');

        $formatCurrent = date($format, strtotime($current));
        $formatTarget = date($format, strtotime($target));
        $test1 = $current . ' --- ' . $target . '<br>';
        $test2 = $formatCurrent . ' --- ' . $formatTarget . '<br>';
        echo $test1;
        echo $test2;
        exit;
        // $userLoginId = $this->userSession->getUserIdLogin();
        // $isLogin = ( ! empty($userLoginId) ) ? TRUE : FALSE;
        // $data = array();
        // $data ['is_login'] = $isLogin;
        // // echo 'test '; exit;
        // $this->load->view('FRONTEND/FeedComponent/test', $data);
        // phpinfo();
        // echo strtotime('2017-03-01 00:00:00') . '<br>';
        // echo date_default_timezone_get();
        $this->user->updateUserBitId();
		// echo ( (979815581 << 20) | (10 << 8) ).'<br>';
		// $var = (979815581 * pow(2, 20));
		// $id = (10 * pow(2, 8));
		// $_bit = (int)rtrim(rtrim(sprintf('%f', $var), '0'), ".");
		// $_id = (int)rtrim(rtrim(sprintf('%f', $id), '0'), ".");
		// echo $_bit .' -- ' . $_id. '<br>';
		// echo ($_bit | $_id). '<br>';
		// echo '<br>';
		// echo $var .' -- ' . $id. '<br>';
		// echo ($var | $id). '<br>';
		// echo (973078016<<20);
        echo 'Done'; exit;
    }

    public function authPusher() {
        $result         = 'Forbidden';
        $httpStatusCode = 403;
        
        if( ! isset($_POST ['channel_name']) || ! isset($_POST ['socket_id'])) {
            header('', TRUE, $httpStatusCode);
            echo $result;
            exit;
        }

        $channelName    = $_POST ['channel_name'];
        $socketId       = $_POST ['socket_id'];

        if ($socketId !== null && !preg_match('/\A\d+\.\d+\z/', $socketId)) {
            header('', TRUE, $httpStatusCode);
            echo 'Invalid socket ID ' . $socketId;
            exit;
        }

        if($this->isPrivateChannel($channelName)) {
            $result         = $this->pusher->socket_auth($channelName, $socketId);
            $httpStatusCode = 200;
        }
        
        if($this->isPresenceChannel($channelName)) {
            $userBitId      = $this->userSession->getUserBitIdLogin();
            $result         = $this->pusher->presence_auth($channelName, $socketId, $userBitId);
            $httpStatusCode = 200;
        }

        header('', TRUE, $httpStatusCode);
        echo $result;
        exit;
    }

    public function index() {
        $data = array();
        $channel = $this->input->get('channel');
        $userId = $this->input->get('user_id');
        if(empty($userId)) {
            $userId = 2;//rand(1, 100);
        }
        $this->session->set_userdata('user_id_login', $userId);

        $data ['new_feed_url'] = base_url(AJAX_PREFIX.'/submit-feed');
        $data ['new_comment_url'] = base_url(AJAX_PREFIX.'/submit-comment');
        $data ['new_reply_url'] = base_url(AJAX_PREFIX.'/submit-reply');
        $data ['like_url'] = base_url(AJAX_PREFIX.'/like-feed');
        $data ['auth_pusher_url'] = base_url('...'); //TODO
        $data ['channel'] = $channel;
        $data ['user_id'] = $userId;
        $data ['feed'] = $this->load->view('FRONTEND/FeedComponent/feed', NULL, TRUE);
        $this->load->view('FRONTEND/test_pusher', $data);
    }

    public function showProfileByUserBitId() {
        $userBitId = $this->input->get('id');
        if(empty($userBitId)) {
            echo 'NULL'; exit;
        }
        $profileUserFeedData = $this->user->getUserDataByUserBitId($userBitId);
        if(empty($profileUserFeedData)) {
            echo 'User not exists'; exit;
        }

        $this->doShowProfileUser($profileUserFeedData);
    }

    public function showProfileByUserSlug($slug = '') {
        if(empty($slug)) {
            echo 'NULL'; exit;
        }
        $profileUserFeedData = $this->user->getUserDataByProfileSlug($slug);
        if(empty($profileUserFeedData)) {
            echo 'User not exists'; exit;
        }

        $this->doShowProfileUser($profileUserFeedData);
    }

    public function doShowProfileUser($profileUserFeedData) {
        $data = array();

        $userFeedId = (int)$profileUserFeedData['id'];
        $begin = 1;
        $perPage = 3;
        //get Feeds of target user
        $feeds = $this->feed->getFeedsByUserId($userFeedId, $begin, $perPage);
        $totalPaging = $this->feed->getTotalPagingFeedByUserId($userFeedId, $perPage);
        $pagingData = $this->calculateNextPaging($begin, $totalPaging);
        $listFeedHtml = $this->renderListFeedHtml($feeds);
        $channel = $profileUserFeedData ['user_bit_id'];

        $userLoginId = $this->userSession->getUserIdLogin();
        $isUserOwnerFeed = ($userFeedId == $userLoginId) ? TRUE : FALSE;
        $avatarUrl = get_resource_url($this->userSession->getUserLoginAvatar());
        $nameProfile = $profileUserFeedData ['username'];
        $data ['avatar_url']       = $avatarUrl;
        $data ['new_feed_url']     = base_url(FEED_PREFIX.'/submit-feed');
        $data ['new_comment_url']  = base_url(FEED_PREFIX.'/submit-comment');
        $data ['new_reply_url']    = base_url(FEED_PREFIX.'/submit-reply');
        $data ['like_url']         = base_url(FEED_PREFIX.'/like-feed');
        $data ['load_feed_url']    = base_url(FEED_PREFIX.'/load-feed');
        $data ['load_comment_url'] = base_url(FEED_PREFIX.'/load-comment');
        $data ['load_reply_url']   = base_url(FEED_PREFIX.'/load-reply');
        $data ['load_num']         = $pagingData ['nextPaging'];
        $data ['has_paging']       = $pagingData ['hasPaging'];
        $data ['channel']          = $channel;
        $data ['user_id']          = $userLoginId;
        $data ['is_user_owner_feed'] = $isUserOwnerFeed;
        $data ['is_login']         = $this->isLogin;
        $data ['user_feed_data'] = $profileUserFeedData;
        $data ['listFeedHtml']     = $listFeedHtml;

        $data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
        // Begin: Get Header Ticker
        $data['header_ticker'] = $this->model->get_header_ticker();
        // End: Get Header Ticker

        $this->template->write('title','Profile - '. $nameProfile);
        $this->template->write_view('content','FRONTEND/FeedComponent/profile', $data);
        $this->template->render();
    }

    public function load_feed() {
        $result = array();
        $channel = $this->input->post('channel');
        $paging = $this->input->post('paging');
        if(empty($channel) || empty($paging)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }

        $profileUserFeedData = $this->user->getUserDataByUserBitId($channel);
        if(empty($profileUserFeedData)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'channel not exists';
            return_json_result($result);
        }

        $userId = (int)$profileUserFeedData['id'];
        $perPage = 3;
        $totalPaging = $this->feed->getTotalPagingFeedByUserId($userId, $perPage);
        if($paging > $totalPaging) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'paging value invalid';
            $result ['hasPaging'] = 0;
            return_json_result($result);
        }

        $pagingData = $this->calculateNextPaging($paging, $totalPaging);
        $feeds = $this->feed->getFeedsByUserId($userId, $paging, $perPage);
        $listFeedHtml = $this->renderListFeedHtml($feeds);
        
        $result ['status'] = STATUS_SUCCESS;
        $result ['paging'] = $pagingData ['nextPaging'];
        $result ['hasPaging'] = $pagingData ['hasPaging'];
        $result ['listFeedHtml'] = $listFeedHtml;
        
        return_json_result($result);
    }

    public function load_comment() {
        $result = array();
        $channel = $this->input->post('channel');
        $feedBitId = $this->input->post('feedBitId');
        $paging = $this->input->post('paging');
        $perPage = 3;

        if(empty($channel) || empty($feedBitId) || empty($paging)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }
        //check feedId exists
        $feedId = $this->feed->checkFeedExists($feedBitId);
        $totalPaging = $this->feed->getTotalPagingCommentByFeedId($feedId, $perPage);
        if($paging > $totalPaging) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'paging value invalid';
            $result ['hasPaging'] = 0;
            return_json_result($result);
        }
        $pagingData = $this->calculateNextPaging($paging, $totalPaging);
        $comments = $this->feed->getCommentsByFeedId($feedId, $paging, $perPage);
        $listCommentHtml = $this->renderListCommentHtml($comments);
        $result ['status']          = STATUS_SUCCESS;
        $result ['feedBitId']       = $feedBitId;
        $result ['paging']          = $pagingData ['nextPaging'];
        $result ['hasPaging']       = $pagingData ['hasPaging'];
        $result ['listCommentHtml'] = $listCommentHtml;

        return_json_result($result);
    }

    public function load_reply() {
        $result = array();
        $channel      = $this->input->post('channel');
        $feedBitId    = $this->input->post('feedBitId');
        $commentBitId = $this->input->post('commentBitId');
        $paging       = $this->input->post('paging');
        $perPage = 3;
        if(empty($channel) || empty($feedBitId) || empty($commentBitId) || empty($paging)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }
        //check comment exists
        $commentData = $this->feed->checkCommentExists($feedBitId, $commentBitId);
        $commentId   = $commentData ['id'];
        $feedId      = $commentData ['id_newfeed'];
        $totalPaging = $this->feed->getTotalPagingReplyByFeedIdAndCommentId($feedId, $commentId, $perPage);
        if($paging > $totalPaging) {
            $result ['status']    = STATUS_FAIL;
            $result ['message']   = 'paging value invalid';
            $result ['hasPaging'] = 0;
            return_json_result($result);
        }

        $pagingData = $this->calculateNextPaging($paging, $totalPaging);
        $replies = $this->feed->getRepliesByFeedIdAndCommentId($feedId, $commentId, $paging, $perPage);
        $listReplyHtml = $this->renderListReplyHtml($replies);
        
        $result ['status']        = STATUS_SUCCESS;
        $result ['feedBitId']     = $feedBitId;
        $result ['commentBitId']  = $commentBitId;
        $result ['paging']        = $pagingData ['nextPaging'];
        $result ['hasPaging']     = $pagingData ['hasPaging'];
        $result ['listReplyHtml'] = $listReplyHtml;

        return_json_result($result);
    }

    /**
     * [submit_feed description]
     * @return [type] [description]
     */
    public function submit_feed() {
        $event = 'newfeed';
        $result = $data = $viewdata = array();

        $channel = strval($this->input->post('channel'));
        $feedContent = trim($this->input->post('feedContent'));

        if(empty($channel) || empty($feedContent)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }

        $userId = $this->userSession->getUserIdLogin();
        if(empty($userId)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'you are not login';
            return_json_result($result);
        }

        $feedData = $this->feed->saveNewFeed($userId, $feedContent);
        if($feedData === FALSE) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'post feed faild, try again';
            return_json_result($result);
        }
        $userData = $this->userSession->getUserLoginData();
        $feedHtml = $this->renderFeedDetailHtml($feedData, $userData, TRUE);

        $data ['feedHtml'] = $feedHtml;
        $this->pusher->trigger($channel, $event, $data);

        $result ['status'] = STATUS_SUCCESS;
        $result ['message'] = 'post feed successfully';
        return_json_result($result);
    }

    public function update_feed() {
        #TODO
    }

    public function delete_feed() {
        #TODO
    }

    public function like_feed() {
        $event = 'likefeed';
        $result = $data = $viewdata = array();

        $feedBitId = trim($this->input->post('feedBitId'));
        $channel   = strval($this->input->post('channel'));
        if(empty($channel) || empty($feedBitId)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }
        //todo count like of feed
        $data ['feedBitId'] = $feedBitId;
        $data ['numLike'] = rand(1, 100);
        $this->pusher->trigger($channel, $event, $data);

        $result ['status'] = STATUS_SUCCESS;
        $result ['message'] = 'like feed successfully';
        return_json_result($result);
    }

    public function submit_comment() {
        $event = 'newcomment';
        $result = $data = $viewdata = array();
        
        $channel        = strval($this->input->post('channel'));
        $feedBitId      = trim($this->input->post('feedBitId'));
        $commentContent = trim($this->input->post('commentContent'));

        if(empty($channel) || empty($feedBitId) || empty($commentContent)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }

        $userId = $this->userSession->getUserIdLogin();
        if(empty($userId)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'you are not login';
            return_json_result($result);
        }

        $feedId = $this->feed->checkFeedExists($feedBitId);
        if(empty($feedId)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'feed not exists';
            return_json_result($result);
        }

        $commentData = $this->feed->saveComment($userId, $feedId, $feedBitId, $commentContent);
        if($commentData === FALSE) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'post comment faild, try again';
            return_json_result($result);
        }
        $userData = $this->userSession->getUserLoginData();
        $commentHtml = $this->renderCommentDetailHtml($commentData, $userData, TRUE);
        //update total comment
        $totalComment = $this->feed->updateNumCommentOfFeed($feedId);

        $data ['feedBitId'] = $feedBitId;
        $data ['commentHtml'] = $commentHtml;
        $data ['totalComment'] = $totalComment;
        $this->pusher->trigger($channel, $event, $data);

        $result ['status'] = STATUS_SUCCESS;
        $result ['message'] = 'post comment successfully';
        return_json_result($result);
    }

    public function update_comment() {
        #TODO
    }

    public function delete_comment() {
        #TODO
    }

    public function submit_reply() {
        $event = 'newreply';
        $result = $data = $viewdata = array();

        $channel      = strval($this->input->post('channel'));
        $feedBitId    = trim($this->input->post('feedBitId'));
        $commentBitId = trim($this->input->post('commentBitId'));
        $replyContent = trim($this->input->post('replyContent'));

        if(empty($channel) || empty($feedBitId) || empty($commentBitId) || empty($replyContent)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'missing required fields';
            return_json_result($result);
        }

        $userId = $this->userSession->getUserIdLogin();
        if(empty($userId)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'you are not login';
            return_json_result($result);
        }

        $commentData = $this->feed->checkCommentExists($feedBitId, $commentBitId);
        if(empty($commentData)) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'comment not exists';
            return_json_result($result);
        }
        $commentId = $commentData ['id'];
        $feedId = $commentData ['id_newfeed'];

        $replyData = $this->feed->saveReply($userId, $feedId, $feedBitId, $commentId, $commentBitId, $replyContent);
        if($replyData === FALSE) {
            $result ['status'] = STATUS_FAIL;
            $result ['message'] = 'post reply faild, try again';
            return_json_result($result);
        }
        $userData = $this->userSession->getUserLoginData();

        $replyHtml = $this->renderReplyDetailHtml($replyData, $userData, TRUE);
        $data ['feedBitId'] = $feedBitId;
        $data ['commentBitId'] = $commentBitId;
        $data ['replyHtml'] = $replyHtml;
        $this->pusher->trigger($channel, $event, $data);

        $result ['status'] = STATUS_SUCCESS;
        $result ['message'] = 'post reply successfully';
        return_json_result($data);
    }

    public function update_reply() {
        #TODO
    }

    public function delete_reply() {
        #TODO
    }

    /**
     * [renderFeedDetailHtml description]
     * @param  [type]  $feedData   [description]
     * @param  [type]  $userData   [description]
     * @param  boolean $returnHtml [description]
     * @return [type]              [description]
     */
    protected function renderFeedDetailHtml($feedData, $userData = NULL, $returnHtml = FALSE) {
        $data = array();
        $data ['feedData'] = $feedData;
        if( ! empty($userData)) {
            $data ['userData'] = $userData;
        }
        return $this->load->view('FRONTEND/FeedComponent/feed_detail_prototype', $data, $returnHtml);
    }

    /**
     * [renderCommentDetailHtml description]
     * @param  [type]  $commentData [description]
     * @param  [type]  $userData    [description]
     * @param  boolean $returnHtml  [description]
     * @return [type]               [description]
     */
    protected function renderCommentDetailHtml($commentData, $userData = NULL, $returnHtml = FALSE) {
        $data = array();
        $data ['commentData'] = $commentData;
        if( ! empty($userData)) {
            $data ['userData'] = $userData;
        }
        // if(!empty($commentData ['id']) && $commentData ['id'] == 16) {
        //  pr($commentData, 1);
        // } #test debug
        return $this->load->view('FRONTEND/FeedComponent/comment_detail_prototype', $data, $returnHtml);
    }

    /**
     * [renderReplyDetailHtml description]
     * @param  [type]  $replyData  [description]
     * @param  [type]  $userData   [description]
     * @param  boolean $returnHtml [description]
     * @return [type]              [description]
     */
    protected function renderReplyDetailHtml($replyData, $userData = NULL, $returnHtml = FALSE) {
        $data = array();
        $data ['replyData'] = $replyData;
        if( ! empty($userData)) {
            $data ['userData'] = $userData;
        }
        return $this->load->view('FRONTEND/FeedComponent/reply_detail_prototype', $data, $returnHtml);
    }

    /**
     * [renderListFeedHtml generate HTML of list feed]
     * @param  [array] $feeds [list feed data]
     * @return [string]        [content html of list feed]
     */
    protected function renderListFeedHtml($feeds) {
        $result = '';
        if(is_array($feeds)) {
            foreach ($feeds as $key => $feed) {
                $feedId  = (int)$feed ['id'];
                $begin   = 1;
                $perPage = 3;
                //get list comment
                $comments        = $this->feed->getCommentsByFeedId($feedId, $begin, $perPage);
                //check paging
                $totalPaging     = $this->feed->getTotalPagingCommentByFeedId($feedId, $perPage);
                $pagingData      = $this->calculateNextPaging($begin, $totalPaging);
                $listCommentHtml = $this->renderListCommentHtml($comments);

                $feed ['listCommentHtml'] = $listCommentHtml;
                $feed ['load_num']        = $pagingData ['nextPaging'];
                $feed ['has_paging']      = $pagingData ['hasPaging'];
                $feed ['user_is_login']   = $this->isLogin;             
                $result .= $this->renderFeedDetailHtml($feed, NULL, TRUE);
            }
        }
        return $result;
    }

    /**
     * [renderListCommentHtml description]
     * @param  [array] $comments [description]
     * @return [string]           [description]
     */
    protected function renderListCommentHtml($comments) {
        $result = '';
        if(is_array($comments)) {
            foreach ($comments as $key => $comment) {
                $feedId    = (int)$comment ['id_newfeed'];
                $commentId = (int)$comment ['id'];
                $begin     = 1;
                $perPage   = 3;
                //get list reply
                $replies       = $this->feed->getRepliesByFeedIdAndCommentId($feedId, $commentId, $begin, $perPage);
                //check paging
                $totalPaging   = $this->feed->getTotalPagingReplyByFeedIdAndCommentId($feedId, $commentId, $perPage);
                $pagingData    = $this->calculateNextPaging($begin, $totalPaging);
                $listReplyHtml = $this->renderListReplyHtml($replies);

                $comment ['listReplyHtml'] = $listReplyHtml;
                $comment ['load_num']      = $pagingData ['nextPaging'];
                $comment ['has_paging']    = $pagingData ['hasPaging'];
                $comment ['user_is_login'] = $this->isLogin;
                $result .= $this->renderCommentDetailHtml($comment, NULL, TRUE);
            }
        }
        return $result;
    }

    /**
     * [renderListReplyHtml description]
     * @param  [array] $replies [description]
     * @return [string]          [description]
     */
    protected function renderListReplyHtml($replies) {
        $result = '';
        if(is_array($replies)) {
            foreach ($replies as $key => $reply) {
                $reply ['user_is_login'] = $this->isLogin;
                $result .= $this->renderReplyDetailHtml($reply, NULL, TRUE);
            }
        }
        return $result;
    }

    /**
     * [calculateNextPaging description]
     * @param  [type] $currentPaging [description]
     * @param  [type] $totalPaging   [description]
     * @return [type]                [description]
     */
    protected function calculateNextPaging($currentPaging, $totalPaging) {
        $result = array();
        if($currentPaging >= $totalPaging) {
            $result ['nextPaging'] = NULL;
            $result ['hasPaging'] = 0;
        } elseif($currentPaging + 1 < $totalPaging) {
            $result ['nextPaging'] = $currentPaging + 1;
            $result ['hasPaging'] = 1;
        } else {
            $result ['nextPaging'] = $totalPaging;
            $result ['hasPaging'] = 1;
        }
        return $result;
    }

    protected function isPrivateChannel($channelName) {
        $privatePrefixChannel = 'private-';
        return (substr($channelName, 0, strlen($privatePrefixChannel)) === $privatePrefixChannel) ? TRUE : FALSE;
    }

    protected function isPresenceChannel($channelName) {
        $presencePrefixChannel = 'presence-';
        return (substr($channelName, 0, strlen($presencePrefixChannel)) === $presencePrefixChannel) ? TRUE : FALSE;
    }
}