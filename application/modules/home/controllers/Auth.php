<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MX_Controller {
    
    private $module = 'home';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('home_model','model');
        $this->load->model('UserSession_model','userSession');

         $this->template->set_template('default');
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        $this->load->library('session');
        $this->load->library('facebook');

    }

    public function index(){ 
         
        $data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
		// Begin: Get Header Ticker
		$data['header_ticker'] = $this->model->get_header_ticker();
		// End: Get Header Ticker

		
        $this->template->write('title','Home page');
        $this->template->write_view('content','FRONTEND/index',$data);
        $this->template->render();    
    }
	
	
    public function signup(){ 
       
        $email          = $this->input->post('email');
        $username       = $this->input->post('username');
        $password       = $this->hashpassword($this->input->post('password'));
         // link proflie/link_url_redm
        $link_url_redm  = $this->model->link_url_redm($username);
        // age - gender
        $age            = $this->input->post('age');
        $gender         = $this->input->post('gender'); 
        $location       = $this->input->post('location');
        $country        = $this->input->post('country');
        $city           = $this->input->post('city');
        $url            = $this->input->post('url');
        $biiography     = $this->input->post('biiography');
        $status_genenal = $this->input->post('status_genenal');
        $founder        = $this->input->post('founder');
        $shortbio       = $this->input->post('shortbio');
        $marketing      = $this->input->post('marketing');
        $contact        = $this->input->post('contact');
        $distributor    = $this->input->post('distributor');
        $facebook       = $this->input->post('facebook');
        $twitter        = $this->input->post('twitter');
        $bandcamp       = $this->input->post('bandcamp');
        $soundcloud     = $this->input->post('soundcloud');
        $mixcloud       = $this->input->post('mixcloud');
        $itunes         = $this->input->post('itunes');
        $beatport       = $this->input->post('beatport');
        $user_type_id   = $this->input->post('gender');  


        switch ($user_type_id) {
           case '1':
           $user_type_id =1;
                break;
            case '2':
                $user_type_id=2;
                break;
            case '3':
                $user_type_id=3;
                break;
            case '4':
                $user_type_id=4;
                break;
            case '5':
                $user_type_id=5;
                break;
            case '6':
                 $user_type_id=6;
                break;
            case '7':
                $user_type_id=7;
                break;
        }
        if(!empty($email) && !empty($username))
        {
            if($this->model->check_mail_info($email))
            {
                $data = array(
                    'email'         => $email,
                    'username'      => $username,
                    'link_url_redm' => $link_url_redm,
                    'location'      => $location,
                    'country'       => $country,
                    'city'          => $city,
                    'age'           => $age,
                    'gender'        => $gender,
                    'user_type_id'  => $user_type_id,
                    'password'      => $password,
                    'url'           => $url,
                    'biiography'    => $biiography,
                    'status_genenal'=> $status_genenal,
                    'founder'       => $founder,
                    'shortbio'      => $shortbio,
                    'marketing'     => $marketing,
                    'contact'       => $contact,
                    'distributor'   => $distributor,
                    'facebook'      => $facebook,
                    'twitter'       => $twitter,
                    'bandcamp'      => $bandcamp,
                    'soundcloud'    => $soundcloud,
                    'mixcloud'      => $mixcloud,
                    'itunes'        => $itunes,
                    'beatport'      => $beatport,
                    );
              
                if($this->model->insert_info($data)){
                   
                    echo 1;
                }
            } 
            else 
            {
              
                echo 0;
            }
        }
        exit();
    }

    //login username/email function
    public function login(){
        
        $email = $username = $this->input->post('username');
        $password = $this->hashpassword($this->input->post('password')) ;
        $request_headers = $this->input->request_headers();

        // check data login
        $result = $this->model->login_info($username,$password,$email);
        if($result){    
            // check username exits
            $result = $this->model->check_user_info($username,$email);
            $user_id = $result[0]->id;
            $resultlocation = $this->model->load_list_location_user($user_id);
            if($result != false){
                $this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
				$this->repu_daily_login();
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = $request_headers ['Referer'];
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }

    //login google
    public function login_google(){
		
        include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
		
        $clientId = GG_CLIENT_ID;
        $clientSecret = GG_CLIENT_SECRET;
        $redirectUrl = base_url() . 'ajax/login_gg';
		
        $gClient = new Google_Client();

        $gClient->setApplicationName('Login to Redm');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $gClient->setScopes(array(
                'email', 
                'profile', 
                'https://www.googleapis.com/auth/plus.login'
                ));
       
        if ( empty($this->input->get('code')) ) {
            redirect($gClient->createAuthUrl());
        } else {

            try {
                //Create google OAuth2 Service
                $google_oauthV2 = new Google_Oauth2Service($gClient);
                //pr($google_oauthV2,1);
                $gClient->authenticate();
                $access_token = $gClient->getAccessToken();
                // $plus->people->get('me');
                $userProfile = $google_oauthV2->userinfo->get();
                if(!empty($userProfile['email']) && !empty($userProfile['family_name']) && !empty($userProfile['given_name'])){
                    $userData['oauth_provider'] = 'google';
                    $userData['oauth_uid'] = $userProfile['id'];
                    $userData['first_name'] = $userProfile['given_name'];
                    $userData['last_name'] = $userProfile['family_name'];
                    $userData['email'] = $userProfile['email'];
                    $userData['locale'] = $userProfile['locale'];
                    $userData['picture_url'] = $userProfile['picture'];
                    // //BEGIN FIX MISSING FIELDS GOOGLE ACCOUNT
                    // // Insert or update user data
                     $userID = $this->model->checkUser($userData);
                    if(!empty($userID)){
                        $this->userSession->setSessionWhenLoginSuccessByUserId($userID);
						$this->repu_daily_login();
                    } else {
                        $data['userData'] = array();
                     }
                    redirect(base_url());
                    // //END FIX MISSING FIELDS GOOGLE ACCOUNT
                    //TODO redirect to personal profile, set session token in web ....
                }else{
                    redirect(base_url());
                }
            } 
            catch (Exception $e) {
                redirect(base_url());
            }
        } 
    }

    //login facebook function
    public function login_fb(){
        $userData = array();
        $result_url = PATH_URL;
       
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            // Preparing data for database insertion
            if(!empty($userProfile['email']) && !empty( $userProfile['first_name']) && !empty($userProfile['last_name'])){
                $userData['oauth_provider'] = 'facebook';
                $userData['oauth_uid'] = $userProfile['id'];
                $userData['first_name'] = $userProfile['first_name'];
                $userData['last_name'] = $userProfile['last_name'];
                $userData['email'] = $userProfile['email'];
                $userData['locale'] = $userProfile['locale'];
                $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
                $userData['picture_url'] = $userProfile['picture']['data']['url'];
                // Insert or update user data
                $userID = $this->model->checkUser($userData);
                // Check user data insert or update status
                if(!empty($userID)){
                    $this->userSession->setSessionWhenLoginSuccessByUserId($userID);
					$this->repu_daily_login();
                } else {
                   $data['userData'] = array();
                }
                // Get logout URL
                $data['logoutUrl'] = $this->facebook->logout_url();

            }
             else{
                 redirect(base_url());
            }
        }else{
            $fbuser = '';
            // Get login URL

            
                $result_url = $this->facebook->login_url();
        }
       
            redirect($result_url);
    }

    //Logout
    public function logout(){
        $see_data = array(
            'username'          => '',
            'id'                => '',
            'location'          => '',
            'link_url_redm'     => '',
            'country'           => '',
            'city'              => '',
            'url'               => '',
            // 'age'               => '',
            // 'gender'            => '',
            'biiography'        => '',
            'status_genenal'    => '',
            'founder'           => '',
            'shortbio'          => '',
            'marketing'         => '',
            'contact'           => '',
            'distributor'       => '',
            'facebook'          => '',
            'twitter'           => '',
            'bandcamp'          => '',
            'beatport'          => '',
            'soundcloud'        => '',
            'itunes'            => '',
            'mixcloud'          => '',
            );
        $this->facebook->destroy_session();
        $this->session->unset_userdata('userData', $see_data);
        // Remove user data from session
        $this->session->unset_userdata('userData');
        $this->session->unset_userdata('token');
        $this->session->sess_destroy();
        $request_headers = $this->input->request_headers();
        redirect($request_headers ['Referer']);
    }

    public function hashpassword($password) {
        return md5($password);
    }
    
    public function changePassword(){
        $userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
        $pass = $this->model->get_password($user_id);
        $password = $pass[0]->password;
        if(!empty($_POST)){
        $currentpass = $_POST['currentPass'];
        $newpass = $_POST['newPass'];
        $confirmpass = $_POST['confirmPass'];
        $currentPassmd5 = $this->hashpassword($currentpass);
            if($password==$currentPassmd5){
                if($newpass != $currentpass){$is_update = 0;
                    if($newpass == $confirmpass){
                        $item = array();
                        $item['password'] = $this->hashpassword($newpass);
                        $this->db->where('id',$user_id);
                        $is_update = $this->db->update('admin_nqt_user_info',$item);
                        $response['success'] = $is_update ? true : false;
                    }else{
                        print 'wrong-password-confirm';
                        exit;
                    }
                    echo json_encode($response);
                }else{
                    print 'repeated-password';
                    exit;
                }
            }else{
                print 'wrong-password';
                exit;
            }
        }
    }
    
    public function for_got_password(){
      if(!empty($_POST)){
        $data = array();
        $email = $this->input->post('email');
        $checkmail = $this->model->check_mail_info($email);
        if($checkmail == false){
            $newPass = generate_random_string(8);
            $newPassMD5 = $this->hashpassword($newPass);
            $subject = 'Send New Password';
            $body = 'NEW PASSWORD:'.$newPass;
            $result = ses_send_mail($subject,$body,$email);
            $response['success'] = $result ? true : false;
            if($response['success'] == true){
                $data['password'] = $newPassMD5;
                $this->db->where('email', $email);
                $this->db->update('admin_nqt_user_info',$data);
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
      }
      echo json_encode($_data); exit;
    }
    
	public function repu_daily_login(){
		$userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
		$checkUser = $this->model->checkUserIdDailyLogin($user_id);
		$data_repu = $this->model->get_time_last_login($user_id);
		$date_current_login = date('Y-m-d 00:00:00');
		
		if($checkUser == true){
			$data['user_id'] = $user_id;
			$data['date_login_last'] = $date_current_login;
			$data['daily_login_value'] = 0;
			$this->db->insert('admin_nqt_reputation',$data);
		}
		else{
			if(!empty($data_repu)){
				$daily_value = $data_repu[0]->daily_login_value;
				$date_last_login_old = strtotime($data_repu[0]->date_login_last);
				$date_last_login_new = strtotime(date('Y-m-d 00:00:00'));
				$distance_date = (int)($date_last_login_new - $date_last_login_old)/86400;
				
				if($distance_date === 1){
					$item['daily_login_value']  = $daily_value + 1;
					$item['date_login_last']  = $date_current_login;
					$this->db->where('user_id',$user_id);
					$this->db->update('admin_nqt_reputation',$item);
				}
				else{
					if($distance_date > 1){
						$item['daily_login_value'] = 0 ;
						$item['date_login_last']  = $date_current_login;
						$this->db->where('user_id',$user_id);
						$this->db->update('admin_nqt_reputation',$item);
					}
				}
			}
		}
	}
}