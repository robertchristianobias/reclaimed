<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post extends MX_Controller {
    
    private $module = 'home';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('home_model','model');
        $this->load->model('UserSession_model','userSession');

         $this->template->set_template('default');
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        $this->load->library('session');
        $this->load->library('facebook');

    }
	
    public function get_articles_featured(){
        return $this->model->get_articles_featured();
    }

    public function get_articles_latest(){
        return $this->model->get_articles_latest();
    }

    public function get_articles_country($country){
        return $this->model->get_articles_country($country);
    }

    private function _is_login(){
        if ($this->session->userdata('token')) {
            redirect(base_url());
        }
        elseif ( ! $this->input->is_ajax_request()) {
            redirect(base_url('/'));
        }
    }
   
    public function blog($slug = '',$start=0) {
        if(!$slug){
            $slug = $this->model->get_articles_blog_feature_new();
            foreach ($slug as $value) {
                $slug =  $value->slug;
            }
            if(!$slug){
            redirect(PATH_URL);
            }
            $data['hidden'] = 'hidden';
        } 

        $start = (int)$start;
        $limit = 2;
        $limit_news= '1';
        $request_headers = $this->input->request_headers();
		
        $lang = $this->lang->default_lang();
        $item_total_list = $this->model->get_item_list();
        

        $data['type_list'] = $this->model->load_list_user();
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
            $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata['userData']['id']);
        }
		$data['item_list'] = '';
        if(!empty($item_total_list)){
            $total_rows = count($item_total_list);
            $item_list = $this->model->get_item_list($start, $limit);
			$data['item_list'] = $item_list;
            $current_url = PATH_URL.$lang.'/blog';
            paginate($this,$current_url,$total_rows,$start,$limit);
        }
        $data['news'] = $this->model->get_latest_news($lang,$limit_news);
        $data['news_related'] = $this->model->get_related_news();

        $data['articles_blog'] =  $this->model->get_articles_blog($slug);

        $data['select_blog_view'] = $this->model->select_blog_view($slug);

        foreach ($data['select_blog_view'] as $result){
            $data['select_blog_view'] = $result->view;
        }

        $data['update_blog_view'] = $this->model->update_blog_view($slug,$data['select_blog_view'] );

        //pr($data['news_related'],1);
        $this->template->write('title',$data['news'][0]['title']);
        $this->template->write_view('content','FRONTEND/blog',$data);
        $this->template->render(); 
    }

    function get_articles_blog_category($category_id,$articles_id){
        return $this->model->get_articles_blog_category($category_id,$articles_id);
    }

    function get_user_comment($articles_id){
       
        return $this->model->get_user_comment($articles_id);
    }
    function get_user_comment_child($articles_id,$id_comment_parent){
       
        return $this->model->get_user_comment_child($articles_id,$id_comment_parent);
    }


    function insert_user_comment(){
        $user_info_id = $this->input->post('user_info_id');
        $id_artcles_item_lang = $this->input->post('id_artcles_item_lang');
        $id_artcles_data = $this->model->get_id_artcles($id_artcles_item_lang);
        $id_artcles = $id_artcles_data[0]->articles_id;
        $comment = $this->input->post('comment');
        $this->model->insert_user_comment($user_info_id,$id_artcles_item_lang,$id_artcles,$comment);
    }

    function insert_user_comment_child(){
        $user_info_id = $this->input->post('user_info_id');
        $id_artcles_item_lang = $this->input->post('id_artcles_item_lang');
		$id_artcles_data = $this->model->get_id_artcles($id_artcles_item_lang);
        $id_artcles = $id_artcles_data[0]->articles_id;
        $id_comment_parent = $this->input->post('id_comment_parent');
        $comment = $this->input->post('comment');
        $this->model->insert_user_comment_child($user_info_id,$id_artcles_item_lang,$id_artcles,$id_comment_parent,$comment);
    }

    function ajax_loadcomment(){
        $articles_id['articles_id'] = $this->input->post('articles_id');
        $this->load->view('FRONTEND/ajax_loadComment', $articles_id);
    }

    function ajax_loadcomment_reply(){
        $articles_id['articles_id'] = $this->input->post('articles_id');
        $articles_id['id_comment_parent'] = $this->input->post('id_comment_parent');
        $this->load->view('FRONTEND/ajax_loadComment_reply', $articles_id);
    }
}