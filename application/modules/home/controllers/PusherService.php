<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	/*
	app_id = "342578"
	key = "df96490926882f24db50"
	secret = "ebdec46a5ec0433c3a71"
	cluster = "ap1"
	 */
const PUSHER_APP_ID = '342578';
const PUSHER_KEY = 'df96490926882f24db50';
const PUSHER_SECRET = 'ebdec46a5ec0433c3a71';
const PUSHER_CLUSTER = 'ap1';

class PusherService extends MX_Controller {
	private $app_id;
	private $key;
	private $secret;
	private $cluster;
	private $pusher;
	private $options;

	public function __construct() {
		parent::__construct();
        $this->load->library('PusherCI');
        $this->options = array(
            'cluster' => PUSHER_CLUSTER,//'ap1',
            'encrypted' => true
        );
        $this->pusher = new Pusher(
            PUSHER_KEY,//'df96490926882f24db50',
            PUSHER_SECRET,//'ebdec46a5ec0433c3a71',
            PUSHER_APP_ID,//'342578',
            $this->options
        );
	}
	public function index() {
		$data = array();
		$channel = $this->input->get('channel');
		$userId = $this->input->get('user_id');
		if(empty($userId)) {
			$userId = rand(1, 100);
		}
		$this->session->set_userdata('user_id_test', $userId); #user_id

		$data ['new_feed_url'] = base_url(AJAX_PREFIX.'/submit-feed');
		$data ['new_comment_url'] = base_url(AJAX_PREFIX.'/submit-comment');
		$data ['new_reply_url'] = base_url(AJAX_PREFIX.'/submit-reply');
		$data ['like_url'] = base_url(AJAX_PREFIX.'/like-feed');
		$data ['channel'] = $channel;
		$data ['user_id'] = $userId;
		$data ['feed'] = $this->load->view('FRONTEND/FeedComponent/feed', NULL, TRUE);
		$this->load->view('FRONTEND/test_pusher', $data);
	}

	public function test() {
		$result = array();
		$string = 'feed, comment, like, share, unlike, follow, user';
		$action = explode(', ', $string);
		foreach ($action as $key => $type) {
			$sum = 0;
			$arr = str_split($type);
			foreach($arr as $k => $item) {
				$sum += intval(ord($item));
			}
			$result[] = array(
				'action' => $type,
				'num' => $sum
				);
			unset($sum);
		}
		pr($result, 1);
		
		// echo $sum;
	}

	public function submit_feed() {
		$result = $data = $viewdata = array();
		$content = trim($this->input->post('content'));
		$user = $this->input->post('user');
		
		$channel = strval($this->input->post('user'));
		$event = 'newfeed';
		$userId = $this->session->userdata('user_id_test'); #user_id
		//generate feed ID
		$feedId = $this->generateNextId($userId, 'newfeed');
		$viewdata ['feedId']  = $feedId;
		$viewdata ['channel'] = $channel;
		$viewdata ['content'] = $content;
		$feedHtml = $this->load->view('FRONTEND/FeedComponent/feed_detail_prototype', $viewdata, TRUE);
		$data ['feedHtml'] = $feedHtml;
		$this->pusher->trigger($channel, $event, $data);
		$this->output
        ->set_content_type('application/json', 'UTF-8')
        ->set_output(json_encode($data));
	}

	public function submit_comment() {
		$result = $data = $viewdata = array();
		$channel = strval($this->input->post('user'));
		$feedId = trim($this->input->post('feedId'));
		$commentContent = trim($this->input->post('commentContent'));
		$event = 'newcomment';

		$userId = $this->session->userdata('user_id_test'); #user_id
		//generate comment ID
		$commentId = $this->generateNextId($userId, 'newcomment');
		$viewdata ['commentId']  = $commentId;
		$viewdata ['channel'] = $channel;
		$viewdata ['feedId'] = $feedId;
		$viewdata ['comment'] = $commentContent;
		$commentHtml = $this->load->view('FRONTEND/FeedComponent/comment_detail_prototype', $viewdata, TRUE);
		$data ['feedId'] = $feedId;
		$data ['commentHtml'] = $commentHtml;
		$this->pusher->trigger($channel, $event, $data);
		$this->output
        ->set_content_type('application/json', 'UTF-8')
        ->set_output(json_encode($data));
	}

	public function submit_reply() {
		$result = $data = $viewdata = array();
		$channel = strval($this->input->post('user'));
		$feedId = trim($this->input->post('feedId'));
		$commentId = trim($this->input->post('commentId'));
		$replyContent = trim($this->input->post('replyContent'));
		$event = 'newreply';

		$userId = $this->session->userdata('user_id_test'); #user_id
		//generate reply ID
		$replyId = $this->generateNextId($userId, 'newreply');
		$viewdata ['replyId']  = $replyId;
		$viewdata ['channel'] = $channel;
		$viewdata ['feedId'] = $feedId;
		$viewdata ['commentId'] = $commentId;
		$viewdata ['comment'] = $replyContent;
		$replyHtml = $this->load->view('FRONTEND/FeedComponent/reply_detail_prototype', $viewdata, TRUE);
		$data ['feedId'] = $feedId;
		$data ['commentId'] = $commentId;
		$data ['replyHtml'] = $replyHtml;
		$this->pusher->trigger($channel, $event, $data);
		$this->output
        ->set_content_type('application/json', 'UTF-8')
        ->set_output(json_encode($data));
	}

	public function like_feed() {
		$result = $data = $viewdata = array();
		$feedId = trim($this->input->post('feedId'));
		$channel = strval($this->input->post('user'));
		$event = 'likefeed';
		//todo count like of feed
		$data ['feedId'] = $feedId;
		$data ['numLike'] = rand(1, 100);
		$this->pusher->trigger($channel, $event, $data);
		pr('assd', 1);
		$this->output
        ->set_content_type('application/json', 'UTF-8')
        ->set_output(json_encode($result));
	}

	function getMiliSecond($delaySecond = 0) {
		$time = explode(' ', microtime());
		return ($time[1] + (int)$delaySecond) . substr(round($time[0], 10), 2);
	}

	function generateNextId($userId, $actionType, $sequence = FALSE, $intervalTimestamp = '', $delaySecond = 0) {
		$actionTypeNum = 0;
		$arr = str_split(strval($actionType));
		foreach($arr as $k => $item) {
			$actionTypeNum += intval(ord($item));
		}
		$sequence = ! empty($sequence) ? floatval($sequence) : date('Ymd', time());
		$time = explode(' ', microtime());
		$miliSecond = ($time[1] + (int)$delaySecond) . substr(round($time[0], 10), 2);
		$intervalTimestamp = ! empty($intervalTimestamp) ? $intervalTimestamp : $miliSecond;
		$id = ($intervalTimestamp << 33 )
	            | ( $userId << 13 )
	            | ($sequence << 3)
	            | $actionTypeNum;
		return $id;
	}
}