<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AddContent extends MX_Controller {
    
    private $module = 'home';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('AddContent_model','model');
         $this->template->set_template('default');
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        $this->load->library('session');
        $this->load->library('facebook');

    }
	
	public function addContent(){
		$response = array();
		$response['success']=false;
		$userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
		
		$content = trim(str_replace(array('<p>','</p>','<i>','</i>','<strong>','</strong>','&nbsp;','<br>','</br>'),'',$_POST['content'])); 
		$url = substr($content, 0, 4);
		
		$item['id_user_info'] = $user_id;
		$item['id_pro'] = $user_id;
		$item['created'] = strtotime(date('Y-m-d h:i:s'));
		$item['content'] = $content;
		
		if($url === 'http'){
			if(strpos($content, 'youtube')!= 0){
				$url = $content;
				$video_id = trim(str_replace(array('='),'',strstr( $url,'=')));

				$page = file_get_contents($url);
				$doc = new DOMDocument();
				$doc->loadHTML($page);

				$title_div = $doc->getElementById('eow-title');
				$title = $title_div->nodeValue;
				
				$metas = $doc->getElementsByTagName('meta');

				for ($i = 0; $i < $metas->length; $i++)
				{
					$meta = $metas->item($i);
					if($meta->getAttribute('name') == 'description')
						$description = $meta->getAttribute('content');
					if($meta->getAttribute('property') == 'og:video:url')
						$video = $meta->getAttribute('content');
					if($meta->getAttribute('property') == 'og:image')
						$image = $meta->getAttribute('content');
				}
				$videoUrl = strstr($video,'?',true);
				$videoEmbed = str_replace('v', 'embed', $videoUrl);
				
				$item['title'] = $title;
				$item['description'] = $description;
				$item['video'] = $videoEmbed;
				$item['image'] = $image;
				$item['url'] = $url;
				
				$result = $this->model->insert_addContent($item);
				$response['success'] = $result ? true : false;
				echo json_encode($response);
				
				
				// echo "Video: $video". '<br/><br/>';
				// echo "Image: $image". '<br/><br/>';
				// echo "Title: $title". '<br/><br/>';
				// echo "Description: $description". '<br/><br/>';
			}
			else{
				$url = $content;
				$html = $this->file_get_contents_curl($url);
				//parsing begins here:
				$doc = new DOMDocument();
				@$doc->loadHTML($html);
				$nodes = $doc->getElementsByTagName('title');

				//get and display what you need:
				$title = $nodes->item(0)->nodeValue;
				$description = '';
				$image = '';

				$metas = $doc->getElementsByTagName('meta');
				for ($i = 0; $i < $metas->length; $i++)
				{
					$meta = $metas->item($i);
					if($meta->getAttribute('property') == 'og:description')
						$description = $meta->getAttribute('content');
						
					if($meta->getAttribute('property') == 'og:image:url'){
						$image = $meta->getAttribute('content');
					} else if($meta->getAttribute('property') == 'og:image'){
						$image = $meta->getAttribute('content');
					}
				}
				$item['title'] = $title;
				$item['description'] = $description;
				$item['image'] = $image;
				$item['url'] = $url;
				// pr($item,1);
				
				$result = $this->model->insert_addContent($item);
				$response['success'] = $result ? true : false;
				echo json_encode($response);
				
				// echo "Image: $image". '<br/><br/>';
				// echo "Description: $description". '<br/><br/>';
				// echo "Title: $title". '<br/><br/>';
			}
		}
		else{
			$result = $this->model->insert_addContent($item);
			$response['success'] = $result ? true : false;
			echo json_encode($response);
		}
	}
	
	public function url_addContent(){
		$response = array();
		$response['success']=false;
		
		$content = trim(str_replace(array('<p>','</p>','<i>','</i>','<strong>','</strong>','&nbsp;'),'',$_POST['content'])); 
		$url = substr($content, 0, 4);
		
		if($url === 'http' || $url ==='www.'){
			if(strpos($content, 'youtube')!= 0){
				$url = $content;
				$video_id = trim(str_replace(array('='),'',strstr( $url,'=')));

				$page = file_get_contents($url);
				$doc = new DOMDocument();
				$doc->loadHTML($page);

				$title_div = $doc->getElementById('eow-title');
				$title = $title_div->nodeValue;
				
				$metas = $doc->getElementsByTagName('meta');

				for ($i = 0; $i < $metas->length; $i++)
				{
					$meta = $metas->item($i);
					if($meta->getAttribute('property') == 'og:description')
						$description = $meta->getAttribute('content');
					if($meta->getAttribute('property') == 'og:video:url')
						$video = $meta->getAttribute('content'); 
					if($meta->getAttribute('property') == 'og:image')
						$image = $meta->getAttribute('content');
				}
				$response['success'] = true;
				$videoUrl = strstr($video,'?',true);
				$videoEmbed = str_replace('v', 'embed', $videoUrl);
				$data = array(
					'url'=>$url,
					'title'=>$title,
					'description'=>$description,
					'videoEmbed'=>$videoEmbed,
					'image'=>$image
				);
				$html = $this->load->view('FRONTEND/post_url', $data, true);
				$response['post_url'] = $html;
				$response['success'] = true;
				
				echo json_encode ($response);
			}
			else{
				$url = $content;
				$html = $this->file_get_contents_curl($url);
				//parsing begins here:
				$doc = new DOMDocument();
				@$doc->loadHTML($html);
				$nodes = $doc->getElementsByTagName('title');

				//get and display what you need:
				$title = $nodes->item(0)->nodeValue;

				$metas = $doc->getElementsByTagName('meta');
				for ($i = 0; $i < $metas->length; $i++)
				{
					$meta = $metas->item($i);
					if($meta->getAttribute('property') == 'og:description')
						$description = $meta->getAttribute('content');
					if($meta->getAttribute('property') == 'og:image')
						$image = $meta->getAttribute('content');
				}
				$response['success'] = true;
				$videoEmbed = '';
				$data = array(
					'url'=>$url,
					'title'=>$title,
					'description'=>$description,
					'videoEmbed'=>$videoEmbed,
					'image'=>$image
				);
				$html = $this->load->view('FRONTEND/post_url', $data, true);
				$response['post_url'] = $html;
				$response['success'] = true;
				
				echo json_encode ($response);
			}
		}
	}
	
	function file_get_contents_curl($url){
		$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_URL,$url);
		$data = curl_exec($ch);

		return $data;
	}
}