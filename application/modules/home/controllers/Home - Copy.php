<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {
    
    private $module = 'home';
    private $current_page_data;
    private $current_category_data;
    private $current_detail_data;
    private $parent_url;
    private $segments;
    private $pre_uri;
    private $title = '123';

    public function __construct(){
        parent::__construct();
        //load model
        $this->load->model('home_model','model');
        $this->load->model('UserSession_model','userSession');
        $this->load->model('admincp_pro_page/admincp_pro_page_model', 'proPage');
        $this->load->model('admincp_pro_page_import/admincp_pro_page_import_model', 'proPageImport');
        $this->load->model('admincp_pro_page_claim/admincp_pro_page_claim_model', 'proPageClaim');
        $this->template->set_template('default');
        $this->current_page_data = NULL;
        $this->segments = $this->uri->segment_array();
        $this->load->library('session');
        $this->load->library('facebook');

    }

    public function test_swiftmailer() {
        $subject = 'Email send from swiftmailer smtp ' . time();
        $body ='test body - nội dung email từ smtp';
        $recepient_emails = array(
                'nhatnguyen.pix@gmail.com' => 'Nhat PIX',
                // 'guongvo.pix@gmail.com' => 'Guong PIX'
            );
        swiftmailer_send_mail($subject, $body, $recepient_emails);

        // $sender_email = 'nhatnguyen.pix@gmail.com';
        // $sender_name = 'REDM';
        // $config = array(
        //     'host'         => SMTP_HOST,
        //     'port'         => SMTP_PORT,
        //     'username'     => SMTP_USERNAME,
        //     'password'     => SMTP_PASSWORD,
        //     'encrypted'    => SMTP_ENCRYPTED,
        //     'sender_email' => SMTP_SENDER_EMAIL,
        //     'sender_name'  => SMTP_SENDER_NAME
        //     );
        // $this->load->library('SwiftMailerCI');
        // $swift_mailer_ci = new SwiftMailerCI($config);
        // // // custom sender 
        // // $swift_mailer_ci->setSender($sender_email, $sender_name);
        // $swift_mailer_ci->sendEmail($subject, $body, $recepient_emails);
    }

    /*public function index(){ 
         
        $data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
		// Begin: Get Header Ticker
		$data['header_ticker'] = $this->model->get_header_ticker();
		// End: Get Header Ticker

		
        $this->template->write('title','Home page');
        $this->template->write_view('content','FRONTEND/index',$data);
        $this->template->render();    
    }*/


    public function index(){ 
         
        $data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
        // Begin: Get Header Ticker
        $data['header_ticker'] = $this->model->get_header_ticker();
        $data['meta_homepage'] = $this->model->get_data_meta_homepage();
        // End: Get Header Ticker
        
        $this->template->write('title','Home page');
        $this->template->write_view('content','FRONTEND/index',$data);
        $this->template->render();    
    }
	
	public function newsfeed(){ 
         
        $data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
        // Begin: Get Header Ticker
        $data['header_ticker'] = $this->model->get_header_ticker();
        $data['meta_homepage'] = $this->model->get_data_meta_homepage();
        // End: Get Header Ticker
        
        $this->template->write('title','Newsfeed');
        $this->template->write_view('content','FRONTEND/homepage_newsfeed',$data);
        $this->template->render();    
    }
	public function form_register(){
        $data = $data_token = array();
		$token = $this->input->get('t');
        
        $social = rawurldecode($this->input->get('social'));
        $oauth_token = rawurldecode($this->input->get('tk'));
        $social_split = explode(':', $social);
        // pr($social_split);
        $oauth_provider = $oauth_uid = '';

        if (isset($social_split[0])) {
            switch ($social_split[0]) {
                case 'f':
                    $oauth_provider = 'facebook';
                    break;
                case 'g':
                    $oauth_provider = 'google';
                    break;
                default:
                    $oauth_provider = '';
                    break;
            }
            if ( ! empty($oauth_provider)) {
                $data ['oauth_provider'] = $oauth_provider;
            }
        }
        if (isset($social_split[1])) {
            $oauth_uid = $social_split[1];
        }
        // if ( ! empty($oauth_token)) {
        //     $data ['oauth_token'] = $oauth_token;//TODO
        // }
		$data ['oauth_uid'] = $oauth_uid;
		$data ['oauth_provider'] = $oauth_provider;
		$data ['oauth_token'] = $oauth_token;
		// pr($data);
        if (empty($oauth_provider) && empty($oauth_uid)) {
            $data['token'] = $token;
            $data_token = $this->proPage->getProPageDataByToken($token);
            if ( ! empty($data_token)) {
                $data['email'] = $data_token ['email'];
                $data['url_of_page'] = $data_token ['url_of_page'];
                $data['user_type_id'] = $data_token ['user_type_id'];
                $data['genre'] = $data_token ['genre'];
            }
        } else {
            $check_data = array();
            $check_data ['oauth_provider'] = $oauth_provider;
            $check_data ['oauth_uid'] = $oauth_uid;
            $verify = $this->verifyOAuthToken($check_data, $oauth_token);
            if ($verify) {
                $oauth_data = $this->model->getOAuthUserData($oauth_provider, $oauth_uid);
				// echo 'tsttttt';
                // pr($oauth_data);
                // $data = array_merge($oauth_data, $data);
                $data ['oauth_token'] = $oauth_token;//TODO
                $data ['email'] = $oauth_data['email'];
            }
        }
        
		$this->load->view('FRONTEND/form_register',$data);
	}
	
	public function searchByGoogle() {
		$keyword = trim($this->input->get('keyword'));
		if(!empty($keyword)){
			$data['keyword'] = $keyword;
			$this->template->write('title','Search');
			$this->template->write_view('content','FRONTEND/search',$data);
			$this->template->render(); 
		}
		else{
			redirect(PATH_URL);
		}
	}
	
	public function artist_Page(){
		$data['type_list'] = $this->model->load_list_user(); 
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }
        $data['header_ticker'] = $this->model->get_header_ticker();
		$list_artists = $this->model->get_artists();
        $artists_mapping = array();
        foreach ($list_artists as  $artists) {
           
            if(empty($artists['username'])){
                $name = '#';
            }else{
                 $name = substr($artists['username'] ,0,1);
                 $name = strtolower($name);
            }
            $artists_mapping[$name][] = $artists;

        }
        $data['artists_mapping'] = $artists_mapping; 
        $this->template->write('title','Home page');
		$this->template->write_view('content','FRONTEND/alpha',$data);
		$this->template->render();  
	}
	
	public function get_header_ticker(){
		return $this->model->get_header_ticker();
	}
    public function get_artists(){
        return $this->model->get_artists();
    }

    public function get_list_menu(){
		$result = $this->model->get_list_menu();
        return $result;
    }
     public function get_background(){
        return $this->model->get_background();
    }
    public function get_link_css(){
        //get css
        return $this->model->get_link_css();
    }
    public function get_footer_link(){
        return $this->model->footer_menu_link();
    }

    public function get_articles_featured(){
        return $this->model->get_articles_featured();
    }

    public function get_articles_latest(){
        return $this->model->get_articles_latest();
    } 
	
	public function get_articles_pro_pages(){
        return $this->model->get_articles_pro_pages();
    }

    public function get_articles_country($country){
        return $this->model->get_articles_country($country);
    }

    public function load_list_user_info($id){
        return $this->model->load_list_user_info($id);
    }
	
	public function logo_image(){
    	return $this->model->logo_image_model();
    }

    public function send_notify_by_onesignal() {
        $title = $this->input->get('title');
        $content = $this->input->get('content');
        $player_id = $this->input->get('player_id');
        $headings = array(
            'en' => $title, //'English title',
            );
        $contents = array(
            'en' => $content, //'Engish content',
            );
        $data = array(
            'key' => 'value',
            );
        $player_ids = array(
            $player_id, //'befadb7c-c008-4ebd-801c-b2829efd59b1'
            );

        $fields = array(
            'app_id' => ONESIGNAL_APP_ID,
            'include_player_ids' => $player_ids,
            'headings' => $headings,
            'contents' => $contents,
            'data' => $data,
            );
        $fields_json = json_encode($fields);
        $request_header = array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . ONESIGNAL_REST_API_KEY
            );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_json);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        pr($response, 1);
    }

    public function test_onesignal() {
        $this->load->view('FRONTEND/test_onesignal', NULL);
    }

    public function test_editor(){
        $data = array();
        $this->load->view('FRONTEND/test_get_oembed', $data);
    }
    
	private function _is_login(){
        if ($this->session->userdata('token')) {
            redirect(base_url());
        }
        elseif ( ! $this->input->is_ajax_request()) {
            redirect(base_url('/'));
        }
    }
    // Resgiter user database
    public function signup(){ 
        $token          = $this->input->post('token');
        $url_of_page    = $this->input->post('url');
		
		//****step 1
        $email          = trim($this->input->post('email'));
        $username       = trim($this->input->post('username'));
        $password       = $this->hashpassword($this->input->post('password'));
		
		//****step 2
        $user_type_id   = $this->input->post('user_type');
		
		//****step 3
        $name 	 			= $this->input->post('name');
        $email  			= $this->input->post('email');
        $mailing  			= $this->input->post('mailing');
        $country_residence  = $this->input->post('country_residence');
		//fb login
        $link_redm  		= $this->input->post('link_redm');
        $city  				= $this->input->post('city');
        $bday  				= $this->input->post('bday');
        $gender  			= $this->input->post('gender');
        $terms  			= $this->input->post('terms');
		
		//****step 4
        $image1  			= $this->input->post('thumbnail_urlAdmincp');
        $photo1  			= $this->input->post('image_urlAdmincp');
        $image2 			= $this->input->post('thumbnail_urlAdmincp_new');
        $photo2  			= $this->input->post('image_urlAdmincp_new');
        $user_type_name  	= $this->input->post('user_type_name');
        $country_step4  	= $this->input->post('country_step4');
        $city_step4  		= $this->input->post('city_step4');
        $short_bio  		= $this->input->post('short_bio');
        
		//****step5 
		$url_website  		= $this->input->post('url_left');
        $genre  			= $this->input->post('genre_step5');
        $key_location  		= $this->input->post('key_location');
        $comment_bio  		= $this->input->post('comment');
        $status  			= $this->input->post('status');
        $distributor  		= $this->input->post('distributor');
        $label  			= $this->input->post('label');
        $contact_info 		= $this->input->post('contact_info');
		//---end step---
		
        $oauth_token    = $this->input->post('oauth_token');
        $oauth_provider = $this->input->post('oauth_provider');
        $oauth_uid      = $this->input->post('oauth_uid');


        function get_client_ip_server() {
            $ipaddress = '';
         
            if($_SERVER['REMOTE_ADDR']){
                @$ipaddress = $_SERVER['REMOTE_ADDR'];
            }
            else {
                @$ipaddress = 'UNKNOWN';
            }
         
            return $ipaddress;
        }

        //$ip = "210.245.33.71";
        $ip = get_client_ip_server();
        @$url2 = "http://freegeoip.net/json/" . $ip;
        $location_all = json_decode(file_get_contents($url2), true);
        //print_r($location);
        $location =  $location_all['city'] . ', ' .  $location_all['country_name'] ;
        $country = $location_all['country_name'];
        $city = $location_all['city'];
        // echo '<br/>' . $address_ip;

       // echo 'location: ' . $location;




        switch ($user_type_id) {
           case '1':
           $user_type_id =1;
                break;
            case '2':
                $user_type_id=2;
                break;
            case '3':
                $user_type_id=3;
                break;
            case '4':
                $user_type_id=4;
                break;
            case '5':
                $user_type_id=5;
                break;
            case '6':
                 $user_type_id=6;
                break;
            case '7':
                $user_type_id=7;
                break;
        }

		// Check $is_oauth first
		$oauth_data = array();
		$is_oauth = FALSE;
		//check user is oauth
		if ( ! empty($oauth_uid) && ! empty($oauth_provider) && ! empty($oauth_token)) {
			$check_data = array();
			$check_data ['oauth_provider'] = $oauth_provider;
			$check_data ['oauth_uid'] = $oauth_uid;
			$verify = $this->verifyOAuthToken($check_data, $oauth_token);
			// pr($check_data);
			$oauth_data = $this->model->getDataUserOAuth($oauth_provider, $oauth_uid);
			$social_data = $this->model->getOAuthUserData($oauth_provider, $oauth_uid);
			$is_oauth = $verify;
		} 
		// pr($oauth_data);
		// exit;
			
		$form_check = !empty($email) && !empty($username);
        if($is_oauth || $form_check)
        {
            $user_id = FALSE;
            
            if($is_oauth || $this->model->check_mail_info($email))
            {
				if($is_oauth || $this->model->check_username_info($username)){
					$data = array(
						// 'link_url_redm' => $link_url_redm,
						'url' 			=> $url_of_page,//propage
						
						//**step1
						'username'      => $username,
						'password'      => $password,
						
						//**step2
						'user_type_id'  => $user_type_id,
						
						//**step3
						'mailing'			=> $mailing, 			
						'country_residence'	=> $country_residence, 
						'link_url_redm'		=> $link_redm,
						'city_residence'	=> $city,  				
						'bday'   			=> $bday,  				
						'gender' 			=> $gender,  			
						'terms'				=> $terms,
						
						//**step4
						'image'				=> $image1,
						'thumbnail'			=> $photo1,//hinh goc
						'image_1'			=> $image2,
						'thumbnail_1'		=> $photo2,//hinh goc
						'user_type_name'  	=> $user_type_name,
						'country_step4'  	=> $country_step4,
						'city_step4'  		=> $city_step4, 
						'short_bio'  		=> $short_bio,
						
						//**step5
						'url_website'  		=> $url_website,
						'genre'				=> $genre,
						'key_location'  	=> $key_location,
						'comment_bio'  		=> $comment_bio,
						'status_user'  		=> $status ,
						'distributor'  		=> $distributor,
						'label'  			=> $label,
						'contact_info'  	=> $contact_info,
						);
					if($is_oauth){
						unset($data['username']);
						// pr('$is_oauth - $data');
						// pr($data,1);
					}
				 
					// $data['image'] = '/assets/uploads/images/avatar.png';
					// $data['thumbnail'] = '/assets/uploads/images/avatar.png';
					
					$data['is_fill_data'] = 1;
					// pr($data,1); // TODO - CHECK USER'S INFO
					
					if ( ! empty($oauth_data)) {
						$data ['email'] = $oauth_data ['email'];
						$user_id = $oauth_data ['id'];
						//update user oauth data
						$this->model->updateDataUserOAuth($user_id, $data);
					} else {
						$data ['email'] = $email;
						$user_id = $this->model->insert_info($data);
					}


					if($user_id){
						//Begin check token pro page
						$params = array();
						$params ['user_id']  = $user_id;
						$params ['user_type_id']  = $user_type_id;
						$params ['token']  = $token;
						$params ['email']  = $email;
						$params ['url_of_page']  = $url_of_page;
						$params ['genre']  = $genre;
						// pr($params, 1);
						$this->request_propage_after_signup($params);
						//End check token pro page
						if (!empty($social_data)) {
							// Copy from login_google(): Step 2-1-2-1-2
							$social_id = $social_data ['id'];
							$this->model->updateUserIdInOAuthData($social_id, $user_id);
						}
						$this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
						$this->repu_daily_login();
						echo 1;
					}
				}
				else{
					echo 2;//username exits
				}
            } 
            else 
            {
                echo 0;
            }
        }
        exit();
    }

    public function request_propage_after_signup($params) {
        $user_id      = $params ['user_id'];
        $user_type_id = $params ['user_type_id'];
        $token        = $params ['token'];
        $email        = $params ['email'];
        $url_of_page  = $params ['url_of_page'];
        $genre 		  = $params ['genre'];
        // pr($params);
        $is_match = FALSE;
        if ( ! empty($token) ) {
            //check token
            $token_data = $this->proPage->getProPageDataByToken($token);
            // pr($token_data, 1);
            if ( ! empty($token_data)) {
                $approve_id = $token_data ['id'];
                if ($email == $token_data ['email']) {
                    //update user_id to propage, auto set approved
                    $this->proPage->updateUserApproveProPage($approve_id, $user_id);
                    $this->proPageImport->updateUserApproveProPage($approve_id);
                    $is_match = TRUE;
                } 
            }
        } 
        //request new propage
        if ($is_match === FALSE) {
            if ($user_type_id != 7) { //khac Reader
                $claim_id = $this->proPageClaim->insertClaim($user_id, $email, $url_of_page, $user_type_id, $genre);
            }
        }
    }

    //login username/email function
	
    public function login(){
        $email = $username = $this->input->post('username');
        $password = $this->hashpassword($this->input->post('password')) ;
        $request_headers = $this->input->request_headers();
		
		if(!empty($_POST['username'])) {
			$sql = "Select * from admin_nqt_user_info where username = '" . $_POST["username"] . "' and password = '" . md5($_POST["password"]) . "'";
			$result = $this->db->query($sql);
			$user = $result->result_array();
			if(!empty($user)) {
				$_SESSION["member_id"] = $user[0]['id'];
				// pr($_POST["remember"]);
				if(isset($_POST["remember"]) == 'on') {
					// setcookie ("member_login",$_POST["username"],time()+ (10 * 365 * 24 * 60 * 60));
					// setcookie ("member_password",$_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
					$_SESSION["member_login"] = $_POST["username"];
					$_SESSION["member_password"] = $_POST["password"];
				} else {
					$_SESSION["member_login"] = '';
					$_SESSION["member_password"] = '';
				}
			} 
		}
        // check data login
        $result = $this->model->login_info($username,$password,$email);
        if($result){    
            // check username exits
            $result = $this->model->check_user_info($username,$email);
            $user_id = $result[0]->id;
            $resultlocation = $this->model->load_list_location_user($user_id);
            if($result != false){
                $this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
				$this->repu_daily_login();
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = $request_headers ['Referer'];
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }

    //login google
	private function login_google_client(){
		// Include the google api php libraries
        include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        //$this->load->library('GoogleAPI');
        // Google Project API Credentials
        $clientId = GG_CLIENT_ID;
        $clientSecret = GG_CLIENT_SECRET;
        $redirectUrl = base_url() . 'ajax/login_gg';
        //pr($redirectUrl,1);
        // Google Client Configuration
        $gClient = new Google_Client();

        $gClient->setApplicationName('Login to Redm');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $gClient->setScopes(array(
                'email', 
                'profile', 
                'https://www.googleapis.com/auth/plus.login'
                ));
		
		return $gClient;
	}
	
    public function login_google(){
		$code = $this->input->get('code');
		$google_authenticated_checked = !empty($code);
		// Url format: http://redm.dev/ajax/login_gg?code=4/16jGLQ1PUrQztGNLq5J1QKlFMAtspnb0VW-y8BtR6EE&authuser=2&session_state=383933c61a6a3accced7ccd3eb559fbc2a01e80b..8987&prompt=consent#
		
		// Step 1: Authenticated with Google => redierect to Google's urls
        if (!$google_authenticated_checked) {
			// pr('Step 1',1);
			
			$gClient = $this->login_google_client();
			$google_url = $gClient->createAuthUrl();
			// pr('login_google - $google_url = '.$google_url,1);
            redirect($google_url);
        }
		// Step 2: Get access token and user's profile
		else 
		{
			// pr('Step 2',1);
			
			// pr('login_google - $code = '.$code,1);
            try {
				$gClient = $this->login_google_client();
                //Create google OAuth2 Service
                $google_oauthV2 = new Google_Oauth2Service($gClient);
                $gClient->authenticate();
                // $access_token = $gClient->getAccessToken(); // Not used
                $userProfile = $google_oauthV2->userinfo->get();
				
				// Step 2-1: Get user's profile successful
                if(!empty($userProfile['email']) && !empty($userProfile['family_name']) && !empty($userProfile['given_name'])){
					// pr('Step 2-1',1);
					
					$oauth_provider = 'google';
					$oauth_uid = $userProfile['id'];
					$social_data = $this->model->getOAuthUserData($oauth_provider, $oauth_uid);
					$user_data_social_checked = !empty($social_data);
					// Step 2-1-1 - Just doing social login
					if(!$user_data_social_checked) {
						// pr('Step 2-1-1',1);
						
						// pr($userProfile,1);
						$userData['oauth_provider'] = $oauth_provider;
						$userData['oauth_uid'] = $oauth_uid;
						$userData['username'] = $userProfile['name']; // TODO
						$userData['first_name'] = $userProfile['given_name'];
						$userData['last_name'] = $userProfile['family_name'];
						$userData['email'] = $userProfile['email'];
						$userData['locale'] = $userProfile['locale'];
						$userData['picture_url'] = $userProfile['picture'];
						// Insert or update user data
						$user_id = $this->model->checkUser($userData, TRUE);
						// last_query(1);
						
						//set SESSION 
						$_SESSION['data_signup_gg'] = array(
													'email' 	=> $userData['email'],
													'username'	=> $userData['username'],
						);
					
						// $user_id = $this->model->getUserIdByEmail($userData['email']); // TODO - REDUNDANCY
							$user_id = NULL; //IMPORTANT
							
							$data = array(
								'provider' 	=> $userData['oauth_provider'],
								'email'		=> $userData['email'],
								'uid'		=> $userData['oauth_uid'],
								'user_id'	=> $user_id
							);

							$data_generate = array(
								'oauth_provider' => $userData['oauth_provider'],
								'oauth_uid'		 => $userData['oauth_uid']
							); 
							
							$this->model->insertOAuthData($data);
							
							$queries = array(
								'social' => 'g:' . $userData['oauth_uid'],
								'tk' => $this->generateOAuhToken($data_generate)
							);
							$query_string = http_build_query($queries);
							$url =  PATH_URL . 'en?' . $query_string;
							redirect($url);
					}
					// Step 2-1-2: Sync social data with basic data of user
					else 
					{
						// pr('Step 2-1-2',1);
						
						//OAuth chua sync voi user
						$social_user_id = $social_data ['user_id'];
						$user_data_social_basic_sync_checked = !empty($social_user_id);
						// Step 2-1-2-1: Doing sync social data with basic data 
						if (!$user_data_social_basic_sync_checked) {
							// pr('Step 2-1-2-1',1);
							
							$email = $social_data ['email'];
							$user_id = $this->model->getUserIdByEmail($email);
							// Step 2-1-2-1-1: Doing sync social data with basic data - Part 1
							if ( empty($user_id) ) {
								// pr('Step 2-1-2-1-1',1);
								
								//tuc la email cua oauth chua signup trong table user_info
								$user_id = NULL; //IMPORTANT
								
								$data_generate = array(
									'oauth_provider' 	=> $social_data['provider'],
									'oauth_uid'		=> $social_data['uid']
								); 
								$queries = array(
									'social' => 'g:' . $social_data['uid'],
									'tk' => $this->generateOAuhToken($data_generate)
								);
								$query_string = http_build_query($queries);
								$url =  PATH_URL . 'en?' . $query_string;
								redirect($url);
								
								// Url format: http://redm.dev/en?social=g%3A100757341678377123455&tk=deec7890dd058f0b3fa782e21c405cf1f969b411
								// => Homepage with popup Sign up form
								// => http://redm.dev/ajax/signup
							}
							// Step 2-1-2-1-2: Doing sync social data with basic data - Part 2 (TODO - NOT USED???)
							else 
							{
								// pr('Step 2-1-2-1-2',1);
								
								$social_id = $social_data ['id'];
								$this->model->updateUserIdInOAuthData($social_id, $user_id);
								$this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
								$this->repu_daily_login();
								$url =  PATH_URL . 'en';
								redirect($url);
							}
						}
						// Step 2-1-2-2: Already synced social data with basic data - successful
						else 
						{
							// pr('Step 2-1-2-2',1);
							
							$this->userSession->setSessionWhenLoginSuccessByUserId($social_user_id);
							$this->repu_daily_login();
							$url =  PATH_URL . 'en';
							redirect($url);
						}
					}
    
					$url =  PATH_URL . 'en';
					redirect($url);
                    // //END FIX MISSING FIELDS GOOGLE ACCOUNT
                    //TODO redirect to personal profile, set session token in web ....
                }
				// Step 2-2: Get user's profile failed (error)
				else
				{
					// pr('Step 2-2',1);
					
					$url =  PATH_URL . 'en';
					redirect($url);
                    // redirect(base_url());
                }
            } 
            catch (Exception $e) {
                redirect(base_url());
            }
        } 
    }

    //login facebook function
    public function login_fb(){
        $userData = array();
        $result_url = PATH_URL;
		
		$facebook_authenticated_checked = $this->facebook->is_authenticated();

		// Step 1: Authenticated with Facebook => redierect to Facebook's urls
        if(!$facebook_authenticated_checked)
		{
			// Get login URL
            $result_url = $this->facebook->login_url();
		}
		// Step 2: Already authenticated - Get user's profile
		else 
		{
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');

			// Step 2-1: Get user's profile successful
            // Preparing data for database insertion
            if(!empty($userProfile['email']) && !empty( $userProfile['first_name']) && !empty($userProfile['last_name'])){
                $userData['oauth_provider'] = 'facebook';
                $userData['oauth_uid'] = $userProfile['id'];
                $userData['first_name'] = $userProfile['first_name'];
                $userData['last_name'] = $userProfile['last_name'];
                $userData['email'] = $userProfile['email'];
                $userData['locale'] = $userProfile['locale'];
                $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
                $userData['picture_url'] = $userProfile['picture']['data']['url'];
                // Insert or update user data
                // $check_data = $this->model->checkUser($userData, TRUE);
                $social_data = $this->model->getOAuthUserData($userData['oauth_provider'], $userData['oauth_uid']);
				$user_data_social_checked = !empty($social_data);
				// Step 2-1-1 - Just doing social login
					if(!$user_data_social_checked) {
						$user_id = $this->model->getUserIdByEmail($userData['email']);
						
						// Step 2-1-1-1 : Not yet social logined
						if ( empty($user_id) ) {
							// pr('Step 2-1-1-1',1);
							
							$user_id = NULL; //IMPORTANT
							
							$data = array(
								'provider' 	=> $userData['oauth_provider'],
								'email'		=> $userData['email'],
								'uid'		=> $userData['oauth_uid'],
								'user_id'	=> $user_id
							);

							$data_generate = array(
								'oauth_provider' => $userData['oauth_provider'],
								'oauth_uid'		 => $userData['oauth_uid']
							); 
							
							$this->model->insertOAuthData($data);
							
							$queries = array(
								'social' => 'f:' . $userData['oauth_uid'],
								'tk' => $this->generateOAuhToken($data_generate)
							);
							$query_string = http_build_query($queries);
							$url =  PATH_URL . 'en?' . $query_string;
							redirect($url);
						}
						// Step 2-1-1-2 : Already social logined
						else 
						{
							// pr('Step 2-1-1-2',1);
							
							$this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
							$this->repu_daily_login();
							$url =  PATH_URL . 'en';
							redirect($url);
						}
					}
					// Step 2-1-2: Sync social data with basic data of user
					else 
					{
						//OAuth chua sync voi user
						$social_user_id = $social_data ['user_id'];
						$user_data_social_basic_sync_checked = !empty($social_user_id);
						// Step 2-1-2-1: Doing sync social data with basic data
						if (!$user_data_social_basic_sync_checked) {
							$email = $social_data ['email'];
							$user_id = $this->model->getUserIdByEmail($email);
							// Step 2-1-2-1-1: Doing sync social data with basic data - Part 1
							if ( empty($user_id) ) {
								// pr('Step 2-1-2-1-1',1);
								
								//tuc la email cua oauth chua signup trong table user_info
								$user_id = NULL; //IMPORTANT
								
								$data_generate = array(
									'oauth_provider' 	=> $social_data['provider'],
									'oauth_uid'		=> $social_data['uid']
								); 
								$queries = array(
									'social' => 'f:' . $social_data['uid'],
									'tk' => $this->generateOAuhToken($data_generate)
								);
								$query_string = http_build_query($queries);
								$url =  PATH_URL . 'en?' . $query_string;
								redirect($url);
							}
							// Step 2-1-2-1-2: Doing sync social data with basic data - Part 2 (TODO - NOT USED???)
							else 
							{
								// pr('Step 2-1-2-1-2',1);
								
								$social_id = $social_data ['id'];
								$this->model->updateUserIdInOAuthData($social_id, $user_id);
								$this->userSession->setSessionWhenLoginSuccessByUserId($user_id);
								$this->repu_daily_login();
								$url =  PATH_URL . 'en';
								redirect($url);
							}
						}
						// Step 2-1-2-2: Already synced social data with basic data - successful
						else 
						{
							$this->userSession->setSessionWhenLoginSuccessByUserId($social_user_id);
							$this->repu_daily_login();
							$url =  PATH_URL . 'en';
							redirect($url);
						}
					}
                // Get logout URL
                $data['logoutUrl'] = $this->facebook->logout_url();

            }
			// Step 2-2: Get user's profile failed (error)
			else 
			{
                // redirect(base_url());
				// Do nothing
            }
        }
       
        redirect($result_url);
    }

	public function facebook_get_accesstoken(){pr(23456);
		$result[] = '';
        $userData = array();
        $result_url = PATH_URL;
		
		$access_token = $this->facebook->is_authenticated();pr($access_token);
        if(!empty($access_token)){
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email');
			
            $log_arr = array(
				'location' => __FILE__ ,
				'function' => 'login_facebook',
				'userProfile' => !empty($userProfile) ? $userProfile : '',
				'access_token' => !empty($access_token) ? $access_token : '',
			);
			debug_log_from_config($log_arr);
            if(!empty($userProfile['email']) && !empty( $userProfile['first_name']) && !empty($userProfile['last_name'])){
                $userData['oauth_provider'] = 'facebook';
                $userData['first_name'] = $userProfile['first_name'];
                $userData['last_name'] = $userProfile['last_name'];
                $userData['email'] = $userProfile['email'];
                $email = $userProfile['email'];
                $userData['access_token'] = $access_token;
				$userData['created'] = getNow();
				$userData['ip'] = getIP();
				$userData['session_id'] = $this->session_id_current;
				
                $data['logoutUrl'] = $this->facebook->logout_url();	
				
				if($this->model->insert('login_account_social', $userData)){
					$result['status_db'] = 'success';
					$obj_id = $this->db->insert_id();
					$result['debug_message'] = 'insert_id = '.$obj_id;
					$obj['id_social'] = $obj_id;
					$this->user_data = $obj; // Save
					$_SESSION['is_login'] = true ;
				} else {
					$result['error_message'] = 'DB_FAIL';
				}
				$api_response_data_login = $this->vnw_api_call_login_facebook($access_token);
				
				$json_data = $api_response_data_login;
			    	// echo "Profile: fullname: email: $email -|- firstname: $first_name -|- lastname: $last_name";
			    	// exit;
					
				if(!empty($json_data)){
					if(isset($json_data->access_token)){
						$obj_updated['access_token'] = (isset($json_data->access_token))? $json_data->access_token : $json_data->refresh_token;
						$result['access_token'] = $obj_updated['access_token'];
						$this->model->update('login_account_social', $obj_updated, "id = {$obj['id_social']}");
						$result['message'] = 'success';
						$result['login'] = 'success';
						$_SESSION['access_token'] = $result['access_token'];
						$result_url = PATH_URL.'home/connect_facebook?accessToken='.$result['access_token'];
					}
					else{
						$result['error_message'] = 'fail';
						print'login fail!!!!';
					}
				}
				else{
					$result['error_message'] = 'fail';
					print'login fail!!!!';
				}
            }
        }
		else{
             // Get login URL
            $result_url = $this->facebook->login_url();
			
			$log_arr = array(
				'location' => __FILE__ ,
				'function' => 'login_facebook',
				'result_url' => !empty($result_url) ? $result_url : '',
			);
        }
		if(!empty($result_url)){
			redirect($result_url);
		}
       return $result;
	}
	
    public function generateOAuhToken($check_data) {
        if (
            empty($check_data ['oauth_provider']) || 
            empty($check_data ['oauth_uid'])
        ) {
            return FALSE;
        }
        $data = array();
        $data ['oauth_uid'] = $check_data ['oauth_uid'];
        // $data ['oauth_provider'] = $check_data ['oauth_provider'];
        $algo = 'ripemd160';
        $string = implode('|', $data);
        $secret = $check_data ['oauth_provider'] . '$%$&*Auth';
        $token = hash_hmac($algo, $string, $secret);
        return $token;
    }

    public function verifyOAuthToken($check_data, $hashed_expected) {
        $hashed_value = $this->generateOAuhToken($check_data);
        if ( ! is_string($hashed_value) || !is_string($hashed_expected)) { 
            return FALSE; 
        } 
        $len = strlen($hashed_value); 
        if ($len !== strlen($hashed_expected)) { 
            return FALSE; 
        } 
        $status = 0; 
        for ($i = 0; $i < $len; $i++) { 
            $status |= ord($hashed_value [$i]) ^ ord($hashed_expected [$i]); 
        } 
        return $status === 0; 
    }

    //Logout
    public function logout(){
        $see_data = array(
            'username'          => '',
            'id'                => '',
            'location'          => '',
            'link_url_redm'     => '',
            'country'           => '',
            'city'              => '',
            'url'               => '',
            // 'age'               => '',
            // 'gender'            => '',
            'biiography'        => '',
            'status_genenal'    => '',
            'founder'           => '',
            'shortbio'          => '',
            'marketing'         => '',
            'contact'           => '',
            'distributor'       => '',
            'facebook'          => '',
            'twitter'           => '',
            'bandcamp'          => '',
            'beatport'          => '',
            'soundcloud'        => '',
            'itunes'            => '',
            'mixcloud'          => '',
            );
        $this->facebook->destroy_session();
        $this->session->unset_userdata('userData', $see_data);
        // Remove user data from session
        $this->session->unset_userdata('userData');
        $this->session->unset_userdata('token');
        $this->session->sess_destroy();
        $request_headers = $this->input->request_headers();
        redirect($request_headers ['Referer']);
    }

    public function blog($slug = '',$start=0) {
        if(!$slug){
            $slug = $this->model->get_articles_blog_feature_new();
            foreach ($slug as $value) {
                $slug =  $value->slug;
            }
            if(!$slug){
            redirect(PATH_URL);
            }
            $data['hidden'] = 'hidden';
        } 

        $start = (int)$start;
        $limit = 2;
        $limit_news= '1';
        $request_headers = $this->input->request_headers();
        $lang = $this->lang->default_lang();
        $item_total_list = $this->model->get_item_list();//last_query(1);
        

        $data['type_list'] = $this->model->load_list_user();
        $userData = $this->session->get_userdata();
        if(isset($this->session->userdata['userData'])){
            $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata['userData']['id']);
        }
		$data['item_list'] = '';
        if(!empty($item_total_list)){
            $total_rows = count($item_total_list);
            $item_list = $this->model->get_item_list($start, $limit);//last_query(1);\
			$data['item_list'] = $item_list;
            $current_url = PATH_URL.$lang.'/blog';
            paginate($this,$current_url,$total_rows,$start,$limit);
        }
        $data['news'] = $this->model->get_latest_news($lang,$limit_news);
        $data['news_related'] = $this->model->get_related_news();

        $data['articles_blog'] =  $this->model->get_articles_blog($slug);

        $data['select_blog_view'] = $this->model->select_blog_view($slug);

        foreach ($data['select_blog_view'] as $result){
            $data['select_blog_view'] = $result->view;
        }

        $data['update_blog_view'] = $this->model->update_blog_view($slug,$data['select_blog_view'] );

        $this->template->write('title',$data['news'][0]['title']);
        $this->template->write_view('content','FRONTEND/blog',$data);
        $this->template->render(); 
    }

    function read($slug = ''){

        if($slug){
            $data['data_tag'] =  $slug;
        } else{
            $data['data_tag'] =  '';
       }

        $this->template->write_view('content','FRONTEND/read',$data);
        $this->template->render(); 
    }
	
	function read_country($slug = ''){

        if($slug){
            $data['data_tag'] =  $slug;
        } else{
            $data['data_tag'] =  '';
       }

        $this->template->write_view('content','FRONTEND/read_country',$data);
        $this->template->render(); 
    }
	
	function readTagCountry($slug = ''){
        if($slug){
			 return $this->model->get_articles_all_tagcountry($slug, $country_tags =1);
		}
    }

    function get_articles_all($slug){
        return $this->model->get_articles_all($slug);
    }
	
	function get_articles_all_country($slug){
        return $this->model->get_articles_all_tagcountry($slug, $country_tags =1);
    }

    function get_articles_blog_category($category_id,$articles_id){
        return $this->model->get_articles_blog_category($category_id,$articles_id);
    }

    function get_user_comment($articles_id){
       
        return $this->model->get_user_comment($articles_id);
    }
    
	function get_user_comment_child($articles_id,$id_comment_parent){
       
        return $this->model->get_user_comment_child($articles_id,$id_comment_parent);
    }

    function insert_user_comment(){
        $user_info_id = $this->input->post('user_info_id');
        $id_artcles_item_lang = $this->input->post('id_artcles_item_lang');
        $id_artcles_data = $this->model->get_id_artcles($id_artcles_item_lang);
        $id_artcles = $id_artcles_data[0]->articles_id;
        $comment = $this->input->post('comment');
        $this->model->insert_user_comment($user_info_id,$id_artcles_item_lang,$id_artcles,$comment);
    }

    function insert_user_comment_child(){
        $user_info_id = $this->input->post('user_info_id');
        $id_artcles_item_lang = $this->input->post('id_artcles_item_lang');
		$id_artcles_data = $this->model->get_id_artcles($id_artcles_item_lang);
        $id_artcles = $id_artcles_data[0]->articles_id;
        $id_comment_parent = $this->input->post('id_comment_parent');
        $comment = $this->input->post('comment');
        $this->model->insert_user_comment_child($user_info_id,$id_artcles_item_lang,$id_artcles,$id_comment_parent,$comment);
    }

    function ajax_loadcomment(){
        $articles_id['articles_id'] = $this->input->post('articles_id');
        $this->load->view('FRONTEND/ajax_loadComment', $articles_id);
    }

    function ajax_loadcomment_reply(){
        $articles_id['articles_id'] = $this->input->post('articles_id');
        $articles_id['id_comment_parent'] = $this->input->post('id_comment_parent');
        $this->load->view('FRONTEND/ajax_loadComment_reply', $articles_id);
    }

    public function hashpassword($password) {
        return md5($password);
    }
    
    public function changePassword(){
        $userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
        $pass = $this->model->get_password($user_id);
        $password = $pass[0]->password;
        if(!empty($_POST)){
        $currentpass = $_POST['currentPass'];
        $newpass = $_POST['newPass'];
        $confirmpass = $_POST['confirmPass'];
        $currentPassmd5 = $this->hashpassword($currentpass);
            if($password==$currentPassmd5){
                if($newpass != $currentpass){$is_update = 0;
                    if($newpass == $confirmpass){
                        $item = array();
                        $item['password'] = $this->hashpassword($newpass);
                        $this->db->where('id',$user_id);
                        $is_update = $this->db->update('admin_nqt_user_info',$item);
                        $response['success'] = $is_update ? true : false;
                    }else{
                        print 'wrong-password-confirm';
                        exit;
                    }
                    echo json_encode($response);
                }else{
                    print 'repeated-password';
                    exit;
                }
            }else{
                print 'wrong-password';
                exit;
            }
        }
    }
    
    public function subscription_setting(){
        $userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
        $checkUserLocation = $this->model->checkUserLocation($user_id);
        $item = array();
        
        $local1['user_id'] = $user_id;
        $local2['user_id'] = $user_id;
        $local3['user_id'] = $user_id;
        $local4['user_id'] = $user_id;
        if(!empty($_POST)){
            $item['global_newsletter'] = $this->input->post('global-newsletter');
            $item['local_newsletter'] = $this->input->post('local-newsletter');
            $item['auto_location'] = $this->input->post('auto-location');
            $item['forum_threads'] = $this->input->post('forum-threads');
            $item['private_messages'] = $this->input->post('private-messages');
            $item['friend_request_notify'] = $this->input->post('friend-request-notify');
            $item['private_messages_notify'] = $this->input->post('private-messages-notify');
            
            $local1['local'] = $this->input->post('locationAdmincp');
            $local2['local'] = $this->input->post('locationAdmincp1');
            $local3['local'] = $this->input->post('locationAdmincp2');
            $local4['local'] = $this->input->post('locationAdmincp3');
            
            $local1['country'] = $this->input->post('countryAdmincp');
            $local2['country'] = $this->input->post('countryAdmincp1');
            $local3['country'] = $this->input->post('countryAdmincp2');
            $local4['country'] = $this->input->post('countryAdmincp3');
            
            $local1['cities'] = $this->input->post('citiesAdmincp');
            $local2['cities'] = $this->input->post('citiesAdmincp1');
            $local3['cities'] = $this->input->post('citiesAdmincp2');
            $local4['cities'] = $this->input->post('citiesAdmincp3');
			
			$local = array(
				$local1,
				$local2,
				$local3,
				$local4
			);
            if($user_id){
				$this->db->where('id',$user_id);
				$this->model->update_info($item);
                if($checkUserLocation == true){
					for($i = 0; $i<4; $i++){
						$this->model->insert_local($local[$i]);
					}
                }
                else{
                    $this->db->where('user_id', $user_id);
					$this->model->delete_local();
                    $checkUserLocation = $this->model->checkUserLocation($user_id);
                    if($checkUserLocation == true){
                       for($i = 0; $i<4; $i++){
							$this->model->insert_local($local[$i]);
						}
                    }
                }           
            }
                $result = $this->model->load_list_user_info($user_id);
                if($this->model->get_id($user_id)){
                    $data['userDataLocation'] = array(
                            'id'            => $user_id,
                            'username'      => $result[0]->username,
                            'global_newsletter' => $item['global_newsletter'],                       
                            'local_newsletter' => $item['local_newsletter'],                       
                            'auto_location' => $item['auto_location'],                       
                            'forum_threads' => $item['forum_threads'],                       
                            'private_messages' => $item['private_messages'],                       
                            'friend_request_notify' => $item['friend_request_notify'],                       
                            'private_messages_notify' => $item['private_messages_notify'],                       
                            'local1' => $local1['local'],                       
                            'local2' => $local2['local'],                       
                            'local3' => $local3['local'],                       
                            'local4' => $local4['local'],                       
                    );
                    $this->session->set_userdata('userData',$data['userDataLocation']);
                }
            $debug = false;
            if($debug){
                echo $this->db->last_query();
                exit();
            }
        
        $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }
    
    public function update_user(){
        
        $id = $this->input->post('id');
        $url = $this->input->post('url');
        $location       = $this->input->post('location');
        $country        = $this->input->post('country');
        $city           = $this->input->post('city');
        $link_url_redm  = $this->input->post('link_url_redm');  
        $age            = $this->input->post('age');
        $gender         = $this->input->post('gender'); 
        $contact        = $this->input->post('contact');
        $biography      = $this->input->post('biography');
        $status_genenal = $this->input->post('status_genenal');
        $founder        = $this->input->post('founder');
        $shortbio       = $this->input->post('shortbio');
        $marketing      = $this->input->post('marketing');
        $contact        = $this->input->post('contact');
        $distributor    = $this->input->post('distributor');
        $facebook       = $this->input->post('facebook');
        $twitter        = $this->input->post('twitter');
        $bandcamp       = $this->input->post('bandcamp');
        $soundcloud     = $this->input->post('soundcloud');
        $mixcloud       = $this->input->post('mixcloud');
        $itunes         = $this->input->post('itunes');
        $beatport       = $this->input->post('beatport');
        $_thumbnail_url = $_image_url = '';

        $result = $this->model->load_list_user_info($id);
        if($this->model->get_id($id)){
        
             $data = array(
                        'location'      => $location,
                        'country'       => $country,
                        'city'          => $city,
                        'gender'        => $gender,
                        'age'           => $age,
                        'url'           => $url,
                        'biiography'    => $biography,
                        'status_genenal'=> $status_genenal,
                        'founder'       => $founder,
                        'shortbio'      => $shortbio,
                        'marketing'     => $marketing,
                        'contact'       => $contact,
                        'distributor'   => $distributor,
                        'facebook'      => $facebook,
                        'twitter'       => $twitter,
                        'bandcamp'      => $bandcamp,
                        'soundcloud'    => $soundcloud,
                        'mixcloud'      => $mixcloud,
                        'itunes'        => $itunes,
                        'beatport'      => $beatport,
                        // 'image' => $_image_url,
                        // 'thumbnail' => $_thumbnail_url
            );

            if( ! empty($_POST['thumbnail_urlAdmincp'])) {
                    $pre_url = $_POST['thumbnail_urlAdmincp'];
                    $_thumbnail_url = move_file_from_url('thumb_avatar', $pre_url, TRUE);
                    $data['thumbnail'] = $_thumbnail_url;
            } 
            if( ! empty($_POST['image_urlAdmincp']) ) {
                    $pre_url = $_POST['image_urlAdmincp'];
                    $_image_url = move_file_from_url('image', $pre_url, FALSE);
                    $data['image'] = $_image_url;
            }
			if( ! empty($_POST['thumbnail_urlAdmincp_new'])) {
                    $pre_url = $_POST['thumbnail_urlAdmincp_new'];
                    $_thumbnail_url_new = move_file_from_url('thumb_avatar', $pre_url, TRUE);
                    $data['thumbnail_1'] = $_thumbnail_url_new;
            } 
            if( ! empty($_POST['image_urlAdmincp_new']) ) {
                    $pre_url = $_POST['image_urlAdmincp_new'];
                    $_image_url_new = move_file_from_url('image', $pre_url, FALSE);
                    $data['image_1'] = $_image_url_new;
            }

            $test['profile'] = $this->model->profile_user($link_url_redm);

            foreach ($test['profile'] as $result) {
                 $test['id_newfeed'] = $result->id;
            }

            if(!isset($test['id_newfeed'])){
                 $data['link_url_redm'] =  $this->model->link_url_redm($link_url_redm);
            }



            $this->db->where('id',$id);
             if($this->model->update_info($data)){
             }
             // FOR DEBUG
             $debug = false;
            if($debug){
            echo $this->db->last_query();
            exit();
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
        echo json_encode($_data); exit;
    }
    
	public function insert_com(){
        $comment = $this->input->post('comment');
    }

    public function for_got_password(){
      if(!empty($_POST)){
        $data = array();
        $email = $this->input->post('email');
        $checkmail = $this->model->check_mail_info($email);
        if($checkmail == false){
            $newPass = generate_random_string(8);
            $newPassMD5 = $this->hashpassword($newPass);
            $subject = 'Send New Password';
            $body = 'NEW PASSWORD:'.$newPass;
            $result = ses_send_mail($subject,$body,$email);
            $response['success'] = $result ? true : false;
            if($response['success'] == true){
                $data['password'] = $newPassMD5;
                $this->db->where('email', $email);
                $this->db->update('admin_nqt_user_info',$data);
            }
            $_data ['status'] = 1;
            $_data ['message'] = 'success';
            $_data ['rdt'] = base_url();
        }
        else{
            $_data ['status'] = 0;
            $_data ['message'] = '....';
        }
      }
      echo json_encode($_data); exit;
    }
    
	public function profile($slug = ''){  
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }

        if( !empty($slug)) {
             redirect(PATH_URL);
        } 
        $data['profile_user'] = $this->model->profile_user($slug);
        foreach ($data['profile_user'] as $result) {
             $data['id_newfeed'] = $result->id;
             $name_profile = $result->username;
        }

        if(!empty($data['id_newfeed'])){
          redirect(PATH_URL); 
        } 

        $data['profile'] = $this->model->profile($data['id_newfeed']);
        $this->template->write('title','Profile - '. $name_profile);
        $this->template->write_view('content','FRONTEND/profile',$data);
        $this->template->render();
        
    }
	
	public function proPage($slug = ''){ 
		$proPages = $this->model->getDataProPage($slug);
		
		foreach($proPages as $key=>$val){
			$user_id[$key] = $val->user_id;
		}
		
		$user_id = $user_id[0];
		$data_info = $this->model->load_list_user_info_pro($user_id);
		$data = array(
			'user_info'=>$data_info
		);
        $this->template->write('title','Pro Page');
        $this->template->write_view('content','FRONTEND/proPage',$data);
        $this->template->render();
    }

    public function get_comment_newfeed($id_newfeed){
       return $this->model->get_comment_newfeed($id_newfeed);
    }

    public function get_comment_newfeed_reply($id_newfeed,$id_comment_parent){
       return $this->model->get_comment_newfeed_reply($id_newfeed,$id_comment_parent);
    }

    public function insert_comment_newfeed(){
        $id_user_info = $this->input->post('id_user_info');
        $id_newfeed = $this->input->post('id_newfeed');
        $comment = $this->input->post('comment');
        $this->model->insert_comment_newfeed($id_user_info,$id_newfeed,$comment);
    }

    public function insert_comment_newfeed_reply(){
        $id_user_info = $this->input->post('id_user_info');
        $id_newfeed = $this->input->post('id_newfeed');
        $id_comment_parent = $this->input->post('id_comment_parent');
        $comment = $this->input->post('comment');
        $this->model->insert_comment_newfeed_reply($id_user_info,$id_newfeed,$id_comment_parent,$comment);

    }

    public function ajax_loadcomment_newfeed(){
        $articles_id['id_artcles_newfeed'] = $this->input->post('id_artcles_newfeed');
        $this->load->view('FRONTEND/ajax_loadComment_newfeed', $articles_id);
    }

    public function ajax_loadcomment_newfeed_reply(){
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata['userData']['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata['userData']['id']);
        }

        $articles_id['id_artcles_newfeed'] = $this->input->post('id_artcles_newfeed');
        $articles_id['id_comment_parent'] = $this->input->post('id_comment_parent');
        $this->load->view('FRONTEND/ajax_loadComment_newfeed_reply', $articles_id);
    }

    public function up_newfeed(){
        $id_user_info = $this->input->post('id_user_info');
        $id_pro = $this->input->post('id_pro');
        $content = $this->input->post('content');
        $this->model->up_newfeed($id_user_info,$id_pro,$content);
		redirect(PATH_URL);
    }

    public function ajax_loadnewfeed($id){
        if(isset($this->session->userdata['userData'])){
         $data['userDataSetting1'] = $this->model->load_list_user_info($this->session->userdata('userData')['id']);
         $data['userDatalocation'] = $this->model->load_list_location_user($this->session->userdata('userData')['id']);
        }

        $data['profile'] = $this->model->profile($id);

        $this->load->view('FRONTEND/ajax_loadNewfeed',$data);
    }

    public function visit_analytic(){
        $this->load->library('GoogleAPI');
    }

    public function get_script(){
       return $this->model->get_script();
    }
 
	public function repu_daily_login(){
		$userData = $this->session->get_userdata();
        $user_id = $userData['userData']['id'];
		$checkUser = $this->model->checkUserIdDailyLogin($user_id);
		$data_repu = $this->model->get_time_last_login($user_id);
		$date_current_login = date('Y-m-d 00:00:00');
		
		if($checkUser == true){
			$data['user_id'] = $user_id;
			$data['date_login_last'] = $date_current_login;
			$data['daily_login_value'] = 0;
			$this->db->insert('admin_nqt_reputation',$data);
		}
		else{
			if(!empty($data_repu)){
				$daily_value = $data_repu[0]->daily_login_value;
				$date_last_login_old = strtotime($data_repu[0]->date_login_last);
				$date_last_login_new = strtotime(date('Y-m-d 00:00:00'));
				$distance_date = (int)($date_last_login_new - $date_last_login_old)/86400;
				
				if($distance_date === 1){
					$item['daily_login_value']  = $daily_value + 1;
					$item['date_login_last']  = $date_current_login;
					$this->db->where('user_id',$user_id);
					$this->db->update('admin_nqt_reputation',$item);
				}
				else{
					if($distance_date > 1){
						$item['daily_login_value'] = 0 ;
						$item['date_login_last']  = $date_current_login;
						$this->db->where('user_id',$user_id);
						$this->db->update('admin_nqt_reputation',$item);
					}
				}
			}
		}
	}

    public function get_tag_list(){
       return $this->model->get_tag_list();
    }
	
    public function get_list_countries(){
       return $this->model->get_list_countries();
    }
	
    public function get_list_genre(){
       return $this->model->get_list_genre();
    }
  
	public function load_addevent(){
		$data = '';
		if(!empty($_SESSION['datalogin'])){
			$data['listpage'] = $_SESSION['datalogin'];
		}
		$this->load->view('FRONTEND/add_event', $data);
	}
}