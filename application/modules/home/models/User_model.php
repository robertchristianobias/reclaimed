<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {

	private $module = 'home';
    private $token;
	private $table = 'admin_nqt_user_info';

    function __construct(){
        parent::__construct();
        //$this->lang_code = $this->lang->default_lang();
        $this->load->database();
        $this ->table = 'admin_nqt_user_info';
        $this->tableName = 'admin_nqt_user_info';
        $this->primaryKey = 'id';
    }

    public function register() {

    }

    public function login() {
        
    }

    public function getUserDataByUsername($username) {
        $result = FALSE;
        $this->db->select('*');
        $this->db->where('username', $username);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
    }

    public function getUserDataByUserId($userId) {
        $result = FALSE;
        $this->db->select('*');
        $this->db->where('id', (int)$userId);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
    }

    public function getUserDataByUserBitId($userBitId) {
        $result = FALSE;
        $this->db->select('*');
        $this->db->where('user_bit_id', $userBitId);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
    }
	
	public function getUserSlug($userId) {
        $result = FALSE;
        $this->db->select('admin_nqt_user_info.link_url_redm');
        $this->db->where('id', $userId);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
    }

    public function getUserDataByProfileSlug($slug) {
        $result = FALSE;
        $this->db->select('*');
        $this->db->where('link_url_redm', $slug);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
    }

    public function updateUserBitId() {
        $result = array();
        $this->db->select('*');
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->result_array()) ) {
            $users = $query->result_array();
            foreach($users as $key => $user) {
                // pr($user, 1);
                $userBitId = $this->generateUserBitId($user ['id'], $user ['created']);
                $data = array('user_bit_id' => $userBitId);
                $this->db->where('id', (int)$user ['id']);
                $this->db->update('admin_nqt_user_info', $data);
                $result[] = $userBitId;
            }
        }
        pr($result, 1);
    }
	
	public function insertUserBitId($userId, $userBitId) {
        $result = array();
		$data = array('user_bit_id' => $userBitId);
		
        $this->db->select('*');
		$this->db->where('id',$userId);
        $query = $this->db->update('admin_nqt_user_info',$data);
    }

    public function generateUserBitId($userId, $created = FALSE) {
        if(empty($created)) {
            $time = explode(' ', microtime());
            $miliSecond = $time[1] . substr(round($time[0], 10), 2);
        } else {
            $miliSecond = strtotime($created) . rand(0, 100);
        }
        $lenMiliSecond = strlen($miliSecond);
        $ourEpoch = strtotime('2017-03-01 00:00:00');
        $intervalTimestamp =  abs($miliSecond - str_pad($ourEpoch, $lenMiliSecond, "0"));
        $id = ($intervalTimestamp << 13);
        $id |= ($userId << 8);
        return $id;
    }

}