<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends MY_Model {

	private $module = 'home';
    private $token;
	private $table = 'admin_nqt_user_info';

    function __construct(){
        parent::__construct();
        //$this->lang_code = $this->lang->default_lang();
        $this->load->database();
        $this ->table = 'admin_nqt_user_info';
        $this->tableName = 'admin_nqt_user_info';
        $this->primaryKey = 'id';
    }

	public function get_events_details($year, $month, $slug){
        $query = $this->db->query("
            SELECT admin_nqt_events.name_cn, admin_nqt_events.slug_cn, admin_nqt_events.name_en, admin_nqt_events.slug_en, admin_nqt_events.thumbnail, admin_nqt_events.image, admin_nqt_events.tagsGenre, admin_nqt_events.tagsArtist, admin_nqt_events.location, admin_nqt_events.sort, admin_nqt_events.date_time_start, admin_nqt_events.date_time_end, admin_nqt_events.tickets, admin_nqt_events_lang.description 
            FROM admin_nqt_events
            INNER JOIN admin_nqt_events_lang
            ON admin_nqt_events.id = admin_nqt_events_lang.id
            WHERE admin_nqt_events_lang.slug = '".$slug."' and EXTRACT(YEAR FROM date_time_start) = '".$year."' and EXTRACT(MONTH FROM date_time_start) = '".$month."'and admin_nqt_events.status = 1 and admin_nqt_events_lang.lang_code = '".$this->lang->default_lang()."' and admin_nqt_events.is_delete = 0 order by admin_nqt_events.id desc
        ");
        return $query->result();
    }

    
    public function check_mail_info($email){
        $result = false;
        
        $this->db->select("id");
        $this->db->where('email', $email);
        $query = $this->db->get("admin_nqt_user_info");
        $query_result = $query->result_array();
        if(empty($query_result)){
            $result = true;
        }
        return $result;
    }
// Section Remember me
    public function storeTokenForUser($user_id, $token,$created_date, $expired_time){
        $data = array(
            'user_id' => $user_id,
            'expired_time' => $expired_time,
            'user_token' =>  $token,
            'created' => $created_date
            
            );
        $this->db->insert('user_session',$data);

    }

    public function deleteUserToken($user_id,$user_token){
        $this->db->where('user_id',$user_id);
        $this->db->where('user_token',$user_token);
        $this->db->delete('user_session');
    }
// End section  	
	public function check_username_info($username){
        $result = false;
        
        $this->db->select("id");
        $this->db->where('username', $username);
        $query = $this->db->get("admin_nqt_user_info");
        $query_result = $query->result_array();
        if(empty($query_result)){
            $result = true;
        }
        return $result;
    }

	// TODO: Check existence
	public function social_login_data_to_db_get_from($user_profile){
		$userData = array();
		$userData['oauth_provider'] = $user_profile['oauth_provider'];
		$userData['oauth_uid'] = $user_profile['oauth_uid'];
		$userData['username'] = $user_profile['name']; // TODO
		$userData['first_name'] = $user_profile['first_name'];
		$userData['last_name'] = $user_profile['last_name'];
		$userData['email'] = $user_profile['email'];
		$userData['locale'] = $user_profile['locale'];
		$userData['picture_url'] = $user_profile['picture_url'];
		
		return $userData;
	}
	
	public function getOAuthUserData($provider, $uid){
        $this->db->select("*");
        $this->db->where('provider', $provider);
        $this->db->where('uid', $uid);
        $query = $this->db->get("oauth_account_user");
        if( empty($query->row_array())){
			return array();
		}
        return $query->row_array();
    }

	public function getUserIdByEmail($email){
        $this->db->select("id");
        $this->db->where('email', $email);
        $query = $this->db->get("admin_nqt_user_info");
	    if( empty ($query->row_array()['id'])){
		   return NULL;
	    }
        return $query->row_array()['id'];
    }
	
	public function get_username_by_id($id){
        $this->db->select("*");
        $this->db->where('id', $id);
        $query = $this->db->get("admin_nqt_user_info");
        return $query->row_array();
    }

    public function load_list_user(){
      
         $query_list = $this->db->query("
            SELECT * FROM user_type
            ORDER BY other asc
            ");
        return $query_list->result();
    }

    public function load_list_user_info($id){
        $this->db->select("*");
        $this->db->where('id',$id);
        $query_list = $this->db->get("admin_nqt_user_info");
        return $query_list->result();
    }
	
	public function load_list_user_info_pro($id){
        $this->db->select('admin_nqt_user_info.*,user_type.type');
		$this->db->from('admin_nqt_user_info');
		$this->db->where('admin_nqt_user_info.id',$id);
		$this->db->join('user_type','user_type.id = admin_nqt_user_info.user_type_id','left');
        
        $query_list = $this->db->get();
        return $query_list->row();
    }
	
	public function load_list_location_user($id){
        $this->db->select('*');
        $this->db->where('user_id',$id);
        $query_list = $this->db->get('local_receive_newsletters');
		if(!empty($query_list)){
			return $query_list->result();
		}
		return NULL;
    }
	
	public function get_password($id){
        $this->db->select("password");
		$this->db->where('id',$id);
        $query_list = $this->db->get("admin_nqt_user_info");
        return $query_list->result();
    }

    public function get_id($id){
        $this->db->select("id");
        $this->db->where('id',$id);
        $query_list = $this->db->get("admin_nqt_user_info");
        return $query_list->result();
    }
	
	public function get_id_artcles($id){
        $this->db->select('articles_id');
        $this->db->where('id',$id);
        $query_list = $this->db->get("admin_nqt_articles_item_lang");
        return $query_list->result();
    }

    public function login_info($username,$password,$email){
       $this->db->select('username','password','email');
       $this->db->from('admin_nqt_user_info');
       $this->db->where("(email ='$email' or username ='$username') and password ='$password'");
       $query = $this->db->get();
       if($query->num_rows() == 1){
        return true;
       }
       return false;
    }

    public function check_user_info($username,$email){
        $condition = "username =" . "'" . $username . "' or email ="."'".$email."'";
        $this->db->select('*');
        $this->db->from('admin_nqt_user_info');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
        return $query->result();
        } else {
        return false;
        }
    }

    public function updateDataUserOAuth($user_id, $data_update) {
        if (is_array($data_update) & ! empty($data_update)) {
            $this->db->where('id', intval($user_id));
            $this->db->update($this->table, $data_update);
        }
    }

    public function getDataUserOAuth($oauth_provider, $oauth_uid) {
        $result = FALSE;
        $this->db->select('id, is_fill_data, email, oauth_provider, oauth_uid');
        $this->db->from($this->table);
        $this->db->where(array('oauth_provider' => $oauth_provider,'oauth_uid' => $oauth_uid));
        $this->db->limit(1);
        $query = $this->db->get();
        if ( ! empty($query->row_array())) {
            $result = $query->row_array();
        }
        return $result;
    }
    
    public function checkUser($data = array(), $return_is_fill = FALSE){
        $result = FALSE;
        $this->db->select('id, is_fill_data, email, oauth_provider, oauth_uid');
        $this->db->from($this->table);
        $this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        if($prevCheck > 0){
            $prevResult = $prevQuery->row_array();
            $update = $this->db->update($this->table,$data,array('id'=>$prevResult['id']));
            $userID         = $prevResult['id'];
            $is_fill_data   = $prevResult ['is_fill_data'];
            $email          = $prevResult ['email'];
            $oauth_provider = $prevResult ['oauth_provider'];
            $oauth_uid      = $prevResult ['oauth_uid'];
        }else{
            $data['created'] = date("Y-m-d H:i:s");
            $data['is_fill_data'] = 0;
            $insert = $this->db->insert($this->table,$data);
            $userID         = $this->db->insert_id();
            $is_fill_data   = $data ['is_fill_data'];
            $email          = $data ['email'];
            $oauth_provider = $data ['oauth_provider'];
            $oauth_uid      = $data ['oauth_uid'];
        }
        if ($return_is_fill === TRUE) {
            $result ['id']        = $userID ? $userID : FALSE;
            $result ['is_fill_data']   = $is_fill_data;
            $result ['email']          = $email;
            $result ['oauth_provider'] = $oauth_provider;
            $result ['oauth_uid']      = $oauth_uid;
        } else {
            $result = $userID ? $userID : FALSE;
        }
        return $result;
    }
   
    public function insert_info($data){
        $this->db->insert("admin_nqt_user_info", $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    } 
	
	public function insertOAuthData($data){
        $this->db->insert("oauth_account_user", $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    }
	
	public function updateUserIdInOAuthData($social_id, $user_id){
		$data = array(
			'user_id' => $user_id,
		);
		$this->db->where('id',$social_id);
        return $this->db->update("oauth_account_user", $data);
    }
	
	public function insert_local($data){
        return $this->insert("local_receive_newsletters", $data);
    }
	
	public function delete_local($where=""){
		if($where != "")
        {
            $this->db->where($where);
        }
        return $this->delete("local_receive_newsletters");
    }
	
    public function update_info($data, $where=""){
       if($where != "")
        {
            $this->db->where($where);
        }
        return $this->db->update("admin_nqt_user_info", $data);
    }

	
	public function checkUserLocation($user_id){
		$result = false;
        
        $this->db->select("user_id");
        $this->db->where('user_id', $user_id);
        $query = $this->db->get("local_receive_newsletters");
        $query_result = $query->result_array();
        if(empty($query_result)){
            $result = true;
        }
        return $result;
	}
    
    public function load_comment(){
        $this->db->select("admin_nqt_comment_info.comment,admin_nqt_comment_info.user_info_id,admin_nqt_user_info.username,admin_nqt_comment_info.created,admin_nqt_user_info.created as since,admin_nqt_user_info.thumbnail");
        $this->db->from("admin_nqt_comment_info");
        $this->db->join('admin_nqt_user_info',"admin_nqt_comment_info.user_info_id= admin_nqt_user_info.id",'left');
        
        $query = $this->db->get();
        return $query->result_array (); 
        // FOR DEBUG
        $debug = false;
        if($debug){
            echo $this->db->last_query();
            exit();
        }   
    }
    public function get_item_list($start=-1, $limit=-1){
        $result = '';
        $select = '1';
        $limit_statement = '';
        $where = 'admin_nqt_user_info.id = admin_nqt_comment_info.user_info_id';
        if($start > -1 && $limit > -1){
            $select = '*';
            $limit_statement = "LIMIT {$start}, {$limit}";
           
        }
        $query = "
                SELECT {$select}
                FROM admin_nqt_comment_info
                JOIN admin_nqt_user_info
                WHERE {$where}
                {$limit_statement}
            ";
        $query = $this->db->query($query);
        $result = $query->result_array();// pr($item_list,1);
        return $result;
    }
    public function get_list_menu(){
        $query = $this->db->query("
            SELECT * FROM admincp_menu_lang
            WHERE status = '1' and lang_code = '".$this->lang->default_lang()."'
            LIMIT 7
            ");
        return $query->result(); 

    }
    public function get_background(){
        $query = $this->db->query("
            SELECT * FROM admincp_background
            WHERE status = '1'
            ORDER BY created desc
            LIMIT 1
            ");
        return $query->result(); 
    }
	
	public function logo_image_model(){
        $query = $this->db->query("
            SELECT * FROM admin_nqt_logo
            WHERE status = '1'
            ORDER BY created desc
            LIMIT 1
            ");
        return $query->result(); 
    }
	
	
    public function get_latest_news($lang_code,$limit_news){
        $this->db->select("*");
        $this->db->from("admin_nqt_articles_item_lang");
        $this->db->where('lang_code',$lang_code);
        $this->db->order_by('created',"desc");
        $this->db->limit($limit_news);
        $query = $this->db->get();
        return $query->result_array(); 
    }
    public function get_related_news(){
        $where = "lang_code = 'en' and admin_nqt_articles_item_lang.category_id = '1'";
        $this->db->select("admin_nqt_articles_item_lang.*");
        $this->db->from("admin_nqt_articles_item_lang");
        $this->db->join('admin_nqt_category_articles',"admin_nqt_category_articles.id = admin_nqt_articles_item_lang.category_id",'left');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array(); 
    }
    public function get_header_ticker(){
        $this->db->select("contents, link");
        $this->db->where("status", "1" );
        $this->db->order_by("id desc");
        //$this->db->limit(1,0);
        $query = $this->db->get("header_ticker");
        return $query->result(); 
    } 
	
	public function get_data_meta_homepage(){
        $this->db->select("image, meta_title, meta_keyword, meta_description");
        $this->db->like("name", "homepage");
        $this->db->limit(1,0);
        $query = $this->db->get("admin_nqt_pages");
		if(empty($query->row_array())){
			return array();
		}
        return $query->row_array();
    } 
	
	public function getDataProPage($slug){
        $this->db->select('*');
        $this->db->where('url_of_page', $slug );
        $query = $this->db->get('pro_page');
        return $query->result(); 
    }

    public function get_articles_featured(){
		$current_datetime = date('Y-m-d H:i:s');
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item.featured_from, admin_nqt_articles_item.featured_to, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description, admin_nqt_articles_item_lang.tags
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item.featured_laster = 1 
			and
			(( admin_nqt_articles_item.featured_from <= '".$current_datetime."' 
			and admin_nqt_articles_item.featured_to >= '".$current_datetime."')
			or
			(admin_nqt_articles_item.featured_from = '0000-00-00 00:00:00'))
			and admin_nqt_articles_item.status = 1 
			and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' 
			and admin_nqt_articles_item.is_delete = 0 order by admin_nqt_articles_item.id desc
			");
        return $query->result();   
    }
   
    // public function get_articles_latest_NOT_USED(){
        // $query=$this->db->query("
        // SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description, admin_nqt_articles_item_lang.tags
        // FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        // ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        // where admin_nqt_articles_item.featured_global = 1
             // and admin_nqt_articles_item.status = 1 
             // and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() .
             // "' and admin_nqt_articles_item.is_delete = 0 order by admin_nqt_articles_item.id desc limit 9");
        // return $query->result(); 
    // }
	public function get_articles_latest($limit = 0, $page = -1){
		$result = false;
		$main_table = 'admin_nqt_articles_item';
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description, admin_nqt_articles_item_lang.tags");
        }
		
		$this->db->from($main_table);
		$this->db->join('admin_nqt_articles_item_lang',"{$main_table}.id = admin_nqt_articles_item_lang.articles_id", 'inner');
		
		if(!$is_count_total_rows){
			// Limit
			$this->db->limit($limit,$page);
			
			// Order
			$order_by = 'id';
			$order_by = "{$main_table}.{$order_by}";
			$order_direction = 'desc';
			$this->db->order_by($order_by, $order_direction);
		}
		
		/*Begin: Condition*/
		// Begin - Search condition
		$this->db->where("{$main_table}.featured_global", 1);
		$this->db->where("{$main_table}.status", 1);
		$this->db->where("{$main_table}.is_delete", 0);
		$this->db->where("admin_nqt_articles_item_lang.lang_code", $this->lang->default_lang());
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
	}
	
	public function event_list($limit = 0, $page = -1){
		$result = false;
		$main_table = 'admin_nqt_events';
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
           $this->db->select("{$main_table}.*, admin_nqt_events_lang.name, admin_nqt_events_lang.description, admin_nqt_events_lang.slug");
        }
		
		$this->db->from($main_table);
		$this->db->join('admin_nqt_events_lang',"{$main_table}.id = admin_nqt_events_lang.event_id", 'inner');
		
		if(!$is_count_total_rows){
			// Limit
			$this->db->limit($limit,$page);
			
			// Order
			$order_by = 'id';
			$order_by = "{$main_table}.{$order_by}";
			$order_direction = 'desc';
			$this->db->order_by($order_by, $order_direction);
		}
		
		/*Begin: Condition*/
		// Begin - Search condition
		$this->db->where("{$main_table}.status", 1);
		$this->db->where("{$main_table}.is_delete", 0);
		$this->db->where("admin_nqt_events_lang.lang_code", $this->lang->default_lang());
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
    }
	
    public function get_articles_all($tag = "", $status = ""){
		
		if($status==1){
			$query=$this->db->query("
         SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where 
             admin_nqt_articles_item.status = 1 
             and admin_nqt_articles_item.featured_country = 1 
             and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() .
             "' and admin_nqt_articles_item.is_delete = 0 and admin_nqt_articles_item.tags like '%". $tag ."%' order by admin_nqt_articles_item.id desc");
		}
		else{
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where 
             admin_nqt_articles_item.status = 1 
             and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() .
             "' and admin_nqt_articles_item.is_delete = 0 and admin_nqt_articles_item.tags like '%" . $tag ."%' order by admin_nqt_articles_item.id desc");
		}
        return $query->result(); 
    }
	
	public function get_articles_all_tagcountry($tag = "", $status = ""){
		
		if($status==1){
			$query=$this->db->query("
         SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where 
             admin_nqt_articles_item.status = 1 
             and admin_nqt_articles_item.featured_country = 1 
             and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() .
             "' and admin_nqt_articles_item.is_delete = 0 and admin_nqt_articles_item.location like '%". $tag ."%' order by admin_nqt_articles_item.id desc");
		}
		else{
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where 
             admin_nqt_articles_item.status = 1 
             and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() .
             "' and admin_nqt_articles_item.is_delete = 0 and admin_nqt_articles_item.location like '%" . $tag ."%' order by admin_nqt_articles_item.id desc");
		}
        return $query->result(); 
    }
    


	public function get_articles_pro_pages(){
        
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.thumbnail, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item.featured_laster = 1 and admin_nqt_articles_item.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' and admin_nqt_articles_item.is_delete = 0 
             order by admin_nqt_articles_item.id desc");
        return $query->result(); 
    }

    public function get_articles_country($country){
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item.featured_laster = 0 and admin_nqt_articles_item.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' and admin_nqt_articles_item.is_delete = 0 and admin_nqt_articles_item.country LIKE '". $country ."' order by admin_nqt_articles_item.id desc limit 3");
        return $query->result(); 
    }

    public function get_articles_blog($slug){
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image,admin_nqt_articles_item.image_blog, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description, admin_nqt_articles_item_lang.meta_title, admin_nqt_articles_item_lang.meta_description, admin_nqt_articles_item_lang.meta_keyword, admin_nqt_articles_item_lang.content, admin_nqt_articles_item_lang.id, admin_nqt_articles_item_lang.category_id, admin_nqt_articles_item_lang.tags
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item_lang.slug = '".$slug."' and admin_nqt_articles_item.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' and admin_nqt_articles_item.is_delete = 0 order by admin_nqt_articles_item.id desc");
        return $query->result(); 
    }

    // feature new 
    public function get_articles_blog_feature_new(){
        $query = $this->db->query("
        SELECT admin_nqt_articles_item_lang.id, admin_nqt_articles_item_lang.slug
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item_lang.status = 1 
        and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."'
        and admin_nqt_articles_item.featured = 1 and admin_nqt_articles_item.is_delete = 0 order by admin_nqt_articles_item.id desc limit 1
        ");
        return $query->result();
    }

    public function get_articles_blog_category($category_id,$articles_id){
        $query=$this->db->query("
        SELECT admin_nqt_articles_item.slug_en, admin_nqt_articles_item.slug_cn, admin_nqt_articles_item.image, admin_nqt_articles_item_lang.title, admin_nqt_articles_item_lang.description, admin_nqt_articles_item_lang.content, admin_nqt_articles_item_lang.id, admin_nqt_articles_item_lang.category_id
        FROM admin_nqt_articles_item INNER JOIN admin_nqt_articles_item_lang 
        ON admin_nqt_articles_item.id = admin_nqt_articles_item_lang.articles_id 
        where admin_nqt_articles_item_lang.category_id = '".$category_id."' and admin_nqt_articles_item_lang.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' and not admin_nqt_articles_item_lang.id = '". $articles_id."' and admin_nqt_articles_item.is_delete = 0 order by admin_nqt_articles_item.id desc");
        return $query->result(); 
    }


    public function get_user_comment($articles_id){
        $query=$this->db->query("
        SELECT admin_nqt_comment_info.id, admin_nqt_comment_info.user_info_id, admin_nqt_user_info.username,
         admin_nqt_comment_info.created, admin_nqt_comment_info.comment, admin_nqt_comment_info.id_comment_parent, 
         admin_nqt_comment_info.like,
         admin_nqt_user_info.thumbnail
        FROM admin_nqt_comment_info INNER JOIN admin_nqt_user_info
        ON admin_nqt_comment_info.user_info_id = admin_nqt_user_info.id
        LEFT JOIN admin_nqt_articles_item_lang on admin_nqt_comment_info.id_artcles_item_lang = admin_nqt_articles_item_lang.id
        where admin_nqt_comment_info.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' 
        and admin_nqt_articles_item_lang.id = '". $articles_id."' and admin_nqt_comment_info.id_comment_parent = '0'
        Order by admin_nqt_comment_info.created desc
        ");
        return $query->result(); 
    }

    public function get_user_comment_child($articles_id,$id_comment_parent){
        $query=$this->db->query("
        SELECT admin_nqt_comment_info.id, admin_nqt_comment_info.user_info_id, admin_nqt_user_info.username, admin_nqt_comment_info.created, admin_nqt_comment_info.comment, admin_nqt_comment_info.id_comment_parent,  admin_nqt_comment_info.like, admin_nqt_user_info.thumbnail
        FROM admin_nqt_comment_info INNER JOIN admin_nqt_user_info
        ON admin_nqt_comment_info.user_info_id = admin_nqt_user_info.id
        LEFT JOIN admin_nqt_articles_item_lang on admin_nqt_comment_info.id_artcles_item_lang = admin_nqt_articles_item_lang.id
        where admin_nqt_comment_info.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' 
        and admin_nqt_articles_item_lang.id = '". $articles_id."' and admin_nqt_comment_info.id_comment_parent = '". $id_comment_parent."'
        Order by admin_nqt_comment_info.created asc
        ");
        return $query->result(); 
    }


    public function insert_user_comment($user_info_id,$id_artcles_item_lang,$id_artcles,$comment){
        $data = array(
            'id' => '',
            'user_info_id' => $user_info_id,
            'id_artcles_item_lang' => $id_artcles_item_lang,
            'id_artcles' => $id_artcles,
            'comment' => $comment,
            'status' => '1',
            'created' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('admin_nqt_comment_info',$data);
    }
    public function insert_user_comment_child($user_info_id,$id_artcles_item_lang,$id_artcles,$id_comment_parent,$comment){
        $data = array(
            'id' => '',
            'user_info_id' => $user_info_id,
            'id_artcles_item_lang' => $id_artcles_item_lang,
            'id_artcles' => $id_artcles,
            'id_comment_parent' => $id_comment_parent,
            'comment' => $comment,
            'status' => '1',
            'created' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('admin_nqt_comment_info',$data);
    }
	
    public function get_link_css(){
        $query = $this->db->query("
            SELECT *
            FROM admincp_style
            where status ='1'
            ORDER BY created desc
          
            ");
        return $query->result();
    }

    public function select_blog_view($slug){
        $query=$this->db->query("
		SELECT admin_nqt_articles_item_lang.view
        FROM admin_nqt_articles_item_lang 
        where admin_nqt_articles_item_lang.slug = '".$slug."' and admin_nqt_articles_item_lang.status = 1 and admin_nqt_articles_item_lang.lang_code = '". $this->lang->default_lang() ."' and admin_nqt_articles_item_lang.is_delete = 0 limit 1");   
        return $query->result(); 
    }

    public function update_blog_view($slug,$view){
        $view = $view+1;
        $data = array(
            'view' => $view,
        );
        $this->db->where("slug", $slug );
        $this->db->update('admin_nqt_articles_item_lang',$data);
    }
    public function footer_menu_link(){
        $query = $this->db->query("SELECT * 
                                   FROM admincp_footer_link_lang
                                   WHERE status = '1' 
                                   AND lang_code = '".$this->lang->default_lang()."'
                                   LIMIT 25
                                    ");
        return $query->result();
    }

    public function profile_user($link_url_redm){
        $this->db->select("*");
        $this->db->where('link_url_redm',$link_url_redm);
        $query_list = $this->db->get("admin_nqt_user_info");
        return $query_list->result();
    }

    public function profile($id_user_info = false){
        $query = $this->db->query("
            SELECT admin_nqt_user_info.*,admin_nqt_user_info.username, admin_newfeed.id , admin_newfeed.created, admin_newfeed.status,  admin_newfeed.content
            FROM admin_newfeed INNER JOIN admin_nqt_user_info
            ON admin_newfeed.id_user_info = admin_nqt_user_info.id
            where admin_newfeed.status = 1 and admin_newfeed.id_user_info = '". $id_user_info ."'
            Order by admin_newfeed.created desc
        ");
        return $query->result();
    }

    public function get_comment_newfeed($id_newfeed){
        $query = $this->db->query("
            SELECT admin_newfeed_comment.*, admin_nqt_user_info.thumbnail, admin_nqt_user_info.username, admin_nqt_user_info.link_url_redm
            FROM admin_newfeed INNER JOIN admin_newfeed_comment
            ON admin_newfeed.id = admin_newfeed_comment.id_newfeed
            LEFT JOIN admin_nqt_user_info on admin_newfeed_comment.id_user_info = admin_nqt_user_info.id
            where admin_newfeed.id = '". $id_newfeed . "' and admin_newfeed.status = 1 and admin_newfeed_comment.id_comment_parent = 0  
            Order by admin_newfeed.created desc
        ");
        return $query->result();
    }

     public function get_comment_newfeed_reply($id_newfeed,$id_comment_parent){
        $query = $this->db->query("
            SELECT admin_newfeed_comment.*, admin_nqt_user_info.thumbnail, admin_nqt_user_info.username, admin_nqt_user_info.link_url_redm
            FROM admin_newfeed INNER JOIN admin_newfeed_comment
            ON admin_newfeed.id = admin_newfeed_comment.id_newfeed
            LEFT JOIN admin_nqt_user_info on admin_newfeed_comment.id_user_info = admin_nqt_user_info.id
            where admin_newfeed.id = '". $id_newfeed . "' and admin_newfeed.status = 1 and admin_newfeed_comment.id_comment_parent = '". $id_comment_parent. "'  
            Order by admin_newfeed.created asc
        ");
        return $query->result();
    }

    public function insert_comment_newfeed($id_user_info,$id_newfeed,$comment){
          $data = array(
            'id' => '',
            'id_user_info' => $id_user_info,
            'id_newfeed' => $id_newfeed,
            'id_comment_parent' => '0',
            'comment' => $comment,
            'like' => '0',
            'status' => '1',
            'created' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('admin_newfeed_comment',$data);
    }

    public function insert_comment_newfeed_reply($id_user_info,$id_newfeed,$id_comment_parent,$comment){
          $data = array(
            'id' => '',
            'id_user_info' => $id_user_info,
            'id_newfeed' => $id_newfeed,
            'id_comment_parent' => $id_comment_parent,
            'comment' => $comment,
            'like' => '0',
            'status' => '1',
            'created' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('admin_newfeed_comment',$data);
    }

    public function up_newfeed($id_user_info,$id_pro='',$content){
        $data = array(
            'id' => '',
            'id_user_info' => $id_user_info,
            'id_pro' => $id_pro,
            'content' => $content,
            'status' => '1',
            'created' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('admin_newfeed',$data);
    }


    public function link_url_redm($str){
        $str = trim($str," ");
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);    

        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);

        $str = preg_replace("/[^a-zA-Z0-9 ]/",'', $str);   
        $str = preg_replace("/ /",'-', $str);    
        return strtolower($str);
    }

    public function get_script(){
        $query = $this->db->query("
            SELECT * FROM admincp_script
            WHERE status = '1'
        ");
        return $query->result(); 
    }
    
	public function get_time_last_login($user_id){
		$this->db->select('*');
		$this->db->from('admin_nqt_reputation');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function checkUserIdDailyLogin($user_id){
		$result = false;
        
        $this->db->select("user_id");
        $this->db->where('user_id', $user_id);
        $query = $this->db->get("admin_nqt_reputation");
        $query_result = $query->result_array();
        if(empty($query_result)){
            $result = true;
        }
        return $result;
	}

    public function get_tag_list(){
        $query = $this->db->query("
            SELECT name,slug FROM  admin_tag_acticles
            WHERE status = '1'
        ");
        return $query->result(); 
    }
	
    public function get_list_countries(){
        $query = $this->db->query("
            SELECT * FROM  countries
        ");
        return $query->result(); 
    }
	
    public function get_list_genre(){
        $query = $this->db->query("
            SELECT * FROM  admin_nqt_genre WHERE is_delete = 0
        ");
        return $query->result(); 
    }
	
	public function get_artists(){
		$query =  $this->db->query("
            SELECT * FROM  admin_nqt_user_info
            WHERE user_type_id = 2
            ORDER BY first_name ASC
        ");
        return $query->result_array();
	}
	
	function get_genre_id(){
		$this->db->select('*');
		$query = $this->db->get('admin_nqt_genre');
		return $query->result();
    }

    public function search_event_list($keyword, $limit = 0, $page = -1){
		$result = false;
		$main_table = 'admin_nqt_events';
		
		$is_count_total_rows = ($limit == 0); // To get total_rows
		
		if($is_count_total_rows){ 
             $this->db->select("{$main_table}.id");
        } else {
            $this->db->select("{$main_table}.*, admin_nqt_events_lang.name, admin_nqt_events_lang.description, admin_nqt_events_lang.slug");
        }
		
        $this->db->from($main_table);
        $this->db->like('name_en', $keyword);
		$this->db->join('admin_nqt_events_lang',"{$main_table}.id = admin_nqt_events_lang.event_id", 'inner');
		
		if(!$is_count_total_rows){
			// Limit
			$this->db->limit($limit,$page);
			
			// Order
			$order_by = 'id';
			$order_by = "{$main_table}.{$order_by}";
			$order_direction = 'desc';
			$this->db->order_by($order_by, $order_direction);
		}
		
		/*Begin: Condition*/
		// Begin - Search condition
		$this->db->where("{$main_table}.status", 1);
		$this->db->where("{$main_table}.is_delete", 0);
		$this->db->where("admin_nqt_events_lang.lang_code", $this->lang->default_lang());
		/*End: Condition*/
		
		
		if($is_count_total_rows){
			$result = $this->db->count_all_results();
		} else {
			$query = $this->db->get();
			$result = $query->result();
		}
		
		// FOR DEBUG
		$debug = false;
		if($debug){
			echo $this->db->last_query();
			exit();
		}
		
		return $result;
    }
} 