<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Addcontent_model extends MY_Model {

	private $module = 'home';
    private $token;
	private $table = 'admin_newfeed';

    function __construct(){
        parent::__construct();
        //$this->lang_code = $this->lang->default_lang();
        $this->load->database();
        $this ->table = 'admin_newfeed';
        $this->tableName = 'admin_newfeed';
        $this->primaryKey = 'id';
    }

    public function insert_addContent($data){
		$userId = $data['id_user_info'];
		
		$this->insert($this->table, $data);
		
		if ($this->db->affected_rows() > 0) {
            $id = $this->db->insert_id();
            $bit_id = generate_id_by_bitwise($userId, $id);
            $dataUpdate = array('feed_bit_id' => $bit_id, 'status'=>(int)1);
            $this->db->where('id', (int)$id);
            $this->db->update('admin_newfeed', $dataUpdate);
            if($this->db->affected_rows() > 0) {
                $dataInsert ['id'] = $id;
                $dataInsert ['feed_bit_id'] = $bit_id;
                $result = $dataInsert;
            }
        }

        return $result;
	}
}