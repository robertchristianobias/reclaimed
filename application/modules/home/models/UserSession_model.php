<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserSession_model extends MY_Model {

	private $module = 'home';

	function __construct() {
		parent::__construct();
	}

	private function getUserDataByUserId($userId) {
		$result = FALSE;
        $this->db->select('*');
        $this->db->where('id', (int)$userId);
        $query = $this->db->get('admin_nqt_user_info');
        if ( ! empty ($query->row_array()) ) {
            $result = $query->row_array();
        }
        return $result;
	}

	public function setSessionWhenLoginSuccessByUserId($userId) {
		// pr($userId,1);
	$userData = $this->getUserDataByUserId($userId);
		if( ! empty($userData)) {
			$data = array();
			foreach($userData as $key => $value) {
				$data ['userData'][$key] = $value;
			}
			$data ['userlogin']['id'] = $userData ['id'];
			$data ['userlogin']['username'] = $userData ['username']; // TODO - Removed and replaced by name
			$data ['userlogin']['name'] = $userData ['username'];
			$data ['userlogin']['thumbnail'] = $userData ['thumbnail'];
			$data ['userlogin']['link_url_redm'] = $userData ['link_url_redm'];
			$data ['userlogin']['user_bit_id'] = $userData ['user_bit_id'];
			$this->session->set_userdata($data);
		}
	}

	public function setUserLoginData($userData) {
		$data = array();
		foreach($userData as $key => $value) {
			$data ['userData'][$key] = $value;
		}
		$data ['userlogin']['id'] = $userData ['id'];
		$data ['userlogin']['username'] = $userData ['username'];
		$data ['userlogin']['thumbnail'] = $userData ['thumbnail'];
		$data ['userlogin']['link_url_redm'] = $userData ['link_url_redm'];
		$data ['userlogin']['user_bit_id'] = $userData ['user_bit_id'];
		$this->session->set_userdata($data);
	}

	public function getUserLoginData() {
		$result = FALSE;
		if ( ! empty($this->session->get_userdata()['userlogin']) ) {
			$result = $this->session->get_userdata()['userlogin'];
		}
		return $result;
	}

	public function getUserIdLogin() {
		$result = FALSE;
		if ( ! empty($this->session->get_userdata()['userlogin']['id']) ) {
			$result = $this->session->get_userdata()['userlogin']['id'];
		}
		return $result;
	}

	public function getUserBitIdLogin() {
		$result = FALSE;
		if ( ! empty($this->session->get_userdata()['userlogin']['user_bit_id']) ) {
			$result = $this->session->get_userdata()['userlogin']['user_bit_id'];
		}
		return $result;
	}

	public function getUserLoginAvatar() {
		$result = FALSE;
		if ( ! empty($this->session->get_userdata()['userlogin']['thumbnail']) ) {
			$result = $this->session->get_userdata()['userlogin']['thumbnail'];
		}
		return $result;
	}

	public function getUserLoginUsername() {
		$result = FALSE;
		if ( ! empty($this->session->get_userdata()['userlogin']['username']) ) {
			$result = $this->session->get_userdata()['userlogin']['username'];
		}
		return $result;
	}


}