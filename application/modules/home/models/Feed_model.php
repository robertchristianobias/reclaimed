<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feed_model extends MY_Model {

	private $module = 'home';
    private $token;
	private $table = '';

	function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /*
    ##### QUERY PAGING
    $page_start = 1, $per_page = 10;
    $offset = abs( (intval( $page_start ) - 1) * intval ( $per_page ) );
    $this->db->limit($per_page, $offset); //paging
    ###### COUNT TOTAL PAGE
    $totalPaging = ceil($query->row_array()['count_id'] / intval($perItem));
     */

    public function getFeedsByUserId($userId, $begin = 1, $perItem = 10) {
        $result = array();
        $offset = abs ((intval ($begin) - 1) * intval ($perItem));

        $this->db->select('newfeed.*, user.thumbnail, user.username, user.link_url_redm, user.user_bit_id');
        $this->db->from('admin_newfeed AS newfeed');
        $this->db->join('admin_nqt_user_info AS user', 'newfeed.id_user_info = user.id', 'left');
        $this->db->where('newfeed.id_user_info', $userId);
        $this->db->where('newfeed.status', 1);
        $this->db->order_by('newfeed.id', 'DESC');
        $this->db->limit($perItem, $offset);
        $query = $this->db->get();

        if ( ! empty ($query->result_array()) ) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function getTotalPagingFeedByUserId($userId, $perItem = 10) {
        $totalPaging = 1;
        $this->db->select('count(id) AS count_id');
        $this->db->from('admin_newfeed AS newfeed');
        $this->db->where('newfeed.id_user_info', $userId);
        $this->db->where('newfeed.status', 1);
        $query = $this->db->get();
        if(!empty($query->row_array())) {
            $totalPaging = ceil($query->row_array()['count_id'] / intval($perItem));
        }
        return $totalPaging;
    }

    public function getCommentsByFeedId($feedId, $begin = 1, $perItem = 10){
        $result = FALSE;
        $offset = abs ((intval ($begin) - 1) * intval ($perItem));
        
        $query = $this->db->query("
            SELECT comment.*, user.thumbnail, user.username, user.link_url_redm, user.user_bit_id
                FROM (SELECT *
                    FROM admin_newfeed_comment 
                    WHERE id_newfeed = {$feedId}
                    AND status = 1
                    AND id_comment_parent = 0
                    ORDER BY id DESC 
                    LIMIT {$offset}, {$perItem}) AS comment
                LEFT JOIN admin_nqt_user_info as user ON comment.id_user_info = user.id
            ORDER BY id ASC");
        // echo $this->db->last_query(); exit;
        if ( ! empty ($query->result_array()) ) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function getTotalPagingCommentByFeedId($feedId, $perItem = 10) {
        $totalPaging = 1;
        $this->db->select('count(id) AS count_id');
        $this->db->from('admin_newfeed_comment AS comment');
        $this->db->where('comment.id_newfeed', (int)$feedId);
        $this->db->where('id_comment_parent', 0);
        $this->db->where('comment.status', 1);
        $query = $this->db->get();
        if(!empty($query->row_array())) {
            $totalPaging = ceil($query->row_array()['count_id'] / intval($perItem));
        }
        return $totalPaging;
    }

     public function getRepliesByFeedIdAndCommentId($feedId, $commentId, $begin = 1, $perItem = 10){
        $result = FALSE;
        $offset = abs ((intval ($begin) - 1) * intval ($perItem));
        $query = $this->db->query("
            SELECT comment.*, user.thumbnail, user.username, user.link_url_redm, user.user_bit_id
                FROM (SELECT *
                    FROM admin_newfeed_comment 
                    WHERE id_newfeed = {$feedId}
                    AND status = 1
                    AND id_comment_parent = {$commentId}
                    ORDER BY id DESC 
                    LIMIT {$offset}, {$perItem}) AS comment
                LEFT JOIN admin_nqt_user_info as user ON comment.id_user_info = user.id
            ORDER BY id ASC");
        // echo $this->db->last_query(); exit;
        if ( ! empty ($query->result_array()) ) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function getTotalPagingReplyByFeedIdAndCommentId($feedId, $commentId, $perItem = 10) {
        $totalPaging = 1;
        $this->db->select('count(id) AS count_id');
        $this->db->from('admin_newfeed_comment AS comment');
        $this->db->where('comment.id_newfeed', (int)$feedId);
        $this->db->where('id_comment_parent', (int)$commentId);
        $this->db->where('comment.status', 1);
        $query = $this->db->get();
        if(!empty($query->row_array())) {
            $totalPaging = ceil($query->row_array()['count_id'] / intval($perItem));
        }
        return $totalPaging;
    }

    /**
     * [saveNewFeed description]
     * @param  [int] $userId  [user ID of feed]
     * @param  [string] $content [content of feed]
     * @return [FALSE / feed data]          [feed data]
     */
    public function saveNewFeed($userId, $content) {
        $result = FALSE;
        $created = strtotime(date('Y-m-d H:i:s'));

        $dataInsert = array();
        $dataInsert ['id_user_info'] = $userId;
        $dataInsert ['content'] = $content;
        $dataInsert ['created'] = $created;
        $dataInsert ['status'] = 1;
        
        $dataInsert ['total_like'] = 0;
        $dataInsert ['total_comment'] = 0;
        $dataInsert ['total_share'] = 0;
        
        $this->db->insert('admin_newfeed', $dataInsert);
        
        if ($this->db->affected_rows() > 0) {
            $id = $this->db->insert_id();
            $bit_id = generate_id_by_bitwise($userId, $id);
            $dataUpdate = array('feed_bit_id' => $bit_id);
            $this->db->where('id', (int)$id);
            $this->db->update('admin_newfeed', $dataUpdate);
            if($this->db->affected_rows() > 0) {
                $dataInsert ['id'] = $id;
                $dataInsert ['feed_bit_id'] = $bit_id;
                $result = $dataInsert;
            }
        }

        return $result;
    }

    public function updateFeed($data) {

    }

    /**
     * [checkFeedExists description]
     * @param  [type] $feedBitId [description]
     * @return [type]            [description]
     */
    public function checkFeedExists($feedBitId) {
        $result = FALSE;
        $this->db->select('id');
        $this->db->where('feed_bit_id', $feedBitId);
        $this->db->where('status', 1);
        $query = $this->db->get('admin_newfeed');
        
        if( ! empty($query->row())) {
            $result = $query->row('id');
        }

        return $result;
    }

    /**
     * [checkCommentExists: function check comment exists]
     * @param  [int] $feedBitId    [Feed bit ID]
     * @param  [int] $commentBitId [Comment bit ID]
     * @return [array] id, id_newfeed [id of comment and feed id of comment]
     */
    public function checkCommentExists($feedBitId, $commentBitId) {
        $result = FALSE;
        $this->db->select('id, id_newfeed');
        $this->db->where('feed_bit_id', $feedBitId);
        $this->db->where('comment_bit_id', $commentBitId);
        $this->db->where('status', 1);
        $query = $this->db->get('admin_newfeed_comment');
        
        if( ! empty($query->row())) {
            $result = $query->row_array();
        }
        return $result;
    }

    /**
     * [saveComment: function save comment]
     * @param  [int] $userId       [user ID of comment]
     * @param  [int] $feedId       [feed ID]
     * @param  [int] $feedBitId    [feed Bit ID]
     * @param  [string] $comment   [content of comment]
     * @return [FALSE/Array]        [FALSE or comment data]
     */
    public function saveComment($userId, $feedId, $feedBitId, $comment) {
        $result = FALSE;
        $created = date('Y-m-d H:i:s');

        $dataInsert = array();
        $dataInsert ['id_user_info'] = $userId;
        $dataInsert ['id_newfeed'] = $feedId;
        $dataInsert ['feed_bit_id'] = $feedBitId;
        $dataInsert ['comment'] = $comment;
        $dataInsert ['created'] = $created;
        $dataInsert ['status'] = 1;
        
        $this->db->insert('admin_newfeed_comment', $dataInsert);
        if ($this->db->affected_rows() > 0) {
            $id = $this->db->insert_id();
            $bit_id = generate_id_by_bitwise($userId, $id);
            $dataUpdate = array('comment_bit_id' => $bit_id);
            $this->db->where('id', (int)$id);
            $this->db->update('admin_newfeed_comment', $dataUpdate);
            if($this->db->affected_rows() > 0) {
                $dataInsert ['id'] = $id;
                $dataInsert ['comment_bit_id'] = $bit_id;
                $result = $dataInsert;
            }
        }

        return $result;
    }

    /**
     * [saveReply description]
     * @param  [type] $userId      [description]
     * @param  [type] $feedId      [description]
     * @param  [type] $feedBitId   [description]
     * @param  [type] $parentId    [description]
     * @param  [type] $parentBitId [description]
     * @param  [type] $comment     [description]
     * @return [type]              [description]
     */
    public function saveReply($userId, $feedId, $feedBitId, $parentId, $parentBitId, $comment) {
        $result = FALSE;
        $created = date('Y-m-d H:i:s');

        $dataInsert = array();
        $dataInsert ['id_user_info'] = $userId;
        $dataInsert ['id_newfeed'] = $feedId;
        $dataInsert ['id_comment_parent'] = $parentId;
        $dataInsert ['feed_bit_id'] = $feedBitId;
        $dataInsert ['parent_comment_bit_id'] = $parentBitId;
        // $dataInsert ['comment_bit_id'] = $parentBitId;
        $dataInsert ['comment'] = $comment;
        $dataInsert ['created'] = $created;
        $dataInsert ['status'] = 1;

        $this->db->insert('admin_newfeed_comment', $dataInsert);
        if ($this->db->affected_rows() > 0) {
            $id = $this->db->insert_id();
            $bit_id = generate_id_by_bitwise($userId, $id);
            $dataUpdate = array('comment_bit_id' => $bit_id);
            $this->db->where('id', (int)$id);
            $this->db->update('admin_newfeed_comment', $dataUpdate);
            if($this->db->affected_rows() > 0) {
                $dataInsert ['id'] = $id;
                $dataInsert ['comment_bit_id'] = $bit_id;
                $result = $dataInsert;
            }
        }

        return $result;
    }

    public function updateNumLikeOfFeed($feedId, $isLike) {

    }

    public function updateNumCommentOfFeed($feedId, $totalComment = FALSE) {
        if($totalComment === FALSE) {
            $totalComment = $this->countTotalCommentOfFeed($feedId);
        }
        $data = array('total_comment' => (int)$totalComment);
        $this->db->where('id', (int)$feedId);
        $this->db->update('admin_newfeed', $data);
        return $totalComment;
    }

    public function countTotalCommentOfFeed($feedId) {
        $result = 0;
        $this->db->select('count(id) AS count_id');
        $this->db->from('admin_newfeed_comment AS comment');
        $this->db->where('comment.id_newfeed', (int)$feedId);
        $this->db->where('id_comment_parent', 0);
        $this->db->where('comment.status', 1);
        $query = $this->db->get();
        if(!empty($query->row_array())) {
            $result = $query->row_array()['count_id'];
        }
        return $result;
    }

    public function getAllComment() {
        $result = FALSE;
        $this->db->select('*');
        $query = $this->db->get('admin_newfeed_comment');
        if(!empty($query->result_array())) {
            $result = $query->result_array();
        }
        return $result;
    }

    public function updateBitIdOfFeed($data) {
        foreach ($data as $key => $item) {
            $data_update = array(
                'feed_bit_id' => $item['feed_bit_id']
                );
            $this->db->where('id', (int)$item['id']);
            $this->db->update('admin_newfeed', $data_update);
            unset($data_update);
        }
    }

    public function updateBitIdOfComment($data) {
        foreach ($data as $key => $comment) {
            $data_update = array(
                'comment_bit_id' => $comment['comment_bit_id'],
                'feed_bit_id' => $comment['feed_bit_id'],
                'parent_comment_bit_id' => $comment['parent_comment_bit_id']
                );
            $this->db->where('id', (int)$comment['id']);
            $this->db->update('admin_newfeed_comment', $data_update);
            unset($data_update);
        }
    }

}