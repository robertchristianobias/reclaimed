<?php 
// Time post status - comment
function time_newfeed($time){
  $time_newfeed = time() - strtotime($time);
  if($time_newfeed < 60 ){
    return 'Just now';
  } else if($time_newfeed < 3600 ) { 
    return floor(($time_newfeed)/60) .' mins';
  } else if($time_newfeed >= 3600 && $time_newfeed < 86400 ){ 
    return floor(($time_newfeed)/3600) .' hrs';
  } else if($time_newfeed >=  86400 && $time_newfeed < 172800 ){ 
    return floor(($time_newfeed)/86400) .' days';
  } else{
    return date('H:i d-m-Y', strtotime($time));
  }
} ?>

<?php 
// Comment of newfeed
$comment2 = modules::run('home/get_comment_newfeed', $id_artcles_newfeed);
foreach ($comment2 as $result2) { ?>

<div class="list_comment_newfeed_detail mg-t20">
    <div class="col-xs-1 padding-left-0">
        <a href="/en/profile/<?php echo $result2->link_url_redm ?>">
            <img class="comment-img" src="<?=get_resource_url($result2->thumbnail)?>" alt="<?php echo $result2->username ?>" width="60" height="60" />
         </a>   
    </div>

    <div class="col-xs-11 padding-left-0">
        <div class="comment-name">
            <a href="/en/profile/<?php echo $result2->link_url_redm ?>"><?php echo $result2->username ?></a>
            <span class="span-time"><?php echo time_newfeed($result2->created) ?></span>
            
            <?php
            // Isset Session user -> Reply
            if(isset($this->session->userdata['userData'])){ ?>
                <span class="span-reply" title="Reply" 
                       data-ajax-load-comment-link="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/ajax_comment_newfeed_reply" 
                       data-comment-parent="<?php echo $result2->id ?>"
                       data-comment-artcles="<?php echo $id_artcles_newfeed ?>">
                    Reply
                </span>
            <?php } ?>
        </div>

        <div class="comment-content">
             <?php echo htmlspecialchars($result2->comment) // display HTML tags as plain text ?>   
        </div>
    </div>

    <div class="clear-fix"></div>

    <div class="list_comment_newfeed_detail_reply">
        <?php 
        // Comment reply of newfeed
        $comment3 = modules::run('home/get_comment_newfeed_reply', $id_artcles_newfeed, $result2->id );
        foreach ($comment3 as $result3) { ?>


        <div class="list_comment_newfeed_detail_reply_box">
            <div class="col-xs-1 padding-left-0">
                <a href="/en/profile/<?php echo $result3->link_url_redm ?>">
                    <img class="comment-img" src="<?=get_resource_url($result3->thumbnail)?>" alt="<?php echo $result3->username ?>" width="60" height="60" />
                </a>
            </div>

            <div class="col-xs-11 padding-left-0">
                <div class="comment-name">
                    <a href="/en/profile/<?php echo $result3->link_url_redm ?>"><?php echo $result3->username ?></a>
                    <span class="span-time"><?php echo time_newfeed($result3->created) ?></span>
                    
                    <?php
                    // Isset Session user -> Reply
                    if(isset($this->session->userdata['userData'])){ ?>
                       <span class="span-reply" title="Reply" 
                       data-ajax-load-comment-link="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/ajax_comment_newfeed_reply" 
                       data-comment-parent="<?php echo $result2->id ?>"
                       data-comment-artcles="<?php echo $id_artcles_newfeed ?>">
                            Reply
                        </span>
                    <?php } ?>
                </div>

                <div class="comment-content">
                     <?php echo htmlspecialchars($result3->comment) // display HTML tags as plain text ?>   
                </div>
            </div>
        </div><!-- End list_comment_newfeed_detail_reply_box -->
        <div class="clear-fix"></div>

        <?php } ?>

        <div class="list_comment_newfeed_detail_reply_comment" data-comment-parent="<?php echo $result2->id ?>">
            <!-- ajax load form comment -->
        </div>

    </div><!-- End list_comment_newfeed_detail_reply -->



</div><!-- End list_comment_newfeed_detail -->
<div class="clear-fix"></div>

<?php } // end comment   ?>