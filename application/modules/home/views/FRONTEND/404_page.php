<div class="container-fluid bg-services no-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
				<div class="text-services row-flex">
					<h2 class="hidden-xs"><?=$title?></h2>
					<h3 class="visible-xs"><?=$title?></h3>
					<p><a class="menu_item" href="index.html">Home</a> / <?=$title?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid no-padding margin-top-60-pc">
	<div class="block-title">
		<h2 class="text-title">
			<?=$this->lang->line('page_not_found')?>
		</h2>
	</div>
</div>
<div class="margin-top-100"></div>