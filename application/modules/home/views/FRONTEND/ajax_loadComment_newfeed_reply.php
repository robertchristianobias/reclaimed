<!-- Comment Reply Newfeed  -->
<div class="clear-fix"></div>
<?php if(isset($this->session->userdata['userData'])){
 $userDataSetting1 = modules::run('home/load_list_user_info', $this->session->userdata['userData']['id']);
foreach ($userDataSetting1 as $value ){
$username =  $value->username;
$thumbnail = $value->thumbnail;
$user_info_id = $value->id;
}


?>

<div class="comment-write comment-write-profile mg-t20">
    <form action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/comments_newfeed_reply" method="POST" class="form-comment-newfeed">
        <div class="mg-t10 mg-b10">
            <img class="comment-img" src="<?=get_resource_url($thumbnail)?>" alt="<?php echo $username ?>" width="60" height="60" />
            <input type="hidden" name='id_user_info' value="<?php echo $user_info_id ?>">
            <input type="hidden" name='id_newfeed' value="<?php echo $id_artcles_newfeed ?>">
            <input type="hidden" name='id_comment_parent' value="<?php echo $id_comment_parent ?>">
            <textarea id="comment-box" name="comment" placeholder="Write Comment..." required rows="4"></textarea>
        </div>
        <div class="comment-action">
            <input type="reset" name="reset" value="Cancel" />
            <input type="submit" name="comment" value="Post"/>
            <div class="clearfix"></div>
        </div>
    </form>
</div>



<div class="clear-fix"></div>
<!-- end write comment -->
<?php } ?>