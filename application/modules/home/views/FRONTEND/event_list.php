<?php
$i = 0;
foreach ($result as $obj_event){
	$i++;
	
	// Example: 
	// https://www.facebook.com/events/166183724063410: Tuesday, January 1, 2019 at 4 PM - 7 PM
	// https://www.facebook.com/events/1892466364117153: - December 31, 2018 � January 1, 2019
	$date_time_start_int = strtotime($obj_event->date_time_start);
	$date_time_end_int = strtotime($obj_event->date_time_end);
	$date_start = date(DATE_FORMAT_DAY, $date_time_start_int);
	$time_start = date('H:i', $date_time_start_int);
	$date_end = date(DATE_FORMAT_DAY, $date_time_end_int);
	$time_end = date('H:i', $date_time_end_int);
	$datetime_displayed = '';
	if($date_start == $date_end){
		// Tuesday, January 1, 2019 at 4 PM - 7 PM
		$datetime_displayed = "{$date_start} at {$time_start} - {$time_end}";
	} else {
		$date_start = date(DATE_FORMAT_DAY, $date_time_start_int);
		$date_end = date(DATE_FORMAT_DAY, $date_time_end_int);
		
		// December 31, 2018 � January 1, 2019
		$datetime_displayed = "{$date_start} - {$date_end}";
	}
	
?>
	<div class="box-info-event">
		<div class="info-event">
			<p class="name-event color-event no_margin text-uppercase"><?=$obj_event->name?></p>
			<p class="dript-event color-event no_margin"><?=$datetime_displayed?></p>
		</div>
		<div class="img-responsive">
			<img class="imgw100" src="<?php echo PATH_URL.$obj_event->image?>" alt="img">
		</div>
	</div>
<?php
}
?>	