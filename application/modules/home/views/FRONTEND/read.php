<?php


?>

<div class="container margintop">

        <div class="" style="color:white;font-size:30px;margin-left:15px;margin-bottom:10px;text-transform: uppercase;">
         <span class="fix_mobi"style="background:red;padding:5px; position: relative;"><?php echo '#'.$data_tag ?></span>
         </div>

        <div class=" list-post">
                    
                <?php
                $articles_all = modules::run('home/get_articles_all',$data_tag );
                //print_r($articles_all);
                foreach ($articles_all as $result) {   
                ?>
                   
                        <div class="col-sm-4 item mg-b20 item-latest-slick">
                            <div class="wrap-image">
                            <?php if($this->lang->default_lang() == 'en') { ?>
                                <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>">
                            <?php } else{ ?>
                                <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>">   
                            <?php  } ?>
                            <img src="<?php echo get_resource_url(substr($result->image, 1)) ?>" alt="<?php echo $result->title ?>" class="image-post">
                            </a>
                    
                          
                            </div>
                            <div class="text-content">
                                <p class="hidden"><?php if($this->lang->default_lang() == 'en') { ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="tag-a-title1 p-fs-20">
                                    <?php } else{ ?>
                                        <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="tag-a-title1 p-fs-20">   
                                    <?php } ?><?php echo $result->title ?></a>
                                </p>
                                <p class="post-p-des" style="min-height: 58px; word-wrap: break-word;"><?php echo the_excerpt($result->description, 16);  ?></p>
                                <?php if($this->lang->default_lang() == 'en') { ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more">
                                <?php } else{ ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more">   
                                <?php } ?><i class="fa fa-external-link" aria-hidden="true"></i> Read more</a>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                  
                <?php
                   
                }
                ?>
                </div>
            
            </div>
        </div>
    </div>

</div>  
   
<style>
.list-post .wrap-image{
    padding-top: 46%;
}
</style>