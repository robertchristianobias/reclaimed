<script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
<script type="text/javascript">
        // tinymce.init({
        //     selector: '#content',
        //     // plugins: "code, link, image, lists, preview, textcolor, pix_embed_online_media, responsivefilemanager",
        //     // toolbar: ['undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect forecolor backcolor | bullist numlist outdent indent | link image | print preview media fullpage removeformat','pix_embed_online_media | responsivefilemanager'],
        //     // external_filemanager_path:"../filemanager/",
        //     // filemanager_title:"Responsive Filemanager" ,
        //     // relative_urls: false,
        //     // // external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
        //     //   // toolbar: "anchor",
        //     //   // menubar: "insert"
        // });
function save(){
    console.log('save');
    var content_html = tinyMCE.get('content').getContent();
        $('#content').val(content_html);
    var options = {
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
    };
}



</script>


<style>
.form-group{
    margin-bottom:0px;
}
form.newfeed-form {
    position: relative;
}
#content{
    max-width: 100%;
    color: #000;
}
</style>
 <div class="w-wapper mt-25">
    <div id="profile-info">
            <div class="main-contact-profile">
                <?php foreach($profile_user as $result) {
                    $id_profile = $result->id; 

                 ?> 
                <div class="contact-profile">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="person-profile-img">
                                <img class="img-responsive img-profile" src="<?=get_resource_url($result->thumbnail)?>" alt="img" />
                            </div>
                        </div>

                        <div class="col-sm-8 col-md-8 mt-25 person-profile-info">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <p class="f-26"><?php echo $result->username ?></p>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <!--<ul>              
                                    <li class="cot-1"><span class="pink">CONNECT</li>
                                    <li class="cot-1"><span class="pink">FOLLOW</li>
                                    <li class="cot-1"><span class="pink">DEMAND</li>
                                </ul>-->
                            </div>
                            <div class="clear-fix mb-25"></div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <ul>
                                    <li class="cot-2"><span class="white">Email:</span> <?php echo $result->email ?>  </li>
                                    <li class="cot-2"><span class="white">Contact:</span> <?php echo $result->contact ?>  </li>
                                    <li class="cot-2"><span class="white">LOCATION:</span> <?php echo $result->location ?>  </li>
                                    <li class="cot-2"><span class="white">DISTRIBUTOR:</span> <?php echo $result->distributor ?>  </li>
                                </ul>
                            </div>
                  
                            <div class="clear-fix mb-25"></div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <ul>
                                    <li class="cot-2"><span class="white">WWW:</span> <?php echo $result->url ?> </li>
                                     <li class="cot-2"><span class="white">FACEBOOK:</span> <?php echo $result->facebook ?></li>
                                    <li class="cot-2"><span class="white">TWITTER:</span> <?php echo $result->twitter ?></li>
                                     <li class="cot-2"><span class="white">BEATPORT:</span> <?php echo $result->beatport ?></li>
                                </ul>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <ul>
                                    <li class="cot-2"><span class="white">SOUNDCLOUD:</span> <?php echo $result->soundcloud ?></li>
                                    <li class="cot-2"><span class="white">MIXCLOUD:</span> <?php echo $result->mixcloud ?></li>
                                     <li class="cot-2"><span class="white">ITUNES</span> <?php echo $result->itunes ?></li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
                <?php } ?>
            </div>


            <div class="clearfix"></div>


            <div class="main-content" style="margin-top:20px">


                <?php
                $latest = modules::run('home/get_articles_latest');
                $i = 0;
                foreach ($latest as $result){
                $i++;

                ?>

                

           


                <div class="pix-col one-line pix-col-3 list-post">
                    <div class="item item-latest mg-b20">
                        <div class="wrap-image">
                             <?php if($this->lang->default_lang() == 'en') { ?>
                                <a href="<?php echo get_url_language('/blog/'. $result->slug_en)  ?>" >
                            <?php } else{ ?>
                                <a href="<?php echo get_url_language('/blog/'. $result->slug_cn)  ?>">   
                            <?php  } ?>
                                <img src="<?php echo get_resource_url(substr($result->image, 1)) ?>" alt="<?php echo $result->title ?>" class="image-post">
                            </a>
                        </div>
                        <div class="text-content-profile">
                        
                              <p class="post-p-des" style="min-height: 22px;word-wrap: break-word;"><?php echo the_excerpt($result->description, 16);  ?></p>
                            <?php if($this->lang->default_lang() == 'en') { ?>
                                <a href="<?php echo get_url_language('/blog/'. $result->slug_en)  ?>" class="read-more">
                            <?php } else{ ?>
                                <a href="<?php echo get_url_language('/blog/'. $result->slug_cn)  ?>" class="read-more">   
                            <?php } ?>Read more</a>
                            <div class="clear-fix"></div>
                        </div>
                    </div>
                </div>

                <?php } ?>


                <div class="clearfix-mt40"></div>
                
                <?php 
                // id user != id newfeed => hide 
                if(isset($this->session->userdata['userData'])){
                    $userDataSetting1 = modules::run('home/load_list_user_info', $this->session->userdata['userData']['id']);
                    foreach ($userDataSetting1 as $value ){
                      $user_info_id = $value->id;
                    }
                    
                } 
                if( !empty($user_info_id) && $user_info_id == $id_profile ){ // login and is_user == id_newfeed ?>
                <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <form class="newfeed-form" method="POST" action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/up_newfeed" >
                        <textarea data-required="1" cols="" rows="8" name="content" id="content" required></textarea>
                        <input type="hidden" name='id_user_info' value="<?php echo $user_info_id ?>">
                        <div class="clearfix"></div>
                        <div class="form-button-submit">
                            <button type="submit">Submit</button>
                            <button type="reset">Cancel</button>
                        </div>
                        </form>
                    </div>    
                </div>  
                <?php }  ?>


                <div class="ajax-load-newfeed" data-load-newfeed="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/ajax_loadnewfeed/<?php echo $id_profile ?>">
                     <!-- ajax - load - newfeed -->
                </div>
                   
            </div>
    </div>
</div>
<div class="clearfix"></div>


