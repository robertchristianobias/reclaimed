    <div class="main-content mt140 alpha-content">
        <div class="title a-to-z">
            <!-- <a href="#"><h3><span class="active">A</span></h3></a> -->
            <a href="#" class="active"><h3><span>A</span></h3></a>
            <a href="#"><h3><span>B</span></h3></a>
            <a href="#"><h3><span>C</span></h3></a>
            <a href="#"><h3><span>D</span></h3></a>
            <a href="#"><h3><span>E</span></h3></a>
            <a href="#"><h3><span>F</span></h3></a>
            <a href="#"><h3><span>G</span></h3></a>
            <a href="#"><h3><span>H</span></h3></a>
            <a href="#"><h3><span>I</span></h3></a>
            <a href="#"><h3><span>J</span></h3></a>
            <a href="#"><h3><span>K</span></h3></a>
            <a href="#"><h3><span>L</span></h3></a>
            <a href="#"><h3><span>M</span></h3></a>
            <a href="#"><h3><span>N</span></h3></a>
            <a href="#"><h3><span>O</span></h3></a>
            <a href="#"><h3><span>P</span></h3></a>
            <a href="#"><h3><span>Q</span></h3></a>
            <a href="#"><h3><span>R</span></h3></a>
            <a href="#"><h3><span>S</span></h3></a>
            <a href="#"><h3><span>T</span></h3></a>
            <a href="#"><h3><span>U</span></h3></a>
            <a href="#"><h3><span>V</span></h3></a>
            <a href="#"><h3><span>W</span></h3></a>
            <a href="#"><h3><span>X</span></h3></a>
            <a href="#"><h3><span>Y</span></h3></a>
            <a href="#"><h3><span>Z</span></h3></a>


        </div>
			<div class="pix-col one-line pix-col-3 list-post mg-t30">
            <?php 
                foreach ($artists_mapping as $key => $val) { 
                    foreach ($val as  $q) {
                        $img = $q['image'];
                        $thumbnail = $q['thumbnail'];
                        $first_name = $q['first_name'];
                        $last_name = $q['last_name'];
						$name = $q['username'];
                        $genre = (isset($q['genre']))? $q['genre'] : '';
						$link_pro = PATH_URL.'en/artist/'.$q['url'];
                        
             ?>     
                <div class="item item-latest item-artist mg-b30 letter_<?=$key?>">
					<a href="<?=$link_pro?>">
						<div class="wrap-image">
							<img src="<?=get_resource_url($img)?>" alt="" class="image-post">
						</div>
						<div class="text-content" style="text-transform: uppercase;">
							<p class="post-p-des" style="min-height: 30px; overflow-wrap: break-word;"><?=$name;?></p>
							<p class="post-p-des" style="min-height: 22px; overflow-wrap: break-word;"><?=$genre?></p>
							<div class="clear-fix"></div>
						</div>
					</a>
                </div>
            <?php } }?>
			</div> 
        </div>
        <div class="clearfix"></div>
    </div>    
    <div class="clearfix"></div>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
	
<script type="text/javascript">
$(document).ready(function(){
    $('.title a').click(function(){
        $('.title a span').removeClass('active');
        $(this).find('span').addClass('active');

        var letter = $(this).text();
        letter = letter.toLowerCase();
        $('.item-artist').hide(); // Hide all first
        var item_class = 'letter_' + letter;
        $('.'+item_class).show();
        return false;
    });

	var jele_letter;
    var a_to_z_len = $(".title.a-to-z a").length;
    for( var i = 0 ; i < a_to_z_len ; i++ ){
       text_alpha =  $(".title.a-to-z a").eq(i).find('span').text();
       text_alpha = text_alpha.toLowerCase();
      
      // console.log($('.letter_' + text_alpha).length);
       if($('.letter_' + text_alpha).length == 0){
            $(".title.a-to-z a").eq(i).addClass('alpha-clear').css({'opacity' : 0.5});
			$(".title.a-to-z a").eq(i).find('span').css({'color' : 'grey'});
       } else {
		   if(!jele_letter){
				jele_letter =  $(".title.a-to-z a").eq(i);
		   }
	   }
    }
	
	// Click first letter having artists
	if(jele_letter){
		console.log(jele_letter);
		jele_letter.get(0).click();
	}
});
</script>

<style type="text/css">
.alpha-clear{
    pointer-events: none;
}    
.list-post .wrap-image{
    padding-top: 70%;
}
</style>