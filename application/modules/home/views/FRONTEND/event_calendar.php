<div class="section-event">
	<div class="inner-event">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		<div class="title-event">
			<!-- <form> -->
				<div class="form-group">
					<input type="text" class="form-control" style="width:100%" id="search_event" placeholder="Search...">
					<br/>
				</div>
			<!-- </form> -->
			<!--<input class="input_date" type="date" name="" value="" placeholder=""><br/><br/>-->
			<div class="fixshow show_hide_cal">
				<div id="calendar"></div>
			</div>
			
			<!-- <div class="date-input" style="margin-bottom: 10px;">
				<p class="form-control date-value"></p>
				<input name="bday" class="date-pc hidden-sp" type="text">
				<input class="date-sp hidden-pc" type="date">
			</div> -->
			<ul class="box-cal">
				<li><button type="button" class="btn btn-default show_cal">Show Calendar</button></li>
				<li><button type="button" class="btn btn-default hide_cal">Hide Calendar</button></li>
			</ul>
			
			TODAY'S EVENT
			
		</div>
		<hr style="margin-top: 10px;margin-bottom: 10px;border-top: 2px solid #00000047;"/>
		
		<div class="box-inner">
		<!-- Ajax Content Here -->
			
		</div>
		
		<div class="event_more_btn">
			<a href="javascript:event_ajax_load($('.inner-event'))" class="read-more-load">See more</a>
		</div>
		
		<!-- <div class="title-event">
			SEARCH
		</div>
		<hr style="margin-top: 10px;margin-bottom: 10px;border-top: 2px solid #00000047;"/>
		<p class="color-event no_margin">Âme are German duo Kristian Beyer & Frank Wiedemann, and Innervisions is their label.</p> -->
				
	</div>
</div>
<style>
	.fc-basic-view .fc-body .fc-row {
		min-height: 2em;
	}
	.fc-ltr .fc-basic-view .fc-day-top .fc-day-number {
		text-align: center;
		width: 40px;
		display: block;
		height: 40px;
		line-height: 40px;
		padding: 0px;
	}
	.fc-row.fc-week.fc-widget-content.fc-rigid{
		height: 40px !important;
	}
	.fc-scroller.fc-day-grid-container{
		height: auto !important;
	}
	.fc-toolbar.fc-header-toolbar{
		margin-bottom: 0px;
	}
	.fc-unthemed td.fc-today {
		background: #167ac6 !important;
		border-radius: 100%;
		width: 40px !important;
		height: 40px !important;
	}
	.fc th, .fc td{
		border-width: 0px;
	}
	td.fc-day-top.fc-wed.fc-today.fc-state-highlight .fc-day-number .span-class-date{
		color: #fff;
	}
	a[data-goto]:hover{
		text-decoration: none;
	}
</style>
<script src="<?=get_resource_url('assets/js/moment.js')?>"></script>
	<script src="<?=get_resource_url('assets/js/jquery.calendar.js')?>"></script>
	<script src="<?=get_resource_url('assets/js/fullcalendar.js')?>"></script>
<script type="text/javascript">
	$().FullCalendarExt({
		calendarSelector: '#calendar',
		lang: 'en'
	});
</script>
<script type="text/javascript">
var event_per_page = <?=EVENT_PER_PAGE?>;
</script>
			