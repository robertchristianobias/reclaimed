
    <div class="main-content mt140 bg-newsfeed">
		<div class="boxlatest">
			<div class="title" style="padding-top: 5px;">
				<h3 class="mg-t0"><span>LATEST</span></h3>
			</div>
			<div class="carousel-home-1">
				<div class="carousel">
					<div class="carousel-items">
						<?php
						// Featured
						$featured = modules::run('home/get_articles_featured');
						foreach ($featured as $result){ ?>
							<div class="slider-item">
							   <div class="slide-item-img col-md-9" style="background: url(<?php echo get_resource_url(substr($result->thumbnail, 1)) ?>) 100% 100%/cover "> 
								   <?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>">   
									<?php } ?>
									   <!--<img src="<?php echo get_resource_url(substr($result->thumbnail, 1)) ?>" alt="<?php echo $result->title ?>">-->
										</a>
										<?php
										$array_tags = explode(',', $result->tags);
										if(!empty($result->tags)){
											?>
											<ul class="listHomeBanner">
												<?php
												foreach($array_tags as $val){
												?>
													<li><a href="#"><span><?=$val?></span></a></li>
												<?php } ?>
											</ul>
										<?php 
										} 
										?>
								</div>
								<div class="text-content carousel-items-description col-md-3" style="word-spacing:inherit !important">
									<p class="hidden"><?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="tag-a-title1 p-fs-20">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="tag-a-title1 p-fs-20"> 
									<?php } ?>
									<?php echo $result->title ?></a></p>
									<p class="carousel-items-p-des" style="word-wrap: break-word;letter-spacing: inherit;word-spacing:inherit !important"><?php echo $result->description ?></p>
									<?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more md">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more md">   
									<?php } ?>Read more</a>
									<div class="clear-fix"></div>
									<!--<ul class="social">
										<li><a href="#"><span>182 Like</span></a></li>
										<li><a href="#"><span></span></a></li>
										<li><a href="#"><span>124 Comment</span></a></li>
									</ul>-->
								</div>
								<?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more lg">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more lg">   
									<?php } ?>Read more</a>
							</div>
						<?php } ?> 
					</div>
					<div class="carousel-prev">
						<i class="fa fa-caret-left" aria-hidden="true"></i>
					</div>
					<div class="carousel-next">
						<i class="fa fa-caret-right" aria-hidden="true"></i>
					</div>
				</div>
			</div>
			<div class="fonttext-info">
				<p>CLASSIC - FROM MAMBO TO HIP HOP - Since we are in the mood for hip hop documentaries, here's one about the Bronx </p>
				<div style="text-align: right;"><a class="font-read-more">+ read more</a></div>			
			</div>
			<hr style="margin-top: 10px;margin-bottom: 15px;"/>
		</div>	
		<div class="boxnewfeed">
			<div class="row">
				<div class="col-md-6 no-padding">
					<span class="font">NEWSFEED</span>
				</div>
				<div class="col-md-6 text_right_newsfeed no-padding">
					<ul class="tagul">
						<li>show<span class="tagspan">latest</span></li>
						<li>|</li>
						<li>most<span class="tagspan">popuplar</span></li>
					</ul>
				</div>
			</div>
			<hr/>
			<div class="row">
				<ul class="tagul tagpost">
					<li>8/2/2018</span></li>
					<li>4:32 PM</li>
					<li>posted by <span class="tagspan-name">popuplar popuplar</span></li>
				</ul>
				<p class="test-info">CLASSIC - FROM MAMBO TO HIP HOP - Since we are in the mood for hip hop documentaries, here's one about the Bronx</P>
				<div class="boxbanner">
					<div><img class="imgw100" src="https://source.unsplash.com/random/950x300/?1" alt=""></div>
				</div>	
				<div id="" class="box-content-newfeed-total-action fixspan">
					<span class="span-newfeed-total-like">
						<span class="feed-total-like-hot">0</span> Hot
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-like-cool">0</span> Cool
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-comment">0</span> Comment
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-share">0</span> Share
					</span> 
				</div>
			</div>
			<hr/>
			<div class="row">
				<ul class="tagul tagpost">
					<li>8/2/2018</span></li>
					<li>4:32 PM</li>
					<li>posted by <span class="tagspan-name">popuplar popuplar</span></li>
				</ul>
				<p class="test-info">CLASSIC - FROM MAMBO TO HIP HOP - Since we are in the mood for hip hop documentaries, here's one about the Bronx</P>
				<div class="boxbanner">
					<div><img class="imgw100" src="https://source.unsplash.com/random/950x300/?2" alt=""></div>
				</div>	
				<div id="" class="box-content-newfeed-total-action fixspan">
					<span class="span-newfeed-total-like">
						<span class="feed-total-like-hot">0</span> Hot
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-like-cool">0</span> Cool
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-comment">0</span> Comment
					</span> 
					<span class="span-newfeed-total-like">
						<span class="feed-total-share">0</span> Share
					</span> 
				</div>
			</div>
			<hr/>
		</div>
		<div class="clear-fix"></div>
	</div>    
    <div class="clearfix"></div>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
	
   