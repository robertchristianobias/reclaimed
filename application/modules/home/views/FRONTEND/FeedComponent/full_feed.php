<!DOCTYPE html>
<head>
  <title>Pusher Test</title>

  <link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
  <link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">

  <link href="<?=get_resource_url('assets/css/fstyle.css?ver=1.0')?>" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style.css?ver=1.0')?>" type="text/css" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style2.css?ver=1.0')?>" type="text/css" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style3.css?ver=1.0')?>" type="text/css" rel="stylesheet">

  <script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
  <style type="text/css">
    .form-group{
      margin-bottom:0px;
    }
    form.newfeed-form {
        position: relative;
    }
    #newFeedContent{
        max-width: 100%;
        color: #000;
    }
    #mainFeed {
      background-color: #888888;
    }

    .element-hidden {
      display: none;
    }
  </style>
</head>
<body>
<center style="color:white;">
<h4>Test UserID: <?=$user_id?> --- Test ChannelID: <?=$channel?></h4>
</center>
  <div class="main-content">
    <?php if($is_user_owner_feed) { ?>
      <!-- begin frm new feed -->
      <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
              <form id="newFeedForm" class="newfeed-form" method="POST" 
                      action="<?=$new_feed_url?>">
                  <textarea data-required="1" cols="" rows="8" 
                      name="newFeedContent" id="newFeedContent" required=""></textarea>
                  <input type="hidden" name="id_user_info" value="2">
                  <div class="clearfix"></div>
                  <div class="form-button-submit">
                      <button type="submit">Submit</button>
                      <button type="reset">Cancel</button>
                  </div>
              </form>
          </div>    
      </div>
      <!-- End frm new feed -->
    <?php } ?>
      <div id="listFeed" class="ajax-load-newfeed" 
        data-load-newfeed="http://redm.dev/en/ajax_loadnewfeed/2"
        data-submit-feed-url="<?=$new_feed_url?>"
        data-like-feed-url="<?=$like_url?>"
        data-submit-comment-url="<?=$new_comment_url?>"
        data-submit-reply-url="<?=$new_reply_url?>"
        data-load-feed-url="<?=$load_feed_url?>"
        data-load-comment-url="<?=$load_comment_url?>"
        data-load-reply-url="<?=$load_reply_url?>"
        data-load-num="<?=$load_num?>"
        data-has-paging="<?=$has_paging?>"
        data-current-user-avt="<?=$avatar_url?>">
        <?=( ! empty($listFeedHtml)) ? $listFeedHtml : ''?>
      </div>
      <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php if($has_paging) { ?>
        <center><span id="loadFeed">loadmore</span></center>
        <?php } ?>
      </div>

      <div id="clone_component" style="display: none; visibility: true;">
          <form id="commentFormPrototype" action="" method="POST" class="form-comment-newfeed">
              <div class="mg-t10 mg-b10">
                  <img class="comment-img" src="" alt="" width="60" height="60">
                  <input type="hidden" name="id_user_info" value="">
                  <input type="hidden" name="id_newfeed" value="">
                  <input type="hidden" name="feedBitId" value="">
                  <textarea id="commentContent" name="commentContent" placeholder="Write Comment..." required="" rows="4"></textarea>
              </div>
              <div class="comment-action">
                  <input type="reset" name="reset" value="Cancel">
                  <input type="submit" name="comment" value="Post">
                  <div class="clearfix"></div>
              </div>
          </form>
          <form id="replyFormPrototype" action="" method="POST" class="form-comment-newfeed">
              <div class="mg-t10 mg-b10">
                  <img class="comment-img" src="" alt="" width="60" height="60">
                  <input type="hidden" name="id_user_info" value="">
                  <input type="hidden" name="id_newfeed" value="">
                  <input type="hidden" name="feedBitId" value="">
                  <input type="hidden" name="commentBitId" value="">
                  <textarea id="replyContent" name="replyContent" placeholder="Write Comment..." required="" rows="4"></textarea>
              </div>
              <div class="comment-action">
                  <input type="reset" name="reset" value="Cancel">
                  <input type="submit" name="comment" value="Post">
                  <div class="clearfix"></div>
              </div>
          </form>
      </div>

  </div>
</body>

  <script src="https://js.pusher.com/4.0/pusher.min.js"></script>
  <script>
    $(document).ready(function(){
      const channelName = "<?=$channel?>";
      doReactFeed(channelName);
      submitData(channelName);
      loadData(channelName);
      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;
      const pusher = new Pusher("<?=PUSHER_KEY?>", {
        cluster: "<?=PUSHER_CLUSTER?>",
        encrypted: true
      });
      const channel = pusher.subscribe(channelName);
      listenerPusher(channel);
      updateTime();
    });

    function listenerPusher(channel) {
      channel.bind("newfeed", function(data) {
        var feedHtml = data.feedHtml;
        var mainFeed = $("#listFeed");
        var newestFeedElement = mainFeed.find("div.box-content-1.box-content-newfeed:first-child");
        if(newestFeedElement.size()) {
          newestFeedElement.before(feedHtml);
        } else {
          mainFeed.html(feedHtml);
        }
        updateTime(true);
      });
      channel.bind("likefeed", function(data) {
        var feedReactStatisticElement = $("#areaFeedReactStatistic"+data.feedBitId);
        feedReactStatisticElement.find(".feed-total-like").text(data.numLike);
      });

      channel.bind("newcomment", function(data) {
        var commentHtml = data.commentHtml;
        var areaListCommentElement = $("#areaListCommentId"+data.feedBitId);
        var newestCommentElement = areaListCommentElement.find("div.list_comment_newfeed_detail").last();
        if (newestCommentElement.size()) {
          newestCommentElement.after(commentHtml);
        } else {
          areaListCommentElement.html(commentHtml);
        }
        updateTime(true);
      });

      channel.bind("newreply", function(data) {
        var replyHtml = data.replyHtml;
        var areaListReplyElement = $("#areaListReplyId"+data.commentBitId);
        var newestReplyElement = areaListReplyElement.find('div.list_comment_newfeed_detail_reply_box').last();
        if (newestReplyElement.size()) {
          newestReplyElement.after(replyHtml);
        } else {
          areaListReplyElement.prepend(replyHtml);
        }
        updateTime(true);
      });

      channel.bind("updatefeed", function(data) {/*ToDo*/});

      channel.bind("deletefeed", function(data) {/*ToDo*/});

      channel.bind("updatecomment", function(data) {/*ToDo*/});

      channel.bind("deletecomment", function(data) {/*ToDo*/});

      channel.bind("updatereply", function(data) {/*ToDo*/});

      channel.bind("deletereply", function(data) {/*ToDo*/});

    }

    function loadData(channelName) {
      //do load more feed
      $("#loadFeed").on("click", function(event) {
        event.preventDefault();
        var submit_url = $("#listFeed").data('load-feed-url');
        var data = {
          channel: channelName,
          paging: $("#listFeed").attr('data-load-num')
        }
        $.ajax({
          type: "POST",
          url: submit_url,
          data: data,
          success: function(data){
            data = jQuery.parseJSON(data);
            if (data.status === 0) {
              console.log(data.message);
              return false;
            }
            var listFeedHtml = data.listFeedHtml;
            var mainFeed = $("#listFeed");
            var lastFeedElement = mainFeed.find("div.box-content-1.box-content-newfeed").last();
            lastFeedElement.after(listFeedHtml);
            $("#listFeed").attr('data-load-num', data.paging);
            if(!data.hasPaging) {
              $("#loadFeed").remove();
            }
            updateTime(true);
          }
        });
      });

      //do load more comment
      $("#listFeed").on("click", "span.show-comment-older", function(event){
        event.preventDefault();
        var feedElement = $(this).closest("div.box-content-1.box-content-newfeed");
        var submit_url = $("#listFeed").data('load-comment-url');
        var feedBitId = feedElement.data('feed-id');
        var paging = feedElement.attr('data-load-num');
        var data = {
          channel: channelName,
          feedBitId: feedBitId,
          paging: paging
        }
        $.ajax({
          type: "POST",
          url: submit_url,
          data: data,
          success: function(data){
            data = jQuery.parseJSON(data);
            if (data.status === 0) {
              console.log(data.message);
              return false;
            }
            var listCommentHtml = data.listCommentHtml;
            var areaListCommentElement = $("#areaListCommentId"+data.feedBitId);
            var firstCommentElement = areaListCommentElement.find("div.list_comment_newfeed_detail").first();
            firstCommentElement.before(data.listCommentHtml);
            feedElement.attr('data-load-num', data.paging);
            if(!data.hasPaging) {
              feedElement.find("span.show-comment-older").remove();
            }
            updateTime(true);
          }
        });
      });

      //do load more reply
      $("#listFeed").on("click", "span.show-reply-older", function(event){
        event.preventDefault();
        var commentElement = $(this).closest("div.list_comment_newfeed_detail");
        var submit_url = $("#listFeed").data('load-reply-url');
        var feedBitId = commentElement.data('feed-id');
        var commentBitId = commentElement.data('comment-id');
        var paging = commentElement.attr('data-load-num');
        var data = {
          channel: channelName,
          feedBitId: feedBitId,
          commentBitId: commentBitId,
          paging: paging
        }
        $.ajax({
          type: "POST",
          url: submit_url,
          data: data,
          success: function(data){
            data = jQuery.parseJSON(data);
            if (data.status === 0) {
              console.log(data.message);
              return false;
            }
            var listReplyHtml = data.listReplyHtml;
            var areaListReplyElement = $("#areaListReplyId"+data.commentBitId);
            var firstReplyElement = areaListReplyElement.find("div.list_comment_newfeed_detail_reply_box").first();
            firstReplyElement.before(data.listReplyHtml);
            commentElement.attr('data-load-num', data.paging);
            if(!data.hasPaging) {
              commentElement.find("span.show-reply-older").remove();
            }
            updateTime(true);
          }
        });
      });

    }

    function submitData(channelName) {
      //do submit new feed
      $("#newFeedForm").on("submit", function(event){
        event.preventDefault();
        var newFeedContentElement = $(this).find("#newFeedContent");
        var feedData = {
          channel: channelName,
          feedContent: newFeedContentElement.val(),
        };
        $.ajax({
          type: "POST",
          url: $(this).attr("action"),
          data: feedData,
          success: function(data){
            newFeedContentElement.val("");
          }
        });
      });

      //do submit comment feed
      $("#listFeed").on("submit", "form[id^='commentForm']", function(event) {
        event.preventDefault();
        var submit_comment_url  = $("#listFeed").data("submit-comment-url");
        var commentContentElement = $(this).find("textarea");
        var commentData = {
            channel: channelName,
            feedBitId: $(this).find(":input[name='feedBitId']").val(),
            commentContent: commentContentElement.val(),
        };
        $.ajax({
          type: "POST",
          url: submit_comment_url,
          data: commentData,
          success: function(data){
            commentContentElement.val('');
          }
        });
      });

      //do submit reply comment
      $("#listFeed").on("submit", "form[id^='replyForm']", function(event) {
        event.preventDefault();
        var submit_reply_url = $("#listFeed").data("submit-reply-url");
        var replyContentElement = $(this).find("textarea");
        var replyData = {
          channel: channelName,
          feedBitId: $(this).find(":input[name='feedBitId']").val(),
          commentBitId: $(this).find(":input[name='commentBitId']").val(),
          replyContent: replyContentElement.val()
        };
        $.ajax({
          type: "POST",
          url: submit_reply_url,
          data: replyData,
          success: function(data){
            console.log(data);
            replyContentElement.val('');
          }
        });
      });
      
    }

    function doReactFeed(channelName) {
        $("#listFeed").on("click", "div.box-content-newfeed-action > span", function(event){
          var type = $(this).data("action-type");
          var feedBitId = $(this).closest("div.box-content-1.box-content-newfeed").data("feed-id");

          switch(type) {
            case "like": doLikeFeed(channelName, feedBitId);
              break;
            case "comment": showCommentForm(feedBitId);
              break;
            case "share": doShareFeed(feedBitId);
              break;
          }
        });
        $("#listFeed").on("click", ".list_comment_newfeed span.span-reply", function(event) {
          var feedBitId = $(this).closest("div.list_comment_newfeed_detail").data("feed-id");
          var commentBitId = $(this).closest("div.list_comment_newfeed_detail").data("comment-id");
          showReplyForm(feedBitId, commentBitId);
        });
    }

    function doLikeFeed(channelName, feedBitId) {
      var submit_like_url  = $("#listFeed").data('like-feed-url');
      var likeData = {
          channel: channelName,
          feedBitId: feedBitId
      };
      $.ajax({
        type: "POST",
        url: submit_like_url,
        data: likeData,
        success: function(data){
          console.log(data);
        }
      });
    }

    function showCommentForm(feedBitId) {
      var avtUrl = $("#listFeed").data('current-user-avt');
      var areaCommentFormElement = $("#areaCommentFormId"+feedBitId);
      var idName = 'commentForm'+feedBitId;
      var formCommentElement = areaCommentFormElement.find("#"+idName);
      if (formCommentElement.length === 0) {
          var element = $("#commentFormPrototype").clone();
          element.attr("id", idName);
          areaCommentFormElement.html(element);
          formCommentElement = element;
      }
      formCommentElement.find('img.comment-img').attr('src', avtUrl);
      formCommentElement.find(":input[name='feedBitId']").val(feedBitId);
      if (areaCommentFormElement.hasClass('element-hidden')) {
        areaCommentFormElement.removeClass('element-hidden');
      }
      formCommentElement.find('textarea').focus();
    }

    function showReplyForm(feedBitId, commentBitId) {
      var avtUrl = $("#listFeed").data('current-user-avt');
      var areaReplyFormElement = $("#areaReplyFormId"+commentBitId);
      var idName = 'replyForm'+commentBitId;
      var formReplyElement = areaReplyFormElement.find("#"+idName);
      if (formReplyElement.length === 0) {
          var element = $("#replyFormPrototype").clone();
          element.attr("id", idName);
          areaReplyFormElement.html(element);
          formReplyElement = element;
      }
      formReplyElement.find('img.comment-img').attr('src', avtUrl);
      formReplyElement.find(":input[name='feedBitId']").val(feedBitId);
      formReplyElement.find(":input[name='commentBitId']").val(commentBitId);
      if (areaReplyFormElement.hasClass('element-hidden')) {
        areaReplyFormElement.removeClass('element-hidden');
      }
      formReplyElement.find('textarea').focus();
    }

    function doShareFeed(feedBitId) {

    }

    function renderCommentFrm() {

    }

    function renderReplyFrm() {

    }

    function updateTime(isAfterInsert) {
      var listTimeElement = $("#listFeed").find("span.date-time-component");
      listTimeElement.each(function(index){
        $(this).html(caclulateTime($(this).data("date-time")));
      });
      if(isAfterInsert === true) {
        return true;
      } else {
        setTimeout(updateTime, 60*1000);
      }
    }

    function caclulateTime(time) {
      var result = '';
      var datetime = new Date(time);
      var now = new Date();
      var rangeTime = (now - datetime) / 1000;
      var rangeHour = Math.floor(rangeTime / 3600);
      if(rangeHour === 0) {
        var rangeMinus = Math.floor(rangeTime / 60);
        if(rangeMinus === 0) {
          result = 'Justnow';
        } else {
          result = rangeMinus + (rangeMinus === 1 ? ' minute' : ' minutes');
        }
      } 
      else if(rangeHour > 23) {
        var hours   = datetime.getHours() < 10 ? '0' + datetime.getHours() : datetime.getHours();
        var minutes = datetime.getMinutes() < 10 ? '0' + datetime.getMinutes() : datetime.getMinutes();
        var month   = datetime.getMonth() < 9 ? '0' + (datetime.getMonth() + 1) : (datetime.getMonth() + 1);
        var date    = datetime.getDate() < 10 ? '0' + datetime.getDate() : datetime.getDate();
        result = datetime.getFullYear() + '-' + month + '-' + date + ' ' + hours + ':' + minutes;
      } 
      else {
        result = rangeHour + (rangeHour === 1 ? ' hour' : ' hours');
      }
      return result;
    }
  </script>