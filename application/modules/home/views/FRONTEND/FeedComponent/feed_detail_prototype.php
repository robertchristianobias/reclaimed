<?php 
    if(!empty($userData)) {  
        $username = $userData ['username'];
        $avatar_url = get_resource_url($userData ['thumbnail']);
        $link_url_redm = $userData ['link_url_redm'];
		$url = $userData ['url'];
		$image = $userData ['image'];
        $video = $userData ['video'];
        $title = $userData ['title'];
        $description = $userData ['description'];
    } else {
        $username = $feedData ['username'];
        $avatar_url = get_resource_url($feedData ['thumbnail']);
        $link_url_redm = $feedData ['link_url_redm'];
		$url = $feedData ['url'];
		$image = $feedData ['image'];
        $video = $feedData ['video'];
        $title = $feedData ['title'];
        $description = $feedData ['description'];
    }

    $feed_id = $feedData ['feed_bit_id'];
    $created = $feedData ['created'];
    $content = $feedData ['content'];
    $status = $feedData ['status'];

    $load_num = isset($feedData ['load_num']) ? $feedData ['load_num'] : 1;
    $has_paging = isset($feedData ['has_paging']) ? $feedData ['has_paging'] : 0;

    //generate profile url
    if(empty($link_url_redm)) {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . '?id=' . $user_bit_id;
    } else {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . $link_url_redm;
    }
    
    $is_login = ( isset($feedData ['user_is_login']) ) ? $feedData ['user_is_login'] : TRUE;
    
    $num_like = $feedData ['total_like'];
    $num_comment = $feedData ['total_comment'];
    $num_share = $feedData ['total_share'];
    
    $name_feed_id = 'feed' . $feed_id;
    $area_comment_form_id = 'areaCommentFormId' . $feed_id;
    $area_list_comment_id = 'areaListCommentId' . $feed_id;
    $area_feed_react_statistic_id = 'areaFeedReactStatistic' . $feed_id;

    $list_comment_html = ( ! empty($feedData ['listCommentHtml'])) ? $feedData ['listCommentHtml'] : '';
?>
<div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12" 
    id="<?=$name_feed_id?>"
    data-channel-id="" 
    data-feed-id="<?=$feed_id?>" 
    data-load-num="<?=$load_num?>" 
    data-has-paging="<?=$has_paging?>">
    <div class="box-content-newfeed-header">
        <div class="newfeed-avt">
            <img src="<?=$avatar_url?>" alt="<?=$username?>" width="60" height="60">
        </div>
        <div class="newfeed-name">
            <p class="mg-b0"><?=$username?></p>
            <p>
                <span class="date-time-component" data-date-time="<?=date("Y-m-d h:i:s",$created)?>"><?=date("Y-m-d h:i:s",$created)?></span>
                <?php if($status == '1'){ ?> 
                     <i class="fa fa-globe fa-newfeed" aria-hidden="true"></i>
                <?php } else { ?>
                    <!-- private -->
                <?php } ?>
            </p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="box-content-newfeed-content">
        <?=strip_tags($content)?>
		<div class="content_feed_url">
			<div class="border2px">
				<?php
				if(!empty($url)){
				?>
				<a href="<?=$url?>" class="link-url">
                    <div class="container">
    					<div class="carousel-home-1">
    						<div class="carousel">
    							<div class="carousel-items">
    								<div class="slider-item">
    								<?php
    								if($video){
    								?>
    									<embed src=<?=$video?> width=500 height=315></embed>
    								<?php
    								}
    								else{
    								?>
    									<img class="image_url" src="<?=$image?>">
    								<?php
    								}
    								?>
    								</div>
    							</div>
    							<div class="title-url"><?=strip_tags($title)?></div>
    							<div class="text-description3">
    								<p><?=strip_tags($description)?></p>
    								<div class="clear-fix"></div>
    							</div>
    						</div>
    					</div>
                    </div>
				</a>
				<?php
				}
				?>
			</div>
		</div>
    </div>
    <div class="clearfix"></div>
    <div id="<?=$area_feed_react_statistic_id?>" class="box-content-newfeed-total-action">
        <span class="span-newfeed-total-like">
            <span class="feed-total-like"><?=$num_like?></span> Like
        </span> 
        <span class="span-newfeed-total-like">
            <span class="feed-total-comment"><?=$num_comment?></span> Comment
        </span> 
        <span class="span-newfeed-total-like">
            <span class="feed-total-share"><?=$num_share?></span> Share
        </span> 
    </div>
    <?php if($is_login) { ?>
    <div class="box-content-newfeed-action">
        <span class="span-newfeed-like" data-action-type="like">
            <i class="fa fa-thumbs-up" aria-hidden="true"></i> Like
        </span>
        <span class="span-newfeed-comment" data-action-type="comment">
            <i class="fa fa-comment" aria-hidden="true"></i> Comment
        </span>
        <span class="span-newfeed-share" data-action-type="share">
            <i class="fa fa-share" aria-hidden="true"></i> Share
        </span>
    </div>
    <?php } ?>
    <div class="clear-fix"></div>
    <!-- write comment -->
    <div id="<?=$area_comment_form_id?>" class="comment-write comment-write-profile mg-t20 element-hidden">
    </div>
    <div class="clear-fix"></div>
    <!-- end write comment -->

    <!-- list comment -->
    <div class="clear-fix"></div>
    <div id="<?=$area_list_comment_id?>" class="list_comment_newfeed list_comment_newfeed_2">
        <?php if ($has_paging) {?> 
        <div class="list_comment_newfeed_detail_showmore mg-t20" >
            <center>
                <span class="show-comment-older">Show older</span>
            </center>
        </div>
        <?php } ?>
        <?=$list_comment_html?>
    </div>
    <div class="clear-fix"></div>
    <!-- end list comment -->
</div>