
<?php 
    if( ! empty($userData)) {
        $username = $userData ['username'];
        $avatar_url = get_resource_url($userData ['thumbnail']);
        $link_url_redm = $userData ['link_url_redm'];
        $user_bit_id = $userData ['user_bit_id'];
    } else {
        $username = $commentData ['username'];
        $avatar_url = get_resource_url($commentData ['thumbnail']);
        $link_url_redm = $commentData ['link_url_redm'];
        $user_bit_id = $commentData ['user_bit_id'];
    }
    $feed_id = $commentData ['feed_bit_id'];
    $comment_id = $commentData ['comment_bit_id'];
    $created = $commentData ['created'];
    $comment = $commentData ['comment'];
    // $tags = $commentData ['tags'];
	// $link_hashtag = convert_clickable_links($tags);
    $status = $commentData ['status'];

    $load_num = isset($commentData ['load_num']) ? $commentData ['load_num'] : 1;
    $has_paging = isset($commentData ['has_paging']) ? $commentData ['has_paging'] : 0;
    
    //generate profile url
    if(empty($link_url_redm)) {
        $profile_url = "#";
    } else {
        $profile_url = PATH_URL .'en/profile/' . $link_url_redm;
    }
    
    $is_login = ( isset($commentData ['user_is_login']) ) ? $commentData ['user_is_login'] : TRUE;

    $name_comment_id = 'comment' . $comment_id;
    $area_reply_form_id = 'areaReplyFormId' . $comment_id;
    $area_list_reply_id = 'areaListReplyId' . $comment_id;

    $list_reply_html = ( ! empty($commentData ['listReplyHtml'])) ? $commentData ['listReplyHtml'] : '';
?>
<div class="list_comment_newfeed_detail mg-t20" 
    id="<?=$name_comment_id?>" 
    data-channel-id="" 
    data-feed-id="<?=$feed_id?>" 
    data-comment-id="<?=$comment_id?>" 
    data-load-num="<?=$load_num?>">
    <div class="col-sm-1 padding-left-0">
        <a href="<?=$profile_url?>">
            <img class="comment-img" src="<?=$avatar_url?>" alt="<?=$username?>" width="60" height="60">
        </a>
    </div>
    <div class="col-sm-11 padding-left-0">
        <div class="comment-name" style="padding-left: 20px">
            <a href="<?=$profile_url?>"><?=$username?></a>
            <span class="span-time date-time-component" data-date-time="<?=$created?>"><?=$created?></span>
            <?php if ($is_login) { ?>
                <span class="span-reply" title="Reply" 
                    data-feed-id="<?=$feed_id?>" 
                    data-comment-id="<?=$comment_id?>">
                    Reply
                </span>
            <?php } ?>
        </div>
        <div class="comment-content" style="padding-left: 20px">
            <?=nl2br(htmlspecialchars($comment))?>
        </div>
        <div class="" style="padding-left:20px">
			<div class="clearfix"></div>
			<div id="" class="box-content-newfeed-total-action">
				<span class="span-newfeed-total-like">
					<span class="feed-total-like-comment-hot"></span> Hot
				</span> 
				<span class="span-newfeed-total-like">
					<span class="feed-total-like-comment-cool"></span> Cool
				</span> 
			</div>
             <div class="box-content-newfeed-action">
               <span class="span-newfeed-like" data-action-type="like-comment-hot">
                    <img src="<?php echo get_resource_url('/assets/images/icon/fire.png')?>">Hot
                </span>        
                <span class="span-newfeed-like" data-action-type="like-comment-cool">
                    <img src="<?php echo get_resource_url('/assets/images/icon/cold.png') ?>">Cool
                </span>
             </div>   
        </div>
    </div>
    <div class="clear-fix"></div>
    <!-- list child comment -->
    <div id="<?=$area_list_reply_id?>" class="list_comment_newfeed_detail_reply">
        <?php if ($has_paging) {?> 
        <div class="list_comment_newfeed_detail_reply_box_showmore mg-t20" >
            <center>
                <span class="show-reply-older">Show older</span>
            </center>
        </div>
        <?php } ?>
        <?=$list_reply_html?>
        <!-- Comment Reply Newfeed  -->
        <div class="list_comment_newfeed_detail_reply_comment active">
            <div class="clear-fix"></div>
            <div id="<?=$area_reply_form_id?>" class="comment-write comment-write-profile mg-t20">
            </div>
            <div class="clear-fix"></div>
        </div>
        <!-- End Comment Reply Newfeed -->
    </div>
    <!-- End list_comment_newfeed_detail_reply -->
</div>
<div class="clear-fix"></div>