

<!-- <script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script> -->
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
<script type="text/javascript">
        // tinymce.init({
        //     selector: '#content',
        //     // plugins: "code, link, image, lists, preview, textcolor, pix_embed_online_media, responsivefilemanager",
        //     // toolbar: ['undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect forecolor backcolor | bullist numlist outdent indent | link image | print preview media fullpage removeformat','pix_embed_online_media | responsivefilemanager'],
        //     // external_filemanager_path:"../filemanager/",
        //     // filemanager_title:"Responsive Filemanager" ,
        //     // relative_urls: false,
        //     // // external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
        //     //   // toolbar: "anchor",
        //     //   // menubar: "insert"
        // });
function save(){
    console.log('save');
    var content_html = tinyMCE.get('content').getContent();
        $('#content').val(content_html);
    var options = {
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
    };
}

</script>

<style>
.form-group{
      margin-bottom:0px;
    }
    form.newfeed-form {
        position: relative;
    }
    #newFeedContent{
        max-width: 100%;
        color: #000;
    }
    #mainFeed {
      background-color: #888888;
    }

    .element-hidden {
      display: none;
    }
</style>
 <div class="w-wapper mt-25 container">
    <div id="profile-info">
        <div class="main-contact-profile">
            <div class="contact-profile">
                <div class="row">
                 
        <div class="clearfix"></div>
        <div class="main-content">
            <div class="clearfix-mt40"></div>
            
            <!-- list feed -->
              <!-- begin frm new feed -->
              <!-- End frm new feed -->
              <div id="listFeed" class="ajax-load-newfeed" 
                data-load-newfeed="http://redm.dev/en/ajax_loadnewfeed/2"
                data-submit-feed-url="<?=$new_feed_url_home?>"
                data-like-feed-url-hot="<?=$like_url_hot?>"
                data-like-feed-url-cool="<?=$like_url_cool?>"
                data-share-feed-url="<?=$share_url?>"
                data-submit-comment-url="<?=$new_comment_url?>"
                data-submit-reply-url="<?=$new_reply_url?>"
                data-load-feed-url="<?=$load_feed_url?>"
                data-load-comment-url="<?=$load_comment_url?>"
                data-load-reply-url="<?=$load_reply_url?>"
                data-load-num="<?=$load_num?>"
                data-has-paging="<?=$has_paging?>"
                data-current-user-avt="<?=$avatar_url?>">
                <?=( ! empty($listFeedHtml)) ? $listFeedHtml : ''?>
              </div>
              <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php if($has_paging) { ?>
                <center><span id="loadFeed">loadmore</span></center>
                <?php } ?>
              </div>

              <div id="clone_component" style="display: none; visibility: true;">
                  <form id="commentFormPrototype" action="" method="POST" class="form-comment-newfeed">
                      <div class="mg-t10 mg-b10">
                          <img class="comment-img" src="" alt="" width="60" height="60">
                          <input type="hidden" name="id_user_info" value="">
                          <input type="hidden" name="id_newfeed" value="">
                          <input type="hidden" name="feedBitId" value="">
                          <textarea id="commentContent" name="commentContent" placeholder="Write Comment..." required="" rows="4"></textarea>
                      </div>
                      <div class="comment-action">
                          <input type="reset" name="reset" value="Cancel">
                          <input type="submit" name="comment" value="Post">
                          <div class="clearfix"></div>
                      </div>
                  </form>
                  <form id="replyFormPrototype" action="" method="POST" class="form-comment-newfeed">
                      <div class="mg-t10 mg-b10">
                          <img class="comment-img" src="" alt="" width="60" height="60">
                          <input type="hidden" name="id_user_info" value="">
                          <input type="hidden" name="id_newfeed" value="">
                          <input type="hidden" name="feedBitId" value="">
                          <input type="hidden" name="commentBitId" value="">
                          <textarea id="replyContent" name="replyContent" placeholder="Write Comment..." required="" rows="4"></textarea>
                      </div>
                      <div class="comment-action">
                          <input type="reset" name="reset" value="Cancel">
                          <input type="submit" name="comment" value="Post">
                          <div class="clearfix"></div>
                      </div>
                  </form>
              </div>
            <!-- End list feed -->
        </div>
    </div>
</div>
<div class="clearfix"></div>

<script src="https://js.pusher.com/4.0/pusher.min.js"></script>
<script id="script_set_login" type="text/javascript">
    $(document).ready(function(){
        var isLogin = "<?=(!empty($is_login)&&$is_login === TRUE) ? '1' : '0'?>";
        localStorage.setItem("islogin",(isLogin === '1') ? true : false);
        console.log(localStorage.getItem("islogin"));
        setTimeout(function(){ 
            $("#script_set_login").remove();
        }, 200);
    });
</script>

<script>
  $(document).ready(function(){
    const channelName = "<?=$channel?>";
    doReactFeed(channelName);
    submitData(channelName);
    loadData(channelName);
    const pusher = new Pusher("<?=PUSHER_KEY?>", {
      // authTransport: 'ajax',
      // authEndpoint: '', //config url
      cluster: "<?=PUSHER_CLUSTER?>",
      encrypted: true,
    });
    const channel = pusher.subscribe(channelName);
    listenerPusher(channel);
    updateTime();
  });

  function listenerPusher(channel) {
    channel.bind("newfeed", function(data) {
      var feedHtml = renderHtmlResult(data.feedHtml);
      var mainFeed = $("#listFeed");
      var newestFeedElement = mainFeed.find("div.box-content-1.box-content-newfeed:first-child");
      if(newestFeedElement.size()) {
        newestFeedElement.before(feedHtml);
      } else {
        mainFeed.html(feedHtml);
      }
      updateTime(true);
    });
	
    channel.bind("likefeedhot", function(data) {
      var feedReactStatisticElement = $("#areaFeedReactStatistic"+data.feedBitId);
      feedReactStatisticElement.find(".feed-total-like-hot").text(data.numLike);
    });
	
	channel.bind("likefeedcool", function(data) {
      var feedReactStatisticElement = $("#areaFeedReactStatistic"+data.feedBitId);
      feedReactStatisticElement.find(".feed-total-like-cool").text(data.numLike);
    });
	
	channel.bind("sharefeed", function(data) {
      var feedReactStatisticElement = $("#areaFeedReactStatistic"+data.feedBitId);
      feedReactStatisticElement.find(".feed-total-share").text(data.numShare);
    });

    channel.bind("newcomment", function(data) {
      var commentHtml = renderHtmlResult(data.commentHtml);
      var areaListCommentElement = $("#areaListCommentId"+data.feedBitId);
      var newestCommentElement = areaListCommentElement.find("div.list_comment_newfeed_detail").last();
      if (newestCommentElement.size()) {
        newestCommentElement.after(commentHtml);
      } else {
        areaListCommentElement.html(commentHtml);
      }
      updateTotalCommentOfFeed(data.feedBitId, data.totalComment);
      updateTime(true);
    });

    channel.bind("newreply", function(data) {
      var replyHtml = renderHtmlResult(data.replyHtml);
      var areaListReplyElement = $("#areaListReplyId"+data.commentBitId);
      var newestReplyElement = areaListReplyElement.find('div.list_comment_newfeed_detail_reply_box').last();
      if (newestReplyElement.size()) {
        newestReplyElement.after(replyHtml);
      } else {
        areaListReplyElement.prepend(replyHtml);
      }
      updateTime(true);
    });

    channel.bind("updatefeed", function(data) {/*ToDo*/});

    channel.bind("deletefeed", function(data) {/*ToDo*/});

    channel.bind("updatecomment", function(data) {/*ToDo*/});

    channel.bind("deletecomment", function(data) {/*ToDo*/});

    channel.bind("updatereply", function(data) {/*ToDo*/});

    channel.bind("deletereply", function(data) {/*ToDo*/});

  }

  function loadData(channelName) {
    //do load more feed
    $("#loadFeed").on("click", function(event) {
      event.preventDefault();
      var submit_url = $("#listFeed").data('load-feed-url');
      var data = {
        channel: channelName,
        paging: $("#listFeed").attr('data-load-num')
      }
      $.ajax({
        type: "POST",
        url: submit_url,
        data: data,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          var listFeedHtml = data.listFeedHtml;
          var mainFeed = $("#listFeed");
          var lastFeedElement = mainFeed.find("div.box-content-1.box-content-newfeed").last();
          lastFeedElement.after(listFeedHtml);
          $("#listFeed").attr('data-load-num', data.paging);
          if(!data.hasPaging) {
            $("#loadFeed").remove();
          }
          updateTime(true);
        }
      });
    });

    //do load more comment
    $("#listFeed").on("click", "span.show-comment-older", function(event){
      event.preventDefault();
      var feedElement = $(this).closest("div.box-content-1.box-content-newfeed");
      var submit_url = $("#listFeed").data('load-comment-url');
      var feedBitId = feedElement.data('feed-id');
      var paging = feedElement.attr('data-load-num');
      var data = {
        channel: channelName,
        feedBitId: feedBitId,
        paging: paging
      }
      $.ajax({
        type: "POST",
        url: submit_url,
        data: data,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          var listCommentHtml = data.listCommentHtml;
          var areaListCommentElement = $("#areaListCommentId"+data.feedBitId);
          var firstCommentElement = areaListCommentElement.find("div.list_comment_newfeed_detail").first();
          firstCommentElement.before(data.listCommentHtml);
          feedElement.attr('data-load-num', data.paging);
          if(!data.hasPaging) {
            feedElement.find("span.show-comment-older").remove();
          }
          updateTime(true);
        }
      });
    });

    //do load more reply
    $("#listFeed").on("click", "span.show-reply-older", function(event){
      event.preventDefault();
      var commentElement = $(this).closest("div.list_comment_newfeed_detail");
      var submit_url = $("#listFeed").data('load-reply-url');
      var feedBitId = commentElement.data('feed-id');
      var commentBitId = commentElement.data('comment-id');
      var paging = commentElement.attr('data-load-num');
      var data = {
        channel: channelName,
        feedBitId: feedBitId,
        commentBitId: commentBitId,
        paging: paging
      }
      $.ajax({
        type: "POST",
        url: submit_url,
        data: data,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          var listReplyHtml = data.listReplyHtml;
          var areaListReplyElement = $("#areaListReplyId"+data.commentBitId);
          var firstReplyElement = areaListReplyElement.find("div.list_comment_newfeed_detail_reply_box").first();
          firstReplyElement.before(data.listReplyHtml);
          commentElement.attr('data-load-num', data.paging);
          if(!data.hasPaging) {
            commentElement.find("span.show-reply-older").remove();
          }
          updateTime(true);
        }
      });
    });

  }

  function submitData(channelName) {
    //do submit new feed
    $("#newFeedForm").on("submit", function(event){
      event.preventDefault();
      var newFeedContentElement = $(this).find("#newFeedContent");
      var feedData = {
        channel: channelName,
        feedContent: newFeedContentElement.val(),
      };
      $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: feedData,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          newFeedContentElement.val("");
        }
      });
    });

    //do submit comment feed
    $("#listFeed").on("submit", "form[id^='commentForm']", function(event) {
      event.preventDefault();
      var submit_comment_url  = $("#listFeed").data("submit-comment-url");
      var commentContentElement = $(this).find("textarea");
      var commentData = {
          channel: channelName,
          feedBitId: $(this).find(":input[name='feedBitId']").val(),
          commentContent: commentContentElement.val(),
      };
      $.ajax({
        type: "POST",
        url: submit_comment_url,
        data: commentData,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          commentContentElement.val('');
        }
      });
    });

    //do submit reply comment
    $("#listFeed").on("submit", "form[id^='replyForm']", function(event) {
      event.preventDefault();
      var submit_reply_url = $("#listFeed").data("submit-reply-url");
      var replyContentElement = $(this).find("textarea");
      var replyData = {
        channel: channelName,
        feedBitId: $(this).find(":input[name='feedBitId']").val(),
        commentBitId: $(this).find(":input[name='commentBitId']").val(),
        replyContent: replyContentElement.val()
      };
      $.ajax({
        type: "POST",
        url: submit_reply_url,
        data: replyData,
        success: function(data){
          data = jQuery.parseJSON(data);
          if (data.status === 0) {
            console.log('Error', data.message);
            return false;
          }
          replyContentElement.val('');
        }
      });
    });
    
  }

  function updateTotalCommentOfFeed(feedBitId, totalComment) {
    if(totalComment > 0) {  
      var feedReactStatisticElement = $("#areaFeedReactStatistic"+feedBitId);
      feedReactStatisticElement.find(".feed-total-comment").text(totalComment);
    }
  }

  function doReactFeed(channelName) {
      $("#listFeed").on("click", "div.box-content-newfeed-action > span", function(event){
        var type = $(this).data("action-type");
        var feedBitId = $(this).closest("div.box-content-1.box-content-newfeed").data("feed-id");

        switch(type) {
          case "like-hot": doLikeFeedHot(channelName, feedBitId);
            break;
		  case "like-cool": doLikeFeedCool(channelName, feedBitId);
            break;
          case "comment": showCommentForm(feedBitId);
            break;
          case "share": doShareFeed(channelName, feedBitId);
            break;
        }
      });
      $("#listFeed").on("click", ".list_comment_newfeed span.span-reply", function(event) {
        var feedBitId = $(this).closest("div.list_comment_newfeed_detail").data("feed-id");
        var commentBitId = $(this).closest("div.list_comment_newfeed_detail").data("comment-id");
        showReplyForm(feedBitId, commentBitId);
      });
  }

  function doLikeFeedHot(channelName, feedBitId) {
    var submit_like_url  = $("#listFeed").data('like-feed-url-hot');
    var likeData = {
        channel: channelName,
        feedBitId: feedBitId
    };
    $.ajax({
      type: "POST",
      url: submit_like_url,
      data: likeData,
      success: function(data){
        console.log(data);
      }
    });
  }
  
  function doLikeFeedCool(channelName, feedBitId) {
    var submit_like_url  = $("#listFeed").data('like-feed-url-cool');
    var likeData = {
        channel: channelName,
        feedBitId: feedBitId
    };
    $.ajax({
      type: "POST",
      url: submit_like_url,
      data: likeData,
      success: function(data){
        console.log(data);
      }
    });
  }

  function doShareFeed(channelName, feedBitId) {
    var submit_share_url  = $("#listFeed").data('share-feed-url');
    var shareData = {
		channel: channelName,
        feedBitId: feedBitId
    };
    $.ajax({
      type: "POST",
      url: submit_share_url,
      data: shareData,
      success: function(data){
		var myobj = JSON.parse(data);
        console.log(data);
		$(".modal-body").append('<center style="font-size:18px">' + myobj.message + '</center>');
		$('.modal-body').removeClass();
		$("#myModal").modal();
      }
    });
  }

  function showCommentForm(feedBitId) {
    var avtUrl = $("#listFeed").data('current-user-avt');
    var areaCommentFormElement = $("#areaCommentFormId"+feedBitId);
    var idName = 'commentForm'+feedBitId;
    var formCommentElement = areaCommentFormElement.find("#"+idName);
    if (formCommentElement.length === 0) {
        var element = $("#commentFormPrototype").clone();
        element.attr("id", idName);
        areaCommentFormElement.html(element);
        formCommentElement = element;
    }
    formCommentElement.find('img.comment-img').attr('src', avtUrl);
    formCommentElement.find(":input[name='feedBitId']").val(feedBitId);
    if (areaCommentFormElement.hasClass('element-hidden')) {
      areaCommentFormElement.removeClass('element-hidden');
    }
    formCommentElement.find('textarea').focus();
  }

  function showReplyForm(feedBitId, commentBitId) {
    var avtUrl = $("#listFeed").data('current-user-avt');
    var areaReplyFormElement = $("#areaReplyFormId"+commentBitId);
    var idName = 'replyForm'+commentBitId;
    var formReplyElement = areaReplyFormElement.find("#"+idName);
    if (formReplyElement.length === 0) {
        var element = $("#replyFormPrototype").clone();
        element.attr("id", idName);
        areaReplyFormElement.html(element);
        formReplyElement = element;
    }
    formReplyElement.find('img.comment-img').attr('src', avtUrl);
    formReplyElement.find(":input[name='feedBitId']").val(feedBitId);
    formReplyElement.find(":input[name='commentBitId']").val(commentBitId);
    if (areaReplyFormElement.hasClass('element-hidden')) {
      areaReplyFormElement.removeClass('element-hidden');
    }
    formReplyElement.find('textarea').focus();
  }

  function renderCommentFrm() {

  }

  function renderReplyFrm() {

  }

  function updateTime(isAfterInsert) {
    var listTimeElement = $("#listFeed").find("span.date-time-component");
    listTimeElement.each(function(index){
      $(this).html(caclulateTime($(this).data("date-time")));
    });
    if(isAfterInsert === true) {
      return true;
    } else {
      setTimeout(updateTime, 60*1000);
    }
  }

  function caclulateTime(time) {
    var result = '';
    var datetime = new Date(time);
    var now = new Date();
    var rangeTime = (now - datetime) / 1000;
    var rangeHour = Math.floor(rangeTime / 3600);
    if(rangeHour === 0 || rangeHour < 0) {
      var rangeMinus = Math.floor(rangeTime / 60);
      if(rangeMinus === 0 || rangeMinus < 0) {
        result = 'Just now';
      } else {
        result = rangeMinus + (rangeMinus === 1 ? ' minute' : ' minutes');
      }
    } 
    else if(rangeHour > 23) {
      var hours   = datetime.getHours() < 10 ? '0' + datetime.getHours() : datetime.getHours();
      var minutes = datetime.getMinutes() < 10 ? '0' + datetime.getMinutes() : datetime.getMinutes();
      var month   = datetime.getMonth() < 9 ? '0' + (datetime.getMonth() + 1) : (datetime.getMonth() + 1);
      var date    = datetime.getDate() < 10 ? '0' + datetime.getDate() : datetime.getDate();
      result = datetime.getFullYear() + '-' + month + '-' + date + ' ' + hours + ':' + minutes;
    } 
    else {
      result = rangeHour + (rangeHour === 1 ? ' hour' : ' hours');
    }
    return result;
  }

  function renderHtmlResult(htmlContent) {
    var element = $($.parseHTML(htmlContent));
    if(localStorage.getItem("islogin") !== 'true') {
      element.each(function(index){
        $(this).find(".span-reply,.box-content-newfeed-action").remove();
      });
    }
    return element;
  }
</script>


