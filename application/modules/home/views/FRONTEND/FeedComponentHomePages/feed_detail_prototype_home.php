<?php 
    if(!empty($userData)) {  
        $username = $userData ['username'];
        $avatar_url = get_resource_url($userData ['thumbnail']);
        $url = $userData ['url'];
        $image = $userData ['image'];
        $video = $userData ['video'];
        $title = $userData ['title'];
        $description = $userData ['description'];
        $link_url_redm = $userData ['link_url_redm'];
        $user_bit_id = $userData ['user_bit_id'];
		$share_by = $userData['share_by'];
    } else {
        $username = $feedData ['username'];
        $avatar_url = get_resource_url($feedData ['thumbnail']);
		$url = $feedData ['url'];
		$image = $feedData ['image'];
		$video = $feedData ['video'];
		$title = $feedData ['title'];
		$description = $feedData ['description'];
        $link_url_redm = $feedData ['link_url_redm'];
        $user_bit_id = $feedData ['user_bit_id'];
		$share_by = $feedData['share_by'];
    }

    $feed_id = $feedData ['feed_bit_id'];
    $created = $feedData ['created'];
    $content = $feedData ['content'];
    $status = $feedData ['status'];

    $load_num = isset($feedData ['load_num']) ? $feedData ['load_num'] : 1;
    $has_paging = isset($feedData ['has_paging']) ? $feedData ['has_paging'] : 0;

    //generate profile url
    if(empty($link_url_redm)) {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . '?id=' . $user_bit_id;
    } else {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . $link_url_redm;
    }
    
    $is_login = ( isset($feedData ['user_is_login']) ) ? $feedData ['user_is_login'] : TRUE;
    
    $num_like_hot = $feedData ['total_like_hot'];
    $num_like_cool = $feedData ['total_like_cool'];
    $num_comment = $feedData ['total_comment'];
    $num_share = $feedData ['total_share'];
    
    $name_feed_id = 'feed' . $feed_id;
    $area_comment_form_id = 'areaCommentFormId' . $feed_id;
    $area_list_comment_id = 'areaListCommentId' . $feed_id;
    $area_feed_react_statistic_id = 'areaFeedReactStatistic' . $feed_id;

    $list_comment_html = ( ! empty($feedData ['listCommentHtml'])) ? $feedData ['listCommentHtml'] : '';
?>
<div class="box_content home">
	<div class="box_content" style="background:#f6f6f6;padding: 10px 15px 1px 15px;margin-top: -1px;"
    id="<?=$name_feed_id?>"
    data-channel-id="" 
    data-feed-id="<?=$feed_id?>" 
    data-load-num="<?=$load_num?>" 
    data-has-paging="<?=$has_paging?>">
	
	<!--update layout newfeed-->
		<ul class="diplay_ib">
			<li class="one-li">show latest</li>
			<li class="two-li">most popular</li>
		</ul>
		<hr class="hr_title_1"/>
		<ul class="diplay_inblock_li">
			<li><?=date("Y-m-d h:i:s",$created)?></li>
		</ul>
		<p>post by <span class="text-uppercase font-span"><?=$username?></span></p>
		<div class="decripstion_1">
			<p><?=strip_tags($content)?></p>
			<div class="content_feed_url">
				<div class="border2px">
					<?php
					if(!empty($url)){
					?>
					<a href="<?=$url?>" class="link-url">
						<div class="carousel-home-1">
							<div class="carousel">
								<div class="carousel-items">
									<div class="slider-item">
									<?php
									if($video){
									?>
										<embed src=<?=$video?> width=100 height=63></embed>
									<?php
									}
									else{
									?>
										<img class="image_url" src="<?=$image?>">
									<?php
									}
									?>
									</div>
								</div>
								<div class="title-url"><?=strip_tags($title)?></div>
								<div class="text-description3">
									<p><?=strip_tags($description)?></p>
									<div class="clear-fix"></div>
								</div>
							</div>
						</div>
					</a>
					<?php
					}
					?>
				</div>
			</div>
		</div>
		
	   <div id="" class="box-content-newfeed-total-action diplay_inblock_social_1">
			<span class="span-newfeed-like" data-action-type="like-hot">
				1&nbsp;Hot
			</span>        
			<span class="span-newfeed-like" data-action-type="like-cool">
				1&nbsp;Cool
			</span>
			<span class="span-newfeed-comment" data-action-type="comment">
				Comment
			</span>
			<span class="span-newfeed-share" data-action-type="share">
				Share
			</span>				
		</div>
		<hr class="fix_hr"/>
	</div>
</div>
		  <!-- write comment khúc này để làm-->
		<div id="<?=$area_comment_form_id?>" class="comment-write comment-write-profile mg-t20 element-hidden">
		</div>  <!-- write comment -->
		
		<!-- write comment khúc này là ví dụ 
		<div id="areaCommentFormId99423463840211256" class="comment-write comment-write-profile mg-t20">
			<form id="commentForm99423463840211256" action="" method="POST" class="form-comment-newfeed">
				  <div class="mg-t10 mg-b10">
					  <img class="comment-img" src="http://localhost/2017_REDM/CODE//assets/uploads/images/avatar.png?r=1507858549" alt="" width="60" height="60">
					  <input type="hidden" name="id_user_info" value="">
					  <input type="hidden" name="id_newfeed" value="">
					  <input type="hidden" name="feedBitId" value="99423463840211256">
					  <textarea id="commentContent" name="commentContent" placeholder="Write Comment..." required="" rows="4"></textarea>
				  </div>
				  <div class="comment-action">
					  <input type="reset" name="reset" value="Cancel">
					  <input type="submit" name="comment" value="Post">
					  <div class="clearfix"></div>
				  </div>
			 </form>
		</div>
		write comment -->
		<!-- end write comment -->
    <!-- write comment -->
    <div id="<?=$area_comment_form_id?>" class="comment-write comment-write-profile mg-t20 element-hidden">
    </div>
    <div class="clear-fix"></div>
    <!-- end write comment -->

    <!-- list comment -->
    <div class="clear-fix"></div>
    <div id="<?=$area_list_comment_id?>" class="list_comment_newfeed list_comment_newfeed_2">
        <?php if ($has_paging) {?> 
        <div class="list_comment_newfeed_detail_showmore mg-t20" >
            <center>
                <span class="show-comment-older">Show older</span>
            </center>
        </div>
        <?php } ?>
        <?=$list_comment_html?>
    </div>
    <div class="clear-fix"></div>
    <!-- end list comment -->
</div>