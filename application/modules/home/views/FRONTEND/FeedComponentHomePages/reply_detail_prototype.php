<?php 
    if( ! empty($userData)) {
        $username = $userData ['username'];
        $avatar_url = get_resource_url($userData ['thumbnail']);
        $link_url_redm = $userData ['link_url_redm'];
        $user_bit_id = $userData ['user_bit_id'];
    } else {
        $username = $replyData ['username'];
        $avatar_url = get_resource_url($replyData ['thumbnail']);
        $link_url_redm = $replyData ['link_url_redm'];
        $user_bit_id = $replyData ['user_bit_id'];
    }

    $reply_id = $replyData ['comment_bit_id'];
    $feed_id = $replyData ['feed_bit_id'];
    $comment_id = $replyData ['parent_comment_bit_id'];
    $created = $replyData ['created'];
    $comment = $replyData ['comment'];
    $status = $replyData ['status'];

    //generate profile url
    if(empty($link_url_redm)) {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . '?id=' . $user_bit_id;
    } else {
        $profile_url = PATH_URL . FEED_PREFIX . '/test-feed/' . $link_url_redm;
    }
    
    $is_login = ( isset($replyData ['user_is_login']) ) ? $replyData ['user_is_login'] : TRUE;

    $name_comment_id = 'comment' . $comment_id;
    $area_reply_form_id = 'areaReplyFormId' . $comment_id;
    $area_list_reply_id = 'areaListReplyElementId' . $comment_id;
    $name_reply_id = 'reply' . $reply_id;
?>
<div id="<?=$name_reply_id?>" class="list_comment_newfeed_detail_reply_box">
    <div class="col-sm-1 padding-left-0">
        <a href="<?=$profile_url?>">
            <img class="comment-img" src="<?=$avatar_url?>" alt="<?=$username?>" width="60" height="60">
        </a>
    </div>
    <div class="col-sm-11 padding-left-0">
        <div class="comment-name" style="padding-left: 20px">
            <a href="<?=$profile_url?>"><?=$username?></a>
            <span class="span-time date-time-component" data-date-time="<?=$created?>"><?=$created?></span>
            <?php if($is_login) { ?>
                <span class="span-reply" title="Reply" 
                    data-feed-id="<?=$feed_id?>" 
                    data-comment-id="<?=$comment_id?>"
                    data-reply-id="<?=$reply_id?>">
                    Reply
                </span>
            <?php } ?>
        </div>
        <div class="comment-content" style="padding-left: 20px">
            <?=nl2br(htmlspecialchars($comment))?>
        </div>
        <div class="" style="padding-left:20px">
                <div class="box-content-newfeed-action">
                    <span class="span-newfeed-like" data-action-type="like-hot">
                        <img src="<?php echo get_resource_url('/assets/images/icon/fire.png')?>">Hot
                        <!-- <i class="fa fa-thumbs-up" aria-hidden="true"></i> Like -->
                    </span>        
                    <span class="span-newfeed-like" data-action-type="like-cool">
                        <img src="<?php echo get_resource_url('/assets/images/icon/cold.png') ?>" alt="">Cool
                        <!-- <i class="fa fa-thumbs-up" aria-hidden="true"></i> Like -->
                    </span>
                    
             </div>
        </div>
    </div>
</div>