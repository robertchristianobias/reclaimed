
    <div class="main-content mt140 fullpagecol">
		<div class="two-col">
			<div class="title">
				<h3 class="mg-t0"><span>FEATURED</span></h3>
			</div>

			<div class="carousel-home-1">
				<div class="carousel">
					<div class="carousel-items">
						<?php
						// Featured
						$featured = modules::run('home/get_articles_featured');
						foreach ($featured as $result){ ?>
							<div class="slider-item">
							   <div class="slide-item-img col-md-7 fixmb" style="background: url(<?php echo get_resource_url(substr($result->thumbnail, 1)) ?>) 100% 100%/cover;background-size: contain;background-repeat: no-repeat;"> 
								   <?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>">   
									<?php } ?>
									   <!--<img src="<?php echo get_resource_url(substr($result->thumbnail, 1)) ?>" alt="<?php echo $result->title ?>">-->
										</a>

								</div>
								<div class="text-content carousel-items-description col-md-5" style="word-spacing:inherit !important">
									<p class="hidden"><?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="tag-a-title1 p-fs-20">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="tag-a-title1 p-fs-20"> 
									<?php } ?>
									<?php echo $result->title ?></a></p>
									<div>
										<?php
											$array_tags = explode(',', $result->tags);
											if(!empty($result->tags)){
												?>
												<ul class="listHomeBanner">
													<?php
													foreach($array_tags as $val){
													?>
														<li><a href="#"><span><?=$val?></span></a></li>
													<?php } ?>
												</ul>
											<?php 
											} 
											?>
									</div>	
									<p class="carousel-items-p-des" style="word-wrap: break-word;letter-spacing: inherit;word-spacing:inherit !important"><?php echo $result->description ?>
										
									</p>
									<?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more">   
									<?php } ?>Read more</a>
									<div class="clear-fix"></div>
									<!--<ul class="social">
										<li><a href="#"><span>182 Like</span></a></li>
										<li><a href="#"><span></span></a></li>
										<li><a href="#"><span>124 Comment</span></a></li>
									</ul>-->
								</div>
								<!--<?php if($this->lang->default_lang() == 'en') { ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more lg">
									<?php } else{ ?>
										<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more lg">   
								<?php } ?>Read more</a>-->
							</div>
						<?php } ?> 
					</div>
					<div class="carousel-prev">
						<i class="fa fa-caret-left" aria-hidden="true"></i>
					</div>
					<div class="carousel-next">
						<i class="fa fa-caret-right" aria-hidden="true"></i>
					</div>
				</div>
			</div>

			<hr/>
			<div class="title">
				<h3><span>LATEST</span></h3>
			</div>
			<div class="pix-col one-line pix-col-3 list-post">
				<?php
				$data = modules::run('home/articles_latest');
				if(!empty($data['html'])){
					echo $data['html'];
				}
				if(!empty($data['is_more'])){
				?>
				<div id="list-post-see-more">
					<a href="javascript:get_articles_latest_more()" class="read-more-load">See more</a>
				</div>
				<?php
				}
				?>
			</div>
			
			<?php 
			if(isset($this->session->userdata['userData'])){ 
				$dataUser = modules::run('home/load_list_user_info', $this->session->userdata['userData']['id']);
				$country = "";
				if(!empty($dataUser)){
					$country = $dataUser[0]->country_residence;
				}
			?>
			<!--Thêm phần mới khung banner-->
				<!--<div class="boxbanner">
					
				</div>-->		
			<!--Kết thúc thêm phần mới ngày ở dây-->

				<div class="clear-fix"></div>
				<div class="title">
					<h3><span style="text-transform: uppercase;">COUNTRY IF LOGGED IN</span></h3>
				</div>		
				<?php
					$latest = modules::run('home/readTagCountry', $country);
						if( !empty($latest)) { 
							$i = 0; 
							foreach ($latest as $result){
								$i++; 
				?>
				   <div class="pix-col one-line pix-col-3 list-post">
				
						<div class="item item-latest mg-b20">
							<div class="wrap-image">
							   <?php if($this->lang->default_lang() == 'en') { ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" >
								<?php } else{ ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>">   
								<?php  } ?>
									<img src="<?php echo get_resource_url(substr($result->image, 1)) ?>" alt="<?php echo $result->title ?>" class="image-post">
								</a>
							</div>
							<div class="text-content">
								<p class="hidden">
								<?php if($this->lang->default_lang() == 'en') { ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="tag-a-title1 p-fs-20">
								<?php } else{ ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="tag-a-title1 p-fs-20">   
								<?php } ?>
								<?php echo $result->title ?></a>
								 </p>
								<p class="post-p-des" style="min-height: 110px;word-wrap: break-word;"><?php echo the_excerpt($result->description, 16);  ?></p>
								<?php if($this->lang->default_lang() == 'en') { ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more">
								<?php } else{ ?>
									<a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more">   
								<?php } ?>Read more</a>
								<div class="clear-fix"></div>

							</div>
						</div>
					</div>
					<?php if($i%2 == 0) { ?>
					<div class="clearfix hidden-md hidden-lg"></div>
					<?php } ?>
					<?php if($i%3 == 0) { ?>
					<div class="clearfix hidden-xs hidden-sm"></div>
					<?php } ?>

						<?php 
							}
						}
						?>
						<div class="clear-fix"></div>
						
				<!--Thêm phần mới khung banner
				<div class="boxbanner">
					<div><img class="imgw100" src="https://source.unsplash.com/random/950x300/?2" alt=""></div>
				</div>		
				Kết thúc thêm phần mới ngày ở dây-->
			</div>
			<div class="col-md-3 fixwith-home">
				<div class="box_content_home">
					<div class="title">
						<h3 class="mg-t0"><span>NEWSFEED</span></h3>
					</div>
					<?php
					if (isset($this->session->userdata['userData'])) {
					?>

						<div class="box_content">
							<?php
							echo modules::run('home/FeedHomePages/showHomePage');
							?>
						</div>

					<?php }?>
				</div>
			</div>
			<div class="clear-fix"></div>
			<div class="title">
			</div>
		<?php
		}
		?>
        <div class="clearfix"></div>
    </div>    
    <div class="clearfix"></div>
<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
<script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
<script type="text/javascript">
var article_latest_per_page_init = <?=ARTICLE_LATEST_PER_PAGE_INIT?>;
var article_latest_per_page = <?=ARTICLE_LATEST_PER_PAGE?>;
</script>
<script src="<?=get_resource_url('assets/js/home.js')?>" type="text/javascript"></script>
	
   