<?php
$attributes = !empty($attributes) ? $attributes : '';
$class_name = !empty($class_name) ? $class_name : '';
?>
<li class="mobile-social <?=$class_name?>" <?=$attributes?>>
	<a style="padding-left: 5px;padding-right: 5px;" href="https://www.facebook.com/ReclaimEDM/" target="_blank">
		<i class="fa fa-facebook" aria-hidden="true"></i>
	</a>
	<a style="padding-left: 5px;padding-right: 5px;" href="https://www.youtube.com/channel/UCwlPcPVs01dbdwmr-QZMNsQ/playlists?view_as=public" target="_blank">
		<i class="fa fa-youtube-play" aria-hidden="true"></i>
	</a>
	<a style="padding-left: 5px;padding-right: 5px;" href="https://hearthis.at/reclaimedm.com" target="_blank">
		<img height="15" width="15" src="<?=PATH_URL.'assets/images/icon/hearthis_icon.png?r=1'?>" />
	</a>
	<!-- <a style="padding-left: 5px;padding-right: 5px;" href="https://soundcloud.com/reclaimedm" target="_blank">
		<i class="fa fa-soundcloud" aria-hidden="true"></i>
	</a>
	<a style="padding-left: 5px;padding-right: 5px;" href="https://mixcloud.com/ReclaimEDM" target="_blank">
		<i class="fa fa-mixcloud" aria-hidden="true"></i>
	</a> -->
</li>