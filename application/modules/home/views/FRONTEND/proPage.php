<?php

if (isset($user_info)) {
    $id_pro = $user_info->id;
    $name = $user_info->username;
    // $city = $user_info->city;
    $city = $user_info->city_residence;
    // $country = $user_info->country;
    $country = $user_info->country_residence;
    $gender = $user_info->gender;
    $url = $user_info->url;
    $url_of_page = $user_info->url_of_page;
    $facebook = $user_info->facebook;
    $twitter = $user_info->twitter;
    $beatport = $user_info->beatport;
    $soundcloud = $user_info->soundcloud;
    $mixcloud = $user_info->mixcloud;
    $image = $user_info->image;
    $type = $user_info->type;
    $genre = $user_info->genre;
}
?>
<link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/libs/flickity/flickity.min.css')?>">
<div class="w-wapper mt-40">
    <div class="propage-info" id="propage-info">
		<div class="person-profile-img" style="margin-top:40px">
			<div style="text-align:center">
				<img class="img-responsive img-profile" src="<?=get_resource_url( (!empty($image)) ? $image : "" )?>" alt="img">
 				<!-- <img class="img-responsive img-profile" src="https://reclaimedm.com/beta/assets/uploads/images/2018/05/212b2a89d851c8942c63da65ef435631.png?r=1528096736" alt=""> -->
			</div>
		</div>
		<div class="main-contact-profile">
			<div class="contact-profile">
				<div class="row" style="text-transform: uppercase;">
					<div class="col-md-11 col-md-offset-1">
						<div class="col-sm-12">
							<div class="title-page row">
								<p class="f-26">FEAT.</p>
							</div>
						</div>
						<div class="col-md-4 mt-25 person-profile-info fix480">
												
							<!-- <div class="list-page col-xs-12 col-sm-8 col-md-8">
								<ul>
									<li class="cot-1"><span class="pink">CONNECT</span></li>
									<li class="cot-1"><span class="pink">FOLLOW</span></li>
									<li class="cot-1"><span class="pink">DEMAND</span></li>
								</ul>
							</div> -->
							<div class="clear-fix mb-25"></div>
							<div class="row">
								<ul class="font_mb">
									<li class="cot-2">
										<?=(!empty($type)) ? $type : "" . ' name' ?>: <span> <?=(!empty($name)) ? $name : ""?></span>
									</li>
									<li class="cot-2">
										CITY: <span><?=(!empty($city)) ? $city : ""?></span>
									</li>
									<li class="cot-2">
										GENRE/S: <span><?=(!empty($genre)) ? $genre : ""?></span>
									</li>
									<li class="cot-2">
										AFFILIATIONS
									</li>
								</ul>
							</div>

							<div class="clear-fix mb-25"></div>
						</div>

						<div class="col-md-4 mt-25 person-profile-info fix480">
							<div class="row social-page">
								<ul class="font_mb">
									<li class="cot-2"><span class="white">WWW:</span> <?=(!empty($url)) ? $url : ""?></li>
									<li class="cot-2"><span class="white">FACEBOOK:</span> <?=(!empty($facebook)) ? $facebook : ""?></li>
									<li class="cot-2"><span class="white">TWITTER:</span> <?=(!empty($twitter)) ? $twitter : ""?></li>
									<li class="cot-2"><span class="white">BEATPORT:</span> <?=(!empty($beatport)) ? $beatport : ""?></li>
									<li class="cot-2"><button class="white" name="addevent" id="addevent">ADD EVENT</button></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4 mt-25 person-profile-info fix480">
							<div class="row social-page">
									<ul class="font_mb">
										<li class="cot-2"><span class="white">SOUNDCLOUD:</span> <?=(!empty($soundcloud)) ? $soundcloud : ""?></li>
										<li class="cot-2"><span class="white">MIXCLOUD:</span> <?=(!empty($mixcloud)) ? $mixcloud : ""?></li>
									</ul>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div class="section-content-page bgxam">
	<div class="box-all container">
		<div class="row">
			<div class="slider autoplayslider show991_slider" role="toolbar">
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pdr25 hide991">
				<div class="title_slider">SIMILAR ARTISTS</div>
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
				<div class="box_slider">
					<div class="img-responsive"><img class="imgw100" src="http://via.placeholder.com/250x150" alt="img"></div>
					<div class="info">
						<p class="tag_artist no_margin">Artist : <span class="text-uppercase">NAME IS HERE</span></p>
						<p class="tag_origin no_margin">Artist : img-responsive</p>
						<p class="tag_sound no_margin">Artist : img-responsive</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 bgblack">
				<div class="box_content_propage">
					<div class="title_content">NEWSFEED</div>
					<hr class="hr_title"/>
					<?php
					if (isset($this->session->userdata['userData'])) {
					?>

						<div class="box_content">
							<?php
								echo modules::run('home/FeedProPage/showHomePage',(!empty($url_of_page)));
							?>
						</div>

					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clearfix-mt40"></div>
<div class="container">

	<?php
if (isset($this->session->userdata['userData'])) {
    $userDataSetting1 = modules::run('home/load_list_user_info', $this->session->userdata['userData']['id']);
    foreach ($userDataSetting1 as $value) {
        $user_info_id = $value->id;
    }
}
if (!empty($user_info_id)) {
    ?>
	<div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<form class="newfeed-form" method="POST" action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/up_newfeed" >
			<textarea data-required="1" cols="" rows="8" name="content" id="content" required></textarea>
			<input type="hidden" name='id_user_info' value="<?php echo $user_info_id ?>">
			<input type="hidden" name='id_pro' value="<?php echo (!empty($id_pro)) ?>">
			<div class="clearfix"></div>
			<div class="form-button-submit">
				<button type="submit">Submit</button>
				<button type="reset">Cancel</button>
			</div>
			</form>
		</div>
	</div>
	<?php }?>
</div>
<script type="text/javascript" src="<?=get_resource_url('assets/libs/flickity/flickity.pkgd.min.js')?>"></script>
<script>
	var $slideshow2 = $('.js-flickity.slideshow-header').flickity({
	  wrapAround: true,
	  autoPlay: 3000,
	  // imagesLoaded: true,
	  // percentPosition: false,
	 // pauseAutoPlayOnHover: false
	 draggable: true,
	 contain: true

	});
</script>
<style>
	.section-footer{
	  background: #1d1d1b;
	  padding: 50px 0;
	}
	.section-footer .container .row{
	  margin-left: 50px;
	}
	.box-link ul li{
	  list-style-type: none;
	}
	.box-link > ul > li > a{
	  text-decoration: none;
	  color: #fff;
	}
	.person-profile-img .img-responsive.img-profile{
	  height: 520px;
	}
	.contact-profile .col-sm-4.col-md-4{
	  padding: 0
	}
	.contact-profile .person-profile-img{
	  height: 520px;
	  width: 100%;
	  margin: 0;
	}
	.contact-profile .person-profile-info{
	  padding-left: 15px;
	}
	.list-post .carousel .carousel-items .slick-list .slick-track .slider-item:focus {
	    outline: none;
	}
	.box-all.container{
		padding: 0;
	}
	.text-content div {
	    height: 70px;
	    overflow: hidden;
	}
	.list-post .carousel .carousel-items .slick-list .slick-track .item{
		/* width: 300px; */
		margin: 0 auto;
	}
	.white:hover, .white:focus {
		/* color: #fff; */
		z-index: 3;
		position: relative;
	}
</style>
<style type="text/css" media="screen">
	#popup_overlay{
	  background-color: #222;
	}
	.popup_banner_content img{
		width: 100%;
	}
	.popup_banner{
	   position: absolute;
	   background-color: #fff;
	}
	.popup_banner_fb{
	   position: absolute;
	}
	.styleiframe{
		width: 768px;
		border: 0px;
		height: 550px;
	}
	@media (min-width: 768px)
	{
	  .popup_banner{
		max-width: 960px;
		max-height: 600px;
	  }
	.popup_banner_fb{
		max-width: 960px;
		max-height: 600px;
	  }
	}
	.popup_banner button.close{
	  position: absolute;
	  right: 0px;
	  z-index: 2;
	}
	.popup_banner_content{
		min-width: 500px;
	}
	@media(max-width:600px) {
		.popup_banner_content{
			max-width: 480px;
		}
	}
	@media(max-width:600px) {
		.popup_banner_content{
			max-width: 480px;
			min-width: auto
		}
	}
	@media(max-width:480px) {
		.popup_banner_content{
			max-width: 360px;
		}
	}
	@media(max-width:360px) {
		.popup_banner_content{
			max-width: 300px;
		}
	}
</style>
<div class="modal" id="popup_overlay">
</div>
<div class="modal" id="popup_container">
	<div class="popup_banner">
	  <button type="button" class="close">&times;</button>
	  <div class="popup_banner_content">
		  <div class="box_inner">
			<div class="social-btn row" style="text-align:right; margin-top:10px;">
				<div class="col-md-12 social">
					<a href="<?=PATH_URL . 'ajax/login_fb_access_token'?>" target="_blank" class="btn btn-default" style="margin-right: 15px;">
						<input type="hidden" id="facebook_accesstoken" name="facebook_accesstoken" style="visibility: hidden; height: 0" value=""/>
						<i class="fa fa-facebook-square" aria-hidden="true"></i>
						<span>Login Facebook</span>
					</a>
				</div>
			</div>
			<div class="col-md-12 list-group" style="margin-top:20px; padding-left:15px" id="facebook_page_list">
				<!-- Dynamic content here -->
				
			</div>
			<div class="col-md-12 list-group" style="margin-top:20px; padding-left:15px" id="facebook_page_event_list">
				<!-- Dynamic content here -->
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- end Modal content-->
<style>
	.fixclose{
		padding:0px !important;
		padding-right:20px !important;
	}
</style>
<div class="modal" id="popup_overlay_fb">
</div>
<div class="modal" id="popup_container_fb">
	<div class="popup_banner_fb">
	  <button type="button" class="close fixclose">&times;</button>
	  <div class="popup_banner_content" style="width:100%">
		<div class="box_inner">
			
		</div>
	  </div>
	</div>
</div>

<script>
  var fb_page_list = '<?php echo (isset($_SESSION['fb_page_list'])) ? $_SESSION['fb_page_list'] : '' ?>';
  var fb_page_event_list = '<?php echo (isset($_SESSION['fb_page_event_list'])) ? $_SESSION['fb_page_event_list'] : '' ?>';
</script>
 <script>
  var is_popup_open = false;
   function show_popup() {
	is_popup_open = true;
	  $('#popup_overlay').show();
	  $('#popup_overlay').css('opacity', 0.8);
	  $('#popup_container').show();
	  var jele = $('#popup_container').find('.popup_banner');
	  if(jele.length){
		var viewport_size = device_viewport();

		if(viewport_size.width <= 767){ // Mobile
		  jele.width(viewport_size.width);
		  jele.css({
			'height' : 'auto'
		  });
		}
		var popup_width = jele.width();
		var popup_height = jele.height();
		var left = parseInt((viewport_size.width - popup_width)/2);
		var top = parseInt((viewport_size.height - popup_height)/2);
		jele.css({
		  'left' : left + 'px',
		  'top' : top + 'px'
		});
		jele.find('button.close').unbind('click').bind('click',hide_popup);
	  }
   }
   function hide_popup() {
		is_popup_open = false;
		$('#popup_overlay').hide();
		$('#popup_container').hide();
   }

   function device_viewport() {
		var e = window, a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width'] , height : e[ a+'Height']};
  }
  $(document).ready(function(){
	  $("#addevent").on("click", function(){
		setTimeout(show_popup,500);
	});
  });
  $(window).on("load resize",function() {
		if(is_popup_open){
		 setTimeout(show_popup,500)
		}
  });

</script>
 <script>
  var is_popup_open_fb = false;
   function show_popup_fb(url) {
	  is_popup_open = true;
	  $('#popup_overlay_fb').show();
	  $('#popup_overlay_fb').css('opacity', 0.8);
	  $('#popup_container_fb').show();
	  var jele = $('#popup_container_fb').find('.popup_banner_fb');
	  if(jele.length){
		// Create iframe
		var html = '<iframe class="styleiframe" src="'+url+'"></iframe>';
		jele.find('.box_inner').html(html);
		
		var viewport_size = device_viewport();

		if(viewport_size.width <= 767){ // Mobile
		  jele.width(viewport_size.width);
		  jele.css({
			'height' : 'auto'
		  });
		}
		var popup_width = jele.width();
		var popup_height = jele.height();
		var left = parseInt((viewport_size.width - popup_width)/2);
		var top = parseInt((viewport_size.height - popup_height)/2);
		jele.css({
		  'left' : left + 'px',
		  'top' : top + 'px'
		});
		jele.find('button.close').unbind('click').bind('click',hide_popup);
	  }
	  
	  return false;
   }
   function hide_popup() {
		is_popup_open = false;
		$('#popup_overlay_fb').hide();
		$('#popup_container_fb').hide();
   }

   function device_viewport() {
		var e = window, a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width'] , height : e[ a+'Height']};
  }
  
  // $(document).ready(function(){
	   // $('a[data-spinner="true"]').on("click", function(){
		// setTimeout(show_popup_fb,500);
	// });
  // });
  // $(window).on("load resize",function() {
		// if(is_popup_open_fb ){
		 // setTimeout(show_popup_fb,500)
		// }
  // });

</script>

