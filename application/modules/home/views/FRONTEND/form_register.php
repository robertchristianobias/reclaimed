<?php
$is_signup_special = !empty($token) || !empty($oauth_token); // Setup Propage or Social Login
$form_step_default = $is_signup_special ? 2 : 1;
$form_button_prev_visible_first_step = !$is_signup_special;
$form_button_prev_visible_steps = $is_signup_special;
$form_button_prev_visible = !$is_signup_special;
?>
 <link href="<?=get_resource_url('assets/css/bootstrap-tagsinput.css')?>" rel="stylesheet">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://apis.google.com/js/platform.js"></script>

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">

  <style type="text/css">
    .gg-btn {
    display: inline-block;
    background: #ce3e26;
    width: 220px;
    height: 40px;
    border-style: none;
    margin: 0;
    white-space: nowrap;   
  }
  .btn{
  	border-style: none !important;
  	border-radius: 0 !important;
  }
  .gg-btn:hover {
    cursor: pointer;
  } 
  .icon-gg-wrapper{
  	float: left;
  	background: #ce3e26;
  	width: 40px;
  	height: 40px;
  }
  .login-wrapper{
  	margin-bottom: 10px; 
  	width: 260px;
  	height: 40px;
  }
  span.icon-gg {
    background: url('<?=PATH_URL?>assets/images/icon/google-plus.png') ;
    display: inline-block;
    vertical-align: middle;
    width: 24px;
    height: 24px;
    margin:8px;
  }
  .fb-btn {
    display: inline-block;
    background: #3B5998;
    color: #fff;
    width: 220px;
    height:40px;
   	border-style: none;
    white-space: nowrap;
    vertical-align: middle;
  }
  .fb-btn:hover {
    cursor: pointer;
  } 
  span.icon-fb {
    background: url('<?=PATH_URL?>assets/images/icon/facebook.png') ;
    display: inline-block;
    vertical-align: middle;
    width: 24px;
    height: 24px;
    margin:8px;
  }
  .icon-fb-wrapper{
  	background: #3B5998;
  	float: left;
  	height: 40px;
  	width: 40px;
  }
  .login-text{
  	color: #fff;
  	margin-top:4px;
  }
.input_radio.disabled {
	color: grey;
}
@media screen and (max-width: 768px ) {
	.hidden-sp{
		display: none !important;
	}
}
@media screen and (min-width: 769px ) {
	.hidden-pc{
		display: none !important;
	}
}
.ui-datepicker{
	z-index: 9990 !important;
}

.date-input{
	position: relative;
	display: inline-block;
	width: 100%;
	height: 24px;
}
.date-input p{
	position: relative;
	z-index: 90;
	width: 100%;
	height: 100%;
	line-height: 24px;
	font-size: inherit;
	padding: 0;
	background: #3d3d3d;
	color: #fff;
}
.date-input input{
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
	display: block;
	width: 100%;
	height: 100%;
	z-index: 100;
	-webkit-appearance: none;
}

</style>
<script type="text/javascript">
    function PopupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow
		if (window.focus) {
		    newWindow.focus();
		}
		
		return false;
	}
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
var is_signup_special = <?=$is_signup_special ? '1' : '0'?>;
var current_tab_order = <?=$form_step_default?>;
$(document).ready(function(){
	<?php 
	if ($is_signup_special) {
	?>
	hide_loadding_allow = false; // Force not hide loading
	$('.signup-right').css('opacity',0); // Signup form (signup-right) is hidden
	setTimeout(function(){
		// alert('before form_signup_display');
		
		form_signup_display();
		setTimeout(function(){
			// alert('before Click Sign up');
			
			// Click Sign up
			$('.nav-tabs-signup')[0].click();
			$('.signup-right').css('opacity',1);

			setTimeout(function(){
				// alert('before Force hide loading');
				
				// Force hide loading
				hide_loadding_allow = true;
				hide_loadding();
			}, 1000);
		}, 1000);
	}, 1000);
	<?php
	}
	?>
	
	// TODO: Show/Hide form - Keep state?
	// Show Step Container
	var step_container_current_jele = $('.section-form[data-step="<?=$form_step_default?>"]');
	step_container_current_jele.addClass('active');
	step_container_current_jele.find('.step_number_displayed').text(step_number_displayed);
	
	// Date input
	datepicker_native_mobile();
});
function reformatDate(dateStr) {
    if (!dateStr) {
        return false;
    }
    dArr = dateStr.split("-"); // ex input "2010-01-18"
    return dArr[2] + "-" + dArr[1] + "-" + dArr[0]; //ex out: "18/01/2010"
}
function datepicker_native_mobile(){
	$('.date-input').each(function() {
        var item = $(this);
        item.find('.date-pc').datepicker({
            dateFormat: "yy-mm-dd",
            yearRange: "1900:2020",
            changeMonth: true,
            changeYear: true
        });
        item.find('.date-sp').on('change focus blur', function() {
            var current_value = $(this).val();
            item.find('.date-pc').datepicker("setDate", current_value);

            // convert current_value to d-m-y
            current_value = reformatDate(current_value);
            if (current_value) {
                item.find('.date-value').text(current_value);
            }
        });
        item.find('.date-pc').on('change focus blur', function() {
            var current_value = $(this).val();
            item.find('.date-sp').val(current_value);
            // convert current_value to d-m-y
            current_value = reformatDate(current_value);
            if (current_value) {
                item.find('.date-value').text(current_value);
            }

        });
    });
}
<?php
$token_value = isset($token) ? $token : '';

$email_value = isset($email) ? $email : '';
$email_readonly = isset($email) ? 'readonly' : '';

// Setup Propage
$is_setup_propage = !empty($is_setup_propage) ? $is_setup_propage : false;
if($is_setup_propage){
	$user_type_id_value = isset($user_type_id) ? $user_type_id : null;
	$user_type_id_name = null;
	if (!empty($type_list)) {
		foreach ($type_list as $key => $value) {
			if ($value->id == $user_type_id_value) {
				$user_type_id_name = $value->type;
			}
		}
	}
}
$url_pro_page_value = isset($url_of_page) ? $url_of_page : '';
$url_propage_readonly = isset($url_of_page) && isset($token) ? 'readonly' : '';
$genre = isset($genre) ? $genre : '';

// Login Social
$oauth_provider_value = isset($oauth_provider) ? $oauth_provider : false;
$oauth_uid_value = isset($oauth_uid) ? $oauth_uid : false;
$oauth_token_value = isset($oauth_token) ? $oauth_token : false;
?>
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=191785574839630&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!-- SIGN UP -->
<div class="signup-right">
	<div class="signup-wrap">
		<div class="tab-menu-signup pd-t20">
			<ul class="nav nav-tabs">
				<li><a data-toggle="tab" href="#signup-form" class="nav-tabs-signup">SIGN UP</a></li>
				<li class="active"><a data-toggle="tab" href="#login-form">LOGIN</a></li>
				<li class="close-signup"><a href="#">close</a></li>
			</ul>
		</div>
		<div class="tab-content">
			<!-- BEGIN: LOGIN FORM -->
			<form id="login-form" class="tab-pane fade in ">
				<input type="hidden" name="token" value="<?=$token_value?>" />
				<div class="login-wrap">
					<div class="col-md-12">
						<div class="login-left">
							<div class="login-style">
								<label style="font-weight: 100;">ENTER YOUR EMAIL</label>
								<input class="username-input" style="height: 30px;background:transparent; color: #fff;text-align: center;" type="text" name="username" val-message="Số lượng từ không hợp lệ" data-required="true" data-check="true" data-error="Please enter your username" value="" />
								 <label style="font-weight: 100;">ENTER YOUR PASSWORD</label>
								<input type="PASSWORD" style="height: 30px;background:transparent; color: #fff;text-align: center;" name="password" value=""  data-required="true" data-error="Please enter your country"/>
							</div>
							<div id="logerror"></div>
							
							<div class="btn-pre-next" style="padding-top: 20px">
								<input type="submit" id="btn-login" data-url="<?=PATH_URL?>" name="login" class="login-btn mg-sm-t20" value="Login" />
							</div>
						</div>
						<!-- <div class="line-login"></div> -->
						<div>
								<div class="login-type" align="center" style="display: block">
									<div class="login-wrapper">
										<div class="icon-gg-wrapper">
											<span class="icon-gg"></span>
										</div>
										<a href="<?=PATH_URL.'ajax/login_gg'?>" class="btn gg-btn"
										onclick="return PopupCenter(this.href, 'Google', '800','700')" target="_blank">
											<p class="login-text">LOGIN WITH GOOGLE</p>
										</a>
									</div>
									<div class="login-wrapper">
										<div class="icon-fb-wrapper">
											<span class="icon-fb"></span>
										</div>
										<a href="<?=PATH_URL.'ajax/login_fb'?>" class="btn fb-btn" 
										onclick="return PopupCenter(this.href, 'Facebook', '800','700')" target="_blank">
											<p class="login-text">LOGIN WITH FACEBOOK</p>
										</a>
									</div>	
									<div class="login-type-t"><a class="forgot-link" data-toggle="modal" data-target="#forgot-mobal">Forgot your password?</a></div>
								</div>
						</div>
					</div>
				</div>
			</form>
			<!-- END: LOGIN FORM -->
			
			
			<!-- BEGIN: SIGNUP FORM -->
			<form id="signup-form" onkeydown="TriggeredKey(this)"  method="post" action="" class="signup-form tab-pane fade in">
			<script>
				function TriggeredKey(e){
					var result = true;
					
					var keycode;
					if (window.event) {
						keycode = window.event.keyCode;
						if (keycode = 13 ){
							result = false;
						}
					}
					
					return result;
				}
			</script>
			
			
				<input type="hidden" name="token" value="<?=$token_value?>" />
				<?php
				if ($oauth_token_value !== false) {
					echo '<input type="hidden" name="oauth_token" value="' . $oauth_token_value . '" />';
				}
				if ($oauth_provider_value !== false) {
					echo '<input type="hidden" name="oauth_provider" value="' . $oauth_provider_value . '" />';
				}
				if ($oauth_uid_value !== false) {
					echo '<input type="hidden" name="oauth_uid" value="' . $oauth_uid_value . '" />';
				}
				?>

				<!-- BEGIN: STEP 1 -->
				<section class="section-form" data-step="1">
					<h3 class="title-step mg-t30 fix-mg-t30 pdd30">Step <span class="step_number_displayed">1</span></h3>
					<div class="step1-wrap">
						<div class="step1">
							<div class="row" style="overflow:hidden;">
								<div class="col-md-6 left t-l-r">
									<div>
										<div class="row">
																			
											<div class="signup-label"><label for="email"  style="text-align:center;font-weight: 100;display: block; font-size: 14px;" >ENTER YOUR EMAIL</label></div>
										</div>
										<input style="text-align: center;height: 30px;background:transparent; color: #fff" id="email_step1" type="email" name="email" data-error="Please enter your email" value="<?=$email_value?>" <?=$email_readonly?>/>
									</div>
									<div class="row" style="padding-top: 5px">
											<div class="signup-label"><label style="text-align:center;font-weight: 100;display: block;font-size: 14px">CHOOSE YOUR USER NAME</label></div></div>
									<input style="text-align: center;height: 30px;background:transparent; color: #fff" class="username-input" type="text" name="username" val-message="Invalid Number Your Name" data-required="true" data-check="true" id="name_step1" data-error="Please enter your username" value="" />
									<div class="row" style="padding-top: 5px">
											<div class="signup-label"><label style="text-align:center;font-weight: 100;display: block;font-size:14px">SET YOUR PASSWORD</label></div></div>
									<input style="text-align: center;height: 30px;background:transparent; color: #fff" class="password" type="password" name="password" value="" data-required="true" data-error="Please enter your password"/>
									<style>
										.pac-container{
										   top:150px !important;
										}
										
									</style>
						   			<!--<label class="mg-t40 mg-b20">ENTER YOUR COUNTRY</label>
									<input id="searchTextField" placeholder="" type="text" name="location" value=""  data-required="true" data-error="Please enter your country"/>

									<input type="hidden" name="country" value="" type="text" id="country-hide-sign">
									<input type="hidden" name="city" value="" type="text" id="city-hide-sign"> -->
									

									<!-- Google reCaptcha -->
									
									
										<div class="g-recaptcha google-captcha" data-sitekey="6LcqyGEUAAAAAPadWU2AZG0KiXkLaPbIcPWuimVL" data-theme="dark" ></div>
										<input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha" value="default">
									
										
								</div>
								<div class="col-md-1 center_conlum" style="width: 2px;">
									<img class="fix-img" src="<?=get_resource_url('assets\images\admin\uploader\uploadDisabled.png')?>" alt="">
								</div>
								<div class="col-md-5 right ">
									<div class="social-inner pdd-signup" align="left"> 
											<div class="login-wrapper">
												<div class="icon-gg-wrapper">
													<span class="icon-gg"></span>
												</div>
												<a href="<?=PATH_URL.'ajax/login_gg'?>" class="btn gg-btn"
												onclick="return PopupCenter(this.href, 'Google', '800','700')" target="_blank">
													<p class="login-text">SIGNUP WITH GOOGLE</p>
												</a>
											</div>
											<div class="login-wrapper">
												<div class="icon-fb-wrapper">
													<span class="icon-fb"></span>
												</div>
												<a href="<?=PATH_URL.'ajax/login_fb'?>" class="btn fb-btn" 
												onclick="return PopupCenter(this.href, 'Facebook', '800','700')" target="_blank">
													<p class="login-text">SIGNUP WITH FACEBOOK</p>
												</a>
											</div>	
										
									</div>
								</div>
							</div>
						</div> 
						<div style="text-align: center; padding-top: 15px">
							<input type="button" name="next" class="signup-next-btn form_signup_process_next  mg-b50 mg-30" value="Next" />
						</div>
					</div>
				</section>
				<!-- END:	STEP 1 -->

				<!-- BEGIN: STEP 2 -->
				<section class="section-form" data-step="2">
					<h3 class="title-step mg-t30 fix-mg-t30">Step <span class="step_number_displayed">2</span></h3>
					<div class="step2-wrap">
						<div class="step2">
							<div class="mg-t0" style="margin-left: -30px">
								<?php echo ($is_setup_propage ? 'Your user type' : 'Please choose a user type')?>
							</div>
								<div class="mg-t30">
									<?php
									// Setup Propage
									if (!empty($user_type_id_value) && !empty($user_type_id_name)) {
									?>
										<input type="radio" name="user_type" value="<?php echo $user_type_id_value?>" checked />
										<label>
											<span class="input_radio disabled" style="padding-left: 15px; color:white"><?php echo $user_type_id_name ?></span>
										</label>
										</br>
									<?php 
									}
									// Login Social or Normal SignUp
									else 
									{
										$i = 1;
										foreach ($type_list as $key => $value) {
											$disabled = '';
											$user_type_name = strtolower($value->type);
											$user_type_id = $value->id;
											
											// TODO - REDUNDANT CODES
											/*
											if (!empty($token)) { // Pro ??? -> Setup Propage ?
												$user_type_name_lower = strtolower($user_type_name);
												if (strpos($user_type_name_lower, 'reader') !== false) {
													$disabled = 'disabled';
												}
											}
											*/
										?>
										<input type="radio" name="user_type" value="<?php echo $user_type_id ?>" <?php if ($i == 1) {echo "checked";}?>>
										<label>
											<span class="input_radio  <?=$disabled?>"  style="padding-left: 15px; color:white"><?php echo $user_type_name ?></span>
										</label>
										</br>
									<?php 
											$i++;
										}
									}
									?>
							</div>
						</div>

						
						
						<div class="btn-pre-next">
							<?php
							if ($form_button_prev_visible_first_step) {
								?>
							<input type="button" name="previous" class="signup-pre-btn mg-b20 mg-20-l" value="Previous" />
							<?php
							} else {
								?>
							<div class="signup-pre-btn mg-b20" style="width: 65px; visibility: hidden; margin:0; padding:0;"></div>
							<?php
							}
							?>
							<input type="button" name="next" class="signup-next-btn form_signup_process_next  mg-b50" value="Next" />
						</div>
					</div>
				</section>
				<!-- END:	STEP 2 -->
				
				<!-- BEGIN: STEP 3 -->	
				<section class="section-form" data-step="3">
					<h3 class="title-step mg-t30 fix-mg-t30">Step <span class="step_number_displayed">3</span></h3>
					<div class="ddd">
						<div class="cover">
							<style type="text/css">
								.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
									background-color: #5f5f5f;
								}
								
							</style>
							<table class= "table table-hover table-sm ">						  
							<tbody>
							    <tr>
							    	<td class="col-3 col-md-3 col-sm-3">Name</td>
							    	<td colspan="3" class="col-md-8 col-sm-8 col-8"><input class="t20 change_name_step_1" id="name_3" name="name" value="<?php echo !empty($form_data_username) ? $form_data_username : '';?>" style="background:transparent;border-style: none; width: 100%;" readonly>	    </td>
							    	<td class="col-1 col-sm-1 col-md-1" align="right"><a href="#" onclick="editName()" id="editName" style="color:#fff">Edit</a></td>
							    </tr>
							    <tr>
							    	<td class="col-3 col-md-3 col-sm-3">Email</td>
							    	<td colspan="3" class="col-md-8 col-sm-8 col-8"><input id="email_3" name="email_3" value="<?php echo !empty($form_data_email) ? $form_data_email : ''; ?>" style="background:transparent;border-style: none; width:100%;" readonly>
							    	<input class="use_email" checked = 'checked' type="checkbox" name="mailing" value="1" >
											<span class="font-span-step3" style="position: relative;top: -5px;">Use for mailing list</span></td>
							    	<td class="col-1 col-sm-1 col-md-1" align="right"><a href="#" onclick="editEmail()" id="editEmail" style="color:#fff">Edit</a></td>
							    </tr>
							    <tr>
									<td class="col-3 col-md-3 col-sm-3">Country of Residence</td>
							    	<td colspan="3" class="col-md-8 col-sm-8 col-8">
							    		<input id="searchTextField"  name="location" type="text"  placeholder="Country" autocomplete="on" style="background:transparent;border-style: none;width: 100%;">	
										<input id="country_step3" name="country" class="form-control input-lg" type="hidden"/>
										<script type="text/javascript">
											function initialize() {
											var options = {types: ['(regions)']};
											var input = document.getElementById('searchTextField');
											var autocomplete = new google.maps.places.Autocomplete(input , options);
											
											google.maps.event.addListener(autocomplete, 'place_changed', 
												function() {
												var address_components=autocomplete.getPlace().address_components;
												var city='';
												var country='';
												for(var j =0 ;j<address_components.length;j++)
												{
												city =address_components[0].long_name;
												if(address_components[j].types[0]=='country')
														{
															country=address_components[j].long_name;
														}
													}
													//document.getElementById('data').innerHTML="City Name : <b>" + city + "</b> <br/>Country Name : <b>" + country + "</b>";
													document.getElementById('country_step3').value = country;
													document.getElementById('city_step3').value = city;
												}
											);
										}
										google.maps.event.addDomListener(window, 'load', initialize);
										</script>
										<style>
										.pac-container {
											z-index: 1050;
											top:185px !important;
										}
										@media(max-width: 991px){
											.pac-container {
												top:250px !important;
											}
										}
										@media(max-width: 480px){
											.pac-container {
												top:150px !important;
											}
										}
										</style>
							    	</td>
							    	<td class="col-1 col-sm-1 col-md-1" align="right"><a href="#" onclick="editCountry()" id="editCountry" style="color:#fff">Edit</a></td>
							    </tr>
							    <tr>
							    	<td class="col-3 col-md-3 col-sm-3">City</td>
							    	<td colspan="3" class="col-md-8 col-sm-8 col-8">
							    		<input id="city_step3" name="cities"  style="background:transparent;border-style: none;width: 100%;" type="text" readonly />
							    	</td>
							    	<td class="col-1 col-sm-1 col-md-1" align="right"><a href="#" onclick="editCity()" id="editCity" style="color:#fff">Edit</a></td>
							    </tr>
							    <tr>
							    	<td class="col-3 col-md-3 col-sm-3">Birthday</td>
							    	<td class="col-md-4 col-sm-4 col-4">
										<div class="date-input">
										    <p class="date-value" style="background: transparent;color:#fdfdfd; height: 100%; margin-bottom: 0px;"></p>
										    <input name="bday" class="date-pc hidden-sp" type="text" style="background:transparent;border-style: none;width: 100%;height: 100%">
										</div>	
							    	</td>
							    	<td class="col-1 col-md-1 col-sm-1">Gender</td>
							    	<td colspan="2" class="col-md-4 col-sm-4 col-4" style="padding-left: 5px;">
							    		<style type="text/css">
											select::-ms-expand {
											    display: none;
											}
											select {
										    -webkit-appearance: none;
										    -moz-appearance: none;
										    text-indent: 1px;
										    text-overflow: '';
										}
										</style>
							    		<select id="gender" class="gender" name="gender"  style="background:transparent;border-style: none; width: 100%;">
											<option value="Male" style="background:#5f5f5f;border-style: none;width: 100%">Male</option>
											<option value="Female" style="background:#5f5f5f;border-style: none;width: 100%">Female</option>
										</select>	
							    	</td>
							    	
							    </tr>
							    <tr>
							    	<td class="col-3 col-md-3 col-sm-3 link">Terms & Condition</td>
							    	<td class="col-md-5 col-sm-5 col-5"><input class="use_email"  type="radio" name="terms"  value="1" checked="checked"><span class="span-step-3">&nbsp Yes, I agree to continue using the website.</span></td>
							    	<td colspan="3" class="col-2 col-md-2 col-sm-2"><input class="use_email" type="radio" name="terms"  value="0" ><span class="span-step-3">&nbsp No, I disagree.</span></td>
							    </tr> 
							</tbody>
							</table>
							
						<div class="btn-pre-next">
							<?php 
							if ($form_button_prev_visible || $form_button_prev_visible_steps) {?>
							<input type="button" name="previous" class="signup-pre-btn mg-b20 mgin-r-10" value="Previous" />
							<?php } else {?>
							<div class="signup-pre-btn mg-b20" style="width: 65px; visibility: hidden; margin:0; padding:0;"></div>
							<?php }?>
							<input type="button" name="next" class="signup-next-btn form_signup_process_next mg-b50 mgin-r-10" value="Next" />
							<input type="button" name="next" class="signup-next-btn form_signup_process_skip mg-b50" value="Skip" />
							<button type="submit" class="bt_save submit-pro form_signup_process_next mg-b50" >Save</button>
						</div>
					</div>
				</section>
				<!-- END:	STEP 3 -->

				<!-- BEGIN:	STEP 4 -->
				<section class="section-form" data-step="4">
					<h3 class="title-step mg-t30 fix-mg-t30">Step <span class="step_number_displayed">4</span></h3>
					<div class="step4-wrap">
						<div class="step4">
							<div class="row">
								<div class="col-md-6 settings-profile-photo pd0">
									<div class="settings-left" style="max-width:100%">
									<label class="mg-b20">PROFILE PHOTO &nbsp; &nbsp; &nbsp; <input type="button" name="input_avatar_1" value="Select File" class="update-img-upload width-select" id="cropContainerHeaderButton" style="font-family:'Lato-Regular';" /></label><br/>
									 <?php /*get avatar url*/
										$image_url =  get_resource_url('assets/images/default_avatar_male.png');
										$thumbnail_url = get_resource_url('assets/images/default_avatar_male.png');
									?>    
										<div class="text-center">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large" style="width: 100%">
													<div class="col-xs-12s">
														<div>
															<input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
															<input type="hidden" id="input_image_url" name="image_urlAdmincp" value="<?php echo $thumbnail_url ?>">
														</div>
														<div class="img-profile" style="position: relative;">
															<div class="col-sm-3">
																<div id="cropic_element" style="display:none"></div>
																<a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>">
																	<img id="preview_image" height="50" width="50" src="<?=$thumbnail_url?>">
																</a>
																<div class="overclick" style="position: absolute;width: 100%;height: 100%;top: 0;"></div>
															</div>
															<div class="col-sm-9">
																<label class="lb_select">Recommended size: 312 x 210.<BR/>.jpg / .jpeg / .gif / .bmp images only.</label>
															</div>																
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 settings-profile-photo pd0">
									<div class="settings-left" style="max-width:100%">
									<label class="mg-b20">PROFILE PHOTO &nbsp; &nbsp; &nbsp; <input type="button" name="input_avatar_1" value="Select File" class="update-img-upload width-select" id="cropContainerHeaderButton_new" style="font-family:'Lato-Regular';" /></label><br/>
									 <?php /*get avatar url*/
										$image_url_2 = (!empty($val->image_2)) ? get_resource_url($val->image_2) : get_resource_url('assets/images/default_avatar_male.png');
										$thumbnail_url_2 = (!empty($val->thumbnail_2)) ? get_resource_url($val->thumbnail_2) : get_resource_url('assets/images/default_avatar_male.png');
									?>    
										<div class="text-center">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large" style="width: 100%">
													<div class="col-xs-12s">
														<div>
															<input type="hidden" id="input_thumbnail_url_new" name="thumbnail_urlAdmincp_new">
															<input type="hidden" id="input_image_url_new" name="image_urlAdmincp_new" value="<?php echo $thumbnail_url_2 ?>">
														</div>
														<div class="img-profile" style="position: relative;">
															<div class="col-sm-3">
																<div id="cropic_element_new" style="display:none"></div>
																<a class="fancybox-button-new" id="preview_image_fancybox_new" href="<?=$image_url_2?>">
																	<img id="preview_image_new" height="50" width="50" src="<?=$thumbnail_url_2?>">
																</a>
																<div class="overclick" style="position: absolute;width: 100%;height: 100%;top: 0;"></div>
															</div>
															<div class="col-sm-9">
																<label class="lb_select">Recommended size: 312 x 210.<BR/>.jpg / .jpeg / .gif / .bmp images only.</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>			
									</div>
								</div>
							</div>
							<hr style="margin: 0px">	
							<div class="form-group row">
								<label class="user_type_4">
									<p style="margin-bottom: 0px" id="<?=USER_TYPE_VENUE?>" >Venue</p>
									<p style="margin-bottom: 0px" id="<?=USER_TYPE_ARTIST?>" >Artist</p>
									<p style="margin-bottom: 0px" id="<?=USER_TYPE_LABEL?>" >Label</p>
									<p style="margin-bottom: 0px" id="<?=USER_TYPE_PROMOTER?>" >Promoter</p>
									<p style="margin-bottom: 0px" id="<?=USER_TYPE_MARKETER?>" >Marketer</p>
								</label>
								<label class="lb_choose" ">name
								</label>
								<br>
								<input type="text" name="user_type_name" id="user_type_name" value="" style="background:transparent; color: #fff;border:2px solid #fff">
							</div>
							
							<div class="form-group row pd15">
								<label class="lb_city">Short bio</label>
								<textarea class="short_bio"  id="short_bio" name="comment" placeholder="" data-bv-field="comment" rows="4" style="background:transparent; color:#fff; border:2px solid #fff"></textarea>
							</div>
							<script type="text/javascript">
								$('textarea').each(function () {
									this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
								}).on('input', function () {
									this.style.height = 'auto';
									this.style.height = (this.scrollHeight) + 'px';
								});
								
							</script>
    					</div>
						<div class="btn-pre-next">
							<?php if ($form_button_prev_visible || $form_button_prev_visible_steps) {?>
							<input type="button" name="previous" class="signup-pre-btn mg-b20 mgin-r-10" value="Previous" />
							<?php } else {?>
							<div class="signup-pre-btn mg-b20" style="width: 65px; visibility: hidden; margin:0; padding:0;"></div>
							<?php }?>
							<input type="button" name="next" class="signup-next-btn form_signup_process_next mg-b20 mgin-r-10" value="Next" />
							<input type="button" name="next" class="signup-next-btn form_signup_process_skip mg-b50" value="Skip" />
						</div>
					</div>
				</section>
				<!-- END:	STEP 4 -->

				<!-- BEGIN:	STEP 5 -->
				<section class="section-form" data-step="5">
					<h3 class="title-step mg-t30 fix-mg-t30">Step <span class="step_number_displayed">5</span></h3>
					<div class="settings-link">
						<div class="form_step5">
							<div class="form-group row">
								<div class="col-md-6">
									<div class="">
										<label class="lb_name">What is your official URL?</label>
										<input class="t20" style="background: transparent;border: 2px solid #fff; color:#fff" placeholder="" name="url_left" value="https://www" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="fix-ip">
										<label class="lb_name">Genres I Represent</label>
										<style>
											.fix-ip .text-core{
												width: 100% !important;
												height: 30px !important; 
											}
											.fix-ip .text-wrap{
												width: 100% !important;
												height: 30px !important;
											}
											#genre_step5{
												border: 2px solid #fff;
												outline: none;
												padding: 5px 15px;
												width: 100%;
												margin-bottom: 8px;
												color: #fff;
												font-family: 'Lato-Regular';
												font-size: inherit;
												height: 30px;
												font-size: 14px;
												position: initial;
												background:transparent;
											}
											#genre_step5-error{
												float: left;
												width: 100%;
											}
											.styled-select.blue.semi-square .text-wrap{
												position: initial;
											}
											textarea[name="genre_step5"]{
												width: 0;
												height: 0;
												padding: 0;
												margin: 0;
												visibility: hidden;
												background:transparent;
												border: 2px solid #fff;
												color:#fff !important;
											}
											.text-core .text-wrap .text-tags .text-tags-on-top {
												color: #fff !important;
												position: relative;

											}
											select option[class="key_location"]{
												color: #222;
												background: transparent;
											}
											
										</style>
										<!-- <div class="styled-select blue semi-square"> -->
											<textarea id="genre_step5" name="genre_step5"  rows="1" style="background: transparent;border: 2px solid #fff; color:#fff"></textarea>
											<script type="text/javascript">
										<?php
										$genre = modules::run('admincp_events/get_genre');
										$genre_arr = array();
										foreach($genre as $k => $v){
											$genre_arr[] = $v->name;
										}
										$genre_arr_json = json_encode($genre_arr);
										?>
										var genre_arr_json = <?=$genre_arr_json?>;
										</script>
										<!-- </div> -->
										
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6">
									<div class="">
										<label class="lb_name">Key location</label>
										<div class="styled-select blue semi-square">
											<select name="key_location" style="background: transparent;border: 2px solid #fff; color:#fff">
											<?php
											$country_residence = modules::run('home/get_list_countries');
											if(!empty($country_residence)){
												foreach($country_residence as $k => $value){
													?>
													<option class="key_location" value="<?=$value->name?>"><?=$value->name?></option>
													<?php
												}
											}
											?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label class="lb_city">Manage related pages</label>
										<textarea style="background: transparent;border: 2px solid #fff; color:#fff;" class="biography"  name="comment" placeholder="" data-bv-field="comment"></textarea>
										<small style="font-size: 11px;padding-top: 0px" id="bioHelp" class="form-text text-muted">You have management control of these pages.</small>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6">
									<div class="">
										<label class="user_type_4">
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_VENUE?>" >Personal</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_ARTIST?>" >Management</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_LABEL?>" >Label's</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_PROMOTER?>" >Personal</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_MARKETER?>" >Personal</p>
										</label>
										<label class="lb_name"> email</label>
										<input class="t20" placeholder="" name="distributor" value="" type="text" style="background: transparent;border: 2px solid #fff; color:#fff">
									</div>
								</div>
								<div class="col-md-6">
									<div class="">
										<label class="user_type_4">
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_VENUE?>" >General</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_ARTIST?>" >Label's</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_LABEL?>" >Label's</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_PROMOTER?>" >General</p>
											<p style="margin-bottom: 0px" id="<?=USER_TYPE_MARKETER?>" >General</p>
										</label>
										<label class="lb_name"> email</label>
										<input class="t20" placeholder="" name="label" value="" type="text" style="background: transparent;border: 2px solid #fff; color:#fff">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6">
									<div class="">
										<label class="lb_name">Contact number</label>
										<input class="t20" placeholder="" name="contact_info" value="" type="text" style="background: transparent;border: 2px solid #fff; color:#fff">
									</div>
								</div>
								
							</div>
							<div class="row settings-left ">
								<!-- Facebook -->
								<div class="col-md-6 pdl0 hidden">
									<label class="">URL Facebook</label><br/>
									<a class="btn btn-social btn-facebook btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#" rel="nofollow"><i class="fa fa-facebook-official"></i> Sign in with Facebook</a>
								</div>
								<!-- Twitter -->
								<div class="col-md-6 pdr0 hidden">
									<label class="">URL Twitter</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i class="fa fa-twitter"></i> Sign in with Twitter</a>
								</div>
								<!-- Bandcamp -->
								<div class="col-md-6 pdl0 hidden">
									<label class="">URL Bandcamp</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i><img src="<?=get_resource_url('assets/images/bandcamp.png')?>" alt="" width="19px"></i> Sign in with Bandcamp</a>
								</div>
									<!-- Mixcloud -->
								<div class="col-md-6 pdr0 hidden">
									<label class="">URL Mixcloud</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i class="fa fa-mixcloud "></i> Sign in with Mixcloud</a>
								</div>
							</div>
							<div class="row settings-right ">
								<!-- Beatport -->
								<div class="col-md-6 pdl0 hidden">
									<label class="">URL Beatport</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i class="fa fa-youtube-play "></i> Sign in with Mixcloud</a>
								</div>
								<!-- Soundcloud -->
								<div class="col-md-6 pdr0 hidden">
									<label class="">URL Soundcloud</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i class="fa fa-soundcloud "></i> Sign in with Soundcloud</a>
								</div>
								<!-- iTunes -->
								<div class="col-md-6 pdl0 hidden">
									<label class="">URL iTunes</label><br/>
									<a class="btn btn-social btn-twitter btn-sm btn-block margin-bottom-10 no-ajaxloader" href="#">
										<i><img src="<?=get_resource_url('assets/images/icon/music.png')?>" alt="" width="19px"></i> Sign in with iTunes</a>
								</div>
							</div>
						</div>
						<div class="btn-pre-next">
							<?php if ($form_button_prev_visible || $form_button_prev_visible_steps) {?>
							<input type="button" name="previous" class="signup-pre-btn mg-b20 mgin-r-70" value="Previous" />
							<?php } else {?>
							<div class="signup-pre-btn mg-b20" style="width: 65px; visibility: hidden; margin:0; padding:0;"></div>
							<?php }?>
							<button type="submit" class="submit-pro form_signup_process_next">Save</button>
						</div>
					</div>
				</section>
				<!-- END:	STEP 5 -->
			</form>
			<!-- END: SIGNUP FORM -->
			
			
			<div id="menu1" class="tab-pane fade">
				<h3>SUBSCRIPTION</h3>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
			<div id="menu2" class="tab-pane fade">
				<h3>REPORTS</h3>
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
			</div>
			<div id="menu3" class="tab-pane fade">
				<h3>NEWS</h3>
				<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			</div>
		</div>
	</div>
</div>
	<!-- SIGN UP -->
<script type="text/javascript" src="<?=get_resource_url('assets/js/bootstrap-tagsinput.js')?>"></script>
<script type="text/javascript" src="<?=get_resource_url('assets/js/form_register.js')?>"></script>
<?php
if(!empty($_GET['test'])){
?>
<script type="text/javascript" src="<?=get_resource_url('assets/js/test/is_testing_form.js')?>"></script>



<?php
}
?>

