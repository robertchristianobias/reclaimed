<!DOCTYPE html>
<head>
  <title>Pusher Test</title>

  <link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
  <link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">

  <link href="<?=get_resource_url('assets/css/fstyle.css?ver=1.0')?>" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style.css?ver=1.0')?>" type="text/css" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style2.css?ver=1.0')?>" type="text/css" rel="stylesheet">
  <link href="<?=get_resource_url('assets/css/style3.css?ver=1.0')?>" type="text/css" rel="stylesheet">

  <script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
  <script src="https://js.pusher.com/4.0/pusher.min.js"></script>

  <script>

    $(document).ready(function(){
      const channelName = "<?=$channel?>";
      doReactFeed(channelName);
      submitData(channelName);
      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;
      const pusher = new Pusher("<?=PUSHER_KEY?>", {
        cluster: "<?=PUSHER_CLUSTER?>",
        encrypted: true
      });
      const channel = pusher.subscribe(channelName);
      listenerPusher(channel);
    });
    function listenerPusher(channel) {
      channel.bind("newfeed", function(data) {
        var feedHtml = data.feedHtml;
        var mainFeed = $("#listFeed");
        var newestFeedElement = mainFeed.find("div.box-content-1.box-content-newfeed:first-child");
        if(newestFeedElement.size()) {
          newestFeedElement.before(feedHtml);
        } else {
          mainFeed.html(feedHtml);
        }
      });
      channel.bind("likefeed", function(data) {
        var feedReactStatisticElement = $("#areaFeedReactStatistic"+data.feedId);
        feedReactStatisticElement.find(".feed-total-like").text(data.numLike);
      });

      channel.bind("newcomment", function(data) {
        var commentHtml = data.commentHtml;
        var areaListCommentElement = $("#areaListCommentId"+data.feedId);
        var newestCommentElement = areaListCommentElement.find("div.list_comment_newfeed_detail").last();
        if (newestCommentElement.size()) {
          newestCommentElement.after(commentHtml);
        } else {
          areaListCommentElement.html(commentHtml);
        }
      });

      channel.bind("newreply", function(data) {
        var replyHtml = data.replyHtml;
        var areaListReplyElement = $("#areaListReplyElementId"+data.commentId);
        var newestReplyElement = areaListReplyElement.find('div.list_comment_newfeed_detail_reply_box').last();
        if (newestReplyElement.size()) {
          newestReplyElement.after(replyHtml);
        } else {
          areaListReplyElement.prepend(replyHtml);
        }
      });

      channel.bind("updatefeed", function(data) {/*ToDo*/});

      channel.bind("deletefeed", function(data) {/*ToDo*/});

    }

    function submitData(channelName) {
      //do submit new feed
      $("#newFeedForm").on("submit", function(event){
        event.preventDefault();
        var newFeedContentElement = $(this).find("#newFeedContent");
        var feedData = {
          user: channelName,//"<?=$channel?>",
          content: newFeedContentElement.val(),
        };
        $.ajax({
          type: "POST",
          url: $(this).attr("action"),
          data: feedData,
          success: function(data){
            newFeedContentElement.val("");
          }
        });
      });

      //do submit comment feed
      $("#listFeed").on("submit", "form[id^='commentForm']", function(event) {
        event.preventDefault();
        var submit_comment_url  = $("#listFeed").data("submit-comment-url");
        var commentContentElement = $(this).find("textarea");
        var commentData = {
            user: channelName,//"<?=$channel?>",
            feedId: $(this).find(":input[name='feedId']").val(),
            commentContent: commentContentElement.val(),
        };
        $.ajax({
          type: "POST",
          url: submit_comment_url,
          data: commentData,
          success: function(data){
            commentContentElement.val('');
          }
        });
      });

      //do submit reply comment
      $("#listFeed").on("submit", "form[id^='replyForm']", function(event) {
        event.preventDefault();
        var submit_reply_url = $("#listFeed").data("submit-reply-url");
        var replyContentElement = $(this).find("textarea");
        var replyData = {
          user: channelName,//"<?=$channel?>",
          feedId: $(this).find(":input[name='feedId']").val(),
          commentId: $(this).find(":input[name='commentId']").val(),
          replyContent: replyContentElement.val()
        };
        $.ajax({
          type: "POST",
          url: submit_reply_url,
          data: replyData,
          success: function(data){
            console.log(data);
            replyContentElement.val('');
          }
        });
      });
    }

    function doReactFeed() {
        $("#listFeed").on("click", "div.box-content-newfeed-action > span", function(event){
          var type = $(this).data("action-type");
          var feedId = $(this).closest("div.box-content-1.box-content-newfeed").data("feed-id");

          switch(type) {
            case "like": doLikeFeed(feedId);
              break;
            case "comment": showCommentForm(feedId);
              break;
            case "share": doShareFeed(feedId);
              break;
          }
        });
        $("#listFeed").on("click", ".list_comment_newfeed span.span-reply", function(event) {
          var feedId = $(this).closest("div.list_comment_newfeed_detail").data("feed-id");
          var commentId = $(this).closest("div.list_comment_newfeed_detail").data("comment-id");
          showReplyForm(feedId, commentId);
        });
    }

    function doLikeFeed(feedId) {
      var submit_like_url  = $("#listFeed").data('like-feed-url');
      var likeData = {
          user: "<?=$channel?>",
          feedId: feedId
      };
      $.ajax({
        type: "POST",
        url: submit_like_url,
        data: likeData,
        success: function(data){
          console.log(data);
        }
      });
    }

    function showCommentForm(feedId) {
      var avtUrl = $("#listFeed").data('current-user-avt');
      var areaCommentFormElement = $("#areaCommentFormId"+feedId);
      var idName = 'commentForm'+feedId;
      var formCommentElement = areaCommentFormElement.find("#"+idName);
      if (formCommentElement.length === 0) {
          var element = $("#commentFormPrototype").clone();
          element.attr("id", idName);
          areaCommentFormElement.html(element);
          formCommentElement = element;
      }
      formCommentElement.find('img.comment-img').attr('src', avtUrl);
      formCommentElement.find(":input[name='feedId']").val(feedId);
      if (areaCommentFormElement.hasClass('element-hidden')) {
        areaCommentFormElement.removeClass('element-hidden');
      }
      formCommentElement.find('textarea').focus();
    }

    function showReplyForm(feedId, commentId) {
      var avtUrl = $("#listFeed").data('current-user-avt');
      var areaReplyFormElement = $("#areaReplyFormId"+commentId);
      var idName = 'replyForm'+commentId;
      var formReplyElement = areaReplyFormElement.find("#"+idName);
      if (formReplyElement.length === 0) {
          var element = $("#replyFormPrototype").clone();
          element.attr("id", idName);
          areaReplyFormElement.html(element);
          formReplyElement = element;
      }
      formReplyElement.find('img.comment-img').attr('src', avtUrl);
      formReplyElement.find(":input[name='feedId']").val(feedId);
      formReplyElement.find(":input[name='commentId']").val(commentId);
      if (areaReplyFormElement.hasClass('element-hidden')) {
        areaReplyFormElement.removeClass('element-hidden');
      }
      formReplyElement.find('textarea').focus();
    }

    function doShareFeed(feedId) {

    }

    function renderCommentFrm() {

    }

    function renderReplyFrm() {

    }



  </script>
  <style type="text/css">
    .form-group{
      margin-bottom:0px;
    }
    form.newfeed-form {
        position: relative;
    }
    #newFeedContent{
        max-width: 100%;
        color: #000;
    }
    #mainFeed {
      background-color: #888888;
    }

    .element-hidden {
      display: none;
    }
  </style>
</head>
<body>
<center style="color:white;">
<h4>Test UserID: <?=$user_id?> --- Test ChannelID: <?=$channel?></h4>
</center>
  <div class="main-content">
      <!-- begin frm new feed -->
      <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
              <form id="newFeedForm" class="newfeed-form" method="POST" 
                      action="<?=$new_feed_url?>">
                  <textarea data-required="1" cols="" rows="8" 
                      name="newFeedContent" id="newFeedContent" required=""></textarea>
                  <input type="hidden" name="id_user_info" value="2">
                  <div class="clearfix"></div>
                  <div class="form-button-submit">
                      <button type="submit">Submit</button>
                      <button type="reset">Cancel</button>
                  </div>
              </form>
          </div>    
      </div>
      <!-- End frm new feed -->
      <div id="listFeed" class="ajax-load-newfeed" 
        data-load-newfeed="http://redm.dev/en/ajax_loadnewfeed/2"
        data-submit-feed-url="<?=$new_feed_url?>"
        data-like-feed-url="<?=$like_url?>"
        data-submit-comment-url="<?=$new_comment_url?>"
        data-submit-reply-url="<?=$new_reply_url?>"
        data-current-user-avt="<?=get_resource_url('/assets/uploads/images/2017/05/thumb_avatar_1495013663_thumbnail.jpeg')?>">

      </div>

      <div id="clone_component" style="display: none; visibility: true;">
          <form id="commentFormPrototype" action="" method="POST" class="form-comment-newfeed">
              <div class="mg-t10 mg-b10">
                  <img class="comment-img" src="" alt="" width="60" height="60">
                  <input type="hidden" name="id_user_info" value="">
                  <input type="hidden" name="id_newfeed" value="">
                  <input type="hidden" name="feedId" value="">
                  <textarea id="commentContent" name="commentContent" placeholder="Write Comment..." required="" rows="4"></textarea>
              </div>
              <div class="comment-action">
                  <input type="reset" name="reset" value="Cancel">
                  <input type="submit" name="comment" value="Post">
                  <div class="clearfix"></div>
              </div>
          </form>
          <form id="replyFormPrototype" action="" method="POST" class="form-comment-newfeed">
              <div class="mg-t10 mg-b10">
                  <img class="comment-img" src="" alt="" width="60" height="60">
                  <input type="hidden" name="id_user_info" value="">
                  <input type="hidden" name="id_newfeed" value="">
                  <input type="hidden" name="feedId" value="">
                  <input type="hidden" name="commentId" value="">
                  <textarea id="replyContent" name="replyContent" placeholder="Write Comment..." required="" rows="4"></textarea>
              </div>
              <div class="comment-action">
                  <input type="reset" name="reset" value="Cancel">
                  <input type="submit" name="comment" value="Post">
                  <div class="clearfix"></div>
              </div>
          </form>
      </div>
  </div>
</body>