    <?php
	function getCurrentPageURL() {
		$pageURL = 'https';

		if (!empty($_SERVER['HTTPS'])) {
		  if ($_SERVER['HTTPS'] == 'on') {
			$pageURL .= "s";
		  }
		}

		$pageURL .= "://";

		if ($_SERVER["SERVER_PORT"] != "80") {
		  $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
		} else {
		  $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	$current_url = getCurrentPageURL();
	?>
	
	
	<?php
    foreach ($articles_blog as $result) {
    // result category
    $category_id = $result->category_id;
    $articles_id = $result->id;
    ?>

		<div class="banner-blog" style="margin-top:40px">
            <?php if( $result->image_blog != ""  ) { ?>
                <div style="text-align:center">
                <img class="" src="<?php echo get_resource_url($result->image_blog) ?>" alt="<?php echo $result->title ?>" style="width:100%;max-width:100%;max-height:100%    ">
                </div>
            <?php } else { ?> 
                
                  <img class="img-top-blog mw-100pt" src="<?php echo get_resource_url($result->image) ?>" alt="<?php echo $result->title ?>">
              
            <?php } ?>
			<?php
			$array_tags = explode(',', $result->tags);
			if(!empty($result->tags)){
			?>
            <ul class="listBlog">
			<?php foreach($array_tags as $val){?>
                <li><a href="#"><span><?=$val?></span></a></li>
			<?php } ?>
            </ul>
			<?php
			}
			?>
        </div>



    <div class="main-content redm-blog">
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<div class="sharethis-inline-share-buttons"></div>
        <!-- <div class="sharethis-inline-share-buttons_fix justify-content-center">
            <div data-network="facebook" class="st-custom-button">Facebook</div>
            <div data-network="tweet" class="st-custom-button">Tweet</div>
            <div data-network="pin" class="st-custom-button">Pin</div>
            <div data-network="email" class="st-custom-button">Email</div>
            <div data-network="share" class="st-custom-button">Share</div>
            <div data-network="share" class="st-custom-button">Share</div>
        </div> -->
		
        <div class="title-classic">
            <h3><?=$result->title?></h3> 
        </div>

        <div class="content-center-blog">
            <div class="text-center-content-blog">
               <?php echo str_replace( array("#222222","black"),"white",$result->content) ?>
            </div>
            
        </div>


        <div class="share-addthis">
             <div class="pull-left">Share This Story, Choose Your Platform!</div>
            <div class="pull-right">
                  <!-- Go to www.addthis.com/dashboard to customize your tools -->
                   <div class="addthis_inline_share_toolbox addthis_inline_share_toolbox_css"></div>
            </div>
        </div>
    <?php
    }
    ?>
        <div class="comment-blog <?php if(isset($hidden)){ echo 'hidden'; } ?>">
            <input type="hidden" value="<?php ($this->session->userdata('start'))? print $this->session->userdata('start') : print 0 ?>" id="start" />
             <input type="hidden" value="10" id="per_page">
            <h2 class="comment-title">
			<?php if (isset($this->session->userdata['userData'])) {?>
			<?php 
			} 
				else{
			?>	
                <!--<span>You must be <a href="<?=PATH_URL.'en?lg-rdt='.$current_url?>">logged in</a> to post a comment.</span>-->
			<?php
				}				
			?>
            </h2>

            <div class="clear-fix"></div>
            <?php if(isset($this->session->userdata['userData'])){
            foreach ($userDataSetting1 as $value ){
                $username =  $value->username;
                $image = $value->image;
                $user_info_id = $value->id;
            }
            $id_artcles_item_lang = $articles_id;
            ?>
            
            <div class="comment-write mg-t20">
                <div class="name-conversation"><a href="#overview"><?php echo $username ?></a> join the conversation</div>
                <form action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/comments" method="POST" id="form-user-comment">


                <div class="mg-t10 mg-b10">
                    <img class="comment-img" src="<?=get_resource_url($image)?>" alt="<?php echo $username ?>" width="60" height="60" />
                    <input type="hidden" name='user_info_id' value="<?php echo $user_info_id ?>">
                    <input type="hidden" name='id_artcles_item_lang' value="<?php echo $id_artcles_item_lang ?>">
                    <textarea id="comment-box" name="comment" placeholder="Write Comment..." required value=""></textarea>
                </div>
                <div class="comment-action">
                    <input type="reset" name="reset" value="Cancel" />
                    <input type="submit" name="comment" value="Post"/>
                    <div class="clearfix"></div>
                </div>
                </form>
                
            </div>
    

            <?php  } ?>
             <div class="clear-fix"></div>


            <div class="comment-show mg-t20">
                <span class="comment-total">
                </span>

                <ol id="ajax_loadcomment" data-ajax-load-comment="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/ajax_comment" data-ajax-load-comment-id="<?php echo $articles_id ?>">
                    <!-- ajax load comment -->
                    
                </ol>
                

          <!--       <div class="comment-next-prev">
                    <ul class="pagination-pix padding-top-50">
                        <?=$paginator?>
                    </ul>
                 </div> -->                                                                                                                                                                                                                                                                                                             

                <div class="comment-next-prev hidden">
                    <ul class="pagination-pix padding-top-50">
                        <li><a class="first" href="#">« Previous</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a class="last" href="#">Next »</a></li>
                    </ul>
                </div>


            </div>
        </div>
        <div class="popup-modal-signup">
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                
                  <div class="modal-body">
                    <p></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
                  </div>
                </div>

            </div>
        </div>

     
    </div>
        <div class="title mg-t30">

            <h3 class="text-read-also">READ ALSO</h3>
        </div>

        <div class=" list-post" style="padding-left:15px;padding-right:15px;">
            <div class="carousel">
                <div class="carousel-items">
                
                <?php 
                $articles_blog_category = modules::run('home/get_articles_blog_category', $category_id, $articles_id);
                foreach ($articles_blog_category as $result) {   
                ?>
                    <div class="slider-item">
                        <div class="item mg-b20 item-latest-slick">
                            <div class="wrap-image">
                            <?php if($this->lang->default_lang() == 'en') { ?>
                                <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>">
                            <?php } else{ ?>
                                <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>">   
                            <?php  } ?>
                            <img src="<?php echo get_resource_url(substr($result->image, 1)) ?>" alt="<?php echo $result->title ?>" class="image-post" style="object-fit: cover">
                            </a>
                    
                          
                            </div>
                            <div class="text-content">
                                <p class="hidden"><?php if($this->lang->default_lang() == 'en') { ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="tag-a-title1 p-fs-20">
                                    <?php } else{ ?>
                                        <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="tag-a-title1 p-fs-20">   
                                    <?php } ?><?php echo $result->title ?></a>
                                </p>
                                <p><?php echo the_excerpt($result->description, 16);  ?></p>
                                <?php if($this->lang->default_lang() == 'en') { ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_en)  ?>" class="read-more">
                                <?php } else{ ?>
                                    <a href="<?php echo get_url_language('/magazine/'. $result->slug_cn)  ?>" class="read-more">   
                                <?php } ?><i class="fa fa-external-link" aria-hidden="true"></i> Read more</a>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                    </div>
                <?php
                   
                }
                ?>
                </div>
                <div class="carousel-prev prev">
                    <i class="fa fa-caret-left" aria-hidden="true"></i>
                </div>
                <div class="carousel-next next">
                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--<img src="<?=get_resource_url('assets/images/ad2.png')?>" class="mg-b20 ad-single-image">-->

    

    </div>
    
   
<style>
.list-post .wrap-image{
    padding-top: 46%;
}
</style>

