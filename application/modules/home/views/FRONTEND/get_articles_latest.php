<?php
// latest
// $latest = modules::run('home/get_articles_latest'); // Not used
$i = 0;
foreach ($result as $obj){
$i++;

?>
<div class="item item-latest mg-b20">
	<div class="wrap-image">
	   <?php if($this->lang->default_lang() == 'en') { ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_en)  ?>" >
		<?php } else{ ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_cn)  ?>">   
		<?php  } ?>
			<img src="<?php echo get_resource_url(substr($obj->image, 1)) ?>" alt="<?php echo $obj->title ?>" class="image-post">
		</a>
	</div>
	<div class="text-content">
		<?php
			$array_tags = explode(',', $obj->tags);
			if(!empty($obj->tags)){
				?>
				<ul class="listFeatured">
				<?php 
				foreach($array_tags as $val){
					?>
					<li><a href="#"><span><?=$val?></span></a></li>
				<?php
				}
				?>
				</ul>
		<?php } ?>
		<p class="hidden">
		<?php if($this->lang->default_lang() == 'en') { ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_en)  ?>" class="tag-a-title1 p-fs-20">
		<?php } else{ ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_cn)  ?>" class="tag-a-title1 p-fs-20">   
		<?php } ?>
		<?php echo $obj->title ?></a>
		 </p>
		<p class="post-p-des" style="min-height:110px;word-wrap: break-word;"><?php echo $obj->description;  ?></p>
		<?php if($this->lang->default_lang() == 'en') { ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_en)  ?>" class="read-more">
		<?php } else{ ?>
			<a href="<?php echo get_url_language('/magazine/'. $obj->slug_cn)  ?>" class="read-more">   
		<?php } ?>Read more</a>
		<div class="clear-fix"></div>
	</div>
</div>
<?php if($i%2 == 0) { ?>
<div class="clearfix hidden-md hidden-lg"></div>
<?php } ?>
<?php if($i%3 == 0) { ?>
<div class="clearfix hidden-xs hidden-sm"></div>
<?php } ?>

<?php } ?>