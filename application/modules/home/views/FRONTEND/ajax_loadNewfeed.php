<?php 
function time_newfeed($time){
  $time_newfeed = time() - strtotime($time);
  if($time_newfeed < 60 ){
    return 'Just now';
  } else if($time_newfeed < 3600 ) { 
    return floor(($time_newfeed)/60) .' mins';
  } else if($time_newfeed >= 3600 && $time_newfeed < 86400 ){ 
    return floor(($time_newfeed)/3600) .' hrs';
  } else if($time_newfeed >=  86400 && $time_newfeed < 172800 ){ 
    return floor(($time_newfeed)/86400) .' days';
  } else{
    return date('H:i d-m-Y', strtotime($time));
  }
} ?>

<?php
$i = 1; 
foreach($profile as $result ){
 // id newfeed
$id_artcles_newfeed = $result->id;    
?>
   <div class="box-content-1 box-content-newfeed col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box-content-newfeed-header">
            <div class="newfeed-avt">
            <img src="<?=get_resource_url($result->thumbnail)?>" alt="<?php echo $result->thumbnail ?>" width="60" height="60" />
            </div>
            <div class="newfeed-name">
                <p class="mg-b0"><?php echo $result->username ?></p>
                <p>
                    <?php echo time_newfeed($result->created) ?>

                    <?php if($result->status == '1'){ ?> 
                         <i class="fa fa-globe fa-newfeed" aria-hidden="true"></i>
                    <?php } else { ?>
                        <!-- private -->
                    <?php } ?>
                </p>    
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="box-content-newfeed-content">
             <?php echo $result->content ?>   

        </div>

        <div class="clearfix"></div>

        <div class="box-content-newfeed-total-action">
            <span class="span-newfeed-total-like">
                <span>10</span> Like
            </span> 
             <span class="span-newfeed-total-like">
                <span>5</span> Comment
            </span> 
             <span class="span-newfeed-total-like">
                <span>3</span> Share
            </span> 
        </div>

        <div class="box-content-newfeed-action">
            <span class="span-newfeed-like">
               <i class="fa fa-thumbs-up" aria-hidden="true"></i> Like
            </span>
            
             <span class="span-newfeed-comment">
              <i class="fa fa-comment" aria-hidden="true"></i> Comment
            </span>

             <span class="span-newfeed-share">
               <i class="fa fa-share" aria-hidden="true"></i> Share
            </span>
        </div>


        <!-- write comment -->
        <div class="clear-fix"></div>
        <?php if(isset($this->session->userdata['userData'])){
        foreach ($userDataSetting1 as $value ){
            $username =  $value->username;
            $thumbnail = $value->thumbnail;
            $user_info_id = $value->id;
        }
        
        ?>

        <div class="comment-write comment-write-profile mg-t20">
            <form action="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/comments_newfeed" method="POST" class="form-comment-newfeed">
                <div class="mg-t10 mg-b10">
                    <img class="comment-img" src="<?=get_resource_url($thumbnail)?>" alt="<?php echo $username ?>" width="60" height="60" />
                    <input type="hidden" name='id_user_info' value="<?php echo $user_info_id ?>">
                    <input type="hidden" name='id_newfeed' value="<?php echo $id_artcles_newfeed ?>">
                    <textarea id="comment-box" name="comment" placeholder="Write Comment..." required rows="4"></textarea>
                </div>
                <div class="comment-action">
                    <input type="reset" name="reset" value="Cancel" />
                    <input type="submit" name="comment" value="Post"/>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>

       
        <?php } ?>
        <div class="clear-fix"></div>
        <!-- end write comment -->


        <!-- list comment -->
        <div class="clear-fix"></div>
            <div class="list_comment_newfeed list_comment_newfeed_<?php echo $i ?>" data-ajax-load-comment="<?php echo PATH_URL ?><?php echo $this->lang->default_lang() ?>/ajax_comment_newfeed" data-ajax-load-comment-id="<?php echo $id_artcles_newfeed ?>">
                <!-- ajax load data - comment newfeed-->
            </div>
        <div class="clear-fix"></div>
        <!-- end list comment -->


        

    </div><!-- box-content-newfeed -->    
<?php $i++; } ?>


<script>

// Newfeed: Submit form comment and load box comment
$(".form-comment-newfeed").submit(function(e) {
    var url = $(this).attr('action'); 
    $.ajax({
           type: "POST",
           url: url,
           data: $(this).serialize(), 
         });

    e.preventDefault();
    $('.form-comment-newfeed textarea').val('');
    setTimeout(function(){
        $(".list_comment_newfeed").load();
    },100);
});



// Newfeed: load data- comment new feed
jQuery(document).ready(function($) {

    // Length of class = Total Posts
    var len = $(".list_comment_newfeed").length;
    //console.log(len);

    // Let ( Not var i )
    for(let i = 1; i <= len; i++){
        $(".list_comment_newfeed_" + i ).load(function(e) {
            var url = $(this).attr('data-ajax-load-comment');
            $.ajax({
                type: "POST",
                url: url,
                    data:'id_artcles_newfeed=' + $(this).data('ajax-load-comment-id'), 
                    success: function(data){
                        $(".list_comment_newfeed_" + i).html(data);


                        $("[data-ajax-load-comment-link]").click(function(e) {
                            var url = $(this).data('ajax-load-comment-link'); // /(en|cn)/ajax_comment_newfeed_reply
                            var datas = $(this).data('comment-child-reply');
                            
                            var data1 = $(this).data('comment-artcles');
                            var data2 = $(this).data('comment-parent');

                            $.ajax({
                                   type: "POST",
                                   url: url,
                                   data:'id_artcles_newfeed=' + data1 + '&id_comment_parent=' + data2, 
                                   success:function(result){

                                        if($('.list_comment_newfeed_detail_reply_comment').hasClass('active')){
                                            $('.list_comment_newfeed_detail_reply_comment').removeClass('active');
                                        };
                                        
                                        $('.list_comment_newfeed_detail_reply_comment[data-comment-parent="' + data2 + '"]').addClass('active').html(result);

                                   
                                        // load commnet reply
                                        $(".form-comment-newfeed").submit(function(e) {
                                            var url = $(this).attr('action'); 
                                            $.ajax({
                                                   type: "POST",
                                                   url: url,
                                                   data: $(this).serialize(), 
                                                 });

                                            e.preventDefault();
                                            $('.form-user-comment-reply textarea').val('');
                                            setTimeout(function(){
                                                $(".list_comment_newfeed").load();
                                            },100);
                                        });

                                   } // end success
                            });

                            e.preventDefault();
                        }); //

                    } // end sucesss
            });
            e.preventDefault();
        });
        // newfeed: Default load comment    
        $(".list_comment_newfeed_" + i).load(); 
    }
    
})

</script>