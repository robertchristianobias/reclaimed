	<link rel="icon" href="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>" type="image/x-icon" />
    <link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick-theme.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/fstyle.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style.css?ver=1.0')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style2.css?ver=1.0')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style3.css?ver=1.0')?>" type="text/css" rel="stylesheet">
	
	<script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>

<div class="profile-setting-detail profile--tab">
    <div class="profile-wrap">
        <div class="tab-content">
            <div class="tab-profile pd-t60">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#setting1">Account settings</a></li>
                    <li><a data-toggle="tab" href="#setting2">Change password</a></li>
                    <li><a data-toggle="tab" href="#setting3">subscription settings</a></li>
                    <li><a data-toggle="tab" href="#setting4">Privacy settings</a></li>
                    <li><a data-toggle="tab" href="#setting5">Add content</a></li>
                    <li class="close-profile"><a href="#">close</a></li>
                </ul>
            </div>
            <div id="setting1" class="account-settings tab-pane fade in active">
                <form class="form-account-settings" method="post" id="account-setting-form">
                    <?php 
                        foreach($userDataSetting1 as $key => $val){
                        ?>
                    <input type="hidden" name="id" value="<?=$this->session->userdata['userData']['id']?>">
                    <div class="settings-country-avatar mg-t50" >
                        <label>RA Username /  You cannot change pixinteraction</label>
                        <label>Default region </label><br/>
                        <div class="settings-left">
                            <input class="mg-t20" placeholder="Enter new url to change" name="url" value="<?=$val->url?>" type="text">
                        </div>
                        <div>
                            <input class="mg-t20" placeholder="Enter new country to change" name="location" value="<?=$val->location?>" type="text" id="country-user">
                            <input type="hidden" name="country" value="<?=$val->country ?>" type="text" id="country-hide">
                            <input type="hidden" name="city" value="<?=$val->city ?>" type="text" id="city-hide">
                        </div>
                    </div>
                    <div class="line-account-settings mg-t30"></div>
                    <div class="clear-fix"></div>
                    <div class="settings-profile-photo">
                        <div class="settings-left">
                            <label class="mg-t50 mg-b20">PROFILE PHOTO</label><br/>
                            <?php /*get avatar url*/
                                $image_url =( ! empty($val->image) ) ? get_resource_url($val->image) : null;
                                   
                                
                                $thumbnail_url = ( ! empty($val->thumbnail) ) ? get_resource_url($val->thumbnail) : null;
                                
                                
                                    ?>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="col-md-8 col-xs-12">
                                            <div>
                                                <input type="button" name="input_avatar" value="Select File" class="update-img-upload width-select" id="cropContainerHeaderButton" />
                                                <!-- image thumbnail -->
                                                <input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
                                                <!-- image original -->
                                                <input type="hidden" id="input_image_url" name="image_urlAdmincp" value="<?php echo $thumbnail_url ?>">
                                            </div>
                                            <div class="img-profile">
                                                <div id="cropic_element" style="display:none"></div>
                                                <a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>">
                                                <img id="preview_image" height=150 width=150 src="<?=$thumbnail_url?>"> 
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label>Recommended size: 312 x 210.<BR/>.jpg / .jpeg / .gif / .bmp images only.</label>
                        </div>
                        <div class="settings-right">
                            <label class="mg-t50 mg-b20">OR ENTER AN EXTERAL IMAGE URL</label><br/>
                            <input placeholder="Enter new url images to upload"  type="text" readonly="readonly" name="thumbnail" value="<?= $image_url?>"><br/>
                            <label class="mg-t50 mg-b20">OR ENTER AN EXTERAL IMAGE URL</label><br/>
                            <input placeholder="Enter new url images to upload"  type="text" readonly="readonly" name="thumbnail" value="<?= $image_url?>"><br/>
                        </div>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="line-account-settings mg-t30"></div>
                    <div class="settings-comment">
                        <div class="settings-left">
                            <label class="mg-t40 mg-b5">Age</label><br/>
                            <select name="age" class="select-age select-age-ml20">
                                <?php for($i=1;$i<=100;$i++){ ?>
                                <option value="<?php echo $i ?>" <?php echo ($i == $val->age ) ? 'selected' : '' ?>><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="settings-right">
                            <label class="mg-t40 mg-b5">Gender</label><br/>
                            <select name="gender" class="select-age">
                                <option value="1" <?php echo ($val->gender == '1' ) ? 'selected' : '' ?> >Female</option>
                                <option value="2" <?php echo ($val->gender == '2' ) ? 'selected' : '' ?>>Male</option>
                                <option value="3" <?php echo ($val->gender == '3' ) ? 'selected' : '' ?>>Unknown</option>
                            </select>
                        </div>
                    </div>
                    <div class="settings-comment">
                        <label class="mg-t40 mg-b5">Link Url Redm:</label><br/>
                        <label class="mg-b5"><?php echo get_resource_url('en/profile/')  ?>
                        <label>
                            <input value="<?=$val->link_url_redm?>" name="link_url_redm" rows="2" type="text">
                    </div>
                    <div class="settings-comment">
                    <label class="mg-t40 mg-b10">Biography</label>
                    <textarea value="<?=$val->biiography?>" name="biography"></textarea>
                    </div>
                    <div class="settings-name">
                        <div class="settings-left">
                            <label class="mg-t40 mg-b10">Status</label><br/>
                            <textarea name="status_genenal" placeholder="Enter new status to change" type="text" value="<?=$val->status_genenal?>"  rows="2"></textarea>
                        </div>
                        <div class="settings-right">
                            <label class="mg-t40 mg-b10">Short bio</label><br/>
                            <textarea name="shortbio" placeholder="Enter new shortbio to change" type="text" value="<?=$val->shortbio?>"  rows="2"></textarea>
                        </div>
                    </div>
                    <div class="settings-name">
                        <div class="settings-left">
                            <label class="mg-t40 mg-b10">Founder</label><br/>
                            <input class="mg-t20" placeholder="Enter new founder to change" name="founder" value="<?=$val->founder?>" type="text">
                        </div>
                        <div class="settings-right">
                            <label class="mg-t40 mg-b10">Marketing</label><br/>
                            <input class="mg-t20" placeholder="Enter new marketing to change" name="marketing" value="<?=$val->marketing?>" type="text">
                        </div>
                    </div>
                    <div class="settings-name">
                        <div class="settings-left">
                            <label class="mg-t40 mg-b10">Distributor</label><br/>
                            <input class="mg-t20" placeholder="Enter new distributor to change" name="distributor" value="<?=$val->distributor?>" type="text">
                        </div>
                        <div class="settings-right">
                            <label class="mg-t40 mg-b10">Contact</label><br/>
                            <input class="mg-t20" placeholder="Enter new contact to change" name="contact" value="<?=$val->contact?>" type="text">
                        </div>
                    </div>
                    <div class="settings-link">
                        <div class="settings-left">
                            <label class="mg-t40 mg-b10">URL Facebook</label><br/>
                            <input placeholder="Enter new url facebook to change" type="text" value="<?=$val->facebook?>" name="facebook" />
                            <label class="mg-t40 mg-b10">URL Twitter</label><br/>
                            <input placeholder="Enter new url twitter to change" type="text" type="text" value="<?=$val->twitter?>" name="twitter" />
                            <label class="mg-t40 mg-b10">URL Bandcamp</label><br/>
                            <input placeholder="Enter new url bandcamp to change" type="text" type="text" value="<?=$val->bandcamp?>" name="bandcamp" />
                            <label class="mg-t40 mg-b10">URL Mixcloud</label><br/>
                            <input placeholder="Enter new url mixcloud to change" type="text" type="text" value="<?=$val->mixcloud?>" name="mixcloud" />
                        </div>
                        <div class="settings-right">
                            <label class="mg-t40 mg-b10">URL Beatport</label><br/>
                            <input placeholder="Enter new url beatport to change" type="text" type="text" value="<?=$val->beatport?>" name="beatport" />
                            <label class="mg-t40 mg-b10">URL Soundcloud</label><br/>
                            <input placeholder="Enter new url soundcloud to change" type="text" type="text" value="<?=$val->soundcloud?>" name="soundcloud" />
                            <label class="mg-t40 mg-b10">URL iTunes</label><br/>
                            <input placeholder="Enter new url itunes to change" type="text" type="text" value="<?=$val->itunes?>" name="itunes" />
                            <div class="btn-profile">
                                <input type="submit" id="btn-update-account" name="Submit" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" data-url="<?=PATH_URL?>" value="Submit" />
                            </div>
                        </div>
                    </div>
                    <?php  } ?> 
                </form>
            </div>
            <div id="setting2" class="change-pwd tab-pane fade">
                <!-- <div class="descript-pwd">To change your password, enter your current password and your new password below. If you do not know your current password you can request a reset link here.</div> -->
                <div class="form-change-password">
                    <label class="mg-t40 mg-b20">CURRENT PASSWORD</label>
                    <input type="PASSWORD" name="currentPass" id = "currentPass" value=""  data-required="true" data-error="Please enter your country"/>
                    <label class="mg-t40 mg-b20">NEW PASSWORD</label>
                    <input type="PASSWORD" name="newPass" id = "newPass" value=""  data-required="true" data-error="Please enter your country"/>
                    <label class="mg-t40 mg-b20">CONFIRM PASSWORD</label>
                    <input type="PASSWORD" name="confirmNewPass" id = "confirmNewPass" value=""  data-required="true" data-error="Please enter your country"/>
                </div>
                <div class="btn-profile">
                    <button onclick="save()" name="save" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" >Save</button>
                </div>
            </div>
            <div id="setting3" class="subscription-settings tab-pane fade">
                <form id="subscription-setting-form" class="form-subscription-settings" method="post">
                    <?php 
                        foreach($userDataSetting1 as $key => $value)
                            {
                                $id = $value->id;
                                $global_newsletter = $value->global_newsletter;
                                $local_newsletter = $value->local_newsletter;
                                $auto_location = $value->auto_location;
                                $forum_threads = $value->forum_threads;
                                $private_messages = $value->private_messages;
                                $friend_request_notify = $value->friend_request_notify;
                                $private_messages_notify = $value->private_messages_notify;
                        if(!empty($userDatalocation)){
                        $local1 = $userDatalocation[0]->local;
                        $local2 = $userDatalocation[1]->local;
                        $local3 = $userDatalocation[2]->local;
                        $local4 = $userDatalocation[3]->local;
                        }
                        else{
                        $local1 = '';
                        $local2 = '';
                        $local3 = '';
                        $local4 = '';
                        }
                        ?>
                    <input name="id" value="<?=$id?>" type="hidden">
                    <div class="credit-card mg-t30">
                        <div class="card-details mg-b30">
                            <div class="mg-t30">
                                <label>CARD HOLDER'S NAME</label><br/>
                                <input type="text" name="carddetails" placeholder="Card Holder's Name" ><br/>
                            </div>
                            <div class="mg-t30">
                                <label>CARD EXPIRY DATE</label><br/>
                                <select>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <span>/</span>
                                <select>
                                    <option>2017</option>
                                    <option>2018</option>
                                    <option>2019</option>
                                </select>
                            </div>
                            <div class="mg-t30">
                                <label>CARD NUMBER</label><br/>
                                <input type="text" name="carddetails" placeholder="Card Number" ><br/>
                            </div>
                            <div class="mg-t30">
                                <label>CVV/CVV2</label><br/>
                                <input type="text" name="carddetails" placeholder="Card Holder's Name" ><br/>
                            </div>
                        </div>
                    </div>
                    <div class="mg-t30">
                        <label>GLOBAL NEWSLETTER</label><br/>
                        <?php
                            if($global_newsletter == 1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="global-newsletter" value="1">Receive the weekly global newsletter.<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="global-newsletter" value="1">Receive the weekly global newsletter.<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>LOCAL NEWSLETTER</label><br/>
                        <label>The weekly local mailer covering upcoming events and other regional based content.You can subscribe to up to four regions below. To unsubscribe deselect the "Receive the above local newsletters" check box below.</label>
                        <div>
                            <div class="newsletter-style">
                                <input data-required="1" type="text" class="form-control" 
                                    name="locationAdmincp" 
                                    id="locationAdmincp" 
                                    value="<?=$local1?>"
                                    />
                                <input id="countryAdmincp" name="countryAdmincp" class="form-control input-lg" type="hidden"/>
                                <input id="citiesAdmincp" name="citiesAdmincp" class="form-control input-lg" type="hidden"/>
                            </div>
                            <div class="newsletter-style">
                                <input data-required="1" type="text" class="form-control" 
                                    name="locationAdmincp1" 
                                    id="locationAdmincp1" 
                                    value="<?=$local2?>"
                                    />
                                <input id="countryAdmincp1" name="countryAdmincp1" class="form-control input-lg" type="hidden"/>
                                <input id="citiesAdmincp1" name="citiesAdmincp1" class="form-control input-lg" type="hidden"/>
                            </div>
                            <div class="newsletter-style">
                                <input data-required="1" type="text" class="form-control" 
                                    name="locationAdmincp2" 
                                    id="locationAdmincp2" 
                                    value="<?=$local3?>"
                                    />
                                <input id="countryAdmincp2" name="countryAdmincp2" class="form-control input-lg" type="hidden"/>
                                <input id="citiesAdmincp2" name="citiesAdmincp2" class="form-control input-lg" type="hidden"/>
                            </div>
                            <div class="newsletter-style">
                                <input data-required="1" type="text" class="form-control" 
                                    name="locationAdmincp3" 
                                    id="locationAdmincp3" 
                                    value="<?=$local4?>"
                                    />
                                <input id="countryAdmincp3" name="countryAdmincp3" class="form-control input-lg" type="hidden"/>
                                <input id="citiesAdmincp3" name="citiesAdmincp3" class="form-control input-lg" type="hidden"/>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <?php
                            if($local_newsletter == 1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="local-newsletter" class="mg-t10" value="1">Receive the above local newsletters<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="local-newsletter" class="mg-t10" value="1">Receive the above local newsletters<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>AUTO LOCATION</label><br/>
                        <?php
                            if($auto_location ==1){
                            ?>
                        <input checked ='checked' type="checkbox" name="auto-location" value="1">Show your location auto<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="auto-location" value="1">Show your location auto<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>FORUM THREADS</label><br/>
                        <?php
                            if($forum_threads ==1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="forum-threads" value="1">Show your active on form<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="forum-threads" value="1">Show your active on form<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>PRIVATE MESSAGES</label><br/>
                        <?php
                            if($private_messages == 1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="private-messages" value="1">Not show your messages<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="private-messages" value="1">Not show your messages<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>FRIEND REQUEST NOTIFY</label><br/>
                        <?php
                            if($friend_request_notify ==1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="friend-request-notify" value="1">Auto email when an member requests you as a friend.<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="friend-request-notify" value="1">Auto email when an member requests you as a friend.<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <div class="mg-t30">
                        <label>PRIVATE MESSAGES NOTIFY</label><br/>
                        <?php
                            if($private_messages_notify ==1){
                            ?>
                        <input checked = 'checked' type="checkbox" name="private-messages-notify" value="1">Auto email when an member messages you.<br/>
                        <?php 
                            }
                            else{
                            ?>
                        <input type="checkbox" name="private-messages-notify" value="1">Auto email when an member messages you.<br/>
                        <?php
                            }
                            ?>
                    </div>
                    <?php
                        }
                        ?>
                </form>
                <div class="btn-profile">
                    <input id="btn-subscription" data-url="<?=PATH_URL?>" type="submit" name="Submit" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" value="Submit" />
                </div>
            </div>
            <div id="setting4" class=" privacy-settings tab-pane fade">
                <div class="form-privacy-settings">
                    <div class="mg-t50">
                        <label>PROFILE</label><br/>
                        <select>
                            <option>All registered RA users (not included search)</option>
                            <option>Everyone (search engines included)</option>
                            <option>My Friends only</option>
                            <option>Private(you will be the only one with access)</option>
                        </select>
                    </div>
                    <div class="mg-t30">
                        <label>REAL NAME</label><br/>
                        <input type="checkbox" name="remember" value="remember">Show your real name in listings and on your profile.<br/>
                    </div>
                    <div class="mg-t30">
                        <label>PERSONAL DETAILS</label><br/>
                        <input type="checkbox" name="remember" value="remember"> Shows your date of birth and contacts (email address and messenger)<br/>
                    </div>
                    <div class="mg-t30">
                        <label>EXCLUDE FORM SEARCH</label><br/>
                        <input type="checkbox" name="remember" value="remember"> Removes your profile from the <a>Friend Finder</a><br/>
                    </div>
                    <div class="mg-t30">
                        <label>ALLOW EVENTS INVITES</label> <br/>
                        <input type="checkbox" name="remember" value="remember"> Allow my friends to invite me to upcoming events.<br/>
                    </div>
                </div>
                <div class="btn-profile">
                    <input type="submit" name="Submit" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" value="Submit" />
                </div>
            </div>
            <div id="setting5" class="remove-account tab-pane fade">
                <div class="descript-remove-acc mg-t50">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea data-required="1" cols="" rows="8" 
                                    name="contentAdmincp" 
                                    id="contentAdmincp">
                                </textarea>
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        <div class="form-actions">
                            <div class="btn-profile">
                                <button onclick="submitAddContent()" type="button" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20">Submit</button>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:10px">
                            <div class="col-md-12">
                                <div class="form-control" 
                                    id="post_url" style="font-size:0px;   color:#fff;   background-color:transparent;border:0;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <script src="<?=get_resource_url('assets/libs/slick/slick.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/nicescroll/nicescroll.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/style.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.mousewheel-3.0.6.pack.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/helper.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page.js')?>"></script>
    <script src="<?=get_resource_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.validate.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/bootstrap.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/admin/jquery.form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/ajax_submit_form.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/croppic.css')?>">
    <script src="<?=get_resource_url('assets/js/croppic.js')?>"></script>
    <script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
    <script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
</div>