<!DOCTYPE html>
<html>
<head>
	<?php
	$lang = 'en';
	$image_homepage = '';
	$title_homepage = '';
	$keyword_homepage = '';
	$description_homepage = '';
	if (!empty($articles_blog)) {
		$articles_blog = $articles_blog[0];
	}
	if(!empty($articles_blog)){
		$title = $articles_blog->title;
		$description = $articles_blog->description;
		$meta_title = $articles_blog->meta_title;
		$meta_description = $articles_blog->meta_description;
		$meta_keywords = $articles_blog->meta_keyword;
		$image = get_resource_url($articles_blog->image);
	}
	else if(!empty($meta_homepage)){
		$image_homepage = $meta_homepage['image'];
		$title_homepage = $meta_homepage['meta_title'];
		$keyword_homepage = $meta_homepage['meta_keyword'];
		$description_homepage = $meta_homepage['meta_description'];
	}

	$title = !empty($title) ? $title : $title_homepage;
	$meta_title = !empty($meta_title) ? $meta_title : $title_homepage;
	$keywords = !empty($meta_keywords) ? $meta_keywords : $keyword_homepage;
	$description = !empty($meta_description) ? $meta_description : $description_homepage;
	$image = !empty($image) ? $image : $image_homepage;

	// Check login
	modules::run('home/login_remember_check');
	$login = 'no';
	$is_music_player_displayed = 1; // TEST - DEBUG
	if (session_check()) {
		$is_music_player_displayed = 1;
		$login = 'yes';
	}

	// Get logo
	$logo_image_url = ''; // 2 HTML codes: PC, Mobile
	$logo_data = modules::run('home/logo_image');
	foreach ($logo_data as $key => $logo_image) {
		if (!empty($logo_image->image)) {
			$logo_image_url = PATH_URL.$logo_image->image;
			break;
		}
	}
	?>
	<title><?=$title?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=$description?>">
    <meta name="keywords" content="<?=$keywords?>">
	
	<meta property="og:title" content="<?=$meta_title?>" />
	<meta property="og:description" content="<?=$description?>" />
	<meta property="og:image" content="<?=$image?>" />
	
    <link rel="icon" href="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>" type="image/x-icon" />

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick-theme.css?ver=1.0')?>" rel="stylesheet">

    <link href="<?=get_resource_url('assets/css/fstyle.css')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style2.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style3.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/thai.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/alpha.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/music.css')?>" type="text/css" rel="stylesheet">
	<link href="<?=get_resource_url('assets/css/style4.css')?>" type="text/css" rel="stylesheet">
	<link href="<?=get_resource_url('assets/css/fullcalendar.css')?>" type="text/css" rel="stylesheet">
	
    <?php
	$link_css = modules::run('home/get_link_css');
	foreach ($link_css as $key => $css) {
		if (!empty($css)) {
	?>
       <link href="<?=get_resource_url($css->file_css)?>" rel="stylesheet">
    <?php 
		}
	}
	?>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b443d34e1ceeb001b8429f3&product=inline-follow-buttons' async='async'></script>
	<!-- Hotjar Tracking Code for www.reclaimedm.com -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1377937,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places&language=en"></script>

	<script type="text/javascript">
		var path_url = '<?=PATH_URL?>';
		
		/*BEGIN: DEBUG*/
		function pr(message){
			if(console && console.log){
				console.log(message);
			}
		}
		/*END: DEBUG*/

		// USER TYPE TEMPLATE
		var USER_TYPE_VENUE = <?=USER_TYPE_VENUE?>;
		var USER_TYPE_ARTIST = <?=USER_TYPE_ARTIST?>;
		var USER_TYPE_LABEL = <?=USER_TYPE_LABEL?>;
		var USER_TYPE_PROMOTER = <?=USER_TYPE_PROMOTER?>;
		var USER_TYPE_MARKETER = <?=USER_TYPE_MARKETER?>;
		var USER_TYPE_READER = <?=USER_TYPE_READER?>;
		
		function initialize() {
		var options = {types: ['(regions)']};
		var input = document.getElementById('locationAdmincp');
		var input1 = document.getElementById('locationAdmincp1');
		var input2 = document.getElementById('locationAdmincp2');
		var input3 = document.getElementById('locationAdmincp3');
		var autocomplete = new google.maps.places.Autocomplete(input , options);
		var autocomplete = new google.maps.places.Autocomplete(input1 , options);
		var autocomplete = new google.maps.places.Autocomplete(input2 , options);
		var autocomplete = new google.maps.places.Autocomplete(input3 , options);

		google.maps.event.addListener(autocomplete, 'place_changed',
			function() {
			var address_components=autocomplete.getPlace().address_components;
			var city='';
			var country='';
			for(var j =0 ;j<address_components.length;j++){
				city =address_components[0].long_name;
				if(address_components[j].types[0]=='country'){
					country=address_components[j].long_name;
				}
			}
			document.getElementById('countryAdmincp').value = country;
			document.getElementById('citiesAdmincp').value = city;
			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
    <style>
		body{
			background: none;
		}
		<?php
		$background_list = modules::run('home/get_background');
		foreach ($background_list as $key => $result_background) {
			if (!empty($result_background->thumbnail_background)) {
		?>
			body{
			  /*   background: url(<?=get_resource_url($result_background->thumbnail_background)?>);
				background-attachment: fixed;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				background-color: #1d1d1b;
				position: relative; */
			}
		<?php 
			} 
			else { // else default 
		?>
			body{
				/* background:rgb(25, 25, 25) !important; */
			}
		<?php 
			}
		}
		?>
		li.mobile-user-logined.mobile-user-logined-menu{
			display: block !important;
    		position: relative !important;
		}
    </style>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111678015-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-111678015-1');
	</script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129431416-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-129431416-1');
	</script>
</head>

<body style="position:relative">

	<div class="div-loader">
        <div class="loader-inner">
            <div class="inner one"></div>
            <div class="inner two"></div>
            <div class="inner three"></div>
        </div>
    </div>
    <div class="parallax-container" data-parallax="scroll" data-bleed="10" data-speed="0.2"  data-image-src="
		<?php
		if (isset($result_background->thumbnail_background)) {
			echo get_resource_url($result_background->background);
		}
		?>
			" data-natural-width="1400" data-natural-height="1400" data-parallaxBgSize="cover" style="position: absolute; top: -70px; right: 0; bottom: 0; left: 0;background-size:cover">
	</div>
    <!--Tab setting-->
    <!-- Modal -->
    <div class="popup-modal-signup">
        <div class="modal fade" id="forgot-mobal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Forgot your password?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="forgot-content">
                        <form id ="for-got-pass" class = "for-got-pass">
                            <label for="email" class="mg-b20 mg-t20">ENTER YOUR EMAIL TO RESET PASSWORD</label><br/>
                            <input id="email" type="email" name="email" data-required="true" data-error="Please enter your email" />
                        </form>
                            <div class="btn-pre-next">
                                <input id ="for-got-password" type="submit" name="for-got-password" data-url="<?=PATH_URL?>" class="login-btn mg-b30 mg-t30" value="Send" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- profile setting-->
    <div class="profile-setting-detail profile--tab">
        <div class="profile-wrap">  
		<?php
		if (isset($userDataSetting1)) {
			foreach ($userDataSetting1 as $key => $val) {
				$type = '';
				switch ($val->user_type_id){
					case USER_TYPE_VENUE:
						$type = 'VENUE'; break;
					case USER_TYPE_ARTIST:
						$type = 'ARTIST'; break;
					case USER_TYPE_LABEL:
						$type = 'LABEL'; break;
					case USER_TYPE_PROMOTER:
						$type = 'PROMOTER'; break;
					case USER_TYPE_MARKETER:
						$type = 'MARKETER'; break;
					default: 
						$type = 'READER'; break;
				}
        ?>
            <div class="tab-content">
				<div class="rder"><?php echo session_name_get()?>, you are signed in as a <?php echo $type?></div>
                <div class="tab-profile pd-t60">
                    <ul class="nav nav-tabs nav-tabs-profile-setting">
                        <li>
							<a data-toggle="tab" href="#setting5">Propage Management</a>
						</li>
                        <li class="active">
							<a data-toggle="tab" href="#setting1">Account settings</a>
						</li>
                        <li>
							<a data-toggle="tab" href="#setting2">Change password</a>
						</li>
                        <li>
							<a data-toggle="tab" href="#setting3">subscription settings</a>
						</li>
                        <li class="close-profile">
							<a href="#">close</a>
						</li>
                    </ul>
                </div>
                <div id="setting1" class="account-settings tab-pane fade in active" >
                    <form class="form-account-settings" method="post" id="account-setting-form">
                        <input type="hidden" name="id" value="<?=$this->session->userdata['userData']['id']?>">
                        <div class="hidden">
							<div class="settings-country-avatar mg-t50"  >
								<!--  
								<div class="settings-left">
									<input class="mg-t20" placeholder="Enter new url to change" name="url" value="<?=$val->url?>" type="text">
								</div> 
								-->
								<div class="">
									<label class="">Country</label>
									<input class="mg-t20" placeholder="Enter new country to change" name="location" value="" type="text" id="country-user">
									<input type="hidden" name="country" value="" type="text" id="country-hide">
									<input type="hidden" name="city" value="" type="text" id="city-hide">
								</div>
							</div>
                        </div>
                        <div class="line-account-settings mg-t30"></div>
                        <div class="clear-fix"></div>
                        <!-- BGIN: ACCOUNT SETTINGS -->
                        <div class="settings-profile-photo" >
                            <div class="settings-left" style="max-width:100%">
                                <label class="mg-t50 mg-b20">PROFILE PHOTO &nbsp; &nbsp; &nbsp; <input type="button" name="input_avatar" value="Select File" class="update-img-upload width-select" id="cropContainerHeaderButton"/>
								</label>
								<br/>
                                <?php /*get avatar url*/
									$image_url = (!empty($val->image)) ? get_resource_url($val->image) : null;
									$thumbnail_url = (!empty($val->thumbnail)) ? get_resource_url($val->thumbnail) : null;
								?>     
                                <div class="text-center">
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="input-group input-large">
											<div class="col-xs-12s">
											   <div>
													<input type="hidden" id="input_thumbnail_url" name="thumbnail_urlAdmincp">
													<input type="hidden" id="input_image_url" name="image_urlAdmincp" value="<?php echo $thumbnail_url ?>">
												</div>
												<div class="img-profile hidden" style="position: relative;">
													<div id="cropic_element" style="display:none"></div>
													<a class="fancybox-button" id="preview_image_fancybox" href="<?=$image_url?>">
														<img id="preview_image" height=150 width=150 src="<?=$thumbnail_url?>">
													</a>
													<div class="overclick" style="position: absolute;width: 100%;height: 100%;top: 0;"></div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <label>Recommended size: 312 x 210.<BR/>.jpg / .jpeg / .gif / .bmp images only.</label>
								
                            </div>
                            <div class="settings-right">
                               <!--  <label class="mg-t50 mg-b20">OR ENTER AN EXTERAL IMAGE URL</label><br/>
                                <input placeholder="Enter new url images to upload"  type="text" readonly="readonly" name="thumbnail" value="<?=$image_url?>"><br/>
                                <label class="mg-t50 mg-b20">OR ENTER AN EXTERAL IMAGE URL</label><br/>
                                <input placeholder="Enter new url images to upload"  type="text" readonly="readonly" name="thumbnail" value="<?=$image_url?>"><br/> -->
                            </div>
                            <div class="clear-fix"></div>
                        </div>
                        <!-- END: ACCOUNT SETTINGS -->
                        <div class="line-account-settings mg-t30"></div>
                        <div class="settings-comment" >
                            <!-- SETTING LEFT -->
                            <div class="settings-left w-100">
								<div class="row no-gutters">
									<div class="col-md-2 col-sm-2 col-xs-2" style="width: 11%">
										<label class="mg-b5 line-h">Age</label>
									</div>
									<div class="col-md-10 col-sm-10 col-xs-10" style="width: 89%">
										<select name="age" class="select-age">
											<?php 
											$age = !empty($val->age) ? $val->age : 0;
											for ($i = 1; $i <= 100; $i++) {
											?>
											<option value="<?php echo $i ?>" <?php echo ($i == $age) ? 'selected' : '' ?>><?php echo $i ?></option>
											<?php }?>
										</select>
									</div>
									 
								</div>
                            </div>
                            <!-- SETTING LEFT -->
                            <!-- SETTING RIGHT -->
                            <div class="settings-right w-100">
								<div class="row no-gutters">
									<div class="col-md-2 col-sm-2 col-xs-2" style="width: 11%">
										<label class="mg-b5 line-h">Gender</label>
									</div>
									<div class="col-md-10 col-sm-10 col-xs-10" style="width: 89%">
										<select name="gender" class="select-age">
											<option value="1" <?php echo ($val->gender == '1') ? 'selected' : '' ?> >Female</option>
											<option value="2" <?php echo ($val->gender == '2') ? 'selected' : '' ?>>Male</option>
											<option value="3" <?php echo ($val->gender == '3') ? 'selected' : '' ?>>Unknown</option>
										</select>
									</div>
								</div>	
                            </div>
                        </div>
                        <div class="settings-comment">
                            <label class="mg-b5">#ReclaimEDM URL:</label>
                            <!--<label class="mg-b5"><?php echo get_resource_url('en/user/') ?></label>-->
                            <input value="<?=$val->link_url_redm?>" name="link_url_redm" rows="2" type="text" id="tLinkRedm">
                        </div>
                        <div class="settings-comment" >
                            <label class="mg-b10">Biography</label>
                            <textarea value="<?=$val->biiography?>" name="biography">
							</textarea>
                        </div>
                        <div class="settings-name">
                            <div class="settings-left">
                                <label class="mg-b10">Status</label>
                                <textarea name="status_genenal" placeholder="Enter new status to change" type="text" value="<?=$val->status_genenal?>"  rows="2">
								</textarea>
                            </div>
                            <div class="settings-right">
                                <label class="mg-b10">Short bio</label>
								<textarea name="shortbio" placeholder="Enter new shortbio to change" type="text" value="<?=$val->shortbio?>"  rows="2">
								</textarea>
                            </div>
                        </div>
                        <div class="settings-name">
                            <div class="settings-left">
                                <label class="mg-b10">Founder</label>
								<input class="" placeholder="Enter new founder to change" name="founder" value="<?=$val->founder?>" type="text">
                            </div>
                            <div class="settings-right">
                                <label class="mg-b10">Marketing</label>
                                <input class="" placeholder="Enter new marketing to change" name="marketing" value="<?=$val->marketing?>" type="text">
                            </div>
                        </div>
						<div class="settings-name">
                            <div class="settings-left">
                                <label class="mg-b10">Distributor</label>
								<input class="" placeholder="Enter new distributor to change" name="distributor" value="<?=$val->distributor?>" type="text">
                            </div>
                            <div class="settings-right"> 
                                <label class="mg-b10">Contact</label>
                                <input class="" placeholder="Enter new contact to change" name="contact" value="<?=$val->contact_info?>" type="text">
                            </div>
                        </div>
                        <div class="settings-link" style="display: none;" >
                            <div class="settings-left">
                                <label class="mg-t40 mg-b10">URL Facebook</label>
								<br/>
                                <input placeholder="Enter new url facebook to change" type="text" value="<?=$val->facebook?>" name="facebook" />
                                <label class="mg-t40 mg-b10">URL Twitter</label>
								<br/>
                                <input placeholder="Enter new url twitter to change" type="text" type="text" value="<?=$val->twitter?>" name="twitter" />
                                <label class="mg-t40 mg-b10">URL Bandcamp</label>
								<br/>
                                <input placeholder="Enter new url bandcamp to change" type="text" type="text" value="<?=$val->bandcamp?>" name="bandcamp" />
                                <label class="mg-t40 mg-b10">URL Mixcloud</label>
								<br/>
                                <input placeholder="Enter new url mixcloud to change" type="text" type="text" value="<?=$val->mixcloud?>" name="mixcloud" />
                            </div>
                            <div class="settings-right">
                                <label class="mg-t40 mg-b10">URL Beatport</label><br/>
                                <input placeholder="Enter new url beatport to change" type="text" type="text" value="<?=$val->beatport?>" name="beatport" />
                                <label class="mg-t40 mg-b10">URL Soundcloud</label><br/>
                                <input placeholder="Enter new url soundcloud to change" type="text" type="text" value="<?=$val->soundcloud?>" name="soundcloud" />
                                <label class="mg-t40 mg-b10">URL iTunes</label><br/>
                                <input placeholder="Enter new url itunes to change" type="text" type="text" value="<?=$val->itunes?>" name="itunes" />
                                <div class="btn-profile">
                                    <input type="submit" id="btn-update-account" name="Submit" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" data-url="<?=PATH_URL?>" value="Submit" />
                                </div>
                            </div>
                        </div>
                        <!-- SETTING RIGHT -->
		<?php 
			}
		}
		?>
                    </form>
                </div>
                <!-- BEGIN: CHANGE PASSWORD -->
                <div id="setting2" class="change-pwd tab-pane fade">
                    <div class="form-change-password">
                        <label class="mg-t40 mg-b20">CURRENT PASSWORD</label>
                        <input type="PASSWORD" name="currentPass" id = "currentPass" value=""  data-required="true" data-error="Please enter your country"/>
                        <label class="mg-t40 mg-b20">NEW PASSWORD</label>
                        <input type="PASSWORD" name="newPass" id = "newPass" value=""  data-required="true" data-error="Please enter your country"/>
                        <label class="mg-t40 mg-b20">CONFIRM PASSWORD</label>
                        <input type="PASSWORD" name="confirmNewPass" id = "confirmNewPass" value=""  data-required="true" data-error="Please enter your country"/>
                    </div>
                    <div class="btn-profile">
                        <button onclick="save()" name="save" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" >Save</button>
                    </div>
                </div>
                <!--END: CHANGE PASSWORD -->
                <div id="setting3" class="subscription-settings tab-pane fade">
                    <form id="subscription-setting-form" class="form-subscription-settings" method="post">
					<?php
					if (isset($userDataSetting1)) {
						foreach ($userDataSetting1 as $key => $value) {
							$id                      = $value->id;
							$global_newsletter       = $value->global_newsletter;
							$local_newsletter        = $value->local_newsletter;
							$auto_location           = $value->auto_location;
							$forum_threads           = $value->forum_threads;
							$private_messages        = $value->private_messages;
							$friend_request_notify   = $value->friend_request_notify;
							$private_messages_notify = $value->private_messages_notify;
							if (!empty($userDatalocation)) {
								$local1 = $userDatalocation[0]->local;
								$local2 = $userDatalocation[1]->local;
								$local3 = $userDatalocation[2]->local;
								$local4 = $userDatalocation[3]->local;
							} else {
								$local1 = '';
								$local2 = '';
								$local3 = '';
								$local4 = '';
							}
					?>
						<input name="id" value="<?=$id?>" type="hidden">
                        <!--BEGIN SUBSCRIPTION SETTINGS -->
                        <div class="credit-card mg-t30" style="margin-top:100px;">
                            <div class="card-details mg-b30">
                                <div class="mg-t30">
                                    <label>CARD HOLDER'S NAME</label>
									<br/>
                                    <input type="text" name="carddetails" placeholder="Card Holder's Name" >
									<br/>
                                </div>
                                <div class="mg-t30">
                                    <label>CARD EXPIRY DATE</label>
									<br/>
                                    <select>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                    <span>/</span>
                                    <select>
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                                <div class="mg-t30">
                                    <label>CARD NUMBER</label><br/>
                                    <input type="text" name="carddetails" placeholder="Card Number" >
									<br/>
                                </div>
                                <div class="mg-t30">
                                    <label>CVV/CVV2</label><br/>
                                    <input type="text" name="carddetails" placeholder="Card Holder's Name" >
									<br/>
                                </div>
                            </div>
                        </div>
                        <div class="mg-t30" style="max-width:700px;margin-left:auto;margin-right:auto">
                            <label>GLOBAL NEWSLETTER</label><br/>
                            <?php
							if ($global_newsletter == 1) {
							?>
                                <input checked = 'checked' type="checkbox" name="global-newsletter" value="1">
								Receive the weekly global newsletter.
								<br/>
                            <?php 
							} 
							else {
							?>
                                <input type="checkbox" name="global-newsletter" value="1">
								Receive the weekly global newsletter.
								<br/>
                            <?php 
							}
							?>
                        </div>
                        <div class="mg-t30" style="max-width:700px;margin-left:auto;margin-right:auto">
                            <label>LOCAL NEWSLETTER</label>
							<br/>
                            <label>
							The weekly local mailer covering upcoming events and other regional based content.You can subscribe to up to four regions below. To unsubscribe deselect the "Receive the above local newsletters" check box below.
							</label>
                            <div class="text-center">
                                <div class="newsletter-style">
                                    <input data-required="1" type="text" class="form-control"
                                        name="locationAdmincp" id="locationAdmincp"
                                        value="<?=$local1?>"
                                        />
                                    <input id="countryAdmincp" name="countryAdmincp" class="form-control input-lg" type="hidden"/>
                                    <input id="citiesAdmincp" name="citiesAdmincp" class="form-control input-lg" type="hidden"/>
                                </div>
                                <div class="newsletter-style">
                                    <input data-required="1" type="text" class="form-control"
                                        name="locationAdmincp1" id="locationAdmincp1"
                                        value="<?=$local2?>"
                                        />
                                    <input id="countryAdmincp1" name="countryAdmincp1" class="form-control input-lg" type="hidden"/>
                                    <input id="citiesAdmincp1" name="citiesAdmincp1" class="form-control input-lg" type="hidden"/>
                                </div>
                                <div class="newsletter-style">
                                    <input data-required="1" type="text" class="form-control"
                                        name="locationAdmincp2" id="locationAdmincp2"
                                        value="<?=$local3?>"
                                        />
                                    <input id="countryAdmincp2" name="countryAdmincp2" class="form-control input-lg" type="hidden"/>
                                    <input id="citiesAdmincp2" name="citiesAdmincp2" class="form-control input-lg" type="hidden"/>
                                </div>
                                <div class="newsletter-style">
                                    <input data-required="1" type="text" class="form-control"
                                        name="locationAdmincp3" id="locationAdmincp3"
                                        value="<?=$local4?>"
										/>
                                    <input id="countryAdmincp3" name="countryAdmincp3" class="form-control input-lg" type="hidden"/>
                                    <input id="citiesAdmincp3" name="citiesAdmincp3" class="form-control input-lg" type="hidden"/>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                            <?php
							if ($local_newsletter == 1) {
							?>
                                <input checked = 'checked' type="checkbox" name="local-newsletter" class="mg-t10" value="1">
								Receive the above local newsletters
								<br/>
                            <?php 
							} 
							else {
							?>
                                <input type="checkbox" name="local-newsletter" class="mg-t10" value="1">
								Receive the above local newsletters
								<br/>
                            <?php 
							}
							?>
                        </div>
                        <div class="mg-t30" style="max-width:700px;margin-left:auto;margin-right:auto">
                            <label>AUTO LOCATION</label>
							<br/>
                            <?php
							if ($auto_location == 1) {
							?>
								<input checked ='checked' type="checkbox" name="auto-location" value="1">
								Show your location auto
								<br/>
                            <?php 
							} 
							else {
							?>
                                <input type="checkbox" name="auto-location" value="1">
								Show your location auto
								<br/>
                            <?php 
							}
							?>
                        </div>
                        <div class="mg-t30" style="max-width:700px;margin-left:auto;margin-right:auto">
                            <label>FORUM THREADS</label>
							<br/>
                            <?php
							if ($forum_threads == 1) {
							?>
                                <input checked = 'checked' type="checkbox" name="forum-threads" value="1">
								Show your active on form
								<br/>
                            <?php 
							} 
							else {
							?>
                                <input type="checkbox" name="forum-threads" value="1">
								Show your active on form
								<br/>
                            <?php 
							}
							?>

                        </div>
                        <!--
						<div class="mg-t30">
                            <label>PRIVATE MESSAGES</label><br/>
                            <?php
							if ($private_messages == 1) {
							?>
								<input checked = 'checked' type="checkbox" name="private-messages" value="1">Not show your messages<br/>
                            <?php 
							}
							else {
							?>
                                <input type="checkbox" name="private-messages" value="1">Not show your messages<br/>
                            <?php 
							}
							?>
                        </div>
                        <div class="mg-t30">
                            <label>PRIVATE MESSAGES NOTIFY</label><br/>
                            <?php
							if ($private_messages_notify == 1) {
							?>
								<input checked = 'checked' type="checkbox" name="private-messages-notify" value="1">Auto email when an member messages you.<br/>
                            <?php 
							} 
							else {
							?>
                                <input type="checkbox" name="private-messages-notify" value="1">Auto email when an member messages you.<br/>
                            <?php 
							}
							?>
                        </div>-->
                    <?php
						}
					}
					?>
                        <div class="btn-profile text-center">
                            <input id="btn-subscription" data-url="<?=PATH_URL?>" type="submit" name="Submit" class="submit-btn-profile  mg-b50 mg-t60 mg-sm-t20" value="Submit" />
                        </div>
                    </form>
                </div>
                <!-- END: SUBSCRIPTION SETTINGS -->
				
				

                <!-- Temporary remove addcontent from profile setting -->
                <div id="setting5" class="remove-account tab-pane fade fix-fade">
                    <div class="descript-remove-acc mg-t50">
                    <p>I think we should have propage Management here</p>
                        <!-- <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php 
                                    	//include 'preview.php';
                                    	
                                    ?>
                                </div>
                            </div>
                            <div class='clearfix'></div>
                            <div class="form-actions">
								<div class="btn-profile">
									<button onclick="submitAddContent()" type="button" class="submit-btn-profile mg-b50 mg-t60 mg-sm-t20">Submit
									</button>
								</div>                       
                            </div>
                            <div class="form-group" style="margin-top:10px">
                                <div class="col-md-12">
                                    <div class="form-control"
                                     id="post_url" style="font-size:0px;   color:#fff;   background-color:transparent;border:0;">
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <!-- Modal -->
    <div class="popup-modal-signup">
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-default btn-close" data-url="<?=PATH_URL?>"  data-dismiss="modal">Close</button>
					</div>
                </div>
            </div>
        </div>
    </div>
	<!--End  tab setting-->
    <div class="menu">
        <div class="top-menu">
            <div class="icon-show-menu-mobile  pull-left">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="logo pull-left">
				<a href="<?=PATH_URL?>">
					<img src="<?=$logo_image_url?>">
				</a>
            </div>
            <div class="text-logo pull-left">
                <a href="/#" class="text-logo-item">
                    #BETA
                </a>
            </div>
            <div class="wrap-slide-text pull-left">
                <marquee class="pull-left slide-text" scrolldelay="0" direction="left">
				<?php
					$header_tickers = modules::run('home/get_header_ticker');
					foreach ($header_tickers as $result) {
						if ($result->link != "") {
							echo '<div class="run-text" >' . '<a style="color:#f39200 !important" href="' . $result->link . '" target="_blank" >' . $result->contents . '</a></div>';
						} 
						else {
							echo '<div class="run-text">' . $result->contents . '</div>';
						}
					}
				?>
					<!--
					<div class="run-text">This will be a running news ticker </div>
                    <div class="run-text">they will be clickable links </div>
                    <div class="run-text"> TEXT MOVES FROM RIGHT TO LEFT </div> -->
                </marquee>
            </div>
            <div class="wrap-login pull-right">
			<?php
				if (session_check()) {
					$session_user_id = session_get('id');
					$userDataSetting1 = modules::run('home/load_list_user_info', $session_user_id);
					foreach ($userDataSetting1 as $value) {
						$link_profile = $value->link_url_redm;
					}
			?>
                <!-- MENNU PROFILE -->
                <div class="menu-profile">
                    <div class="menu-style-social pull-right">
                        <div class="search-social pull-left icon-find">
                            <span>
								<img src="<?=get_resource_url('assets/images/icon/find.png')?>">
							</span>
                        </div>
                        <div class="search-social pull-left icon-music">
                            <span>
								<img src="<?=get_resource_url('assets/images/icon/music.png')?>">
							</span>
                        </div> 
                        <div class="search-social pull-left icon-fire">
                            <span>
								<img src="<?=get_resource_url('assets/images/icon/fire-fix.png')?>">
							</span>
                        </div>
						<div class="search-social pull-left icon-star">
                            <span>
								<img style="max-width: 20px;height: 20px;" src="<?=get_resource_url('assets/images/icon/sao.png')?>">
							</span>
                        </div>
                    </div>
                    <div class="menu-profile-right pull-right">
                        <div class="click-menu-profile">
                            <i class="fa-rotate fa fa-angle-double-down" aria-hidden="true"></i>
                            <?php
							if (!empty($authUrl)) {
								redirect($authUrl);
							} 
							else {
							?>
								<span><?php echo session_name_get()?></span>
                            <?php 
							}
							?>
                        </div>
                        <ul class="menu-profile-toggle">
                            <li class="profile-title profile-setting">Profile Setting</li>
                            <?php 
							if (!empty($link_profile)) {
							?>
                            <li class="profile-title">
								<a href="<?=PATH_URL?>en/user/<?php echo $link_profile ?>"><?=$link_profile?></a>
							</li>
                            <?php 
							} 
							?>
                            <li class="profile-title profile-logout">
								<a class="btn-logout" href="<?=PATH_URL . 'ajax/logout'?>">Logout</a>
							</li>
                        </ul>
                    </div>
                    <div class="login pull-right" style="display: block;">
						<ul class="side-menu-setting1" style="display: inline-block;">
							<?php
							$data['class_name'] = 'mobile-social-style1';
							echo modules::run('home/header_social_buttons', $data);
							?>
						</ul>
					</div>
                </div>
                <!--  MENU PROFILE -->
                <?php
				} else {
				?>      
                <!--  LOGIN -->
                <div class="login pull-right" style="display: inline-flex;">
                 
					<ul class="side-menu-setting1" style="display: inline-block;">
						<?php
						$data['class_name'] = 'mobile-social-style1';
						echo modules::run('home/header_social_buttons', $data);
						?>
					</ul>
                </div>
                <!--  LOGIN -->
            <?php
			}
			?>
                <div class="clear-fix"></div>
            </div>
        </div>
        <!-- SLIDE MENU -->
        <div class="wrap-side-menu"></div> 
        <div class="side-menu side-menu-setting1">
            <div class="side-menu-view">
                <div class="side-menu-top">
                    <ul class="list-side-menu side-menu-setting1">
						<?php
						$data['attributes'] = '';
						echo modules::run('home/header_social_buttons', $data);
						?>
                        <li class="menu-search">
                            <a class="side-menu-link side-menu-link-search">
								<form action="<?=PATH_URL.'en/search'?>" method="get" id="search-form" style="margin: 0">
									<input value="<?= isset( $_SESSION['keyword'] ) ? $_SESSION['keyword'] : "" ?>" type="text" class="mobile-search" name="keyword"/>
									<img class="img-mobile-search" src="<?=get_resource_url('assets/images/icon/find.png')?>">
								</form>
                            </a>
                        </li>
                        <?php 
						$menu_list = modules::run('home/get_list_menu');
							foreach ($menu_list as $key => $menu) {
								$menu_url = !empty($menu->url) ? PATH_URL . $menu->url : PATH_URL . $lang . '/blog';
						?>
                        <li class="menu-item menu-item-<?php echo $key; ?>">
							<a class="side-menu-item side-menu-link" href="<?=$menu_url?>">
								<span><?=$menu->name?></span>
							</a>
						</li>
                        <?php
							}
						?>                    
                    </ul>
                </div>
            </div>
        </div>
        <!-- SLIDE MENU -->
        <div class="mobile-menu">
            <div class="mobile-top-menu">
                <div class="icon-show-menu-mobile  pull-left">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <div class="logo pull-left">
					<a href="<?=PATH_URL?>">
						<img src="<?=$logo_image_url?>">
					</a>       
                </div>
                <div class="text-logo pull-left">
                    <a href="#" class="text-logo-item">
                        #BETA
                    </a>
                </div>
                <div class="wrap-slide-text pull-left ">
                    <marquee class="pull-left slide-text" scrolldelay="0" scrollamount="3">
                        <!-- <div class="run-text">This will be a running news ticker </div>
                        <div class="run-text">they will be clickable links </div>
                        <div class="run-text"> TEXT MOVES FROM RIGHT TO LEFT </div> -->
                          <?php
                    $header_tickers = modules::run('home/get_header_ticker');
                    foreach ($header_tickers as $result) {
                        if ($result->link != "") {
                            echo '<div class="run-text">' . '<a style="color:#f39200 !important" href="' . $result->link . '" target="_blank">' . $result->contents . '</a></div>';
                        }
						else {
                            echo '<div class="run-text">' . $result->contents . '</div>';
                        }
                    }
                    ?>
                    </marquee>
                </div>
                <!-- <div class="mobile-menu-right-icon pull-right">
                    MENU
                </div> -->
                <div class="clear-fix"></div>
            </div>
            <div class="mobile-side-menu">
                <div class="mobile-side-menu-view">
                    <div class="side-menu-top">
                        <ul class="list-side-menu">
						<?php
						if (session_check()) {
						?>
                           <li class="mobile-user-logined mobile-user-logined-menu">
                                <div class="menu-profile-right">
                                    <div class="click-menu-profile"><?php echo session_name_get()?>
										<i class="fa fa-angle-double-down" aria-hidden="true"></i>
									</div>
                                    <ul class="menu-profile-toggle">
                                        <li class="profile-title profile-setting">Profile Setting</li>
                                        <?php
                                        	if(!empty($link_profile)){
                                        ?>
                                        <li class="profile-title">
											<a href="<?=PATH_URL?>en/profile/<?php echo $link_profile ?>"><?=$link_profile?></a>
										</li>
										<?php
											}
										?>
                                        <li class="profile-title profile-logout">
											<a class="btn-logout" href="<?=PATH_URL . 'ajax/logout'?>">Logout</a>
										</li>
                                    </ul>
                                </div>
                            </li>
						<?php
						} 
						else {
						?>
						<?php
						}
							echo modules::run('home/header_social_buttons');
						?>
							
							

							<?php 
							$menu_list = modules::run('home/get_list_menu');
								foreach ($menu_list as $key => $menu) {
									$menu_url = !empty($menu->url) ? PATH_URL . $menu->url : PATH_URL . $lang . '/blog';
							?>
							<li class="menu-item menu-item-<?php echo $key; ?>">
								<a class="side-menu-item side-menu-link" href="<?=$menu_url?>">
									<span><?=$menu->name?></span>
								</a>
							</li>
							<?php
								}
							?>
                            <?php 
							if (isset($this->session->userdata['userData'])) { 
							?>
                            <li class="menu-item menu-item-">
								<a class="side-menu-item side-menu-link side-menu-link-mb-click-music-player" href="#" style="background: black">
									<span>Music player</span>
								</a>
							</li>
                            <?php 
							} 
							?>                     
                        </ul>
                    </div>
                </div>
            </div>
            <!-- RIGHT MENU -->
            <div class="mobile-right-menu">
                <div class="display-table">
                    <div class="display-table-cell">
                        <div class="mobile-wrap-right-menu">
                            <div class="mobile-signup">
                                SIGN UP
                            </div>
                            <div class="mobile-login">
                                LOGIN
                            </div>
							<?php
							// echo modules::run('home/header_social_buttons');
							?>
                            <div class="mobile-social mg-b10">
                                <a href="#" class="social-item">
                                    FB
                                </a>
                                <a href="#" class="social-item">
                                    TW
                                </a>
                                <a href="#" class="social-item">
                                    SC
                                </a>
                                <a href="#" class="social-item">
                                    MC
                                </a>
                            </div>
                            <input type="text" class="mobile-search" placeholder="Search" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- RIGHT MENU -->
        </div>
        <!-- Login/Signup -->
        <?=modules::run('home/form_register')?>
    </div>
    <main>
        <?=$content?>
    </main>
	<?php
	if(!empty($is_music_player_displayed)){
		echo modules::run('music_page/index');
	}
	?>
	
	<?php
	echo modules::run('home/event_calendar');
	?>
	
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.core.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.ajax.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.arrow.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.autocomplete.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.clear.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.filter.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.focus.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.prompt.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.suggestions.js'?>"></script>
	<script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/js/textext.plugin.tags.js'?>"></script>

	<script src="<?=PATH_URL.'assets/js/tags/'?>jquery-ui.min.js" type="text/javascript"></script>

	<script src="<?=PATH_URL.'assets/js/'?>tag-it.js" type="text/javascript" charset="utf-8"></script>

	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<link href="<?=PATH_URL.'assets/css/'?>jquery.tagit.css" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.core.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.arrow.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.autocomplete.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.clear.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.focus.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.prompt.css'?>" rel="stylesheet" type="text/css">
	<link href="<?=PATH_URL.'assets/js/admin/jquery-textext-master/src/css/textext.plugin.tags.css'?>" rel="stylesheet" type="text/css">
	
	<link href="<?=get_resource_url('assets/css/jquery-ui.css')?>" rel="stylesheet">
	<script type="text/javascript" src="<?=get_resource_url('assets/js/jquery-ui.js')?>"></script>
	 
    <!-- call js page -->
    <script src="<?=get_resource_url('assets/libs/parallax/parallax.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/slick/slick.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/nicescroll/nicescroll.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/style.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fancybox.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.mousewheel-3.0.6.pack.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/helper.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_comment.js')?>"></script>
    <script src="<?=get_resource_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.validate.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.numeric.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/bootstrap.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fitvids.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/admin/jquery.form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/ajax_submit_form.js')?>"></script>
	
	<!-- Not used???
    <script src="<?=get_resource_url('assets/js/js_page_comment.js')?>"></script>
	--> 
    <link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/croppic.css')?>">
    <script src="<?=get_resource_url('assets/js/croppic.js')?>"></script>
    <script type="text/javascript" src="<?=PATH_URL . 'assets/js/admin/'?>jquery.slugit.js"></script>
    <script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a2771999c6fb926"></script>
    <script src='<?=get_resource_url('assets/js/react.min.js')?>'></script>
    <script src='<?=get_resource_url('assets/js/react-dom.min.js')?>'></script>
    <script src="<?=get_resource_url('assets/js/preview.js')?>"></script>
    <script type="text/javascript" src="<?=get_resource_url('assets/js/music.js')?>"></script>
    <script type="text/javascript">
		var login = '<?=$login?>';
        //$(document).ready( function(){
        tinymce.init({
			selector: '#contentAdmincp_rm',
			plugins: "code, link, image, lists, preview, textcolor, pix_embed_online_media, responsivefilemanager",
			//toolbar: ['undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect forecolor backcolor | bullist numlist outdent indent | link image | print preview media fullpage removeformat','pix_embed_online_media | responsivefilemanager'],
			//external_filemanager_path:"<?=PATH_URL?>/filemanager/",
			//filemanager_title:"Responsive Filemanager" ,
			//relative_urls: false,
			setup: function (editor) {
					editor.on('keydown', function(e) {
					}).on('keyup', function(e) {
					var key = e.keyCode || e.which;
						if (key == 13) {
							var content = tinyMCE.get('contentAdmincp_rm').getContent();
							var url = 'ajax/url_Content';
							var root = $(".submit-btn-profile").attr("data-url") + url;
							$.ajax
							(
								{
									type: 'POST',
									url: root,
									data:
									{
										'content': content
									},
									success: function(data)
									{
										let myObj = JSON.parse(data);
										if(myObj.success == true){
											console.log(myObj.post_url);
											$('#post_url').html(myObj.post_url);
										}
									}
								}
							)
						}
					});
				}
			//external_plugins: { "filemanager" : "assets/filemanager/plugin.min.js"}
			// toolbar: "anchor",
			// menubar: "insert"
        });
        //End FOR WYSIWYG content
        //});
    </script>
	
	<style type="text/css">
		textarea{
			color:black;
			position:relative;
			z-index:999;

		}
		#cropic_element {
			height: 200px;
			width: 200px;
		}
		 #cropic_element_new {
			height: 200px;
			width: 200px;
		}
		.width-select{
			width: 100px !important;
			height: 25px !important;
			font-weight: 400;

		}
		@media (max-width: 991px){
			input.input_date{
				width: 110px;
			}
		}
		@media (min-width: 992px){
			input.input_date{
				width: 100%;
			}
		}
		
		.fix-fade{
			display: none;
		}
		
		
		/* Social Buttons*/
		.mobile-social-style1 a {
			padding-left: 5px;
			padding-right: 5px;
		}
	</style>
	
    <script>
		var croppicHeaderOptions = {
				uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
				cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
				uploadData:{'prefix':'avatar_'},
				enableMousescroll:true,
				customUploadButtonId:'cropContainerHeaderButton',
				outputUrlId:'input_thumbnail_url',
				modal:true,
				rotateControls: false,
				doubleZoomControls:false,
				imgEyecandyOpacity:0.4,
				onBeforeImgUpload: function(){ },
				onAfterImgUpload: function(){ appendOriginImageAward(); },
				onImgDrag: function(){ },
				onImgZoom: function(){ },
				onBeforeImgCrop: function(){ },
				onAfterImgCrop:function(){appendAward(); },
				onReset:function(){ onResetCropic(); },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var croppic = new Croppic('cropic_element', croppicHeaderOptions);
		function appendOriginImageAward() {
			var url_origin = $("div#croppicModalObj > img").attr('src');
			$('#input_image_url').val(url_origin);
			$("#preview_image_fancybox").attr("href", $('#input_image_url').val());
		}
		function appendAward(){
			$("#preview_image").attr("src", $('#input_thumbnail_url').val());
			$("#preview_image").show();
		}
		function onResetCropic(){
			$('#input_image_url').val('');
			$('#input_thumbnail_url').val('');
			$("#preview_image_fancybox").attr("href", '');
			$("#preview_image").attr("src", '');
		}
		// $(document).ready(function(){
			// $('.side-menu').click(function(){
				// $(this).addClass('active');
			// });
			// $(document.body).on('click', function(event){
				// event.stopPropagation();
				// $('.side-menu').removeClass('active');
			// });
		// });

		// $('a[href="#login-form"]').click(function(){
			 // $('#login-form').css('display','block');
			// $('#signup-form').css('display','none')
		  // });
		// $('a[href="#signup-form"]').click(function(){
			// $('#login-form').css('display','none');
			 // $('#signup-form').css('display','block')
		// });
	</script>
	<script>
		var croppicHeaderOptions_new = {
				uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
				cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
				uploadData:{'prefix':'avatar_'},
				enableMousescroll:true,
				customUploadButtonId:'cropContainerHeaderButton_new',
				outputUrlId:'input_thumbnail_url_new',
				modal:true,
				rotateControls: false,
				doubleZoomControls:false,
				imgEyecandyOpacity:0.4,
				onBeforeImgUpload: function(){ },
				onAfterImgUpload: function(){ appendOriginImageAward_new(); },
				onImgDrag: function(){ },
				onImgZoom: function(){ },
				onBeforeImgCrop: function(){ },
				onAfterImgCrop:function(){appendAward_new(); },
				onReset:function(){ onResetCropic_new(); },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var croppic = new Croppic('cropic_element_new', croppicHeaderOptions_new);
		function appendOriginImageAward_new() {
			var url_origin = $("div#croppicModalObj > img").attr('src');
			$('#input_image_url_new').val(url_origin);
			$("#preview_image_fancybox_new").attr("href", $('#input_image_url_new').val());
		}
		function appendAward_new(){
			$("#preview_image_new").attr("src", $('#input_thumbnail_url_new').val());
			$("#preview_image_new").show();
		}
		function onResetCropic_new(){
			$('#input_image_url_new').val('');
			$('#input_thumbnail_url_new').val('');
			$("#preview_image_fancybox_new").attr("href", '');
			$("#preview_image_new").attr("src", '');
		}
	</script>
	<?php
	if (!is_localhost()) {
		//admincp_script
		$get_script = modules::run('home/get_script');
		foreach ($get_script as $result) {
			echo $result->script;
		}
	}
	?>
	
	<script src="https://apis.google.com/js/platform.js"></script>
</body>
</html>