<div class="profile-view-detail profile--tab">
    <div class="profile-wrap">
        <div class="tab-content">
            <div class="tab-profile pd-t60">
                <ul class="nav nav-tabs">
                    <li class="active" id="overview-link"><a data-toggle="tab" href="#view1">Overview</a></li>
                    <li><a data-toggle="tab" href="#view2">Favourites</a></li>
                    <li class="close-profile"><a href="#">close</a></li>
                </ul>
            </div>
            <div id="view1" class="profile-view-overview tab-pane fade in active mg-b50">
                <div class="profile-view-table">
                    <table class="mg-t50">
                        <tr>
                            <th>
                                <span>RA since</span>
                                <p>April 2017</p>
                            </th>
                            <th>
                                <span>Total comments</span>
                                <p>0</p>
                            </th>
                            <th>
                                <span>Last online</span>
                                <p>17 April 2017</p>
                            </th>
                            <th>
                                <span>Location</span>
                                <p>VietNam</p>
                            </th>
                        </tr>
                    </table>
                </div>
                <div class="profile-view-favourite mg-t50">
                    <div class="title">FAVOURITE ARTISTS</div>
                    <div class="title-view-favourite view-artists">Manage top 8</div>
                    <div class="content-view-favourite toggle-artists">
                        <!-- <div class="text-updated">Your top favourites were updated.</div> -->
                        <label>1/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>2/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>3/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>4/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <br/>
                        <label>5/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>6/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>7/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>8/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <input class="update-profile-favourite" type="button" name="" value="Update"/>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-view-favourite mg-t50">
                    <div class="title">FAVOURITE LABELS</div>
                    <div class="title-view-favourite view-labels">Manage top 8</div>
                    <div class="content-view-favourite toggle-labels">
                        <!-- <div class="text-updated">Your top favourites were updated.</div> -->
                        <label>1/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>2/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>3/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>4/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <br/>
                        <label>5/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>6/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>7/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>8/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <input class="update-profile-favourite" type="button" name="" value="Update"/>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="view2" class="tab-pane fade mg-b50">
                <div class="profile-view-favourite mg-t50">
                    <div class="title">FAVOURITE ARTISTS</div>
                    <div class="title-view-favourite view-artists-1">Manage top 8</div>
                    <div class="content-view-favourite toggle-artists-1">
                        <!-- <div class="text-updated">Your top favourites were updated.</div> -->
                        <label>1/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>2/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>3/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>4/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <br/>
                        <label>5/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>6/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>7/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>8/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <input class="update-profile-favourite" type="button" name="" value="Update"/>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-view-favourite mg-t50">
                    <div class="title">FAVOURITE LABELS</div>
                    <div class="title-view-favourite view-labels-1">Manage top 8</div>
                    <div class="content-view-favourite toggle-labels-1">
                        <!-- <div class="text-updated">Your top favourites were updated.</div> -->
                        <label>1/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>2/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>3/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>4/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <br/>
                        <label>5/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>6/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>7/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <label>8/</label>
                        <select>
                            <option>--Your Favourite--</option>
                            <option>--Remove--</option>
                            <option>--Zombie Zombie--</option>
                        </select>
                        <input class="update-profile-favourite" type="button" name="" value="Update"/>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <div class="content-view-display">
                                <div class="img-content"><img src="<?=get_resource_url('assets/images/versatile-records.jpg')?>"></div>
                                <div class="title-content-main">Versatile Records</div>
                                <div class="action" ><span class="action-hover">Favourite</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>