
 <script type="text/javascript">
  var myDataSet = {
    title: '',
    text: ''
  };
</script>  

<style>
 
 /*html,
body {
  width: 100%;
  height: 100%;
  margin: 0px;
  padding: 0px;
}*/
 
#app button{
    position: relative;
    z-index: 999;
}


#app h1{
  display:none !important;
}

#app h1 {
  margin: 0px;
  padding: 10px;
  color: #333;
  font-size: 26px;
  background: #ddd;
  border-bottom: 1px solid #ccc;
  display: none;
}

#app form {
  margin: 10px;
}
#app form textarea {
  border: 1px solid #d0d0d0;
  border-radius: 3px;
  width: 100%;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 10px 0;
  min-height: 100px;
  padding: 10px;
  font-size: 16px;
}
#app form textarea:focus {
  border-color: #42afe3;
  outline: 0;
}
#app form button {
  display: none;
  border: none;
  background: #f39200;
  color: #fff;
  padding: 10px 22px;
  border-radius: 3px;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-weight: 600;
  border: 3px solid #f39200;
  font-size: 14px;
  outline: none;
}
#app form button:hover {
  background: #fff;
  color: #f39200;
  border: 3px solid #f39200;
}

#app .preview-box {
  position: relative;
  background: #fafafa;
  border: 1px solid #ddd;
  border-radius: 5px;
  font-size: 16px;
  padding: 40px 15px 20px;
  font-weight: 500;
  margin: 10px 0;
  color: #333;
}
#app .preview-box .preview-title {
  position: absolute;
  top: 0;
  left: 0;
  font-size: 10px;
  text-transform: uppercase;
  background: #eee;
  font-weight: 600;
  border: 1px solid #ddd;
  border-top: 0;
  border-left: 0;
  border-radius: 5px 0 5px 0;
  padding: 5px 12px;
  letter-spacing: 1px;
}
#app .preview-box div{
  text-align: left;
}


</style>





<style type="text/css">
  #mentioned-user-list{
  position: absolute;
  border: 1px solid #CCC;
  margin: 0;
  padding: 0;
  width: 200px;
  height: 200px;
  overflow-y: scroll;
}
#mentioned-user-list li{
  border-bottom: 1px solid #FFF;
  padding: 10px;
  list-style-type: none;
}
#mentioned-user-list li:hover{
  background: #ffffff;
}
#mentioned-user-list li.active{
  background-color: #d9d9d9;
  color: #FFF;
}
#mentioned-user-list li img{
  width: 24px;
  height: 24px;
  margin-right: 10px;
}
#mentioned-user-list li span{}
#app form textarea {
  border: 1px solid #d0d0d0;
  border-radius: 3px;
  width: 100%;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  margin: 10px 0;
  min-height: 100px;
  padding: 10px;
  font-size: 16px;
}
#app form textarea:focus {
  border-color: #42afe3;
  outline: 0;
}
</style>

<script type="text/javascript">
  (function( $ ) {
  var cachedName            = "";
  var fullCachedName        = "";
  var mentioningUser        = false;
  var textArea              = null;
  var container             = null;
  var userListWrapper       = $("<ul id='mentioned-user-list'></ul>");
  var userList              = null;
  var inputText             = null;
  var targetURL             = null;
  var onComplete            = null;
  var options               = null;
  var debugMode             = false;
  var debuggerBlock         = "<div id='mentionable-debugger'></div>"
  var caretStartPosition    = 0;
  var keyRespondingTimeOut  = null;
  var keyRespondTime        = 500;
  var listSize              = 0;
  var isUserFrameShown      = false;

  var KEY = {
    BACKSPACE:    8,
    DELETE:       46,
    TAB:          9,
    ENTER:        13,
    ESCAPE:       27,
    SPACE:        32,
    PAGE_UP:      33,
    PAGE_DOWN:    34,
    END:          35,
    HOME:         36,
    LEFT:         37,
    UP:           38,
    RIGHT:        39,
    DOWN:         40,
    NUMPAD_ENTER: 108,
    COMMA:        188,
    ATSIGN:       64
  };

  /*
   * make a textarea support user mentioning
   *
   * param usersURL             A url to fire an ajax call to retrieve user list.
   * param opts                 An options:
   *                              (id) the id of the user list block.
   *                              (minimumChar) the minimum number of character to trigger user data retrieval
   *                              (parameterName) the query parameter name
   *                              (position) the position of the list (right, bottom, left)
   * param onCompleteFunction   A callback function when user list is retrieved. Expected to be a user item generation.
   *
   */
  $.fn.mentionable = function(usersURL, opts, onCompleteFunction) {
    textArea  = this;

    // remove other mentionable text area before enable current one
    if($("textarea.mentionable-textarea").length){
      $("textarea.mentionable-textarea").val("");
      $("textarea.mentionable-textarea").off("keypress");
      $("textarea.mentionable-textarea").off("keyup");
    }

    container = textArea.parent();
    targetURL = usersURL;
    options   = $.extend({
      "id" : "mentioned-user-list",
      "minimumChar" : 2,
      "parameterName" : "mentioning",
      "position" : "bottom"
    }, opts);
    userListWrapper = $("<ul id='" + options.id + "'></ul>");

    if(debugMode){
      container.before(debuggerBlock);
    }

    this.keypress(function(e){

      watchKey();

      switch(e.keyCode){
        case KEY.ATSIGN:
          initNameCaching();
          break;
        case KEY.ENTER:
          if(mentioningUser){
            selectUser(userList.find("li.active"));
            e.preventDefault();
          }
          hideUserFrame();
          break;
        case KEY.SPACE:
          hideUserFrame();
          break;
        default:
          // Firefox hacked!
          // There is a problem on FF that @'s keycode returns 0.
          // The case KEY.ATSIGN fails to catch, so we need to do it here instead
          if(String.fromCharCode(e.charCode) == "@"){
            initNameCaching();
          }
          else{
            // append pressed character to cache
            if(cachedName != ""){
              cachedName += String.fromCharCode(e.charCode);
            }
          }
      }

      // if user typed any letter while the caret is not at the end
      // completely remove the string behind the caret.
      fullCachedName = cachedName;
    });
    this.keyup(function(e){
      switch(e.keyCode){
        case KEY.DELETE:
        case KEY.BACKSPACE:
          // delete or backspace key is pressed
          cachedName = cachedName.substring(0, cachedName.length -1);
          fullCachedName = cachedName;
          if(cachedName==""){
            hideUserFrame();
          }
          else{
            watchKey();
          }
          break;
        case KEY.ESCAPE:
          hideUserFrame();
          break;
        case KEY.LEFT:
          watchKey();
          caretMoveLeft();
          break;
        case KEY.UP:
          caretMoveUp();
          break;
        case KEY.RIGHT:
          watchKey();
          caretMoveRight();
          break;
        case KEY.DOWN:
        caretMoveDown();
          break;
      }
    });
  };

  /*
   * initialize a cache that store the user name that is being mentioned
   */
  function initNameCaching(){
    caretStartPosition = currentCaretPosition();
    cachedName         = "@";
  }

  /*
   * hide the user list frame, and clear some related stuffs
   */
  function hideUserFrame(){
    cachedName     = "";
    fullCachedName = "";
    listSize       = 0;
    mentioningUser = false;
    if(isUserFrameShown){
      userList.remove();
      isUserFrameShown = false;
    }
  }

  /*
   * show the user list frame
   */
  function showUserFrame(){
    container.append(userListWrapper);
    mentioningUser = true;


    userList = $("#" + options.id);
    if(options.position == "left"){
      userList.css("left", -1 * userList.outerWidth());
      userList.css("top", 0);
    }
    else if(options.position == "right"){
      userList.css("left", textArea.outerWidth());
      userList.css("top", 0);
    }
    else if(options.position == "bottom"){
      userList.css("left", 0);
      userList.css("top", textArea.outerHeight());
      userList.css("width", textArea.outerWidth());
    }

    userList.show();
    isUserFrameShown = true;
  }

  /*
   * replace @ with empyty string, then fire a request for user list
   */
  function populateItems(keyword){
    if(keyword.length > options.minimumChar){

      if(!isUserFrameShown){
        showUserFrame();
      }

      userList.html("");
      var data = {};
      if(keyword != undefined){
        data[options.parameterName] = keyword.substring(1, keyword.length);
      }
      if(onComplete != undefined){
        $.getJSON(targetURL, data, onComplete);
      }
      else{
        $.getJSON(targetURL, data, function(data){
          fillItems(data);
        });
      }
      bindItemClicked();
    }
  }

  /*
   * fill user name and image as a list item in user list block
   */
  function fillItems(data){
    if(data.length > 0){
      listSize = data.length;
      $.each(data, function(key, value){
        userList.append("<li><img src='" + value.image_url + "' /><span>" + value.name + "</span></li>");
      });
      userList.find("li:first-child").attr("class","active");
      bindItemClicked();
    }
    else{
      userList.append("<li>No user found</li>");
    }
  }

  /*
   * bind item clicked to all item in user list
   */
  function bindItemClicked(){
    // handle when user item is clicked.
    var userListItems = userList.find("li");
    userListItems.click(function(){
      selectUser($(this));
    });
  }

  /*
   * perform an user selection by adding the selected user name
   * to the text aprea
   */
  function selectUser(userItem){
    inputText    = textArea.val();
    replacedText = replaceString(caretStartPosition, caretStartPosition +
                                  fullCachedName.length, inputText, "@" +
                                  userItem.find("span").html());
    textArea.focus();
    textArea.val(replacedText);
    hideUserFrame();
  }

  function caretMoveLeft(){
    if(mentioningUser){
      //remove last char from cachedName while maintaining the fullCachedName
      if(cachedName != "@"){
        cachedName = fullCachedName.substring(0, cachedName.length - 1);
      }
      else{
        hideUserFrame();
      }
    }
  }

  function caretMoveRight(){
    if(mentioningUser){
      if(cachedName == fullCachedName){
        hideUserFrame();
      }
      else{
        //append to the tail the next character retrieved from fullCachedName
        cachedName = fullCachedName.substring(0, cachedName.length + 1);
      }
    }
  }

  function caretMoveUp(){

    currentUserItem = userList.find("li.active");
    if(currentUserItem.index() != 0){
      previousUserItem = currentUserItem.prev();
      currentUserItem.attr("class","");
      previousUserItem.attr("class","active");
      userList.scrollTop(previousUserItem.index()*previousUserItem.outerHeight());
    }
  }

  function caretMoveDown(){
    currentUserItem = userList.find("li.active");
    if(currentUserItem.index() != listSize-1){
      nextUserItem = currentUserItem.next();
      currentUserItem.attr("class","");
      nextUserItem.attr("class","active");
      userList.scrollTop(nextUserItem.index()*nextUserItem.outerHeight());
    }
  }

  function debug(){
    myDebugger = $("#mentionable-debugger");
    myDebugger.html("<b>cache : </b>" + cachedName +" | <b>full cache : </b>" + fullCachedName);
  }

  /*
   * return an integer of a curret caret position
   */
  function currentCaretPosition(){
    caretContainer = textArea[0];
    return caretContainer.selectionStart;
  }

  /*
   * replace a part of originalString from [from] to [to] position with addedString
   * param from               An integer of a begining position
   * param to                 An itenger of an ending position
   * param originalString     An original string to be partialy replaced
   * param addedString        A string to be replaced
   */
  function replaceString(from, to, originalString, addedString){
    try{
      if(from == 0){
        return addedString + originalString.substring(to, originalString.length);
      }
      if(from != 0){
        firstChunk = originalString.substring(0, from);
        lastChunk  = originalString.substring(to, originalString.length);
        return firstChunk + addedString + lastChunk;
      }
    }
    catch(error){
      return originalString;
    }
  }

  /*
   * initialize the key timeout. It will observe the user interaction.
   * If the user did not respond within a specific time, e.g. pausing typing,
   * it will fire poplateItems()
   */
  function watchKey(){
    clearTimeout(keyRespondingTimeOut);
    keyRespondingTimeOut = setTimeout(
      function(){
        populateItems(cachedName);
      },
      keyRespondTime
    );
  }

  /*
   * return a jquery object of the user list item that is in an active state
   */
  function activeUserItemIndex(){
    return userList.find("li.active").index();
  }

})( jQuery );
</script>

  
    <!-- <div class="msg-form-content">
      <div class="msg-form-header"> Update Status
        <div class="msg-loader" id="post-loader" style="display:none"><img src="loading.gif" /></div>
      </div>
      <form id="form" name="form" action="" method="POST" >
        <div class="msg-form-inner">
          <textarea id="message" class="message-form" placeholder="What's on your mind?" name="message"></textarea>
        </div>
        <div type="button" name="action" class="msg-btn" value="Post"><a onclick="updatedata()" class="uibutton large confirm">Post</a></div>
      </form>
    </div> -->
    <div id="app" style="position: relative;">
      <textarea id="textarea"></textarea>
    </div>
<script type="text/javascript">
// Commit error
  $("#app").mentionable(
    "ajax/tagList.json",
    {minimumChar: 1, parameterName: "genre_tag"}
    );
</script>
