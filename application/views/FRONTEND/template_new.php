<!DOCTYPE html>
<html>
<head>
    <?php $lang= 'en'; ?>
    <title><?=$title?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="REDM">
    <meta name="keywords" content="REDM">
    <link rel="icon" href="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>" type="image/x-icon" />
    <link href="<?=get_resource_url('assets/libs/bootstrap/bootstrap.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/font-awesome/font-awesome.min.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/libs/slick/slick-theme.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/fstyle.css?ver=1.0')?>" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style.css?ver=1.0')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style2.css?ver=1.0')?>" type="text/css" rel="stylesheet">
    <link href="<?=get_resource_url('assets/css/style3.css?ver=1.0')?>" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <?php
        $link_css = modules::run('home/get_link_css');
        foreach ($link_css as $key => $css) {
            if(!empty($css)) { ?>
    <link href="<?=get_resource_url($css->file_css)?>" rel="stylesheet">
    <?php } } ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiJHmYESPNUKsb6YIVaYIOivKZPTQnJ2M&amp;libraries=places&language=en"></script>
    <script type="text/javascript">
        function initialize() {
        var options = {types: ['(regions)']};
        var input = document.getElementById('locationAdmincp');
        var input1 = document.getElementById('locationAdmincp1');
        var input2 = document.getElementById('locationAdmincp2');
        var input3 = document.getElementById('locationAdmincp3'); 
        var autocomplete = new google.maps.places.Autocomplete(input , options);
        var autocomplete = new google.maps.places.Autocomplete(input1 , options);
        var autocomplete = new google.maps.places.Autocomplete(input2 , options);
        var autocomplete = new google.maps.places.Autocomplete(input3 , options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', 
            function() {
            var address_components=autocomplete.getPlace().address_components;
            var city='';
            var country='';
            for(var j =0 ;j<address_components.length;j++)
            {
             city =address_components[0].long_name;
             if(address_components[j].types[0]=='country')
                {
                    country=address_components[j].long_name;
                }
            }
            document.getElementById('countryAdmincp').value = country;
            document.getElementById('citiesAdmincp').value = city;
        });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script src="<?=get_resource_url('assets/libs/jquery/jquery-1.12.4.min.js?ver=1.0')?>"></script>
    <style>
        <?php
            $background_list = modules::run('home/get_background');
            foreach ($background_list as $key => $result) {
                if( ! empty($result->background)) { ?>
        body{
        background: url(<?=get_resource_url($result->background)?>);
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-color: #1d1d1b;
        position: relative;
        }
        <?php } else { // else default ?>
        body{
        background:rgb(25, 25, 25) !important;
        }
        <?php } } ?>
        .parallax-mirror{

        }
    </style>
</head>
<body>
    <div class="div-loader">
        <div class="loader-inner">
            <div class="inner one"></div>
            <div class="inner two"></div>
            <div class="inner three"></div>
        </div>
    </div>
    <div class="parallax-container" data-parallax="scroll" data-bleed="10" data-speed="0.2" data-image-src="<?=get_resource_url('assets/uploads/images/2017/08/admincp_background_1503545767.jpg')?>" data-natural-width="1400" data-natural-height="1400" style="position: absolute; top: -70px; right: 0; bottom: 0; left: 0;"></div>
    <!-- Menu -->
    <div class="menu">
        <div class="top-menu">
            <div class="icon-show-menu-mobile  pull-left">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="logo pull-left">
                <a href="<?=PATH_URL?>">
                <img src="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>">
                </a>
            </div>
            <div class="text-logo pull-left">
                <a href="/#" class="text-logo-item">
                #RECLAIM.EDM
                </a>
            </div>
            <div class="wrap-slide-text pull-left">
                <marquee class="pull-left slide-text" scrolldelay="0" direction="left">
                    <?php
                        $header_tickers = modules::run('home/get_header_ticker');
                        foreach ($header_tickers as $result){
                           echo '<div class="run-text">'. $result->contents . '</div>';
                                           } ?>
                </marquee>
            </div>
            <div class="wrap-login pull-right">
                <?php
                    if (isset($this->session->userdata['userData'])) {
                        $userData['username'] = ($this->session->userdata['userData']['username']);
                        $userDataSetting1 = modules::run('home/load_list_user_info', $this->session->userdata('userData')['id']);
                        foreach ($userDataSetting1 as $value) {
                          $link_profile = $value->link_url_redm;
                        } ?>
                    <div class="menu-profile">
                        <div class="menu-style-social pull-right">
                            <div class="search-social pull-left icon-find">
                                <span><img src="<?=get_resource_url('assets/images/icon/find.png')?>"></span>
                            </div>
                            <div class="search-social pull-left icon-music">
                                <span><img src="<?=get_resource_url('assets/images/icon/music.png')?>"></span>
                            </div>
                            <div class="search-social pull-left icon-fire">
                                <span><img src="<?=get_resource_url('assets/images/icon/fire.png')?>"></span>
                            </div>
                        </div>
                        <div class="menu-profile-right pull-right">
                            <div class="click-menu-profile">
                                <i class="fa-rotate fa fa-angle-double-down" aria-hidden="true"></i>
                                <?php
                                    if (!empty($authUrl)) { redirect($authUrl); 
                                    } else { ?>
                                        <span><? echo $userData['username']; ?></span>
                                <?php } ?>
                            </div>
                            <ul class="menu-profile-toggle">
                                <li class="profile-title profile-tickets">My Tickets</li>
                                <li class="profile-title profile-view">My Profile</li>
                                <li class="profile-title profile-setting">Profile Setting</li>
                                <li class="profile-title"><a href="<?=PATH_URL?>en/profile/<?php echo $link_profile ?>">New Feed</a></li>
                                <li class="profile-title profile-logout"><a class="btn-logout" href="<?=PATH_URL.'ajax/logout'?>">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="login pull-right">
                        <a href="#"><div class="power-off-login"></div></a>
                    </div>
                <?php } ?>
                <div class="clear-fix"></div>
            </div>
        </div>
        <div class="wrap-side-menu"></div>
        <div class="side-menu">
            <div class="side-menu-view">
                <div class="side-menu-top">
                    <ul class="list-side-menu">
                        <a class="side-menu-link side-menu-link-search">
                            <li>
                                <input type="text" class="mobile-search" />
                                <img class="img-mobile-search" src="<?=get_resource_url('assets/images/icon/find.png')?>">
                            </li>
                        </a>
                        <?php $menu_list = modules::run('home/get_list_menu');
                            foreach ($menu_list as $key => $menu) { ?>
                            <a class="side-menu-link" href="<?=PATH_URL.$lang?>/blog">
                                <li class="side-menu-item"><?=$menu->name?></li>
                            </a>
                        <?php } ?>
                        <li class="mobile-social">
                            <a href="social-item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="social-item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="social-item"><i class="fa fa-soundcloud" aria-hidden="true"></i></a>
                            <a href="social-item"><i class="fa fa-mixcloud" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="side-menu-middle">
                    <div class="wrap-trending">
                        <p class="title-trending">
                            TRENDING
                        </p>
                        <ul class="list-trending">
                            <li class="item-trending">
                                <a href="#">house</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">techno</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">electro</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">nujazz</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">hiphop</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">ukgarage</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">dj</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">producer</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">london</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">tokyo</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">singapore</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">morekeywords</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">hashtags</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">alignleft</a>
                            </li>
                            <li class="item-trending">
                                <a href="#">nojustify</a>
                            </li>
                        </ul>
                    </div>
                    <div class="side-menu-banner">
                        <img src="<?=get_resource_url('assets/images/banner_menu_side.png')?>" />
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu">
            <div class="mobile-top-menu">
                <div class="icon-show-menu-mobile  pull-left">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <div class="logo pull-left">
                    <a href="#">
                    <img src="<?=get_resource_url('assets/images/redm-web-1_03.jpg')?>">
                    </a>
                </div>
                <div class="text-logo pull-left">
                    <a href="#" class="text-logo-item">#RECLAIM.EDM</a>
                </div>
                <div class="wrap-slide-text pull-left ">
                    <marquee class="pull-left slide-text" scrolldelay="0" scrollamount="3">
                        <div class="run-text">This will be a running news ticker </div>
                        <div class="run-text">they will be clickable links </div>
                        <div class="run-text"> TEXT MOVES FROM RIGHT TO LEFT </div>
                    </marquee>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="mobile-side-menu">
                <div class="mobile-side-menu-view">
                    <div class="side-menu-top">
                        <ul class="list-side-menu">
                            <?php
                                if (isset($this->session->userdata['userData'])) 
                                {
                                $userData['username'] = ($this->session->userdata['userData']['username']);
                                ?>
                            <li class="mobile-user-logined mobile-user-logined-menu">
                                <div class="menu-profile-right">
                                    <div class="click-menu-profile"><?=$userData['username']?><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
                                    <ul class="menu-profile-toggle">
                                        <li class="profile-title profile-tickets">My Tickets</li>
                                        <li class="profile-title profile-view">My Profile</li>
                                        <li class="profile-title profile-setting">Profile Setting</li>
                                        <li class="profile-title profile-logout"><a class="btn-logout" href="<?=PATH_URL.'ajax/logout'?>">Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                            <?php
                                }
                                else
                                {
                                ?>
                            <a class="side-menu-link side-menu-link-login">
                                <li class="side-menu-item mobile-login">
                                    <div class="power-off-login">
                                        <i class="fa fa-power-off" aria-hidden="true"></i>
                                    </div>
                                </li>
                            </a>
                            <?php
                                }
                                ?>
                            <a class="side-menu-link side-menu-link-search">
                                <li class="mobile-search-style">
                                    <input type="text" class="mobile-search" />
                                    <img class="img-mobile-search" src="<?=get_resource_url('assets/images/icon/find.png')?>">
                                </li>
                            </a>
                            <a class="side-menu-link" href="<?=PATH_URL.$lang?>/blog">
                                <li class="side-menu-item">ARTISTS</li>
                            </a>
                            <a class="side-menu-link" href="">
                                <li class="side-menu-item">LISTEN</li>
                            </a>
                            <a class="side-menu-link" href="#">
                                <li class="side-menu-item">READ</li>
                            </a>
                            <a class="side-menu-link" href="#">
                                <li class="side-menu-item">CLASSICS</li>
                            </a>
                            <a class="side-menu-link" href="#">
                                <li class="side-menu-item">VENUES & PROMOTERS</li>
                            </a>
                            <a class="side-menu-link" href="#">
                                <li class="side-menu-item">SIGN IN FOR PROMOTIONS</li>
                            </a>
                            <li class="mobile-social">
                                <a href="social-item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="social-item"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="social-item"><i class="fa fa-soundcloud" aria-hidden="true"></i></a>
                                <a href="social-item"><i class="fa fa-mixcloud" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="side-menu-middle">
                        <div class="wrap-trending">
                            <p class="title-trending">
                                TRENDING
                            </p>
                            <ul class="list-trending">
                                <li class="item-trending">
                                    <a href="#">house</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">techno</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">electro</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">nujazz</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">hiphop</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">ukgarage</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">dj</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">producer</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">london</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">tokyo</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">singapore</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">morekeywords</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">hashtags</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">alignleft</a>
                                </li>
                                <li class="item-trending">
                                    <a href="#">nojustify</a>
                                </li>
                            </ul>
                        </div>
                        <div class="side-menu-banner">
                            <img src="<?=get_resource_url('assets/images/banner_menu_side.png')?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-right-menu">
                <div class="display-table">
                    <div class="display-table-cell">
                        <div class="mobile-wrap-right-menu">
                            <div class="mobile-signup">
                                SIGN UP
                            </div>
                            <div class="mobile-login">
                                LOGIN
                            </div>
                            <div class="mobile-social mg-b10">
                                <a href="#" class="social-item">
                                FB
                                </a>
                                <a href="#" class="social-item">
                                TW
                                </a>
                                <a href="#" class="social-item">
                                SC
                                </a>
                                <a href="#" class="social-item">
                                MC
                                </a>
                            </div>
                            <input type="text" class="mobile-search" placeholder="Search" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="signup-right">
            <div class="signup-wrap">
                <div class="tab-menu-signup pd-t60">
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#login-form">LOGIN</a></li>
                        <li class="active"><a data-toggle="tab" href="#signup-form">SIGN UP</a></li>
                        <?php if (isset($this->session->userdata['userData'])) { ?>
                        <li><a data-toggle="tab" href="#menu1">SUBSCRIPTION</a></li>
                        <li><a data-toggle="tab" href="#menu2">REPORTS</a></li>
                        <li><a data-toggle="tab" href="#menu3">NEWS</a></li>
                        <?php } ?>
                        <li class="close-signup"><a href="#">close</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <form id="login-form"  class="tab-pane fade in ">
                        <div class="login-wrap">
                            <div class="login-left col-md-6 col-sm-6 col-xs-12">
                                <div class="login-style">
                                    <label class="mg-t40 mg-b20">ENTER YOUR EMAIL/USER NAME</label>
                                    <input class="username-input" type="text" name="username" val-message="Số lượng từ không hợp lệ" data-required="true" data-check="true" data-error="Please enter your username" value="" />
                                    <label class="mg-t40 mg-b20">ENTER YOUR PASSWORD</label>
                                    <input type="PASSWORD" name="password" value=""  data-required="true" data-error="Please enter your country"/>
                                </div>
                                <div id="logerror"></div>
                                <div class="login-checkbox">
                                    <input type="checkbox" name="remember" value="remember"> Remember me<br>
                                </div>
                            </div>
                            <div class="line-login"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="login-type">
                                        <a class="login-with-fb" href="<?=PATH_URL?>ajax/login_fb">Login with Facebook</a>
                                        <a class="login-with-gg mg-t10 mg-b10" href="<?=PATH_URL . 'ajax/login_gg'?>">Login with Google+</a>
                                        <a class="forgot-link" data-toggle="modal" data-target="#forgot-mobal">Forgot your password?</a>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-pre-next">
                                <input type="submit" id="btn-login" data-url="<?=PATH_URL?>" name="login" class="login-btn  mg-b50 mg-t60 mg-sm-t20" value="Login" />
                            </div>
                        </div>
                    </form>
                    <form id="signup-form" onkeydown="TriggeredKey(this)" method="post" action="" class="signup-form tab-pane fade in active">
                        <section class="active section-form" data-step="1">
                            <h3 class="title-step mg-t30">Step 1</h3>
                            <div class="step1-wrap">
                                <div class="step1">
                                    <div>
                                        <label for="email" class="mg-b20">ENTER YOUR EMAIL</label>
                                        <input id="email" type="email" name="email" data-required="true" data-error="Please enter your email" />
                                        <!-- <span class="error">A valid email address is required</span> -->
                                    </div>
                                    <label class="mg-t40 mg-b20">ENTER YOUR USER NAME</label>
                                    <input class="username-input" type="text" name="username" val-message="Invalid Number Your Name" data-required="true" data-check="true" data-error="Please enter your username" value="" />
                                    <label class="mg-t40 mg-b20">ENTER YOUR PASSWORD</label>
                                    <input type="password" name="password" value=""  data-required="true" data-error="Please enter your password"/>
                                    <style>
                                        .pac-container{
                                        top:150px !important;
                                        }
                                    </style>
                                    <label class="mg-t40 mg-b20">ENTER YOUR COUNTRY</label>
                                    <input id="searchTextField" placeholder="" type="text" name="location" value=""  data-required="true" data-error="Please enter your country"/>
                                    <input type="hidden" name="country" value="" type="text" id="country-hide-sign">
                                    <input type="hidden" name="city" value="" type="text" id="city-hide-sign">
                                </div>
                                <div class="btn-pre-next">
                                    <input type="button" name="next" class="signup-next-btn  mg-b50" value="Next" />
                                </div>
                            </div>
                        </section>
                        <section class="section-form" data-step="2">
                            <h3 class="title-step mg-t30">Step 2</h3>
                            <div class="des-step-2 mg-t30">You may pick more than one.</div>
                            <div class="step2-wrap">
                                <div class="step2">
                                    <div class="mg-t80">are you a...</div>
                                    <div class="mg-t30">
                                        <?php foreach ($type_list as $key => $value) {?>
                                        <input type="radio" name="gender" value="<?php echo $value->id?>"><?php echo $value->type?></br>
                                        <?php  } ?>
                                    </div>
                                </div>
                                <div class="btn-pre-next">
                                    <input type="button" name="previous" class="signup-pre-btn mg-b20" value="Previous" />
                                    <input type="button" name="next" class="signup-next-btn  mg-b50" value="Next" />
                                </div>
                            </div>
                        </section>
                        <section class="section-form bg-333" data-step="3">
                            <h3 class="title-step mg-t30">Step 3</h3>
                            <div class="des-step-2 mg-t30">Your preferences and profile settings.</div>
                            <!-- <button id="btn-signup" onclick="return save_signup();" type="submit" name="next" class="signup-save-btn">Save</button> -->
                            <div class="step3 mg-t20">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="step3-input url">
                                            <label>Age</label>
                                            <select name="age" class="select-age">
                                                <?php for($i=1;$i<=100;$i++){ ?>
                                                <option value="<?php echo $i ?>" <?php echo ($i == '20' ) ? 'selected' : '' ?>><?php echo $i ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="step3-input url">
                                            <label>Gender</label>
                                            <select name="gender" class="select-age">
                                                <option value="1">Female</option>
                                                <option value="2">Male</option>
                                                <option value="3">Unknown</option>
                                            </select>
                                        </div>
                                        <div class="step3-input founder">
                                            <label>Founder</label>
                                            <input type="text" name="founder" />
                                        </div>
                                        <div class="step3-input biiography">
                                            <label>Biography</label>
                                            <textarea type="text" name="biiography" rows="2"></textarea>
                                        </div>
                                        <div class="step3-input shortbio">
                                            <label>Short bio</label>
                                            <textarea type="text" name="shortbio" rows="1"></textarea>
                                        </div>
                                        <div class="step3-input status">
                                            <label>Status</label>
                                            <textarea type="text" name="status_genenal" rows="1"></textarea>
                                        </div>
                                        <div class="step3-input marketing">
                                            <label>Marketing</label>
                                            <input type="text" name="marketing" />
                                        </div>
                                        <div class="step3-input contact">
                                            <label>Additional Contacts</label>
                                            <input type="text" name="contact" />
                                        </div>
                                        <div class="step3-input distributor">
                                            <label>Distributor/s</label>
                                            <input type="text" name="distributor" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="step3-input url">
                                            <label>URL</label>
                                            <input type="text" name="url" />
                                        </div>
                                        <div class="step3-input facebook">
                                            <label>URL Facebook </label>
                                            <input type="text" name="facebook" />
                                        </div>
                                        <div class="step3-input twitter">
                                            <label>URL Twitter</label>
                                            <input type="text" name="twitter" />
                                        </div>
                                        <div class="step3-input bandcamp">
                                            <label>URL Bandcamp</label>
                                            <input type="text" name="bandcamp" />
                                        </div>
                                        <div class="step3-input soundcloud">
                                            <label>URL Soundcloud</label>
                                            <input type="text" name="soundcloud" />
                                        </div>
                                        <div class="step3-input mixcloud">
                                            <label>URL Mixcloud</label>
                                            <input type="text" name="mixcloud" />
                                        </div>
                                        <div class="step3-input itunes">
                                            <label>URL iTunes</label>
                                            <input type="text" name=itunes />
                                        </div>
                                        <div class="step3-input beatport">
                                            <label>URL Beatport</label>
                                            <input type="text" name="beatport" />
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-pre-next">
                                    <input type="button" name="previous" class="signup-pre-btn mg-b50" value="Previous" />
                                    <button  id="btn-signup" data-url="<?=PATH_URL?>" type="submit" name="next" class="signup-submit-btn">Submit</button>
                                </div>
                            </div>
                        </section>
                    </form>
                    <div id="menu1" class="tab-pane fade">
                        <h3>SUBSCRIPTION</h3>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>REPORTS</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>NEWS</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-modal-signup">
            <div class="modal fade" id="forgot-mobal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Forgot your password?</h4>
                        </div>
                        <div class="modal-body">
                            <div class="forgot-content">
                                <form id ="for-got-pass" class = "for-got-pass">
                                    <label for="email" class="mg-b20 mg-t20">ENTER YOUR EMAIL TO RESET PASSWORD</label><br/>
                                    <input id="email" type="email" name="email" data-required="true" data-error="Please enter your email" />
                                </form>
                                <div class="btn-pre-next">
                                    <input id ="for-got-password" type="submit" name="for-got-password" data-url="<?=PATH_URL?>" class="login-btn mg-b30 mg-t30" value="Send" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Menu -->
    
    <!-- profile view-->
    <!-- /profile view -->

    <!-- profile setting-->
    <!-- /profile setting -->

    <main>
        <?=$content?>
    </main>
    <!-- call js page -->
    <script src="<?=get_resource_url('assets/libs/parallax/parallax.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/slick/slick.min.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/libs/nicescroll/nicescroll.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/style.js?ver=1.0')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fancybox.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.mousewheel-3.0.6.pack.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/helper.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_comment.js')?>"></script>
    <script src="<?=get_resource_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.validate.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.numeric.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/bootstrap.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/jquery.fitvids.js')?>"></script>     
    <script src="<?=get_resource_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/admin/jquery.form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/ajax_submit_form.js')?>"></script>
    <script src="<?=get_resource_url('assets/js/js_page_comment.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=get_resource_url('assets/css/croppic.css')?>">
    <script src="<?=get_resource_url('assets/js/croppic.js')?>"></script>
    <script type="text/javascript" src="<?=PATH_URL.'assets/js/admin/'?>jquery.slugit.js"></script>
    <script src="<?=get_resource_url('assets/js/admin/uploader/jquery.plupload.queue.js')?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?=get_resource_url('assets/wysiwyg_editor/tinymce/tinymce.min.js')?>"></script>
    <script type="text/javascript">
        function initialize() {
            var options = {
                types: ['(regions)']
            };
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input , options);
        
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var address_components=autocomplete.getPlace().address_components;
                var city='';
                var country='';
                for(var j =0 ;j<address_components.length;j++) {
                    city =address_components[0].long_name;
                
                    if(address_components[j].types[0]=='country') {
                       country=address_components[j].long_name;
                       console.log(address_components[0]);
                    }
                }
                document.getElementById('country-hide-sign').value = country;
                document.getElementById('city-hide-sign').value = city;
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        
    </script>
    <script type="text/javascript">
        tinymce.init({ 
            selector: '#contentAdmincp',
            plugins: "code, link, image, lists, preview, textcolor, pix_embed_online_media, responsivefilemanager",
            toolbar: ['undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect forecolor backcolor | bullist numlist outdent indent | link image | print preview media fullpage removeformat','pix_embed_online_media | responsivefilemanager'],
            external_filemanager_path:"<?=PATH_URL?>/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            relative_urls: false,
            setup: function (editor) {
                editor.on('keydown', function(e) {}).on('keyup', function(e) { 
                    var key = e.keyCode || e.which; 
                    if (key == 13) {
                        var content = tinyMCE.get('contentAdmincp').getContent();
                        var url = 'ajax/url_Content';
                        var root = $(".submit-btn-profile").attr("data-url") + url; 
                        $.ajax({
                            type: 'POST',
                            url: root,
                            data: {'content': content},
                            success: function(data) {
                                let myObj = JSON.parse(data);
                                if (myObj.success == true) {
                                    console.log(myObj.post_url);
                                    $('#post_url').html(myObj.post_url);
                                }
                            }
                        })
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        textarea{color:black !important;position:relative;z-index:999;}
        #cropic_element {height: 200px;width: 200px;}
        .width-select{width: 150px !important;height: 30px !important;font-weight: 400;}
    </style>
    <script>
        var croppicHeaderOptions = {    
            uploadUrl:"<?=get_resource_url('img_upload_to_file.php')?>",
            cropUrl:"<?=get_resource_url('img_crop_to_file.php')?>",
            uploadData:{'prefix':'avatar_'},
            enableMousescroll:true,
            customUploadButtonId:'cropContainerHeaderButton',
            outputUrlId:'input_thumbnail_url',
            modal:true,
            rotateControls: false,
            doubleZoomControls:false,
            imgEyecandyOpacity:0.4,
            onBeforeImgUpload: function(){ },
            onAfterImgUpload: function(){ appendOriginImageAward(); },
            onImgDrag: function(){ },
            onImgZoom: function(){ },
            onBeforeImgCrop: function(){ },
            onAfterImgCrop:function(){appendAward(); },
            onReset:function(){ onResetCropic(); },
            onError:function(errormessage){ console.log('onError:'+errormessage) }
        }   
        var croppic = new Croppic('cropic_element', croppicHeaderOptions);
        function appendOriginImageAward() {
            var url_origin = $("div#croppicModalObj > img").attr('src');
            $('#input_image_url').val(url_origin);
            $("#preview_image_fancybox").attr("href", $('#input_image_url').val());
        }
        function appendAward(){
            $("#preview_image").attr("src", $('#input_thumbnail_url').val());
            $("#preview_image").show();
        }
        function onResetCropic(){
            $('#input_image_url').val('');
            $('#input_thumbnail_url').val('');
            $("#preview_image_fancybox").attr("href", '');
            $("#preview_image").attr("src", '');
        }
        var clickEvent = undefined;
        if (Modernizr.touch) { 
            clickEvent = "touchstart";
        } else {
            clickEvent = "click";
        }
        $(document).ready(function(){
            /*$(".side-menu").bind(clickEvent,function(e){
                $(this).addClass('active');
                if(!$(this).hasClass('active')){
                    $(this).find('.side-menu-link').click(function(){
                        return false;
                    });
                }
                //alert(clickEvent);
            });*/
        });

    </script>
    <?php if ( ! is_localhost()) {
            //admincp_script
            $get_script = modules::run('home/get_script');
            foreach ($get_script as $result) {
                echo $result->script;
            }
        } ?>
</body>
</html>