<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'users';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home'; //home
// $route['404_override'] = 'home/pagemissing';
$route['translate_uri_dashes'] = FALSE;

//Config Router Multi Language
// $route['^(en|cn)/(.+)$'] = "$1";
$route['^(en|cn)$'] = $route['default_controller'];
$route['^(en|cn)'.'/artists'] = 'home/artist_Page';
$route[AJAX_PREFIX . '/signup'] ='home/signup';
$route[AJAX_PREFIX . '/login'] ='home/login';
$route[AJAX_PREFIX . '/magazine'] ='home/blog';
$route[AJAX_PREFIX . '/blog'] ='home/blog';
$route[AJAX_PREFIX . '/read'] ='home/read';
$route[AJAX_PREFIX . '/read/country'] ='home/read_country';
$route[AJAX_PREFIX . '/logout'] ='home/logout';
$route[AJAX_PREFIX . '/user'] ='home/profile';
$route[AJAX_PREFIX . '/artist'] ='home/proPage';
$route[AJAX_PREFIX . '/event/(:any)'] ='admincp_login_facebook/get_event/$1';
$route[AJAX_PREFIX . '/save_event/(:any)'] ='admincp_login_facebook/save_event/$1';
$route[AJAX_PREFIX . '/login_fb_access_token'] ='admincp_login_facebook/login_fb'; // Propage - Add Events (from Facebook)
$route[AJAX_PREFIX . '/login_fb_get_event_access_token'] ='admincp_getEvents_facebook/login_fb';
$route[AJAX_PREFIX . '/connect_facebook'] ='admincp_login_facebook/connect_facebook';
$route[AJAX_PREFIX . '/connect_event'] ='admincp_login_facebook/connect_event';
$route[AJAX_PREFIX . '/login_fb'] ='home/login_fb';
$route[AJAX_PREFIX . '/changePassword'] ='home/changePassword';
$route[AJAX_PREFIX . '/addContent'] ='home/AddContent/addContent';
$route[AJAX_PREFIX . '/url_Content'] ='home/AddContent/url_addContent';
$route[AJAX_PREFIX . '/subscription'] ='home/subscription_setting';

$route[AJAX_PREFIX . '/event/save'] ='Admincp_login_facebook/save/';

$route[AJAX_PREFIX . '/for-got-password'] ='home/for_got_password';
$route[AJAX_PREFIX . '/update'] ='home/update_user';
$route[AJAX_PREFIX . '/comment'] ='home/insert_com';

$route[AJAX_PREFIX . '/login_gg'] ='home/login_google';

$route[AJAX_PREFIX . '/articles_latest'] ='home/articles_latest_ajax';
$route[AJAX_PREFIX . '/event'] ='home/event_list_ajax';
$route[AJAX_PREFIX . '/search_event'] ='home/search_event_list_ajax';


$route['^(en|cn)/comments'] = 'home/insert_user_comment';
$route['^(en|cn)/addevent'] = 'home/load_addevent';
$route['^(en|cn)/newsfeed'] = 'home/newsfeed';

$route['^(en|cn)/ajax_comment'] = 'home/ajax_loadcomment';
$route['^(en|cn)/ajax_comment_reply'] = 'home/ajax_loadcomment_reply';


$route['^(en|cn)/comments_newfeed'] = 'home/insert_comment_newfeed';
$route['^(en|cn)/comments_newfeed_reply'] = 'home/insert_comment_newfeed_reply';


$route['^(en|cn)/ajax_comment_newfeed'] = 'home/ajax_loadcomment_newfeed';
$route['^(en|cn)/ajax_comment_newfeed_reply'] = 'home/ajax_loadcomment_newfeed_reply';


$route['^(en|cn)/ajax_loadnewfeed'] = 'home/ajax_loadnewfeed';

$route['^(en|cn)/ajax_loadnewfeed/(:num)'] = 'home/ajax_loadnewfeed/$2';

$route['^(en|cn)/up_newfeed'] = 'home/up_newfeed';


$route['^(en|cn)/comments-reply'] = 'home/insert_user_comment_child';

$route['^(en|cn)/magazine/(:any)'] = 'home/blog/$2';
$route['^(en|cn)/blog/(:any)'] = 'home/blog/$2';

$route['^(en|cn)/read/(:any)'] = 'home/read/$2';
$route['^(en|cn)/read/country/(:any)'] = 'home/read_country/$2';

$route['^(en|cn)/user/(:any)'] = 'home/profile/$2';
$route['^(en|cn)/artist/(:any)'] = 'home/proPage/$2';

$route['^(en|cn)/search'] = 'home/searchByGoogle';

$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/magazine'];
//$route['^(en|cn)/blog/(:any)'] = 'home/blog/$2';
$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/user'];
$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/artist'];

$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/read'];
$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/read/country'];

//$route['^(en|cn)/(.+)$'] = $route[AJAX_PREFIX . '/profile'];

$route[AJAX_PREFIX . '/test-editor'] ='home/test_editor';
$route[AJAX_PREFIX . '/test-onesignal'] ='home/test_onesignal';
$route[AJAX_PREFIX . '/send-onesignal'] ='home/send_notify_by_onesignal';
$route[AJAX_PREFIX . '/test-ga'] = 'payment/ga_analytics';
$route[AJAX_PREFIX . '/test-ads'] = 'home/test_ads';
$route[AJAX_PREFIX . '/get-active-user'] = 'payment/ga_analytics/get_active_users';
$route[AJAX_PREFIX . '/retrieve-data'] = 'payment/ga_analytics/retrieve_data';
$route[AJAX_PREFIX . '/test_date'] = 'payment/ga_analytics/test_date';
$route[AJAX_PREFIX . '/test-pusher'] = 'home/PusherService/test_private';
$route[AJAX_PREFIX . '/auth-pusher'] = 'home/PusherService/auth_pusher';
$route[AJAX_PREFIX . '/test-pusher'] = 'home/PusherServiceHomePages/test_private';
$route[AJAX_PREFIX . '/auth-pusher'] = 'home/PusherServiceHomePages/auth_pusher';

$route[AJAX_PREFIX . '/send-pusher'] = 'home/PusherService/sendPusher';
$route[AJAX_PREFIX . '/test-pusher-1'] = 'home/test_pusher_1';
$route[AJAX_PREFIX . '/submit-feed'] = 'home/PusherService/submit_feed';
$route[AJAX_PREFIX . '/submit-comment'] = 'home/PusherService/submit_comment';
$route[AJAX_PREFIX . '/submit-reply'] = 'home/PusherService/submit_reply';
$route[AJAX_PREFIX . '/like-feed-hot'] = 'home/PusherService/like_feed_hot';
$route[AJAX_PREFIX . '/like-feed-cool'] = 'home/PusherService/like_feed_cool';
$route[AJAX_PREFIX . '/test-feed'] = 'home/PusherService/test';	

$route[FEED_PREFIX . '/auth-pusher'] = 'home/Feed/authPusher';
$route[FEED_PREFIX . '/test-feed'] = 'home/Feed/showProfileByUserBitId';
$route[FEED_PREFIX . '/test-feed/(:any)'] = 'home/Feed/showProfileByUserSlug/$1';
$route[FEED_PREFIX . '/submit-feed'] = 'home/Feed/submit_feed';
$route[FEED_PREFIX . '/submit-comment'] = 'home/Feed/submit_comment';
$route[FEED_PREFIX . '/submit-reply'] = 'home/Feed/submit_reply';
$route[FEED_PREFIX . '/like-feed'] = 'home/Feed/like_feed';
$route[FEED_PREFIX . '/load-feed'] = 'home/Feed/load_feed';
$route[FEED_PREFIX . '/load-comment'] = 'home/Feed/load_comment';
$route[FEED_PREFIX . '/load-reply'] = 'home/Feed/load_reply';
$route[FEED_PREFIX . '/upgrade'] = 'home/Feed/test';	

$route[AJAX_PREFIX . '/send-pusher-home'] = 'home/PusherServiceHomePages/sendPusher';
$route[AJAX_PREFIX . '/test-pusher-1-home'] = 'home/test_pusher_1';
$route[AJAX_PREFIX . '/submit-feed-home'] = 'home/PusherServiceHomePages/submit_feed';
$route[AJAX_PREFIX . '/submit-feed-home-home'] = 'home/PusherServiceHomePages/submit_feed_home';
$route[AJAX_PREFIX . '/submit-comment-home'] = 'home/PusherServiceHomePages/submit_comment';
$route[AJAX_PREFIX . '/submit-reply-home'] = 'home/PusherServiceHomePages/submit_reply';
$route[AJAX_PREFIX . '/like-feed-hot-home'] = 'home/PusherServiceHomePages/like_feed_hot';
$route[AJAX_PREFIX . '/like-feed-cool-home'] = 'home/PusherServiceHomePages/like_feed_cool';
$route[AJAX_PREFIX . '/share-feed-home'] = 'home/PusherServiceHomePages/share_feed';
$route[AJAX_PREFIX . '/test-feed-home'] = 'home/PusherServiceHomePages/test';

$route[FEED_PREFIX . '/auth-pusher-home'] = 'home/FeedHomePages/authPusher';
$route[FEED_PREFIX . '/home'] = 'home/FeedHomePages/showHomePage';
$route[FEED_PREFIX . '/home-home'] = 'home/FeedHomePages/showHomePage_home';
$route[FEED_PREFIX . '/home/(:any)'] = 'home/FeedHomePages/showProfileByUserSlug/$1';
$route[FEED_PREFIX . '/home-home/(:any)'] = 'home/FeedHomePages/showProfileByUserSlug_home/$1';
$route[FEED_PREFIX . '/submit-feed-home'] = 'home/FeedHomePages/submit_feed';
$route[FEED_PREFIX . '/submit-feed-home-home'] = 'home/FeedHomePages/submit_feed_home';
$route[FEED_PREFIX . '/submit-comment-home'] = 'home/FeedHomePages/submit_comment';
$route[FEED_PREFIX . '/submit-reply-home'] = 'home/FeedHomePages/submit_reply';
$route[FEED_PREFIX . '/like-feed-hot-home'] = 'home/FeedHomePages/like_feed_hot';
$route[FEED_PREFIX . '/like-feed-cool-home'] = 'home/FeedHomePages/like_feed_cool';
$route[FEED_PREFIX . '/share-feed-home'] = 'home/FeedHomePages/share_feed';
$route[FEED_PREFIX . '/load-feed-home'] = 'home/FeedHomePages/load_feed';
$route[FEED_PREFIX . '/load-comment-home'] = 'home/FeedHomePages/load_comment';
$route[FEED_PREFIX . '/load-reply-home'] = 'home/FeedHomePages/load_reply';
$route[FEED_PREFIX . '/upgrade-home'] = 'home/FeedHomePages/test';

$route[AJAX_PREFIX . '/payment'] = 'stripe_demo/index';
$route[AJAX_PREFIX . '/payment/submit'] = 'stripe_demo/stripeConnectUser';



//Config Router Front End
// //Payment
// $route[PAYMENT_PREFIX . '/'] = 'payment';
// $route[PAYMENT_PREFIX . '/success'] = 'payment/payment_success';
// $route[PAYMENT_PREFIX . '/failure'] = 'payment/payment_failure';
// $route[PAYMENT_PREFIX . '/cancel'] = 'payment/payment_cancel';
// $route[PAYMENT_PREFIX . '/pending'] = 'payment/payment_pending';
// $route[PAYMENT_PREFIX . '/confirm'] = 'payment/payment_confirm';

// $route[PAYMENT_PREFIX . '/getorder'] = 'payment/getOrderDetail';

// //Config AWS
$route[AMAZON_PREFIX] = 'aws_service';
$route[AMAZON_PREFIX.'/test_parse'] = 'aws_service/test_parse';
$route[AMAZON_PREFIX.'/test_bounce'] = 'aws_service/test_send_email_bounce';
$route[AMAZON_PREFIX.'/test_compaint'] = 'aws_service/test_send_email_complaint';

$route[AMAZON_PREFIX.'/get_bounce'] = 'aws_service/amazon_message_listener';
$route[AMAZON_PREFIX.'/get_complaint'] = 'aws_service/amazon_message_listener';

$route[AMAZON_PREFIX.'/verify_email'] = 'aws_service/verify_email_sandbox/';
$route[AMAZON_PREFIX.'/test_send_mail'] = 'aws_service/test_send_mail/';
$route[AMAZON_PREFIX.'/test_sqs'] = 'aws_service/test_sqs/';

$route[AMAZON_PREFIX.'/unsubscribe'] = 'admincp_campaigns/admincp_checkmail_unsubscribe/';
// //Config url ajax request
// $route[AJAX_PREFIX] = 'test';
// // page 
$route['^(en|cn)/(:any)'] = 'home/router/$1/$2';
// // page / category / 
$route['^(en|cn)/(:any)/(:any)'] = 'home/router/$1/$2/$3';
// // page / category / item
$route['^(en|cn)/(:any)/(:any)/(:any)'] = 'home/router/$1/$2/$3/$4';
$route['events/(:num)/(:num)/(:any)'] = 'home/event_details/$1/$2/$3';
//Config Router Admincp
$route[ADMINCP] = "admincp";
$route[ADMINCP.'/menu'] = "admincp/menu";
$route[ADMINCP.'/login'] = "admincp/login";
$route[ADMINCP.'/logout'] = "admincp/logout";
$route[ADMINCP.'/permission'] = "admincp/permission";
$route[ADMINCP.'/saveLog'] = "admincp/saveLog";
$route[ADMINCP.'/update_profile'] = "admincp/update_profile";
$route[ADMINCP.'/setting'] = "admincp/setting";
$route[ADMINCP.'/getSetting'] = "admincp/getSetting";
$route[ADMINCP.'/theme'] = "admincp_theme/admincp_index";
$route[ADMINCP.'/(:any)/(:any)/(:any)/(:any)'] = "$1/admincp_$2/$3/$4";
$route[ADMINCP.'/(:any)/(:any)/(:any)'] = "$1/admincp_$2/$3";
$route[ADMINCP.'/(:any)/(:any)'] = "$1/admincp_$2";
$route[ADMINCP.'/(:any)'] = "$1/admincp_index";

$route[AJAX_PREFIX . '/test_fb'] = 'test/fb';
// $route[AJAX_PREFIX . '/testt'] = 'test/fb_event_parse';
// $route[AJAX_PREFIX . '/testt'] = 'test/fb_login';
$route[AJAX_PREFIX . '/testt'] = 'home/login_remember_check';
$route[AJAX_PREFIX . '/test_krypto'] = 'test/krypto_capture';


$route[AJAX_PREFIX . '/admincp/keep_session'] = 'admincp/keep_session';