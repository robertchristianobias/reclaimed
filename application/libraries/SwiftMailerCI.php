<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "swiftmailer/lib/swift_init.php";
require_once "swiftmailer/lib/swift_required.php";

class SwiftMailerCI {
	private $config;
	private $transport;
	private $mailer;
	private $message;
	private $sender;
	function __construct($config = array()){
		$this->sender = array();
		$this->config = array(
			'host'         => NULL,
			'port'         => NULL,
			'username'     => NULL,
			'password'     => NULL,
			'encrypted'    => NULL,
			'sender_email' => NULL,
			'sender_name'  => NULL
			);
		$this->setConfig($config);
	}

	public function setConfig($config) {
		if (isset($config ['host'])) {
			$this->config ['host'] = $config ['host'];
		}
		if (isset($config ['port'])) {
			$this->config ['port'] = $config ['port'];
		}
		if (isset($config ['username'])) {
			$this->config ['username'] = $config ['username'];
		}
		if (isset($config ['password'])) {
			$this->config ['password'] = $config ['password'];
		}
		if (isset($config ['password'])) {
			$this->config ['password'] = $config ['password'];
		}
		if (isset($config ['encrypted'])) {
			$this->config ['encrypted'] = $config ['encrypted'];
		}
		if (isset($config ['sender_email'])) {
			$this->config ['sender_email'] = $config ['sender_email'];
			if (isset($config ['sender_name'])) {
				$this->config ['sender_name'] = $config ['sender_name'];
			}
			$this->sender = array(
					$this->config ['sender_email'] => $this->config ['sender_name']
				);
		}
		$host = $this->config ['host'];
		$port = $this->config ['port'];
		$username = $this->config ['username'];
		$password = $this->config ['password'];
		$encrypted = $this->config ['encrypted'];
		
		$this->transport = new Swift_SmtpTransport($host, $port, $encrypted);
		$this->transport->setUsername($username);
		$this->transport->setPassword($password);
		
		$this->mailer = new Swift_Mailer($this->transport);
	}

	public function setConfigField($host, $port, $username, $password, $encrypted = NULL) {
		$this->transport = new Swift_SmtpTransport($host, $port, $encrypted);
		$this->transport->setUsername($username);
		$this->transport->setPassword($password);

		$this->mailer = new Swift_Mailer($this->transport);
	}

	public function getConfig() {
		return $this->config;
	}

	public function setSender($sender_email, $sender_name = NULL) {
		$this->sender = array($sender_email => $sender_name);
	}

	public function getSender() {
		return $this->sender;
	}


	public function sendEmail($subject, $body, $recepient_emails, $sender_email = NULL, $sender_name = NULL) {
		$recepient_emails = (is_array($recepient_emails)) ? $recepient_emails : (array)$recepient_emails;
		$sender = $this->sender;
		// print_r($this->mailer); exit;
		//for custom sender
		if ( ! empty($sender_email)) {
			$sender = array($sender_email => $sender_name);
		}
		$message = (new Swift_Message($subject))
          ->setFrom($sender)
          ->setTo($recepient_emails)
          ->setBody($body);
          // print_r($body); exit;
        $result = $this->mailer->send($message);
        return $result;
	}
}