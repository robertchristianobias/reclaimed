﻿var divBienDong_DivMapContainer;
var imgBienDong_ImageRoot = "http://khachsan.chudu24.com/img/map/biendong";
var makerbds = new Array();
var MakerBDs =
	[
	[16.64934950378666, 112.74169921875, imgBienDong_ImageRoot + '/map-hoangsa.png'], //Hoang Sa
	[10.714586690981509, 115.81787109375, imgBienDong_ImageRoot + '/map-truongsa.png'],
	[15.728813770533964, 114.31549072265625, imgBienDong_ImageRoot + '/map-biendong.png']
];
var bdmaps = {
	3 : {
		x6y3 : 1
	},
	4 : {
		x13y7 : 1,
		x12y7 : 1
	},
	5 : {
		x25y14 : 1,
		x26y14 : 1,
		x26y15 : 1
	},
	6 : {
		x52y29 : 1,
		x51y28 : 1,
		x51y29 : 1,
		x52y28 : 1,
		x52y30 : 1
	},
	7 : {
		x104y57 : 1,
		x104y58 : 1,
		x105y60 : 1
	},
	8 : {
		x208y115 : 1,
		x208y116 : 1,
		x209y116 : 1,
		x210y120 : 1
	}
};
function setEventValidMap(map) {
	google.maps.event.addListener(map, "dragend", function () {
		PerformValidMap(map);
	});
	google.maps.event.addListener(map, "zoomend", function () {
		PerformValidMap(map);
	});
	google.maps.event.addListener(map, "tilesloaded", function () {
		PerformValidMap(map);
	});
	google.maps.event.addListener(map, "load", function () {
		PerformValidMap(map);
	});
}
function AutoPerformValidMap(map) {
	PerformValidMap(map)
	setTimeout(function () {
		AutoPerformValidMap(map)
	}, 1000);
}
function PerformValidMap(map) {
	if (map.getZoom() < 9) {
		var regex = /&x=[0-9]+&y=[0-9]+&z=[0-9]+&/g;
		var arrItems;
		if ($('img', $("#" + divBienDong_DivMapContainer)[0]).length > 0)
			arrItems = $('img', $("#" + divBienDong_DivMapContainer)[0]);
		else
			arrItems = $('#' + divBienDong_DivMapContainer + ' img');
		arrItems.each(function () {
			var src = this.src;
			var points = src.match(regex);
			if (points && points.length > 0) {
				var info = GetPoints(points[0]);
				if (bdmaps[info.z] && bdmaps[info.z]['x' + info.x + 'y' + info.y]) {
					if (bdmaps[info.z]['x' + info.x + 'y' + info.y] < 0) {
						this.src = imgBienDong_ImageRoot + '/z0x0y0.png';
					} else if (bdmaps[info.z]['x' + info.x + 'y' + info.y] == 1) {
						this.src = imgBienDong_ImageRoot + '/z' + info.z + 'x' + info.x + 'y' + info.y + '.png';
					} else if (bdmaps[info.z]['x' + info.x + 'y' + info.y] == 2) {
						this.src = imgBienDong_ImageRoot + '/z1x1y1.png';
					}
				}
			}
		});
		ShowMakerBD(map, false);
	} else {
		ShowMakerBD(map, true);
	}
}
function ShowMakerBD(map, isshow) {
	for (var i = 0; i < makerbds.length; i++) {
		if (isshow) {
			makerbds[i].setMap(map);
		} else {
			makerbds[i].setMap(null);
		}
	}
}
function GetPoints(text) {
	var x = -1,
	y = -1,
	z = -1;
	var str = text.split('&');
	for (var i = 0; i < str.length; i++) {
		if (str[i].length > 0) {
			if (str[i].indexOf('x=') >= 0)
				x = parseInt(str[i].split('=')[1]);
			else if (str[i].indexOf('y=') >= 0)
				y = parseInt(str[i].split('=')[1]);
			else if (str[i].indexOf('z=') >= 0)
				z = parseInt(str[i].split('=')[1]);
		}
	}
	if (x < 0 || y < 0 || z < 0)
		return null;
	return {
		x : x,
		y : y,
		z : z
	};
}
function BienDongInit(map, id) {
	divBienDong_DivMapContainer = id;
	setEventValidMap(map);
	for (var i = 0; i < MakerBDs.length; i++) {
		var imgBd = new google.maps.MarkerImage(
				MakerBDs[i][2],
				new google.maps.Size(164, 74), // size
				new google.maps.Point(5, 5), // origin
				new google.maps.Point(26, 30) // anchor
			);
		var latLng = new google.maps.LatLng(MakerBDs[i][0], MakerBDs[i][1]);
		var makerbd = new google.maps.Marker(latLng, {
				icon : imgBd
			});
		makerbd.setMap(map);
		makerbd.setMap(null);
		makerbds.push(makerbd);
	}
	AutoPerformValidMap(map);
}
