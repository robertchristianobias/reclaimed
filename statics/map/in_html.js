var is_marker_clicked = false;
function HideMarker(){
	is_marker_clicked = false;
	$("#divMapInfoWindow").hide();
	return false;
}
function iframe_goto_url(url){
	if (window!=window.top) { /* I'm in a frame! */ 
		if(parent.postMessage){
			pr('postMessage');
			parent.postMessage(url,root);
		}
	} else {
		location.href = url;
	}
	return false;
}

var stop_loading_marker = false;
var count_interval = 400;
var marker_count_segment_start = 0;
var marker_count_segment_end = marker_count_segment_start + count_interval;
var marker_total_count = 0;
var timeout_interval = 100;
var time1 = null;
var time2 = null;
var first_load = true;

var current_map_type = '';
$(function () {
	// map - start
	var map;
	var overlay;
	var mapLoaded = false;
	var markers = [];
	var ZoomLevel = 14; // 8; // 13;
	var infowindow = new google.maps.InfoWindow({
			content : "content..."
		});

	var imageGreen = new google.maps.MarkerImage(
			'http://maps.google.com/mapfiles/ms/micons/green-dot.png',
			new google.maps.Size(32, 32), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var imageShadow = new google.maps.MarkerImage(
			'http://maps.google.com/mapfiles/ms/micons/msmarker.shadow.png',
			new google.maps.Size(59, 32), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var image_icon_red = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var image_icon_blue = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50_blue.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var image_icon_gray = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50_gray.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var image_icon_green = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50_green.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	var image_icon_yellow = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50_yellow.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);	

	var image_icon_bluelo = new google.maps.MarkerImage(
			root+'static/admin/map/img/marker/50x50_bluelo.png',
			new google.maps.Size(50, 50), // size
			new google.maps.Point(0, 0), // origin
			new google.maps.Point(16, 32) // anchor
		);
	
	function initializeMap(Lat, Lng, ZoomLevel) {
		//DisplayMessage("ZoomLevel: " + ZoomLevel);
		var mapDiv = document.getElementById('map-canvas');
		map = new google.maps.Map(mapDiv, {
				center : new google.maps.LatLng(Lat, Lng),				
				minZoom : 11,
				maxZoom : 19,
				zoom : 15,
				mapTypeId : google.maps.MapTypeId.ROADMAP,
				scaleControl : true,
				scaleControlOptions : {
					position : google.maps.ControlPosition.BOTTOM_LEFT
				}
			});
			map.setzoom

		google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
			// pr("google.maps.event.addListenerOnce(map, 'tilesloaded'");
			setTimeout(function(){
				// pr('tilesloaded - GetArrayLatLng');
				GetArrayLatLng(); // IMPORTANT
				//ShowPoints();
			},timeout_interval);
		});
		
		overlay = new google.maps.OverlayView();
		overlay.draw = function () {};
		overlay.setMap(map);

		BienDongInit(map, "map-canvas");
	}
	
	/*BEGIN: MOD*/
	function load_district_list(){
		var option = $("option:selected", $("select.txtCities"));
		var CityId = option.val();
		if(CityId){
			$.post(root+'admincp/store_map/load_district_list',{'province_id':CityId, 'allow_select_all_districts':allow_select_all_districts},function(response){
				// pr(response);
				$('#txtDistrict').html(response);
				
				
				if(first_load){
					first_load = false;
				} else {
					// Change map + Show stores
					// pr('load_district_list - GetArrayLatLng');
					GetArrayLatLng();
				}
			});
		}
	}
	
	$('#view_store_map').on('click', function(){
		show_loading();
		GetArrayLatLng(); // Call GetArrayLatLng later
	});
	
	/*
	$('.txtCities').change(function(){
		show_loading();
		// $("#store_type").val('');
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		},100);
	});
	*/
	$('.txtYearmonth').change(function(){
		//show_loading();
		var year_month = $('.txtYearmonth option:selected').html().split('/');
		year = year_month[1];
		month = year_month[0];
		$.post(root+'admincp/store_map/get_day_list_by_year_month',{year:year,month:month},function(res){
			$('#txtDay').html(res);
			/*
			setTimeout(function(){
				GetArrayLatLng(); // Call GetArrayLatLng later
			},200);
			*/
		});

	});
	/*
	$('.txtDay').change(function(){
		setTimeout(function(){
				GetArrayLatLng(); // Call GetArrayLatLng later
			},200);
	});
	
	$('#branch_select').change(function(){
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		}, 200);
	});
	
	$('#rsm_select').change(function(){
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		}, 200);
	});
	
	$('#asm_select').change(function(){
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		}, 200);
	});
	
	$('#npp_select').change(function(){
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		}, 200);
	});
	
	$('#store_type').change(function(){
		setTimeout(function(){
			GetArrayLatLng(); // Call GetArrayLatLng later
		}, 200);
	});
	*/
	
	/*
	$('.store_type').change(function(){
		setTimeout(function(){
				GetArrayLatLng(); // Call GetArrayLatLng later
			},200);
	});
	*/
	
	// $('.txtDistrict').change(function(){
		// setTimeout(function(){
			// pr('txtDistrict timeout');
			// GetArrayLatLng();
		// },100);
	// });
	/*END: MOD*/

	var ajaxCall = null;
	// Change map + Show stores
	function GetArrayLatLng() {
	
		// Benchmark: Wrong time
		// time1 = new Date();
		
		show_loading();
		
		$("span.spnLoading").show();
		if (ajaxCall != null)
			ajaxCall.abort();
		var selected_province_jele = $("option:selected", $("select.txtCities"));
		var CityId = selected_province_jele.val();
		CityId = parseInt(CityId);
		// var selected_district_jele = $("option:selected", $("select.txtDistrict"));
		// var district_id = selected_district_jele.val();
		// district_id = parseInt(district_id);
		var district_id = 0;
		
		var selected_yearmonth_jele = $("option:selected", $("select.txtYearmonth"));
		var yearmonth = selected_yearmonth_jele.html().split('/');
		var year = yearmonth[1];
		var month = yearmonth[0];
		var day = parseInt($("select.txtDay option:selected").val());
		
		//var store_type = $("#store_type").val();
		
		var Latitude = '';
		var Longitude = '';
		if(district_id==0){
			// Get coordinate of province
			Latitude = parseFloat(selected_province_jele.attr("Latitude"));
			Longitude = parseFloat(selected_province_jele.attr("Longitude"));
			// pr('Latitude='+Latitude);
			// pr('Longitude='+Longitude);
		}
		
		if(Latitude > 0 && Longitude > 0){
			var location = new google.maps.LatLng(Latitude, Longitude);
			map_list_store(CityId,district_id,year,month,day,location/*,store_type*/);
		} else {
			
			var geocoder = new google.maps.Geocoder();
			var province_name = selected_province_jele.text();
			var address = province_name;
			if(district_id){
				var district_name = selected_district_jele.text();
				address += ', '+district_name;
			}
			address += ', Vietnam';
			pr('address='+address);
			geocoder.geocode({
				'address': address
			}, function(results, status) {pr('status='+status);
				if (status == google.maps.GeocoderStatus.OK) {
					var location = results[0].geometry.location;
					map_list_store(CityId,district_id,year,month,day,location/*,store_type*/);
				}
			});
		}
	}
	if(typeof(GetArrayLatLng_func) != 'undefined'){
		GetArrayLatLng_func = GetArrayLatLng;
	}
	
	function map_list_store(CityId, district_id, year,month,day, location/*, store_type*/){
		if (CityId != '') {
			$.cookie('KhachSanTheoBanDo - CityId', CityId); // save cookie
			map.setCenter(location); // CHANGE MAP!
			mapLoaded = true;
			//current_map_type = typeof(map_type) != 'undefined' ? map_type : '';
			current_map_type = $('#map_type').val();
			branch_id = $('#branch_select').val();
			rsm_id = $('#rsm_select').val();
			asm_id = $('#asm_select').val();
			npp_id = $('#npp_select').val();
			store_type_id = $('#store_type').val();
			
			setTimeout(function(){
				DeleteAllMarkers(); // Slow performance
				marker_total_count = 0;
				ajaxCall = $.ajax({
						type : "POST",
						url : root+"admincp/store_map/map_list_store_test",
						data : {
							'CityId': CityId, 
							'district_id' : district_id, 
							'year': year, 
							'month': month,
							'day' : day,
							'map_type' : current_map_type, 
							'branch_id' : branch_id,
							'rsm_id' : rsm_id,
							'asm_id' : asm_id,
							'npp_id' : npp_id,
							'store_type_id' : store_type_id	
							//'store_type': store_type
						},
						// contentType : "application/json; charset=utf-8",
						dataType : "json",
						success : function (msg) {
							$("div.divMapData").html($(msg.d));
							
							// pr('ajaxCall - finish - ShowPoints');
							marker_total_count = $("div.divHotel", $("div.divMapData")).length;
							// pr('marker_total_count = '+marker_total_count);
							ShowPoints(); // IMPORTANT
							$("span.spnLoading").hide();
						},
						error : function (e) {
							DisplayMessage('Error (GetArrayLatLng) !!!');
							hide_loading();
						}
				});
			},100);
		}
	}

	function ShowPoints() {//pr('In ShowPoints()');
		if(stop_loading_marker){
			pr('ShowPoints(): stop_loading_marker');
			hide_loading();
			return;
		}
		
		var count = 0;
		$("div.divHotel", $("div.divMapData")).each(function () {
			if(count >= marker_count_segment_start && count <= marker_count_segment_end){
				// var HotelName = $(this).attr("HotelName");
				var store_code = $(this).attr("store_code");
				var Name2 = $(this).attr("Name2");
				var FullNameID = store_code;
				if(Name2){
					FullNameID += ' - '+Name2;
				}
				var rating = $(this).attr("import_status");
				rating = rating.toString();
				//alert(rating);
				//throw new Error("Something went badly wrong!");
				
				var Latitude = $(this).attr("Latitude");
				var Latitude_int_val = parseInt(Latitude);
				var Longitude = $(this).attr("Longitude");
				var Longitude_int_val = parseInt(Longitude);
				var Address = $(this).attr("Address");
				var HotelIdInt = $(this).attr("HotelIdInt");
				
				
				if(Latitude_int_val && Longitude_int_val){
					// BEGIN: DEBUG
					// if(marker_total_count == 1){
						// pr('Show marker of store with id = '+HotelIdInt+' - address = '+Address+ ' - Longitude = '+Longitude+ ' - Latitude = '+Latitude);
					// }
					// END: DEBUG
					
					var imageIcon = imageGreen;
					//alert(rating);
					switch(rating){
						case 'xam':
							imageIcon = image_icon_gray;
							break;
						case 'do':
							imageIcon = image_icon_bluelo; // Thay mau do thanh mau xanh lo
							break; 
						case 'vang':
							imageIcon = image_icon_yellow;
							break;
						case 'xanh':
							imageIcon = image_icon_blue;
							break;
						default: alert(rating);
							break;
					}
					//alert(imageIcon);
					var latLng = new google.maps.LatLng(Latitude, Longitude);
					var marker = new google.maps.Marker({
							position : latLng,
							map : map,
							// icon : imageGreen,
							icon : imageIcon
							// icon : imageSVGIcon,
							// shadow : imageShadow
							// optimized: true
						});
					var detail_url = root + 'admincp/store/update/'+HotelIdInt;
					var content = 
					'<div style="font-size:10pt;width:100%;float:left;" id="marker'+HotelIdInt+'">' +
						'<div style="float:left;width:100%;margin-bottom:5px;font-weight:bold;font-size:1.2em;">' + FullNameID+ '</div>' +
						'<div style="float:left;width:100%;color:#888;font-size:8pt;">Địa chỉ: ' + Address + '</div>' +
						'<div style="float:left;width:100%;display:none;" class="marker_action"><a onclick="return iframe_goto_url(\''+detail_url+'\')" href="'+detail_url+'" style="border:1px solid;padding-left:5px;padding-right:5px;">VIEW</a></div>' +
					'</div>' +
					'<div class="clear"></div>' + 
					'<a href="javascript:void(0)" onclick="return HideMarker()" style="display:none;position:absolute;top:1px;right:1px;border:1px solid;padding-left:1px;padding-right:1px;" class="marker_close">X</a>';
				
					marker['content'] = content;
					marker['hotelIdInt'] = HotelIdInt;
					markers.push(marker);
					
					google.maps.event.addListener(marker, 'click', function () {
						// ShowContentPopup_InfoWindow(this);
						var hotelIdInt = this['hotelIdInt'];
						// pr('hotelIdInt='+hotelIdInt);
						is_marker_clicked = true;
						var current_marker_jele = $('#marker'+hotelIdInt);
						if(current_marker_jele.length){
							current_marker_jele.parent().find('a.marker_close').show();
							current_marker_jele.find('div.marker_action').show();
						}
					});
					google.maps.event.addListener(marker, 'mouseover', function () {
						if(!is_marker_clicked){
							ShowContentPopup(this);
							// pr('mouseover');
						}
					});
					google.maps.event.addListener(marker, 'mouseout', function () {
						if(!is_marker_clicked){
							$("#divMapInfoWindow").hide(); // COMMENT TO KEEP POPUP INFO OF STORE
							// pr('mouseout');
						}
					});
				}
			}
			count++;
		});
		
		if(marker_count_segment_end < marker_total_count){
			marker_count_segment_start = marker_count_segment_end+1;
			marker_count_segment_end = marker_count_segment_start + count_interval-1;
			// pr('marker_count_segment_start = '+marker_count_segment_start);
			// pr('marker_count_segment_end = '+marker_count_segment_end);
			
			setTimeout(function(){
				ShowPoints();
			},timeout_interval);
		} else {
			$('.loading_content .loading_text').html('<b>Đã load xong '+marker_total_count+' cửa hàng!</b>');
			
			// Reset
			stop_loading_marker = false;
			marker_count_segment_start = 0;
			marker_count_segment_end = marker_count_segment_start + count_interval;
			marker_total_count = 0;
			time1 = null;
			time2 = null;
				
			setTimeout(function(){
				hide_loading();								
				// pr('ShowPoints finished!');
			},1000);
			
			// Benchmark: Wrong time
			// time2 = new Date();
			// var difference = time2-time1;
			// pr(difference);
		}
	}
	
	function ShowContentPopup(_this) {
		// alert('aaa');
		var strContent = _this['content'];
		var divMapInfoWindow = $("#divMapInfoWindow");
		divMapInfoWindow.html(strContent);
		var projection = overlay.getProjection();
		var divPixel = projection.fromLatLngToContainerPixel(_this.getPosition());
		var x = 0;
		if (divPixel.x + divMapInfoWindow.width() < $("#map-canvas").width()) {
			x = divPixel.x + $("#map-canvas").position().left + 25;
		} else {
			x = $("#map-canvas").position().left + divPixel.x - divMapInfoWindow.width() - 28;
		}
		var y = 0;
		if (divPixel.y + divMapInfoWindow.height() < $("#map-canvas").height()) {
			y = divPixel.y + $("#map-canvas").position().top- divMapInfoWindow.height();
		} else {
			y = $("#map-canvas").position().top + divPixel.y - divMapInfoWindow.height() - 58;
		}
		divMapInfoWindow.css('left', x);
		divMapInfoWindow.css('top', y);
		divMapInfoWindow.width(300); // Set width of Window
		divMapInfoWindow.show();
	}

	function ShowContentPopup_InfoWindow(_this) {
		var hotelIdInt = _this['hotelIdInt'];
		var _url = "/M/Reservations_v2/WebServices/MapFullScreen_GetHotel.aspx";
		var _data = "HotelIdInt=" + escape(hotelIdInt);
		$.ajax({
			url : _url,
			data : _data,
			cache : false,
			success : function (html) {
				if (html != "") {
					infowindow.setContent(html);
					infowindow.open(map, _this);
					$('a.thumbPreview').lightBox({
						fixedNavigation : true,
						imageLoading : '/img/lightbox/lightbox-ico-loading.gif',
						imageBtnPrev : '/img/lightbox/lightbox-btn-prev.gif',
						imageBtnNext : '/img/lightbox/lightbox-btn-next.gif',
						imageBtnClose : '/img/lightbox/lightbox-btn-close.gif',
						imageBlank : '/img/lightbox/lightbox-blank.gif'
					});
					$("a.lnkToggleHotelThumbnails").click(function (e) {
						e.preventDefault();
						$("div.divHotelThumbnails").slideToggle();
					});
				}
			},
			error : function () {
				DisplayMessage("Error (ShowContentPopup_InfoWindow)");
			}
		});
	}

	$("a.lnkHotelName").click(function (e) {
		e.preventDefault();
	});

	/*$("select.txtCities").change(function () {
		GetArrayLatLng();
	});*/

	function DeleteAllMarkers() {
		if (markers) {
			// remove points
			for (i in markers) {
				markers[i].setMap(null);
			}
			// remove items in array
			for (j in markers) {
				markers.splice(j, 1);
			}
		}
	}

	$("div.divStarRating").slider({
		range : true,
		min : 0,
		max : 7,
		values : [0, 7],
		slide : function (event, ui) {
			$("span.spnMinStarRating").text(ui.values[0]);
			$("span.spnMaxStarRating").text(ui.values[1]);
			GetArrayLatLng();
		}
	});
	$("span.spnMinStarRating").text($("div.divStarRating").slider("option", "min"));
	$("span.spnMaxStarRating").text($("div.divStarRating").slider("option", "max"));

	var MinRate_Default = (($("a.lnkCurrency").text() == "VND") ? 200000 : 10);
	var MaxRate_Default = (($("a.lnkCurrency").text() == "VND") ? 10000000 : 500);
	var Step_Default = (($("a.lnkCurrency").text() == "VND") ? 200000 : 10);
	$("div.divRate").slider({
		range : true,
		min : MinRate_Default,
		max : MaxRate_Default,
		step : Step_Default,
		values : [MinRate_Default, MaxRate_Default],
		slide : function (event, ui) {
			var objMinRate = $("<span>" + ui.values[0] + "</span>");
			objMinRate.format({
				format : "#,###"
			});
			$("span.spnMinRate").text(objMinRate.text());
			var objMaxRate = $("<span>" + ui.values[1] + "</span>");
			objMaxRate.format({
				format : "#,###"
			});
			$("span.spnMaxRate").text(objMaxRate.text());
			GetArrayLatLng();
		}
	});

	var objMinRate = $("<span>" + $("div.divRate").slider("option", "min") + "</span>");
	objMinRate.format({
		format : "#,###"
	});
	$("span.spnMinRate").text(objMinRate.text());

	var objMaxRate = $("<span>" + $("div.divRate").slider("option", "max") + "</span>");
	objMaxRate.format({
		format : "#,###"
	});
	$("span.spnMaxRate").text(objMaxRate.text());

	// load preset map

	$.QueryString = (function (a) {
		if (a == "")
			return {};
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p = a[i].split('=');
			if (p.length != 2)
				continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'));

	var CityId = $.QueryString["CityId"];
	if ($.trim(CityId).length == 0) {
		// if ($.cookie('KhachSanTheoBanDo - CityId')) {
			// CityId = $.cookie('KhachSanTheoBanDo - CityId');
		// } else {
			CityId = 8 /* ho-chi-minh */
		// }
	}
	var option = $("select.txtCities option[value=" + CityId + "]");
	option.attr("selected", "selected");

	initializeMap(option.attr("Latitude"), option.attr("Longitude"), ZoomLevel);
	// GetArrayLatLng();
	
	// MOD
	// load_district_list();

	// map - end

	function DisplayMessage(msg) {
		if (console)
			console.log(msg);
	}

	$("a.lnkCurrency").click(function (e) {
		e.preventDefault();
		$("span.spnLoading").show();
		var Currency = ($(this).text() == "VND") ? "USD" : "VND";
		var _url = "/WebServices/WebServices.asmx/SetCurrency";
		var _data = "{'Currency': '" + Currency + "'}";
		$.ajax({
			url : _url,
			data : _data,
			type : "POST",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function (html) {
				window.location.href = window.location.href;
			},
			error : function () {
				alert("Error when changing currency...");
			}
		});
	});

});