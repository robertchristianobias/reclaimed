########################################
##  GSBannerAd Script 0.9.2 to 0.9.4  ##
########################################

A version 0.9.3 was planned for the release, however was delayed to include more changes/improvements 
which is now version 0.9.4.

================================================================================
!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANT IMPORTANT IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!
================================================================================
DO NOT DELETE THE includes/CONNECT.PHP FILE!!! Do not DELETE includes/connect.php file!!!

IF YOU HAVE MADE SOME CHANGES TO THE SCRIPT FILES,
MAKE SURE YOU MAKE SAME CHANGES TO NEW FILES.
================================================================================
!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANT IMPORTANT IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!
================================================================================

From version 0.8.2 and higher, the script is PHP5 Compatible.
From version 0.9.4 and higher, the script require PHP version 5.1.2 or newer.


Here what you need to do:
********************************************************************************************
1) Copy/Write-over new files to your existing banner script folder *online*:
** YOU DO NOT NEED TO COPY THE \install FOLDER **

FROM: GSBannerAd_0.9.4\SCRIPT-UPLOAD\...
TO: *ADFOLDER*/...


- languages/ *ALL FILES*
- pages/ *ALL FILES*
- includes/ *ALL FILES*
- images/ *ALL FILES*
- admin/ *ALL FILES & FOLDERS*
- / *ALL FILES* (ad-ad3, redirect-redirect3, cron, index)


********************************************************************************************
********************************************************************************************
2) Now you need to copy the upgrader file to your existing banner script folder *online*:

FROM: GSBannerAd_0.9.4\UPGRADE\0.9.2 to 0.9.4\upgrade.php
TO: *ADFOLDER*/
- http://www.yourdomain.com/*ADFOLDER*/upgrade.php

The upgrade.php file will update your current MySQL Database v0.9.2 to v0.9.4.


********************************************************************************************