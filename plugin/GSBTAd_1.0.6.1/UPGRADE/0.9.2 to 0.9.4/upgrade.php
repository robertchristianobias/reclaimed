<?php
// ########################################
// ########################################
// ####### UPGRADE v0.9.2 to v0.9.4 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerAd v0.9.4</title>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.gif" alt="logo" border="0">
</div>
<?php
if ($_GET['step'] == "1") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE banners
  ADD `payment` varchar(25) NOT NULL default '' AFTER `camid`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>banners</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Step 2\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "2") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig
  ADD `cad` varchar(3) NOT NULL AFTER `jpy`,
  ADD `aud` varchar(3) NOT NULL AFTER `cad`,
  ADD `chf` varchar(3) NOT NULL AFTER `aud`,
  ADD `btc` varchar(3) NOT NULL AFTER `chf`,
  ADD `accept_ppc` varchar(5) NOT NULL default '' AFTER `accept_ppy`, 
  ADD `accept_ppd` varchar(5) NOT NULL default '' AFTER `accept_ppc`, 
  ADD `accept_ppf` varchar(5) NOT NULL default '' AFTER `accept_ppd`, 
  ADD `accept_bca` varchar(5) NOT NULL default '' AFTER `accept_bcy`, 
  ADD `accept_bcd` varchar(5) NOT NULL default '' AFTER `accept_bca`, 
  ADD `accept_bcf` varchar(5) NOT NULL default '' AFTER `accept_bcd`, 
  ADD `accept_bcc` varchar(5) NOT NULL default '' AFTER `accept_bcf`,
  ADD `accept_mg` varchar(5) NOT NULL default '' AFTER `accept_bcc`, 
  ADD `accept_mgc` varchar(5) NOT NULL default '' AFTER `accept_mg`, 
  ADD `accept_apc` varchar(5) NOT NULL default '' AFTER `accept_app`, 
  ADD `accept_apd` varchar(5) NOT NULL default '' AFTER `accept_apc`, 
  ADD `accept_apf` varchar(5) NOT NULL default '' AFTER `accept_apd`, 
  ADD `accept_stpc` varchar(5) NOT NULL default '' AFTER `accept_stpy`, 
  ADD `accept_stpd` varchar(5) NOT NULL default '' AFTER `accept_stpc`, 
  ADD `accept_stpf` varchar(5) NOT NULL default '' AFTER `accept_stpd`, 
  ADD `accept_cgd` varchar(5) NOT NULL default '' AFTER `accept_cgy`, 
  ADD `accept_cga` varchar(5) NOT NULL default '' AFTER `accept_cgd`, 
  ADD `accept_cgf` varchar(5) NOT NULL default '' AFTER `accept_cga`, 
  ADD `accept_mbc` varchar(5) NOT NULL default '' AFTER `accept_mby`, 
  ADD `accept_mbd` varchar(5) NOT NULL default '' AFTER `accept_mbc`, 
  ADD `accept_mbf` varchar(5) NOT NULL default '' AFTER `accept_mbd`, 
  ADD `accept_tcoc` varchar(5) NOT NULL default '' AFTER `accept_tcoy`, 
  ADD `accept_tcod` varchar(5) NOT NULL default '' AFTER `accept_tcoc`, 
  ADD `accept_tcof` varchar(5) NOT NULL default '' AFTER `accept_tcod`, 
  ADD `accept_gmc` varchar(5) NOT NULL default '' AFTER `accept_gmy`, 
  ADD `accept_gmd` varchar(5) NOT NULL default '' AFTER `accept_gmc`, 
  ADD `accept_gmf` varchar(5) NOT NULL default '' AFTER `accept_gmd`, 
  ADD `mg` varchar(255) NOT NULL default '' AFTER `bc`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Step 3\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "3") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns
  DROP `next_banner`,
  ADD `avail` varchar(255) NOT NULL default '' AFTER `unlimited`,
  ADD `maxnumad` varchar(255) NOT NULL default '' AFTER `avail`,
  ADD `addesc_type` varchar(15) NOT NULL default '' AFTER `maxnumad`,
  ADD `addspace` varchar(3) NOT NULL AFTER `addesc_type`,
  ADD `teaserbgc` varchar(15) NOT NULL AFTER `addspace`,
  ADD `teaserbgi` varchar(255) NOT NULL AFTER `teaserbgc`,
  ADD `opacity` varchar(15) NOT NULL AFTER `teaserbgi`,
  ADD `teaserfc` varchar(15) NOT NULL AFTER `opacity`,
  ADD `teaserbt` varchar(35) NOT NULL AFTER `teaserfc`,
  ADD `teaserbtlr` varchar(25) NOT NULL AFTER `teaserbt`,
  ADD `teaserbtrr` varchar(25) NOT NULL AFTER `teaserbtlr`,
  ADD `adtextcss` varchar(255) NOT NULL AFTER `teaserbtrr`,
  ADD `pricecad` decimal(14,2) NOT NULL default '0.00' AFTER `pricejpy`,
  ADD `priceaud` decimal(14,2) NOT NULL default '0.00' AFTER `pricecad`,
  ADD `pricechf` decimal(14,2) NOT NULL default '0.00' AFTER `priceaud`,
  ADD `pricebtc` decimal(14,2) NOT NULL default '0.00' AFTER `pricechf`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=4'\" value=\"Step 4\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "4") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 4/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE `users` modify `pass` VARCHAR(45);") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>users</i>' - Done. Change the length of password field.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=5'\" value=\"Step 5\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "5") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 5/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

// Generate temp password
 function RandomPassword($PwdLength=8, $PwdType='standard')
    {
    $Ranges='';
     if('test'==$PwdType)         return 'test';
    elseif('standard'==$PwdType) $Ranges='65-78,80-90,97-107,109-122,50-57';
    elseif('alphanum'==$PwdType) $Ranges='65-90,97-122,48-57';
    elseif('any'==$PwdType)      $Ranges='40-59,61-91,93-126';
    if($Ranges<>'')
        {
        $Range=explode(',',$Ranges);
        $NumRanges=count($Range);
        mt_srand(time()); //not required after PHP v4.2.0
        $p='';
        for ($i = 1; $i <= $PwdLength; $i++)
            {
            $r=mt_rand(0,$NumRanges-1);
            list($min,$max)=explode('-',$Range[$r]);
            $p.=chr(mt_rand($min,$max));
            }
        return $p;			
        }
    }
	$password = RandomPassword();
	$hashedpwd = hash('ripemd160', $password);

  $sql = mysql_query("ALTER TABLE `admin` modify `pass` VARCHAR(45);") or die(mysql_error());
  $sql = mysql_query("UPDATE `admin` SET pass = '$hashedpwd';") or die(mysql_error());

if ($sql) { echo "Done. Your temporary Admin Password is <b><font size=3>$password</font></b>. Please copy it. You can change the password after you log-in.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Final\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "final") 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to v0.9.4
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else 
{
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerAd Upgrade Page - v0.9.2 to v0.9.4! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>banners</i>' - add a new field</LI>
<LI>Alter TABLE '<i>payfig</i>' -  add new payment fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' - add new fields</LI>
<LI>Alter TABLE '<i>admin</i>' - update & add a temporary password (PBA now do not store plain text password)</LI>
</OL>
The upgrade will remove, add, and update/alter your database.
<br />
<br />
<br />
<br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br />
<br />
</body>
</html>