<?php
// ########################################
// ########################################
// ######## UPGRADE v0.9 to v0.9.1 ########
// #########    upgrade9.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerAd v0.9.1</title>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.gif" alt="logo" border="0">
</div>
<?php
if ($_GET['step'] == "1") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/2</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE config
  DROP COLUMN `next_banner`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>config</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade91.php?step=2'\" value=\"Step 2\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "2") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/2</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns
   ADD `next_banner` int(255) NOT NULL; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade91.php?step=final'\" value=\"Final\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "final") 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to v0.9.1
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else {
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/2</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerAd Upgrade Page - v0.9 to v0.9.1! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>config</i>' and delete 'next_banner' </LI>
<LI>Alter TABLE '<i>campaigns</i>' and add new field 'next_banner'</LI>
</OL>
<center>
</center>
<br /><br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade91.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br /><br />
</body>
</html>