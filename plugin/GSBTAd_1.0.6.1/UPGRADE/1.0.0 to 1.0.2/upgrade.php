<?php
// ########################################
// ########################################
// ####### UPGRADE v1.0.0 to v1.0.2 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerTextlinkAd v1.0.2</title>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_RIGHT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.png" alt="logo" border="0">
</div>

<?php
if ($_GET['step'] == "1") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig
  DROP `accept_bc`,
  DROP `accept_bcr`,
  DROP `accept_bcp`,
  DROP `accept_bcy`,
  DROP `accept_bca`,
  DROP `accept_bcd`,
  DROP `accept_bcf`,
  DROP `accept_bcc`,
  DROP `accept_bci`,
  DROP `accept_bcs`,
  DROP `accept_bcn`,
  DROP `accept_bcm`,
  DROP `accept_bch`,
  DROP `accept_mg`,
  DROP `accept_mgc`,
  DROP `accept_eg`,
  DROP `accept_egr`,
  DROP `accept_egp`,
  DROP `accept_egy`,
  DROP `accept_ep`,
  DROP `accept_epr`,
  DROP `accept_rp`,
  DROP `accept_rpr`,
  DROP `bc`,
  DROP `mg`,
  DROP `eg`,
  DROP `ep`,
  DROP `rp`,
  ADD `nzd` varchar(3) NOT NULL default '' AFTER `hkd`,
  ADD `pln` varchar(3) NOT NULL default '' AFTER `nzd`,
  ADD `dkk` varchar(3) NOT NULL default '' AFTER `pln`,
  ADD `czk` varchar(3) NOT NULL default '' AFTER `dkk`,
  ADD `accept_ppnz` varchar(5) NOT NULL default '' AFTER `accept_pph`,
  ADD `accept_ppl` varchar(5) NOT NULL default '' AFTER `accept_ppnz`,
  ADD `accept_ppk` varchar(5) NOT NULL default '' AFTER `accept_ppl`,
  ADD `accept_ppz` varchar(5) NOT NULL default '' AFTER `accept_ppk`,
  ADD `accept_apz` varchar(5) NOT NULL default '' AFTER `accept_aph`,
  ADD `accept_apl` varchar(5) NOT NULL default '' AFTER `accept_apz`,
  ADD `accept_apk` varchar(5) NOT NULL default '' AFTER `accept_apl`,
  ADD `accept_apczk` varchar(5) NOT NULL default '' AFTER `accept_apk`,
  ADD `accept_mbz` varchar(5) NOT NULL default '' AFTER `accept_mbh`,
  ADD `accept_tcoz` varchar(5) NOT NULL default '' AFTER `accept_tcoh`,
  ADD `accept_tcok` varchar(5) NOT NULL default '' AFTER `accept_tcoz`,
  ADD `accept_gmz` varchar(5) NOT NULL default '' AFTER `accept_gmh`,
  ADD `accept_paxd` varchar(5) NOT NULL default '' AFTER `accept_gdpr`,
  ADD `accept_paxr` varchar(5) NOT NULL default '' AFTER `accept_paxd`,
  ADD `accept_paxc` varchar(5) NOT NULL default '' AFTER `accept_paxr`, 
  ADD `accept_paymd` varchar(5) NOT NULL default '' AFTER `accept_paxc`,
  ADD `accept_paymr` varchar(5) NOT NULL default '' AFTER `accept_paymd`,
  ADD `accept_paymp` varchar(5) NOT NULL default '' AFTER `accept_paymr`,
  ADD `accept_payma` varchar(5) NOT NULL default '' AFTER `accept_paymp`,
  ADD `accept_paymz` varchar(5) NOT NULL default '' AFTER `accept_payma`,
  ADD `accept_pop` varchar(5) NOT NULL default '' AFTER `accept_paymz`,
  ADD `accept_wb` varchar(5) NOT NULL default '' AFTER `accept_pop`,
  ADD `accept_cb` varchar(5) NOT NULL default '' AFTER `accept_wb`,
  ADD `accept_cbr` varchar(5) NOT NULL default '' AFTER `accept_cb`,
  ADD `accept_cbp` varchar(5) NOT NULL default '' AFTER `accept_cbr`,
  ADD `accept_cbc` varchar(5) NOT NULL default '' AFTER `accept_cbp`,
  ADD `accept_wmz` varchar(5) NOT NULL default '' AFTER `accept_cbc`,
  ADD `accept_wme` varchar(5) NOT NULL default '' AFTER `accept_wmz`,
  ADD `accept_nc` varchar(5) NOT NULL default '' AFTER `accept_wme`,
  ADD `accept_ops` varchar(5) NOT NULL default '' AFTER `accept_nc`,
  ADD `accept_ops2` varchar(5) NOT NULL default '' AFTER `accept_ops`,
  ADD `accept_ops3` varchar(5) NOT NULL default '' AFTER `accept_ops2`,
  ADD `accept_ops4` varchar(5) NOT NULL default '' AFTER `accept_ops3`,
  ADD `accept_ops5` varchar(5) NOT NULL default '' AFTER `accept_ops4`,
  ADD `accept_ops6` varchar(5) NOT NULL default '' AFTER `accept_ops5`,
  ADD `accept_ops7` varchar(5) NOT NULL default '' AFTER `accept_ops6`,
  ADD `accept_ops8` varchar(5) NOT NULL default '' AFTER `accept_ops7`,
  ADD `accept_ops9` varchar(5) NOT NULL default '' AFTER `accept_ops8`,
  ADD `accept_ops10` varchar(5) NOT NULL default '' AFTER `accept_ops9`,
  ADD `accept_ops11` varchar(5) NOT NULL default '' AFTER `accept_ops10`,
  ADD `accept_ops12` varchar(5) NOT NULL default '' AFTER `accept_ops11`,
  ADD `accept_ops13` varchar(5) NOT NULL default '' AFTER `accept_ops12`,
  ADD `accept_ops14` varchar(5) NOT NULL default '' AFTER `accept_ops13`,
  ADD `accept_ops15` varchar(5) NOT NULL default '' AFTER `accept_ops14`,
  ADD `accept_ops16` varchar(5) NOT NULL default '' AFTER `accept_ops15`,
  ADD `accept_ops17` varchar(5) NOT NULL default '' AFTER `accept_ops16`,
  ADD `lr2` varchar(255) NOT NULL default '' AFTER `lr`,
  ADD `pax` varchar(255) NOT NULL default '' AFTER `gdpstore`,
  ADD `paym` varchar(255) NOT NULL default '' AFTER `pax`,
  ADD `po` varchar(255) NOT NULL default '' AFTER `paym`,
  ADD `wb` varchar(255) NOT NULL default '' AFTER `po`,
  ADD `cb` varchar(255) NOT NULL default '' AFTER `wb`,
  ADD `wm` varchar(255) NOT NULL default '' AFTER `cb`,
  ADD `wm2` varchar(255) NOT NULL default '' AFTER `wm`,
  ADD `nc` varchar(255) NOT NULL default '' AFTER `wm2`,
  ADD `ops` text NOT NULL default '' AFTER `nc`,
  ADD `ops2` varchar(255) NOT NULL default '' AFTER `ops`;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Step 2\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "2") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns
  ADD `pricedate` varchar(255) NOT NULL default '0000-00-00' AFTER `adtextcss`,
  ADD `pricenzd` decimal(14,2) NOT NULL default '0.00' AFTER `pricehkd`,
  ADD `pricepln` decimal(14,2) NOT NULL default '0.00' AFTER `pricenzd`,
  ADD `pricedkk` decimal(14,2) NOT NULL default '0.00' AFTER `pricepln`,
  ADD `priceczk` decimal(14,2) NOT NULL default '0.00' AFTER `pricedkk`;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Step 3\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "3") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE config
  ADD `language` varchar(5) NOT NULL default 'en' AFTER `description`,
  ADD `currency` varchar(5) NOT NULL default 'usd' AFTER `language`,
  ADD `theme` varchar(25) NOT NULL default 'default' AFTER `currency`,
  ADD `gtranslate` varchar(5) NOT NULL default 'no' AFTER `theme`;
 ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>config</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=4'\" value=\"Step 4\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "4") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 4/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("CREATE TABLE `denycountries` (
  `id` int(255) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;") or die(mysql_error());

if ($sql) { echo "Create the TABLE '<i>denycountries</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=5'\" value=\"Step 5\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "5") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 5/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("CREATE TABLE `denyips` (
  `id` int(255) NOT NULL auto_increment,
  `ip` varchar(30) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;") or die(mysql_error());

if ($sql) { echo "Create the TABLE '<i>denyips</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=6'\" value=\"Step 6\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "6") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 6/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE `trackip`
  ADD `act` varchar(3) NOT NULL AFTER `ip`;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>trackip</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Final\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "final") 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to v1.0.2
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else 
{
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerTextlinkAd Upgrade Page - v1.0.0 to v1.0.2! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>payfig</i>' -  add new payment & currency fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' - add new currency fields</LI>
<LI>Alter TABLE '<i>config</i>' - add new fields</LI>
<LI>Create TABLE '<i>denycountries</i>'</LI>
<LI>Create TABLE '<i>denyips</i>'</LI>
<LI>Alter TABLE '<i>trackip</i>' - add a field</LI>
</OL>
The upgrade will remove, add, and update/alter your database.
<br />
<br />
<br />
<br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br />
<br />
</body>
</html>