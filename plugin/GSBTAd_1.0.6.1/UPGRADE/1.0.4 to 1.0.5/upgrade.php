<?php
// ########################################
// ########################################
// ####### UPGRADE v1.0.4 to v1.0.5 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerTextlinkAd v1.0.5</title>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.png" alt="logo" border="0">
</div>
<p>
<!-- Begin TranslateThis Button -->
<div id="translate-this"><a style="width:180px;height:18px;display:block;" class="translate-this-button" href="http://www.translatecompany.com/">Translate Company</a></div>
<script type="text/javascript" src="http://x.translateth.is/translate-this.js"></script>
<script type="text/javascript">
TranslateThis();
</script>
<!-- End TranslateThis Button -->
</p>
<?php
if ($_GET['step'] == '1') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig

  ADD `ltc` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `btc`,
  ADD `ftc` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `ltc`,
  ADD `ppc` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `ftc`,
  ADD `nmc` varchar(3) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `ppc`,

  ADD `accept_ops26` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ops25`,
  ADD `accept_ops27` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ops26`,
  ADD `accept_ops28` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ops27`,
  ADD `accept_ops29` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ops28`,
 
  ADD `accept_btc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_wps`,
  ADD `accept_ltc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_btc`,
  ADD `accept_ftc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ltc`,
  ADD `accept_pppc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ftc`,
  ADD `accept_cpbtc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_ppc`,
  ADD `accept_cpltc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpbtc`,
  ADD `accept_cpftc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpltc`,
  ADD `accept_cpppc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpftc`,
  ADD `accept_cpnmc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpppc`,
  ADD `accept_cpusd` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpnmc`,
  ADD `accept_cpeur` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpusd`,
  ADD `accept_cpgbp` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpeur`,
  ADD `accept_cpcad` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpgbp`,
  ADD `accept_cpaud` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpcad`,
  ADD `accept_nmc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_cpaud`,
  ADD `accept_bmusd` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_nmc`,
  ADD `accept_bmeur` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmusd`,
  ADD `accept_bmgbp` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmeur`,
  ADD `accept_bmjpy` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmgbp`,
  ADD `accept_bmcad` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmjpy`,
  ADD `accept_bmaud` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmcad`,
  ADD `accept_bmchf` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmaud`,
  ADD `accept_bmnok` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmchf`,
  ADD `accept_bmsek` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmnok`,
  ADD `accept_bmnzd` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmsek`,
  ADD `accept_bmpln` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmnzd`,
  ADD `accept_bmdkk` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmpln`,
  ADD `accept_bmsgd` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmdkk`,
  ADD `accept_bmrub` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmsgd`,
  ADD `accept_bmcny` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmrub`,
  ADD `accept_bmthb` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmcny`,
  ADD `accept_bmhkd` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmthb`,
  ADD `accept_bmbtc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmhkd`,
  ADD `accept_bmltc` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `accept_bmbtc`,
 
  ADD `bc` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `wp`,
  ADD `lc` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `bc`,
  ADD `fc` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `lc`,
  ADD `pc` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `fc`,
  ADD `cp` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `pc`,
  ADD `nmc2` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `cp`,
  ADD `bm` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL default '' AFTER `nmc2`
 
;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?
}
if ($_GET['step'] == '2') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns

  ADD `priceltc` decimal(14,8) NOT NULL default '0.00000000' AFTER `pricebtc`,
  ADD `priceftc` decimal(14,8) NOT NULL default '0.00000000' AFTER `priceltc`,
  ADD `priceppc` decimal(14,8) NOT NULL default '0.00000000' AFTER `priceftc`,
  ADD `pricenmc` decimal(14,8) NOT NULL default '0.00000000' AFTER `priceppc`
  
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?
}
if ($_GET['step'] == '3') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE config
  
  ADD `gcurrency` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL AFTER `gtranslate`,
  ADD `maintop` text character set utf8 collate utf8_unicode_ci NOT NULL AFTER `lastcron`,
  ADD `mainbot` text character set utf8 collate utf8_unicode_ci NOT NULL AFTER `maintop`
  
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>config</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=4'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?
}
if ($_GET['step'] == '4') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 4/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER DATABASE $dbuser
 CHARACTER SET utf8
 DEFAULT CHARACTER SET utf8
 COLLATE utf8_unicode_ci
 DEFAULT COLLATE utf8_unicode_ci
 ;
  ") or die(mysql_error());
$new_charset = 'utf8'; // change to the required character set - you're probably changing to utf8 ?
$new_collation = 'utf8_unicode_ci'; // change to the required collation - you're probably changing to utf8_unicode_ci ?
// Loop through all tables changing collation
$result=mysql_query('show tables');
while($tables = mysql_fetch_array($result)) {
$table = $tables[0];
mysql_query("ALTER TABLE $table DEFAULT CHARACTER SET $new_charset COLLATE $new_collation");

// loop through each column changing collation
$columns = mysql_query("SHOW FULL COLUMNS FROM $table where collation is not null");
while($cols = mysql_fetch_array($columns)) {
$column = $cols[0];
$type = $cols[1];
mysql_query("ALTER TABLE $table MODIFY $column $type CHARACTER SET $new_charset COLLATE $new_collation");
}
}
if ($sql) { echo "Update Database, Update TABLES, Update Columns - Done. UFT8 & utf8 unicode.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?php
}
if ($_GET['step'] == 'final') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to version 1.0.5.
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>
<?php
}
else 
{
?>
<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/4</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerTextlinkAd Upgrade Page - v1.0.4 to v1.0.5! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>payfig</i>' -  add/remove fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' -  add new field</LI>
<LI>Alter TABLE '<i>config</i>' -  add new field</LI>
<LI>Alter TABLE '<i>config</i>' -  add new field</LI>
</OL>
The upgrade will remove, add, and update or alter your database.
<br />
<br />
<br />
<br />
<p style="font-weight:bold;font-size:15px;color:red;">!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</p>
<OL>
<LI> <strong>You should download a copy or backup of your current MySQL database!</strong></LI> 
</OL> 
<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?php
}
?>
<br />
<br />
</body>
</html>