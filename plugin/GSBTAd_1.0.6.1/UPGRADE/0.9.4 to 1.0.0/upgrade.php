<?php
// ########################################
// ########################################
// ####### UPGRADE v0.9.4 to v1.0.0 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerAd v1.0.0</title>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.png" alt="logo" border="0">
</div>
<?php
if ($_GET['step'] == "1") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE banners
  ADD `adurl` text NOT NULL default '' AFTER `forward`,
  ADD `adtitle` text NOT NULL default '' AFTER `adurl`,
  ADD `adtyid` varchar(15) NOT NULL default '' AFTER `camid`,
  ADD `coupon` varchar(12) NOT NULL default '' AFTER `payment`,
  RENAME TO ads; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>banners</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Step 2\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "2") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig
  ADD `inr` varchar(3) NOT NULL default '' AFTER `btc`,
  ADD `sek` varchar(3) NOT NULL default '' AFTER `inr`,
  ADD `nok` varchar(3) NOT NULL default '' AFTER `sek`,
  ADD `mxn` varchar(3) NOT NULL default '' AFTER `nok`,
  ADD `hkd` varchar(3) NOT NULL default '' AFTER `mxn`,
  
  ADD `accept_pps` varchar(5) NOT NULL default '' AFTER `accept_ppf`,
  ADD `accept_ppn` varchar(5) NOT NULL default '' AFTER `accept_pps`,
  ADD `accept_ppm` varchar(5) NOT NULL default '' AFTER `accept_ppn`,
  ADD `accept_pph` varchar(5) NOT NULL default '' AFTER `accept_ppm`,
  
  ADD `accept_bci` varchar(5) NOT NULL default '' AFTER `accept_bcc`,
  ADD `accept_bcs` varchar(5) NOT NULL default '' AFTER `accept_bci`,
  ADD `accept_bcn` varchar(5) NOT NULL default '' AFTER `accept_bcs`,
  ADD `accept_bcm` varchar(5) NOT NULL default '' AFTER `accept_bcn`,
  ADD `accept_bch` varchar(5) NOT NULL default '' AFTER `accept_bcm`,
  
  ADD `accept_api` varchar(5) NOT NULL default '' AFTER `accept_apf`,
  ADD `accept_aps` varchar(5) NOT NULL default '' AFTER `accept_api`,
  ADD `accept_apn` varchar(5) NOT NULL default '' AFTER `accept_aps`,
  ADD `accept_aph` varchar(5) NOT NULL default '' AFTER `accept_apn`,
  
  ADD `accept_cgi` varchar(5) NOT NULL default '' AFTER `accept_cgf`,
  ADD `accept_cgs` varchar(5) NOT NULL default '' AFTER `accept_cgi`,
  ADD `accept_cgm` varchar(5) NOT NULL default '' AFTER `accept_cgs`,
  ADD `accept_cgh` varchar(5) NOT NULL default '' AFTER `accept_cgm`,
  
  ADD `accept_mbi` varchar(5) NOT NULL default '' AFTER `accept_mbf`,
  ADD `accept_mbs` varchar(5) NOT NULL default '' AFTER `accept_mbi`,
  ADD `accept_mbn` varchar(5) NOT NULL default '' AFTER `accept_mbs`,
  ADD `accept_mbh` varchar(5) NOT NULL default '' AFTER `accept_mbn`,
  
  ADD `accept_tcoi` varchar(5) NOT NULL default '' AFTER `accept_tcof`,
  ADD `accept_tcos` varchar(5) NOT NULL default '' AFTER `accept_tcoi`,
  ADD `accept_tcon` varchar(5) NOT NULL default '' AFTER `accept_tcos`,
  ADD `accept_tcom` varchar(5) NOT NULL default '' AFTER `accept_tcon`,
  ADD `accept_tcoh` varchar(5) NOT NULL default '' AFTER `accept_tcom`,
  
  ADD `accept_gmh` varchar(5) NOT NULL default '' AFTER `accept_gmf`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Step 3\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "3") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns
  ADD `adtyid` varchar(15) NOT NULL AFTER `title`,
  ADD `default_textlink` varchar(255) NOT NULL AFTER `default_banner`,
  ADD `tlstyle` text NOT NULL AFTER `height`,
  ADD `adsbyable` varchar(3) NOT NULL AFTER `tlstyle`,
  ADD `adsby` text NOT NULL AFTER `adsbyable`,
  ADD `special` varchar(1) NOT NULL AFTER `adsby`,
  DROP `addspace`,
  ADD `priceinr` decimal(14,2) NOT NULL default '0.00' AFTER `pricebtc`,
  ADD `pricesek` decimal(14,2) NOT NULL default '0.00' AFTER `priceinr`,
  ADD `pricenok` decimal(14,2) NOT NULL default '0.00' AFTER `pricesek`,
  ADD `pricemxn` decimal(14,2) NOT NULL default '0.00' AFTER `pricenok`,
  ADD `pricehkd` decimal(14,2) NOT NULL default '0.00' AFTER `pricemxn`; 
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=4'\" value=\"Step 4\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "4") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 4/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("CREATE TABLE `coupons` (
  `id` int(255) NOT NULL auto_increment,
  `cname` varchar(255) NOT NULL default '',
  `ccode` varchar(10) NOT NULL default '',
  `cnum` varchar(3) NOT NULL default '',
  `cdate` varchar(25) NOT NULL default '',
  `status` varchar(3) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;") or die(mysql_error());

if ($sql) { echo "Create the TABLE '<i>coupons</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=5'\" value=\"Step 5\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "5") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 5/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("CREATE TABLE `history` (
  `id` int(255) NOT NULL auto_increment,
  `email` varchar(255) NOT NULL default '',
  `status` varchar(255) NOT NULL default '',
  `credits` decimal(65,0) NOT NULL,
  `dateadded` date NOT NULL,
  `camid` varchar(255) NOT NULL default '',
  `payment` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;") or die(mysql_error());

if ($sql) { echo "Create the TABLE '<i>history</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=6'\" value=\"Step 6\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "6") 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 5/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);
if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("CREATE TABLE `trackip` (
  `id` int(255) NOT NULL auto_increment,
  `adid` varchar(255) NOT NULL default '',
  `date` varchar(255) NOT NULL default '',
  `ip` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;") or die(mysql_error());

if ($sql) { echo "Create the TABLE '<i>trackip</i>' - Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Final\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == "final") 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to v1.0.0
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else 
{
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/6</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerTextlinkAd Upgrade Page - v0.9.4 to v1.0.0! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>banners</i>' - change name and add few fields</LI>
<LI>Alter TABLE '<i>payfig</i>' -  add new payment fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' - add new fields</LI>
<LI>Add TABLE '<i>coupons</i>' - add fields</LI>
<LI>Add TABLE '<i>history</i>' - add fields</LI>
<LI>Add TABLE '<i>trackip</i>' - add fields</LI>
</OL>
The upgrade will remove, add, and update/alter your database.
<br />
<br />
<br />
<br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br />
<br />
</body>
</html>