<?php
// ########################################
// ########################################
// ####### UPGRADE v1.0.2 to v1.0.3 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerTextlinkAd v1.0.3</title>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.png" alt="logo" border="0">
</div>

<?php
if ($_GET['step'] == '1') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig
  DROP `po`,
  DROP `gdp`,
  DROP `gdpstore`,
  DROP `accept_gdp`,
  DROP `accept_gdpr`,
  DROP `accept_pop`,

  ADD `sgd` varchar(3) NOT NULL default '' AFTER `czk`,
  ADD `twd` varchar(3) NOT NULL default '' AFTER `sgd`,
  ADD `thb` varchar(3) NOT NULL default '' AFTER `twd`,
  ADD `ils` varchar(3) NOT NULL default '' AFTER `thb`,
  ADD `rub` varchar(3) NOT NULL default '' AFTER `ils`,
  ADD `krw` varchar(3) NOT NULL default '' AFTER `rub`,
  ADD `cny` varchar(3) NOT NULL default '' AFTER `krw`,
  
  ADD `accept_tcosd` varchar(5) NOT NULL default '' AFTER `accept_tcok`,
  ADD `accept_tcois` varchar(5) NOT NULL default '' AFTER `accept_tcosd`,
  ADD `accept_tcorr` varchar(5) NOT NULL default '' AFTER `accept_tcois`,
  
  ADD `accept_apsd` varchar(5) NOT NULL default '' AFTER `accept_apczk`,

  ADD `accept_cgyr` varchar(5) NOT NULL default '' AFTER `accept_cgh`,
  
  ADD `accept_gmub` varchar(5) NOT NULL default '' AFTER `accept_gmz`,
  ADD `accept_gmny` varchar(5) NOT NULL default '' AFTER `accept_gmub`,
  
  ADD `accept_mbsd` varchar(5) NOT NULL default '' AFTER `accept_mbz`,
  ADD `accept_mbis` varchar(5) NOT NULL default '' AFTER `accept_mbsd`,
  ADD `accept_mbt` varchar(5) NOT NULL default '' AFTER `accept_mbis`,
  ADD `accept_mbtb` varchar(5) NOT NULL default '' AFTER `accept_mbt`,
  ADD `accept_mbw` varchar(5) NOT NULL default '' AFTER `accept_mbtb`,
  
  ADD `accept_ppg` varchar(5) NOT NULL default '' AFTER `accept_ppz`,
  ADD `accept_ppt` varchar(5) NOT NULL default '' AFTER `accept_ppg`,
  ADD `accept_ppb` varchar(5) NOT NULL default '' AFTER `accept_ppt`,
  ADD `accept_ppi` varchar(5) NOT NULL default '' AFTER `accept_ppb`,
  
  ADD `accept_ops18` varchar(5) NOT NULL default '' AFTER `accept_ops17`,
  ADD `accept_ops19` varchar(5) NOT NULL default '' AFTER `accept_ops18`,
  ADD `accept_ops20` varchar(5) NOT NULL default '' AFTER `accept_ops19`,
  ADD `accept_ops21` varchar(5) NOT NULL default '' AFTER `accept_ops20`,
  ADD `accept_ops22` varchar(5) NOT NULL default '' AFTER `accept_ops21`,
  ADD `accept_ops23` varchar(5) NOT NULL default '' AFTER `accept_ops22`,
  ADD `accept_ops24` varchar(5) NOT NULL default '' AFTER `accept_ops23`,
  
  ADD `accept_dw1` varchar(5) NOT NULL default '' AFTER `accept_ops24`,
  
  ADD `accept_egod` varchar(5) NOT NULL default '' AFTER `accept_dw1`,
  ADD `accept_egor` varchar(5) NOT NULL default '' AFTER `accept_egod`,
  
  ADD `accept_okp1` varchar(5) NOT NULL default '' AFTER `accept_dw1`,
  ADD `accept_okp2` varchar(5) NOT NULL default '' AFTER `accept_okp1`,
  ADD `accept_okp3` varchar(5) NOT NULL default '' AFTER `accept_okp2`,
  ADD `accept_okp4` varchar(5) NOT NULL default '' AFTER `accept_okp3`,
  ADD `accept_okp5` varchar(5) NOT NULL default '' AFTER `accept_okp4`,
  ADD `accept_okp6` varchar(5) NOT NULL default '' AFTER `accept_okp5`,
  ADD `accept_okp7` varchar(5) NOT NULL default '' AFTER `accept_okp6`,
  ADD `accept_okp11` varchar(5) NOT NULL default '' AFTER `accept_okp7`,
  ADD `accept_okp12` varchar(5) NOT NULL default '' AFTER `accept_okp11`,
  ADD `accept_okp14` varchar(5) NOT NULL default '' AFTER `accept_okp12`,
  ADD `accept_okp15` varchar(5) NOT NULL default '' AFTER `accept_okp14`,
  ADD `accept_okp16` varchar(5) NOT NULL default '' AFTER `accept_okp15`,
  ADD `accept_okp17` varchar(5) NOT NULL default '' AFTER `accept_okp16`,
  ADD `accept_okp18` varchar(5) NOT NULL default '' AFTER `accept_okp17`,
  ADD `accept_okp19` varchar(5) NOT NULL default '' AFTER `accept_okp18`,
  ADD `accept_okp21` varchar(5) NOT NULL default '' AFTER `accept_okp19`,
  ADD `accept_okp22` varchar(5) NOT NULL default '' AFTER `accept_okp21`,
  ADD `accept_okp24` varchar(5) NOT NULL default '' AFTER `accept_okp22`,
  
  
  ADD `dwok` varchar(255) NOT NULL default '' AFTER `ops2`,
  ADD `dwos` varchar(255) NOT NULL default '' AFTER `dwok`,
  ADD `ego` varchar(255) NOT NULL default '' AFTER `dwos`,
  ADD `ego2` varchar(255) NOT NULL default '' AFTER `ego`,
  ADD `okp` varchar(255) NOT NULL default '' AFTER `ego2`
  
  
;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?
}
if ($_GET['step'] == '2') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns

  ADD `pricesgd` decimal(14,2) NOT NULL default '0.00' AFTER `priceczk`,
  ADD `pricetwd` decimal(14,2) NOT NULL default '0.00' AFTER `pricesgd`,
  ADD `pricethb` decimal(14,2) NOT NULL default '0.00' AFTER `pricetwd`,
  ADD `priceils` decimal(14,2) NOT NULL default '0.00' AFTER `pricethb`,
  ADD `pricerub` decimal(14,2) NOT NULL default '0.00' AFTER `priceils`,
  ADD `pricekrw` decimal(14,2) NOT NULL default '0.00' AFTER `pricerub`,
  ADD `pricecny` decimal(14,2) NOT NULL default '0.00' AFTER `pricekrw`
  
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?
}
if ($_GET['step'] == '3') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE config

  ADD `scriptby` varchar(5) NOT NULL default 'yes' AFTER `gtranslate`
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>config</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=4'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?
}
if ($_GET['step'] == '4') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 4/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE ads

  ADD `tnum` varchar(15) NOT NULL AFTER `id`
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>ads</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=5'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?
}
if ($_GET['step'] == '5') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 5/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE history

  ADD `tnum` varchar(15) NOT NULL AFTER `id`
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>history</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == 'final') 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to version 1.0.3.
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else 
{
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/5</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerTextlinkAd Upgrade Page - v1.0.2 to v1.0.3! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>payfig</i>' -  add/remove fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' -  add new fields</LI>
<LI>Alter TABLE '<i>config</i>' -  add new field</LI>
<LI>Alter TABLE '<i>ads</i>' -  add new field</LI>
<LI>Alter TABLE '<i>history</i>' -  add new field</LI>
</OL>
The upgrade will remove, add, and update/alter your database.
<br />
<br />
<br />
<br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br />
<br />
</body>
</html>