<?php
// ########################################
// ########################################
// ####### UPGRADE v1.0.3 to v1.0.4 #######
// ##########    upgrade.php    ###########
// ##PUT THIS FILE IN YOUR MAIN AD FOLDER##
// ########################################
// ########################################
include("includes/connect.php");
?>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<title>Upgrade PayBannerTextlinkAd v1.0.4</title>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<br />
<div align="center">
<img src="admin/img/logo.png" alt="logo" border="0">
</div>

<p>
<!-- Begin TranslateThis Button -->
<div id="translate-this"><a style="width:180px;height:18px;display:block;" class="translate-this-button" href="http://www.translatecompany.com/">Translate Company</a></div>
<script type="text/javascript" src="http://x.translateth.is/translate-this.js"></script>
<script type="text/javascript">
TranslateThis();
</script>
<!-- End TranslateThis Button -->
</p>

<?php
if ($_GET['step'] == '1') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 1/3</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE payfig
  DROP `lr`,
  DROP `lr2`,
  DROP `accept_lr`,
  DROP `accept_lrr`,
  DROP `accept_wb`,
  DROP `wb`,

  ADD `php` varchar(3) NOT NULL default '' AFTER `cny`,

  ADD `accept_ppu` varchar(5) NOT NULL default '' AFTER `accept_ppi`,
  ADD `accept_ppo` varchar(5) NOT NULL default '' AFTER `accept_ppu`,
  
  ADD `accept_ops25` varchar(5) NOT NULL default '' AFTER `accept_ops24`,
  
  ADD `accept_okp25` varchar(5) NOT NULL default '' AFTER `accept_okp24`,
  
  ADD `accept_se` varchar(5) NOT NULL default '' AFTER `accept_egor`,
  
  ADD `accept_wp` varchar(5) NOT NULL default '' AFTER `accept_se`,
  ADD `accept_wpr` varchar(5) NOT NULL default '' AFTER `accept_wp`,
  ADD `accept_wpp` varchar(5) NOT NULL default '' AFTER `accept_wpr`,
  ADD `accept_wpc` varchar(5) NOT NULL default '' AFTER `accept_wpp`,
  ADD `accept_wpa` varchar(5) NOT NULL default '' AFTER `accept_wpc`,
  ADD `accept_wpf` varchar(5) NOT NULL default '' AFTER `accept_wpa`,
  ADD `accept_wpn` varchar(5) NOT NULL default '' AFTER `accept_wpf`,
  ADD `accept_wph` varchar(5) NOT NULL default '' AFTER `accept_wpn`,
  ADD `accept_wpz` varchar(5) NOT NULL default '' AFTER `accept_wph`,
  ADD `accept_wpd` varchar(5) NOT NULL default '' AFTER `accept_wpz`,
  ADD `accept_wps` varchar(5) NOT NULL default '' AFTER `accept_wpd`,

  ADD `se` varchar(255) NOT NULL default '' AFTER `okp`,
  ADD `sestore` varchar(255) NOT NULL default '' AFTER `se`,
  ADD `wp` varchar(255) NOT NULL default '' AFTER `sestore`
  
;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>payfig</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=2'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?
}
if ($_GET['step'] == '2') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 2/3</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE campaigns

  ADD `pricephp` decimal(14,2) NOT NULL default '0.00' AFTER `pricecny`
  
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>campaigns</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=3'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>
<?
}
if ($_GET['step'] == '3') 
{
?>
<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 3/3</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
<?php
$connect = mysql_connect($dbhost,$dbuser,$dbpass);

if (!$connect) {
    die('Could not connect: ' . mysql_error());
}
$db = mysql_select_db($dbname);

  $sql = mysql_query("ALTER TABLE coupons

  ADD `climit` varchar(5) NOT NULL AFTER `ccode`
  ;
  ") or die(mysql_error());

if ($sql) { echo "Alter the TABLE '<i>coupons</i>'- Done.
<br /><br /><br />
<p align=\"center\"> 
<input type=\"button\" onClick=\"location.href='upgrade.php?step=final'\" value=\"Next\"> 
</p>"; }
mysql_close;
exit;
?>

</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
if ($_GET['step'] == 'final') 
{
?>

<DIV ALIGN="center">
<br /><br /> 
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | COMPLETED</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 13px; font-family: Verdana;"> 
You're done. Your database should now be updated to version 1.0.4.
<br /><br />If finished, you MUST <strong>delete/remove</strong> this file.<br />
<br />
<br /><br /></TD>
</TR>
</TABLE>
</FIELDSET>
</DIV>


<?php
}
else 
{
?>

<DIV ALIGN="center">
<br /><br />
<FIELDSET STYLE="width: 750px;"> <LEGEND STYLE="font-size: 12px; font-family: Verdana;">[<B>Upgrade | Step 0/3</B>]</LEGEND> 
<br /> 
<TABLE width="99%">
<TR><TD STYLE="font-size: 12px; font-family: Verdana;"> 
Welcome to the PayBannerTextlinkAd Upgrade Page - v1.0.3.0-1 to v1.0.4! <br /><br /><br /> This file will connect to your database and ... 
<OL>
<LI>Alter TABLE '<i>payfig</i>' -  add/remove fields</LI>
<LI>Alter TABLE '<i>campaigns</i>' -  add new field</LI>
<LI>Alter TABLE '<i>coupons</i>' -  add new field</LI>
</OL>
The upgrade will remove, add, and update/alter your database.
<br />
<br />
<br />
<br />
<font color="red"><strong>!! IMPORTANT !! VERY URGENT FOR YOU TO READ BELOW BEFORE YOU START...</strong></font>
<OL>
<LI> <strong>You should make/download a copy/backup of your current *MySQL* database!</strong></LI> 
</OL> 

<br /><br /><br />
<p align="center"> 
<input type="button" onClick="location.href='upgrade.php?step=1'" value="STEP 1"> 
</p>
</TD></TR>
</TABLE>
</FIELDSET>
</DIV>

<?php
}
?>
<br />
<br />
</body>
</html>