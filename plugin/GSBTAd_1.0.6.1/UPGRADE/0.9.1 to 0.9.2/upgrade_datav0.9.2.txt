-- phpMyAdmin SQL Dump
-- version 3.3.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 28, 2011 at 10:31 PM
-- Server version: 5.0.91
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `paybannerad`
--

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(255) NOT NULL default '0',
  `allow_reg` varchar(3) NOT NULL default '',
  `show_mstats` varchar(3) NOT NULL default '',
  `admin_email` varchar(255) NOT NULL default '',
  `send_from_email` varchar(255) NOT NULL default '',
  `absolute_path` varchar(255) NOT NULL default '',
  `site_name` varchar(255) NOT NULL default '',
  `site_url` varchar(255) NOT NULL default '',
  `keywords` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `lastcron` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `allow_reg`, `show_mstats`, `admin_email`, `send_from_email`, `absolute_path`, `site_name`, `site_url`, `keywords`, `description`, `lastcron`) VALUES
(1, 'no', 'yes', 'info@groovyscripts.net', 'info@groovyscripts.net', '/home/USER/public_html/adfolder/', 'Sitename', 'http://www.domain.com/adfolder/', 'keywords', 'description', '0000-00-00');


-- --------------------------------------------------------

--
-- Table structure for table `payfig`
--

CREATE TABLE IF NOT EXISTS `payfig` (
  `id` int(255) NOT NULL default '0',
  `usd` varchar(3) NOT NULL,
  `eur` varchar(3) NOT NULL,
  `gbp` varchar(3) NOT NULL,
  `jpy` varchar(3) NOT NULL,
  `accept_pp` varchar(5) NOT NULL default '',
  `accept_ppr` varchar(5) NOT NULL default '',
  `accept_ppp` varchar(5) NOT NULL default '',
  `accept_ppy` varchar(5) NOT NULL default '',
  `accept_an` varchar(5) NOT NULL default '',
  `accept_bc` varchar(5) NOT NULL default '',
  `accept_bcr` varchar(5) NOT NULL default '',
  `accept_bcp` varchar(5) NOT NULL default '',
  `accept_bcy` varchar(5) NOT NULL default '',
  `accept_eg` varchar(5) NOT NULL default '',
  `accept_egr` varchar(5) NOT NULL default '',
  `accept_egp` varchar(5) NOT NULL default '',
  `accept_egy` varchar(5) NOT NULL default '',
  `accept_ap` varchar(5) NOT NULL default '',
  `accept_apr` varchar(5) NOT NULL default '',
  `accept_app` varchar(5) NOT NULL default '',
  `accept_stp` varchar(5) NOT NULL default '',
  `accept_stpr` varchar(5) NOT NULL default '',
  `accept_stpp` varchar(5) NOT NULL default '',
  `accept_stpy` varchar(5) NOT NULL default '',
  `accept_lr` varchar(5) NOT NULL default '',
  `accept_lrr` varchar(5) NOT NULL default '',
  `accept_cg` varchar(5) NOT NULL default '',
  `accept_cgr` varchar(5) NOT NULL default '',
  `accept_cgp` varchar(5) NOT NULL default '',
  `accept_cgy` varchar(5) NOT NULL default '',
  `accept_pu` varchar(5) NOT NULL default '',
  `accept_pur` varchar(5) NOT NULL default '',
  `accept_pm` varchar(5) NOT NULL default '',
  `accept_pmr` varchar(5) NOT NULL default '',
  `accept_mb` varchar(5) NOT NULL default '',
  `accept_mbr` varchar(5) NOT NULL default '',
  `accept_mbp` varchar(5) NOT NULL default '',
  `accept_mby` varchar(5) NOT NULL default '',
  `accept_hdm` varchar(5) NOT NULL default '',
  `accept_tco` varchar(5) NOT NULL default '',
  `accept_tcor` varchar(5) NOT NULL default '',
  `accept_tcop` varchar(5) NOT NULL default '',
  `accept_tcoy` varchar(5) NOT NULL default '',
  `accept_gco` varchar(5) NOT NULL default '',
  `accept_gcop` varchar(5) NOT NULL default '',
  `accept_ep` varchar(5) NOT NULL default '',
  `accept_epr` varchar(5) NOT NULL default '',
  `accept_gm` varchar(5) NOT NULL default '',
  `accept_gmr` varchar(5) NOT NULL default '',
  `accept_gmp` varchar(5) NOT NULL default '',
  `accept_gmy` varchar(5) NOT NULL default '',
  `accept_egc` varchar(5) NOT NULL default '',
  `accept_egcr` varchar(5) NOT NULL default '',
  `accept_egcp` varchar(5) NOT NULL default '',
  `accept_rp` varchar(5) NOT NULL default '',
  `accept_rpr` varchar(5) NOT NULL default '',
  `accept_gdp` varchar(5) NOT NULL default '',
  `accept_gdpr` varchar(5) NOT NULL default '',
  `pp` varchar(255) NOT NULL default '',
  `an` varchar(255) NOT NULL default '',
  `antk` varchar(255) NOT NULL default '',
  `bc` varchar(255) NOT NULL default '',
  `eg` varchar(255) NOT NULL default '',
  `ap` varchar(255) NOT NULL default '',
  `stp` varchar(255) NOT NULL default '',
  `lr` varchar(255) NOT NULL default '',
  `cg` varchar(255) NOT NULL default '',
  `pu` varchar(255) NOT NULL default '',
  `pm` varchar(255) NOT NULL default '',
  `pm2` varchar(255) NOT NULL default '',
  `mb` varchar(255) NOT NULL default '',
  `hdm` varchar(255) NOT NULL default '',
  `tco` varchar(255) NOT NULL default '',
  `gco` varchar(255) NOT NULL default '',
  `gco2` varchar(255) NOT NULL default '',
  `ep` varchar(255) NOT NULL default '',
  `gm` varchar(255) NOT NULL default '',
  `egc` varchar(255) NOT NULL default '',
  `rp` varchar(255) NOT NULL default '',
  `gdp` varchar(255) NOT NULL default '',
  `egcstore` varchar(255) NOT NULL default '',
  `gdpstore` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payfig`
--

INSERT INTO `payfig` (`id`, `usd`, `eur`, `gbp`, `jpy`, `accept_pp`, `accept_ppr`, `accept_ppp`, `accept_ppy`, `accept_an`, `accept_bc`, `accept_bcr`, `accept_bcp`, `accept_bcy`, `accept_eg`, `accept_egr`, `accept_egp`, `accept_egy`, `accept_ap`, `accept_apr`, `accept_app`, `accept_stp`, `accept_stpr`, `accept_stpp`, `accept_stpy`, `accept_lr`, `accept_lrr`, `accept_cg`, `accept_cgr`, `accept_cgp`, `accept_cgy`, `accept_pu`, `accept_pur`, `accept_pm`, `accept_pmr`, `accept_mb`, `accept_mbr`, `accept_mbp`, `accept_mby`, `accept_hdm`, `accept_tco`, `accept_tcor`, `accept_tcop`, `accept_tcoy`, `accept_gco`, `accept_gcop`, `accept_ep`, `accept_epr`, `accept_gm`, `accept_gmr`, `accept_gmp`, `accept_gmy`, `accept_egc`, `accept_egcr`, `accept_egcp`, `accept_rp`, `accept_rpr`, `accept_gdp`, `accept_gdpr`, `pp`, `an`, `antk`, `bc`, `eg`, `ap`, `stp`, `lr`, `cg`, `pu`, `pm`, `pm2`, `mb`, `hdm`, `tco`, `gco`, `gco2`, `ep`, `gm`, `egc`, `rp`, `gdp`, `egcstore`, `gdpstore`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------
