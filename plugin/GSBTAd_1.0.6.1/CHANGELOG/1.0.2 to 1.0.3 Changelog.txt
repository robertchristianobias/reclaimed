###############################################################
#############GS Banner Textlink Ad Script v1.0.3###############
###############################################################

It is considered as near-major upgrade. There are multiples changes to MySQL Database Tables, and Script files.

What removed/added in this version:

// ADDED:
# Payment: Payza (AlertPay), EgoPay, Dwolla, OkPay
# Currencies: SGD, TWD, THB, ILS, RUB, KRW, CNY
# Themes: New theme design
# Language: Korean
# Admin Area: Redesigned pages
# And many improvements & modifications on both Main and Admin Areas.

// REMOVED:
# AlertPay, PayOffline, GlobalDigitalPay


********************************************************************************************
1) Copy/Write-over new files to your existing banner script folder *online*:
** YOU DO NOT NEED TO COPY THE \install FOLDER **

FROM: GSBTAd_1.0.3\SCRIPT-UPLOAD\...
TO: *ADFOLDER*/...


- languages/ *ALL FILES*
- themes/ *ALL FILES*
- pages/ *ALL FILES*
- includes/ *ALL FILES*
- images/ *ALL FILES*
- admin/ *ALL FILES & FOLDERS*
- / *ALL FILES* 


********************************************************************************************