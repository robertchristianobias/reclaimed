<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: Spanish / Español
Date    : Aug 21 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
The translation was done with Google Translate.

PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Do you know Spanish Language? If so, then please help correcting any translating errors below. Email us with the updated files.
----------------------------------------------------

La traducción la realizó con Google Translate.

POR FAVOR ¡Ciudado Editting ABAJO PORQUE
ALGUNAS LINEAS contienen códigos HTML.

Por favor, utilice los códigos de caracteres especiales para que estén bien se muestra en el navegador.

¿Sabe usted español? Si es así, entonces por favor ayudar a corregir cualquier error traducir a continuación. Envíenos un correo electrónico con los archivos actualizados.
===================================================
*/
define('LANG_TRANSLATION', 'no garantiza la exactitud de la traducción y la eficiencia.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'es');
define('LANG_CONT_HTML', 'es');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'casa');
define('LANG_MENU_ADVERTISE', 'anunciar');
define('LANG_MENU_LOGIN', 'acceder');
define('LANG_MENU_LOGGEDIN', 'Se Conectó');
define('LANG_MENU_CONTACT', 'contacto');

define('LANG_FMENU_HOME', 'casa');
define('LANG_FMENU_ADVERTISE', 'anunciarse con nosotros');
define('LANG_FMENU_LOGIN', 'anunciante entrada');
define('LANG_FMENU_CONTACT', 'contáctenos');
define('LANG_FMENU_RIGHTS', 'Reservados todos los derechos');
define('LANG_FMENU_SCRIPTBY', 'Guión de');

define('LANG_MAIN_WELCOME', 'Para adquirir publicidad, por favor, para registrar una cuenta o login si ya tiene uno.');
define('LANG_MAIN_ADOPTIONS', 'Opciones de publicidad');
define('LANG_MAIN_CTITLE', 'Campaña');
define('LANG_MAIN_TITLE', 'Título');
define('LANG_MAIN_SIZE', 'Tamaño');
define('LANG_MAIN_TYPE', 'Tipo');
define('LANG_MAIN_ADTYPEALLOWED', 'Tipo de anuncios permitidos');
define('LANG_MAIN_ROTATE', 'Rotar');
define('LANG_MAIN_CREDITS', 'Créditoss');
define('LANG_MAIN_UNLCREDITS', 'Ilimitado Créditoss');
define('LANG_MAIN_TEXT', 'Texto');
define('LANG_MAIN_YES', 'Sí');
define('LANG_MAIN_NO', 'No');
define('LANG_MAIN_AVAIL', 'Disponibilidad');
define('LANG_MAIN_VIEWPRICES', 'Ver Precios');
define('LANG_MAIN_ON', 'En');
define('LANG_MAIN_OFF', 'De');
define('LANG_MAIN_UNLIMITED', 'Ilimitado');
define('LANG_MAIN_ETC', 'Los precios indicados arriba es el costo total. Si un precio que no se muestra de la divisa que desee, entonces no está disponible para su compra.');
define('LANG_MAIN_ADTYPES', 'CPI - Coste por impresión, CPC - Coste por clic a través de, CPD - Costo por día');
define('LANG_MAIN_PAYMENTMETHOD', 'Forma de pago');
define('LANG_MAIN_PRODUCT', 'Producto');
define('LANG_MAIN_BACKBUT', 'Espalda');
define('LANG_MAIN_NEXTBUT', 'Próximo');
define('LANG_MAIN_PREVBUT', 'Prevista');
define('LANG_MAIN_AREA', 'Principal del Área de');
define('LANG_MAIN_LOGOUT', 'Cierre de sesión');
define('LANG_MAIN_CLOSEWINDOW', 'Cerrar esta ventana');
define('LANG_MAIN_DATE', 'Fecha');
define('LANG_MAIN_DATEADDED', 'Fecha Alta');
define('LANG_MAIN_TOTAL', 'Suma');
define('LANG_MAIN_IMPRESSIONS', 'Impresiones');
define('LANG_MAIN_CLICKS', 'Clics');
define('LANG_MAIN_DAYS', 'Días');
define('LANG_MAIN_STATUS', 'Estado');
define('LANG_MAIN_YOURADS', 'Sus anuncios');
define('LANG_MAIN_CREMAINING', 'Resto de créditos');
define('LANG_MAIN_CHFDS', 'Ver aquí para estadísticas detalladas');
define('LANG_MAIN_CREATEAD', 'Crear un nuevo anuncio');
define('LANG_MAIN_CREATEADAPP', 'Añadir un anuncio (todos los anuncios están sujetos al administrador de la aprobación)');
define('LANG_MAIN_ACCEPTCURR', 'Monedas aceptadas');
define('LANG_MAIN_PRICE', 'Precio');
define('LANG_MAIN_SELECTED', 'Ha seleccionado');
define('LANG_MAIN_ADGRAPHICURL', 'Anuncio gráfico URL');
define('LANG_MAIN_ADFORWARDURL', 'Reenviar URL del anuncio');
define('LANG_MAIN_ADTEXT', 'Texto del anuncio');
define('LANG_MAIN_WIDTH', 'Ancho');
define('LANG_MAIN_HEIGHT', 'Altura');
define('LANG_MAIN_USERINFO', 'Información del usuario');
define('LANG_MAIN_ADDETAILS', 'Detalles del anuncio');
define('LANG_MAIN_VERIDETAILS', 'Por favor, compruebe todos los datos son correctos antes de continuar');
define('LANG_MAIN_TESTAD', 'Haga clic en la ficha de arriba para la prueba - que se abrirá en una ventana nueva');
define('LANG_MAIN_NEWUSER', 'Nuevo Anunciante');
define('LANG_MAIN_USERLOGIN', 'Nombre del anunciante');
define('LANG_MAIN_ULOGIN', 'Acceder');
define('LANG_MAIN_FORGETINFO', 'Olvídese de información de acceso?');
define('LANG_MAIN_NAME', 'Nombre');
define('LANG_MAIN_EMAIL', 'Correo electrónico');
define('LANG_MAIN_PASSWORD', 'Contraseña');
define('LANG_MAIN_REGISTER', 'Registro');

define('LANG_MAIN_ADFORWARDURL2', 'Anuncio URL visible');
define('LANG_MAIN_ADFORWARDURL3', '(Mostrar acortar URL: domain.com)');
define('LANG_MAIN_HISTORY', 'Historial de compras');
define('LANG_MAIN_CODE', 'Código');
define('LANG_MAIN_PERCENT', 'Por ciento');
define('LANG_MAIN_NUMBERS', 'Números');
define('LANG_MAIN_SUBMBUT', 'Presentar');
define('LANG_MAIN_ENTERCOUPON', 'Introduzca el código de cupón');
define('LANG_MAIN_DISCOUNT', 'Descuento');
define('LANG_MAIN_FREE', 'Libre');

define('LANG_MAIN_CTR', 'Haga clic a través de tasa');

define('LANG_FORM_HELLO', '¡Hola');
define('LANG_FORM_LOGIN', 'Puedes acceder al');
define('LANG_FORM_REGARDS', 'Saludos');
define('LANG_FORM_REMIND2', 'Usted ha solicitado recientemente que su información de acceso por correo electrónico a esta dirección.');
define('LANG_FORM_REMIND4', 'Si usted piensa que usted no solicitó por esta información, por favor ignore este mensaje y acepte nuestras disculpas por tomar su tiempo.');
define('LANG_FORM_REMIND5', 'Gracias y buen día!');
define('LANG_FORM_SIGNUP2', 'Gracias por tú inscripción.');
define('LANG_FORM_SIGNUP3', 'Su información de acceso se encuentra por debajo, guardar esta información en un lugar seguro');
define('LANG_FORM_COMPLETE1', 'Tan pronto como administrador apruebe su anuncio, que se activará.');

define('LANG_FORM_ADAPPROVED', 'Su anuncio ha sido aprobada y ahora está recibiendo la exposición.');
define('LANG_FORM_ADDISAPPROVED1', 'Su anuncio se ha rechazado.');
define('LANG_FORM_ADDISAPPROVED2', 'La razón de desaprobación está por debajo'); // :
define('LANG_FORM_ADDISAPPROVED3', 'Por favor, responda a este correo electrónico con cualquier pregunta.');
define('LANG_FORM_PAPPROVED', 'Su pago ha sido aprobado.');
define('LANG_FORM_PDISAPPROVED', 'Su pago se ha rechazado.');

define('LANG_FORM_FORGETEMAIL', 'Su información de entrada ha sido enviada a su dirección de correo electrónico');
define('LANG_FORM_REMINDESUBJ', 'Recordatorio de Contraseña');
define('LANG_FORM_NORECEMAIL', 'No hay ningún registro de la dirección de correo electrónico.');
define('LANG_FORM_NONAME', 'Debe introducir un nombre de contacto!');
define('LANG_FORM_NOPASS', 'Debe introducir una contraseña!');
define('LANG_FORM_NOEMAIL', 'Debe introducir una dirección de correo electrónico!');
define('LANG_FORM_EMAILUSED', 'La dirección de correo electrónico se utiliza ya!');
define('LANG_FORM_SIGNUPESUBJ', 'Confirmación de Registro de');
define('LANG_FORM_ACCCREATED', 'Su cuenta ha sido creado! <br /> Por favor haga clic aquí para ir a la zona de Venta y envíe su anuncio(s).');
define('LANG_FORM_ENTERGRPURL', 'Debe introducir una dirección URL de anuncios gráficos!');
define('LANG_FORM_GRAPICADFILE', 'archivo gráfico del anuncio debe ser un .jpg, .gif, o .png imagen!');
define('LANG_FORM_ENTERFORURL', 'Debe introducir una URL del sitio web hacia adelante!');
define('LANG_FORM_GRAPICMWIDTH', 'Gráfico del anuncio supera el tamaño Anchura máxima de');
define('LANG_FORM_GRAPICMHEIGHT', 'Gráfico del anuncio supera el tamaño de Altura máxima de');
define('LANG_FORM_SELECTPAYMENT', 'Debe seleccionar una forma de pago!');

define('LANG_PAGE_UNAUTHORIZED', 'Usted no tiene permiso para ver esta página.');
define('LANG_PAGE_UNAUTHORIZED2', 'Usted puede acceder al sistema aquí');

define('LANG_PAGE_CANCEL', 'RAZÓN: <br />1) Ha cancelado su compra. <br />2) Problema con la cuenta del comerciante. <br />3) Problema con procesadores de sistemas de pago. <br /><br />Si esto es un error, por favor póngase en contacto con nosotros. <br />');

define('LANG_PAGE_UFAILED', 'Nombre de usuario No se pudo');
define('LANG_PAGE_FAILED', 'El nombre de usuario y la contraseña no coincide con los de la base de datos. <br /> Por favor, regrese y vuelva a conectarse. Por favor, asegúrese de que haya confirmado su dirección de correo electrónico. <br /> Si sigue teniendo problemas por favor contactenos.');

define('LANG_PAGE_REGDISABLED', 'Lo sentimos, el registro ha sido deshabilitado por el administrador. <br /> Si usted tiene alguna pregunta o duda, póngase en contacto con nosotros.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'La confirmación del anuncio');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN-Nuevo anuncio de aprobación a la espera!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Haga clic abajo para ver'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'Su pago se ha completado. Su anuncio será revisado y si no hay ningún problema, será aprobada pronto! <br /> <br /> Gracias!');

define('LANG_PAGE_SENDQRF', 'Envíenos su pregunta, petición, o de votos');
define('LANG_PAGE_MSGSENT', 'Usted mensaje ha sido enviado! <br /> Nos pondremos en contacto contigo en breve. <br /> <br />');
define('LANG_PAGE_ENTERMSG', 'Debe introducir un mensaje!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Publicidad Email de contacto'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'Mensaje');
define('LANG_MAIN_SUBJECT', 'Tema');
define('LANG_MAIN_SEND', 'Enviar');
define('LANG_MAIN_SHOWIP', 'Su IP registrada');
define('LANG_MAIN_THANKYOU', 'Gracias');

define('LANG_PAGE_STATTITLE', 'Estadísticas detalladas de anuncios');

define('LANG_PAGE_SELCAMPAIGN', 'Selecciona a continuación de una campaña');
define('LANG_PAGE_ADTYPEALLOWED', 'Tipo de anuncios permitidos');
define('LANG_PAGE_LIMITCHARLEFT', 'personajes limitada a la izquierda:');
define('LANG_PAGE_NOHTMLCODES', 'Ningún código HTML! Sólo palabras de texto!');

define('LANG_PAGE_PAYBUT', 'Proceder al pago de');

define('LANG_PAGE_UPLOADIMAGE', 'Subir archivo de imagen'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Subir archivo flash (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', 'URL del anuncio del flash'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', 'Administración Área Login');
define('LANG_FMENU_LOADTIME', 'Esta página tomó %f segundos en cargar.');
define('LANG_ADMIN_USER', 'Nombre de usuario');
define('LANG_ADMIN_SUBMITBUT', 'Presentar');
define('LANG_ADMIN_LOGININFO', 'Log-in para ver la zona de administración');
define('LANG_ADMIN_LOGINFAIL', 'No se pudo. Re-ingresar de nuevo.');
define('LANG_ADMIN_ACCTSUC', 'Cuenta editado correctamente detalles');
define('LANG_ADMIN_LOGINBUT', 'Acceder');
define('LANG_ADMIN_INCWRITE', 'Soy capaz de escribir a /incluye directorio. Este es un riesgo de seguridad, debe CHMOD la carpeta (permisos) de vuelta a 755!');
define('LANG_ADMIN_INSWRITE', 'La / el directorio de instalación existe! Este es un riesgo para la seguridad, debe eliminar el archivo / directorio de instalación!');

define('LANG_AMENU_LOGGEDAS', 'Conectado como');
define('LANG_AMENU_ADMINR', 'ADMINISTRATOR');
define('LANG_AMENU_LOGOUT', 'Cierre la sesión');
define('LANG_AMENU_CHGPW', 'Cambiar contraseña');
define('LANG_AMENU_HOME', 'Casa');
define('LANG_AMENU_EMAILAD', 'Email Publicistas');
define('LANG_AMENU_MANADV', 'Administrar Publicistas');
define('LANG_AMENU_MANCAM', 'Administrar Campañas');
define('LANG_AMENU_SITESET', 'Configuración del sitio');
define('LANG_AMENU_CURRSET', 'Configuración de divisas');
define('LANG_AMENU_PAYSSET', 'Ajustes de pago');
define('LANG_AMENU_ADMACCT', 'Gestión de Admin');
define('LANG_AMENU_UPGRADE', 'Mejorar');
define('LANG_AMENU_HOWTOS', '¿Cómo tos');

define('LANG_AMENU_ADWAIT', 'Esperando');
define('LANG_AMENU_CLICKSLOG', 'Lista de IPs');
define('LANG_AMENU_CLICKSLOG2', 'Útiles para vigilar las actividades sospechosas, tales como el fraude de clics, de lo contrario.');
define('LANG_AMENU_COUPONS', 'Cupones');
define('LANG_AMENU_MANCOUP', 'Gestionar los cupones');
define('LANG_AMENU_ADVERTISERS', 'Anunciantes');
define('LANG_AMENU_CAMPAIGNS', 'Campañas');
define('LANG_AMENU_SETTINGS', 'Configuración');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Denegar las direcciones IP');

define('LANG_ADMIN_UPGRADEAREA', 'Descargar / Actualizar Espacio');

define('LANG_ADMIN_PAYSETTINGS', 'Ajustes de pago');
define('LANG_ADMIN_PAYENABLECUR', 'Debe habilitar las monedas en');
define('LANG_ADMIN_OFFDISABLED', 'OFF / DISCAPACITADOS');
define('LANG_ADMIN_ONENABLED', 'ON / HABILITADO');
define('LANG_ADMIN_CURRACCEPTED', 'Monedas Aceptadas');
define('LANG_ADMIN_CURRENCYNOTE', '*Usted debe marcar la casilla (s) de arriba para permitir que los anunciantes para ver Monedas Opciones en el Estado miembro / Venta Zona.');
define('LANG_ADMIN_ECUMONEYNOTE', 'IMPORTANTE</ strong>: En primer lugar ver más información en la página Herramientas de vendedor antes de usar ECUmoney');
define('LANG_ADMIN_EMAILADDRESS', 'Correo electrónico');
define('LANG_ADMIN_ACCOUNTNUM', 'Cuenta #');
define('LANG_ADMIN_USERID', 'ID de usuario');
define('LANG_ADMIN_HOLDINGNUM', 'Sosteniendo #');
define('LANG_ADMIN_STORENAME', 'Nombre de la tienda');
define('LANG_ADMIN_STOREID', 'ID de la tienda');
define('LANG_ADMIN_LOGINID', 'LoginID');
define('LANG_ADMIN_TRANSKEY', 'Transaction Key');
define('LANG_ADMIN_UPDATEBUT', 'Actualización');

define('LANG_ADMIN_GOBACK', 'Regresar');
define('LANG_ADMIN_DELETEBUT', 'Borrar');
define('LANG_ADMIN_STATS', 'Estadísticas del anuncio');
define('LANG_ADMIN_DATEJOIN', 'Fecha de Ingreso');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', 'Publicistas (Haga clic sobre Venta de editar los detalles o ver los anuncios)');
define('LANG_ADMIN_NUMOFADS', '# De Anuncios');
define('LANG_ADMIN_ADDNEWAD', 'Añadir nuevo anuncio a la Venta');
define('LANG_ADMIN_ADDNEWUSER', 'Agregar usuario de Venta Nueva');

define('LANG_ADMIN_SITESETTINGS', 'Configuración del sitio');
define('LANG_ADMIN_OPEN', 'Abierto');
define('LANG_ADMIN_CLOSED', 'Cerrado');
define('LANG_ADMIN_IFCLOSED', '* Si se cierra, entonces la gente no será capaz de registrar una cuenta.');
define('LANG_ADMIN_AEMAIL', 'Email del administrador');
define('LANG_ADMIN_SENDFROM', 'Enviar por E-mail');
define('LANG_ADMIN_ABPATHTO', 'Absoluto Camino al sitio');
define('LANG_ADMIN_SITEURL', 'URL del sitio');
define('LANG_ADMIN_SITENAME', 'Nombre del sitio');
define('LANG_ADMIN_SITEKEYWORD', 'Sitio Palabras clave');
define('LANG_ADMIN_SITEDESC', 'Descripción del Sitio');
define('LANG_ADMIN_DEFAULTLAN', 'Lenguaje por defecto');
define('LANG_ADMIN_DEFAULTCUR', 'Mneda predeterminada');
define('LANG_ADMIN_DEFAULTTEM', 'Tema por defecto');
define('LANG_ADMIN_ALLOWREG', 'Permitir registro');
define('LANG_ADMIN_SHOWMSTATS', 'Mostrar Estadísticas');

define('LANG_ADMIN_SRYNOADVRS', 'Lo sentimos no se encontraron los anunciantes para enviar el email a!');
define('LANG_ADMIN_SENDTOWHO', 'Enviar a');
define('LANG_ADMIN_NUMOFADVRE', 'Número de correo electrónico enviado a Advertiers');
define('LANG_ADMIN_EADVRSNOTE', 'Puede enviar un correo electrónico a los anunciantes para hacerles saber acerca de ofertas especiales, los bajos precios, recordando que nadie que no han pagado, etc');
define('LANG_ADMIN_ALLADVRCOUNT', 'Todos los anunciantes - Conde');
define('LANG_ADMIN_HELLONAME', 'Hola');
define('LANG_ADMIN_RECEIVEEMAIL', 'Usted está recibiendo este mensaje porque usted tiene una cuenta de anunciante con nosotros.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Sustituciones (inserta su uso en el cuerpo del mensaje y el campo tema)');

define('LANG_ADMIN_SERVERD', 'Detalles del Servidor');
define('LANG_ADMIN_GSVERSION', 'Versión corrector');
define('LANG_ADMIN_GSNEWS', 'Últimas noticias de Dijiteol');
define('LANG_ADMIN_TENANCE', 'Mantenimiento');

define('LANG_ADMIN_STATSINFO', 'Estadísticas (Comunicaciones)');
define('LANG_ADMIN_MADVERS', 'Publicistas');
define('LANG_ADMIN_MCAMIGS', 'Campañas');
define('LANG_ADMIN_MACTADS', 'Anuncios activos');
define('LANG_ADMIN_MINACTADS', 'Anuncios Inactivo');
define('LANG_ADMIN_MUNLIADS', 'Anuncios ilimitados');
define('LANG_ADMIN_MACTCRED', 'Activo Créditos');
define('LANG_ADMIN_MINACTCRED', 'Inactivo Créditos');
define('LANG_ADMIN_ML30DAYSI', '-30 Días impresiones');
define('LANG_ADMIN_ML30DAYSC', '-30 Días clics');
define('LANG_ADMIN_ML7DAYSI', '-7 Días impresiones');
define('LANG_ADMIN_ML7DAYSC', '-7 Días clics');
define('LANG_ADMIN_MYTIMPS', '-1 Imps totales ayer');
define('LANG_ADMIN_MYTDAYS', '-1 Ayer todos los clics');
define('LANG_ADMIN_TTIMPS', 'Hoy Total de imps');
define('LANG_ADMIN_TTCLICKS', 'Hoy en día todos los clics');
define('LANG_ADMIN_LASTCRONDATE', 'Cron Última Fecha');
define('LANG_ADMIN_CRONNEEDED', 'Cron se necesita hoy! Haga clic en el enlace');
define('LANG_ADMIN_CRONNONEED', 'Cron no se necesita hoy en día.');
define('LANG_ADMIN_ADSWAITING', 'Anuncios en espera de aprobación');
define('LANG_ADMIN_APPROVE', 'Aprobar');
define('LANG_ADMIN_DISAPPROVE', 'Desaprobar');
define('LANG_ADMIN_DISAREASON', 'Motivo del rechazo');
define('LANG_ADMIN_PAIDVIA', 'Pago a trave;s de');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Optimizar las tablas de base de datos');
define('LANG_ADMIN_OPTIMIZEDBT2', 'Optimizar');
define('LANG_ADMIN_OPTIMIZEDBT3', 'diseñados para deshacerse de todos los bloques fragmentados, liberar espacio sin utilizar y hacen que funcione su base de datos más rápido.');

define('LANG_ADMIN_PURHISTORY', 'Historial de compras (verificar sus pagos antes de verificar sus anuncios');

define('LANG_AFORM_EADAPPROVED', 'Su anuncio ha sido aprobado');
define('LANG_AFORM_EADDISAPPROVED', 'Su anuncio no ha sido aprobado');
define('LANG_AFORM_DBOPTIMIZED', 'Tablas de Bases Optimizado');
define('LANG_AFORM_SITESETTINGS', 'La configuración del sitio Actualizado');
define('LANG_AFORM_PAYSETTINGS', 'Ajustes de pago Actualizado');
define('LANG_AFORM_CURSETTINGS', 'Configuración de divisas Actualizado');
define('LANG_AFORM_USERDELETED', 'Usuario borrado');
define('LANG_AFORM_USERADDED', 'Alta de usuario');
define('LANG_AFORM_ADDTOUSER', 'Anuncio añadido para el usuario');
define('LANG_AFORM_USERUPDATED', 'Actualizado usuario');
define('LANG_AFORM_ADUPDATED', 'Actualización del anuncio');
define('LANG_AFORM_ADDELETED', 'Anuncio suprimido');
define('LANG_AFORM_ADADDED', 'Ha añadido un anuncio');
define('LANG_AFORM_CAMGNADDED', 'Campaña Alta');
define('LANG_AFORM_CAMGNUPDATED', 'Campaña de Actualización');
define('LANG_AFORM_CAMGNDELETED', 'Campaña suprimida');

define('LANG_AFORM_COUPONDELETED', 'Cupón eliminados');
define('LANG_AFORM_COUPONADDED', 'Cupón de Alta');
define('LANG_AFORM_COUPONUPDATED', 'Cupón Actualizado');

define('LANG_ADMIN_CAMGNTITLE', 'Campañas (Haga clic en la campaña para modificar detalles o obtener los códigos de anuncios)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Añadir nueva campaña');
define('LANG_ADMIN_SHORTDESC', 'Descripción corta');
define('LANG_ADMIN_SHORTDESCN', '*Info corto sobre la campaña, donde anuncios que se muestran, ...');
define('LANG_ADMIN_DEFAULTADN', 'Si no hay ningún anuncio disponible, entonces un anuncio predeterminado se mostrará.');
define('LANG_ADMIN_DEFAULTADU', 'Anuncio gráfico URL');
define('LANG_ADMIN_DEFAULTADF', 'Reenviar URL del anuncio');
define('LANG_ADMIN_LIMITCHARN', 'En caso negativo, por debajo de límite de caracteres de texto no importan.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Límite de caracteres de texto:');
define('LANG_ADMIN_TYPECPI', 'Costo por impresión');
define('LANG_ADMIN_TYPECPC', 'Coste por clic');
define('LANG_ADMIN_TYPECPD', 'Costo por día');
define('LANG_ADMIN_TYPECPDNOTE', 'Nota: Este requisito tareas cron automático o manual cada vez al día.');
define('LANG_ADMIN_ADVIL9', 'Escriba \'9999\' si desea permitir \'ilimitado\' anuncios.');
define('LANG_ADMIN_CURRCONVERTER', 'convertidor de divisas');
define('LANG_ADMIN_PRICEINJPY2', 'Para JPY-cantidad que no puede tener .xx <br /> Si el precio es &#165; 90.9270 entonces el formato correcto es &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'No saber tamaño exacto de anuncios? Esto puede ayudarle!');
define('LANG_ADMIN_ROTATESPEED', 'Velocidad de rotación de anuncios');
define('LANG_ADMIN_MILISECONDS', 'milisegundos');
define('LANG_ADMIN_NOTUSEROTAT', '*Deja BLANCO (sin espacios) si usted no desea utilizar la rotación.');
define('LANG_ADMIN_MILISECTOSEC', '10000 milisegundos = 10 segs, 30000 milisegundos = 30 segs.');
define('LANG_ADMIN_CONVERTMILLI', 'Convertir milisegundos a segundos');
define('LANG_ADMIN_NUMOFADRUN', 'Número de anuncios que se muestran');
define('LANG_ADMIN_TOTALNUMAD', 'El número máximo de anuncios que se muestran en la página. 5 *100x100* Los anuncios? 1 *468x60* anuncios?<br />Si deja en blanco, el valor predeterminado es un anuncio.');
define('LANG_ADMIN_ADDSPACE', 'Espacio: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Añadir un espacio entre los anuncios horizontales.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Descripción de visualización de anuncios de texto en Mouseover si el texto del anuncio habilitado.)');
define('LANG_ADMIN_CSSTEASERBGI', 'Background image');
define('LANG_ADMIN_CSSTEASERBGC', 'Background color');
define('LANG_ADMIN_CSSTEASERTO', 'Transparency / Opacity');
define('LANG_ADMIN_CSSTEASERFC', 'Font color');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Descripción de anuncios de display de texto debajo del anuncio si el texto del anuncio habilitado.)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Estilo de texto');
define('LANG_ADMIN_IFBLANK', 'Si está en blanco');
define('LANG_ADMIN_ADCODES', 'Códigos de anuncios');
define('LANG_ADMIN_ADCODEOP1', 'una opción: códigos PHP para las páginas PHP');
define('LANG_ADMIN_ADCODEOP2', 'la segunda opción: iframe código para otras páginas como: HTML, TPL, CGI y PHP así');
define('LANG_ADMIN_ADCODEIFRAME', '(usted tendrá que *aumentar* el iframe ancho y altura tamaños si se habilita Texto del anuncio!)');
define('LANG_ADMIN_HOWTOCRON', 'Cron puestos de trabajo le permiten automatizar ciertos comandos o scripts en su sitio. Puede configurar un comando o una secuencia de comandos para ejecutar en un momento específico cada día, semana, si no.');
define('LANG_ADMIN_HOWTOCRON1', 'Trabajo Cron se requiere para las campañas');
define('LANG_ADMIN_HOWTOCRON2', 'El archivo cron.php buscará a través de anuncios activos con campañas de uso de CPD y restar -1 de crédito cada anuncio.');
define('LANG_ADMIN_HOWTOCRON3', 'NOTA: Manualmente, se puede visitar el archivo cron.php se si Cron no está disponible');
define('LANG_ADMIN_HOWTOCRON4', 'Configurar el cron');
define('LANG_ADMIN_HOWTOCRON5', 'Ejecutar tarea programada una vez al día. Por debajo de aquí son varios códigos de trabajo cron puede utilizar');
define('LANG_ADMIN_HOWTOCRON6', 'Debe consultar a su servicio de alojamiento web y pregunte si Cron es permitido y qué códigos Cron que puede utilizar.');
define('LANG_ADMIN_HOWTOCRON7', 'Lea más sobre Cron');

define('LANG_ADMIN_PRICEIN', 'Precio en');
define('LANG_ADMIN_SPECIALDEAL', 'Marca especial?');
define('LANG_ADMIN_SPECIALDEAL2', 'Oferta Especial, Oferta por tiempo limitado, y así sucesivamente.');
define('LANG_ADMIN_DEFAULTATXT', 'Por defecto el texto del anuncio');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Iniciar una campaña de banners de publicidad nuevo');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Iniciar un nuevo enlace de texto campaña publicitaria');
define('LANG_ADMIN_CAMPAIGNTODO', 'Para ello');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Detalles');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Precios');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Estilos de campaña');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Estilos anuncio');
define('LANG_ADMIN_CAMPAIGNTODOG', 'Obtener el codigo');
define('LANG_ADMIN_ADGOESHERE', 'Descripción del anuncio va aquí...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Vista previa de su campaña al estilo actual');
define('LANG_ADMIN_ADSBYABLE', 'Mostrar Anuncios por sesión');
define('LANG_ADMIN_ADSBYLABEL', 'Anuncios de etiquetas / Nombre');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Por debajo de aquí, puede editar el estilo CSS');
define('LANG_ADMIN_CAMPDEFASTYLE', 'A continuación aquí es el estilo por defecto CSS');
define('LANG_ADMIN_NEWBANAD', 'Iniciar un nuevo anuncio de la bandera');
define('LANG_ADMIN_NEWTEXTAD', 'Iniciar un anuncio de texto nuevo enlace');
define('LANG_ADMIN_EXAMPLESTYLE', 'Ejemplo: (A continuación el uso de un estilo preestablecido) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Haga clic en la imagen para ver las estadísticas del anuncio');
define('LANG_ADMIN_CLICKSLOGCRTY', 'País');
define('LANG_ADMIN_COUPONCODEN', 'Cupones / Descuentos');
define('LANG_ADMIN_COUPONADDNEW', 'Añade nuevo Cupón / Descuento');
define('LANG_ADMIN_COUPONEDITC', 'Editar Cupón / Descuento');
define('LANG_ADMIN_NUMOFC', '# Usado');
define('LANG_ADMIN_LIMITUSES', 'utiliza límite');
define('LANG_ADMIN_NUMPERUSER', 'Número por usuario');

define('LANG_ADMIN_EDITAD', 'Editar Detalles del anuncio');
define('LANG_ADMIN_DENYIPSADD', 'Denegar la dirección IP');
define('LANG_AFORM_DENYIPADDED', 'IP Address Added');
define('LANG_ADMIN_IP', 'Dirección IP');
define('LANG_ADMIN_DENYIPS', 'Lista de direcciones IP denegado / prohibido');
define('LANG_AMENU_DENYCTRYS', 'negar a los países');
define('LANG_ADMIN_DENYCTRYS', 'negar a los países');
define('LANG_ADMIN_COUNTRY', 'País');
define('LANG_ADMIN_CTRY', 'Denegar País');
define('LANG_ADMIN_DENYCTRYADD', 'negar a los países');
define('LANG_AFORM_DENYCTRYADDED', 'País añadió');

define('LANG_ADMIN_OTHERPAY', 'Otras Formas de Pago');
define('LANG_ADMIN_OTHERPAY2', 'Enumere sus opciones de pago. es decir: Correo efectivo, cheque, bitcoin, más.');
define('LANG_ADMIN_OTHERPAY3', 'Ingrese su instrucción de pago aquí. por ejemplo: métodos alternativos de pago (bitcoin), pagos en línea sin cargo adicional.');

define('LANG_ADMIN_GCURR1', 'Si está habilitado, los precios se actualizan a través de Google convertidor de divisas al iniciar sesión en el área de administración.');
define('LANG_ADMIN_GCURR2', 'Aviso: Seleccione su moneda por defecto.');
define('LANG_ADMIN_GCURR3', 'También puede hacer el trabajo cron automático.');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Iniciar una nueva campaña de publicidad de enlaces mixtos bandera y texto'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Iniciar una nueva bandera y enlace de texto Anuncio'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Iniciar un nuevo flash (swf) anuncio'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Permitir anuncio flash (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Permitir la carga de archivos de imagen?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Permitir la carga del flash (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Tamaño máximo de archivo'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Aviso: No se recomienda'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'Si está habilitado, el anunciante puede cargar un archivo a la carpeta del servidor de alojamiento web.'); // 2014
define('LANG_ADMIN_DELETEALERT', '¿Seguro que deseas eliminarlo?'); // 2014
define('LANG_ADMIN_BUTTONALERT', '¿Estás seguro de que quieres hacer esto?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Archivo de Flash debe ser .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Iniciar un nuevo anuncio personalizado'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Inserte cualquier anuncios que desea utilizar como Google AdSense, o cualquier red de publicidad / intercambios. También puedes añadir el anuncio costumbre aquí.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Anuncio Personalizado'); // 2014
?>