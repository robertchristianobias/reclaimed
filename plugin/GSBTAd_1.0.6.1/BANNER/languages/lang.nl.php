<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Taal	: Nederlands
Datum   : Apr 28 2013
Modified: Nov 07, 2013
Credits : NIV.NL, Google Translate, GroovyScripts, Dijiteol
------------------------
===================================================
GELIEVE VOORZICHTIG TE ZIJN ALS U HIERONDER AANPASSINGEN UITVOERD WANT 
SOMMIGE REGELS BEVATTEN HTML CODES.

Gelieve speciale teken codes te gebruiken zodat ze goed weergegeven worden in de browser.

Misschien zijn er andere betere woorden? Als dat zo is, gelieve ons dan te helpen door vertalingen aan te passen. E-mail ons de geupdate bestanden.
===================================================
*/
define('LANG_TRANSLATION', 'garandeert niet de vertaling nauwkeurigheid en efficiëntie.');
// HTML PAGINA
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'nl');
define('LANG_CONT_HTML', 'nl');
// HOOFD & ADMININISTRATOR VOORKOMENDE WOORDEN (Op Veel pagina/'s komt een woord meer dan eens voor)
define('LANG_MENU_HOME', 'Home');
define('LANG_MENU_ADVERTISE', 'Adverteren');
define('LANG_MENU_LOGIN', 'Login');
define('LANG_MENU_LOGGEDIN', 'Ingelogd');
define('LANG_MENU_CONTACT', 'Contact');

define('LANG_FMENU_HOME', 'home');
define('LANG_FMENU_ADVERTISE', 'adverteer bij ons');
define('LANG_FMENU_LOGIN', 'adverteerders login');
define('LANG_FMENU_CONTACT', 'contacteer ons');
define('LANG_FMENU_RIGHTS', 'Alle rechten voorbehouden');
define('LANG_FMENU_SCRIPTBY', 'Script door');

define('LANG_MAIN_WELCOME', 'Om advertentieruimte te huren, gelieve een account te registreren of in te loggen als u al een account bij ons heeft.');
define('LANG_MAIN_ADOPTIONS', 'Adverteer Opties');
define('LANG_MAIN_CTITLE', 'Campagne');
define('LANG_MAIN_TITLE', 'Titel');
define('LANG_MAIN_SIZE', 'Advertentie Formaat');
define('LANG_MAIN_TYPE', 'Type');
define('LANG_MAIN_ADTYPEALLOWED', 'Advertentie Type Toegestaan');
define('LANG_MAIN_ROTATE', 'Rotatie');
define('LANG_MAIN_CREDITS', 'Credits');
define('LANG_MAIN_UNLCREDITS', 'Onbeperkte Credits');
define('LANG_MAIN_TEXT', 'Tekst');
define('LANG_MAIN_YES', 'Ja');
define('LANG_MAIN_NO', 'Nee');
define('LANG_MAIN_AVAIL', 'Beschikbaarheid');
define('LANG_MAIN_VIEWPRICES', 'Tarieven');
define('LANG_MAIN_ON', 'Aan');
define('LANG_MAIN_OFF', 'Uit');
define('LANG_MAIN_UNLIMITED', 'Onbeperkt');
define('LANG_MAIN_ETC', 'Het bedrag hierboven is de totaal Prijs. Als een prijs niet is weergegeven in de valuta die u wilt, dan is het niet beschikbaar voor aankoop.');
define('LANG_MAIN_ADTYPES', 'PPV- Prijs Per Vertoning, PPK- Prijs Per Klik naar uw site, PPD- Prijs Per Dag');
define('LANG_MAIN_PAYMENTMETHOD', 'Betaal Methode');
define('LANG_MAIN_PRODUCT', 'Product');
define('LANG_MAIN_BACKBUT', 'Terug');
define('LANG_MAIN_NEXTBUT', 'Verder');
define('LANG_MAIN_PREVBUT', 'Bekijk');
define('LANG_MAIN_AREA', 'Hoofd Zone');
define('LANG_MAIN_LOGOUT', 'Uitloggen');
define('LANG_MAIN_CLOSEWINDOW', 'Sluit Dit Venster');
define('LANG_MAIN_DATE', 'Datum');
define('LANG_MAIN_DATEADDED', 'Datum Toegevoegd');
define('LANG_MAIN_TOTAL', 'Totaal');
define('LANG_MAIN_IMPRESSIONS', 'Vertoningen');
define('LANG_MAIN_CLICKS', 'Kliks');
define('LANG_MAIN_DAYS', 'Dagen');
define('LANG_MAIN_STATUS', 'Staat');
define('LANG_MAIN_YOURADS', 'Uw Advertenties');
define('LANG_MAIN_CREMAINING', 'Resterende Credits');
define('LANG_MAIN_CHFDS', 'Klik hier voor getaileerde statistieken');
define('LANG_MAIN_CREATEAD', 'Maak een Nieuwe Advertentie');
define('LANG_MAIN_CREATEADAPP', 'Voeg een nieuwe Advertentie toe (alle advertenties moeten goedgekeurd worden door de administrator)');
define('LANG_MAIN_ACCEPTCURR', 'Geaccepteerde Valuta');
define('LANG_MAIN_PRICE', 'Tarief');
define('LANG_MAIN_SELECTED', 'U selecteerde');
define('LANG_MAIN_ADGRAPHICURL', 'Advertentie Grafische URL');
define('LANG_MAIN_ADFORWARDURL', 'Advertentie Doorstuur URL');
define('LANG_MAIN_ADTEXT', 'Advertentie Tekst');
define('LANG_MAIN_WIDTH', 'Breedte');
define('LANG_MAIN_HEIGHT', 'Hoogte');
define('LANG_MAIN_USERINFO', 'Gebruikers Informatie');
define('LANG_MAIN_ADDETAILS', 'Advertentie Details');
define('LANG_MAIN_VERIDETAILS', 'Gelieve alle details nakijken of ze correct zijn voordat u door gaat');
define('LANG_MAIN_TESTAD', 'Klik op de advertentie hierboven om te testen - het zal geopend worden in een nieuw venster');
define('LANG_MAIN_NEWUSER', 'Nieuwe Adverteerder');
define('LANG_MAIN_USERLOGIN', 'Adverteerder Login');
define('LANG_MAIN_ULOGIN', 'Login');
define('LANG_MAIN_FORGETINFO', 'Login Info Vergeten?');
define('LANG_MAIN_NAME', 'Naam');
define('LANG_MAIN_EMAIL', 'E-mail Adres');
define('LANG_MAIN_PASSWORD', 'Wachtwoord');
define('LANG_MAIN_REGISTER', 'Registreer');

define('LANG_MAIN_ADFORWARDURL2', 'Ad zichtbare URL');
define('LANG_MAIN_ADFORWARDURL3', '(Display korte URL: domain.com)');
define('LANG_MAIN_HISTORY', 'Aankoopgeschiedenis');
define('LANG_MAIN_CODE', 'Code');
define('LANG_MAIN_PERCENT', 'Procent');
define('LANG_MAIN_NUMBERS', 'Nummers');
define('LANG_MAIN_SUBMBUT', 'Invoeren');
define('LANG_MAIN_ENTERCOUPON', 'Voer coupon code');
define('LANG_MAIN_DISCOUNT', 'Korting');
define('LANG_MAIN_FREE', 'Kosteloos');

define('LANG_MAIN_CTR', 'Klik thru rate');

define('LANG_FORM_HELLO', 'Hallo');
define('LANG_FORM_LOGIN', 'U kunt inloggen op');
define('LANG_FORM_REGARDS', 'Met vriendelijke groet');
define('LANG_FORM_REMIND2', 'U heeft onlangs verzocht om uw login informatie gemaild naar dit adres.');
define('LANG_FORM_REMIND4', 'Als u denkt dat u niet heeft gevraagd voor deze informatie, gelieve dit bericht negeren en ons te verontschuldigen voor het nemen van uw tijd.');
define('LANG_FORM_REMIND5', 'Dank u en een goede dag!');
define('LANG_FORM_SIGNUP2', 'Bedankt voor het aanmelden.');
define('LANG_FORM_SIGNUP3', 'Uw inloggegevens informatie is hieronder, slaan deze informatie op een veilige plaats');
define('LANG_FORM_COMPLETE1', 'Zodra admin uw advertentie goedkeurt, wordt het geactiveerd.');

define('LANG_FORM_ADAPPROVED', 'Uw advertentie is goedgekeurd en wordt nu ontvangen blootstellingen.');
define('LANG_FORM_ADDISAPPROVED1', 'Uw advertentie is afgekeurd.');
define('LANG_FORM_ADDISAPPROVED2', 'De reden voor afkeuring is hieronder'); // :
define('LANG_FORM_ADDISAPPROVED3', 'Kunt u reageren op deze e-mail met eventuele vragen.');
define('LANG_FORM_PAPPROVED', 'Uw betaling is goedgekeurd.');
define('LANG_FORM_PDISAPPROVED', 'Uw betaling is afgekeurd.');

define('LANG_FORM_FORGETEMAIL', 'Uw login informatie is verzonden naar uw e-mail adres.');
define('LANG_FORM_REMINDESUBJ', 'Wachtwoord Herinnering');
define('LANG_FORM_NORECEMAIL', 'Geen record van het e-mail adres.');
define('LANG_FORM_NONAME', 'U dient een contact naam in te vullen!');
define('LANG_FORM_NOPASS', 'U dient een wachtwoord in te vullen!');
define('LANG_FORM_NOEMAIL', 'U dient een e-mail adres in te vullen!');
define('LANG_FORM_EMAILUSED', 'Het e-mail adres is reeds ingebruik!');
define('LANG_FORM_SIGNUPESUBJ', 'Aanmeld Bevestiging');
define('LANG_FORM_ACCCREATED', 'Uw account is aangemaakt!<br />Klik aub hier om naar de Adverteerders Rubriek te gaan, waar u uw advertentie(s) kunt plaatsen.');
define('LANG_FORM_ENTERGRPURL', 'U dient een grafische advertentie URL in te vullen!');
define('LANG_FORM_GRAPICADFILE', 'Een grafisch advertentie bestand moet een .jpg, .gif, of .png afbeelding zijn!');
define('LANG_FORM_ENTERFORURL', 'U dient een doorstuur URL in te vullen!');
define('LANG_FORM_GRAPICMWIDTH', 'Grafische Advertentie overschrijdt de MAXIMUM breede van');
define('LANG_FORM_GRAPICMHEIGHT', 'Grafische Advertentie overschrijdt de MAXIMUM hoogte van');
define('LANG_FORM_SELECTPAYMENT', 'U dient een betaalmethode te selecteren!');

define('LANG_PAGE_UNAUTHORIZED', 'U heeft niet de rechten om deze pagina te bekijken.');
define('LANG_PAGE_UNAUTHORIZED2', 'U kunt hier inloggen');

define('LANG_PAGE_CANCEL', 'REDEN: <br />1) U heeft u aankoop geannuleerd. <br />2) Probleem met het verkopers account. <br />3) Probleem met het Betaling Verwerkings Systeem. <br /><br />Als dit een fout is, gelieve ons dan te contacteren. <br />');

define('LANG_PAGE_UFAILED', 'Gebruikers Login Mislukt');
define('LANG_PAGE_FAILED', 'De gebruikersnaam en wachtwoord komen niet overeen met degene in onze database.<br />Gelieve terug te gaan en opnieuw in te loggen. Gelieve zeker ervan te zijn dat u uw e-mail adres bevestigd heeft.<br />Als u nog steeds problemen heeft gelieve ons te contacteren.');

define('LANG_PAGE_REGDISABLED', 'Excuses, registratie is uitgeschakeld door de administrator. <br /> Als u vragen of twijfels heeft, gelieve ons te contacteren.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'Advertentie Bevestiging');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMINISTRATOR- Nieuwe advertentie in afwachting van goedkeuring!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Klik hieronder om te bekijken'); // link URL naar website Administrator Zone in email naar administrator

define('LANG_PAGE_COMPLETED', 'Uw betaling is compleet. Uw ADVERTENTIE zal bekeken worden en als er geen problemen zijn, zal het tijdig goedgekeurd worden!<br /><br />Hartelijk Dank!');

define('LANG_PAGE_SENDQRF', 'Verzend ons uw Vraag, Aanvraag, of Feedback');
define('LANG_PAGE_MSGSENT', 'Uw bericht is verzonden!<br />Wij zullen zo spoedig mogelijk hierop terug komen.<br /><br />');
define('LANG_PAGE_ENTERMSG', 'U dient een bericht in te vullen!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Adverteren Contact E-mail'); // het e-mail onderwerp naar Administrator van contact formulier
define('LANG_MAIN_MESSAGE', 'Bericht');
define('LANG_MAIN_SUBJECT', 'Onderwerp');
define('LANG_MAIN_SEND', 'Verzend');
define('LANG_MAIN_SHOWIP', 'Uw gelogde IP');
define('LANG_MAIN_THANKYOU', 'Hartelijk Dank!');

define('LANG_PAGE_STATTITLE', 'Gedetaileerde Advertentie Statistieken');

define('LANG_PAGE_SELCAMPAIGN', 'Selecteer een Campagne hieronder');
define('LANG_PAGE_ADTYPEALLOWED', 'Advertentie Type Toegestaan');
define('LANG_PAGE_LIMITCHARLEFT', 'Gelimiteerde tekens over:');
define('LANG_PAGE_NOHTMLCODES', 'Geen HTML codes! Enkel Tekst Woorden!');

define('LANG_PAGE_PAYBUT', 'Ga door om te betalen');

define('LANG_PAGE_UPLOADIMAGE', 'Upload beeldbestand'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Upload flash-bestand (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', 'Advertentie flash URL'); // 2014

// ADMINISTRATOR
define('LANG_ADMIN_LOGINAREA', 'Administratie Login Zone');
define('LANG_FMENU_LOADTIME', 'Deze pagina is in  %f seconden geladen.');
define('LANG_ADMIN_USER', 'Gebruikersnaam');
define('LANG_ADMIN_SUBMITBUT', 'Verzenden');
define('LANG_ADMIN_LOGININFO', 'Login om de Administratie Zone te bekijken');
define('LANG_ADMIN_LOGINFAIL', 'Mislukt. Opnieuw inloggen.');
define('LANG_ADMIN_ACCTSUC', 'Account details succesvol bewerkt');
define('LANG_ADMIN_LOGINBUT', 'Login');
define('LANG_ADMIN_INCWRITE', 'Ik kan schrijven naar de /includes map. Dit is een beveiliging risico, u MOET CHMOD de map (Permissies) terugzetten naar 755!');
define('LANG_ADMIN_INSWRITE', 'De /install map bestaat! Dit is een beveiliging risico, u MOET de /install map verwijderen!');

define('LANG_AMENU_LOGGEDAS', 'Ingelogd als');
define('LANG_AMENU_ADMINR', 'ADMINISTRATOR');
define('LANG_AMENU_LOGOUT', 'Uitloggen');
define('LANG_AMENU_CHGPW', 'Wijzig Wachtwoord');
define('LANG_AMENU_HOME', 'Home');
define('LANG_AMENU_EMAILAD', 'E-mail Adverteerders');
define('LANG_AMENU_MANADV', 'Beheer Adverteerders');
define('LANG_AMENU_MANCAM', 'Beheer Campagnes');
define('LANG_AMENU_SITESET', 'Website Instellingen');
define('LANG_AMENU_CURRSET', 'Valuta-instellingen');
define('LANG_AMENU_PAYSSET', 'Betaling Instellingen');
define('LANG_AMENU_ADMACCT', 'Administrator Account');
define('LANG_AMENU_UPGRADE', 'Upgrade');
define('LANG_AMENU_HOWTOS', 'How To\'s');

define('LANG_AMENU_ADWAIT', 'Wachten');
define('LANG_AMENU_CLICKSLOG', 'Logs van IPs');
define('LANG_AMENU_CLICKSLOG2', 'Nuttig zijn om naar te kijken op verdachte activiteiten, zoals klikfraude, anders.');
define('LANG_AMENU_COUPONS', 'Coupons');
define('LANG_AMENU_MANCOUP', 'Beheer Coupons');
define('LANG_AMENU_ADVERTISERS', 'Adverteerders');
define('LANG_AMENU_CAMPAIGNS', 'Campagnes');
define('LANG_AMENU_SETTINGS', 'Instellingen');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Weigeren / Ban IP-adressen');

define('LANG_ADMIN_UPGRADEAREA', 'Download/Upgrade Zone');

define('LANG_ADMIN_PAYSETTINGS', 'Betaling Instellingen');
define('LANG_ADMIN_PAYENABLECUR', 'Je moet munten staat in');
define('LANG_ADMIN_OFFDISABLED', 'UIT/UITGESCHAKELD');
define('LANG_ADMIN_ONENABLED', 'AAN/INGESCHAKELD');
define('LANG_ADMIN_CURRACCEPTED', 'Geaccepteerde Valuta');
define('LANG_ADMIN_CURRENCYNOTE', '*U dient de vak(en) hierboven te selecteren om adverteerders de Valuta Opties in de Leden/Adverteerder Zone te laten zien.');
define('LANG_ADMIN_ECUMONEYNOTE', 'BELANGRIJK: Eerst meer informatie bekijken op de Verkoper Tools pagina voordat u gebruikt gaat maken van ECUmoney.');
define('LANG_ADMIN_EMAILADDRESS', 'E-mail Adres');
define('LANG_ADMIN_ACCOUNTNUM', 'Account #');
define('LANG_ADMIN_USERID', 'Gebruiker ID');
define('LANG_ADMIN_HOLDINGNUM', 'Bedrijf #');
define('LANG_ADMIN_STORENAME', 'Winkel Naam');
define('LANG_ADMIN_STOREID', 'Winkel ID');
define('LANG_ADMIN_LOGINID', 'Login ID');
define('LANG_ADMIN_TRANSKEY', 'Transactie Sleutel');
define('LANG_ADMIN_UPDATEBUT', 'Update');

define('LANG_ADMIN_GOBACK', 'Ga Terug');
define('LANG_ADMIN_DELETEBUT', 'Verwijder');
define('LANG_ADMIN_STATS', 'Advertentie Statistieken');
define('LANG_ADMIN_DATEJOIN', 'Datum Geregistreerd');
define('LANG_ADMIN_IDNUM', 'ID#');
define('LANG_ADMIN_ADEDITVIEW', 'Adverteerders (Klik op Adverteerder om details te bewerken of om advertenties te bekijken)');
define('LANG_ADMIN_NUMOFADS', '# Advertenties');
define('LANG_ADMIN_ADDNEWAD', 'Voeg Nieuwe Advertentie Toe aan Adverteerder');
define('LANG_ADMIN_ADDNEWUSER', 'Voeg Nieuwe Adverteerder/Gebruiker Toe');

define('LANG_ADMIN_SITESETTINGS', 'Website Instellingen');
define('LANG_ADMIN_OPEN', 'Open');
define('LANG_ADMIN_CLOSED', 'Gesloten');
define('LANG_ADMIN_IFCLOSED', '*Als gesloten, dan kunnen mensen geen account registeren.');
define('LANG_ADMIN_AEMAIL', 'Administrator E-mail Adres');
define('LANG_ADMIN_SENDFROM', 'Verzonden van E-mail Adres');
define('LANG_ADMIN_ABPATHTO', 'Absolute pad naar de site');
define('LANG_ADMIN_SITEURL', 'Website URL');
define('LANG_ADMIN_SITENAME', 'Website Naam');
define('LANG_ADMIN_SITEKEYWORD', 'Website Trefwoorden');
define('LANG_ADMIN_SITEDESC', 'Website Omschrijving');
define('LANG_ADMIN_DEFAULTLAN', 'Standaard taal');
define('LANG_ADMIN_DEFAULTCUR', 'Standaard valuta');
define('LANG_ADMIN_DEFAULTTEM', 'Standaard thema');
define('LANG_ADMIN_ALLOWREG', 'Registratie Toestaan');
define('LANG_ADMIN_SHOWMSTATS', 'Toon Statistieken');

define('LANG_ADMIN_SRYNOADVRS', 'Sorrie er zijn geen adverteerders gevonden om de e-mail naar toe te zenden!');
define('LANG_ADMIN_SENDTOWHO', 'Verzend naar');
define('LANG_ADMIN_NUMOFADVRE', 'Aantal Adverteerders e-mail verzenden naar');
define('LANG_ADMIN_EADVRSNOTE', 'U kunt een e-mail verzenden naar adverteerders voor hun te informeren over speciale aanbiedingen, lage prijzen, iemand herinneren die niet betaald heeft, etc.');
define('LANG_ADMIN_ALLADVRCOUNT', 'Alle adverteerders - Tellen');
define('LANG_ADMIN_HELLONAME', 'Hallo');
define('LANG_ADMIN_RECEIVEEMAIL', 'U ontvangd deze e-mail omdat u een adverteerders account heeft bij ons.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Substituties (gebruik invoegingen in de inhoud en het onderwerp velden van het bericht)');

define('LANG_ADMIN_SERVERD', 'Server Details');
define('LANG_ADMIN_GSVERSION', 'Versie checker');
define('LANG_ADMIN_GSNEWS', 'Laatste nieuws van Dijiteol');
define('LANG_ADMIN_TENANCE', 'Website Onderhoud');

define('LANG_ADMIN_STATSINFO', 'Statistieken (Informatie)');
define('LANG_ADMIN_MADVERS', 'Adverteerders');
define('LANG_ADMIN_MCAMIGS', 'Campagnes');
define('LANG_ADMIN_MACTADS', 'Actieve Advertenties');
define('LANG_ADMIN_MINACTADS', 'Inactieve Advertenties');
define('LANG_ADMIN_MUNLIADS', 'Onbeperkte Advertenties');
define('LANG_ADMIN_MACTCRED', 'Actieve Credits');
define('LANG_ADMIN_MINACTCRED', 'Inactieve Credits');
define('LANG_ADMIN_ML30DAYSI', '-30 Dagen Totaal Vertoningen');
define('LANG_ADMIN_ML30DAYSC', '-30 Dagen Totaal Kliks');
define('LANG_ADMIN_ML7DAYSI', '-7 Dagen Totaal Vertoningen');
define('LANG_ADMIN_ML7DAYSC', '-7 Dagen Totaal Kliks');
define('LANG_ADMIN_MYTIMPS', '-1 Totaal Vertoningen Gisteren');
define('LANG_ADMIN_MYTDAYS', '-1 Totaal Kliks Gisteren');
define('LANG_ADMIN_TTIMPS', 'Totaal Vertoningen Vandaag');
define('LANG_ADMIN_TTCLICKS', 'Totaal Kliks Vandaag');
define('LANG_ADMIN_LASTCRONDATE', 'Cron Laaste Datum');
define('LANG_ADMIN_CRONNEEDED', 'Cron is nodig vandaag! Klik op de link');
define('LANG_ADMIN_CRONNONEED', 'Cron is niet nodig vandaag.');
define('LANG_ADMIN_ADSWAITING', 'Advertenties in Afwachting van Goedkeuring');
define('LANG_ADMIN_APPROVE', 'GOEDKEUREN');
define('LANG_ADMIN_DISAPPROVE', 'AFKEUREN');
define('LANG_ADMIN_DISAREASON', 'Reden Afkeuring');
define('LANG_ADMIN_PAIDVIA', 'Betaling via');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Optimaliseer Database Tabellen');
define('LANG_ADMIN_OPTIMIZEDBT2', 'Optimalitseer');
define('LANG_ADMIN_OPTIMIZEDBT3', 'ontworpen om zich te ontdoen van alle gefragmenteerde blokken, ongebruikte ruimte vrij te maken en uw database sneller te maken.');

define('LANG_ADMIN_PURHISTORY', 'Aankoopgeschiedenis (controleren of hun betalingen voor het controleren van hun advertenties)');

define('LANG_AFORM_EADAPPROVED', 'Uw advertentie is goedgekeurd');
define('LANG_AFORM_EADDISAPPROVED', 'Uw advertentie is <nog> niet goedgekeurd');
define('LANG_AFORM_DBOPTIMIZED', 'Database Tabellen Geoptimaliseerd');
define('LANG_AFORM_SITESETTINGS', 'Website Instellingen Geüpdate');
define('LANG_AFORM_PAYSETTINGS', 'Betaling Instellingen Geüpdate');
define('LANG_AFORM_CURSETTINGS', 'Valuta-instellingen Bijgewerkt');
define('LANG_AFORM_USERDELETED', 'Gebruiker Verwijderd');
define('LANG_AFORM_USERADDED', 'Gebruiker Toegevoegd');
define('LANG_AFORM_ADDTOUSER', 'Advertentie gekoppeld aan gebruiker');
define('LANG_AFORM_USERUPDATED', 'Gebruiker Bijgewerkt');
define('LANG_AFORM_ADUPDATED', 'Advertentie Bijgewerkt');
define('LANG_AFORM_ADDELETED', 'Advertentie Verwijderd');
define('LANG_AFORM_ADADDED', 'Advertentie Toegevoegd');
define('LANG_AFORM_CAMGNADDED', 'Campagne Toegevoegd');
define('LANG_AFORM_CAMGNUPDATED', 'Campagne Bijgewerkt');
define('LANG_AFORM_CAMGNDELETED', 'Campagne Verwijderd');

define('LANG_AFORM_COUPONDELETED', 'Coupon verwijderd');
define('LANG_AFORM_COUPONADDED', 'Coupon toegevoegd');
define('LANG_AFORM_COUPONUPDATED', 'Coupon bijgewerkt');

define('LANG_ADMIN_CAMGNTITLE', 'Campagnes (Klik op Campagne om de details te bewerken of om advertentie codes te krijgen)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Nieuwe Campagne Toevoegen');
define('LANG_ADMIN_SHORTDESC', 'Korte Omschrijving');
define('LANG_ADMIN_SHORTDESCN', '*Korte info over campagne, waar advertenties worden vertoond, ...');
define('LANG_ADMIN_DEFAULTADN', 'Als er geen advertentie aanwezig is, dan zal de standaard ad vertoond worden.');
define('LANG_ADMIN_DEFAULTADU', 'Standaard Advertentie Grafische URL');
define('LANG_ADMIN_DEFAULTADF', 'Standaard Advertentie Doorstuur URL');
define('LANG_ADMIN_LIMITCHARN', 'Zo Nee, Teksttekens Limiet hieronder doen er niet toe.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Limiet Teksttekens');
define('LANG_ADMIN_TYPECPI', 'Kosten per Vertoning');
define('LANG_ADMIN_TYPECPC', 'Kosten per Klik');
define('LANG_ADMIN_TYPECPD', 'Kosten per Dag');
define('LANG_ADMIN_TYPECPDNOTE', 'Let op: Dit vereist automatische of handmatige cron taak elke keer per dag.');
define('LANG_ADMIN_ADVIL9', 'Hoeveel wilt u er verkopen? Typ \'9999\' in als u een \'ongelimiteerd\' aantal advertenties wil toestaan.');
define('LANG_ADMIN_CURRCONVERTER', 'Valuta Omrekenen');
define('LANG_ADMIN_PRICEINJPY2', 'Voor JPY- bedrag kan niet .xx <br /> Als de prijs &#165; 90.9270 is dan is het correcte formaat &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'Weet u niet de exacte advertentie afmetingen? Dit kan u helpen!');
define('LANG_ADMIN_ROTATESPEED', 'Advertentie Rotatie Snelheid');
define('LANG_ADMIN_MILISECONDS', 'milliseconden');
define('LANG_ADMIN_NOTUSEROTAT', '*Laat LEEG (geen spatie) als u wenst geen rotatie te gebruiken.');
define('LANG_ADMIN_MILISECTOSEC', '10000 Milliseconden = 10 Seconden, 30000 Milliseconden = 30 Seconden.');
define('LANG_ADMIN_CONVERTMILLI', 'Converteer Milliseconden naar Seconden');
define('LANG_ADMIN_NUMOFADRUN', '# advertentie Getoont');
define('LANG_ADMIN_TOTALNUMAD', 'Het maximum aantal advertentie(s) getoont op de pagina. 5 *100x100* Advertenties? 1 *468x60* Advertentie?<br />Als het leeg is LEEG gelaten, dan is de standaard 1 Advertentie.');
define('LANG_ADMIN_ADDSPACE', 'Ruimte: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Voeg een ruimte toe tussen *horizontale* advertenties.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Geef Advertentie Tekst Omschrijving Weer bij Muisover als Tekst Advertentie Ingeschakeld zijn)');
define('LANG_ADMIN_CSSTEASERBGI', 'Achtergrond afbeelding');
define('LANG_ADMIN_CSSTEASERBGC', 'Achtergrond kleur');
define('LANG_ADMIN_CSSTEASERTO', 'Transparantie / Ondoorzichtigheid');
define('LANG_ADMIN_CSSTEASERFC', 'Lettertype kleur');
define('LANG_ADMIN_CSSTEASERBT', 'Rand-boven');
define('LANG_ADMIN_CSSTEASERBTLR', 'Rand-boven links radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Rand-boven rechts radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Geef Advertentie Tekst Omschrijving Weer onder advertentie als Tekst Advertentie Ingeschakeld zijn)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Tekst Stijl');
define('LANG_ADMIN_IFBLANK', 'Als leeg');
define('LANG_ADMIN_ADCODES', 'Advertentie Codes');
define('LANG_ADMIN_ADCODEOP1', 'Optie 1: php codes voor PHP pagina\'s');
define('LANG_ADMIN_ADCODEOP2', 'Optie 2: iframe code voor andere pagina\'s zoals: HTML, TPL, CGI, en PHP alsmede');
define('LANG_ADMIN_ADCODEIFRAME', '(Mogelijk dient u de breedte & hoogte te veranderen zodat het goed uitziet op uw website pagina\'s.)');
define('LANG_ADMIN_HOWTOCRON', 'Cron taken staan u toe om bepaalde commando\'s of scripts te automatiseren op uw website. U kunt een commando of script instellen om op een bepaalde tijd iedere dag, week, etc. uitgevoerd te worden.');
define('LANG_ADMIN_HOWTOCRON1', 'Cron taak is vereist voor Campagne gebruik');
define('LANG_ADMIN_HOWTOCRON2', 'Het cron.php bestand zal zoeken door actieve advertenties met PPD campagnes en -1 credit per advertentie aftrekken.');
define('LANG_ADMIN_HOWTOCRON3', 'LET OP: U kunt handmatig het cron.php bestand bezoeken als Cron niet beschikbaar is');
define('LANG_ADMIN_HOWTOCRON4', 'Cron Oprichten');
define('LANG_ADMIN_HOWTOCRON5', 'Voer cron taak een keer Per Dag uit. Hieronder zijn verschillende cron taak codes die u kunt gebruiken');
define('LANG_ADMIN_HOWTOCRON6', 'U zul uw Web Host moeten contacteren en vragen of Cron is toegestaan en welke Cron codes u kunt gebruiken.');
define('LANG_ADMIN_HOWTOCRON7', 'Lees meer over Cron');

define('LANG_ADMIN_PRICEIN', 'Prijs in');
define('LANG_ADMIN_SPECIALDEAL', 'Merk het bijzonder?');
define('LANG_ADMIN_SPECIALDEAL2', 'Speciale deal, Tijdelijke aanbieding, en ga zo maar door.');
define('LANG_ADMIN_DEFAULTATXT', 'Standaard advertentietekst');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Start een nieuwe banner reclamecampagne');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Start een nieuwe tekst link reclamecampagne');
define('LANG_ADMIN_CAMPAIGNTODO', 'Om te doen');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Details');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Prijzen');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Campagne stijlen');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Ad stijlen');
define('LANG_ADMIN_CAMPAIGNTODOG', 'Krijgen codes');
define('LANG_ADMIN_ADGOESHERE', 'Ad omschrijving komt hier...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Een voorbeeld van uw huidige campagne-stijl');
define('LANG_ADMIN_ADSBYABLE', 'Toon Ads door teken');
define('LANG_ADMIN_ADSBYLABEL', 'Ads door Label/Naam');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Hier beneden kunt u de CSS-stijl');
define('LANG_ADMIN_CAMPDEFASTYLE', 'Hier beneden is de standaard CSS-stijl');
define('LANG_ADMIN_NEWBANAD', 'Start een nieuwe banner advertentie');
define('LANG_ADMIN_NEWTEXTAD', 'Start een nieuwe tekst link advertentie');
define('LANG_ADMIN_EXAMPLESTYLE', 'Voorbeeld: (Hieronder gebruik maken van een vooraf ingestelde stijl) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Klik op de afbeelding om ad stats te bekijken');
define('LANG_ADMIN_CLICKSLOGCRTY', 'Land');
define('LANG_ADMIN_COUPONCODEN', 'Coupons / Kortingen');
define('LANG_ADMIN_COUPONADDNEW', 'Voeg nieuwe Coupon / Korting');
define('LANG_ADMIN_COUPONEDITC', 'Edit Coupon / Korting');
define('LANG_ADMIN_NUMOFC', '# Gebruikt');
define('LANG_ADMIN_LIMITUSES', 'Limiet gebruikt');
define('LANG_ADMIN_NUMPERUSER', '# Per gebruiker');

define('LANG_ADMIN_EDITAD', 'Bewerk Advertentie details');
define('LANG_ADMIN_DENYIPSADD', 'Weigeren IP-adres');
define('LANG_AFORM_DENYIPADDED', 'IP Address Added');
define('LANG_ADMIN_IP', 'IP-adres');
define('LANG_ADMIN_DENYIPS', 'Lijst van IP-adressen geweigerd/verboden');
define('LANG_AMENU_DENYCTRYS', 'Weigeren Landen');
define('LANG_ADMIN_DENYCTRYS', 'Weigeren Landen');
define('LANG_ADMIN_COUNTRY', 'Land');
define('LANG_ADMIN_CTRY', 'Weigeren Land');
define('LANG_ADMIN_DENYCTRYADD', 'Weigeren Landen');
define('LANG_AFORM_DENYCTRYADDED', 'Land toegevoegd');

define('LANG_ADMIN_OTHERPAY', 'Andere betaalwijzen');
define('LANG_ADMIN_OTHERPAY2', 'Lijst van uw betalingsopties. dat wil zeggen: mail contant geld, cheque, bitcoin, anders.');
define('LANG_ADMIN_OTHERPAY3', 'Voer hier uw betaalopdracht. dat wil zeggen: alternatieve betalingsmethoden (bitcoin), offline betalingen, anders.');

define('LANG_ADMIN_GCURR1', 'Indien ingeschakeld, worden de prijzen geactualiseerd met behulp van Google het omrekenen van valuta wanneer je inlogt in admin gebied.');
define('LANG_ADMIN_GCURR2', 'Waarschuwing: Selecteer uw standaard valuta.');
define('LANG_ADMIN_GCURR3', 'U kunt ook automatische cron job');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Start een nieuw gemengd banner en tekstlink reclamecampagne'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Start een nieuw gemengd banner en tekstlink advertentie'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Start een nieuwe flash (swf) advertentie'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Laat flash advertentie (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Toestaan ​​beeldbestanden uploaden?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Toestaan ​​flash (.swf) bestanden uploaden?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Maximale bestandsgrootte'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Waarschuwing: Niet aanbevolen'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'Indien ingeschakeld, kunnen adverteerder een bestand uploaden naar uw web host map server.'); // 2014
define('LANG_ADMIN_DELETEALERT', 'Weet u zeker dat u dit wilt verwijderen?'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'Weet je zeker dat je dit wilt doen?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Flash-bestand moet .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Start nieuwe aangepaste advertentie'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Steek geen advertenties die u wilt gebruiken, zoals Google AdSense, of een reclame-netwerken / uitwisselingen. U kunt ook hier aangepaste advertentie.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Aangepaste advertentie'); // 2014
?>