<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: Korean / 한국인
Date    : Jul 02 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
때문 아래 편집할 때 조심하세요
일부 라인은 HTML 코드를 포함합니다.

그들이 제대로 브라우저에 표시되도록 특수 문자 코드를 사용하십시오.

아마 다른 적당한 단어가있다? 그렇다면 다음 아래의 번역 오류를 수정하는 데 도움 주시기 바랍니다.업데이트된 파일과 함께 이메일을 보내주시기 바랍니다.
===================================================
*/
define('LANG_TRANSLATION', '번역의 정확성과 효율성을 보장하지 않습니다.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'ko');
define('LANG_CONT_HTML', 'ko');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', '홈');
define('LANG_MENU_ADVERTISE', '광고하다');
define('LANG_MENU_LOGIN', '로그인');
define('LANG_MENU_LOGGEDIN', '로그인');
define('LANG_MENU_CONTACT', '이메일');

define('LANG_FMENU_HOME', '홈');
define('LANG_FMENU_ADVERTISE', '우리와 함께 광고');
define('LANG_FMENU_LOGIN', '광고주 로그인');
define('LANG_FMENU_CONTACT', '이메일');
define('LANG_FMENU_RIGHTS', '모든 권리 보유');
define('LANG_FMENU_SCRIPTBY', '에 의해 스크립트');

define('LANG_MAIN_WELCOME', '광고를 구입하려면 이미있을 경우 계정 또는 로그인을 등록하시기 바랍니다.');
define('LANG_MAIN_ADOPTIONS', '광고 옵션');
define('LANG_MAIN_CTITLE', '운동');
define('LANG_MAIN_TITLE', '표제');
define('LANG_MAIN_SIZE', '광고 크기');
define('LANG_MAIN_TYPE', '상징');
define('LANG_MAIN_ADTYPEALLOWED', '허용 이미지 광고 유형');
define('LANG_MAIN_ROTATE', '순환');
define('LANG_MAIN_CREDITS', '크레딧');
define('LANG_MAIN_UNLCREDITS', '무제한 크레딧');
define('LANG_MAIN_TEXT', '정본');
define('LANG_MAIN_YES', '예');
define('LANG_MAIN_NO', '아니');
define('LANG_MAIN_AVAIL', '당선 가능성');
define('LANG_MAIN_VIEWPRICES', '가격보기');
define('LANG_MAIN_ON', '할 수있게하다');
define('LANG_MAIN_OFF', '무력하게하다');
define('LANG_MAIN_UNLIMITED', '제한없는');
define('LANG_MAIN_ETC', '위에 표시된 가격은 총 비용입니다.가격은 당신이 원하는 화폐로 표시되지 않는 경우는 구입할 수 없습니다.');
define('LANG_MAIN_ADTYPES', 'CPI- 노출 당 비용, CPC- 을 통해 클릭당 비용, CPD- 하루에 비용');
define('LANG_MAIN_PAYMENTMETHOD', '지불 방법');
define('LANG_MAIN_PRODUCT', '생성물');
define('LANG_MAIN_BACKBUT', '뒤로');
define('LANG_MAIN_NEXTBUT', '다음에');
define('LANG_MAIN_PREVBUT', '예비 검사');
define('LANG_MAIN_AREA', '주요 지역');
define('LANG_MAIN_LOGOUT', '로그아웃');
define('LANG_MAIN_CLOSEWINDOW', '이 창 닫기');
define('LANG_MAIN_DATE', '당일');
define('LANG_MAIN_DATEADDED', '날짜 추가');
define('LANG_MAIN_TOTAL', '합계');
define('LANG_MAIN_IMPRESSIONS', '노출');
define('LANG_MAIN_CLICKS', '클릭수');
define('LANG_MAIN_DAYS', '일');
define('LANG_MAIN_STATUS', '터');
define('LANG_MAIN_YOURADS', '귀하의 광고');
define('LANG_MAIN_CREMAINING', '남은 크레딧');
define('LANG_MAIN_CHFDS', '상세 통계 보시려면 여기를 클릭하세요');
define('LANG_MAIN_CREATEAD', '새 광고 만들기');
define('LANG_MAIN_CREATEADAPP', '새 광고 추가 (모든 광고는 관리자의 승인을받습니다)');
define('LANG_MAIN_ACCEPTCURR', '허용되는 통화');
define('LANG_MAIN_PRICE', '가격');
define('LANG_MAIN_SELECTED', '당신이 선택한');
define('LANG_MAIN_ADGRAPHICURL', '광고 그래픽 URL');
define('LANG_MAIN_ADFORWARDURL', '광고 전달 URL');
define('LANG_MAIN_ADTEXT', '광고 문안');
define('LANG_MAIN_WIDTH', '폭');
define('LANG_MAIN_HEIGHT', '고도');
define('LANG_MAIN_USERINFO', '사용자 정보');
define('LANG_MAIN_ADDETAILS', '광고 세부 정보');
define('LANG_MAIN_VERIDETAILS', '모든 세부 계속하기 전에 정확한지 확인하십시오');
define('LANG_MAIN_TESTAD', '테스트 위의 광고를 클릭 - 그것은 새 창에서 열립니다');
define('LANG_MAIN_NEWUSER', '신규 광고주');
define('LANG_MAIN_USERLOGIN', '광고주 로그인');
define('LANG_MAIN_ULOGIN', '로그인');
define('LANG_MAIN_FORGETINFO', '정보 로그인 잊어 버리라 고요?');
define('LANG_MAIN_NAME', '이름');
define('LANG_MAIN_EMAIL', '이메일 주소');
define('LANG_MAIN_PASSWORD', '암호');
define('LANG_MAIN_REGISTER', '레지스터');

define('LANG_MAIN_ADFORWARDURL2', '광고 표시');
define('LANG_MAIN_ADFORWARDURL3', '(짧은 표시 URL: domain.com)');
define('LANG_MAIN_HISTORY', '구매 내역');
define('LANG_MAIN_CODE', '규약');
define('LANG_MAIN_PERCENT', '퍼센트');
define('LANG_MAIN_NUMBERS', '민수기');
define('LANG_MAIN_SUBMBUT', '제출하다');
define('LANG_MAIN_ENTERCOUPON', '쿠폰 코드를 입력');
define('LANG_MAIN_DISCOUNT', '할인');
define('LANG_MAIN_FREE', '무료의');

define('LANG_MAIN_CTR', '통해 속도를 클릭하십시오');

define('LANG_FORM_HELLO', '안녕하세요');
define('LANG_FORM_LOGIN', '당신이에 로그인 할 수 있습니다');
define('LANG_FORM_REGARDS', '감사합니다');
define('LANG_FORM_REMIND2', '당신은 최근에 로그인 정보가이 주소로 이메일하도록 요청했습니다.');
define('LANG_FORM_REMIND4', '당신은이 정보에 대한 요청하지 않은 것을 생각하면,이 메시지를 무시하고 시간을내어 우리의 사과를 받아 주시기 바랍니다.');
define('LANG_FORM_REMIND5', '당신과 좋은 하루 감사합니다!');
define('LANG_FORM_SIGNUP2', '가입 주셔서 감사합니다.');
define('LANG_FORM_SIGNUP3', '로그인 정보를 원하시면 아래이며, 안전한 장소에이 정보를 저장');
define('LANG_FORM_COMPLETE1', '빨리 관리자의 광고를 승인로서, 그것은 활성화됩니다.');

define('LANG_FORM_ADAPPROVED', '귀하의 광고가 승인되어 현재 노출을 받고있다.');
define('LANG_FORM_ADDISAPPROVED1', '귀하의 광고가 승인되지 않았습니다.');
define('LANG_FORM_ADDISAPPROVED2', '승인에 대한 이유는 아래와 같습니다'); // :
define('LANG_FORM_ADDISAPPROVED3', '어떤 질문이 이메일에 회신 해 주시기 바랍니다.');
define('LANG_FORM_PAPPROVED', '귀하의 지불이 승인되었습니다.');
define('LANG_FORM_PDISAPPROVED', '귀하의 지불이 승인되었습니다.');

define('LANG_FORM_FORGETEMAIL', '로그인 정보가 귀하의 이메일 주소로 발송되었습니다.');
define('LANG_FORM_REMINDESUBJ', '비밀 번호 알림');
define('LANG_FORM_NORECEMAIL', '이메일 주소에 대한 기록은 전혀 없어요.');
define('LANG_FORM_NONAME', '당신은 연락처 이름을 입력한다!');
define('LANG_FORM_NOPASS', '당신은 비밀 번호를 입력해야합니다!');
define('LANG_FORM_NOEMAIL', '당신은 이메일 주소를 입력하셔야합니다!');
define('LANG_FORM_EMAILUSED', '이메일 주소가 이미 사용 중입니다!');
define('LANG_FORM_SIGNUPESUBJ', '가입 확인');
define('LANG_FORM_ACCCREATED', '귀하의 계정이 만들어졌습니다!<br />광고주 지역에 가서 광고를 제출하려면 여기를 클릭하십시오.');
define('LANG_FORM_ENTERGRPURL', '당신은 그래픽 광고 링크 주소를 입력하셔야합니다!');
define('LANG_FORM_GRAPICADFILE', '그래픽 광고 파일이어야합니다 .jpg, .gif, or .png 이미지!');
define('LANG_FORM_ENTERFORURL', '당신은 앞으로 웹사이트 링크 주소를 입력하셔야합니다!');
define('LANG_FORM_GRAPICMWIDTH', '그래픽 광고의 최대 너비 크기를 초과');
define('LANG_FORM_GRAPICMHEIGHT', '그래픽 광고의 최대 높이 크기를 초과');
define('LANG_FORM_SELECTPAYMENT', '당신은 지불 방법을 선택해야합니다!');

define('LANG_PAGE_UNAUTHORIZED', '이 페이지를 볼 권한이 없습니다.');
define('LANG_PAGE_UNAUTHORIZED2', '당신이 로그인에 있습니다 여기에');

define('LANG_PAGE_CANCEL', '이유: <br />1) 귀하의 구매를 취소했다. <br />2) 판매자의 계정 문제. <br />3) 지불 프로세서 시스템과 문제. <br /><br />이것이 실수있다면 연락 주시기 바랍니다. <br />');

define('LANG_PAGE_UFAILED', '사용자 로그인 실패');
define('LANG_PAGE_FAILED', '사용자 이름과 암호가 데이터베이스에있는 것들과 일치하지 않습니다.<br />돌아가서 다시 로그인하십시오. 여러분의 이메일 주소를 확인하시기 바랍니다.<br />그래도 문제가 발생하면 것은 문의해 주시기 바랍니다.');

define('LANG_PAGE_REGDISABLED', '죄송 관리자에 의해 등록이 비활성화되었습니다. <br /> 질문이나 문제가 있으면 문의하시기 바랍니다.');

define('LANG_PAGE_ADCOMFIRMSUBJ', '광고 확인');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN - 새 광고가 승인을 기다리는!');
define('LANG_PAGE_ADMINCOMFSUBJ2', '보시려면 아래를 클릭'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', '결제가 완료되었습니다. 귀하의 광고가 검토되고 아무 문제가 없다면, 그것은 곧 승인됩니다!<br /><br />당신을 감사하십시오!');

define('LANG_PAGE_SENDQRF', '귀하의 질문, 요청, 또는 의견 보내기');
define('LANG_PAGE_MSGSENT', '메시지가 전송되었습니다!<br />우리는 곧 다시 얻을 것이다.<br /><br />');
define('LANG_PAGE_ENTERMSG', '당신은 메시지를 입력하셔야합니다!');
define('LANG_PAGE_CONTEMAILSUBJ', '광고 담당자 이메일'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', '메시지');
define('LANG_MAIN_SUBJECT', '제목');
define('LANG_MAIN_SEND', '보내기');
define('LANG_MAIN_SHOWIP', '당신의 기록된 IP 주소');
define('LANG_MAIN_THANKYOU', '감사합니다');

define('LANG_PAGE_STATTITLE', '상세 광고 통계');

define('LANG_PAGE_SELCAMPAIGN', '아래 캠페인을 선택합니다');
define('LANG_PAGE_ADTYPEALLOWED', '허용 이미지 광고 유형');
define('LANG_PAGE_LIMITCHARLEFT', '한정자가 남았습니다:');
define('LANG_PAGE_NOHTMLCODES', '아니 HTML 코드! 오직 텍스트 단어!');

define('LANG_PAGE_PAYBUT', '납부 진행');

define('LANG_PAGE_UPLOADIMAGE', '이미지 파일을 업로드'); // 2014
define('LANG_PAGE_UPLOADFLASH', '플래시 파일을 업로드 (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', '광고 플래시 URL'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', '행정 로그인 영역');
define('LANG_FMENU_LOADTIME', '이 페이지는했다 %f 초로드합니다.');
define('LANG_ADMIN_USER', '아이디');
define('LANG_ADMIN_SUBMITBUT', '제출');
define('LANG_ADMIN_LOGININFO', '관리자 영역을 보려면 로그인');
define('LANG_ADMIN_LOGINFAIL', '실패. 다시 로그 인해보십시오.');
define('LANG_ADMIN_ACCTSUC', '계정 세부 정보가 성공적으로 수정');
define('LANG_ADMIN_LOGINBUT', '로그인');
define('LANG_ADMIN_INCWRITE', '나는 쓸 수 있습니다 /includes 디렉토리. 이것은 보안 위험은, 당신이 반드시 chmod 를 뒤쪽으로 폴더 (권한) 755!');
define('LANG_ADMIN_INSWRITE', '/install 디렉토리가 존재합니다! 이것은 보안 위험은, 당신을 삭제해야 /install 디렉토리!');

define('LANG_AMENU_LOGGEDAS', '로 로그인');
define('LANG_AMENU_ADMINR', '관리자');
define('LANG_AMENU_LOGOUT', '로그아웃');
define('LANG_AMENU_CHGPW', '암호 변경');
define('LANG_AMENU_HOME', '홈');
define('LANG_AMENU_EMAILAD', '이메일 광고');
define('LANG_AMENU_MANADV', '광고주 관리');
define('LANG_AMENU_MANCAM', '캠페인 관리');
define('LANG_AMENU_SITESET', '사이트 설정');
define('LANG_AMENU_CURRSET', '통화 설정');
define('LANG_AMENU_PAYSSET', '지불 설정');
define('LANG_AMENU_ADMACCT', '관리 계정');
define('LANG_AMENU_UPGRADE', '치받이길');
define('LANG_AMENU_HOWTOS', '방법의로');

define('LANG_AMENU_ADWAIT', '대기 중 광고');
define('LANG_AMENU_CLICKSLOG', '아이피의 로그');
define('LANG_AMENU_CLICKSLOG2', '또, 이러한 부정 클릭 등 의심스러운 활동을 감시하는 데 유용합니다.');
define('LANG_AMENU_COUPONS', '쿠폰');
define('LANG_AMENU_MANCOUP', '쿠폰 관리');
define('LANG_AMENU_ADVERTISERS', '광고주');
define('LANG_AMENU_CAMPAIGNS', '캠페인');
define('LANG_AMENU_SETTINGS', '설정');
define('LANG_AMENU_EXTRAS', '부가 기능');

define('LANG_AMENU_DENYIPS', '금지 IP 주소를');

define('LANG_ADMIN_UPGRADEAREA', '면적 다운로드');

define('LANG_ADMIN_PAYSETTINGS', '지불 설정');
define('LANG_ADMIN_PAYENABLECUR', '당신의 통화를 사용하도록 설정해야합니다');
define('LANG_ADMIN_OFFDISABLED', '오프 | 장애인');
define('LANG_ADMIN_ONENABLED', '활성화 |에서');
define('LANG_ADMIN_CURRACCEPTED', '통화 수락함');
define('LANG_ADMIN_CURRENCYNOTE', '*당신은 광고주가 광고주 지역의 통화 옵션을 볼 수 있도록 위에있는 확인란을 선택합니다.');
define('LANG_ADMIN_ECUMONEYNOTE', '중요: 첫째 ECUmoney를 사용하기 전에 판매자 도구 페이지에서 더 많은 정보를 참조하십시오.');
define('LANG_ADMIN_EMAILADDRESS', '이메일 주소');
define('LANG_ADMIN_ACCOUNTNUM', '계정 #');
define('LANG_ADMIN_USERID', '사용자 ID');
define('LANG_ADMIN_HOLDINGNUM', '개최 #');
define('LANG_ADMIN_STORENAME', '스토어 이름');
define('LANG_ADMIN_STOREID', '스토어 ID');
define('LANG_ADMIN_LOGINID', '로그인 ID');
define('LANG_ADMIN_TRANSKEY', '거래 키');
define('LANG_ADMIN_UPDATEBUT', '최신 정보');

define('LANG_ADMIN_GOBACK', '돌아가기');
define('LANG_ADMIN_DELETEBUT', '삭제하다');
define('LANG_ADMIN_STATS', '광고 통계');
define('LANG_ADMIN_DATEJOIN', '날짜 가입');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', '광고주 (자세한하거나 볼 광고를 수정하기 위해 광고주를 클릭)');
define('LANG_ADMIN_NUMOFADS', '# 의 광고');
define('LANG_ADMIN_ADDNEWAD', '광고주에게 새 광고를 추가합니다');
define('LANG_ADMIN_ADDNEWUSER', '신규 광고주 추가');

define('LANG_ADMIN_SITESETTINGS', '사이트 설정');
define('LANG_ADMIN_OPEN', '열기');
define('LANG_ADMIN_CLOSED', '폐장');
define('LANG_ADMIN_IFCLOSED', '*닫혀있다면, 사람들은 계정을 등록할 수 없게됩니다.');
define('LANG_ADMIN_AEMAIL', '관리자 이메일 주소');
define('LANG_ADMIN_SENDFROM', '이메일 주소로 보내기');
define('LANG_ADMIN_ABPATHTO', '사이트에 대한 절대 경로');
define('LANG_ADMIN_SITEURL', '사이트 주소');
define('LANG_ADMIN_SITENAME', '사이트 이름');
define('LANG_ADMIN_SITEKEYWORD', '사이트 키워드');
define('LANG_ADMIN_SITEDESC', '사이트 설명');
define('LANG_ADMIN_DEFAULTLAN', '기본 언어');
define('LANG_ADMIN_DEFAULTCUR', '기본 통화');
define('LANG_ADMIN_DEFAULTTEM', '테마 기본값');
define('LANG_ADMIN_ALLOWREG', '등록 허용');
define('LANG_ADMIN_SHOWMSTATS', '통계보기');

define('LANG_ADMIN_SRYNOADVRS', '죄송 어떤 광고주에게 보내는 이메일을 찾을 수 없습니다!');
define('LANG_ADMIN_SENDTOWHO', '로 보내기');
define('LANG_ADMIN_NUMOFADVRE', '광고 메일의 개수로 전송');
define('LANG_ADMIN_EADVRSNOTE', '당신이 그들을 특별한 거래, 낮은 가격, 더, 지불하지 않은 사람은 상기에 대해 알려주셔서 광고주에게 이메일을 보낼 수 있습니다.');
define('LANG_ADMIN_ALLADVRCOUNT', '모든 광고주 - 계산');
define('LANG_ADMIN_HELLONAME', '안녕하세요');
define('LANG_ADMIN_RECEIVEEMAIL', '당신이 우리와 함께 광고주 계정을 가지고 있기 때문에이 이메일을 보내드립니다.');
define('LANG_ADMIN_SUBSTITUTIONS', '대체 (메시지 본문과 제목 필드에 삽입 사용)');

define('LANG_ADMIN_SERVERD', '서버 세부 정보');
define('LANG_ADMIN_GSVERSION', '버전 검사');
define('LANG_ADMIN_GSNEWS', 'Dijiteol에서 최신 뉴스');
define('LANG_ADMIN_TENANCE', '웹 사이트 유지 보수');

define('LANG_ADMIN_STATSINFO', '통계 (정보)');
define('LANG_ADMIN_MADVERS', '광고주');
define('LANG_ADMIN_MCAMIGS', '캠페인');
define('LANG_ADMIN_MACTADS', '액티브 광고');
define('LANG_ADMIN_MINACTADS', '비활성 광고');
define('LANG_ADMIN_MUNLIADS', '무제한 광고');
define('LANG_ADMIN_MACTCRED', '활성 크레딧');
define('LANG_ADMIN_MINACTCRED', '비활성 크레딧');
define('LANG_ADMIN_ML30DAYSI', '-30 일 총 노출수');
define('LANG_ADMIN_ML30DAYSC', '-30 일 총 클릭수');
define('LANG_ADMIN_ML7DAYSI', '-7 일 총 노출수');
define('LANG_ADMIN_ML7DAYSC', '-7 일 총 클릭수');
define('LANG_ADMIN_MYTIMPS', '-1 일 총 노출수');
define('LANG_ADMIN_MYTDAYS', '-1 일 총 클릭수');
define('LANG_ADMIN_TTIMPS', '오늘의 총 노출수');
define('LANG_ADMIN_TTCLICKS', '오늘 총 클릭수');
define('LANG_ADMIN_LASTCRONDATE', 'Cron은 마지막 날짜');
define('LANG_ADMIN_CRONNEEDED', 'Cron은 오늘 필요합니다!링크를 클릭하십시오');
define('LANG_ADMIN_CRONNONEED', 'Cron은 오늘 필요하지 않습니다.');
define('LANG_ADMIN_ADSWAITING', '승인을 기다리는 광고');
define('LANG_ADMIN_APPROVE', '시인하다');
define('LANG_ADMIN_DISAPPROVE', '불찬성하다');
define('LANG_ADMIN_DISAREASON', '비승인 이유');
define('LANG_ADMIN_PAIDVIA', '통해 지불');
define('LANG_ADMIN_OPTIMIZEDBT1', '데이터베이스 테이블을 최적화');
define('LANG_ADMIN_OPTIMIZEDBT2', '최적화');
define('LANG_ADMIN_OPTIMIZEDBT3', '무료로 사용되지 않는 공간까지 모두 조각난 블록 제거하고 데이터베이스를 빠르게 실행할 수 있도록 설계되었습니다.');

define('LANG_ADMIN_PURHISTORY', '구매 내역 (자신의 광고를 확인하기 전에 그들의 지불을 확인)');

define('LANG_AFORM_EADAPPROVED', '귀하의 광고가 승인되었습니다.');
define('LANG_AFORM_EADDISAPPROVED', '광고가 승인되지 않았습니다.');
define('LANG_AFORM_DBOPTIMIZED', '데이터베이스 테이블 최적화');
define('LANG_AFORM_SITESETTINGS', '사이트 설정 업데이트');
define('LANG_AFORM_PAYSETTINGS', '결제 설정 업데이트');
define('LANG_AFORM_CURSETTINGS', '통화 설정 업데이트');
define('LANG_AFORM_USERDELETED', '사용자 삭제');
define('LANG_AFORM_USERADDED', '사용자 추가');
define('LANG_AFORM_ADDTOUSER', '광고는 사용자에 추가');
define('LANG_AFORM_USERUPDATED', '사용자 업데이트');
define('LANG_AFORM_ADUPDATED', '광고 업데이트');
define('LANG_AFORM_ADDELETED', '광고가 삭제됨');
define('LANG_AFORM_ADADDED', '광고가 추가됨');
define('LANG_AFORM_CAMGNADDED', '캠페인 추가');
define('LANG_AFORM_CAMGNUPDATED', '캠페인 업데이트');
define('LANG_AFORM_CAMGNDELETED', '캠페인 삭제');

define('LANG_AFORM_COUPONDELETED', '쿠폰 삭제');
define('LANG_AFORM_COUPONADDED', '쿠폰 추가');
define('LANG_AFORM_COUPONUPDATED', '쿠폰 업데이트');

define('LANG_ADMIN_CAMGNTITLE', '캠페인 (세부 정보를 수정 또는 광고 코드를 얻을 캠페인을 클릭합니다)');
define('LANG_ADMIN_ADDNEWCAMGN', '새 캠페인 추가');
define('LANG_ADMIN_SHORTDESC', '짧은 설명');
define('LANG_ADMIN_SHORTDESCN', '*광고가 게재 된 캠페인에 대한 간단한 정보 ...');
define('LANG_ADMIN_DEFAULTADN', '사용할 수있는 광고가없는 경우 기본 광고가 표시됩니다.');
define('LANG_ADMIN_DEFAULTADU', '기본 광고 그래픽 URL');
define('LANG_ADMIN_DEFAULTADF', '기본 광고 앞으로의 URL');
define('LANG_ADMIN_LIMITCHARN', '어떤 경우, 텍스트 캐릭터는 중요하지 않습니다 아래의 제한 없습니다.');
define('LANG_ADMIN_LIMITCHARLEFT', '텍스트 문자 제한');
define('LANG_ADMIN_TYPECPI', '노출 당 비용 (CPM)');
define('LANG_ADMIN_TYPECPC', '클릭당 비용 (CPC)');
define('LANG_ADMIN_TYPECPD', '하루 비용 (CPD)');
define('LANG_ADMIN_TYPECPDNOTE', '참고: 이 모든 하루에 한 번씩 자동 또는 수동 cron 작업이 필요합니다.');
define('LANG_ADMIN_ADVIL9', '당신은 얼마나 많은 판매하려는합니까? 9999을 입력하면 무제한 광고를 허용하려는 경우.');
define('LANG_ADMIN_CURRCONVERTER', '환율 계산기');
define('LANG_ADMIN_PRICEINJPY2', 'JPY을 위해 - 금액은 할 수 없습니다 .xx <br /> 가격은 경우 &#165; 90.9270 다음 올바른 형식은 &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', '정확한 광고 크기를 모르십니까? 이 당신을 도울 수 있습니다!');
define('LANG_ADMIN_ROTATESPEED', '광고 회전 속도');
define('LANG_ADMIN_MILISECONDS', '밀리 초');
define('LANG_ADMIN_NOTUSEROTAT', '*떠나 빈 (공백)가 회전을 사용하지하고자하는 경우 주세요.');
define('LANG_ADMIN_MILISECTOSEC', '10000 밀리 초 = 10 초, 30000 밀리 초 = 30 초.');
define('LANG_ADMIN_CONVERTMILLI', '초 밀리 초 변환합니다.');
define('LANG_ADMIN_NUMOFADRUN', '표시 광고의 수');
define('LANG_ADMIN_TOTALNUMAD', '페이지에 게재되는 광고의 최대 개수입니다. 5 *100x100* 광고? 1 *468x60* 광고?<br />왼쪽 빈 경우, 기본값은 입니다 1 광고.');
define('LANG_ADMIN_ADDSPACE', '공간: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', '사이에 공백을 추가 *수평선상의* 광고.');
define('LANG_ADMIN_CSSTEASER', 'CSS (텍스트 광고가 활성화 된 경우 마우스 오버에 광고 문안 설명을 표시)');
define('LANG_ADMIN_CSSTEASERBGI', '배경 이미지');
define('LANG_ADMIN_CSSTEASERBGC', '배경 색상');
define('LANG_ADMIN_CSSTEASERTO', '투명도 / 불투명');
define('LANG_ADMIN_CSSTEASERFC', '글꼴 색상');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (텍스트 광고가 활성화 된 경우 광고 아래 광고 문안 설명을 표시)');
define('LANG_ADMIN_CSSADTEXTSTYLE', '텍스트 스타일');
define('LANG_ADMIN_IFBLANK', '빈 경우');
define('LANG_ADMIN_ADCODES', '광고 코드');
define('LANG_ADMIN_ADCODEOP1', '옵션 1 : PHP 페이지에 PHP 코드');
define('LANG_ADMIN_ADCODEOP2', '옵션 2 : HTML, TPL, CGI, 그리고 PHP뿐만 아니라 : 같은 다른 페이지에 대한 iframe 코드');
define('LANG_ADMIN_ADCODEIFRAME', '(그것은 귀하의 웹 사이트 페이지 오른쪽에 표시됩니다 니가 iframe을 폭 및 높이 크기를 변경해야 할 수도 있습니다.)');
define('LANG_ADMIN_HOWTOCRON', 'Cron은 작업이 귀하의 사이트에서 특정 명령 또는 스크립트를 자동화 할 수 있습니다. 당신은 매일, 주 등 특정 시간에 실행되도록 명령 또는 스크립트를 설정할 수 있습니다.');
define('LANG_ADMIN_HOWTOCRON1', 'Cron 작업은 사용하는 캠페인이 필요합니다');
define('LANG_ADMIN_HOWTOCRON2', 'cron.php 파일은 CPD 캠페인중인 광고를 통해 검색하고 -1 신용 카드 때마다 광고를 뺍합니다.');
define('LANG_ADMIN_HOWTOCRON3', '참고 : Cron의를 사용할 수없는 경우 수동으로 cron.php 파일을 직접 방문 할 수 있습니다');
define('LANG_ADMIN_HOWTOCRON4', '설정 Cron의');
define('LANG_ADMIN_HOWTOCRON5', '하루에 한 번 cron 작업을 실행합니다. 다음은 여기에 사용할 수있는 여러 cron 작업 코드는');
define('LANG_ADMIN_HOWTOCRON6', '귀하의 웹 호스트에 문의하여 Cron의이 허용되어 사용할 수 Cron의 코드 경우 문의해야합니다.');
define('LANG_ADMIN_HOWTOCRON7', 'Cron의 대한 자세한 내용은 다음 페이지를 참조');

define('LANG_ADMIN_PRICEIN', '가격 의');
define('LANG_ADMIN_SPECIALDEAL', '이 특별 표시?');
define('LANG_ADMIN_SPECIALDEAL2', '특별 할인, 한정 상품 등.');
define('LANG_ADMIN_DEFAULTATXT', '기본 광고 문안');
define('LANG_ADMIN_NEWBANCAMPAIGN', '새로운 배너 광고 캠페인을 시작');
define('LANG_ADMIN_NEWTXTCAMPAIGN', '새 텍스트 링크 광고 캠페인을 시작');
define('LANG_ADMIN_CAMPAIGNTODO', '하려면');
define('LANG_ADMIN_CAMPAIGNTODOD', '세부');
define('LANG_ADMIN_CAMPAIGNTODOP', '물가');
define('LANG_ADMIN_CAMPAIGNTODOS', '캠페인 스타일');
define('LANG_ADMIN_CAMPAIGNTODOS2', '광고 스타일');
define('LANG_ADMIN_CAMPAIGNTODOG', '코드 가져 오기');
define('LANG_ADMIN_ADGOESHERE', '광고 설명이 여기에 표시됩니다...');
define('LANG_ADMIN_PREVCAMPSTYLE', '현재 캠페인 스타일을 미리 검토하십시오');
define('LANG_ADMIN_ADSBYABLE', '로그인하여 광고 표시');
define('LANG_ADMIN_ADSBYLABEL', '이름으로 광고');
define('LANG_ADMIN_CAMPEDITSTYLE', '다음 당신은 여기 CSS 스타일을 편집 할 수 있습니다');
define('LANG_ADMIN_CAMPDEFASTYLE', '다음은 여기에 기본 CSS 스타일입니다');
define('LANG_ADMIN_NEWBANAD', '새로운 배너 광고를 시작');
define('LANG_ADMIN_NEWTEXTAD', '새로운 텍스트 링크 광고를 시작');
define('LANG_ADMIN_EXAMPLESTYLE', '예: (아래 사전 설정 스타일을 사용) ');
define('LANG_ADMIN_CLICKIMGSTATS', '광고 통계를 볼 수 있습니다 이미지를 클릭');
define('LANG_ADMIN_CLICKSLOGCRTY', '국가');
define('LANG_ADMIN_COUPONCODEN', '쿠폰 / 할인');
define('LANG_ADMIN_COUPONADDNEW', '새로운 쿠폰을 추가 / 할인');
define('LANG_ADMIN_COUPONEDITC', '쿠폰을 편집합니다 / 할인');
define('LANG_ADMIN_NUMOFC', '사용 총');
define('LANG_ADMIN_LIMITUSES', '제한 사용');
define('LANG_ADMIN_NUMPERUSER', '사용자 당 수');

define('LANG_ADMIN_EDITAD', '광고 내용을 수정');
define('LANG_ADMIN_DENYIPSADD', 'IP 주소 거부');
define('LANG_AFORM_DENYIPADDED', 'IP 주소 추가');
define('LANG_ADMIN_IP', 'IP 주소');
define('LANG_ADMIN_DENYIPS', 'IP 주소의 목록 거부/불법적 인');
define('LANG_AMENU_DENYCTRYS', '국가 거부');
define('LANG_ADMIN_DENYCTRYS', '국가 거부');
define('LANG_ADMIN_COUNTRY', '국가');
define('LANG_ADMIN_CTRY', '국가 거부');
define('LANG_ADMIN_DENYCTRYADD', '국가 거부');
define('LANG_AFORM_DENYCTRYADDED', '국가가 추가되었습니다');

define('LANG_ADMIN_OTHERPAY', '다른 지불 방법');
define('LANG_ADMIN_OTHERPAY2', '결제 옵션을 나열합니다. 예 : 메일 현금, 수표, 비트 코인, 다른.');
define('LANG_ADMIN_OTHERPAY3', '결제 명령어를 여기에 입력하십시오. 예 : 다른 지불 방법 (비트 코인), 오프라인 결제, 다른.');

define('LANG_ADMIN_GCURR1', '활성화 된 경우, 가격은 구글 계산기 당신이 때 관리 지역 로그 기능을 사용하여 업데이트됩니다.');
define('LANG_ADMIN_GCURR2', '경고 : 기본 통화를 선택합니다.');
define('LANG_ADMIN_GCURR3', '또한 자동으로 cron 작업을 수행 할 수 있습니다');

define('LANG_ADMIN_NEWMIXCAMPAIGN', '새로운 혼합 배너 및 텍스트 링크 광고 캠페인을 시작합니다'); // 2014
define('LANG_ADMIN_NEWMIXAD', '새로운 혼합 배너 및 텍스트 링크 광고를보기 시작'); // 2014
define('LANG_ADMIN_NEWSWFAD', '새로운 플래시 (swf) 광고보기 시작'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', '플래시 광고 (.swf)을 허용 하시겠습니까?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', '이미지 파일은 업로드 허용?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', '플래시 (.swf) 파일을 업로드 허용?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', '최대 파일 크기'); // 2014
define('LANG_ADMIN_ALLOWWARNING', '경고: 추천하지 않음'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', '활성화 된 경우, 광고주가 웹 호스트 서버 폴더에 파일을 업로드 할 수 있습니다.'); // 2014
define('LANG_ADMIN_DELETEALERT', '당신은 당신이 삭제 하시겠습니까?'); // 2014
define('LANG_ADMIN_BUTTONALERT', '당신은이 작업을 수행 하시겠습니까?'); // 2014
define('LANG_ADMIN_FLASHADFILE', '플래시 파일이 있어야합니다 .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', '새로운 사용자 정의 광고를 시작합니다'); // 2014
define('LANG_ADMIN_INSERTCADS', '당신이 구글 애드 센스, 또는 광고 네트워크로 사용하고자하는 광고를 삽입합니다. 또한 사용자 정의 광고를 여기에 추가 할 수 있습니다.'); // 2014
define('LANG_ADMIN_CUSTADVERT', '맞춤 광고'); // 2014
?>