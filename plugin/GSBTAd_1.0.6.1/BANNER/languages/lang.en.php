<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: American / English
Date    : Aug 21 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol
------------------------
===================================================
PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Maybe there are other proper words? If so, then please help correcting any translating errors below. Email us with the updated files.
===================================================
*/
define('LANG_TRANSLATION', 'does not guarantee the translation accuracy and efficiency.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'en');
define('LANG_CONT_HTML', 'en');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'home');
define('LANG_MENU_ADVERTISE', 'advertise');
define('LANG_MENU_LOGIN', 'log-in');
define('LANG_MENU_LOGGEDIN', 'Logged In');
define('LANG_MENU_CONTACT', 'contact');

define('LANG_FMENU_HOME', 'home');
define('LANG_FMENU_ADVERTISE', 'advertise with us');
define('LANG_FMENU_LOGIN', 'advertiser log-in');
define('LANG_FMENU_CONTACT', 'contact us');
define('LANG_FMENU_RIGHTS', 'All rights reserved');
define('LANG_FMENU_SCRIPTBY', 'Script by');

define('LANG_MAIN_WELCOME', 'To purchase advertising, please register an account or login if already have one.');
define('LANG_MAIN_ADOPTIONS', 'Advertising Options');
define('LANG_MAIN_CTITLE', 'Campaign');
define('LANG_MAIN_TITLE', 'Title');
define('LANG_MAIN_SIZE', 'Ad Size');
define('LANG_MAIN_TYPE', 'Type');
define('LANG_MAIN_ADTYPEALLOWED', 'Image Ad Type Allowed');
define('LANG_MAIN_ROTATE', 'Rotation');
define('LANG_MAIN_CREDITS', 'Credits');
define('LANG_MAIN_UNLCREDITS', 'Unlimited Credits');
define('LANG_MAIN_TEXT', 'Text');
define('LANG_MAIN_YES', 'Yes');
define('LANG_MAIN_NO', 'No');
define('LANG_MAIN_AVAIL', 'Availability');
define('LANG_MAIN_VIEWPRICES', 'View Prices');
define('LANG_MAIN_ON', 'On');
define('LANG_MAIN_OFF', 'Off');
define('LANG_MAIN_UNLIMITED', 'Unlimited');
define('LANG_MAIN_ETC', 'Prices shown above is the total cost. If a price is not shown for the currency you want, then it is not available for purchase.');
define('LANG_MAIN_ADTYPES', 'CPI- Cost per impression, CPC- Cost per click Through, CPD- Cost per day');
define('LANG_MAIN_PAYMENTMETHOD', 'Payment Method');
define('LANG_MAIN_PRODUCT', 'Product');
define('LANG_MAIN_BACKBUT', 'Back');
define('LANG_MAIN_NEXTBUT', 'Next');
define('LANG_MAIN_PREVBUT', 'Preview');
define('LANG_MAIN_AREA', 'Main Area');
define('LANG_MAIN_LOGOUT', 'Logout');
define('LANG_MAIN_CLOSEWINDOW', 'Close This Window');
define('LANG_MAIN_DATE', 'Date');
define('LANG_MAIN_DATEADDED', 'Date Added');
define('LANG_MAIN_TOTAL', 'Total');
define('LANG_MAIN_IMPRESSIONS', 'Impressions');
define('LANG_MAIN_CLICKS', 'Clicks');
define('LANG_MAIN_DAYS', 'Days');
define('LANG_MAIN_STATUS', 'Status');
define('LANG_MAIN_YOURADS', 'Your Ads');
define('LANG_MAIN_CREMAINING', 'Credits Remaining');
define('LANG_MAIN_CHFDS', 'Click here for detailed stats');
define('LANG_MAIN_CREATEAD', 'Create New Ad');
define('LANG_MAIN_CREATEADAPP', 'Add a new Ad (all ads are subject to admin approval)');
define('LANG_MAIN_ACCEPTCURR', 'Accepted Currencies');
define('LANG_MAIN_PRICE', 'Price');
define('LANG_MAIN_SELECTED', 'You selected');
define('LANG_MAIN_ADGRAPHICURL', 'Ad Graphic URL');
define('LANG_MAIN_ADFORWARDURL', 'Ad Forward URL');
define('LANG_MAIN_ADTEXT', 'Ad Text');
define('LANG_MAIN_WIDTH', 'Width');
define('LANG_MAIN_HEIGHT', 'Height');
define('LANG_MAIN_USERINFO', 'User Information');
define('LANG_MAIN_ADDETAILS', 'Ad Details');
define('LANG_MAIN_VERIDETAILS', 'Please verify all details are correct before continuing');
define('LANG_MAIN_TESTAD', 'Click on the ad above to test - it will open in a new window');
define('LANG_MAIN_NEWUSER', 'New Advertiser');
define('LANG_MAIN_USERLOGIN', 'Advertiser Login');
define('LANG_MAIN_ULOGIN', 'Login');
define('LANG_MAIN_FORGETINFO', 'Forget Login Info?');
define('LANG_MAIN_NAME', 'Name');
define('LANG_MAIN_EMAIL', 'Email Address');
define('LANG_MAIN_PASSWORD', 'Password');
define('LANG_MAIN_REGISTER', 'Register');

define('LANG_MAIN_ADFORWARDURL2', 'Ad Display URL');
define('LANG_MAIN_ADFORWARDURL3', '(Display short URL: domain.com)');
define('LANG_MAIN_HISTORY', 'Purchase History');
define('LANG_MAIN_CODE', 'Code');
define('LANG_MAIN_PERCENT', 'Percent');
define('LANG_MAIN_NUMBERS', 'Numbers');
define('LANG_MAIN_SUBMBUT', 'Submit');
define('LANG_MAIN_ENTERCOUPON', 'Enter coupon code');
define('LANG_MAIN_DISCOUNT', 'Discount');
define('LANG_MAIN_FREE', 'FREE');

define('LANG_MAIN_CTR', 'Click thru rate');

define('LANG_FORM_HELLO', 'Hello');
define('LANG_FORM_LOGIN', 'You can login at');
define('LANG_FORM_REGARDS', 'Regards');
define('LANG_FORM_REMIND2', 'You have recently requested to have your login information emailed to this address.');
define('LANG_FORM_REMIND4', 'If you think that you did not request for this info, please disregard this message and accept our apologies for taking your time.');
define('LANG_FORM_REMIND5', 'Thank you and good day!');
define('LANG_FORM_SIGNUP2', 'Thank you for signing up.');
define('LANG_FORM_SIGNUP3', 'Your Login info is below, store this information in a safe place');
define('LANG_FORM_COMPLETE1', 'As soon as admin approve your advert, it will be activated.');

define('LANG_FORM_ADAPPROVED', 'Your advert has been approved and is now receiving exposures.');
define('LANG_FORM_ADDISAPPROVED', 'Your advert has been disapproved.');
define('LANG_FORM_ADDISAPPROVED1', 'Your advert has been disapproved.');
define('LANG_FORM_ADDISAPPROVED2', 'The reason for disapproval is below'); // :
define('LANG_FORM_ADDISAPPROVED3', 'Please reply to this email with any questions.');
define('LANG_FORM_PAPPROVED', 'Your payment has been approved.');
define('LANG_FORM_PDISAPPROVED', 'Your payment has been disapproved.');

define('LANG_FORM_FORGETEMAIL', 'Your login information has been sent to your email address.');
define('LANG_FORM_REMINDESUBJ', 'Password Reminder');
define('LANG_FORM_NORECEMAIL', 'No record of the email address.');
define('LANG_FORM_NONAME', 'You must enter a contact name!');
define('LANG_FORM_NOPASS', 'You must enter a password!');
define('LANG_FORM_NOEMAIL', 'You must enter an email address!');
define('LANG_FORM_EMAILUSED', 'The email address is already used!');
define('LANG_FORM_SIGNUPESUBJ', 'Sign-Up Confirmation');
define('LANG_FORM_ACCCREATED', 'Your account has been created!<br />Please click here to go to the Advertiser Area and submit your Ad(s).');
define('LANG_FORM_ENTERGRPURL', 'You must enter a graphic ad URL!');
define('LANG_FORM_GRAPICADFILE', 'Graphic ad file must be a .jpg, .gif, or .png image!');
define('LANG_FORM_ENTERFORURL', 'You must enter a forward website URL!');
define('LANG_FORM_GRAPICMWIDTH', 'Graphic Ad exceeds the MAXIMUM width size of');
define('LANG_FORM_GRAPICMHEIGHT', 'Graphic Ad exceeds the MAXIMUM height size of');
define('LANG_FORM_SELECTPAYMENT', 'You must select a payment method!');

define('LANG_PAGE_UNAUTHORIZED', 'You do not have the permission to view this page.');
define('LANG_PAGE_UNAUTHORIZED2', 'You can log-in here');

define('LANG_PAGE_CANCEL', 'REASON: <br />1) You cancelled your purchase. <br />2) Problem with the merchant\'s account. <br />3) Problem with Payment Processor\'s system. <br /><br />If this is an mistake, please contact us. <br />');

define('LANG_PAGE_UFAILED', 'User Login Failed');
define('LANG_PAGE_FAILED', 'The username and password does not match with the ones in the database.<br />Please go back and login again. Please make sure that you have confirmed your email address.<br />If you are still having problems please contact us.');

define('LANG_PAGE_REGDISABLED', 'Sorry, registration has been disabled by the administrator. <br /> If you have any questions or concerns, please contact us.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'Ad Confirmation');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN- New ad awaiting approval!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Click below to view'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'Your payment is completed. Your AD will be reviewed and if there is no problem, it will be approved soon!<br /><br />Thank you!');

define('LANG_PAGE_SENDQRF', 'Send us your Question, Request, or Feedback');
define('LANG_PAGE_MSGSENT', 'Your message has been sent!<br />We will get back to you shortly.<br /><br />');
define('LANG_PAGE_ENTERMSG', 'You must enter a message!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Advertising Contact Email'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'Message');
define('LANG_MAIN_SUBJECT', 'Subject');
define('LANG_MAIN_SEND', 'Send');
define('LANG_MAIN_SHOWIP', 'Your recorded IP');
define('LANG_MAIN_THANKYOU', 'Thank you');

define('LANG_PAGE_STATTITLE', 'Detailed Ad Stats');

define('LANG_PAGE_SELCAMPAIGN', 'Select a Campaign below');
define('LANG_PAGE_ADTYPEALLOWED', 'Image Ad Type Allowed');
define('LANG_PAGE_LIMITCHARLEFT', 'Limited characters left:');
define('LANG_PAGE_NOHTMLCODES', 'No HTML codes! Only Text Words!');

define('LANG_PAGE_PAYBUT', 'Proceed to pay');

define('LANG_PAGE_UPLOADIMAGE', 'Upload image file'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Upload flash (swf) file'); // 2014
define('LANG_PAGE_ADFLASHURL', 'Ad Flash URL'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', 'Administration Login Area');
define('LANG_FMENU_LOADTIME', 'This page took %f seconds to load.');
define('LANG_ADMIN_USER', 'Username');
define('LANG_ADMIN_SUBMITBUT', 'Submit');
define('LANG_ADMIN_LOGININFO', 'Log-in to view Admin Area');
define('LANG_ADMIN_LOGINFAIL', 'Failed. Re-login again.');
define('LANG_ADMIN_ACCTSUC', 'Account details successfully edited');
define('LANG_ADMIN_LOGINBUT', 'Login');
define('LANG_ADMIN_INCWRITE', 'I can write to /includes directory. This is a security risk, you MUST CHMOD the folder (Permissions) back to 755!');
define('LANG_ADMIN_INSWRITE', 'The /install directory exists! This is a security risk, you MUST delete the /install directory!');

define('LANG_AMENU_LOGGEDAS', 'Logged in as');
define('LANG_AMENU_ADMINR', 'ADMINISTRATOR');
define('LANG_AMENU_LOGOUT', 'Logout');
define('LANG_AMENU_CHGPW', 'Change Password');
define('LANG_AMENU_HOME', 'Home');
define('LANG_AMENU_EMAILAD', 'Email Advertisers');
define('LANG_AMENU_MANADV', 'Manage Advertisers');
define('LANG_AMENU_MANCAM', 'Manage Campaigns');
define('LANG_AMENU_SITESET', 'Site Settings');
define('LANG_AMENU_CURRSET', 'Currency Settings');
define('LANG_AMENU_PAYSSET', 'Payment Settings');
define('LANG_AMENU_ADMACCT', 'Admin Account');
define('LANG_AMENU_UPGRADE', 'Upgrade');
define('LANG_AMENU_HOWTOS', 'How to\'s');

define('LANG_AMENU_ADWAIT', 'Ads Waiting');
define('LANG_AMENU_CLICKSLOG', 'Logs of IPs');
define('LANG_AMENU_CLICKSLOG2', 'Useful to watch for suspicious activities such as Click fraud, else.');
define('LANG_AMENU_COUPONS', 'Coupon');
define('LANG_AMENU_MANCOUP', 'Manage Coupons');
define('LANG_AMENU_ADVERTISERS', 'Advertisers');
define('LANG_AMENU_CAMPAIGNS', 'Campaigns');
define('LANG_AMENU_SETTINGS', 'Settings');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Deny/Ban IP Addresses');

define('LANG_ADMIN_UPGRADEAREA', 'Download/Upgrade Area');

define('LANG_ADMIN_PAYSETTINGS', 'Payment Settings');
define('LANG_ADMIN_PAYENABLECUR', 'You must enable currencies in');
define('LANG_ADMIN_OFFDISABLED', 'OFF/DISABLED');
define('LANG_ADMIN_ONENABLED', 'ON/ENABLED');
define('LANG_ADMIN_CURRACCEPTED', 'Currencies Accepted');
define('LANG_ADMIN_CURRENCYNOTE', '*You must check the box(es) above to allow advertisers to see Currencies Options in the Member / Advertiser Area.');
define('LANG_ADMIN_ECUMONEYNOTE', 'IMPORTANT: First see more informations at Merchant Tools page before using ECUmoney.');
define('LANG_ADMIN_EMAILADDRESS', 'Email Address');
define('LANG_ADMIN_ACCOUNTNUM', 'Account #');
define('LANG_ADMIN_USERID', 'User ID');
define('LANG_ADMIN_HOLDINGNUM', 'Holding #');
define('LANG_ADMIN_STORENAME', 'Store Name');
define('LANG_ADMIN_STOREID', 'Store ID');
define('LANG_ADMIN_LOGINID', 'LoginID');
define('LANG_ADMIN_TRANSKEY', 'Transaction Key');
define('LANG_ADMIN_UPDATEBUT', 'Update');

define('LANG_ADMIN_GOBACK', 'Go Back');
define('LANG_ADMIN_DELETEBUT', 'Delete');
define('LANG_ADMIN_STATS', 'Ad Stats');
define('LANG_ADMIN_DATEJOIN', 'Date Joined');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', 'Advertisers (Click on Advertiser to edit details or view ads)');
define('LANG_ADMIN_NUMOFADS', '# of Ads');
define('LANG_ADMIN_ADDNEWAD', 'Add New Ad to Advertiser');
define('LANG_ADMIN_ADDNEWUSER', 'Add New Advertiser / User');

define('LANG_ADMIN_SITESETTINGS', 'Site Settings');
define('LANG_ADMIN_OPEN', 'Open');
define('LANG_ADMIN_CLOSED', 'Closed');
define('LANG_ADMIN_IFCLOSED', '*If closed, then people will not be able to register an account.');
define('LANG_ADMIN_AEMAIL', 'Admin Email Address');
define('LANG_ADMIN_SENDFROM', 'Send from Email Address');
define('LANG_ADMIN_ABPATHTO', 'Absolute Path to site');
define('LANG_ADMIN_SITEURL', 'Site URL');
define('LANG_ADMIN_SITENAME', 'Site Name');
define('LANG_ADMIN_SITEKEYWORD', 'Site Keywords');
define('LANG_ADMIN_SITEDESC', 'Site Description');
define('LANG_ADMIN_DEFAULTLAN', 'Default Language');
define('LANG_ADMIN_DEFAULTCUR', 'Default Currency');
define('LANG_ADMIN_DEFAULTTEM', 'Default Theme');
define('LANG_ADMIN_ALLOWREG', 'Allow Registration');
define('LANG_ADMIN_SHOWMSTATS', 'Show Stats');


define('LANG_ADMIN_SRYNOADVRS', 'Sorry no advertisers were found to send the email to!');
define('LANG_ADMIN_SENDTOWHO', 'Send to');
define('LANG_ADMIN_NUMOFADVRE', 'Number of Advertisers email sent to');
define('LANG_ADMIN_EADVRSNOTE', 'You can send an email to advertisers for letting them know about special deals, low prices, reminding anyone who have not paid, etc.');
define('LANG_ADMIN_ALLADVRCOUNT', 'All Advertisers - Count');
define('LANG_ADMIN_HELLONAME', 'Hello');
define('LANG_ADMIN_RECEIVEEMAIL', 'You are receiving this email because you have an advertiser account with us.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Substitutions (use inserts in the message body and subject field)');

define('LANG_ADMIN_SERVERD', 'Server Details');
define('LANG_ADMIN_GSVERSION', 'Version Checker');
define('LANG_ADMIN_GSNEWS', 'Latest News From Dijiteol');
define('LANG_ADMIN_TENANCE', 'Maintenance');

define('LANG_ADMIN_STATSINFO', 'Statistics (Information)');
define('LANG_ADMIN_MADVERS', 'Advertisers');
define('LANG_ADMIN_MCAMIGS', 'Campaigns');
define('LANG_ADMIN_MACTADS', 'Active Ads');
define('LANG_ADMIN_MINACTADS', 'Inactive Ads');
define('LANG_ADMIN_MUNLIADS', 'Unlimited Ads');
define('LANG_ADMIN_MACTCRED', 'Active Credits');
define('LANG_ADMIN_MINACTCRED', 'Inactive Credits');
define('LANG_ADMIN_ML30DAYSI', '-30 Days Total Imps');
define('LANG_ADMIN_ML30DAYSC', '-30 Days Total Clicks');
define('LANG_ADMIN_ML7DAYSI', '-7 Days Total Imps');
define('LANG_ADMIN_ML7DAYSC', '-7 Days Total Clicks');
define('LANG_ADMIN_MYTIMPS', '-1 Day Total Imps');
define('LANG_ADMIN_MYTDAYS', '-1 Day Total Clicks');
define('LANG_ADMIN_TTIMPS', 'Today Total Imps');
define('LANG_ADMIN_TTCLICKS', 'Today Total Clicks');
define('LANG_ADMIN_LASTCRONDATE', 'Cron Last Date');
define('LANG_ADMIN_CRONNEEDED', 'Cron is needed today! Click on the link');
define('LANG_ADMIN_CRONNONEED', 'Cron is not needed today.');
define('LANG_ADMIN_ADSWAITING', 'Ads Awaiting Approval');
define('LANG_ADMIN_APPROVE', 'APPROVE');
define('LANG_ADMIN_DISAPPROVE', 'DISAPPROVE');
define('LANG_ADMIN_DISAREASON', 'Disapproval Reason');
define('LANG_ADMIN_PAIDVIA', 'Payment via');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Optimize Database Tables');
define('LANG_ADMIN_OPTIMIZEDBT2', 'Optimize');
define('LANG_ADMIN_OPTIMIZEDBT3', 'designed to get rid of all fragmented blocks, free up unused space and make your database run faster.');

define('LANG_ADMIN_PURHISTORY', 'Purchase History (verify their payments before checking their ads)');

define('LANG_AFORM_EADAPPROVED', 'Your ad has been approved');
define('LANG_AFORM_EADDISAPPROVED', 'Your ad has not been approved');
define('LANG_AFORM_DBOPTIMIZED', 'Database Tables Optimized');
define('LANG_AFORM_SITESETTINGS', 'Site Settings Updated');
define('LANG_AFORM_PAYSETTINGS', 'Payment Settings Updated');
define('LANG_AFORM_CURSETTINGS', 'Currency Settings Updated');
define('LANG_AFORM_USERDELETED', 'User Deleted');
define('LANG_AFORM_USERADDED', 'User Added');
define('LANG_AFORM_ADDTOUSER', 'Ad added to user');
define('LANG_AFORM_USERUPDATED', 'User Updated');
define('LANG_AFORM_ADUPDATED', 'Ad Updated');
define('LANG_AFORM_ADDELETED', 'Ad Deleted');
define('LANG_AFORM_ADADDED', 'Ad Added');
define('LANG_AFORM_CAMGNADDED', 'Campaign Added');
define('LANG_AFORM_CAMGNUPDATED', 'Campaign Updated');
define('LANG_AFORM_CAMGNDELETED', 'Campaign Deleted');

define('LANG_AFORM_COUPONDELETED', 'Coupon Deleted');
define('LANG_AFORM_COUPONADDED', 'Coupon Added');
define('LANG_AFORM_COUPONUPDATED', 'Coupon Updated');

define('LANG_ADMIN_CAMGNTITLE', 'Campaigns (Click on Campaign to edit details or get ad codes)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Add New Campaign');
define('LANG_ADMIN_SHORTDESC', 'Short Description');
define('LANG_ADMIN_SHORTDESCN', '*Short info about campaign, where ads shown, ...');
define('LANG_ADMIN_DEFAULTADN', 'If there is no ad available, then a default ad will be shown.');
define('LANG_ADMIN_DEFAULTADU', 'Default Ad Graphic URL');
define('LANG_ADMIN_DEFAULTADF', 'Default Ad Forward URL');
define('LANG_ADMIN_LIMITCHARN', 'If No, Text Characters Limit below don\'t matter.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Limit Text Characters');
define('LANG_ADMIN_TYPECPI', 'Cost per impression');
define('LANG_ADMIN_TYPECPC', 'Cost per click');
define('LANG_ADMIN_TYPECPD', 'Cost per day');
define('LANG_ADMIN_TYPECPDNOTE', 'Note: This require automatic or manual cron job every once a day.');
define('LANG_ADMIN_ADVIL9', 'How many do you want to sell? Type in \'9999\' if you want to allow \'unlimited\' ads.');
define('LANG_ADMIN_CURRCONVERTER', 'Currency Converter');
define('LANG_ADMIN_PRICEINJPY2', 'For JPY- amount cannot have .xx <br /> If the price is &#165; 90.9270 then the correct format is &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'Don\'t know exact ad sizes? This can help you!');
define('LANG_ADMIN_ROTATESPEED', 'Ad Rotation Speed');
define('LANG_ADMIN_MILISECONDS', 'milliseconds');
define('LANG_ADMIN_NOTUSEROTAT', '*Leave BLANK (no space) if you wish not to use rotation.');
define('LANG_ADMIN_MILISECTOSEC', '10000 Milliseconds = 10 Secs, 30000 Milliseconds = 30 Secs.');
define('LANG_ADMIN_CONVERTMILLI', 'Convert Milliseconds to Seconds');
define('LANG_ADMIN_NUMOFADRUN', '# of Ads Shown');
define('LANG_ADMIN_TOTALNUMAD', 'The maximum number of ad(s) shown on the page. 5 *100x100* Ads? 1 *468x60* Ad?<br />If left BLANK, the default is 1 Ad.');
define('LANG_ADMIN_ADDSPACE', 'Space: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Add a space between *horizontal* ads.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Display Ad Text Description on Mouseover if Text Ad Enabled)');
define('LANG_ADMIN_CSSTEASERBGI', 'Background image');
define('LANG_ADMIN_CSSTEASERBGC', 'Background color');
define('LANG_ADMIN_CSSTEASERTO', 'Transparency / Opacity');
define('LANG_ADMIN_CSSTEASERFC', 'Font color');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Display Ad Text Description below ad if Text Ad Enabled)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Text Style');
define('LANG_ADMIN_IFBLANK', 'If blank');
define('LANG_ADMIN_ADCODES', 'Ad Codes');
define('LANG_ADMIN_ADCODEOP1', 'Option 1: php codes for PHP pages');
define('LANG_ADMIN_ADCODEOP2', 'Option 2: iframe code for other pages such as: HTML, TPL, CGI, and PHP as well');
define('LANG_ADMIN_ADCODEIFRAME', '(You might have to change the iframe width & height sizes so it will look right on your website pages.)');
define('LANG_ADMIN_HOWTOCRON', 'Cron jobs allow you to automate certain commands or scripts on your site. You can set a command or script to run at a specific time every day, week, etc.');
define('LANG_ADMIN_HOWTOCRON1', 'Cron job is required for Campaigns using');
define('LANG_ADMIN_HOWTOCRON2', 'The cron.php file will search through active ads with CPD campaigns and subtract -1 credit each ad.');
define('LANG_ADMIN_HOWTOCRON3', 'NOTE: You can manually visit the cron.php file yourself if Cron is not available');
define('LANG_ADMIN_HOWTOCRON4', 'Set up Cron');
define('LANG_ADMIN_HOWTOCRON5', 'Run cron job Once a Day. Below here are several cron job codes you can use');
define('LANG_ADMIN_HOWTOCRON6', 'You should contact your Web Host and ask if Cron is allowed and what Cron codes you can use.');
define('LANG_ADMIN_HOWTOCRON7', 'Read more about Cron');

define('LANG_ADMIN_PRICEIN', 'Price in');
define('LANG_ADMIN_SPECIALDEAL', 'Mark it Special?');
define('LANG_ADMIN_SPECIALDEAL2', 'Special Deal, Limited Time Offer, and so on.');
define('LANG_ADMIN_DEFAULTATXT', 'Default Ad Text');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Start a new banner advertising campaign');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Start a new text link advertising campaign');
define('LANG_ADMIN_CAMPAIGNTODO', 'To Do');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Details');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Prices');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Campaign Styles');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Ad Styles');
define('LANG_ADMIN_CAMPAIGNTODOG', 'Get Codes');
define('LANG_ADMIN_ADGOESHERE', 'Ad description goes here...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Preview Your Current Campaign Style');
define('LANG_ADMIN_ADSBYABLE', 'Show Ads by Sign');
define('LANG_ADMIN_ADSBYLABEL', 'Ads by Label/Name');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Below here you can edit the css style');
define('LANG_ADMIN_CAMPDEFASTYLE', 'Below here is the default css style');
define('LANG_ADMIN_NEWBANAD', 'Start a new banner ad');
define('LANG_ADMIN_NEWTEXTAD', 'Start a new text link ad');
define('LANG_ADMIN_EXAMPLESTYLE', 'Example: (Below use a preset style) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Click on the image to view ad stats');
define('LANG_ADMIN_CLICKSLOGCRTY', 'Country');
define('LANG_ADMIN_COUPONCODEN', 'Coupons / Discounts');
define('LANG_ADMIN_COUPONADDNEW', 'Add new Coupon / Discount');
define('LANG_ADMIN_COUPONEDITC', 'Edit Coupon / Discount');
define('LANG_ADMIN_NUMOFC', '# Used');
define('LANG_ADMIN_LIMITUSES', 'Limit Uses');
define('LANG_ADMIN_NUMPERUSER', '# per User/Advertiser');

define('LANG_ADMIN_EDITAD', 'Edit Ad Details');
define('LANG_ADMIN_DENYIPSADD', 'Deny IP Address');
define('LANG_AFORM_DENYIPADDED', 'IP Address Added');
define('LANG_ADMIN_IP', 'IP Address');
define('LANG_ADMIN_DENYIPS', 'List of IP Addresses Denied/Banned');
define('LANG_AMENU_DENYCTRYS', 'Deny Countries');
define('LANG_ADMIN_DENYCTRYS', 'Deny Countries');
define('LANG_ADMIN_COUNTRY', 'Country');
define('LANG_ADMIN_CTRY', 'Deny Country');
define('LANG_ADMIN_DENYCTRYADD', 'Deny Countries');
define('LANG_AFORM_DENYCTRYADDED', 'Country Added');

define('LANG_ADMIN_OTHERPAY', 'Other Payment Methods');
define('LANG_ADMIN_OTHERPAY2', 'List your payment options. IE: Mail Cash/Check, bitcoin, else.');
define('LANG_ADMIN_OTHERPAY3', 'Enter your payment instruction here. IE: alternate payment methods (bitcoin), off-line payments, else.');

define('LANG_ADMIN_GCURR1', 'If enabled, prices are updated using Google Currency Converter when you log-in Admin Area.');
define('LANG_ADMIN_GCURR2', 'Warning: Select your Default Currency.');
define('LANG_ADMIN_GCURR3', 'You can also do automatic cron job');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Start new mixed banner & text link advertising campaign'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Start new banner & text link ad'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Start new flash (swf) ad'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Allow flash advert (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Allow image files upload?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Allow flash (.swf) files upload?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Maximum File Size'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Warning: Not recommended'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'If enabled, advertiser can upload a file to your web host server folder.'); // 2014
define('LANG_ADMIN_DELETEALERT', 'Are you SURE you want to delete this?'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'Are you SURE you want to do this?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Flash file must be .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Start new custom advert'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Insert any adverts you want to use such as Google AdSense, or any advertising networks / exchanges. You can also add custom advert here.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Custom advert'); // 2014
?>