<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: German / Deutsch
Date    : Aug 21 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
The translation was done with Google Translate.

PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Do you know German Language? If so, then please help correcting any translating errors below. Email us with the updated files.
----------------------------------------------------

Die Übersetzung wurde mit Google Translate gemacht.

Achten Sie bei Editierpanel unten weil
Einige Zeilen enthalten HTML-Codes.

Bitte verwenden Sie spezielle Zeichen-Codes, so dass sie korrekt im Browser angezeigt werden.

Kennen Sie deutsche Sprache? Wenn ja, dann bitte helfen korrigiert jeden Fehler der Übersetzung unten. Mailen Sie uns mit den aktualisierten Dateien.
===================================================
*/
define('LANG_TRANSLATION', 'übernimmt keine Garantie für die Übersetzung Genauigkeit und Effizienz.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'de');
define('LANG_CONT_HTML', 'de');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'haus');
define('LANG_MENU_ADVERTISE', 'werben');
define('LANG_MENU_LOGIN', 'einloggen');
define('LANG_MENU_LOGGEDIN', 'Angemeldet');
define('LANG_MENU_CONTACT', 'kontakt');

define('LANG_FMENU_HOME', 'haus');
define('LANG_FMENU_ADVERTISE', 'werben sie mit uns');
define('LANG_FMENU_LOGIN', 'anbieter login');
define('LANG_FMENU_CONTACT', 'kontaktieren sie uns');
define('LANG_FMENU_RIGHTS', 'Alle rechte vorbehalten');
define('LANG_FMENU_SCRIPTBY', 'Script von');

define('LANG_MAIN_WELCOME', 'Zum Erwerb der Werbung, registrieren Sie sich bitte ein Konto oder loggen Sie sich, wenn bereits eine besitzen.');
define('LANG_MAIN_ADOPTIONS', 'Werbung Optionen');
define('LANG_MAIN_CTITLE', 'Kampagne');
define('LANG_MAIN_TITLE', 'Titel');
define('LANG_MAIN_SIZE', 'Ad-Größe');
define('LANG_MAIN_TYPE', 'Typ');
define('LANG_MAIN_ADTYPEALLOWED', 'Ad-Typ erlaubt');
define('LANG_MAIN_ROTATE', 'Rotieren');
define('LANG_MAIN_CREDITS', 'Kredit');
define('LANG_MAIN_UNLCREDITS', 'Unbegrenzten Kredit');
define('LANG_MAIN_TEXT', 'Text');
define('LANG_MAIN_YES', 'Ja');
define('LANG_MAIN_NO', 'Nicht');
define('LANG_MAIN_AVAIL', 'Verfügbarkeit');
define('LANG_MAIN_VIEWPRICES', 'Siehe Preise');
define('LANG_MAIN_ON', 'Auf');
define('LANG_MAIN_OFF', 'Ab');
define('LANG_MAIN_UNLIMITED', 'Unbegrenzt');
define('LANG_MAIN_ETC', 'Die angegebenen Preise oberhalb der Gesamtkosten. Wenn ein Preis ist nicht für die gewünschte Währung angezeigt, dann ist es nicht zum Kauf angeboten.');
define('LANG_MAIN_ADTYPES', 'CPI- Kosten pro Impression, CPC- Kosten pro Klick durch, CPD- Kosten pro Tag');
define('LANG_MAIN_PAYMENTMETHOD', 'Zahlungsart');
define('LANG_MAIN_PRODUCT', 'Produkt');
define('LANG_MAIN_BACKBUT', 'Zurück');
define('LANG_MAIN_NEXTBUT', 'Nächste');
define('LANG_MAIN_PREVBUT', 'Vorschau');
define('LANG_MAIN_AREA', 'Hauptbereich');
define('LANG_MAIN_LOGOUT', 'Ausloggen');
define('LANG_MAIN_CLOSEWINDOW', 'Fenster Schliessen');
define('LANG_MAIN_DATE', 'Datum');
define('LANG_MAIN_DATEADDED', 'Aufgenommen');
define('LANG_MAIN_TOTAL', 'Summe');
define('LANG_MAIN_IMPRESSIONS', 'Impressionen');
define('LANG_MAIN_CLICKS', 'Klicks');
define('LANG_MAIN_DAYS', 'Tage');
define('LANG_MAIN_STATUS', 'Status');
define('LANG_MAIN_YOURADS', 'Ihre Anzeigen');
define('LANG_MAIN_CREMAINING', 'Verbleibenden Kredite');
define('LANG_MAIN_CHFDS', 'Klicken Sie hier für die Statistik');
define('LANG_MAIN_CREATEAD', 'Erstellung einer neuen Anzeige');
define('LANG_MAIN_CREATEADAPP', 'Fügen Sie ein neues Ad (alle Anzeigen unterliegen dem Admin-Zulassung)');
define('LANG_MAIN_ACCEPTCURR', 'Akzeptierte Währungen');
define('LANG_MAIN_PRICE', 'Preis');
define('LANG_MAIN_SELECTED', 'Sie wählten');
define('LANG_MAIN_ADGRAPHICURL', 'Ad-Grafik-URL');
define('LANG_MAIN_ADFORWARDURL', 'Ad vorn URL');
define('LANG_MAIN_ADTEXT', 'Anzeigentext');
define('LANG_MAIN_WIDTH', 'Breite');
define('LANG_MAIN_HEIGHT', 'Höhe');
define('LANG_MAIN_USERINFO', 'Benutzer-Informationen');
define('LANG_MAIN_ADDETAILS', 'Ad Details');
define('LANG_MAIN_VERIDETAILS', 'Bitte überprüfen Sie alle Angaben sind richtig, bevor Sie fortfahren');
define('LANG_MAIN_TESTAD', 'Klicken Sie auf die Anzeige oben auf dem Prüfstand - es wird in einem neuen Fenster geöffnet');
define('LANG_MAIN_NEWUSER', 'Neue Anbieter');
define('LANG_MAIN_USERLOGIN', 'Inserent Anmelden');
define('LANG_MAIN_ULOGIN', 'Einloggen');
define('LANG_MAIN_FORGETINFO', 'Vergessen loggen Sie sich Informationen?');
define('LANG_MAIN_NAME', 'Name');
define('LANG_MAIN_EMAIL', 'E-Mail-Adresse');
define('LANG_MAIN_PASSWORD', 'Passwort');
define('LANG_MAIN_REGISTER', 'registrieren');

define('LANG_MAIN_ADFORWARDURL2', 'Ad Anzeige-URL');
define('LANG_MAIN_ADFORWARDURL3', '(Anzeige short URL: domain.com)');
define('LANG_MAIN_HISTORY', 'Kauf Geschichte');
define('LANG_MAIN_CODE', 'Kode');
define('LANG_MAIN_PERCENT', 'Prozent');
define('LANG_MAIN_NUMBERS', 'Numbers');
define('LANG_MAIN_SUBMBUT', 'Einreichen');
define('LANG_MAIN_ENTERCOUPON', 'Geben Sie den Coupon-Code');
define('LANG_MAIN_DISCOUNT', 'Rabatt');
define('LANG_MAIN_FREE', 'Kostenlos');

define('LANG_MAIN_CTR', 'Klickrate');

define('LANG_FORM_HELLO', 'Hallo');
define('LANG_FORM_LOGIN', 'Sie können an ein');
define('LANG_FORM_REGARDS', 'Grüße');
define('LANG_FORM_REMIND2', 'Sie haben vor kurzem aufgefordert, Ihre Login-Daten per E-Mail an diese Adresse.');
define('LANG_FORM_REMIND4', 'Wenn Sie denken, dass Sie nicht für diese Informationen anzufordern, ignorieren Sie bitte diese Nachricht und nehmen Sie unsere Entschuldigung für Ihre Zeit.');
define('LANG_FORM_REMIND5', 'Vielen Dank und guten Tag!');
define('LANG_FORM_SIGNUP2', 'Vielen Dank für Ihre Anmeldung.');
define('LANG_FORM_SIGNUP3', 'Ihre Anmeldedaten sind unten, halten Sie diese Informationen an einem sicheren Ort');
define('LANG_FORM_COMPLETE1', 'Sobald Administrator Ihre Anzeige genehmigen, wird es aktiviert werden.');

define('LANG_FORM_ADAPPROVED', 'Ihre Anzeige wurde genehmigt und erhält jetzt Forderungen.');
define('LANG_FORM_ADDISAPPROVED1', 'Ihre Anzeige wurde abgelehnt.');
define('LANG_FORM_ADDISAPPROVED2', 'Der Grund für die Ablehnung ist unten'); // :
define('LANG_FORM_ADDISAPPROVED3', 'Bitte antworten Sie auf diese E-Mail mit Ihren Fragen.');
define('LANG_FORM_PAPPROVED', 'Ihre Zahlung genehmigt worden ist.');
define('LANG_FORM_PDISAPPROVED', 'Ihre Zahlung wurde abgelehnt.');

define('LANG_FORM_FORGETEMAIL', 'Ihre Login-Daten wurde an Ihre E-Mail-Adresse gesendet.');
define('LANG_FORM_REMINDESUBJ', 'Passwort-Erinnerung');
define('LANG_FORM_NORECEMAIL', 'Kein Eintrag der E-Mail-Adresse');
define('LANG_FORM_NONAME', 'Sie müssen den Namen eines Kontakts!');
define('LANG_FORM_NOPASS', 'Sie müssen ein Passwort eingeben');
define('LANG_FORM_NOEMAIL', 'Sie müssen eine E-Mail-Adresse!');
define('LANG_FORM_EMAILUSED', 'Die E-Mail-Adresse wird bereits verwendet!');
define('LANG_FORM_SIGNUPESUBJ', 'Anmelde-Bestätigung');
define('LANG_FORM_ACCCREATED', 'Ihr Konto wurde erstellt! <br /> Bitte klicken Sie hier, um zum Inserent Bereich gehen und übermitteln Sie Ihre Anzeigen');
define('LANG_FORM_ENTERGRPURL', 'Sie müssen eine grafische Anzeigen-URL!');
define('LANG_FORM_GRAPICADFILE', 'Grafik ad-Datei muss eine sein .jpg, .gif, oder .png bild!');
define('LANG_FORM_ENTERFORURL', 'Sie müssen eine vorausschauende Website-URL!');
define('LANG_FORM_GRAPICMWIDTH', 'Grafik ad überschreitet die maximale Größe der Breite');
define('LANG_FORM_GRAPICMHEIGHT', 'Grafik ad überschreitet die maximale Höhe der Größe');
define('LANG_FORM_SELECTPAYMENT', 'Sie müssen eine Zahlungsmethode auswählen!');

define('LANG_PAGE_UNAUTHORIZED', 'Sie haben nicht die Berechtigung, diese Seite zu sehen.');
define('LANG_PAGE_UNAUTHORIZED2', 'Sie können sich hier einloggen');

define('LANG_PAGE_CANCEL', 'GRUND: <br />1) Sie Ihren Kauf storniert. <br />2) Problem mit dem Händler-Konto. <br />3) Problem mit Payment Processor-System. <br /><br />Wenn dies ein Fehler, bitte kontaktieren Sie uns. <br />');

define('LANG_PAGE_UFAILED', 'Login fehlgeschlagen');
define('LANG_PAGE_FAILED', 'Der Benutzername und das Passwort stimmt nicht mit denen in der Datenbank übereinstimmen. <br /> Bitte gehen Sie zurück und wieder einloggen. Bitte stellen Sie sicher, dass Sie Ihre E-Mail-Adresse bestätigt. <br /> Wenn Sie noch Probleme haben, kontaktieren Sie uns.');

define('LANG_PAGE_REGDISABLED', 'Entschuldigung. Die Registrierung wurde durch den Administrator deaktiviert worden. <br /> Wenn Sie Fragen oder Bedenken haben, kontaktieren Sie uns bitte.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'Ad-Bestätigung');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN- New ad wartet auf die Genehmigung!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Klicken Sie unten, um'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'Ihre Zahlung ist abgeschlossen. Ihre AD überprüft werden und, wenn es kein Problem gibt, wird es in Kürze verabschiedet werden! <br /> <br /> Vielen Dank!');

define('LANG_PAGE_SENDQRF', 'Schicken Sie uns Ihre Fragen, Anregungen oder Feedback');
define('LANG_PAGE_MSGSENT', 'Du Nachricht wurde gesendet! <br /> Wir werden umgehend mit Ihnen in Verbindung setzen.<br /><br />');
define('LANG_PAGE_ENTERMSG', 'Sie müssen eine Nachricht!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Werbung per E-Mail Kontakt'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'Nachricht');
define('LANG_MAIN_SUBJECT', 'Gegenstand');
define('LANG_MAIN_SEND', 'Senden');
define('LANG_MAIN_SHOWIP', 'Ihre aufgezeichneten IP');
define('LANG_MAIN_THANKYOU', 'Danke');

define('LANG_PAGE_STATTITLE', 'Detaillierte Statistik Ad');

define('LANG_PAGE_SELCAMPAIGN', 'Wählen Sie eine Kampagne unter');
define('LANG_PAGE_ADTYPEALLOWED', 'Ad-Typ erlaubt');
define('LANG_PAGE_LIMITCHARLEFT', 'Begrenzten Zeichen übrig:');
define('LANG_PAGE_NOHTMLCODES', 'Kein HTML-Codes! Nur Wörter!');

define('LANG_PAGE_PAYBUT', 'Gehen Sie zu zahlen');

define('LANG_PAGE_UPLOADIMAGE', 'Galerie Bilddatei'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Flash-Datei hochladen (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', 'Anzeigen Flash-URL'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', 'Adminbereich');
define('LANG_FMENU_LOADTIME', 'Diese Seite wurde %f Sekunden geladen.');
define('LANG_ADMIN_USER', 'Benutzername');
define('LANG_ADMIN_SUBMITBUT', 'Einreichen');
define('LANG_ADMIN_LOGININFO', 'Melden Sie sich an, um Admin-Bereich');
define('LANG_ADMIN_LOGINFAIL', 'Gescheitert. wieder neu einloggen.');
define('LANG_ADMIN_ACCTSUC', 'Einzelheiten des Kontos erfolgreich bearbeitet');
define('LANG_ADMIN_LOGINBUT', 'Einloggen');
define('LANG_ADMIN_INCWRITE', 'Ich kann schreiben /includes direktorium. Dies ist ein Sicherheitsrisiko, MÜSSEN Sie den Ordner CHMOD (Permissions) auf 755 zurück!');
define('LANG_ADMIN_INSWRITE', 'Der /install direktorium existiert! Dies ist ein Sicherheitsrisiko, MÜSSEN Sie löschen /install direktorium!');

define('LANG_AMENU_LOGGEDAS', 'Angemeldet als');
define('LANG_AMENU_ADMINR', 'ADMINISTRATOR');
define('LANG_AMENU_LOGOUT', 'Austragen');
define('LANG_AMENU_CHGPW', 'Passwort ändern');
define('LANG_AMENU_HOME', 'Haus');
define('LANG_AMENU_EMAILAD', 'E-Mail-Benutzer');
define('LANG_AMENU_MANADV', 'Verwalten Inserenten.');
define('LANG_AMENU_MANCAM', 'Siehe Kampagnen');
define('LANG_AMENU_SITESET', 'Site-Einstellungen');
define('LANG_AMENU_CURRSET', 'Währung Einstellungen');
define('LANG_AMENU_PAYSSET', 'Pay-Einstellungen');
define('LANG_AMENU_ADMACCT', 'Admin-Konto');
define('LANG_AMENU_UPGRADE', 'Upgrade');
define('LANG_AMENU_HOWTOS', 'Wie tos');

define('LANG_AMENU_ADWAIT', 'Warten');
define('LANG_AMENU_CLICKSLOG', 'Logs von IPs');
define('LANG_AMENU_CLICKSLOG2', 'Nützlich, um nach verdächtigen Aktivitäten wie Klickbetrug zu sehen, sonst.');
define('LANG_AMENU_COUPONS', 'Coupons');
define('LANG_AMENU_MANCOUP', 'Verwalten Rabatte');
define('LANG_AMENU_ADVERTISERS', 'Werbetreibende');
define('LANG_AMENU_CAMPAIGNS', 'Kampagnen');
define('LANG_AMENU_SETTINGS', 'Einstellungen');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Verbot von IP-Adressen');

define('LANG_ADMIN_UPGRADEAREA', 'Download/Upgrade Bereich');

define('LANG_ADMIN_PAYSETTINGS', 'Pay-Einstellungen');
define('LANG_ADMIN_PAYENABLECUR', 'Sie müssen in Währungen ermöglichen');
define('LANG_ADMIN_OFFDISABLED', 'AUS/deaktiviert');
define('LANG_ADMIN_ONENABLED', 'AUF/aktivieren');
define('LANG_ADMIN_CURRACCEPTED', 'Akzeptierte Währungen');
define('LANG_ADMIN_CURRENCYNOTE', '*Sie müssen die Box (en) vor, damit Werbekunden die Währungen in den Optionen Mitglied Inserenten Bereich sehen.');
define('LANG_ADMIN_ECUMONEYNOTE', 'WICHTIG: Zunächst finden weitere Informationen unter Händler-Tools Seite, bevor Sie ECUmoney.');
define('LANG_ADMIN_EMAILADDRESS', 'E-Mail Adresse');
define('LANG_ADMIN_ACCOUNTNUM', 'Konto #');
define('LANG_ADMIN_USERID', 'Benutzer-ID');
define('LANG_ADMIN_HOLDINGNUM', 'Besitz #');
define('LANG_ADMIN_STORENAME', 'Shop-Name');
define('LANG_ADMIN_STOREID', 'Shop-ID');
define('LANG_ADMIN_LOGINID', 'LoginID');
define('LANG_ADMIN_TRANSKEY', 'Transaktion Schlüssel');
define('LANG_ADMIN_UPDATEBUT', 'Bericht');

define('LANG_ADMIN_GOBACK', 'Zurück');
define('LANG_ADMIN_DELETEBUT', 'Löschen');
define('LANG_ADMIN_STATS', 'Ad Statistik');
define('LANG_ADMIN_DATEJOIN', 'Beitrittsdatum');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', 'Werbetreibende (Advertiser Klicken Sie auf Details zu bearbeiten oder anzuzeigen ads)');
define('LANG_ADMIN_NUMOFADS', '# von Anzeigen');
define('LANG_ADMIN_ADDNEWAD', 'Hinzufügen neuer Anzeige an Anbieter');
define('LANG_ADMIN_ADDNEWUSER', 'Hinzufügen neuer Benutzer Inserenten');

define('LANG_ADMIN_SITESETTINGS', 'Site-Einstellungen');
define('LANG_ADMIN_OPEN', 'Geöffnet');
define('LANG_ADMIN_CLOSED', 'Geschlossen');
define('LANG_ADMIN_IFCLOSED', '*Wenn geschlossen, dann werden die Menschen nicht in der Lage, einen Account zu registrieren.');
define('LANG_ADMIN_AEMAIL', 'Verwaltungs E-Mail Adresse');
define('LANG_ADMIN_SENDFROM', 'Senden von E-Mail Adresse');
define('LANG_ADMIN_ABPATHTO', 'absoluten Pfad zur Seite');
define('LANG_ADMIN_SITEURL', 'Website-URL');
define('LANG_ADMIN_SITENAME', 'Site Name');
define('LANG_ADMIN_SITEKEYWORD', 'Stichworte Site');
define('LANG_ADMIN_SITEDESC', 'Website-Beschreibung');
define('LANG_ADMIN_DEFAULTLAN', 'Standard Sprache');
define('LANG_ADMIN_DEFAULTCUR', 'Standard Währung');
define('LANG_ADMIN_DEFAULTTEM', 'Standard Thema');
define('LANG_ADMIN_ALLOWREG', 'Die Registrierung');
define('LANG_ADMIN_SHOWMSTATS', 'Statistiken anzeigen');

define('LANG_ADMIN_SRYNOADVRS', 'Sorry keine Werbekunden gefunden wurden, um die E-Mail zu senden!');
define('LANG_ADMIN_SENDTOWHO', 'Per E-Mail');
define('LANG_ADMIN_NUMOFADVRE', 'Anzahl der Nutzer E-Mail an');
define('LANG_ADMIN_EADVRSNOTE', 'Sie können eine E-Mail an Inserenten schicken lassen Sie sie wissen, über spezielle Angebote, niedrige Preise, erinnert wer nicht bezahlt haben, etc.');
define('LANG_ADMIN_ALLADVRCOUNT', 'Alle Werbetreibende - Count');
define('LANG_ADMIN_HELLONAME', 'Hallo');
define('LANG_ADMIN_RECEIVEEMAIL', 'Sie erhalten diese E-Mail, weil Sie ein Inserent Konto bei uns haben.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Auswechslungen (Einsatz Einsätze in den Nachrichtentext und Betreff-Feld)');

define('LANG_ADMIN_SERVERD', 'Server-Daten');
define('LANG_ADMIN_GSVERSION', 'Versions-Checker');
define('LANG_ADMIN_GSNEWS', 'Aktuelle Nachrichten aus Dijiteol');
define('LANG_ADMIN_TENANCE', 'Website-Pflege');

define('LANG_ADMIN_STATSINFO', 'Statistik (Information)');
define('LANG_ADMIN_MADVERS', 'Werbetreibende');
define('LANG_ADMIN_MCAMIGS', 'Kampagnen');
define('LANG_ADMIN_MACTADS', 'Aktiven Anzeigen');
define('LANG_ADMIN_MINACTADS', 'Inaktiven Anzeigen');
define('LANG_ADMIN_MUNLIADS', 'unbegrenzt Anzeigen');
define('LANG_ADMIN_MACTCRED', 'Aktiven Kredit');
define('LANG_ADMIN_MINACTCRED', 'Inaktiven Kredit');
define('LANG_ADMIN_ML30DAYSI', '-30 Tage Insgesamt Imps');
define('LANG_ADMIN_ML30DAYSC', '-30 Tage Insgesamt Klicks');
define('LANG_ADMIN_ML7DAYSI', '-7 Tage Insgesamt Imps');
define('LANG_ADMIN_ML7DAYSC', '-7 Tage Insgesamt Klicks');
define('LANG_ADMIN_MYTIMPS', '-1 Gestrige Imps');
define('LANG_ADMIN_MYTDAYS', '-1 Gestrige Klicks');
define('LANG_ADMIN_TTIMPS', 'Heutige Imps');
define('LANG_ADMIN_TTCLICKS', 'Heutige Klicks');
define('LANG_ADMIN_LASTCRONDATE', 'Cron letzten Tag');
define('LANG_ADMIN_CRONNEEDED', 'Cron ist heute notwendig! Klicken Sie auf den link');
define('LANG_ADMIN_CRONNONEED', 'Cron ist heute nicht ntig.');
define('LANG_ADMIN_ADSWAITING', 'Anzeigen wartet auf die Genehmigung');
define('LANG_ADMIN_APPROVE', 'Billigen');
define('LANG_ADMIN_DISAPPROVE', 'Missbilligen');
define('LANG_ADMIN_DISAREASON', 'Ablehnungsgrund');
define('LANG_ADMIN_PAIDVIA', 'Die Bezahlung per');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Optimieren Datenbanktabellen');
define('LANG_ADMIN_OPTIMIZEDBT2', 'Optimieren');
define('LANG_ADMIN_OPTIMIZEDBT3', 'entwickelt, um loszuwerden, alle fragmentierten Blöcke, Freigabe ungenutzter Raum und machen Sie Ihre Datenbank schneller laufen.');

define('LANG_ADMIN_PURHISTORY', 'Kauf-Historie (überprüfen ihre Zahlungen vor dem Einchecken ihre Anzeigen)');

define('LANG_AFORM_EADAPPROVED', 'Ihre Anzeige wurde genehmigt');
define('LANG_AFORM_EADDISAPPROVED', 'Ihre Anzeige wurde nicht genehmigt');
define('LANG_AFORM_DBOPTIMIZED', 'Datenbanktabellen optimiert');
define('LANG_AFORM_SITESETTINGS', 'Website-Einstellungen aktualisiert');
define('LANG_AFORM_PAYSETTINGS', 'Payment-Einstellungen aktualisiert');
define('LANG_AFORM_CURSETTINGS', 'Währung Einstellungen aktualisiert');
define('LANG_AFORM_USERDELETED', 'Benutzer gelöscht');
define('LANG_AFORM_USERADDED', 'Benutzer hat');
define('LANG_AFORM_ADDTOUSER', 'Ad hinzugefügt, um Anwender');
define('LANG_AFORM_USERUPDATED', 'Benutzer aktualisiert');
define('LANG_AFORM_ADUPDATED', 'Ad aktualisiert');
define('LANG_AFORM_ADDELETED', 'Ad gelöscht');
define('LANG_AFORM_ADADDED', 'Ad Hinzugefügt');
define('LANG_AFORM_CAMGNADDED', 'Hinzugefügt Kampagne');
define('LANG_AFORM_CAMGNUPDATED', 'Kampagne aktualisiert');
define('LANG_AFORM_CAMGNDELETED', 'Kampagne gelöscht');

define('LANG_AFORM_COUPONDELETED', 'Coupon gelöscht');
define('LANG_AFORM_COUPONADDED', 'Coupon hinzu');
define('LANG_AFORM_COUPONUPDATED', 'Coupon aktualisiert');

define('LANG_ADMIN_CAMGNTITLE', 'Kampagnen (Klicken Sie auf Details der Kampagne zu bearbeiten oder sich ad-Codes)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Neue Kampagne hinzufügen');
define('LANG_ADMIN_SHORTDESC', 'Beschreibung');
define('LANG_ADMIN_SHORTDESCN', '*Kurze Infos über Kampagnen, Anzeigen, wo gezeigt, ...');
define('LANG_ADMIN_DEFAULTADN', 'Wenn es keine Anzeige vorhanden, dann eine Standard-Anzeige wird angezeigt.');
define('LANG_ADMIN_DEFAULTADU', 'Standard-Grafik-Anzeige-URL');
define('LANG_ADMIN_DEFAULTADF', 'Standardmäßig ad vorn URL');
define('LANG_ADMIN_LIMITCHARN', 'Wenn Nein, Text Zeichen beschränken unten keine Rolle.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Limit Textzeichen:');
define('LANG_ADMIN_TYPECPI', 'Kosten pro Impression');
define('LANG_ADMIN_TYPECPC', 'Kosten pro Klick');
define('LANG_ADMIN_TYPECPD', 'Kosten pro Tag');
define('LANG_ADMIN_TYPECPDNOTE', 'Hinweis: Diese erfordern die automatische oder manuelle Cron-Job alle einmal am Tag.');
define('LANG_ADMIN_ADVIL9', 'Geben Sie \'9999\', wenn Sie \'unbegrenzt\' Anzeigen zulassen wollen.');
define('LANG_ADMIN_CURRCONVERTER', 'Währungsumrechner');
define('LANG_ADMIN_PRICEINJPY2', 'Für JPY-Betrag kann nicht sein, .xx <br /> Wenn der Preis &#165; 90.9270 dann das richtige Format ist &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'Sie wissen nicht genau ad Größen? Dies kann Ihnen helfen!');
define('LANG_ADMIN_ROTATESPEED', 'Ad Drehgeschwindigkeit');
define('LANG_ADMIN_MILISECONDS', 'Millisekunden');
define('LANG_ADMIN_NOTUSEROTAT', '*Lassen  BLANK  (ohne Leerzeichen), wenn Sie möchten nicht auf Rotation benutzen.');
define('LANG_ADMIN_MILISECTOSEC', '10000 Millisekunden = 10 Sekunden, 30000 Milliseconds = 30 Sekunden.');
define('LANG_ADMIN_CONVERTMILLI', 'Konvertieren Millisekunden bis Sekunden');
define('LANG_ADMIN_NUMOFADRUN', 'Anzahl der Anzeigen geschaltet');
define('LANG_ADMIN_TOTALNUMAD', 'Die maximale Anzahl der Anzeigen auf der Seite angezeigt. 5 *100x100* Anzeigen? 1 *468x60* Anzeige?<br />Wenn leer, wird der Standardwert 1 ad.');
define('LANG_ADMIN_ADDSPACE', 'Raum: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Fügen Sie ein Leerzeichen zwischen horizontalen Anzeigen.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Anzeige Anzeigentext Beschreibung auf Mouseover wenn Textanzeige aktiviert.)');
define('LANG_ADMIN_CSSTEASERBGI', 'Background image');
define('LANG_ADMIN_CSSTEASERBGC', 'Background color');
define('LANG_ADMIN_CSSTEASERTO', 'Transparency / Opacity');
define('LANG_ADMIN_CSSTEASERFC', 'Font color');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Anzeige Anzeigentext Beschreibung unten ad wenn Textanzeige aktiviert)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Textstil');
define('LANG_ADMIN_IFBLANK', 'Wenn leer');
define('LANG_ADMIN_ADCODES', 'Ad-Codes');
define('LANG_ADMIN_ADCODEOP1', 'Option 1: PHP-Code für PHP-Seiten');
define('LANG_ADMIN_ADCODEOP2', 'Option 2: iframe-Code für andere Seiten wie zB: HTML, TPL, CGI, PHP und ebenso');
define('LANG_ADMIN_ADCODEIFRAME', '(Sie müssen *erhöhung* der iframe Breite & Höhe, wenn Sie Größen aktiviert Anzeigentext!)');
define('LANG_ADMIN_HOWTOCRON', 'Cron Jobs können Sie bestimmte Befehle oder Skripte auf Ihrer Site zu automatisieren. Sie können einen Befehl oder ein Skript zu einer bestimmten Zeit laufen jeden Tag, Woche, sonst.');
define('LANG_ADMIN_HOWTOCRON1', 'Cron-Job ist für Kampagnen mit erforderlichen');
define('LANG_ADMIN_HOWTOCRON2', 'Die cron.php Datei wird durch die aktive Anzeigen mit Kampagnen mit CPD Suche und subtrahieren -1 Kredit jede geschaltete Anzeige');
define('LANG_ADMIN_HOWTOCRON3', 'Hinweis: Sie können manuell besuchen Sie die Datei selbst, wenn cron.php Cron ist nicht verfügbar');
define('LANG_ADMIN_HOWTOCRON4', 'Einrichten Cron');
define('LANG_ADMIN_HOWTOCRON5', 'Run cron-Job einmal täglich. Nachfolgend sind hier einige cron-Job-Codes können Sie');
define('LANG_ADMIN_HOWTOCRON6', 'Sie sollten mit Ihrem Web-Host und fragen, ob Cron erlaubt ist und was Cron Codes, die Sie verwenden können.');
define('LANG_ADMIN_HOWTOCRON7', 'Lesen Sie mehr über Cron');

define('LANG_ADMIN_PRICEIN', 'Preis in');
define('LANG_ADMIN_SPECIALDEAL', 'markieren Sie besonders?');
define('LANG_ADMIN_SPECIALDEAL2', 'SSonderangebot, Zeitlich begrenztes Angebot, und so weiter.');
define('LANG_ADMIN_DEFAULTATXT', 'Standard-Anzeigentext');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Starten Sie ein neues Banner-Werbekampagne');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Starten Sie eine neue Text-Link-Werbekampagne');
define('LANG_ADMIN_CAMPAIGNTODO', 'Dazu');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Nähere');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Preise');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Kampagne Stile');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Ad-Stile');
define('LANG_ADMIN_CAMPAIGNTODOG', 'bekommen Codes');
define('LANG_ADMIN_ADGOESHERE', 'Ad-Beschreibung...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Vorschau Ihrer aktuellen Kampagne Stil');
define('LANG_ADMIN_ADSBYABLE', 'Anzeigen ansehen by Registrieren');
define('LANG_ADMIN_ADSBYLABEL', 'Anzeige von Etikett/Name');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Unten hier können Sie den CSS-Stil');
define('LANG_ADMIN_CAMPDEFASTYLE', 'Unten hier ist die Standard-CSS-Stil');
define('LANG_ADMIN_NEWBANAD', 'Starten Sie ein neues Werbebanner');
define('LANG_ADMIN_NEWTEXTAD', 'Starten Sie eine neue Text-Link-Anzeige');
define('LANG_ADMIN_EXAMPLESTYLE', 'Beispiel: (Unter Verwendung einer voreingestellten Stil) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Klicken Sie auf das Bild, um ad stats Blick');
define('LANG_ADMIN_CLICKSLOGCRTY', 'Land');
define('LANG_ADMIN_COUPONCODEN', 'Gutscheine / Rabatte');
define('LANG_ADMIN_COUPONADDNEW', 'Hinzufügen neuer Gutschein / Rabatt');
define('LANG_ADMIN_COUPONEDITC', 'Bearbeiten Coupon / Rabatt');
define('LANG_ADMIN_NUMOFC', '# Gebrauchte');
define('LANG_ADMIN_LIMITUSES', 'Grenze verwendet');
define('LANG_ADMIN_NUMPERUSER', 'Anzahl pro benutzer');

define('LANG_ADMIN_EDITAD', 'Bearbeiten Anzeigendetails');
define('LANG_ADMIN_DENYIPSADD', 'Leugnen IP-Adresse');
define('LANG_AFORM_DENYIPADDED', 'IP Address Added');
define('LANG_ADMIN_IP', 'IP-Adresse');
define('LANG_ADMIN_DENYIPS', 'Liste von IP Adressen verweigert/verboten.');
define('LANG_AMENU_DENYCTRYS', 'Leugnen Ländern');
define('LANG_ADMIN_DENYCTRYS', 'Leugnen Ländern');
define('LANG_ADMIN_COUNTRY', 'Land');
define('LANG_ADMIN_CTRY', 'Leugnen Ländern');
define('LANG_ADMIN_DENYCTRYADD', 'Leugnen Ländern');
define('LANG_AFORM_DENYCTRYADDED', 'Land hinzugefügt');

define('LANG_ADMIN_OTHERPAY', 'Andere Zahlungsmethoden');
define('LANG_ADMIN_OTHERPAY2', 'Listen Sie Ihre Zahlungsoptionen. Beispiele: mail Bargeld, Scheck, Bitcoin, anderes.');
define('LANG_ADMIN_OTHERPAY3', 'Geben Sie Ihren Zahlungsauftrag hier. Beispiele: alternative Zahlungsmethoden (Bitcoin), indirekte Zahlungen, sonst.');

define('LANG_ADMIN_GCURR1', 'Wenn aktiviert, werden die Preise mit Google Währungsumrechnung aktualisiert, wenn Sie Login-Admin-Bereich.');
define('LANG_ADMIN_GCURR2', 'Achtung: Wählen Sie die Standardwährung.');
define('LANG_ADMIN_GCURR3', 'Sie können auch automatische Cron-Job zu tun');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Starten Sie eine neue Misch Banner & Textlink-Werbeaktion'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Starten Sie ein neues Banner & Textlink-Anzeige'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Starten Sie eine neue Flash (swf) Anzeige'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Lassen Sie Flash-Anzeige (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Veröffentlichung von Bilddateien hochladen?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Veröffentlichung Flash (.swf) Dateien hochladen?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Maximale Dateigröße'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Achtung: Nicht zu empfehlen'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'Wenn aktiviert, kann Inserent eine Datei auf Ihren Web-Host-Server-Ordner hochladen.'); // 2014
define('LANG_ADMIN_DELETEALERT', 'Sind Sie sicher, dass Sie diese löschen?'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'Sind Sie sicher, dass Sie das tun wollen?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Flash-Datei muss .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Starten Sie neue benutzerdefinierte Anzeige'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Fügen Sie alle Anzeigen Sie wie Google AdSense oder irgendwelche Werbe-Netzwerke / Austausch verwenden möchten. Sie können auch benutzerdefinierte Anzeige hier hinzufügen.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Benutzerdefinierte Anzeige'); // 2014
?>