<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: Japanese 日本語
Date    : Oct 21, 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Maybe there are other proper words? If so, then please help correcting any translating errors below. Email us with the updated files.

ことなので、以下の製作中時寸前して下さい
一部の行は、HTMLコードが含まれています。

それらが適切にブラウザに表示されるように、特殊文字コードを使用してください。

たぶん、他の適切な言葉があります？もしそうなら、以下のいずれかの変換エラーを訂正する助けてください。更新されたファイルに電子メールをお送りください。
===================================================
*/
define('LANG_TRANSLATION', '翻訳精度と効率を保証するものではありません。');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'en');
define('LANG_CONT_HTML', 'en');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'ホームページ');
define('LANG_MENU_ADVERTISE', 'アドバタイズ');
define('LANG_MENU_LOGIN', 'ログイン');
define('LANG_MENU_LOGGEDIN', 'でログイン');
define('LANG_MENU_CONTACT', '連絡');

define('LANG_FMENU_HOME', 'ホームページ');
define('LANG_FMENU_ADVERTISE', '広告掲載お問合せ');
define('LANG_FMENU_LOGIN', '広告主ログイン');
define('LANG_FMENU_CONTACT', 'お問い合わせ');
define('LANG_FMENU_RIGHTS', '無断複写·転載を禁じます');
define('LANG_FMENU_SCRIPTBY', 'で符号化され');

define('LANG_MAIN_WELCOME', '広告を購入するには、すでにお持ちの場合、アカウントまたはログイン情報を登録してください。');
define('LANG_MAIN_ADOPTIONS', '広告オプション');
define('LANG_MAIN_CTITLE', 'キャンペーン');
define('LANG_MAIN_TITLE', 'タイトル');
define('LANG_MAIN_SIZE', '広告の大きさ');
define('LANG_MAIN_TYPE', '種類');
define('LANG_MAIN_ADTYPEALLOWED', '可イメージ広告の種類');
define('LANG_MAIN_ROTATE', '循環');
define('LANG_MAIN_CREDITS', 'クレジット');
define('LANG_MAIN_UNLCREDITS', 'アンリミテッドクレジット');
define('LANG_MAIN_TEXT', 'テキスト');
define('LANG_MAIN_YES', 'はい');
define('LANG_MAIN_NO', 'ノー');
define('LANG_MAIN_AVAIL', '可用性');
define('LANG_MAIN_VIEWPRICES', '価格を見る');
define('LANG_MAIN_ON', '上の');
define('LANG_MAIN_OFF', 'オフ');
define('LANG_MAIN_UNLIMITED', '無制限の');
define('LANG_MAIN_ETC', '上記価格は、総コストです。価格は希望の通貨のために示されていない場合、それは購入することはできません。');
define('LANG_MAIN_ADTYPES', 'CPI- 印象あたりのコスト, CPC- クリックスルーあたりのコスト, CPD- 一日あたりのコスト');
define('LANG_MAIN_PAYMENTMETHOD', '支払方法');
define('LANG_MAIN_PRODUCT', '製品');
define('LANG_MAIN_BACKBUT', 'バック');
define('LANG_MAIN_NEXTBUT', '次');
define('LANG_MAIN_PREVBUT', 'プレビュー');
define('LANG_MAIN_AREA', 'メインエリア');
define('LANG_MAIN_LOGOUT', 'ログアウト');
define('LANG_MAIN_CLOSEWINDOW', 'このウィンドウを閉じる');
define('LANG_MAIN_DATE', '年代');
define('LANG_MAIN_DATEADDED', '追加された日付');
define('LANG_MAIN_TOTAL', '合計');
define('LANG_MAIN_IMPRESSIONS', '感想');
define('LANG_MAIN_CLICKS', 'クリック数');
define('LANG_MAIN_DAYS', 'デイズ');
define('LANG_MAIN_STATUS', 'ステータス');
define('LANG_MAIN_YOURADS', '広告掲載');
define('LANG_MAIN_CREMAINING', '残りクレジット');
define('LANG_MAIN_CHFDS', '詳細な統計情報はここをクリック');
define('LANG_MAIN_CREATEAD', '新しい広告を作成');
define('LANG_MAIN_CREATEADAPP', '新しい広告を追加 (すべての広告は、管理者の承認の対象となり)');
define('LANG_MAIN_ACCEPTCURR', '受け入れられた通貨');
define('LANG_MAIN_PRICE', '価格');
define('LANG_MAIN_SELECTED', 'あなたが選択した');
define('LANG_MAIN_ADGRAPHICURL', '広告グラフィックURL');
define('LANG_MAIN_ADFORWARDURL', '広告のフォワードURL');
define('LANG_MAIN_ADTEXT', '広告テキスト');
define('LANG_MAIN_WIDTH', '幅');
define('LANG_MAIN_HEIGHT', '高さ');
define('LANG_MAIN_USERINFO', 'ユーザー情報');
define('LANG_MAIN_ADDETAILS', '広告の詳細');
define('LANG_MAIN_VERIDETAILS', 'すべての詳細は、続行する前に正しいことを確認してください');
define('LANG_MAIN_TESTAD', 'テストに上記の広告をクリックしてください - それは新しいウィンドウで開きます');
define('LANG_MAIN_NEWUSER', '新たな広告主');
define('LANG_MAIN_USERLOGIN', '広告主ログイン');
define('LANG_MAIN_ULOGIN', 'ログイン');
define('LANG_MAIN_FORGETINFO', 'ログイン情報忘れる？');
define('LANG_MAIN_NAME', '名前');
define('LANG_MAIN_EMAIL', 'メールアドレス');
define('LANG_MAIN_PASSWORD', 'パスワード');
define('LANG_MAIN_REGISTER', '登録');

define('LANG_MAIN_ADFORWARDURL2', '広告の表示URL');
define('LANG_MAIN_ADFORWARDURL3', '(ショートURLを表示します: domain.com)');
define('LANG_MAIN_HISTORY', '購入履歴');
define('LANG_MAIN_CODE', 'コード');
define('LANG_MAIN_PERCENT', 'パーセント');
define('LANG_MAIN_NUMBERS', '数字');
define('LANG_MAIN_SUBMBUT', '提出する');
define('LANG_MAIN_ENTERCOUPON', 'クーポンコードを入力してください');
define('LANG_MAIN_DISCOUNT', '割引');
define('LANG_MAIN_FREE', 'フリー');

define('LANG_MAIN_CTR', 'スルーレートをクリック');

define('LANG_FORM_HELLO', 'こんにちは');
define('LANG_FORM_LOGIN', 'あなたがでログインすることができます');
define('LANG_FORM_REGARDS', 'よろしく');
define('LANG_FORM_REMIND2', 'あなたは最近、あなたのログイン情報は、このアドレスに電子メールで送信が要求している。');
define('LANG_FORM_REMIND4', 'あなたは、この情報を要求しなかったと思われる場合は、このメッセージを無視して、あなたの時間を割いて私たちの謝罪を受け入れてください。');
define('LANG_FORM_REMIND5', 'あなたと良い一日をありがとう！');
define('LANG_FORM_SIGNUP2', 'サインアップしていただきありがとうございます。');
define('LANG_FORM_SIGNUP3', 'あなたのログイン情報は以下の通りです、安全な場所に、この情報を格納します');
define('LANG_FORM_COMPLETE1', '早く管理あなたの広告を承認するように、それが有効になります。');

define('LANG_FORM_ADAPPROVED', 'あなたの広告が承認されており、現在の曝露を受けている。');
define('LANG_FORM_ADDISAPPROVED1', 'あなたの広告が承認されました。');
define('LANG_FORM_ADDISAPPROVED2', '不承認の理由は以下の通りです'); // :
define('LANG_FORM_ADDISAPPROVED3', 'ご質問がございましたら、このメールに返信してください。');
define('LANG_FORM_PAPPROVED', 'あなたの支払が承認されています。');
define('LANG_FORM_PDISAPPROVED', 'あなたの支払が承認されました。');

define('LANG_FORM_FORGETEMAIL', 'あなたのログイン情報は、あなたのメールアドレスに送信されました。');
define('LANG_FORM_REMINDESUBJ', 'パスワードリマインダー');
define('LANG_FORM_NORECEMAIL', 'メールアドレスの記録はありません。');
define('LANG_FORM_NONAME', 'あなたが連絡先の名前を入力する必要があります！');
define('LANG_FORM_NOPASS', 'パスワードを入力する必要があります！');
define('LANG_FORM_NOEMAIL', 'あなたは、電子メールアドレスを入力する必要があります！');
define('LANG_FORM_EMAILUSED', 'メールアドレスは既に使用されている！');
define('LANG_FORM_SIGNUPESUBJ', '確認サインアップ');
define('LANG_FORM_ACCCREATED', 'あなたのアカウントが作成されました！<br />広告エリアに移動し、あなたの広告を掲載するにはここをクリックしてください。');
define('LANG_FORM_ENTERGRPURL', 'あなたは、グラフィック広告のURLを入力する必要があります！');
define('LANG_FORM_GRAPICADFILE', 'グラフィック広告のファイルがなければなりません .jpg, .gif, or .png 画像!');
define('LANG_FORM_ENTERFORURL', 'あなたが前方のウェブサイトのURLを入力する必要があります！');
define('LANG_FORM_GRAPICMWIDTH', 'グラフィック広告は、最大幅のサイズを超えてい');
define('LANG_FORM_GRAPICMHEIGHT', 'グラフィック広告は、最大高さ寸法を超えている');
define('LANG_FORM_SELECTPAYMENT', 'あなたは、お支払い方法を選択する必要があります！');

define('LANG_PAGE_UNAUTHORIZED', 'このページを表示する権限がありません。');
define('LANG_PAGE_UNAUTHORIZED2', 'あなたがここにログインすることができます');

define('LANG_PAGE_CANCEL', '事由: <br />1) あなたの購入をキャンセルしました。 <br />2) 商人のアカウントに問題がある。 <br />3) 支払い処理のシステムに問題があります。 <br /><br />これが間違いである場合は、ご連絡ください。 <br />');

define('LANG_PAGE_UFAILED', 'ユーザーログインに失敗しました');
define('LANG_PAGE_FAILED', 'ユーザー名とパスワードは、データベース内のものと一致していません。<br />戻って、再度ログインしてください。あなたのメールアドレスを確認していることを確認してください。<br />それでも問題が解決しない場合にはご連絡ください。');

define('LANG_PAGE_REGDISABLED', '申し訳ありませんが、管理者によって登録無効にされています。 <br /> ご質問やご懸念がある場合は、ご連絡ください。');

define('LANG_PAGE_ADCOMFIRMSUBJ', '広告の確認');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: 管理- 新しい広告は、承認を待っている！');
define('LANG_PAGE_ADMINCOMFSUBJ2', '表示するために、次をクリックして'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'お支払いが完了しました。広告が検討され、問題がなければ、それはすぐに承認され！<br /><br />ありがとうございました！');

define('LANG_PAGE_SENDQRF', '私達にあなたの質問、リクエスト、またはフィードバックを送信');
define('LANG_PAGE_MSGSENT', 'あなたのメッセージを送信しました！<br />我々はまもなくあなたに戻ってきます。<br /><br />');
define('LANG_PAGE_ENTERMSG', 'あなたは、メッセージを入力する必要があります！');
define('LANG_PAGE_CONTEMAILSUBJ', '広告連絡先メールアドレス'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'メッセージ');
define('LANG_MAIN_SUBJECT', '主題');
define('LANG_MAIN_SEND', '送り届ける');
define('LANG_MAIN_SHOWIP', 'あなたの記録されたIP');
define('LANG_MAIN_THANKYOU', '有り難う御座います');

define('LANG_PAGE_STATTITLE', '詳しい広告統計');

define('LANG_PAGE_SELCAMPAIGN', '下記のキャンペーンを選択');
define('LANG_PAGE_ADTYPEALLOWED', '可イメージ広告の種類');
define('LANG_PAGE_LIMITCHARLEFT', '限られた文字が残って:');
define('LANG_PAGE_NOHTMLCODES', 'いいえHTMLコード！テキストのみの言葉！');

define('LANG_PAGE_PAYBUT', '支払いに進みます');

define('LANG_PAGE_UPLOADIMAGE', 'イメージファイルをアップロード'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'フラッシュファイルをアップロード (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', '広告フラッシュのURL'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', '管理ログインエリア');
define('LANG_FMENU_LOADTIME', 'このページには、ロードするために%f秒かかった。');
define('LANG_ADMIN_USER', 'ユーザー名');
define('LANG_ADMIN_SUBMITBUT', '同じる');
define('LANG_ADMIN_LOGININFO', '管理エリアを表示するには、ログイン');
define('LANG_ADMIN_LOGINFAIL', '失敗しました。再度、再ログイン.');
define('LANG_ADMIN_ACCTSUC', 'アカウントの詳細を正常に編集しました。');
define('LANG_ADMIN_LOGINBUT', 'ログイン');
define('LANG_ADMIN_INCWRITE', '私は、ディレクトリに書き込むことができます。 これは、セキュリティ上のリスクですが、あなたは755に戻って、フォルダのパーミッションを変更する必要があります！');
define('LANG_ADMIN_INSWRITE', 'インストールディレクトリが存在しています！これは、セキュリティ上のリスクである、インストールディレクトリを削除する必要があります！');

define('LANG_AMENU_LOGGEDAS', 'としてログイン');
define('LANG_AMENU_ADMINR', '管理者');
define('LANG_AMENU_LOGOUT', 'ログアウト');
define('LANG_AMENU_CHGPW', 'パスワードの変更');
define('LANG_AMENU_HOME', '宅');
define('LANG_AMENU_EMAILAD', 'メール広告');
define('LANG_AMENU_MANADV', '広告主を管理する');
define('LANG_AMENU_MANCAM', 'キャンペーンを管理');
define('LANG_AMENU_SITESET', 'サイトの設定');
define('LANG_AMENU_CURRSET', '通貨の設定');
define('LANG_AMENU_PAYSSET', '支払い設定');
define('LANG_AMENU_ADMACCT', '管理者アカウント');
define('LANG_AMENU_UPGRADE', '上り坂');
define('LANG_AMENU_HOWTOS', 'どのようにのへ');

define('LANG_AMENU_ADWAIT', '待機広告');
define('LANG_AMENU_CLICKSLOG', 'IPのログ');
define('LANG_AMENU_CLICKSLOG2', '他に、このようなクリック詐欺などの不審な活動を監視するのに便利。');
define('LANG_AMENU_COUPONS', 'クーポン');
define('LANG_AMENU_MANCOUP', 'クーポンを管理する');
define('LANG_AMENU_ADVERTISERS', '広告主');
define('LANG_AMENU_CAMPAIGNS', 'キャンペーン');
define('LANG_AMENU_SETTINGS', '設定');
define('LANG_AMENU_EXTRAS', 'エクストラ');

define('LANG_AMENU_DENYIPS', 'IPアドレスの禁止');

define('LANG_ADMIN_UPGRADEAREA', 'エリアをダウンロード');

define('LANG_ADMIN_PAYSETTINGS', '支払い設定');
define('LANG_ADMIN_PAYENABLECUR', 'あなたの通貨を有効にする必要があります');
define('LANG_ADMIN_OFFDISABLED', '使用禁止');
define('LANG_ADMIN_ONENABLED', '使用可能');
define('LANG_ADMIN_CURRACCEPTED', '通貨は受け入れ');
define('LANG_ADMIN_CURRENCYNOTE', '*あなたは、広告主が広告エリアに通貨オプションを参照できるようにするため、上記のチェックボックスをオンにしなければなりません。');
define('LANG_ADMIN_ECUMONEYNOTE', '重要：ECUmoneyを使用する前に、マーチャント·ツールページでより多くの情報を参照してくださいまず。');
define('LANG_ADMIN_EMAILADDRESS', 'メールアドレス');
define('LANG_ADMIN_ACCOUNTNUM', 'アカウント番号 #');
define('LANG_ADMIN_USERID', 'ユーザー ID');
define('LANG_ADMIN_HOLDINGNUM', 'ホールディング #');
define('LANG_ADMIN_STORENAME', 'ストア名');
define('LANG_ADMIN_STOREID', 'ストア ID');
define('LANG_ADMIN_LOGINID', 'ログイン ID');
define('LANG_ADMIN_TRANSKEY', 'トランザクションキー');
define('LANG_ADMIN_UPDATEBUT', '修正');

define('LANG_ADMIN_GOBACK', '戻る');
define('LANG_ADMIN_DELETEBUT', '抜かす');
define('LANG_ADMIN_STATS', '広告統計');
define('LANG_ADMIN_DATEJOIN', '登録日付');
define('LANG_ADMIN_IDNUM', 'Id #');
define('LANG_ADMIN_ADEDITVIEW', '広告主 (詳細を編集したり、広告を表示する広告主をクリック)');
define('LANG_ADMIN_NUMOFADS', '広告の数');
define('LANG_ADMIN_ADDNEWAD', '広告主に新しい広告を追加');
define('LANG_ADMIN_ADDNEWUSER', '新しい広告主を追加');

define('LANG_ADMIN_SITESETTINGS', 'サイトの設定');
define('LANG_ADMIN_OPEN', 'オープン');
define('LANG_ADMIN_CLOSED', '閉店');
define('LANG_ADMIN_IFCLOSED', '*閉じている場合は、その後の人がアカウントを登録することができません。');
define('LANG_ADMIN_AEMAIL', '管理用メールアドレス');
define('LANG_ADMIN_SENDFROM', 'メールアドレスから送信');
define('LANG_ADMIN_ABPATHTO', 'サイトへの絶対パス');
define('LANG_ADMIN_SITEURL', 'サイトのURL');
define('LANG_ADMIN_SITENAME', 'サイトの名前');
define('LANG_ADMIN_SITEKEYWORD', 'サイトのキーワード');
define('LANG_ADMIN_SITEDESC', 'サイトの説明');
define('LANG_ADMIN_DEFAULTLAN', 'デフォルト言語');
define('LANG_ADMIN_DEFAULTCUR', 'デフォルト通貨');
define('LANG_ADMIN_DEFAULTTEM', 'デフォルトのテーマ');
define('LANG_ADMIN_ALLOWREG', '登録を許可する');
define('LANG_ADMIN_SHOWMSTATS', '統計を表示する');

define('LANG_ADMIN_SRYNOADVRS', '大変申し訳ございません。広告主は、電子メールを送信するために見つかりませんでした！');
define('LANG_ADMIN_SENDTOWHO', 'へ送る');
define('LANG_ADMIN_NUMOFADVRE', '広告メールの数に送ら');
define('LANG_ADMIN_EADVRSNOTE', 'あなたは彼らが特別割引、低価格、支払っていない思い出さ誰などについて知らせるために広告主にメールを送信することができます.');
define('LANG_ADMIN_ALLADVRCOUNT', 'すべての広告主 - 数に入れる');
define('LANG_ADMIN_HELLONAME', 'こんにちは');
define('LANG_ADMIN_RECEIVEEMAIL', 'あなたが私達と広告主のアカウントを持っているため、このメールを受け取っている。');
define('LANG_ADMIN_SUBSTITUTIONS', '代用 (メッセージ本文と件名フィールドでインサートを使用)');

define('LANG_ADMIN_SERVERD', 'サーバーの詳細');
define('LANG_ADMIN_GSVERSION', 'バージョンチェッカ');
define('LANG_ADMIN_GSNEWS', 'Dijiteolの最新ニュース');
define('LANG_ADMIN_TENANCE', 'サイトのメンテナンス');

define('LANG_ADMIN_STATSINFO', '統計 (情報)');
define('LANG_ADMIN_MADVERS', '広告主');
define('LANG_ADMIN_MCAMIGS', 'キャンペーン');
define('LANG_ADMIN_MACTADS', 'アクティブ広告');
define('LANG_ADMIN_MINACTADS', '非アクティブ広告');
define('LANG_ADMIN_MUNLIADS', '無制限の広告');
define('LANG_ADMIN_MACTCRED', 'アクティブクレジット');
define('LANG_ADMIN_MINACTCRED', '非アクティブクレジット');
define('LANG_ADMIN_ML30DAYSI', '30日間の合計感想過去');
define('LANG_ADMIN_ML30DAYSC', '30日間の合計クリック過去');
define('LANG_ADMIN_ML7DAYSI', '過去7日間合計感想');
define('LANG_ADMIN_ML7DAYSC', '過去7日間合計クリック');
define('LANG_ADMIN_MYTIMPS', '1日合計感想過去');
define('LANG_ADMIN_MYTDAYS', '1日の合計クリック過去');
define('LANG_ADMIN_TTIMPS', '今日合計感想');
define('LANG_ADMIN_TTCLICKS', '今日合計のクリック');
define('LANG_ADMIN_LASTCRONDATE', 'cronの遅い日付');
define('LANG_ADMIN_CRONNEEDED', 'cronのは、今日必要とされる！リンクをクリックして');
define('LANG_ADMIN_CRONNONEED', 'cronのは、今日必要とされていません.');
define('LANG_ADMIN_ADSWAITING', '承認待ちの広告');
define('LANG_ADMIN_APPROVE', '承認する');
define('LANG_ADMIN_DISAPPROVE', '不可とする');
define('LANG_ADMIN_DISAREASON', '不承認の理由');
define('LANG_ADMIN_PAIDVIA', '支払い');
define('LANG_ADMIN_OPTIMIZEDBT1', 'データベーステーブルを最適化する');
define('LANG_ADMIN_OPTIMIZEDBT2', '最適化する');
define('LANG_ADMIN_OPTIMIZEDBT3', '無料の未使用スペースまでのすべての断片化されたブロックを取り除くと、データベースが高速に実行できるように設計。');

define('LANG_ADMIN_PURHISTORY', '購入履歴 (自分の広告をチェックする前に彼らの支払いを確認する)');

define('LANG_AFORM_EADAPPROVED', '広告は承認されています');
define('LANG_AFORM_EADDISAPPROVED', '広告は承認されていない');
define('LANG_AFORM_DBOPTIMIZED', 'データベーステーブルは、最適化された');
define('LANG_AFORM_SITESETTINGS', 'サイトの設定更新');
define('LANG_AFORM_PAYSETTINGS', '支払い設定更新');
define('LANG_AFORM_CURSETTINGS', '通貨設定の更新');
define('LANG_AFORM_USERDELETED', 'ユーザ削除');
define('LANG_AFORM_USERADDED', 'ユーザーが追加しました');
define('LANG_AFORM_ADDTOUSER', '広告は、ユーザに追加された');
define('LANG_AFORM_USERUPDATED', 'ユーザ更新');
define('LANG_AFORM_ADUPDATED', '広告更新');
define('LANG_AFORM_ADDELETED', '広告は、削除された');
define('LANG_AFORM_ADADDED', '広告を追加');
define('LANG_AFORM_CAMGNADDED', 'キャンペーンを追加');
define('LANG_AFORM_CAMGNUPDATED', 'キャンペーン更新');
define('LANG_AFORM_CAMGNDELETED', 'キャンペーンは、削除された');

define('LANG_AFORM_COUPONDELETED', 'クーポンは、削除された');
define('LANG_AFORM_COUPONADDED', 'クーポンが追加されました');
define('LANG_AFORM_COUPONUPDATED', 'クーポン更新');

define('LANG_ADMIN_CAMGNTITLE', 'キャンペーン (詳細を編集または広告コードを取得するにはキャンペーンをクリックします。)');
define('LANG_ADMIN_ADDNEWCAMGN', '新たなキャンペーンを追加');
define('LANG_ADMIN_SHORTDESC', '簡単な説明');
define('LANG_ADMIN_SHORTDESCN', '*広告が示さキャンペーン、他についての短い情報。');
define('LANG_ADMIN_DEFAULTADN', '利用可能な広告がない場合、デフォルト広告が表示されます。');
define('LANG_ADMIN_DEFAULTADU', 'デフォルトの広告グラフィックのURL');
define('LANG_ADMIN_DEFAULTADF', 'デフォルトの広告フォワードURL');
define('LANG_ADMIN_LIMITCHARN', 'ない場合は、テキスト文字は重要ではありません以下制限はありません。');
define('LANG_ADMIN_LIMITCHARLEFT', 'テキスト文字を制限する');
define('LANG_ADMIN_TYPECPI', '印象あたりのコスト');
define('LANG_ADMIN_TYPECPC', '1クリックあたりのコスト');
define('LANG_ADMIN_TYPECPD', '一日あたりのコスト');
define('LANG_ADMIN_TYPECPDNOTE', '注意: これは、1日1回、自動または手動のcronジョブを必要とする。');
define('LANG_ADMIN_ADVIL9', 'あなたはどのように多く売りたいとお考えですか？ 9999を入力して、無制限の広告を許可する場合。');
define('LANG_ADMIN_CURRCONVERTER', '通貨のコンバーター');
define('LANG_ADMIN_PRICEINJPY2', 'JPY - 金額は持つことはできません .xx<br />価格は90.9270円である場合、正しい形式では91円です。');
define('LANG_ADMIN_ADSIZEHELP', '正確な広告サイズを知らない？これはあなたを助けることができる！');
define('LANG_ADMIN_ROTATESPEED', '広告の回転速度');
define('LANG_ADMIN_MILISECONDS', 'ミリセカント');
define('LANG_ADMIN_NOTUSEROTAT', '*のまま空白（スペースなし）を使用すると、回転を使用しないようにしたい場合。を');
define('LANG_ADMIN_MILISECTOSEC', '10000ミリ秒=10秒]、30000ミリ秒= 30秒]。');
define('LANG_ADMIN_CONVERTMILLI', '秒をミリ秒に変換');
define('LANG_ADMIN_NUMOFADRUN', '示される広告の数');
define('LANG_ADMIN_TOTALNUMAD', 'ページに表示される広告の最大数。 5 *100x100* 広告? 1 *468x60* 広告?<br />空白のままにした場合、デフォルトは1広告です。');
define('LANG_ADMIN_ADDSPACE', '空白: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', '水平の広告の間にスペースを追加します。');
define('LANG_ADMIN_CSSTEASER', 'CSS (テキスト広告が有効になっている場合マウスオーバーに広告テキストの説明を表示します。)');
define('LANG_ADMIN_CSSTEASERBGI', '背景画像');
define('LANG_ADMIN_CSSTEASERBGC', '背景色');
define('LANG_ADMIN_CSSTEASERTO', '透明 / 不透明');
define('LANG_ADMIN_CSSTEASERFC', 'フォントの色');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (テキスト広告が有効な場合、広告の下に広告テキストの説明を表示します。)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'テキストスタイル');
define('LANG_ADMIN_IFBLANK', '場合は空白');
define('LANG_ADMIN_ADCODES', '広告コード');
define('LANG_ADMIN_ADCODEOP1', 'オプション 1: PHPページ用のPHPコード');
define('LANG_ADMIN_ADCODEOP2', 'オプションn 2: HTML、TPL、CGIやPHPだけでなく、次のような他のページのためのiframeコード。');
define('LANG_ADMIN_ADCODEIFRAME', '(それはあなたのウェブサイトのページ上で右になりますように、iframeの幅と高さのサイズを変更する必要があります。)');
define('LANG_ADMIN_HOWTOCRON', 'cronジョブは、あなたのサイト上で特定のコマンドやスクリプトを自動化することができます。あなたは、他のすべての日、週、特定の時刻に実行するコマンドまたはスクリプトを設定することができます。');
define('LANG_ADMIN_HOWTOCRON1', 'cronジョブを使用してキャンペーンに必要とされる');
define('LANG_ADMIN_HOWTOCRON2', 'cron.php ファイルには、CPDキャンペーンでアクティブな広告を検索し、-1功績に各広告を差し引きます.');
define('LANG_ADMIN_HOWTOCRON3', '注: cronが利用できない場合は、手動でcron.phpをファイルを自分で訪問することができます。');
define('LANG_ADMIN_HOWTOCRON4', 'セットアップのcron');
define('LANG_ADMIN_HOWTOCRON5', '一日一回のcronジョブを実行します。以下、ここでは、使用できるいくつかのcronジョブのコードです');
define('LANG_ADMIN_HOWTOCRON6', 'あなたのWebホストに連絡してくださいとcronが許可されているし、使用できるもののcronコードかどうか尋ねるべきである。');
define('LANG_ADMIN_HOWTOCRON7', 'クーロンについて詳しく読む');

define('LANG_ADMIN_PRICEIN', 'の価格');
define('LANG_ADMIN_SPECIALDEAL', 'それは特別なマーク？');
define('LANG_ADMIN_SPECIALDEAL2', '特別プラン、期間限定、などなど。');
define('LANG_ADMIN_DEFAULTATXT', 'デフォルトの広告テキスト');
define('LANG_ADMIN_NEWBANCAMPAIGN', '新しいバナー広告キャンペーンを開始します。');
define('LANG_ADMIN_NEWTXTCAMPAIGN', '新しいテキストリンク広告キャンペーンを開始します。');
define('LANG_ADMIN_CAMPAIGNTODO', '行う');
define('LANG_ADMIN_CAMPAIGNTODOD', '細部');
define('LANG_ADMIN_CAMPAIGNTODOP', '物価');
define('LANG_ADMIN_CAMPAIGNTODOS', 'キャンペーンスタイル');
define('LANG_ADMIN_CAMPAIGNTODOS2', '広告スタイル');
define('LANG_ADMIN_CAMPAIGNTODOG', 'コードを取得');
define('LANG_ADMIN_ADGOESHERE', '広告の説明がここに入ります。。。');
define('LANG_ADMIN_PREVCAMPSTYLE', 'あなたの現在のキャンペーンスタイルをプレビュー');
define('LANG_ADMIN_ADSBYABLE', 'サインで広告を表示する');
define('LANG_ADMIN_ADSBYLABEL', 'ラベルまたは名前による広告');
define('LANG_ADMIN_CAMPEDITSTYLE', '以下、ここでは、CSSスタイルを編集することができます');
define('LANG_ADMIN_CAMPDEFASTYLE', '以下、ここでデフォルトのCSSスタイルです');
define('LANG_ADMIN_NEWBANAD', '新しいバナー広告を開始します。');
define('LANG_ADMIN_NEWTEXTAD', '新しいテキストリンク広告を開始します。');
define('LANG_ADMIN_EXAMPLESTYLE', '例：（以下はプリセットスタイルを使用） ');
define('LANG_ADMIN_CLICKIMGSTATS', '広告の統計情報を表示するには画像をクリックして');
define('LANG_ADMIN_CLICKSLOGCRTY', '国');
define('LANG_ADMIN_COUPONCODEN', 'クーポン / 割引');
define('LANG_ADMIN_COUPONADDNEW', '新しいクーポンを追加 / 割引');
define('LANG_ADMIN_COUPONEDITC', 'クーポンを編集 / 割引');
define('LANG_ADMIN_NUMOFC', '# 中古');
define('LANG_ADMIN_LIMITUSES', '使用を制限');
define('LANG_ADMIN_NUMPERUSER', 'ユーザーあたりの数');

define('LANG_ADMIN_EDITAD', '広告の詳細を編集');
define('LANG_ADMIN_DENYIPSADD', 'IPアドレスを拒否');
define('LANG_AFORM_DENYIPADDED', 'IPアドレスが追加された');
define('LANG_ADMIN_IP', 'IPアドレス');
define('LANG_ADMIN_DENYIPS', 'IPアドレスのリストが拒否されました/禁止された');
define('LANG_AMENU_DENYCTRYS', '国を否定');
define('LANG_ADMIN_DENYCTRYS', '国を否定');
define('LANG_ADMIN_COUNTRY', '国');
define('LANG_ADMIN_CTRY', '国を否定');
define('LANG_ADMIN_DENYCTRYADD', '国を否定');
define('LANG_AFORM_DENYCTRYADDED', '国が追加され');

define('LANG_ADMIN_OTHERPAY', '他の支払方法');
define('LANG_ADMIN_OTHERPAY2', 'あなたの支払いオプションを一覧表示します。 例：メール現金、小切手、Bitcoinの、他。');
define('LANG_ADMIN_OTHERPAY3', 'あなたの支払い命令をここに入力します。 例：代替支払方法（Bitcoinの）、オフラインの支払、他。');

define('LANG_ADMIN_GCURR1', '有効にすると、価格がGoogleの通貨コンバータをあなたは時に管理エリアログインインを使用して更新されます。');
define('LANG_ADMIN_GCURR2', '警告：あなたのデフォルトの通貨を選択します。');
define('LANG_ADMIN_GCURR3', 'また、自動cronジョブを行うことができます');

define('LANG_ADMIN_NEWMIXCAMPAIGN', '新しいミックスバナー·テキストリンク広告キャンペーンを開始'); // 2014
define('LANG_ADMIN_NEWMIXAD', '新しいミックスバナー·テキストリンク広告を開始'); // 2014
define('LANG_ADMIN_NEWSWFAD', '新しいFlash（SWF）の広告を開始'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'フラッシュ広告（。swfファイル）を許可しますか？'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', '画像ファイルのアップロードを許可しますか？'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'フラッシュ（。SWF）ファイルのアップロードを許可しますか？'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', '最大ファイル·サイズ'); // 2014
define('LANG_ADMIN_ALLOWWARNING', '警告：推奨しません'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', '有効にすると、広告主は、あなたのWeb​​ホストサーバのフォルダにファイルをアップロードすることができます。'); // 2014
define('LANG_ADMIN_DELETEALERT', 'あなたは、これを削除してもよろしいですか？'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'あなたは本当にこれをしたいですか？'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Flashファイルでなければなりません .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', '新しいカスタムの広告を開始します'); // 2014
define('LANG_ADMIN_INSERTCADS', 'このようなGoogle AdSenseの、または任意の広告ネットワーク/交換として使用する任意の広告を挿入します。また、カスタムの広告をここに追加することができます。'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'カスタム広告'); // 2014
?>