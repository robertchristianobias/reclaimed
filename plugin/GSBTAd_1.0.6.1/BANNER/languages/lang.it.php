<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: Italian
Date    : Aug 21 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
Si prega di fare attenzione durante la modifica in basso, poiché
Alcune linee contengono codici HTML.

Si prega di utilizzare codici di caratteri speciali in modo che siano correttamente visualizzati sul browser.

Forse ci sono altre parole adatte? Se è così, allora per favore aiuto correggere eventuali errori di traduzione di seguito. E-mail con i file aggiornati.
===================================================
PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Maybe there are other proper words? If so, then please help correcting any translating errors below. Email us with the updated files.
===================================================
*/
define('LANG_TRANSLATION', 'does not guarantee the translation accuracy and efficiency.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'it');
define('LANG_CONT_HTML', 'it');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'casa');
define('LANG_MENU_ADVERTISE', 'pubblicità');
define('LANG_MENU_LOGIN', 'accesso');
define('LANG_MENU_LOGGEDIN', 'Connesso');
define('LANG_MENU_CONTACT', 'contatto');

define('LANG_FMENU_HOME', 'casa');
define('LANG_FMENU_ADVERTISE', 'pubblicizzare con noi');
define('LANG_FMENU_LOGIN', 'accesso');
define('LANG_FMENU_CONTACT', 'contattaci');
define('LANG_FMENU_RIGHTS', 'Tutti i diritti riservati');
define('LANG_FMENU_SCRIPTBY', 'Script a cura di');

define('LANG_MAIN_WELCOME', 'Per l\'acquisto di pubblicità, si prega di registrare un account oppure Accedi se già uno.');
define('LANG_MAIN_ADOPTIONS', 'Pubblicità Opzioni');
define('LANG_MAIN_CTITLE', 'Campagna');
define('LANG_MAIN_TITLE', 'Titolo');
define('LANG_MAIN_SIZE', 'Dimensioni');
define('LANG_MAIN_TYPE', 'Tipo');
define('LANG_MAIN_ADTYPEALLOWED', 'Tipo di annuncio ammessi');
define('LANG_MAIN_ROTATE', 'Rotazione');
define('LANG_MAIN_CREDITS', 'Crediti');
define('LANG_MAIN_UNLCREDITS', 'Illimitato Crediti');
define('LANG_MAIN_TEXT', 'Testo');
define('LANG_MAIN_YES', 'Sì');
define('LANG_MAIN_NO', 'No');
define('LANG_MAIN_AVAIL', 'Disponibilità');
define('LANG_MAIN_VIEWPRICES', 'Visualizza Prezzi');
define('LANG_MAIN_ON', 'Su');
define('LANG_MAIN_OFF', 'Spento');
define('LANG_MAIN_UNLIMITED', 'Illimitato');
define('LANG_MAIN_ETC', 'I prezzi indicati qui sopra è il costo totale. Se il prezzo non è indicato per la moneta che si desidera, allora non è disponibile per l\'acquisto.');
define('LANG_MAIN_ADTYPES', 'CPI- Il costo per impressione, CPC- Costo per click-through, CPD- Costo giornaliero');
define('LANG_MAIN_PAYMENTMETHOD', 'Metodo di pagamento');
define('LANG_MAIN_PRODUCT', 'Prodotto');
define('LANG_MAIN_BACKBUT', 'Retro');
define('LANG_MAIN_NEXTBUT', 'Prossimo');
define('LANG_MAIN_PREVBUT', 'Anteprima');
define('LANG_MAIN_AREA', 'Superficie principale');
define('LANG_MAIN_LOGOUT', 'Esci');
define('LANG_MAIN_CLOSEWINDOW', 'Chiudere questa finestra');
define('LANG_MAIN_DATE', 'Data');
define('LANG_MAIN_DATEADDED', 'Data di inserimento');
define('LANG_MAIN_TOTAL', 'Totale');
define('LANG_MAIN_IMPRESSIONS', 'Impressioni');
define('LANG_MAIN_CLICKS', 'Clic');
define('LANG_MAIN_DAYS', 'Giorni');
define('LANG_MAIN_STATUS', 'Stato');
define('LANG_MAIN_YOURADS', 'I tuoi annunci');
define('LANG_MAIN_CREMAINING', 'Crediti rimanenti');
define('LANG_MAIN_CHFDS', 'Clicca qui per statistiche dettagliate');
define('LANG_MAIN_CREATEAD', 'Creare un nuovo annuncio');
define('LANG_MAIN_CREATEADAPP', 'Aggiungere un nuovo annuncio (tutti gli annunci sono soggetti ad approvazione)');
define('LANG_MAIN_ACCEPTCURR', 'Valute accettate');
define('LANG_MAIN_PRICE', 'Prezzo');
define('LANG_MAIN_SELECTED', 'È stato selezionato');
define('LANG_MAIN_ADGRAPHICURL', 'annuncio grafico URL');
define('LANG_MAIN_ADFORWARDURL', 'Annuncio in avanti URL');
define('LANG_MAIN_ADTEXT', 'Testo');
define('LANG_MAIN_WIDTH', 'Larghezza');
define('LANG_MAIN_HEIGHT', 'Altezza');
define('LANG_MAIN_USERINFO', 'Informazioni utente');
define('LANG_MAIN_ADDETAILS', 'Dettagli annuncio');
define('LANG_MAIN_VERIDETAILS', 'Si prega di verificare tutti i dati siano corretti prima di continuare');
define('LANG_MAIN_TESTAD', 'Fare clic su un annuncio di sopra di prova - si aprirà in una nuova finestra');
define('LANG_MAIN_NEWUSER', 'Nuovo Inserzionista');
define('LANG_MAIN_USERLOGIN', 'Accesso utente');
define('LANG_MAIN_ULOGIN', 'Accedere');
define('LANG_MAIN_FORGETINFO', 'Dimenticate le vostre informazioni di accesso?');
define('LANG_MAIN_NAME', 'Nome');
define('LANG_MAIN_EMAIL', 'Indirizzo e-mail');
define('LANG_MAIN_PASSWORD', 'Password');
define('LANG_MAIN_REGISTER', 'Registrati');

define('LANG_MAIN_ADFORWARDURL2', 'Annuncio URL di visualizzazione');
define('LANG_MAIN_ADFORWARDURL3', '(URL di visualizzazione breve: domain.com)');
define('LANG_MAIN_HISTORY', 'Acquisti precedenti');
define('LANG_MAIN_CODE', 'Codice');
define('LANG_MAIN_PERCENT', 'Per cento');
define('LANG_MAIN_NUMBERS', 'Numeri');
define('LANG_MAIN_SUBMBUT', 'Inserire');
define('LANG_MAIN_ENTERCOUPON', 'Inserisci il codice coupon');
define('LANG_MAIN_DISCOUNT', 'Sconto');
define('LANG_MAIN_FREE', 'Libero');

define('LANG_MAIN_CTR', 'Percentuale di clic');

define('LANG_FORM_HELLO', 'Ciao');
define('LANG_FORM_LOGIN', 'È possibile effettuare il login a');
define('LANG_FORM_REGARDS', 'Saluti');
define('LANG_FORM_REMIND2', 'Lei ha recentemente chiesto di avere le informazioni di accesso inviata a questo indirizzo.');
define('LANG_FORM_REMIND4', 'Se si pensa che non hai richiesto per queste informazioni, si prega di ignorare questo messaggio e di accettare le nostre scuse per prendere il vostro tempo.');
define('LANG_FORM_REMIND5', 'Grazie e buona giornata!');
define('LANG_FORM_SIGNUP2', 'Grazie per esserti iscritto.');
define('LANG_FORM_SIGNUP3', 'Le informazioni di accesso è inferiore, memorizzare queste informazioni in un luogo sicuro');
define('LANG_FORM_COMPLETE1', 'Appena amministratore approvare il tuo annuncio, verrà attivato.');

define('LANG_FORM_ADAPPROVED', 'Il tuo annuncio è stato approvato ed è ora ricevendo esposizioni.');
define('LANG_FORM_ADDISAPPROVED1', 'Il tuo annuncio non è stato approvato.');
define('LANG_FORM_ADDISAPPROVED2', 'Il motivo di disapprovazione è al di sotto'); // :
define('LANG_FORM_ADDISAPPROVED3', 'Si prega di rispondere a questa e-mail con tutte le domande.');
define('LANG_FORM_PAPPROVED', 'Il vostro pagamento è stato approvato.');
define('LANG_FORM_PDISAPPROVED', 'Il pagamento non è stato approvato.');

define('LANG_FORM_FORGETEMAIL', 'Le informazioni di accesso è stato inviato al tuo indirizzo email.');
define('LANG_FORM_REMINDESUBJ', 'Password promemoria');
define('LANG_FORM_NORECEMAIL', 'Nessun record dell\'indirizzo email.');
define('LANG_FORM_NONAME', 'È necessario inserire un nome di un contatto!');
define('LANG_FORM_NOPASS', 'È necessario inserire una password!');
define('LANG_FORM_NOEMAIL', 'È necessario inserire un indirizzo e-mail!');
define('LANG_FORM_EMAILUSED', 'L\'indirizzo e-mail è già utilizzato!');
define('LANG_FORM_SIGNUPESUBJ', 'Iscriviti Conferma');
define('LANG_FORM_ACCCREATED', 'Il tuo account è stato creato!<br />Fare clic qui per andare alla zona inserzionista e presentare i tuoi annunci.');
define('LANG_FORM_ENTERGRPURL', 'È necessario inserire un URL grafica annuncio!');
define('LANG_FORM_GRAPICADFILE', 'File annuncio Graphic deve essere un jpg, gif, or png image!');
define('LANG_FORM_ENTERFORURL', 'È necessario inserire un URL del sito Web in avanti!');
define('LANG_FORM_GRAPICMWIDTH', 'Graphic Ad supera la dimensione Larghezza massima del');
define('LANG_FORM_GRAPICMHEIGHT', 'Graphic Ad supera la dimensione Altezza massima della');
define('LANG_FORM_SELECTPAYMENT', 'È necessario selezionare un metodo di pagamento!');

define('LANG_PAGE_UNAUTHORIZED', 'Non hai il permesso di visualizzare questa pagina.');
define('LANG_PAGE_UNAUTHORIZED2', 'Puoi loggarti qui');

define('LANG_PAGE_CANCEL', 'RAGIONE: <br />1) Hai annullato l\'acquisto. <br />2) Problema con il conto del commerciante. <br />3) Problema con sistema di pagamento processore. <br /><br />Se questo è un errore, vi preghiamo di contattarci. <br />');

define('LANG_PAGE_UFAILED', 'Login utente non riuscito');
define('LANG_PAGE_FAILED', 'Il nome utente e la password non coincide con quelle del database.<br />Si prega di tornare indietro e rifare il login. Assicurati di aver confermato il tuo indirizzo e-mail.<br />Se avete ancora problemi, contattateci.');

define('LANG_PAGE_REGDISABLED', 'Spiacente, la registrazione è stata disabilitata dall\'amministratore. <br /> Se avete domande o dubbi, non esitate a contattarci.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'Annuncio Conferma');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN- Nuovo annuncio in attesa di approvazione!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Clicca sotto per vedere'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'Il tuo pagamento è stato completato. Il tuo annuncio sarà riesaminata e, se non c\'è nessun problema, sarà approvato presto!<br /><br />Grazie!');

define('LANG_PAGE_SENDQRF', 'Inviaci la tua domanda, richiesta, o feedback');
define('LANG_PAGE_MSGSENT', 'Il tuo messaggio è stato inviato!<br />Ti risponderemo al più presto.<br /><br />');
define('LANG_PAGE_ENTERMSG', 'Devi inserire un messaggio!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Pubblicità Contatto Email'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'Messaggio');
define('LANG_MAIN_SUBJECT', 'Oggetto');
define('LANG_MAIN_SEND', 'Invia');
define('LANG_MAIN_SHOWIP', 'Il tuo IP registrati');
define('LANG_MAIN_THANKYOU', 'Grazie');

define('LANG_PAGE_STATTITLE', 'Statistiche dettagliate degli annunci');

define('LANG_PAGE_SELCAMPAIGN', 'Seleziona una campagna di sotto');
define('LANG_PAGE_ADTYPEALLOWED', 'Tipo di annuncio ammessi');
define('LANG_PAGE_LIMITCHARLEFT', 'Caratteri limitato a sinistra:');
define('LANG_PAGE_NOHTMLCODES', 'No HTML codici! Solo testo di parole!');

define('LANG_PAGE_PAYBUT', 'Procedere al pagamento');

define('LANG_PAGE_UPLOADIMAGE', 'Carica file immagine'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Carica file flash (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', 'Annuncio URL del flash'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', 'Amministrazione zona');
define('LANG_FMENU_LOADTIME', 'Questa pagina ha avuto %f secondi per caricare.');
define('LANG_ADMIN_USER', 'Nome utente');
define('LANG_ADMIN_SUBMITBUT', 'Invia');
define('LANG_ADMIN_LOGININFO', 'Accedi per visualizzare Area Amministrazione');
define('LANG_ADMIN_LOGINFAIL', 'Impossibile. Provare per accedere di nuovo.');
define('LANG_ADMIN_ACCTSUC', 'Dettagli Account curato con successo');
define('LANG_ADMIN_LOGINBUT', 'Accedere');
define('LANG_ADMIN_INCWRITE', 'Sono in grado di scrivere a /includes cartella. Questo è un rischio sicurezza, è necessario CHMOD della cartella (permessi) di nuovo a 755!');
define('LANG_ADMIN_INSWRITE', 'La directory / install esiste! Questo è un rischio per la sicurezza, è necessario eliminare la cartella di installazione!');

define('LANG_AMENU_LOGGEDAS', 'Collegato come');
define('LANG_AMENU_ADMINR', 'AMMINISTRATORE');
define('LANG_AMENU_LOGOUT', 'Esci');
define('LANG_AMENU_CHGPW', 'Modificare la password');
define('LANG_AMENU_HOME', 'Casa');
define('LANG_AMENU_EMAILAD', 'Email inserzionisti');
define('LANG_AMENU_MANADV', 'Gestire gli inserzionisti');
define('LANG_AMENU_MANCAM', 'Gestire le campagne');
define('LANG_AMENU_SITESET', 'Impostazioni sito');
define('LANG_AMENU_CURRSET', 'Valuta Impostazioni');
define('LANG_AMENU_PAYSSET', 'Pagamenti istituito');
define('LANG_AMENU_ADMACCT', 'Conto');
define('LANG_AMENU_UPGRADE', 'Aalita');
define('LANG_AMENU_HOWTOS', 'Come tos');

define('LANG_AMENU_ADWAIT', 'In attesa');
define('LANG_AMENU_CLICKSLOG', 'Logs degli IPs');
define('LANG_AMENU_CLICKSLOG2', 'Utile guardare la ricerca di attività sospette come il click fraud, altro.');
define('LANG_AMENU_COUPONS', 'Buoni');
define('LANG_AMENU_MANCOUP', 'Gestire i Buoni');
define('LANG_AMENU_ADVERTISERS', 'Gli inserzionisti');
define('LANG_AMENU_CAMPAIGNS', 'Campagne');
define('LANG_AMENU_SETTINGS', 'Impostazioni');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Nega / Ban indirizzi IP');

define('LANG_ADMIN_UPGRADEAREA', 'Download/Aggiornamento Area');

define('LANG_ADMIN_PAYSETTINGS', 'Impostazioni di pagamento');
define('LANG_ADMIN_PAYENABLECUR', 'È necessario attivare valute');
define('LANG_ADMIN_OFFDISABLED', 'Off/disabili');
define('LANG_ADMIN_ONENABLED', 'On/abilitato');
define('LANG_ADMIN_CURRACCEPTED', 'Valute accettate');
define('LANG_ADMIN_CURRENCYNOTE', '*È necessario controllare le caselle di cui sopra per consentire agli inserzionisti di vedere Valute Opzioni nella zona di membri inserzionista.');
define('LANG_ADMIN_ECUMONEYNOTE', 'IMPORTANTE: In primo luogo avere maggiori informazioni alla pagina Strumenti per commercianti prima di utilizzare ECUmoney.');
define('LANG_ADMIN_EMAILADDRESS', 'Indirizzo e-mail');
define('LANG_ADMIN_ACCOUNTNUM', 'Conto #');
define('LANG_ADMIN_USERID', 'ID utente');
define('LANG_ADMIN_HOLDINGNUM', 'premuto #');
define('LANG_ADMIN_STORENAME', 'Nome del Negozio');
define('LANG_ADMIN_STOREID', 'ID del negozio');
define('LANG_ADMIN_LOGINID', 'LoginID');
define('LANG_ADMIN_TRANSKEY', 'Chiave di transazione');
define('LANG_ADMIN_UPDATEBUT', 'Aggiornamento');

define('LANG_ADMIN_GOBACK', 'Torna Indietro');
define('LANG_ADMIN_DELETEBUT', 'Elimina');
define('LANG_ADMIN_STATS', 'annunci gratuiti');
define('LANG_ADMIN_DATEJOIN', 'Data riunite');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', 'Gli inserzionisti (Clicca sul inserzionista per modificare i dettagli o annunci di vista)');
define('LANG_ADMIN_NUMOFADS', '# di annunci');
define('LANG_ADMIN_ADDNEWAD', 'Aggiungi nuovo annuncio per inserzionista');
define('LANG_ADMIN_ADDNEWUSER', 'Aggiungi un nuovo inserzionista utente');

define('LANG_ADMIN_SITESETTINGS', 'Impostazioni sito');
define('LANG_ADMIN_OPEN', 'Aperto');
define('LANG_ADMIN_CLOSED', 'Chiuso');
define('LANG_ADMIN_IFCLOSED', '*Se chiuso, allora la gente non sarà in grado di registrare un account.');
define('LANG_ADMIN_AEMAIL', 'Indirizzo e-mail Admin');
define('LANG_ADMIN_SENDFROM', 'Il testo da Indirizzo e-mail');
define('LANG_ADMIN_ABPATHTO', 'Percorso assoluto al sito');
define('LANG_ADMIN_SITEURL', 'URL del sito');
define('LANG_ADMIN_SITENAME', 'Nome sito');
define('LANG_ADMIN_SITEKEYWORD', 'Parole chiave del sito');
define('LANG_ADMIN_SITEDESC', 'Descrizione del sito');
define('LANG_ADMIN_DEFAULTLAN', 'Lingua predefinita');
define('LANG_ADMIN_DEFAULTCUR', 'Valuta predefinita');
define('LANG_ADMIN_DEFAULTTEM', 'Tema predefinito');
define('LANG_ADMIN_ALLOWREG', 'Consentire la registrazione');
define('LANG_ADMIN_SHOWMSTATS', 'Visualizza statistiche');

define('LANG_ADMIN_SRYNOADVRS', 'Spiacente, non sono stati trovati gli inserzionisti di inviare l\'email a!');
define('LANG_ADMIN_SENDTOWHO', 'Invia a');
define('LANG_ADMIN_NUMOFADVRE', 'Numero di email inviate a inserzionisti');
define('LANG_ADMIN_EADVRSNOTE', 'È possibile inviare una e-mail agli inserzionisti per far loro conoscere le offerte speciali, prezzi bassi, ricordando a tutti coloro che non hanno pagato, ecc');
define('LANG_ADMIN_ALLADVRCOUNT', 'Tutti gli inserzionisti - conteggio');
define('LANG_ADMIN_HELLONAME', 'Ciao');
define('LANG_ADMIN_RECEIVEEMAIL', 'Hai ricevuto questa email perché hai un conto inserzionista con noi.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Sostituzioni (inserti in uso nel corpo del messaggio e il campo soggetto)');

define('LANG_ADMIN_SERVERD', 'Dettagli del server');
define('LANG_ADMIN_GSVERSION', 'Controllo della versione');
define('LANG_ADMIN_GSNEWS', 'Ultime notizie dalla Dijiteol');
define('LANG_ADMIN_TENANCE', 'Manutenzione');

define('LANG_ADMIN_STATSINFO', 'Statistiche (Informazioni)');
define('LANG_ADMIN_MADVERS', 'Inserzionisti');
define('LANG_ADMIN_MCAMIGS', 'Campagne');
define('LANG_ADMIN_MACTADS', 'Attivi annunci');
define('LANG_ADMIN_MINACTADS', 'Inattivo annunci');
define('LANG_ADMIN_MUNLIADS', 'numero illimitato di annunci');
define('LANG_ADMIN_MACTCRED', 'Attivi crediti');
define('LANG_ADMIN_MINACTCRED', 'Inattivo crediti');
define('LANG_ADMIN_ML30DAYSI', '-30 Giorni Impressioni');
define('LANG_ADMIN_ML30DAYSC', '-30 Giorni clic');
define('LANG_ADMIN_ML7DAYSI', '-7 Giorni Impressioni');
define('LANG_ADMIN_ML7DAYSC', '-7 Giorni clic');
define('LANG_ADMIN_MYTIMPS', '-1 Ieri totale impressioni');
define('LANG_ADMIN_MYTDAYS', '-1 Ieri totale di clic');
define('LANG_ADMIN_TTIMPS', '1 Giorni Impressioni');
define('LANG_ADMIN_TTCLICKS', '1 Giorni clic');
define('LANG_ADMIN_LASTCRONDATE', 'Cron ultima data');
define('LANG_ADMIN_CRONNEEDED', 'Cron è necessario oggi! Clicca sul link');
define('LANG_ADMIN_CRONNONEED', 'Cron non è necessario oggi.');
define('LANG_ADMIN_ADSWAITING', 'Annunci attesa di approvazione');
define('LANG_ADMIN_APPROVE', 'APPROVAZIONE');
define('LANG_ADMIN_DISAPPROVE', 'DISAPPROVARE');
define('LANG_ADMIN_DISAREASON', 'Disapprovazione ragione');
define('LANG_ADMIN_PAIDVIA', 'Pagamento tramite');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Ottimizzare le tabelle del database');
define('LANG_ADMIN_OPTIMIZEDBT2', 'ottimizzare');
define('LANG_ADMIN_OPTIMIZEDBT3', 'progettato per sbarazzarsi di tutti i blocchi frammentato, liberare spazio inutilizzato e fanno funzionare il tuo database più veloce.');

define('LANG_ADMIN_PURHISTORY', 'Acquisti precedenti (verificare i pagamenti prima di controllare i propri annunci)');

define('LANG_AFORM_EADAPPROVED', 'Il suo annuncio è stato approvato');
define('LANG_AFORM_EADDISAPPROVED', 'L\'annuncio non è stato approvato');
define('LANG_AFORM_DBOPTIMIZED', 'Tabelle di database ottimizzato');
define('LANG_AFORM_SITESETTINGS', 'Impostazioni sito Aggiornato');
define('LANG_AFORM_PAYSETTINGS', 'Impostazioni di pagamento aggiornate');
define('LANG_AFORM_CURSETTINGS', 'Impostazioni di valuta aggiornati');
define('LANG_AFORM_USERDELETED', 'Utente eliminato');
define('LANG_AFORM_USERADDED', 'Utente Aggiunto');
define('LANG_AFORM_ADDTOUSER', 'Annunci aggiunto per l\'utente');
define('LANG_AFORM_USERUPDATED', 'Utente Aggiornato');
define('LANG_AFORM_ADUPDATED', 'Annunci Aggiornato');
define('LANG_AFORM_ADDELETED', 'Annunci cancellati');
define('LANG_AFORM_ADADDED', 'Annunci Aggiunto');
define('LANG_AFORM_CAMGNADDED', 'Aggiunto campagna');
define('LANG_AFORM_CAMGNUPDATED', 'Campagna Aggiornato');
define('LANG_AFORM_CAMGNDELETED', 'Campagna eliminata');

define('LANG_AFORM_COUPONDELETED', 'Coupon eliminati');
define('LANG_AFORM_COUPONADDED', 'Coupon aggiunto');
define('LANG_AFORM_COUPONUPDATED', 'Cedola aggiornato');

define('LANG_ADMIN_CAMGNTITLE', 'Campagne (Clicca sulla campagna per modificare i dettagli o ottenere i codici di annunci)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Aggiungi nuova campagna');
define('LANG_ADMIN_SHORTDESC', 'Breve descrizione');
define('LANG_ADMIN_SHORTDESCN', '*Brevi informazioni campagna, dove gli annunci visualizzati, ...');
define('LANG_ADMIN_DEFAULTADN', 'Se non ci sono annunci disponibili, quindi un annuncio di default sarà mostrata.');
define('LANG_ADMIN_DEFAULTADU', 'Default URL dell\'annuncio grafico');
define('LANG_ADMIN_DEFAULTADF', 'Default annuncio URL avanti');
define('LANG_ADMIN_LIMITCHARN', 'Se no, i caratteri del testo limite al di sotto non importa.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Limite di caratteri di testo:');
define('LANG_ADMIN_TYPECPI', 'Il costo per impressione');
define('LANG_ADMIN_TYPECPC', 'Costo per clic');
define('LANG_ADMIN_TYPECPD', 'Costo giornaliero');
define('LANG_ADMIN_TYPECPDNOTE', 'Nota: questo richiede cron automatico o manuale ogni volta al giorno.');
define('LANG_ADMIN_ADVIL9', 'Quanti ne volete vendere? Digitare *9999* se si desidera consentire numero illimitato di annunci.');
define('LANG_ADMIN_CURRCONVERTER', 'Convertitore di valute');
define('LANG_ADMIN_PRICEINJPY2', 'Per JPY di importo non può avere .xx <br /> Se il prezzo è &#165; 90.9270 poi il formato corretto è &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'Non so le dimensioni esatte di annunci? Questo ti può aiutare!');
define('LANG_ADMIN_ROTATESPEED', 'Ad velocità di rotazione');
define('LANG_ADMIN_MILISECONDS', 'millisecondi');
define('LANG_ADMIN_NOTUSEROTAT', '*Lasciare vuoto se non si desidera utilizzare la rotazione.');
define('LANG_ADMIN_MILISECTOSEC', '10000 Millisecondi = 10 Secs, 30000 Millisecondi = 30 Secs.');
define('LANG_ADMIN_CONVERTMILLI', 'Conversione da millisecondi a secondi');
define('LANG_ADMIN_NUMOFADRUN', '# di annunci visualizzati');
define('LANG_ADMIN_TOTALNUMAD', 'Il numero massimo di annunci visualizzati sulla pagina. 5 *100x100* Ads? 1 *468x60* Ad?<br />Se lasciato vuoto, il default è 1 annunci.');
define('LANG_ADMIN_ADDSPACE', 'Spazio: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Aggiungere uno spazio tra gli annunci orizzontali.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Visualizza testo Descrizione di annunci al passaggio del mouse se l\'annuncio di testo attivato)');
define('LANG_ADMIN_CSSTEASERBGI', 'Background image');
define('LANG_ADMIN_CSSTEASERBGC', 'Background color');
define('LANG_ADMIN_CSSTEASERTO', 'Transparency / Opacity');
define('LANG_ADMIN_CSSTEASERFC', 'Font color');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Display Ad Descrizione testo qui sotto annuncio se testo dell\'annuncio Attivato)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Stile di testo');
define('LANG_ADMIN_IFBLANK', 'Se vuoto');
define('LANG_ADMIN_ADCODES', 'Annunci Codici');
define('LANG_ADMIN_ADCODEOP1', 'Opzione 1: php codici per le php pagine');
define('LANG_ADMIN_ADCODEOP2', 'Opzione 2: iframe codice per le altre pagine come: HTML, TPL, CGI, and PHP');
define('LANG_ADMIN_ADCODEIFRAME', '(sarà necessario cambiare le dimensioni larghezza e altezza se si desidera visualizzare più di 1 annunci e attivato il testo dell\'annuncio!)');
define('LANG_ADMIN_HOWTOCRON', 'Cronjobs consentono di automatizzare alcuni comandi o script sul tuo sito. È possibile impostare un comando o uno script da eseguire in un momento specifico ogni giorno, settimana, ecc');
define('LANG_ADMIN_HOWTOCRON1', 'Cronjob è necessaria per campagne che utilizzano');
define('LANG_ADMIN_HOWTOCRON2', 'Il file cron.php cercherà attraverso annunci attivi con campagne CPD e sottrarre -1 credito ogni annuncio.');
define('LANG_ADMIN_HOWTOCRON3', 'NOTA: È possibile visitare manualmente il file cron.php te se Cron non è disponibile');
define('LANG_ADMIN_HOWTOCRON4', 'Impostazione di cron');
define('LANG_ADMIN_HOWTOCRON5', 'Esegui cronjob volta al giorno. Qui di seguito ecco i vari codici di cron è possibile utilizzare');
define('LANG_ADMIN_HOWTOCRON6', 'Si consiglia di contattare il tuo host e chiedere se Cron è permesso e ciò codici Cron è possibile utilizzare.');
define('LANG_ADMIN_HOWTOCRON7', 'Per saperne di più Cron');

define('LANG_ADMIN_PRICEIN', 'Prezzo in');
define('LANG_ADMIN_SPECIALDEAL', 'Segna come speciale?');
define('LANG_ADMIN_SPECIALDEAL2', 'Offerta speciale, un\'offerta limitata nel tempo, e così via.');
define('LANG_ADMIN_DEFAULTATXT', 'Testo dell\'annuncio default');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Inizia una nuova campagna pubblicitaria banner');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Avviare un nuovo collegamento campagna testo pubblicitario');
define('LANG_ADMIN_CAMPAIGNTODO', 'Per fare');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Dettagli');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Prezzi');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Stili di campagna');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Stili annuncio');
define('LANG_ADMIN_CAMPAIGNTODOG', 'Ottenere codici');
define('LANG_ADMIN_ADGOESHERE', 'Descrizione annuncio va qui...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Anteprima tuo stile corrente campagna');
define('LANG_ADMIN_ADSBYABLE', 'Mostra Annunci Segno');
define('LANG_ADMIN_ADSBYLABEL', 'Annunci Label / Nome');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Qui sotto è possibile modificare lo stile CSS');
define('LANG_ADMIN_CAMPDEFASTYLE', 'Di seguito è riportato qui lo stile di default CSS');
define('LANG_ADMIN_NEWBANAD', 'Inizia un nuovo annuncio banner');
define('LANG_ADMIN_NEWTEXTAD', 'Avviare un nuovo annuncio link testuale');
define('LANG_ADMIN_EXAMPLESTYLE', 'Esempio: (Sotto usare uno stile predefinito) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Clicca sull\'immagine per visualizzare le statistiche degli annunci');
define('LANG_ADMIN_CLICKSLOGCRTY', 'Paese');
define('LANG_ADMIN_COUPONCODEN', 'Buoni / Sconti');
define('LANG_ADMIN_COUPONADDNEW', 'Aggiungi nuovo Coupon / Sconto');
define('LANG_ADMIN_COUPONEDITC', 'Modifica Coupon / Sconto');
define('LANG_ADMIN_NUMOFC', '# Usato');
define('LANG_ADMIN_LIMITUSES', 'limite usa');
define('LANG_ADMIN_NUMPERUSER', 'Numero per utente');

define('LANG_ADMIN_EDITAD', 'Modifica Dettagli annuncio');
define('LANG_ADMIN_DENYIPSADD', 'Nega Indirizzo IP');
define('LANG_AFORM_DENYIPADDED', 'Indirizzo IP aggiunto');
define('LANG_ADMIN_IP', 'Indirizzo IP');
define('LANG_ADMIN_DENYIPS', 'Elenco di indirizzi IP negato/vietato');
define('LANG_AMENU_DENYCTRYS', 'negare ai paesi');
define('LANG_ADMIN_DENYCTRYS', 'negare ai paesi');
define('LANG_ADMIN_COUNTRY', 'paese');
define('LANG_ADMIN_CTRY', 'nega Paese');
define('LANG_ADMIN_DENYCTRYADD', 'negare ai paesi');
define('LANG_AFORM_DENYCTRYADDED', 'paese inserito');

define('LANG_ADMIN_OTHERPAY', 'Altri Metodi di Pagamento');
define('LANG_ADMIN_OTHERPAY2', 'Elencare le opzioni di pagamento. vale a dire: la posta in contanti, assegno, Bitcoin, altro.');
define('LANG_ADMIN_OTHERPAY3', 'Inserisci il tuo istruzione di pagamento qui. vale a dire: i metodi alternativi di pagamento (bitcoin), i pagamenti offline, altrimenti.');

define('LANG_ADMIN_GCURR1', 'Se abilitata, i prezzi vengono aggiornati con google convertitore di valuta quando si accede in area admin.');
define('LANG_ADMIN_GCURR2', 'Attenzione: Selezionare la valuta di default.');
define('LANG_ADMIN_GCURR3', 'Si può anche fare il lavoro automatico cron');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Inizia un nuovo banner e text link campagna pubblicitaria mista'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Inizia un nuovo annuncio striscione & link di testo'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Avviare un nuovo flash (swf) Annuncio'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Lasciare il flash annuncio (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Consenti file di immagine carica?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Consenti file flash (.swf) carica?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Dimensione massima file'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Attenzione: Non consigliato'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'Se abilitata, inserzionista può caricare un file nella cartella del server host web.'); // 2014
define('LANG_ADMIN_DELETEALERT', 'Sei sicuro di voler eliminare questo?'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'Sei sicuro di voler fare questo?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'File Flash deve essere .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Inizia nuova pubblicità personalizzata'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Inserire Personalizzato eventuali annunci che si desidera utilizzare come Google AdSense, o qualsiasi reti pubblicitarie / scambi. È inoltre possibile aggiungere pubblicità personalizzata qui.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Annuncio personalizzato'); // 2014
?>