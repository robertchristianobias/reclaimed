<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
/*
------------------------
Language: French
Date    : Mar 06 2013
Modified: Nov 07, 2013
Credits : GroovyScripts, Dijiteol, Google Translate
------------------------
===================================================
The translation was done with Google Translate.

PLEASE BECAREFUL WHEN EDITTING BELOW BECAUSE 
SOME LINES CONTAIN HTML CODES.

Please use special characters codes so they are properly shown on the browser.

Do you know French Language? If so, then please help correcting any translating errors below. Email us with the updated files.
----------------------------------------------------

La traduction a été faite avec Google Translate.

S'IL VOUS PLAÎT ETRE prudent lorsque vous éditez ci-dessous parce
QUELQUES LIGNES contenir des codes HTML.

S'il vous plaît utilisez des codes spéciaux caractères de sorte qu'ils sont correctement indiqués sur le navigateur.

Savez-vous en français? Si oui, alors s'il vous plaît m'aider à corriger les erreurs traduction ci-dessous. Envoyez-nous avec les fichiers mis à jour.
===================================================
*/
define('LANG_TRANSLATION', 'ne garantit pas l\'exactitude de la traduction et l\'efficacité.');
// HTML PAGE
define('LANG_CHARSET_ISO', 'utf-8');
define('LANG_HTML', 'fr');
define('LANG_CONT_HTML', 'fr');
// MAIN AND ADMIN- COMMON WORDS (Many pages have same words more than once)
define('LANG_MENU_HOME', 'accueil');
define('LANG_MENU_ADVERTISE', 'publicité');
define('LANG_MENU_LOGIN', 'connexion');
define('LANG_MENU_LOGGEDIN', 'Connecté');
define('LANG_MENU_CONTACT', 'contact');

define('LANG_FMENU_HOME', 'page d\'accueil');
define('LANG_FMENU_ADVERTISE', 'annoncez avec nous');
define('LANG_FMENU_LOGIN', 'connexion');
define('LANG_FMENU_CONTACT', 'nous contacter');
define('LANG_FMENU_RIGHTS', 'Tous droits réservés');
define('LANG_FMENU_SCRIPTBY', 'Script par');

define('LANG_MAIN_WELCOME', 'Pour acheter de la publicité, s\'il vous plaît enregistrer un compte ou se connecter si déjà un.');
define('LANG_MAIN_ADOPTIONS', 'Options Publicité');
define('LANG_MAIN_CTITLE', 'Campagne');
define('LANG_MAIN_TITLE', 'Titre');
define('LANG_MAIN_SIZE', 'Taille');
define('LANG_MAIN_TYPE', 'Type');
define('LANG_MAIN_ADTYPEALLOWED', 'Type de l\'annonce admis');
define('LANG_MAIN_ROTATE', 'Rotateur');
define('LANG_MAIN_CREDITS', 'Crédits');
define('LANG_MAIN_UNLCREDITS', 'Crédits Illimités');
define('LANG_MAIN_TEXT', 'Texte');
define('LANG_MAIN_YES', 'Oui');
define('LANG_MAIN_NO', 'N');
define('LANG_MAIN_AVAIL', 'Disponibilité');
define('LANG_MAIN_VIEWPRICES', 'Voir les prix');
define('LANG_MAIN_ON', 'Le');
define('LANG_MAIN_OFF', 'Off');
define('LANG_MAIN_UNLIMITED', 'Illimitée');
define('LANG_MAIN_ETC', 'Les prix indiqués ci-dessus est le coût total. Si le prix n\'est pas indiqué pour la devise que vous voulez, alors il n\'est pas disponible à l\'achat.');
define('LANG_MAIN_ADTYPES', 'CPI- Coût par impression, CPC- Coût par clic moyen, CPD- Coût par jour');
define('LANG_MAIN_PAYMENTMETHOD', 'Mode de paiement');
define('LANG_MAIN_PRODUCT', 'Produit');
define('LANG_MAIN_BACKBUT', 'Retour');
define('LANG_MAIN_NEXTBUT', 'Suivant');
define('LANG_MAIN_PREVBUT', 'Extrait');
define('LANG_MAIN_AREA', 'Zone Principale');
define('LANG_MAIN_LOGOUT', 'Déconnexion');
define('LANG_MAIN_CLOSEWINDOW', 'Fermer cette fenêtre');
define('LANG_MAIN_DATE', 'Date');
define('LANG_MAIN_DATEADDED', 'Date d\'ajout');
define('LANG_MAIN_TOTAL', 'Total');
define('LANG_MAIN_IMPRESSIONS', 'Impressions');
define('LANG_MAIN_CLICKS', 'Clics');
define('LANG_MAIN_DAYS', 'Jours');
define('LANG_MAIN_STATUS', 'Statut');
define('LANG_MAIN_YOURADS', 'Vos annonces');
define('LANG_MAIN_CREMAINING', 'Crédits restants');
define('LANG_MAIN_CHFDS', 'Cliquez ici pour des statistiques détaillées');
define('LANG_MAIN_CREATEAD', 'Créez une nouvelle annonce');
define('LANG_MAIN_CREATEADAPP', 'Ajouter une nouvelle annonce (Toutes les annonces sont soumises à l\'approbation admin)');
define('LANG_MAIN_ACCEPTCURR', 'Devises acceptées');
define('LANG_MAIN_PRICE', 'Prix');
define('LANG_MAIN_SELECTED', 'Vous avez sélectionné');
define('LANG_MAIN_ADGRAPHICURL', 'Annonce URL Graphic');
define('LANG_MAIN_ADFORWARDURL', 'URL avant l\'annonce');
define('LANG_MAIN_ADTEXT', 'Texte');
define('LANG_MAIN_WIDTH', 'Largeur');
define('LANG_MAIN_HEIGHT', 'Hauteur');
define('LANG_MAIN_USERINFO', 'Informations sur l\'utilisateur');
define('LANG_MAIN_ADDETAILS', 'Détail de l\'annonce');
define('LANG_MAIN_VERIDETAILS', 'S\'il vous plaît vérifier toutes les informations sont correctes avant de continuer');
define('LANG_MAIN_TESTAD', 'Cliquez sur l\'annonce ci-dessus pour tester - il sera ouvert dans une nouvelle fenêtre');
define('LANG_MAIN_NEWUSER', 'Nouveau Annonceur');
define('LANG_MAIN_USERLOGIN', 'Connexion Annonceur');
define('LANG_MAIN_ULOGIN', 'Connectez-vous');
define('LANG_MAIN_FORGETINFO', 'Oubliez les informations de connexion?');
define('LANG_MAIN_NAME', 'Nom');
define('LANG_MAIN_EMAIL', 'Adresse e-mail');
define('LANG_MAIN_PASSWORD', 'Mot de passe');
define('LANG_MAIN_REGISTER', 'Registre');

define('LANG_MAIN_ADFORWARDURL2', 'URL Annonce illustrée');
define('LANG_MAIN_ADFORWARDURL3', '(URL à afficher courte: domain.com)');
define('LANG_MAIN_HISTORY', 'Historique des achats');
define('LANG_MAIN_CODE', 'Code');
define('LANG_MAIN_PERCENT', 'Pour cent');
define('LANG_MAIN_NUMBERS', 'Numéros');
define('LANG_MAIN_SUBMBUT', 'Envoyer');
define('LANG_MAIN_ENTERCOUPON', 'Entrez le code coupon');
define('LANG_MAIN_DISCOUNT', 'Remise');
define('LANG_MAIN_FREE', 'Gratuit');

define('LANG_MAIN_CTR', 'Taux de clic');

define('LANG_FORM_HELLO', 'Bonjour');
define('LANG_FORM_LOGIN', 'Vous pouvez vous connecter à');
define('LANG_FORM_REGARDS', 'Cordialement');
define('LANG_FORM_REMIND2', 'Vous avez récemment demandé d\'avoir vos informations de connexion par courriel à cette adresse.');
define('LANG_FORM_REMIND4', 'Si vous pensez que vous n\'avez pas demandé pour cette information, s\'il vous plaît ignorer ce message et accepter nos excuses pour prendre votre temps.');
define('LANG_FORM_REMIND5', 'Merci et bonne journée Merci!');
define('LANG_FORM_SIGNUP2', 'Nous vous remercions de vous inscrire.');
define('LANG_FORM_SIGNUP3', 'Vos informations de connexion sont ci-dessous, stocker ces informations dans un endroit sûr');
define('LANG_FORM_COMPLETE1', 'Dès administration approuve votre annonce, elle sera activée.');

define('LANG_FORM_ADAPPROVED', 'Votre annonce a été approuvé et est maintenant recevoir des expositions.');
define('LANG_FORM_ADDISAPPROVED1', 'Votre annonce a été refusé.');
define('LANG_FORM_ADDISAPPROVED2', 'Le motif du refus est inférieur à'); // :
define('LANG_FORM_ADDISAPPROVED3', 'S\'il vous plaît répondez à ce courriel avec vos questions.');
define('LANG_FORM_PAPPROVED', 'Votre paiement a été approuvé.');
define('LANG_FORM_PDISAPPROVED', 'Votre paiement a été refusé.');

define('LANG_FORM_FORGETEMAIL', 'Vos informations de connexion a été envoyé à votre adresse e-mail.');
define('LANG_FORM_REMINDESUBJ', 'Mot de passe Rappel');
define('LANG_FORM_NORECEMAIL', 'Aucune trace de l\'adresse e-mail.');
define('LANG_FORM_NONAME', 'Vous devez entrer un nom de contact!');
define('LANG_FORM_NOPASS', 'Vous devez entrer un mot de passe!');
define('LANG_FORM_NOEMAIL', 'Vous devez entrer une adresse e-mail!');
define('LANG_FORM_EMAILUSED', 'L\'adresse e-mail est déjà utilisée!');
define('LANG_FORM_SIGNUPESUBJ', 'Inscription Confirmation');
define('LANG_FORM_ACCCREATED', 'Votre compte a été créé!<br />S\'il vous plaît cliquer ici pour accéder à la zone annonceur et soumettre vos annonces.');
define('LANG_FORM_ENTERGRPURL', 'Vous devez entrer une URL de l\'annonce graphique!');
define('LANG_FORM_GRAPICADFILE', 'Fichier ad graphique doit être un .jpg, .gif, or .png image!');
define('LANG_FORM_ENTERFORURL', 'Vous devez entrer une URL de site Web de l\'avant!');
define('LANG_FORM_GRAPICMWIDTH', 'Ad graphique dépasse la taille de largeur maximale de');
define('LANG_FORM_GRAPICMHEIGHT', 'Ad graphique dépasse la taille de la hauteur maximale');
define('LANG_FORM_SELECTPAYMENT', 'Vous devez sélectionner un mode de paiement!');

define('LANG_PAGE_UNAUTHORIZED', 'Vous n\'avez pas l\'autorisation d\'afficher cette page.');
define('LANG_PAGE_UNAUTHORIZED2', 'Vous pouvez vous connecter ici');

define('LANG_PAGE_CANCEL', 'RAISON: <br />1) Vous avez annulé votre achat. <br />2) Problème avec le compte du commerçant. <br />3) Problème avec le système de paiement du processeur. <br /><br />S\'il s\'agit d\'une erreur, s\'il vous plaît contactez-nous. <br />');

define('LANG_PAGE_UFAILED', 'Connexion utilisateur n\'a pas');
define('LANG_PAGE_FAILED', 'Le nom d\'utilisateur et mot de passe ne correspondent pas à celles de la base de données.<br />S\'il vous plaît revenir en arrière et vous connecter à nouveau. S\'il vous plaît assurez-vous que vous avez confirmé votre adresse e-mail.<br />Si vous rencontrez toujours des problèmes s\'il vous plaît contactez-nous.');

define('LANG_PAGE_REGDISABLED', 'Désolé, l\'enregistrement a été désactivé par l\'administrateur. <br />Si vous avez des questions ou des inquiétudes, s\'il vous plaît contactez-nous.');

define('LANG_PAGE_ADCOMFIRMSUBJ', 'Confirmation de l\'annonce');
define('LANG_PAGE_ADMINCOMFSUBJ', 'PayBannerAd: ADMIN- Nouvelle annonce en attente d\'approbation!');
define('LANG_PAGE_ADMINCOMFSUBJ2', 'Cliquez ci-dessous pour afficher'); // link URL to site Admin Area in email to admin

define('LANG_PAGE_COMPLETED', 'Votre paiement est terminée. Votre annonce sera examiné et s\'il n\'y a pas de problème, il sera approuvé prochainement!<br /><br />Merci!');

define('LANG_PAGE_SENDQRF', 'Envoyez-nous votre question, demande ou d\'évaluation');
define('LANG_PAGE_MSGSENT', 'Votre message a été envoyé!<br />Nous vous répondrons sous peu.<br /><br />');
define('LANG_PAGE_ENTERMSG', 'Vous devez entrer un message!');
define('LANG_PAGE_CONTEMAILSUBJ', 'Email Contact Publicité'); // the email subject to Admin from contact form
define('LANG_MAIN_MESSAGE', 'Message');
define('LANG_MAIN_SUBJECT', 'Sujet');
define('LANG_MAIN_SEND', 'Envoyer');
define('LANG_MAIN_SHOWIP', 'Votre IP enregistrées');
define('LANG_MAIN_THANKYOU', 'Merci');

define('LANG_PAGE_STATTITLE', 'Stats ad détaillée');

define('LANG_PAGE_SELCAMPAIGN', 'Sélectionnez une campagne ci-dessous');
define('LANG_PAGE_ADTYPEALLOWED', 'Type de l\'annonce admis');
define('LANG_PAGE_LIMITCHARLEFT', 'Caractères Limited gauche:');
define('LANG_PAGE_NOHTMLCODES', 'Aucun de ces codes HTML! Mots Texte seulement!');

define('LANG_PAGE_PAYBUT', 'Procéder à payer');

define('LANG_PAGE_UPLOADIMAGE', 'Télécharger le fichier d\'image'); // 2014
define('LANG_PAGE_UPLOADFLASH', 'Télécharger le fichier flash (swf)'); // 2014
define('LANG_PAGE_ADFLASHURL', 'Flash annonce URL'); // 2014

// ADMIN
define('LANG_ADMIN_LOGINAREA', 'Administration Connexion');
define('LANG_FMENU_LOADTIME', 'Cette page a pris %f secondes à charger.');
define('LANG_ADMIN_USER', 'Nom d\'utilisateur');
define('LANG_ADMIN_SUBMITBUT', 'Soumettre');
define('LANG_ADMIN_LOGININFO', 'Connectez-vous pour afficher zone admin');
define('LANG_ADMIN_LOGINFAIL', 'Échec. Re-connexion à nouveau.');
define('LANG_ADMIN_ACCTSUC', 'Informations sur le compte modifié avec succès.');
define('LANG_ADMIN_LOGINBUT', 'Connectez-vous');
define('LANG_ADMIN_INCWRITE', 'Je peux écrire dans /includes. Il s\'agit d\'un risque pour la sécurité, vous DEVEZ CHMOD le dossier (autorisations) de retour à 755!');
define('LANG_ADMIN_INSWRITE', 'Le répertoire /install existe! Il s\'agit d\'un risque pour la sécurité, vous devez supprimer le répertoire /install!');

define('LANG_AMENU_LOGGEDAS', 'Connecté en tant que');
define('LANG_AMENU_ADMINR', 'ADMINISTRATEUR');
define('LANG_AMENU_LOGOUT', 'Déconnexion');
define('LANG_AMENU_CHGPW', 'Changer mot de passe');
define('LANG_AMENU_HOME', 'Accueil');
define('LANG_AMENU_EMAILAD', 'Email Annonceurs');
define('LANG_AMENU_MANADV', 'Gérer les Annonceurs');
define('LANG_AMENU_MANCAM', 'Gérer les Campagnes');
define('LANG_AMENU_SITESET', 'Paramètres du site');
define('LANG_AMENU_CURRSET', 'Réglages de devises');
define('LANG_AMENU_PAYSSET', 'Config de paiement');
define('LANG_AMENU_ADMACCT', 'Config admin');
define('LANG_AMENU_UPGRADE', 'Télécharger');
define('LANG_AMENU_HOWTOS', 'Comment tos');

define('LANG_AMENU_ADWAIT', 'Attendre');
define('LANG_AMENU_CLICKSLOG', 'Logs d\'IPs');
define('LANG_AMENU_CLICKSLOG2', 'Utiles pour surveiller les activités suspectes telles que la fraude au clic, d\'autre.');
define('LANG_AMENU_COUPONS', 'Coupons');
define('LANG_AMENU_MANCOUP', 'Gérer la Coupons');
define('LANG_AMENU_ADVERTISERS', 'Annonceurs');
define('LANG_AMENU_CAMPAIGNS', 'Campagnes');
define('LANG_AMENU_SETTINGS', 'Réglages');
define('LANG_AMENU_EXTRAS', 'Extras');

define('LANG_AMENU_DENYIPS', 'Refuser/bannir des adresses IP');

define('LANG_ADMIN_UPGRADEAREA', 'Télécharger / Amélioration de l\'espace');

define('LANG_ADMIN_PAYSETTINGS', 'Configuration de paiement');
define('LANG_ADMIN_PAYENABLECUR', 'Vous devez activer devises');
define('LANG_ADMIN_OFFDISABLED', 'Off/Handicapées');
define('LANG_ADMIN_ONENABLED', 'Le/Activé');
define('LANG_ADMIN_CURRACCEPTED', 'Devises acceptées');
define('LANG_ADMIN_CURRENCYNOTE', '*Vous devez cocher la case (s) ci-dessus pour permettre aux annonceurs de voir Devises Options dans la zone membre / annonceur.');
define('LANG_ADMIN_ECUMONEYNOTE', 'IMPORTANT: Première voir plus d\'informations à Merchant Tools page avant d\'utiliser ECUmoney.');
define('LANG_ADMIN_EMAILADDRESS', 'Adresse e-mail');
define('LANG_ADMIN_ACCOUNTNUM', 'Numéro de compte');
define('LANG_ADMIN_USERID', 'ID utilisateur');
define('LANG_ADMIN_HOLDINGNUM', 'Nombre Holding');
define('LANG_ADMIN_STORENAME', 'Nom du magasin');
define('LANG_ADMIN_STOREID', 'Store ID');
define('LANG_ADMIN_LOGINID', 'ID de connexion');
define('LANG_ADMIN_TRANSKEY', 'clé de transaction');
define('LANG_ADMIN_UPDATEBUT', 'Mise à jour');

define('LANG_ADMIN_GOBACK', 'Aller Retour');
define('LANG_ADMIN_DELETEBUT', 'Supprimer');
define('LANG_ADMIN_STATS', 'Annonce Stats');
define('LANG_ADMIN_DATEJOIN', 'Date Inscrit');
define('LANG_ADMIN_IDNUM', 'Id#');
define('LANG_ADMIN_ADEDITVIEW', 'Annonceurs (Cliquez sur l\'annonceur de modifier des détails ou des annonces vue)');
define('LANG_ADMIN_NUMOFADS', '# des annonces');
define('LANG_ADMIN_ADDNEWAD', 'Ajouter une nouvelle annonce à l\'annonceur');
define('LANG_ADMIN_ADDNEWUSER', 'Ajouter nouvel annonceur/utilisateur');

define('LANG_ADMIN_SITESETTINGS', 'Paramètres du site');
define('LANG_ADMIN_OPEN', 'Ouverte');
define('LANG_ADMIN_CLOSED', 'Fermé');
define('LANG_ADMIN_IFCLOSED', '*S\'il est fermé, alors les gens ne seront pas en mesure d\'enregistrer un compte.');
define('LANG_ADMIN_AEMAIL', 'Adresse Email Admin');
define('LANG_ADMIN_SENDFROM', 'Envoyer par Email Adresse');
define('LANG_ADMIN_ABPATHTO', 'Chemin d\'accès absolu vers le site');
define('LANG_ADMIN_SITEURL', 'URL du site');
define('LANG_ADMIN_SITENAME', 'Nom du site');
define('LANG_ADMIN_SITEKEYWORD', 'Mots-clés du site');
define('LANG_ADMIN_SITEDESC', 'Description du site');
define('LANG_ADMIN_DEFAULTLAN', 'Langue par défaut');
define('LANG_ADMIN_DEFAULTCUR', 'Devise par défaut');
define('LANG_ADMIN_DEFAULTTEM', 'Thème par défaut');
define('LANG_ADMIN_ALLOWREG', 'Permettre l\'inscription');
define('LANG_ADMIN_SHOWMSTATS', 'Voir les statistiques');

define('LANG_ADMIN_SRYNOADVRS', 'Désolé, pas d\'annonceurs ont été trouvés pour envoyer l\'email à!');
define('LANG_ADMIN_SENDTOWHO', 'Envoyer à');
define('LANG_ADMIN_NUMOFADVRE', 'Nombre de courriels envoyés à Annonceurs');
define('LANG_ADMIN_EADVRSNOTE', 'Vous pouvez envoyer un e-mail à des annonceurs pour leur laisser savoir au sujet des offres spéciales, des prix bas, rappelant tous ceux qui n\'ont pas payé, etc');
define('LANG_ADMIN_ALLADVRCOUNT', 'Tous les annonceurs - Décompte');
define('LANG_ADMIN_HELLONAME', 'Bonjour');
define('LANG_ADMIN_RECEIVEEMAIL', 'Vous recevez ce message car vous avez un compte annonceur avec nous.');
define('LANG_ADMIN_SUBSTITUTIONS', 'Remplaçant (inserts utilisés dans le corps du message et de domaine)');

define('LANG_ADMIN_SERVERD', 'Server Details');
define('LANG_ADMIN_GSVERSION', 'Version Checker');
define('LANG_ADMIN_GSNEWS', 'Les dernières nouvelles de Dijiteol');
define('LANG_ADMIN_TENANCE', 'Maintenance du site');

define('LANG_ADMIN_STATSINFO', 'Statistique (Information)');
define('LANG_ADMIN_MADVERS', 'Annonceurs');
define('LANG_ADMIN_MCAMIGS', 'Campagnes');
define('LANG_ADMIN_MACTADS', 'Annonces actives');
define('LANG_ADMIN_MINACTADS', 'Annonces inactifs');
define('LANG_ADMIN_MUNLIADS', 'Annonces illimitée');
define('LANG_ADMIN_MACTCRED', 'Crédits Active');
define('LANG_ADMIN_MINACTCRED', 'Crédits inactifs');
define('LANG_ADMIN_ML30DAYSI', '-30 jours d\'impression');
define('LANG_ADMIN_ML30DAYSC', '-30 Jours clics');
define('LANG_ADMIN_ML7DAYSI', '-7 jours d\'impression');
define('LANG_ADMIN_ML7DAYSC', '-7 Jours clics');
define('LANG_ADMIN_MYTIMPS', '-1 Jour d\'impression');
define('LANG_ADMIN_MYTDAYS', '-1 Jour clics');
define('LANG_ADMIN_TTIMPS', 'Jour d\'impression');
define('LANG_ADMIN_TTCLICKS', 'Jour clics');
define('LANG_ADMIN_LASTCRONDATE', 'Cron date de la dernière');
define('LANG_ADMIN_CRONNEEDED', 'Cron est nécessaire dès aujourd\'hui! Cliquez sur le lien');
define('LANG_ADMIN_CRONNONEED', 'Cron ne sont pas nécessaires aujourd\'hui.');
define('LANG_ADMIN_ADSWAITING', 'Annonces en attente d\'approbation');
define('LANG_ADMIN_APPROVE', 'Approuver');
define('LANG_ADMIN_DISAPPROVE', 'Désapprouver');
define('LANG_ADMIN_DISAREASON', 'Raison désapprobation');
define('LANG_ADMIN_PAIDVIA', 'Paiement par');
define('LANG_ADMIN_OPTIMIZEDBT1', 'Tables de base de données Optimiser');
define('LANG_ADMIN_OPTIMIZEDBT2', 'Optimiser');
define('LANG_ADMIN_OPTIMIZEDBT3', 'conçu pour se débarrasser de tous les blocs fragmentés, libérer de l\'espace utilisé et faire fonctionner votre base de données plus rapide.');

define('LANG_ADMIN_PURHISTORY', 'Historique des achats (vérifier leurs paiements avant de vérifier leurs annonces)');

define('LANG_AFORM_EADAPPROVED', 'Votre annonce a été approuvée');
define('LANG_AFORM_EADDISAPPROVED', 'Votre annonce n\'a pas été approuvé');
define('LANG_AFORM_DBOPTIMIZED', 'Tables de base de données optimisée');
define('LANG_AFORM_SITESETTINGS', 'Mise à jour les paramètres du site');
define('LANG_AFORM_PAYSETTINGS', 'Mise à jour des paramètres de paiement');
define('LANG_AFORM_CURSETTINGS', 'Réglages de change mise à jour');
define('LANG_AFORM_USERDELETED', 'Utilisateur supprimé');
define('LANG_AFORM_USERADDED', 'Ajouté utilisateur');
define('LANG_AFORM_ADDTOUSER', 'Annonce ajoutée à l\'utilisateur');
define('LANG_AFORM_USERUPDATED', 'Mise à jour de l\'utilisateur');
define('LANG_AFORM_ADUPDATED', 'Annonce mise à jour');
define('LANG_AFORM_ADDELETED', 'Annonce supprimés');
define('LANG_AFORM_ADADDED', 'Annonce ajoutée');
define('LANG_AFORM_CAMGNADDED', 'Ajouté campagne');
define('LANG_AFORM_CAMGNUPDATED', 'Mise à jour de la campagne');
define('LANG_AFORM_CAMGNDELETED', 'Campagne supprimée');

define('LANG_AFORM_COUPONDELETED', 'Coupon supprimés');
define('LANG_AFORM_COUPONADDED', 'Coupon ajoutée');
define('LANG_AFORM_COUPONUPDATED', 'Coupon à jour');

define('LANG_ADMIN_CAMGNTITLE', 'Campagnes (Cliquez sur la campagne à modifier des détails ou obtenir les codes d\'annonce)');
define('LANG_ADMIN_ADDNEWCAMGN', 'Ajouter une nouvelle campagne');
define('LANG_ADMIN_SHORTDESC', 'Description succincte');
define('LANG_ADMIN_SHORTDESCN', '*Brève information sur la campagne, où les annonces diffusées, ...');
define('LANG_ADMIN_DEFAULTADN', 'S\'il n\'y a pas d\'annonces disponibles, puis une annonce par défaut sera affiché.');
define('LANG_ADMIN_DEFAULTADU', 'URL par défaut de publicité graphique');
define('LANG_ADMIN_DEFAULTADF', 'URL par défaut avant l\'annonce');
define('LANG_ADMIN_LIMITCHARN', 'Si Non, caractères de texte ci-dessous Limite n\'ont pas d\'importance.');
define('LANG_ADMIN_LIMITCHARLEFT', 'Limite des caractères textuels:');
define('LANG_ADMIN_TYPECPI', 'Coût par impression');
define('LANG_ADMIN_TYPECPC', 'Coût par clic');
define('LANG_ADMIN_TYPECPD', 'Coût par jour');
define('LANG_ADMIN_TYPECPDNOTE', 'Notez: que ce besoin de cron automatique ou manuel chaque fois par jour.');
define('LANG_ADMIN_ADVIL9', 'Tapez \'9999\' si vous voulez permettre à nombre d\'annonces illimité.');
define('LANG_ADMIN_CURRCONVERTER', 'Convertisseur de devises');
define('LANG_ADMIN_PRICEINJPY2', 'Pour JPY-montant ne peut pas avoir .xx <br /> Si le prix est &#165; 90.9270 puis le bon format est &#165; 91');
define('LANG_ADMIN_ADSIZEHELP', 'Je ne sais pas exactement les tailles d\'annonce? Cela peut vous aider!');
define('LANG_ADMIN_ROTATESPEED', 'Vitesse de rotation');
define('LANG_ADMIN_MILISECONDS', 'millisecondes');
define('LANG_ADMIN_NOTUSEROTAT', '*Laissez vide (pas d\'espace) si vous ne souhaitez pas utiliser la rotation.');
define('LANG_ADMIN_MILISECTOSEC', '10000 millisecondes = 10 Secs, 30000 millisecondes = 30 Secs.');
define('LANG_ADMIN_CONVERTMILLI', 'Convert Millisecondes to Secondes');
define('LANG_ADMIN_NUMOFADRUN', '# d\'annonces affichées');
define('LANG_ADMIN_TOTALNUMAD', 'Le nombre maximal d\'annonces illustrées à la page. 5 *100x100* Ads? 1 *468x60* Ad?<br />Si laissé vide, la valeur par défaut est de 1 ad.');
define('LANG_ADMIN_ADDSPACE', 'Espace: (&amp;nbsp;)');
define('LANG_ADMIN_ADDSPACE2', 'Ajouter un espace entre les annonces horizontale.');
define('LANG_ADMIN_CSSTEASER', 'CSS (Annonce illustrée Texte Description passage de la souris si le texte des annonces activée.)');
define('LANG_ADMIN_CSSTEASERBGI', 'Background image');
define('LANG_ADMIN_CSSTEASERBGC', 'Background color');
define('LANG_ADMIN_CSSTEASERTO', 'Transparency / Opacity');
define('LANG_ADMIN_CSSTEASERFC', 'Font color');
define('LANG_ADMIN_CSSTEASERBT', 'Border-top');
define('LANG_ADMIN_CSSTEASERBTLR', 'Border-top left radius');
define('LANG_ADMIN_CSSTEASERBTRR', 'Border-top right radius');
define('LANG_ADMIN_CSSADTEXT', 'CSS (Annonce illustrée ci-dessous Description Texte annonce si le texte des annonces activée.)');
define('LANG_ADMIN_CSSADTEXTSTYLE', 'Style du texte');
define('LANG_ADMIN_IFBLANK', 'Si vierge');
define('LANG_ADMIN_ADCODES', 'Codes de l\'annonce');
define('LANG_ADMIN_ADCODEOP1', 'Option 1: php codes for PHP pages');
define('LANG_ADMIN_ADCODEOP2', 'Option 2: iframe code for other pages such as: HTML, TPL, CGI, and PHP as well');
define('LANG_ADMIN_ADCODEIFRAME', '(vous devrez changer le iframe  largeur </ strong> hauteur  </ strong> tailles si vous voulez voir plus de 1 annonce et a permis un texte d\'annonce!)');
define('LANG_ADMIN_HOWTOCRON', 'Cronjobs vous permettent d\'automatiser certaines commandes ou des scripts sur votre site. Vous pouvez définir une commande ou un script à exécuter à une heure précise chaque jour, semaine, etc');
define('LANG_ADMIN_HOWTOCRON1', 'Cron job est nécessaire pour les campagnes à l\'aide');
define('LANG_ADMIN_HOWTOCRON2', 'Le fichier cron.php ne recherche que dans les annonces actives avec l\'aide de campagnes de PPC et soustraire -1 de crédit de chaque annonce.');
define('LANG_ADMIN_HOWTOCRON3', 'Notez: que vous pouvez parcourir manuellement tous les fichiers si vous cron.php Cron n\'est pas disponible');
define('LANG_ADMIN_HOWTOCRON4', 'Configuration Cron');
define('LANG_ADMIN_HOWTOCRON5', 'Cron Exécuter une fois par jour. Ci-dessous voici quelques codes tâche cron, vous pouvez utiliser');
define('LANG_ADMIN_HOWTOCRON6', 'Vous devez contacter votre hébergeur et lui demander si Cron est permis et ce codes Cron, vous pouvez utiliser.');
define('LANG_ADMIN_HOWTOCRON7', 'En savoir plus sur Cron');

define('LANG_ADMIN_PRICEIN', 'Prix en');
define('LANG_ADMIN_SPECIALDEAL', 'Le rendre spécial?');
define('LANG_ADMIN_SPECIALDEAL2', 'Offre spéciale, Offre à durée limitée, et ainsi de suite.');
define('LANG_ADMIN_DEFAULTATXT', 'Texte de l\'annonce par défaut');
define('LANG_ADMIN_NEWBANCAMPAIGN', 'Lancer une campagne nouvelle bannière publicitaire');
define('LANG_ADMIN_NEWTXTCAMPAIGN', 'Lancer une campagne publicitaire du nouveau texte lien');
define('LANG_ADMIN_CAMPAIGNTODO', 'Pour ce faire');
define('LANG_ADMIN_CAMPAIGNTODOD', 'Détails');
define('LANG_ADMIN_CAMPAIGNTODOP', 'Prix');
define('LANG_ADMIN_CAMPAIGNTODOS', 'Campagne styles');
define('LANG_ADMIN_CAMPAIGNTODOS2', 'Annonce styles');
define('LANG_ADMIN_CAMPAIGNTODOG', 'Obtenir des codes');
define('LANG_ADMIN_ADGOESHERE', 'Description de l\'annonce va ici...');
define('LANG_ADMIN_PREVCAMPSTYLE', 'Prévisualiser votre style de campagne actuelle');
define('LANG_ADMIN_ADSBYABLE', 'Montrer Annonces Inscription');
define('LANG_ADMIN_ADSBYLABEL', 'Ads by Label/Nom');
define('LANG_ADMIN_CAMPEDITSTYLE', 'Voici ici, vous pouvez modifier le style CSS');
define('LANG_ADMIN_CAMPDEFASTYLE', 'Ci-dessous voici le style CSS par défaut');
define('LANG_ADMIN_NEWBANAD', 'Démarrer une nouvelle bannière');
define('LANG_ADMIN_NEWTEXTAD', 'Démarrer une nouvelle annonce textuelle lien');
define('LANG_ADMIN_EXAMPLESTYLE', 'Exemple: (Ci-dessous utiliser un style prédéfini) ');
define('LANG_ADMIN_CLICKIMGSTATS', 'Cliquez sur l\'image pour afficher les statistiques ad');
define('LANG_ADMIN_CLICKSLOGCRTY', 'Pays');
define('LANG_ADMIN_COUPONCODEN', 'Coupons / Réductions');
define('LANG_ADMIN_COUPONADDNEW', 'Ajouter de nouveaux Coupons / Remise');
define('LANG_ADMIN_COUPONEDITC', 'Modifier Coupon / Remise');
define('LANG_ADMIN_NUMOFC', '# Utilisé');
define('LANG_ADMIN_LIMITUSES', 'limiter les utilisations');
define('LANG_ADMIN_NUMPERUSER', 'Nombre par utilisateur');

define('LANG_ADMIN_EDITAD', 'Modifier Détail de l\'annonce');
define('LANG_ADMIN_DENYIPSADD', 'Refuser adresse IP');
define('LANG_AFORM_DENYIPADDED', 'IP Address Added');
define('LANG_ADMIN_IP', 'Adresse IP');
define('LANG_ADMIN_DENYIPS', 'Liste des adresses IP Refusé / Interdit');
define('LANG_AMENU_DENYCTRYS', 'refuser les pays');
define('LANG_ADMIN_DENYCTRYS', 'refuser les pays');
define('LANG_ADMIN_COUNTRY', 'pays');
define('LANG_ADMIN_CTRY', 'refuser Pays');
define('LANG_ADMIN_DENYCTRYADD', 'refuser les pays');
define('LANG_AFORM_DENYCTRYADDED', 'Pays ajouté');

define('LANG_ADMIN_OTHERPAY', 'D\'autres méthodes de paiement');
define('LANG_ADMIN_OTHERPAY2', 'Dressez la liste de vos options de paiement. ie: le courrier espèces, par chèque, bitcoin, autre.');
define('LANG_ADMIN_OTHERPAY3', 'Entrez votre instruction de paiement ici. ie: des méthodes alternatives de paiement (bitcoin), paiements en différé le cas contraire.');

define('LANG_ADMIN_GCURR1', 'Si elle est activée, les prix sont mis à jour en utilisant google convertisseur de devises lorsque vous vous connectez en zone d\'administration.');
define('LANG_ADMIN_GCURR2', 'Attention: Choisissez votre devise par défaut.');
define('LANG_ADMIN_GCURR3', 'Vous pouvez aussi faire le travail de cron automatique');

define('LANG_ADMIN_NEWMIXCAMPAIGN', 'Démarrer une nouvelle bannière et le texte campagne lien publicitaire mixte'); // 2014
define('LANG_ADMIN_NEWMIXAD', 'Démarrer une nouvelle bannière et lien de texte annonce'); // 2014
define('LANG_ADMIN_NEWSWFAD', 'Lancer un nouveau flash (swf) annonce'); // 2014
define('LANG_ADMIN_ALLOWFLASHAD', 'Laisser le flash annonce (.swf)?'); // 2014
define('LANG_ADMIN_ALLOWIMGUP', 'Autoriser les fichiers image charger?'); // 2014
define('LANG_ADMIN_ALLOWSWFUP', 'Autoriser le contenu flash (.swf) la page?'); // 2014
define('LANG_ADMIN_ALLOWMAXSIZE', 'Taille maximale du fichier'); // 2014
define('LANG_ADMIN_ALLOWWARNING', 'Attention: Il n\'est pas recommandé'); // 2014
define('LANG_ADMIN_ALLOWWARNING2', 'Si elle est activée, l\'annonceur peut télécharger un fichier dans votre dossier de serveur de l\'hébergeur.'); // 2014
define('LANG_ADMIN_DELETEALERT', 'Etes-vous sûr de vouloir supprimer ce?'); // 2014
define('LANG_ADMIN_BUTTONALERT', 'Etes-vous sûr de vouloir faire cela?'); // 2014
define('LANG_ADMIN_FLASHADFILE', 'Fichier Flash doit être .swf'); // 2014
define('LANG_ADMIN_NEWCUSAD', 'Démarrer une nouvelle annonce personnalisée'); // 2014
define('LANG_ADMIN_INSERTCADS', 'Insérer des annonces que vous souhaitez utiliser comme Google AdSense, ou tous les réseaux publicitaires / échanges. Vous pouvez également ajouter annonce personnalisé ici.'); // 2014
define('LANG_ADMIN_CUSTADVERT', 'Annonce personnalisée'); // 2014
?>