<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
include_once ('lgcheck.php');

$campaign_url=$_POST['campaign_url'];
$id = $campaign_url;
$campaign_info = get_campaign_info($id);

if ($campaign_url==''){
echo '<a href="index.php?action=buyad"><strong>'.LANG_MAIN_BACKBUT.'</strong></a>';
header("location:index.php?action=buyad");
}
if ($_POST['adtyid_url']==''){
echo '<a href="index.php?action=buyad"><strong>'.LANG_MAIN_BACKBUT.'</strong></a>';
header("location:index.php?action=buyad");
}

extract($_POST,EXTR_PREFIX_ALL,'post');
?>
<script type="text/javascript" src="<? echo $conf['site_url']; ?>includes/js/txtlimit.js"></script>
<br /><br />
<? include_once ('membar.php'); //echo $_POST['adtyid_url']; ?>
<br /><br />

<form enctype="multipart/form-data" name="input" action="index.php?action=preview" method="post">
<input type="hidden" name="campaign_url" value="<? echo $_POST['campaign_url'] ?>">
<input type="hidden" name="coupon_code" value="<? echo $_POST['coupon_code'] ?>">
<input type="hidden" name="adtyid_url" value="<? echo $_POST['adtyid_url'] ?>">
<table id="mem">
	<thead>
		<th colspan="2"><?php echo LANG_MAIN_CREATEADAPP; ?>:</th>
	</thead>
<tbody>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_SELECTED; ?>:</td>
    <td id="mtableleft"><?=$_POST['id'] ?><strong><? echo $campaign_info["title"] ?></strong><br />
	<?php echo LANG_MAIN_PRICE; ?>: <? include_once ('pricing.php'); ?><br />
	<?php echo LANG_MAIN_TYPE; ?>: <? echo $campaign_info['adtype'] ?><br />
	<?php echo LANG_MAIN_SIZE; ?>: <? echo str_replace('px','',$campaign_info['width']); ?>x<? echo str_replace('px','',$campaign_info['height']); ?><br />
	<? 
	if($campaign_info["unlimited"] == "yes")
	{
		echo " ";
		echo LANG_MAIN_UNLCREDITS;
		echo " ";
	}
	if($campaign_info["unlimited"] == "no")
	{
		echo " ";
		echo LANG_MAIN_CREDITS;
		echo ": ".$campaign_info["credits"] ." ";
	}
	if($campaign_info["unlimited"] == "")
	{
		echo " ";
		echo LANG_MAIN_CREDITS;
		echo ": ".$campaign_info["credits"] ." ";
	}	
	 ?> <br /> 
	 <?php echo LANG_MAIN_TEXT; ?>:<? 
	if($campaign_info["allow_addesc"] == "yes")
	{
		echo " ";
		echo LANG_MAIN_YES;
	}
	if($campaign_info["allow_addesc"] == "no")
	{
		echo " ";
		echo LANG_MAIN_NO;
	}	
	 ?> <br />
	<? echo $campaign_info["shortdesc"] ?> <br />
	</td>
  </tr>
<? if($_POST['adtyid_url'] == 'banner') { ?>
  <tr>
    <td>&nbsp;</td>
    <td id="mtableleft"><img src="<?php echo $conf['site_url'] ?>images/i/z_image_files.png" title="Banner Images" alt="Banner Images" width="266" height="95" border="0"></td>
  </tr>
<? if($conf['allowupload']=='yes') { /* Bytes */ ?>
  <tr>
    <td id="mtableright"><?php echo LANG_PAGE_UPLOADIMAGE; ?>:</td>
    <td id="mtableleft"><input type="hidden" name="MAX_FILE_SIZE" value="<? echo $conf["uploadsize"]; ?>" /><input name="banner_url" id="banner_url" type="file" /></td>
  </tr>
<? } ?>
  <tr>
    <td id="mtableright"><?php echo LANG_MAIN_ADGRAPHICURL; ?>:</td>
    <td id="mtableleft"><input name="banner_url" type="text" class="webforms" id="banner_url" placeholder="http://url-to-image.com/file.png" size="50"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td id="mtableleft"><font color=red><b><?php echo LANG_MAIN_ADTYPEALLOWED; ?></b>: <font color=green><b>.gif</b></font>, <font color=green><b>.jpg</b></font>, <font color=green><b>.jpeg</b></font>, <font color=green><b>.png</b></font> <br />
	<b><?php echo LANG_MAIN_SIZE; ?></b>: <?php echo LANG_MAIN_WIDTH; ?>=<font color=green><b><? echo str_replace('px','',$campaign_info['width']); ?></b></font> x <?php echo LANG_MAIN_HEIGHT; ?>=<font color=green><b><? echo str_replace('px','',$campaign_info['height']); ?></b></font>
	</font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td id="mtableright"><?php echo LANG_MAIN_ADFORWARDURL; ?>:</td>
    <td id="mtableleft"><input name="forward_url" type="text" class="webforms" id="forward_url" value="<? echo $forward_url ?>http://" size="50"></td>
  </tr><?php if ($campaign_info["allow_addesc"] == "yes") { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_ADTEXT; ?>:</td>
    <td><textarea name="addesc_url" cols="50" class="webforms" id="addesc_url" onKeyDown="limitText(this.form.addesc_url,this.form.countdown,<? echo $campaign_info["text_limit"] ?>);" onKeyUp="limitText(this.form.addesc_url,this.form.countdown,<? echo $campaign_info["text_limit"] ?>);"><? echo $addesc_url ?></textarea> 
    <?php echo LANG_PAGE_LIMITCHARLEFT; ?> <input readonly type="text" name="countdown" size="3" value="<? echo $campaign_info["text_limit"] ?>">.
	<br />(<font color=red><strong><?php echo LANG_PAGE_NOHTMLCODES; ?></strong></font>)</td>
  </tr><?php } ?>
<? } if($_POST['adtyid_url'] == 'flash') { ?>
  <tr>
    <td>&nbsp;</td>
    <td id="mtableleft"><img src="<?php echo $conf['site_url'] ?>images/i/z_file_swf.png" title="Flash" alt="Flash" width="96" height="96" border="0"></td>
  </tr>
<? if($conf['allowfupload']=='yes') { /* Bytes */ ?>
  <tr>
    <td id="mtableright"><?php echo LANG_PAGE_UPLOADFLASH; ?>:</td>
    <td id="mtableleft"><input type="hidden" name="MAX_FILE_SIZE" value="<? echo $conf["uploadfsize"]; ?>" /><input name="banner_url" id="banner_url" type="file" /></td>
  </tr>
<? } ?>
  <tr>
    <td id="mtableright"><?php echo LANG_PAGE_ADFLASHURL; ?>:</td>
    <td id="mtableleft"><input name="banner_url" type="text" class="webforms" id="banner_url" placeholder="http://url-to-flash.com/file.swf" size="50"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td id="mtableleft"><font color=red><b><?php echo LANG_MAIN_ADTYPEALLOWED; ?></b>: <font color=green><b>.swf</b></font><br />
	<b><?php echo LANG_MAIN_SIZE; ?></b>: <?php echo LANG_MAIN_WIDTH; ?>=<font color=green><b><? echo str_replace('px','',$campaign_info['width']); ?></b></font> x <?php echo LANG_MAIN_HEIGHT; ?>=<font color=green><b><? echo str_replace('px','',$campaign_info['height']); ?></b></font>
	</font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td id="mtableright"><?php echo LANG_MAIN_ADFORWARDURL; ?>:</td>
    <td id="mtableleft"><input name="forward_url" type="text" class="webforms" id="forward_url" value="<? echo $forward_url ?>http://" size="50"></td>
  </tr><?php if ($campaign_info["allow_addesc"] == "yes") { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_ADTEXT; ?>:</td>
    <td><textarea name="addesc_url" cols="50" class="webforms" id="addesc_url" onKeyDown="limitText(this.form.addesc_url,this.form.countdown,<? echo $campaign_info["text_limit"] ?>);" onKeyUp="limitText(this.form.addesc_url,this.form.countdown,<? echo $campaign_info["text_limit"] ?>);"><? echo $addesc_url ?></textarea> 
    <?php echo LANG_PAGE_LIMITCHARLEFT; ?> <input readonly type="text" name="countdown" size="3" value="<? echo $campaign_info["text_limit"] ?>">.
	<br />(<font color=red><strong><?php echo LANG_PAGE_NOHTMLCODES; ?></strong></font>)</td>
  </tr><?php } ?>
<? } if($_POST['adtyid_url'] == 'textlink') { ?>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_TITLE; ?></td>
    <td id="mtableleft"><input name="adtitle_url" type="text" id="adtitle_url" onkeyup="document.getElementById('adtitle').innerHTML=this.value" value="<? echo $adtitle_url ?>" size="25"></td>
  </tr>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_ADTEXT; ?>:</td>
    <td id="mtableleft"><textarea onKeyDown="limitText(this.form.addesc_url,this.form.countdown,<? echo $campaign_info["text_limit"] ?>);" onkeyup="document.getElementById('addesc').innerHTML=this.value" name="addesc_url" cols="50" id="addesc_url" ><? echo $addesc_url ?></textarea> 
    <?php echo LANG_PAGE_LIMITCHARLEFT; ?> <input readonly type="text" name="countdown" size="3" value="<? echo $campaign_info["text_limit"] ?>">.
	<br />(<font color=red><strong><?php echo LANG_PAGE_NOHTMLCODES; ?></strong></font>)</td>
  </tr>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_ADFORWARDURL; ?></td>
    <td id="mtableleft"><input onkeyup="document.getElementById('adforward').innerHTML=this.value" name="forward_url" type="text" id="forward_url" value="http://<? echo $forward_url ?>" size="50"></td>
  </tr>
  <tr>
    <td id="mtableright" valign="top"><?php echo LANG_MAIN_ADFORWARDURL2; ?></td>
    <td id="mtableleft"><input name="adurl_url" type="text" id="adurl_url" onkeyup="document.getElementById('adurl').innerHTML=this.value" value="<? echo $adurl_url ?>" size="25"> 
    <?php echo LANG_MAIN_ADFORWARDURL3; ?></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td id="mtableright" valign="top"></td>
    <td>
<?php if($campaign_info['tlstyle'] == ''){ ?>
<style>
div#boxcpn<? echo $campaign_info['id']; ?> {
	overflow:hidden;
	width:auto;
	height:auto;
	border:1px solid #ccc;
	padding:1px;
}
li#spacecpn<? echo $campaign_info['id']; ?> {
	padding-right:8px;
	padding-bottom:8px;
	float: left;
	width:auto;
}
.ul1cpn<? echo $campaign_info['id']; ?> {
    margin: 0;
	padding: 1px 2px 1px 2px;
    text-align: left;
    float: left;
    list-style: none;
}
.adtitlecpn<? echo $campaign_info['id']; ?> {
    color: #0000FF;
	font-weight:bold;
	font-family: 'TrebuchetMS', 'Trebuchet MS', sans-serif;
	font-size: 13px;
	text-decoration:underline;
}
.adtitlecpn<? echo $campaign_info['id']; ?> a {
	color: #0000FF;
	text-decoration: underline;
}
.adtitlecpn<? echo $campaign_info['id']; ?> a:link {
	color: #0000FF;
	text-decoration: underline;
}
.adtitlecpn<? echo $campaign_info['id']; ?> a:visited {
	text-decoration: underline;
	color: #0000FF;
}
.adtitlecpn<? echo $campaign_info['id']; ?> a:hover {
	text-decoration: underline;
	color: #0000FF;
}
.adtitlecpn<? echo $campaign_info['id']; ?> a:active {
	text-decoration: underline;
	color: #0000FF;
}
.addesccpn<? echo $campaign_info['id']; ?> {
    color: #333333;
	font-family: 'TrebuchetMS', 'Trebuchet MS', sans-serif;
	font-size: 12px;
}
.adurlcpn<? echo $campaign_info['id']; ?> {
    color: #429344;
	font-family: 'TrebuchetMS', 'Trebuchet MS', sans-serif;
	font-size: 12px;
}
</style>
<? } else { echo '<style>
'.$campaign_info['tlstyle'].'
</style>'; } ?>

<div id="boxcpn<? echo $campaign_info['id']; ?>">
<ul class="ul1cpn<? echo $campaign_info['id']; ?>">
   <li id="spacecpn<? echo $campaign_info['id']; ?>">
   <span class="adtitlecpn<? echo $campaign_info['id']; ?>" id="adtitle"></span> <br />
   <span class="addesccpn<? echo $campaign_info['id']; ?>" id="addesc"></span> <br />
   <span class="adurlcpn<? echo $campaign_info['id']; ?>" id="adurl"></span>
   </li>
</ul>
</div>
	</td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    </tr>
<? } ?>
	<tr>
	  <td>&nbsp;</td>
	  <td><input type="hidden" name="amount" value="<? echo $amount; ?>"></td>
	</tr>
    <tr>
    <td id="mtableright"><input type=button value="<?php echo LANG_MAIN_BACKBUT; ?>" onClick="history.go(-1)"></td>
	<td id="mtableleft"><?php if($_POST['adtyid_url'] == 'banner'){ ?><input name="preview_banner" type="submit" id="preview_banner" value="<?php echo LANG_MAIN_PREVBUT; ?>">
<? } elseif($_POST['adtyid_url'] == 'flash') { ?><input name="preview_banner2" type="submit" id="preview_banner2" value="<?php echo LANG_MAIN_NEXTBUT; ?>">
<? } else { ?><input name="go_to_payment" type="submit" id="go_to_payment" value="<?php echo LANG_MAIN_NEXTBUT; ?>"><? } ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
</table>
</form>