<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################

/*if($_REQUEST['check']==0)
{
if (!headers_sent()) {
    header("Location: index.php?action=unauthorized");
    exit;
} else {
  // use a meta redirect:
  echo '<html><head><meta http-equiv="refresh" content="0;url=index.php?action=unauthorized" /></head><body><a href="index.php?action=unauthorized">';
echo LANG_PAGE_UNAUTHORIZED;
echo '</a>.</body></html>';
  exit;
}
}*/

//Start session
session_start();
$timeout=2100; // Number of seconds until it times out.
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
    // See if the number of seconds since the last visit is larger than the timeout period.
    $duration = time() - (int)$_SESSION['timeout'];
    if($duration > $timeout) {
        // Destroy the session and restart it.
		unset($_SESSION['email']);
		unset($_SESSION['password']);
		unset($_COOKIE['email']);
		unset($_COOKIE['password']);
		setcookie('email', '', time() - 3600);
		setcookie('password', '', time() - 3600);
		$_SESSION = array();
		session_destroy();
        session_start();
    }
}
// Update the timout field with the current time.
$_SESSION['timeout'] = time();
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['email']) || (trim($_SESSION['password']) == '')) {
header("location: index.php?action=logout");
exit();
}
?>