<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
// Total Clicks
$sqryvar="Select sum(clicks) from track";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$tot1=mysql_result($iqryvar,0,0);
$clickserved=$tot1;

// Total Exposures
$sqryvar="Select sum(exp) from track";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$tot2=mysql_result($iqryvar,0,0);
$expserved=$tot2;

// Total Inactive Credits
$sqryvar="Select sum(credits) from ads WHERE status = 'inactive'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$tot3=mysql_result($iqryvar,0,0);
$incredits=$tot3;

// Total Active Credits
$sqryvar="Select sum(credits) from ads WHERE status = 'active'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$tot4=mysql_result($iqryvar,0,0);
$accredits=$tot4;

// # of Unlimited Ads
$t1 = mysql_query("SELECT * FROM ads WHERE unlimited = 'yes'") or die (mysql_error());
$unlicredits = mysql_num_rows($t1);

// # of Advertisers
$t1 = mysql_query("SELECT * FROM users")or die (mysql_error());
$c1 = mysql_num_rows($t1);

// # of Inactive Ads
$t1 = mysql_query("SELECT * FROM ads WHERE status = 'inactive'") or die (mysql_error());
$c2 = mysql_num_rows($t1);

// # of Active Ads
$t1 = mysql_query("SELECT * FROM ads WHERE status = 'active'") or die (mysql_error());
$c3 = mysql_num_rows($t1);

// # of Campaign
$t1 = mysql_query("SELECT * FROM campaigns") or die (mysql_error());
$c4 = mysql_num_rows($t1);


$nowdate = date("Y-m-d");
$wkdate = date("Y-m-d", strtotime("-30 days"));
// Total Exposures (Impressions) Last 30 Days - 1 Month
$sqryvar="Select sum(exp) from track WHERE date BETWEEN '$wkdate' AND '$nowdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totimth=mysql_result($iqryvar,0,0);
$expmth=$totimth;

// Total Clicks Last Last 30 Days - 1 Month
$sqryvar="Select sum(clicks) from track WHERE date BETWEEN '$wkdate' AND '$nowdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totcmth=mysql_result($iqryvar,0,0);
$climth=$totcmth;

$wkdate = date("Y-m-d", strtotime("-7 days"));
// Total Exposures (Impressions) Last 7 Days
$sqryvar="Select sum(exp) from track WHERE date BETWEEN '$wkdate' AND '$nowdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totiwk=mysql_result($iqryvar,0,0);
$expwk=$totiwk;

// Total Clicks Last 7 Days
$sqryvar="Select sum(clicks) from track WHERE date BETWEEN '$wkdate' AND '$nowdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totcwk=mysql_result($iqryvar,0,0);
$cliwk=$totcwk;

$yestdate = date("Y-m-d", strtotime("-1 days"));
// Total Exposures (Impressions) Yesterday
$sqryvar="Select sum(exp) from track WHERE date = '$yestdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totiy=mysql_result($iqryvar,0,0);
$expyest=$totiy;

// Total Clicks Yesterday
$sqryvar="Select sum(clicks) from track WHERE date = '$yestdate'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totcy=mysql_result($iqryvar,0,0);
$cliyest=$totcy;

$todayis = date("Y-m-d");
// Total Exposures (Impressions) Today
$sqryvar="Select sum(exp) from track WHERE date = '$todayis'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$toti=mysql_result($iqryvar,0,0);
$exptoday=$toti;

// Total Clicks Today
$sqryvar="Select sum(clicks) from track WHERE date = '$todayis'";
$iqryvar=mysql_query($sqryvar)or die (mysql_error());
$totc=mysql_result($iqryvar,0,0);
$clitoday=$totc;

$mclicks = LANG_MAIN_CLICKS;
$mimpressions = LANG_MAIN_IMPRESSIONS;
$liy = LANG_ADMIN_MYTIMPS;
$lcy = LANG_ADMIN_MYTDAYS;
$lit = LANG_ADMIN_TTIMPS;
$lct = LANG_ADMIN_TTCLICKS;


if ($conf["show_mstats"] == "yes")
  { 
?>
<table id="main">
	<thead>
		<th><?=$mimpressions?></th>
		<th><?=$liy?></th>
		<th><?=$lit?></th>
		<th><?=$mclicks?></th>
		<th><?=$lcy?></th>
		<th><?=$lct?></th>
	</thead>
<tbody>
	<tr>
		<td id="tablecenter"><?=$expserved?></td>
		<td id="tablecenter"><?=$expyest?></td>
		<td id="tablecenter"><?=$exptoday?></td>
		<td id="tablecenter"><?=$clickserved?></td>
		<td id="tablecenter"><?=$cliyest?></td>
		<td id="tablecenter"><?=$clitoday?></td>
	</tr>
</tbody>
</table>
<? } ?>