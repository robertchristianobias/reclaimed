<?
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
include_once ('lgcheck.php'); ?>
<?
if ($_REQUEST['payment_method'] == "paypal")
{
	  ?>
	  
	  <form action= "https://www.paypal.com/cgi-bin/webscr/" method="post">
	  <input type="hidden" name="cmd" value="_xclick">
	  <input type="hidden" name="business" value="<? echo $conf2["pp"] ?>">
	  <input type="hidden" name="item_name" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	  <input type="hidden" value="2" name="rm">
	  <input type="hidden" name="amount" value="<? echo $amount ?>">
	  <input type="hidden" name="return" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	  <input type="hidden" name="cancel_return" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">
	  <input type="hidden" name="currency_code" value="USD">
	  <input type="submit" name="I3" value="<?php echo LANG_PAGE_PAYBUT; ?>">
       </form>
	   
	  <?
}
if ($_REQUEST['payment_method'] == "authorizenet")
{

// -- AUTHORIZE.NET -- //
$loginID		= $conf2["an"];
$transactionKey = $conf2["antk"];
//$amount = $campaign_info["priceusd"];
$timeStamp = time();
$sequence = $banner_id;
// The following lines generate the SIM fingerprint. PHP versions 5.1.2 and newer have the necessary hmac function built in.
//For older versions, it will try to use the mhash library.
if( phpversion() >= '5.1.2' )
	{ $fingerprint = hash_hmac("md5", $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey); }
else 
	{ $fingerprint = bin2hex(mhash(MHASH_MD5, $loginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^", $transactionKey)); }
// -- AUTHORIZE.NET -- //
	  ?>
	  
       <form method='post' action='https://secure.authorize.net/gateway/transact.dll' />
	  <input type='hidden' name='x_login' value='<? echo $loginID ?>' />
	  <input type='hidden' name='x_amount' value='<? echo $amount ?>' />
	  <input type='hidden' name='x_email' value='<? echo $user_rows["email"] ?>' />
	  <input type='hidden' name='x_description' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>' />
	  <input type='hidden' name='x_invoice_num' value='<? echo $_POST['transaction_url']; ?>' />
	  <input type='hidden' name='x_fp_sequence' value='<? echo $sequence ?>' />
	  <input type='hidden' name='x_fp_timestamp' value='<?php echo $timeStamp; ?>' />
	  <input type='hidden' name='x_fp_hash' value='<?php echo $fingerprint; ?>' />
	  <input type='hidden' name='x_relay_response' value='TRUE' />
	  <input type='hidden' name='x_relay_url' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>' />
	  <input type='hidden' name='x_show_form' value='PAYMENT_FORM' />
	  <input type='submit' name="I3a" value='<?php echo LANG_PAGE_PAYBUT; ?>' />
       </form>
		
	   <?
}
if ($_REQUEST['payment_method'] == "payza")
{
		?>
	
     <form action='https://secure.payza.com/checkout' method='post'>
	  <input type='hidden' name='ap_purchasetype' value='Item'>
	  <input type='hidden' name='ap_merchant' value='<? echo $conf2["ap"] ?>'>
	  <input type='hidden'  name='ap_itemname' value='<? echo $_POST['transaction_url']; ?> <? echo $campaign_info["title"] ?>'>
	  <input type='hidden'  name='ap_currency' value='USD'>
	  <input type='hidden'  name='ap_returnurl' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
	  <input type='hidden'  name='ap_cancelurl' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
	  <input type='hidden' name='ap_description' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
	  <input type='hidden'  name='ap_amount' value='<? echo $amount ?>'>
	  <input type="submit" name="I1" value="<?php echo LANG_PAGE_PAYBUT; ?>">
	</form>
	

			   <?
}
if ($_REQUEST['payment_method'] == "solidtrustpay")
{
		?>
	
	<form action='http://solidtrustpay.com/handle.php' method='post'>
	     <input type='hidden' name='merchantAccount' value='<? echo $conf2["stp"] ?>'>
	     <input type='hidden' name='amount' value='<? echo $amount ?>'>
		 <input type='hidden' name='currency' value='USD' />
	     <input type='hidden' name='item_id' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
	     <input type='hidden' name='return_url' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
	     <input type='hidden' name='cancel_url' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
	     <input type='submit' name='I5' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
	
	   <?
}
if ($_REQUEST['payment_method'] == "cgold")
{
		?>
	
	<form action='https://c-gold.com/clicktopay/' method='post'>
	  <input type='hidden' name='payee_account' value='<? echo $conf2["cg"] ?>'>
	  <input type='hidden' name='payee_name' value='<? echo $conf["site_name"] ?>'>
	  <input type='hidden' name='payment_amount' value='<? echo $amount ?>'>
	  <input type='hidden' name='payment_units' value='USD worth'>
	  <input type='hidden' name='payment_url' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
	  <input type='hidden' name='payment_url_method' value='post'>
	  <input type='hidden' name='nopayment_url' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
	  <input type='hidden' name='nopayment_url_method' value='post'>
	  <input type='hidden' name='suggested_memo' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
	  <input type='submit' name='I7' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
  	</form>
	
	   <?
}
if ($_REQUEST['payment_method'] == "pecunix")
{
		?>
	
	<form action="https://pri.pecunix.com/money.refined" method='post'>
	  <input type='hidden' name='PAYMENT_AMOUNT' value='<? echo $amount ?>'>  
	  <input type='hidden' name='PAYMENT_UNITS' value='USD'>
	  <input type='hidden' name='PAYMENT_URL' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
	  <input type='hidden' name='NOPAYMENT_URL' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
	  <input type='hidden' name='PAYEE_ACCOUNT' value='<? echo $conf2["pu"] ?>'>
	  <input type='hidden' name='SUGGESTED_MEMO' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
		  <input type='submit' name='I9' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
		
	   <?
}
if ($_REQUEST['payment_method'] == "perfectmoney")
{
		?>
	
	<form action="https://perfectmoney.com/api/step1.asp" method='post'>
	  <input type='hidden' name='PAYEE_ACCOUNT' value='<? echo $conf2[pm] ?>'>
	  <input type='hidden' name='PAYEE_NAME' value='<? echo $conf["site_name"] ?>'>
	  <input type='hidden' name='PAYMENT_AMOUNT' value='<? echo $amount ?>'> 
	  <input type='hidden' name='PAYMENT_UNITS' value='USD'>  
	  <input type='hidden' name='PAYMENT_URL' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
	  <input type='hidden' name='NOPAYMENT_URL' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
	  <input type='hidden' name='SUGGESTED_MEMO' value='#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
	  <input type='submit' name='I11' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
		
	   <?
}
if ($_REQUEST['payment_method'] == "moneybookers")
{
		?>
	
	<form action="https://www.moneybookers.com/app/payment.pl" method="post">
	  <input type="hidden" name="pay_to_email" value="<? echo $conf2["mb"] ?>">
		  <input type="hidden" name="recipient_description" value="<? echo $conf["site_name"] ?>">
	  <input type="hidden" name="language" value="EN">
	  <input type="hidden" name="amount" value="<? echo $amount ?>">
	  <input type="hidden" name="currency" value="USD">
		  <input type="hidden" name="return_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
		  <input type="hidden" name="cancel_url" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">
	  <input type="hidden" name="detail1_description" value="Description:">
	  <input type="hidden" name="detail1_text" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	  <input type="hidden" name="confirmation_note" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	  <input type='submit' name='I13' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
	
	   <?
}
if ($_REQUEST['payment_method'] == "hdmoney")
{
		?>
	
	<form method="post" action="https://www.hd-money.com/secured/Payment.aspx">
	  <input type="hidden" name="hd_item_name" value="<? echo $banner_id ?>">
	  <input type="hidden" name="hd_amount" value="<? echo $amount ?>">
	  <input type="hidden" name="hd_recipient" value="<? echo $conf2["hdm"] ?>">
	  <input type="hidden" name="hd_order_id" value="<? echo $_POST['transaction_url']; ?>">
	  <input type="hidden" name="hd_return_url" value="<? echo $conf["site_url"] ?>index.php">
		  <input type="hidden" name="hd_process_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	  <input type="hidden" name="hd_description" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>"> 
	  <input type='submit' name='I18' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
		
	   <?
}
if ($_REQUEST['payment_method'] == "2checkout")
{
		?>
	
	<form method="post" action="https://www.2checkout.com/checkout/spurchase">
	  <input type="hidden" name="id_type" value="1">
	  <input type="hidden" name="tco_currency" value="USD">
	  <input type="hidden" name="total" value="<? echo $amount ?>">
	  <input type="hidden" name="sid" value="<? echo $conf2["tco"] ?>">
	  <input type="hidden" name="cart_order_id" value="AD#<? echo $_POST['transaction_url']; ?>">
		  <input type="hidden" name="c_prod" value="AD-<? echo $_POST['transaction_url']; ?>">
	  <input type="hidden" name="x_receipt_link_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	  <input type="hidden" name="c_description" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	  <input type="hidden" name="x_Description" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>"> 
		  <input type="hidden" name="c_name" value="AD#<? echo $_POST['transaction_url']; ?> <? echo $campaign_info["title"] ?>">
		  
	  <input type='submit' name='I19' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
		
	   <?
}
if ($_REQUEST['payment_method'] == "googlecheckout")
{
		?>
		
	<form action= "https://checkout.google.com/cws/v2/Merchant/<? echo $conf2["gco"] ?>/checkoutForm" id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm">
	  <input type="hidden" name="item_name_1" value="<? echo $campaign_info["title"] ?>">
	  <input type="hidden" name="item_description_1" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>"/>
	  <input type="hidden" name="item_quantity_1" value="1">
	  <input type="hidden" name="item_price_1" value="<? echo $amount ?>">
	  <input type="hidden" name="item_currency_1" value="USD">
		  <input type="hidden" name="continue_shopping_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>"> 
	  <input type="hidden" name="_charset_" value="utf-8">
	  <input type='submit' name='I20' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form>
<?
}
if ($_REQUEST['payment_method'] == 'eurogoldcash')
{
?>
		
	<form method="POST" action="http://sci.eurogoldcash.com">
	  <input type="hidden" name="egc_acc" value="<? echo $conf2["egc"] ?>">
	  <input type="hidden" name="egc_store" value="<? echo $conf2[egcstore] ?>">
	  <input type="hidden" name="egc_success_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	  <input type="hidden" name="egc_success_url_method" value="1">
	  <input type="hidden" name="egc_fail_url" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">
	  <input type="hidden" name="egc_fail_url_method" value="1"> 
	  <input type="hidden" name="egc_amnt" value="<? echo $amount ?>">
	  <input type="hidden" name="egc_comments" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	  <input type="hidden" name="egc_currency" value="1">
	  <input type='submit' name='I25' value='<?php echo LANG_PAGE_PAYBUT; ?>'>
	</form> 
		
<?
}
if ($_REQUEST['payment_method'] == 'coinbin')
{
?>
<form action="https://coinb.in/cart/" method="GET">
	<input type="hidden" name="uid" value="<? echo $conf2["cb"] ?>">
	<input type="hidden" name="currency" value="USD">
	<input type="hidden" name="callback" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	<input type="hidden" name="return" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	<input type="hidden" name="checkout" value="false">
	<!-- item one -->
	<input type="hidden" name="item[1][name]" value="#<? echo $_POST['transaction_url']; ?>">
	<input type="hidden" name="item[1][description]" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	<input type="hidden" name="item[1][value]" value="<? echo $amount ?>">
	<input type="hidden" name="item[1][quantity]" value="1">
	<input type="hidden" name="item[1][custom_var1]" value="#<? echo $_POST['transaction_url']; ?>">
	<input type="submit" name="I2" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'paxum')
{
?>
<form action="https://paxum.com/payment/phrame.php?action=displayProcessPaymentLogin" method="post">
<input type="hidden" name="business_email" value="<? echo $conf2["pax"] ?>" />
<input type="hidden" name="button_type_id" value="1" />
<input type="hidden" name="item_id" value="<? echo $_POST['transaction_url']; ?>" />
<input type="hidden" name="item_name" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>" />
<input type="hidden" name="amount" value="<? echo $amount ?>" />
<input type="hidden" name="currency" value="USD" />
<input type="hidden" name="ask_shipping" value="0" />
<input type="hidden" name="cancel_url" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>" />
<input type="hidden" name="finish_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>" />
<input type="submit" name="I33" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'paymate')
{
?>
<form action="https://www.paymate.com/PayMate/ExpressPayment" method="post">
<input type="hidden" id="mid" name="mid" value="<? echo $conf2["paym"] ?>">
<input type="hidden" id="logo" name="logo" value="">
<input type="hidden" id="amt" name="amt" value="<? echo $amount ?>">
<input type="hidden" id="amt_editable" name="amt_editable" value="N">
<input type="hidden" id="pmt_item_name" name="pmt_item_name" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
<input type="hidden" id="currency" name="currency" value="USD">
<input type="hidden"id="shipping" name="shipping" value="0">
<input type="hidden" id="variableShipping" name="variableShipping" value="0">
<input type="hidden" id="return" name="return" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
<input type="hidden" id="ref" name="ref" value="<? echo $_POST['transaction_url']; ?>">
<input type="submit" name="I36" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'webmoney')
{
?>
<form method="POST" action="https://merchant.wmtransfer.com/lmi/payment.asp">
	<input type="hidden" name="LMI_PAYMENT_AMOUNT" value="<? echo $amount ?>">
	<input type="hidden" name="LMI_PAYMENT_DESC" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
	<input type="hidden" name="LMI_PAYMENT_NO" value="<? echo $_POST['transaction_url']; ?>">
	<input type="hidden" name="LMI_PAYEE_PURSE" value="<? echo $conf2["wm"] ?>">
	<input type="hidden" name="LMI_SIM_MODE" value="0">
	<input type="hidden" name="LMI_SUCCESS_URL" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	<input type="hidden" name="LMI_SUCCESS_METHOD" value="2">
	<input type="hidden" name="LMI_FAIL_URL" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">
	<input type="hidden" name="LMI_FAIL_METHOD" value="2">
	<input type="submit" name="I37" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'okpay')
{
?>
<form method="post" action="https://www.okpay.com/process.html">
	<input type="hidden" name="ok_receiver" value="<? echo $conf2["okp"] ?>" />
	<input type="hidden" name="ok_item_1_name" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>" />
	<input type="hidden" name="ok_currency" value="usd" />
	<input type="hidden" name="ok_item_1_type" value="service" />
	<input type="hidden" name="ok_item_1_price" value="<? echo $amount ?>" />
	<input type="hidden" name="ok_return_success" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">
	<input type="hidden" name="ok_return_fail" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">
	<input type="submit" name="I38" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'dwolla')
{
$key = $conf2['dwok'];
$secret = $conf2['dwos'];
$timestamp = time();
$order_id = $banner_id;
$signature = hash_hmac('sha1', "{$key}&{$timestamp}&{$order_id}", $secret);
?>
<form accept-charset="UTF-8" action="https://www.dwolla.com/payment/pay" method="post">
    <input id="key" name="key" type="hidden" value="<? echo $key; ?>" />
    <input id="signature" name="signature" type="hidden" value="<? echo $signature; ?>" />
    <input id="redirect" name="redirect" type="hidden" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>" />
    <input id="name" name="name" type="hidden" value="#<? echo $_POST['transaction_url']; ?> <? echo $campaign_info["title"] ?>" />
    <input id="description" name="description" type="hidden" value="<? echo $conf["site_name"] ?> - #<? echo $banner_id ?> - <? echo $campaign_info["title"] ?>" />
    <input id="destinationid" name="destinationid" type="hidden" value="<? echo $conf2["dwo"] ?>" />
    <input id="amount" name="amount" type="hidden" value="<? echo $amount ?>" />
    <input id="shipping" name="shipping" type="hidden" value="0.00" />
    <input id="tax" name="tax" type="hidden" value="0.00" />
    <input id="orderid" name="orderid" type="hidden" value="<? echo $order_id; ?>" />
    <input id="timestamp" name="timestamp" type="hidden" value="<? echo $timestamp; ?>" />
    <input type="submit" name="I39" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'egopay')
{
require('includes/EgoPaySci.php');
try {
    $oEgopay = new EgoPaySci(array(
        'store_id'          => "".$conf2[ego]."",
        'store_password'    => "".$conf2[ego2]."",
    ));
    $sPaymentHash = $oEgopay->createHash(array(
        'amount'    => $amount,
        'currency'  => 'USD',
        'description' => "".$conf['site_name']." - #".$_POST['transaction_url']."",
        'fail_url'	=> "".$conf["site_url"]."index.php?action=complete&email=".$user_rows["email"]."",
        'success_url'	=> "".$conf["site_url"]."index.php?action=complete&email=".$user_rows["email"]."&id=".$banner_id."",
    ));
} catch (EgoPayException $e) {
    die($e->getMessage());
}
?>
<form action="<?php echo EgoPaySci::EGOPAY_PAYMENT_URL; ?>" method="post">    
    <input type="hidden" name="hash" value="<?php echo $sPaymentHash ?>" />
	<input type="submit" name="I40" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'sensipay')
{
?>
<form action='https://www.sensipay.com/users/buyerLogin' method='Post'>
<input type='hidden' name='sp_acc' value='<? echo $conf2["se"] ?>'>
<input type='hidden' name='sp_amount' value='<? echo $amount ?>'>
<input type='hidden' name='sp_currency' value='USD'>
<input type='hidden' name='sp_success_url' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
<input type='hidden' name='sp_success_url_method' value='Post'>
<input type='hidden' name='sp_fail_url' value='<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>'>
<input type='hidden' name='sp_fail_url_method' value='Post'>
<input type='hidden' name='sp_notify_url' value='<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>'>
<input type='hidden' name='sp_store' value='<? echo $conf2["sestore"] ?>'>
<input type='hidden' name='sp_details' value='<? echo $_POST['transaction_url']; ?>'>
<input type='hidden' name='prod' value='<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>'>
<input type="submit" name="I41" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'worldpay')
{
?>
<form action="https://secure.worldpay.com/wcc/purchase" method=POST>
 <input type="hidden" name="testMode" value="0">
 <input type="hidden" name="instId" value="<? echo $conf2["wp"] ?>">
 <input type="hidden" name="cartId" value="<? echo $_POST['transaction_url']; ?>">
 <input type="hidden" name="amount" value="<? echo $amount ?>">
 <input type="hidden" name="currency" value="USD">
 <input type="hidden" name="desc" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
 <input type="hidden" name="email" value="<? echo $user_rows["email"] ?>">
 <input type="submit" name="I42" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form>
<?
}
if ($_REQUEST['payment_method'] == 'coinpayments')
{
?>
<form action="https://www.coinpayments.net/index.php" method="post">
  <input type="hidden" name="cmd" value="_pay">
  <input type="hidden" name="reset" value="1">
  <input type="hidden" name="want_shipping" value="0">
  <input type="hidden" name="merchant" value="<? echo $conf2["cp"] ?>">
  <input type="hidden" name="currency" value="USD">
  <input type="hidden" name="amountf" value="<? echo $amount ?>">
  <input type="hidden" name="item_name" value="#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>">
  <input type="hidden" name="item_number" value="<? echo $_POST['transaction_url']; ?>">
  <input type="hidden" name="allow_extra" value="1">  
  <input type="hidden" name="success_url" value="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>">  
  <input type="hidden" name="cancel_url" value="<? echo $conf["site_url"] ?>index.php?action=cancel&email=<? echo $user_rows["email"] ?>&id=<? echo $banner_id ?>">  
  <input type="submit" name="I44" value="<?php echo LANG_PAGE_PAYBUT; ?>">
</form><?
}
if ($_REQUEST['payment_method'] == 'btcmerch')
{
?>
    <iframe id="framesample" src="https://www.btcmerch.com/embedded-payment?merchant_id=<? echo $conf2["bm"] ?>&amount=<? echo $amount ?>&currency_code=USD&item_name=#<? echo $_POST['transaction_url']; ?> <? echo $conf["site_name"] ?> <? echo $campaign_info["title"] ?>&item_number=<? echo $_POST['transaction_url']; ?>"
    frameborder="0" allowtransparency="true" scrolling="auto"
    style="width: 500px; height: 165px; border: none; box-shadow: 0px 0px 8px rgba(0,0,0,0.3); overflow: auto;">
    </iframe>
<? } if ($_REQUEST['payment_method'] == 'otherpayments') { ?>
<input type="submit" name="submit" value="<? echo LANG_MAIN_SUBMBUT; ?>" onClick=location.href="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>"> 
<? } if($_REQUEST['payment_method'] == 'free') { ?>
<input type="submit" name="submit" value="<? echo LANG_MAIN_SUBMBUT; ?>" onClick=location.href="<? echo $conf["site_url"] ?>index.php?action=complete&email=<? echo $user_rows["email"] ?>"> 
<? } ?>