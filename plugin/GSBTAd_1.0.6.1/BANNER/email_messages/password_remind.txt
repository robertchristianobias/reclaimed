Hello,

You have recently requested to have your login information emailed to this address. 

Your username is: {user}
Your password is: {pass}


You can login at:
%{site_url}index.php?action=login

If you think that you did not request for this info, please disregard this message and accept our apologies for taking your time.

Thank you and good day!

Sincerely,
%{site_name}