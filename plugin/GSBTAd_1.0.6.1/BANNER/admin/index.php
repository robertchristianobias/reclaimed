<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
session_start();
header('Cache-control: private'); // IE 6 FIX
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	ob_start("ob_gzhandler");
else
	ob_start();
//setup php for working with Unicode data
header('Content-Type: text/html; charset=utf-8');
ini_set('default_charset', 'utf-8');
mb_detect_order('utf-8');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');
ob_start('mb_output_handler');
//---- Turn off all error reporting ---- 
error_reporting(0);
//---- display no errors ---- 
@ini_set(�display_errors�, 0);
//---- Turn on error reporting to all but notices ---- 
//error_reporting(E_ALL ^ E_NOTICE);	
// Insert this block of code at the very top of your page:
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;

include ('../includes/connect.php');
include ('includes/functions.php');
// *************************************************************
// QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK
// provided by AirCraft24.com / www.aircraft24.com
// version 1.8, released 2013-02-04 15:05 GMT+1
// *************************************************************
$phpbug_53632_vars='';
if (isset($_GET))    $phpbug_53632_vars.='|'.serialize($_GET);
if (isset($_POST))   $phpbug_53632_vars.='|'.serialize($_POST);
if (isset($_COOKIE)) $phpbug_53632_vars.='|'.serialize($_COOKIE);

if ($phpbug_53632_vars!='') 
{
  if (strpos(str_replace('.','',$phpbug_53632_vars), '22250738585072013')!== FALSE) 
  {
    if (!headers_sent()) header('Status: 422 Unprocessable Entity');
    die ('Script interrupted due to floating point DoS attack.');
  }
}
unset($phpbug_53632_vars); // cleanup

// *************************************************************
// END QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK	
// *************************************************************
//--------------------------------------------------------------------------------------------------
// Login check code is added here
if(!$_SESSION['login_id'] || $_SESSION['timeout2'] + 50 * 60 < time())
	{
		unset($_SESSION['msg']);
		$logininfo = LANG_ADMIN_LOGININFO;
		$message = $logininfo;
		header("Location: login.php ");
		exit;
	}
//---------------------------------------------------------------------------------------------------
include ('includes/ver.php');
include ('includes/head.php');
include ('includes/forms.php'); 
include ('includes/menu.php');
// Do the following silent cron jobs
include_once ('includes/ads_cron.php');
if ($conf['gcurrency'] == 'yes'){include_once ('includes/currs_cron.php');}

$pathtofolder = $conf["absolute_path"];
if (is_writable("$pathtofolderincludes"))
{
	$incwriteable = LANG_ADMIN_INCWRITE;
	echo "<div align=center><img src=\"img/icon_alert.gif\" alt=\"alert\" width=\"16\" height=\"15\" border=\"0\"><font size=2 color=red><b>$incwriteable</b></font><img src=\"img/icon_alert.gif\" alt=\"alert\" width=\"16\" height=\"15\" border=\"0\"></div>";
}
if (file_exists("../install"))
{
	$inswriteable = LANG_ADMIN_INSWRITE;
	echo "<div align=center><img src=\"img/icon_alert.gif\" alt=\"alert\" width=\"16\" height=\"15\" border=\"0\"><font size=2 color=red><b>$inswriteable</b></font><img src=\"img/icon_alert.gif\" alt=\"alert\" width=\"16\" height=\"15\" border=\"0\"></div>";
}

if ($_GET['action'] == "")
{
	include ('includes/main.php');
}
if ($_GET['action'] == "home")
{
	include ('includes/main.php');
}
if ($_GET['action'] == "users")
{
	include ('includes/users.php');
}
if ($_GET['action'] == "newaduser")
{
	include ('includes/newaduser.php');
}
if ($_GET['action'] == "emailadvers")
{
	include ('includes/emailadvers.php');
}
if ($_GET['action'] == "adswaiting")
{
	include ('includes/aprdenyads.php');
}
if ($_GET['action'] == "settings")
{
	include ('includes/settings.php');
}
if ($_GET['action'] == "htmls")
{
	include ('includes/htmls.php');
}
if ($_GET['action'] == "campaign")
{
	include ('includes/campaign.php');
}
if ($_GET['action'] == "campaigns")
{
	include ('includes/campaigns.php');
}
if ($_GET['action'] == "newcampaign")
{
	include ('includes/newcampaign.php');
}
if ($_GET['action'] == "paylist")
{
	include ('includes/paylist.php');
}
if ($_GET['action'] == "paysettings")
{
	include ('includes/paysettings.php');
}
if ($_GET['action'] == "cursettings")
{
	include ('includes/cursettings.php');
}
if ($_GET['action'] == "adminuser")
{
	include ('includes/adminuser.php');
}
if ($_GET['action'] == "upgrade")
{
	include ('includes/upgrade.php');
}
if ($_GET['action'] == "banners")
{
	include ('includes/banners.php');
}
if ($_GET['action'] == "editad")
{
	include ('includes/editad.php');
}
if ($_GET['action'] == "previewad")
{
	include ('includes/previewad.php');
}
if ($_GET['action'] == "stats")
{
	include ('includes/stats.php');
}
if ($_GET['action'] == "howtos")
{
	include ('includes/howtos.php');
}
if ($_GET['action'] == "clickslog")
{
	include ('includes/clickslog.php');
}
if ($_GET['action'] == "coupons")
{
	include ('includes/coupons.php');
}
if ($_GET['action'] == "coupon")
{
	include ('includes/coupon.php');
}
if ($_GET['action'] == "denyips")
{
	include ('includes/denyips.php');
}
if ($_GET['action'] == "denycountries")
{
	include ('includes/denycountries.php');
}
if ($_GET['action'] == "stats30days")
{
	include ('includes/stats30days.php');
}
include ('includes/foot.php');
?>