<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
 session_start();
 unset($_SESSION['login_id']);
 unset($_SESSION['login_pass']);
 unset($_COOKIE['login_id']);
 unset($_COOKIE['login_pass']);
 setcookie('login_id', '', time() - 3600);
 setcookie('login_pass', '', time() - 3600);
 $_SESSION = array();
 session_destroy();
 $login = LANG_ADMIN_LOGININFO;
 $message="$login";
 header("Location: login.php");
 exit;
?>