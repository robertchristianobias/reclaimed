<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
session_start();
header('Cache-control: private'); // IE 6 FIX

if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	ob_start("ob_gzhandler");
else
	ob_start();
//---- Turn off all error reporting ---- 
error_reporting(0);
//---- display no errors ---- 
@ini_set(‘display_errors’, 0);
//---- Turn on error reporting to all but notices ---- 
//error_reporting(E_ALL ^ E_NOTICE);
include ('../includes/connect.php');
include ('includes/functions.php');
include ('includes/ver.php');
include ('includes/headless.php');
//include ('includes/language.php');
include ('includes/forms.php');
// *************************************************************
// QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK
// provided by AirCraft24.com / www.aircraft24.com
// version 1.8, released 2013-02-04 15:05 GMT+1
// *************************************************************
$phpbug_53632_vars='';
if (isset($_GET))    $phpbug_53632_vars.='|'.serialize($_GET);
if (isset($_POST))   $phpbug_53632_vars.='|'.serialize($_POST);
if (isset($_COOKIE)) $phpbug_53632_vars.='|'.serialize($_COOKIE);

if ($phpbug_53632_vars!='') 
{
  if (strpos(str_replace('.','',$phpbug_53632_vars), '22250738585072013')!== FALSE) 
  {
    if (!headers_sent()) header('Status: 422 Unprocessable Entity');
    die ('Script interrupted due to floating point DoS attack.');
  }
}
unset($phpbug_53632_vars); // cleanup
// *************************************************************
// END QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK	
// *************************************************************
include ('includes/menu.php');
?>
<br />
<? //if(($_SERVER['HTTP_X_FORWARDED_FOR']) || ($_SERVER['HTTP_USER_AGENT']=='') || ($_SERVER['HTTP_VIA']!='')){ ?>
<div class="row">
	<div class="column grid_4"></div>
	<div class="column grid_4">
				<div id="bob"><?php echo LANG_ADMIN_LOGINAREA; ?></div>
				<div id="bob2">
				<div id="boblgnname"><?php echo LANG_ADMIN_LOGININFO; ?></div>
				<form name="form1" method="post" action="admin_login.php">
				<label><?php echo LANG_ADMIN_USER; ?>:</label><br /><input name="login_id" type="text" id="admintxfldlgn"><br />
				<label><?php echo LANG_MAIN_PASSWORD; ?>:</label><br /><input name="login_pass" type="password" id="admintxfldlgn" class="keyboardInput"><br />
				<button id="adminlgnbut"><?php echo LANG_ADMIN_LOGINBUT; ?></button>
				</form>
				</div>
	</div>
	<div class="column grid_6"></div>
</div>
<? //} ?>
<br />
<br />
<? include ('includes/foot.php'); ?>