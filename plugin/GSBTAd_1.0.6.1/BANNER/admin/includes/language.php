<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
if(isSet($_GET['lang']))
{
$lang = $_GET['lang'];
// register the session and set the cookie
$_SESSION['lang'] = $lang;
setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
$lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
$lang = $_COOKIE['lang'];
}
else
{
$lang = $conf['language']; // the default language
}
switch ($lang) {
  case 'en':
  $lang_file = 'lang.en.php'; // English
  break;
  case 'de':
  $lang_file = 'lang.de.php'; // Deutsch
  break;
  case 'es':
  $lang_file = 'lang.es.php'; // Espa�ol
  break;
  case 'fr':
  $lang_file = 'lang.fr.php'; // French
  break;
  case 'it':
  $lang_file = 'lang.it.php'; // Italian
  break;
  case 'nl':
  $lang_file = 'lang.nl.php'; // Dutch
  break;
  case 'ko':
  $lang_file = 'lang.ko.php'; // Korean
  break;
  case 'jp':
  $lang_file = 'lang.jp.php'; // Japanese
  break;
  default:
  $lang_file = 'lang.en.php'; // the default language
}
include_once '../languages/'.$lang_file;
?>