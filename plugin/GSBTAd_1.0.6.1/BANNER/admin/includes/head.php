﻿<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
//error_reporting(E_ALL);
// # of Inactive Ads
$adswaitingnow = mysql_query("SELECT * FROM ads WHERE status = 'inactive'") or die (mysql_error());
$helloadswaiting = mysql_num_rows($adswaitingnow);
?>
<!DOCTYPE html>
<? include_once ('includes/language.php'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANG_HTML; ?>" lang="<?php echo LANG_HTML; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo LANG_CHARSET_ISO; ?>">
<title>Pay Banner Ad v<? echo $pbaversion ?> <?php echo LANG_ADMIN_LOGINAREA; ?></title>
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/grid.css">
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/main.css">
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<script type="text/javascript" src="<? echo $conf["site_url"]; ?>admin/includes/keyboard.js" charset="UTF-8"></script>
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/keyboard.css">
<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
<script type="text/javascript" src="<? echo $conf["site_url"]; ?>admin/includes/dropdowntabs.js"></script>
<? if ($conf['gtranslate'] == 'yes'){ ?><script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><? } ?>
</head>
<body>
<div class="row">
	<div class="column grid_14" style="text-align:right;">
<font size="2"><?php echo LANG_AMENU_LOGGEDAS; ?>: <strong><font color="red"><?php echo LANG_AMENU_ADMINR; ?></font></strong> [ <a href="logout.php"><strong><?php echo LANG_AMENU_LOGOUT; ?></strong></a> | <a href="index.php?action=adminuser"><strong><?php echo LANG_AMENU_CHGPW; ?></strong></a></font> ]
	</div>
</div>
<div class="row">
	<div class="column grid_14">
		<div class="title">
			<img src="<? echo $conf["site_url"]; ?>admin/img/logo.png" alt="PayBannerAd PHP Script" width="500" height="50" border="0" />
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=en" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/en.png" alt="English" title="English" width="18" height="18" border="0" /></a> 
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=de" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/de.png" alt="Deutsch" title="Deutsch" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=es" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/es.png" alt="Español" title="Español" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=fr" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/fr.png" alt="Française" title="Française" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=it" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/it.png" alt="Italiano" title="Italiano" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=nl" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/nl.png" alt="Nederlandse" title="Nederlandse" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=ko" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/ko.png" alt=" 한국어" title=" 한국어" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=jp" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/jp.png" alt="日本語" title="日本語" width="18" height="18" border="0" /></a>
		</div>

<div id="tabsK" class="slidetabsmenu">
  <ul>
	<li><a href="index.php" title="Home"><span><?php echo LANG_AMENU_HOME; ?></span></a></li>
	<li><a href="index.php?action=adswaiting" title="Ads Waiting"><span>(<font color="yellow"><? echo $helloadswaiting; ?></font>) <?php echo LANG_AMENU_ADWAIT; ?>!</span></a></li>
	<li><a href="#" rel="dropmenu1_c"><span><?php echo LANG_AMENU_ADVERTISERS; ?></span></a></li>
	<li><a href="#" rel="dropmenu2_c"><span><?php echo LANG_AMENU_CAMPAIGNS; ?></span></a></li>
	<li><a href="#" rel="dropmenu3_c"><span><?php echo LANG_AMENU_SETTINGS; ?></span></a></li>
	<li><a href="#" rel="dropmenu4_c"><span><?php echo LANG_AMENU_COUPONS; ?></span></a></li>
	<li><a href="#" rel="dropmenu5_c"><span><?php echo LANG_AMENU_EXTRAS; ?></span></a></li>
	<li><a href="index.php?action=upgrade" title="Upgrade"><span><?php echo LANG_AMENU_UPGRADE; ?></span></a></li>
  </ul>
</div>

<br style="clear: left;" />
<br class="IEonlybr" />                                                
<div id="dropmenu1_c" class="dropmenudiv_c">
<a href="index.php?action=emailadvers" title="Email Advertisers"><?php echo LANG_AMENU_EMAILAD; ?></a>
<a href="index.php?action=users" title="Manage Advertisers"><?php echo LANG_AMENU_MANADV; ?></a>
<a href="index.php?action=newaduser&type=banner" title="Start a new banner ad"><?php echo LANG_ADMIN_NEWBANAD; ?></a>
<a href="index.php?action=newaduser&type=textlink" title="Start a new text link ad"><?php echo LANG_ADMIN_NEWTEXTAD; ?></a>
<a href="index.php?action=newaduser&type=flash" title="Start a new flash advert"><?php echo LANG_ADMIN_NEWSWFAD; ?></a>
<a href="index.php?action=newaduser&type=custom" title="Start a new custom advert"><?php echo LANG_ADMIN_NEWCUSAD; ?></a>
</div>                                           
<div id="dropmenu2_c" class="dropmenudiv_c">
<a href="index.php?action=campaigns" title="Manage Campaigns"><?php echo LANG_AMENU_MANCAM; ?></a>
<a href="index.php?action=newcampaign&type=banner" title="New Banner Campaign"><? echo LANG_ADMIN_NEWBANCAMPAIGN; ?></a>
<a href="index.php?action=newcampaign&type=textlink" title="New Text Link Campaign"><? echo LANG_ADMIN_NEWTXTCAMPAIGN; ?></a>
<a href="?action=newcampaign&type=bannertextlink"><? echo LANG_ADMIN_NEWMIXCAMPAIGN; ?></a>
</div>
<div id="dropmenu3_c" class="dropmenudiv_c">
<a href="index.php?action=settings" title="Site Settings"><?php echo LANG_AMENU_SITESET; ?></a>
<a href="index.php?action=htmls" title="HTMLs">HTMLs</a>
<a href="index.php?action=cursettings" title="Currency Settings"><?php echo LANG_AMENU_CURRSET; ?></a>
<a href="index.php?action=paylist" title="Payment Settings"><?php echo LANG_AMENU_PAYSSET; ?></a>
<a href="index.php?action=adminuser" title="Admin Account"><?php echo LANG_AMENU_ADMACCT; ?></a>
</div>
<div id="dropmenu4_c" class="dropmenudiv_c">
<a href="index.php?action=coupons" title="Manage Coupons"><?php echo LANG_AMENU_MANCOUP; ?></a>
</div>
<div id="dropmenu5_c" class="dropmenudiv_c">
<a href="index.php?action=denyips" title="Deny IPs"><?php echo LANG_AMENU_DENYIPS; ?></a>
<a href="index.php?action=denycountries" title="Deny Countries"><?php echo LANG_AMENU_DENYCTRYS; ?></a>
<a href="index.php?action=clickslog" title="Logs of IPs"><?php echo LANG_AMENU_CLICKSLOG; ?></a>
<a href="index.php?action=howtos" title="How Tos / Help"><?php echo LANG_AMENU_HOWTOS; ?></a>
</div>
<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("tabsK")
</script>
	</div>
</div>

<div class="row">
	<div class="column grid_14">
	</div>
</div>