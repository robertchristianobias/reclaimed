﻿<style>
tr#cur_row1{ background-color: #f0f0f0;}
</style>

<div class="row">
	<div class="column grid_2">
	</div>
	<div class="column grid_8">
<div id="bob" style="width:200px;"><?php echo LANG_AMENU_CURRSET; ?></div>
<div id="bob3">

<form action="index.php?action=cursettings" method="post">
<table  border="0" cellpadding="3" cellspacing="3">
	<tr>
    <td><div align="center"></div></td>
	<td colspan="4"><div align="right"><input type="checkbox" name="checkbox" disabled value="checkbox"> <?php echo LANG_ADMIN_OFFDISABLED; ?> <input name="checkbox" disabled type="checkbox" value="checkbox" checked> <?php echo LANG_ADMIN_ONENABLED; ?></div></td>
    </tr>
	<tr bgcolor="#CFCFCF">
    <td colspan="4" height="3"><div align="center"></div></td>
	</tr>
	<tr>
    <td><div align="center"><strong>United States dollar</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td width="60"><div align="center"><strong>USD</strong></div></td>
	<td width="200"><? if ($conf2["usd"] == "yes") { ?> <input name="usd" type="checkbox" id="usd" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="usd" type="checkbox" id="usd" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Euro</strong></div></td>
	<td><div align="center"><strong>&#128;</strong></div></td>
	<td><div align="center"><strong>EUR</strong></div></td>
	<td><? if ($conf2["eur"] == "yes") { ?> <input name="eur" type="checkbox" id="eur" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="eur" type="checkbox" id="eur" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Pound sterling</strong></div></td>
	<td><div align="center"><strong>&#163;</strong></div></td>
	<td><div align="center"><strong>GBP</strong></div></td>
	<td><? if ($conf2["gbp"] == "yes") { ?> <input name="gbp" type="checkbox" id="gbp" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="gbp" type="checkbox" id="gbp" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Japanese yen 円</strong></div></td>
	<td><div align="center"><strong>&#165;</strong></div></td>
	<td><div align="center"><strong>JPY</strong></div></td>
	<td><? if ($conf2["jpy"] == "yes") { ?> <input name="jpy" type="checkbox" id="jpy" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="jpy" type="checkbox" id="jpy" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Canadian dollar</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td><div align="center"><strong>CAD</strong></div></td>
	<td><? if ($conf2["cad"] == "yes") { ?> <input name="cad" type="checkbox" id="cad" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="cad" type="checkbox" id="cad" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Australian dollar</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td><div align="center"><strong>AUD</strong></div></td>
	<td><? if ($conf2["aud"] == "yes") { ?> <input name="aud" type="checkbox" id="aud" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="aud" type="checkbox" id="aud" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Swiss franc</strong></div></td>
	<td><div align="center"><strong>&#8355;r</strong></div></td>
	<td><div align="center"><strong>CHF</strong></div></td>
	<td><? if ($conf2["chf"] == "yes") { ?> <input name="chf" type="checkbox" id="chf" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="chf" type="checkbox" id="chf" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Indian rupee</strong></div></td>
	<td><div align="center"><strong>&#8377;</strong></div></td>
	<td><div align="center"><strong>INR</strong></div></td>
	<td><? if ($conf2["inr"] == "yes") { ?> <input name="inr" type="checkbox" id="inr" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="inr" type="checkbox" id="inr" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Swedish krona</strong></div></td>
	<td><div align="center"><strong>kr</strong></div></td>
	<td><div align="center"><strong>SEK</strong></div></td>
	<td><? if ($conf2["sek"] == "yes") { ?> <input name="sek" type="checkbox" id="sek" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="sek" type="checkbox" id="sek" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Norwegian krone</strong></div></td>
	<td><div align="center"><strong>kr</strong></div></td>
	<td><div align="center"><strong>NOK</strong></div></td>
	<td><? if ($conf2["nok"] == "yes") { ?> <input name="nok" type="checkbox" id="nok" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="nok" type="checkbox" id="nok" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Mexican peso</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td><div align="center"><strong>MXN</strong></div></td>
	<td><? if ($conf2["mxn"] == "yes") { ?> <input name="mxn" type="checkbox" id="mxn" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="mxn" type="checkbox" id="mxn" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Hong Kong dollar</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td><div align="center"><strong>HKD</strong></div></td>
	<td><? if ($conf2["hkd"] == "yes") { ?> <input name="hkd" type="checkbox" id="hkd" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="hkd" type="checkbox" id="hkd" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>New Zealand dollar</strong></div></td>
	<td><div align="center"><strong>&#36;</strong></div></td>
	<td><div align="center"><strong>NZD</strong></div></td>
	<td><? if ($conf2["nzd"] == "yes") { ?> <input name="nzd" type="checkbox" id="nzd" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="nzd" type="checkbox" id="nzd" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Polish zloty</strong></div></td>
	<td><div align="center"><strong>z&#322;</strong></div></td>
	<td><div align="center"><strong>PLN</strong></div></td>
	<td><? if ($conf2["pln"] == "yes") { ?> <input name="pln" type="checkbox" id="pln" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="pln" type="checkbox" id="pln" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Danish krone</strong></div></td>
	<td><div align="center"><strong>kr</strong></div></td>
	<td><div align="center"><strong>DKK</strong></div></td>
	<td><? if ($conf2["dkk"] == "yes") { ?> <input name="dkk" type="checkbox" id="dkk" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="dkk" type="checkbox" id="dkk" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Czech koruna</strong></div></td>
	<td><div align="center"><strong>K&#269;</strong></div></td>
	<td><div align="center"><strong>CZK</strong></div></td>
	<td><? if ($conf2["czk"] == "yes") { ?> <input name="czk" type="checkbox" id="czk" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="czk" type="checkbox" id="czk" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Singapore dollar</strong></div></td>
	<td><div align="center"><b>S&#36;</b></div></td>
	<td><div align="center"><strong>SGD</strong></div></td>
	<td><? if ($conf2["sgd"] == "yes") { ?> <input name="sgd" type="checkbox" id="sgd" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="sgd" type="checkbox" id="sgd" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>New Taiwan Dollar</strong></div></td>
	<td><div align="center"><strong>NT&#36;</strong></div></td>
	<td><div align="center"><strong>TWD</strong></div></td>
	<td><? if ($conf2["twd"] == "yes") { ?> <input name="twd" type="checkbox" id="twd" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="twd" type="checkbox" id="twd" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Thai Baht</strong></div></td>
	<td><div align="center"><strong>&#3647;</strong></div></td>
	<td><div align="center"><strong>THB</strong></div></td>
	<td><? if ($conf2["thb"] == "yes") { ?> <input name="thb" type="checkbox" id="thb" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="thb" type="checkbox" id="thb" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Israeli New Shekel</strong></div></td>
	<td><div align="center"><strong>&#8362;</strong></div></td>
	<td><div align="center"><strong>ILS</strong></div></td>
	<td><? if ($conf2["ils"] == "yes") { ?> <input name="ils" type="checkbox" id="ils" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="ils" type="checkbox" id="ils" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Russian ruble</strong></div></td>
	<td><div align="center"><strong>&#1088;&#1091;&#1073;</strong></div></td>
	<td><div align="center"><strong>RUB</strong></div></td>
	<td><? if ($conf2["rub"] == "yes") { ?> <input name="rub" type="checkbox" id="rub" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="rub" type="checkbox" id="rub" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>(South) Korea Won 원</strong></div></td>
	<td><div align="center"><strong>&#8361;</strong></div></td>
	<td><div align="center"><strong>KRW</strong></div></td>
	<td><? if ($conf2["krw"] == "yes") { ?> <input name="krw" type="checkbox" id="krw" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="krw" type="checkbox" id="krw" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>China Yuan Renminbi</strong></div></td>
	<td><div align="center"><strong>&#165;</strong></div></td>
	<td><div align="center"><strong>CNY</strong></div></td>
	<td><? if ($conf2["cny"] == "yes") { ?> <input name="cny" type="checkbox" id="cny" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="cny" type="checkbox" id="cny" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Philippine peso</strong></div></td>
	<td><div align="center"><strong>&#8369;</strong></div></td>
	<td><div align="center"><strong>PHP</strong></div></td>
	<td><? if ($conf2["php"] == "yes") { ?> <input name="php" type="checkbox" id="php" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="php" type="checkbox" id="php" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr bgcolor="#CFCFCF">
	<td colspan="4" height="3"><div align="center"></div></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Bitcoin</strong></div></td>
	<td><div align="center"><strong>BTC</strong></div></td>
	<td><div align="center"><strong>BTC</strong></div></td>
	<td><? if ($conf2["btc"] == "yes") { ?> <input name="btc" type="checkbox" id="btc" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="btc" type="checkbox" id="btc" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Litecoin</strong></div></td>
	<td><div align="center"><strong>LTC</strong></div></td>
	<td><div align="center"><strong>LTC</strong></div></td>
	<td><? if ($conf2["ltc"] == "yes") { ?> <input name="ltc" type="checkbox" id="ltc" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="ltc" type="checkbox" id="ltc" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Feathercoin</strong></div></td>
	<td><div align="center"><strong>FTC</strong></div></td>
	<td><div align="center"><strong>FTC</strong></div></td>
	<td><? if ($conf2["ftc"] == "yes") { ?> <input name="ftc" type="checkbox" id="ftc" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="ftc" type="checkbox" id="ftc" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr>
    <td><div align="center"><strong>Peercoin</strong></div></td>
	<td><div align="center"><strong>PPC</strong></div></td>
	<td><div align="center"><strong>PPC</strong></div></td>
	<td><? if ($conf2["ppc"] == "yes") { ?> <input name="ppc" type="checkbox" id="ppc" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="ppc" type="checkbox" id="ppc" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr id="cur_row1">
    <td><div align="center"><strong>Namecoin</strong></div></td>
	<td><div align="center"><strong>NMC</strong></div></td>
	<td><div align="center"><strong>NMC</strong></div></td>
	<td><? if ($conf2["nmc"] == "yes") { ?> <input name="nmc" type="checkbox" id="nmc" value="yes" checked> <img src="./img/enabled_icon.png" alt="Enabled" width="10" height="10" border="0">
 <? } else { ?> <input name="nmc" type="checkbox" id="nmc" value="yes"> <img src="./img/disabled_icon.png" alt="Disabled" width="10" height="10" border="0"><? } ?></td>
	</tr>
	<tr bgcolor="#CFCFCF">
	<td colspan="4" height="3"><div align="center"></div></td>
	</tr>
	<tr>
	<td colspan="4"><input name="update_cursettings" type="submit" id="update_cursettings" onClick="return confirm('<?php echo LANG_ADMIN_BUTTONALERT; ?>')" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"></td>
	</tr>
</table>
</form>

</div>
	</div>
	<div class="column grid_4">
	</div>
</div>