﻿<?php
// Insert this block of code at the very top of your page:
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
?>
<!DOCTYPE html>
<? include_once ('includes/language.php'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANG_HTML; ?>" lang="<?php echo LANG_HTML; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo LANG_CHARSET_ISO; ?>">
<title>Pay Banner Ad v<? echo $pbaversion ?> <?php echo LANG_ADMIN_LOGINAREA; ?></title>
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/grid.css">
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/main.css">
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<script type="text/javascript" src="<? echo $conf["site_url"]; ?>admin/includes/keyboard.js" charset="UTF-8"></script>
<link rel="stylesheet" media="screen" type="text/css" href="<? echo $conf["site_url"]; ?>admin/includes/keyboard.css">
<? if ($conf['gtranslate'] == 'yes'){ ?><script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><? } ?>
</head>
<body>

<div class="row">
	<div class="column grid_14">
	
		<div class="title">
			<img src="<? echo $conf["site_url"]; ?>admin/img/logo.png" alt="PayBannerAd PHP Script" width="500" height="50" border="0" />
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=en" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/en.png" alt="English" title="English" width="18" height="18" border="0" /></a> 
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=de" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/de.png" alt="Deutsch" title="Deutsch" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=es" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/es.png" alt="Español" title="Español" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=fr" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/fr.png" alt="Française" title="Française" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=it" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/it.png" alt="Italiano" title="Italiano" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=nl" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/nl.png" alt="Nederlandse" title="Nederlandse" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=ko" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/ko.png" alt=" 한국어" title=" 한국어" width="18" height="18" border="0" /></a>
			<a href="<? echo $conf["site_url"]; ?>admin/index.php?lang=jp" target="_self"><img src="<? echo $conf["site_url"]; ?>languages/jp.png" alt="日本語" title="日本語" width="18" height="18" border="0" /></a>
		</div>
			
		<div id="tabsK"></div>

	</div>
</div>