<?php 
include ('../../../includes/connect.php');

$query2 = "SELECT date, SUM(exp) AS totexp, SUM(clicks) AS totclicks FROM track GROUP BY date;"; 
$result = mysql_query($query2) or die('Error, query failed : ' . mysql_error()); 
while($row = mysql_fetch_assoc($result)) {
   $data[] = $row;
}
$data[0]['date'];
$data[1]['date'];
$data[2]['date'];
$data[3]['date'];
$data[4]['date'];
$data[5]['date'];
$data[6]['date'];

$data[0]['totexp'];
$data[1]['totexp'];
$data[2]['totexp'];
$data[3]['totexp'];
$data[4]['totexp'];
$data[5]['totexp'];
$data[6]['totexp'];

$data[0]['totclicks'];
$data[1]['totclicks'];
$data[2]['totclicks'];
$data[3]['totclicks'];
$data[4]['totclicks'];
$data[5]['totclicks'];
$data[6]['totclicks'];


echo '<'.'?xml version="1.0" encoding="UTF-8"?'.'>
';  
?>
<chart>
<? 
echo '<series>
';
echo '<value xid="0">'.$data[0]['date'].'</value>
'; 
echo '<value xid="1">'.$data[1]['date'].'</value>
'; 
echo '<value xid="2">'.$data[2]['date'].'</value>
'; 
echo '<value xid="3">'.$data[3]['date'].'</value>
'; 
echo '<value xid="4">'.$data[4]['date'].'</value>
'; 
echo '<value xid="5">'.$data[5]['date'].'</value>
'; 
echo '<value xid="6">'.$data[6]['date'].'</value>
'; 
echo '</series>
';
?>
	<graphs>
<?
echo '<graph title="Clicks" fill_alpha="60" line_width="2" bullet="round" color="#FF9E01">
';
echo '<value xid="0">'.$data[0]["totclicks"].'</value>
'; 
echo '<value xid="1">'.$data[1]["totclicks"].'</value>
'; 
echo '<value xid="2">'.$data[2]["totclicks"].'</value>
'; 
echo '<value xid="3">'.$data[3]["totclicks"].'</value>
'; 
echo '<value xid="4">'.$data[4]["totclicks"].'</value>
'; 
echo '<value xid="5">'.$data[5]["totclicks"].'</value>
'; 
echo '<value xid="6">'.$data[6]["totclicks"].'</value>
'; 
echo '</graph>
';

echo '<graph title="Impressions" fill_alpha="60" line_width="2" bullet="round" color="#0D8ECF">
';
echo '<value xid="0">'.$data[0]["totexp"].'</value>
'; 
echo '<value xid="1">'.$data[1]["totexp"].'</value>
'; 
echo '<value xid="2">'.$data[2]["totexp"].'</value>
'; 
echo '<value xid="3">'.$data[3]["totexp"].'</value>
'; 
echo '<value xid="4">'.$data[4]["totexp"].'</value>
'; 
echo '<value xid="5">'.$data[5]["totexp"].'</value>
'; 
echo '<value xid="6">'.$data[6]["totexp"].'</value>
'; 
echo '</graph>
';

?>
	</graphs>
</chart>