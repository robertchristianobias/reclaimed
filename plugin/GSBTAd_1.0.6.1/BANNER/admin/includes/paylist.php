<style type="text/css">
div#list {
width: 198px;
height: 35px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
background-color:white;
border:1px solid lightgray;
margin-bottom:1px;
}
div#list:hover {
background-color:#FFF7DE;
border:1px solid #878787;
}
div#list2 {
width: 198px;
height: 35px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
background-color:#FFF7DE;
border:1px solid #009900;
margin-bottom:1px;
}
span#name {
position:relative;
left:15px;
top:6px;
font-size:14px;
color:#CC3300;
text-decoration:none;
}
a#name {
font-size:14px;
color:#CC3300;
text-decoration:none;
}
a#name:hover {
font-weight:bold;
}
</style>



<div class="row">
	<div class="column grid_2">
	</div>
	<div class="column grid_8">
<div id="bob" style="width:200px;"><?php echo LANG_ADMIN_PAYSETTINGS; ?></div>
<div id="bob3">

<table width="500px" border="0" cellspacing="2" cellpadding="0">
 <tr>
 <td height="20"><p><img src="./img/icon_attention.png" width="16" height="16"> <?php echo LANG_ADMIN_PAYENABLECUR; ?> <strong>'<?php echo LANG_AMENU_SETTINGS; ?>'</strong> \ <strong>'<a href="index.php?action=cursettings"><?php echo LANG_AMENU_CURRSET; ?></a>'</strong>.</p></td>
 </tr>
 <tr>
 <td height="20">
 <? if($conf2[tco]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=2checkout" id="name">2Checkout</a></span></div>
 <? if($conf2[an]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=authorizenet" id="name">Authorize.Net</a></span></div> 
 <? if($conf2[bc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=bitcoin" id="name">Bitcoin</a></span></div>
 <? if($conf2[bm]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=btcmerch" id="name">BTCMerch</a></span></div>
 <? if($conf2[cg]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=cgold" id="name">c-gold</a></span></div>
 <? if($conf2[cb]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=coinbin" id="name">coinb.in</a></span></div>
 <? if($conf2[cp]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=coinpayments" id="name">CoinPayments</a></span></div>
 <? if($conf2[dwos]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=dwolla" id="name">Dwolla</a></span></div>
 <? if($conf2[ego]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=egopay" id="name">EgoPay</a></span></div>
 <? if($conf2[egc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=eurogoldcash" id="name">EuroGoldCash</a></span></div>
 <? if($conf2[fc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=feathercoin" id="name">Feathercoin</a></span></div>
 <? if($conf2[gco]=='' || $conf2[gco2]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=googlecheckout" id="name">Google Checkout</a></span></div>
 <? if($conf2[hdm]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=hdmoney" id="name">HD-Money</a></span></div>
 <? if($conf2[lc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=litecoin" id="name">Litecoin</a></span></div>
 <? if($conf2[mb]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=moneybookers" id="name">MoneyBookers | Skrill</a></span></div>
 <? if($conf2[nmc2]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=namecoin" id="name">Namecoin</a></span></div>
 <? if($conf2[nc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=nochex" id="name">NoChex</a></span></div>
 <? if($conf2[okp]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=okpay" id="name">OKPAY</a></span></div>
 <? if($conf2[ops]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=otherpayments" id="name">Other Payments</a></span></div>
 <? if($conf2[pax]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=paxum" id="name">Paxum</a></span></div>
 <? if($conf2[paym]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=paymate" id="name">Paymate</a></span></div>
 <? if($conf2[pp]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=paypal" id="name">PayPal</a></span></div>
 <? if($conf2[ap]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=payza" id="name">Payza</a></span></div> 
 <? if($conf2[pu]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=pecunix" id="name">Pecunix</a></span></div>
 <? if($conf2[pc]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=peercoin" id="name">Peercoin</a></span></div>
 <? if($conf2[pm]=='' || $conf2[pm2]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=perfectmoney" id="name">PerfectMoney</a></span></div>
 <? if($conf2[se]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=sensipay" id="name">SensiPay</a></span></div>
 <? if($conf2[stp]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=solidtrustpay" id="name">SolidTrustPay</a></span></div>
 <? if($conf2[wm]=='' || $conf2[wm2]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=webmoney" id="name">WebMoney</a></span></div>
 <? if($conf2[wp]=='') { ?><div id="list"><? } else { ?><div id="list2"><? } ?><span id="name"><a href="index.php?action=paysettings&p=worldpay" id="name">WorldPay</a></span></div>
 </td>
 </tr>
 <tr>
 <td height="20">&nbsp;</td>
 </tr>
</table>

</div>
	</div>
	<div class="column grid_4">
	</div>
</div>