<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
?>
<div class="row">
	<div class="column grid_1">
	</div>
	<div class="column grid_12">
<div id="bob"><?php echo LANG_AMENU_HOWTOS; ?></div>
<div id="bob3">

  <h3><u>Set up Campaigns</u>?</h3>
What is '<b>Availability</b>'? - how many 'active' ads you want to allow in each campaign. It lets you not to over-sell (too many ads, less exposures).<br />
<br />
For a 468x60 Banner Ad Campaign, you want to allow 5 'active' ads. You set '5' for availability.<br />
For a 150x150 Banner Ad Campaign, you want to allow 15 'active' ads. You set '15' for availability.<br />
For a  Text link Ad Campaign, you want to allow 30 'active' ads. You set '30' for availability.<br />
<br />
When ads run out of credits or removed/deleted, then the availability for campaign will open for sale.
<br />
<br />
How to set '<b>Credits</b>'? Examples below..<br />
<br />
~ 5000 Impressions ~<br />
Type: Cost per impression (CPI)<br />
Unlimited Credits: No (If Yes, then unlimited impressions)<br />
Credits: 5000 (number of impressions)<br />
* -1 credit each page<br />
<br />
~ 100 Clicks ~<br />
Type: Cost per click (CPC)<br />
Unlimited Credits: No (If Yes, then unlimited clicks)<br />
Credits: 100 (number of clicks)<br />
* -1 credit each click<br />
<br />
~ 3 Days ~<br />
Type: Cost per day (CPD)<br />
Unlimited Credits: No (If Yes, then unlimited days)<br />
Credits: 3 (number of days)<br />
* -1 credit each/per day<br />
<br />
For Cost per day, you need to setup cron-job OR just manually click on the link (<b>Cron</b>):<br />
"Cron Last Date: 0000-00-00 - Cron is needed today! Click on the link -> Cron."<br />
The cron.php file will check ads using CPD and take -1 credit per day.<br />
<br />
If the cron-job or you try to hit the cron.php again less than 24 hours, the page will show like this:<br />
"Too early to do cron. Must be every 24 Hours / 1 Day.<br />
Date Now: 2013-02-29<br />
Last Cron: 2013-02-29"<br />
<br />
  <hr size=1>
  <h3><u>Cron jobs</u>?</h3>
<? echo LANG_ADMIN_HOWTOCRON; ?> <br />
<img src="<? echo $conf["site_url"]; ?>admin/img/cpanelcron.png" alt="Cpanel Cron jobs" width="504" height="95" border="0">
<br /><br /> <? echo LANG_ADMIN_HOWTOCRON1; ?> <? echo LANG_MAIN_TYPE; ?>: <b>CPD (<? echo LANG_ADMIN_TYPECPD; ?>)</b>. <? echo LANG_ADMIN_HOWTOCRON2; ?>
**<? echo LANG_ADMIN_HOWTOCRON3; ?> -> <a href="<? echo $conf["site_url"]; ?>cron.php" target="_blank">Cron</a>.
  <h4><u><? echo LANG_ADMIN_HOWTOCRON4; ?></u>:</h4> 
<img src="<? echo $conf["site_url"]; ?>admin/img/cpanelcron2.png" alt="Cpanel Cron jobs" width="729" height="125" border="0">
<br /><br />
<? echo LANG_ADMIN_HOWTOCRON5; ?>:
<textarea name="croncodes" cols="100" rows="10">-------------------------------------------------------------------
Cron codes: (Not all Web Hosts use same setup)
-------------------------------------------------------------------
0 1 * * * /usr/bin/lynx -source <? echo $conf["site_url"]; ?>cron.php
0 2 * * * /usr/bin/lynx -source <? echo $conf["site_url"]; ?>admin/gcron.php

0 1 * * * lynx -dump <? echo $conf["site_url"]; ?>cron.php
0 2 * * * lynx -dump <? echo $conf["site_url"]; ?>admin/gcron.php

0 1 * * * /usr/bin/wget <? echo $conf["site_url"]; ?>cron.php
0 2 * * * /usr/bin/wget <? echo $conf["site_url"]; ?>admin/gcron.php

0 1 * * * curl --silent --compressed <? echo $conf["site_url"]; ?>cron.php
0 2 * * * curl --silent --compressed <? echo $conf["site_url"]; ?>admin/gcron.php

0 1 * * * /usr/bin/php <? echo $conf["absolute_path"]; ?>cron.php
0 2 * * * /usr/bin/php <? echo $conf["absolute_path"]; ?>admin/gcron.php

0 1 * * * /usr/local/bin/php -q ? <? echo $conf["absolute_path"]; ?>cron.php
0 2 * * * /usr/local/bin/php -q ? <? echo $conf["absolute_path"]; ?>admin/gcron.php

-------------------------------------------------------------------
GoDaddy PHP5: Replace *folders* with your real folders
-------------------------------------------------------------------
/web/cgi-bin/php5 "$HOME/html/*folders*/cron.php"
/web/cgi-bin/php5 "$HOME/html/*folders*/admin/gcron.php"
</textarea> 
<br />
<? echo LANG_ADMIN_HOWTOCRON6; ?>
  <h4><u><? echo LANG_ADMIN_HOWTOCRON7; ?></u>:</h4> 
- <a href="http://docs.cpanel.net/twiki/bin/view/AllDocumentation/CpanelDocs/CronJobs" target="_blank">cPanel - Cron Jobs</a> | <a href="http://kb.parallels.com/en/5308" target="_blank">Plesk - Set up Cron Job</a> | 
<a href="http://kb.parallels.com/en/390" target="_blank">Plesk - cron jobs for PHP</a> | <a href="http://daipratt.co.uk/crontab-plesk-php/" target="_blank">Plesk - cron jobs for PHP</a> | <a href="http://en.wikipedia.org/wiki/Cron" target="_blank">Cron @ Wikipedia</a> <br /><br />
  <hr size=1>
<br />



</div>
	</div>
	<div class="column grid_1">
	</div>
</div>