﻿<div class="row">
	<div class="column grid_1">
	</div>
	<div class="column grid_12">
<div id="bob"><?php echo LANG_ADMIN_SITESETTINGS; ?></div>
<div id="bob3">
<form action="index.php?action=settings" method="post">
<input type="hidden" name="setts_id" value="1">
<table width="100%"  border="0" cellspacing="2" cellpadding="0">
<tr>
    <td><?php echo LANG_ADMIN_ALLOWREG; ?></td>
    <td><?
	if ($conf["allow_reg"] == "yes")
	{
		?>
		<input name="allow_reg" type="radio" value="yes" checked><?php echo LANG_ADMIN_OPEN; ?>
		<input name="allow_reg" type="radio" value="no"><?php echo LANG_ADMIN_CLOSED; ?>
		<?
	}
	else
	{
		?>
		<input name="allow_reg" type="radio" value="yes"><?php echo LANG_ADMIN_OPEN; ?>
		<input name="allow_reg" type="radio" value="no" checked><?php echo LANG_ADMIN_CLOSED; ?>
		<?
	}
	?> <br /> 
		<strong><?php echo LANG_ADMIN_IFCLOSED; ?></strong></td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SHOWMSTATS; ?></td>
    <td><?
	if ($conf["show_mstats"] == "yes")
	{
		?>
		<input name="show_mstats" type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name="show_mstats" type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name="show_mstats" type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name="show_mstats" type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?>
    </td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_ALLOWIMGUP; ?></td>
    <td><?
	if ($conf['allowupload'] == 'yes')
	{
		?>
		<input name='allowupload' type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name='allowupload' type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name='allowupload' type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name='allowupload' type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?> <br /><?php echo LANG_ADMIN_ALLOWMAXSIZE; ?>: <input name="uploadsize" type="text" id="uploadsize" value="<? echo $conf["uploadsize"] ?>" size="10"> <a href="https://www.google.com/#q=1mb+to+bytes" target="_blank">Bytes</a> <br />
	<font color="red"><?php echo LANG_ADMIN_ALLOWWARNING; ?></font>. <?php echo LANG_ADMIN_ALLOWWARNING2; ?>
	</td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_ALLOWSWFUP; ?></td>
    <td><?
	if ($conf['allowfupload'] == 'yes')
	{
		?>
		<input name='allowfupload' type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name='allowfupload' type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name='allowfupload' type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name='allowfupload' type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?> <br /><?php echo LANG_ADMIN_ALLOWMAXSIZE; ?>: <input name="uploadfsize" type="text" id="uploadfsize" value="<? echo $conf["uploadfsize"] ?>" size="10"> <a href="https://www.google.com/#q=1mb+to+bytes" target="_blank">Bytes</a> <br />
	<font color="red"><?php echo LANG_ADMIN_ALLOWWARNING; ?></font>. <?php echo LANG_ADMIN_ALLOWWARNING2; ?>
	</td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_AEMAIL; ?></td>
    <td><input name="admin_email" type="text" id="admin_email" value="<? echo $conf["admin_email"] ?>" size="50"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SENDFROM; ?></td>
    <td><input name="send_from_email" type="text" id="send_from_email" value="<? echo $conf["send_from_email"] ?>" size="50"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_ABPATHTO; ?></td>
    <td><input name="site_absolute_path" type="text" id="site_absolute_path" value="<? echo $conf["absolute_path"] ?>" size="70"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SITEURL; ?></td>
    <td><input name="site_url" type="text" id="site_url" value="<? echo $conf["site_url"] ?>" size="70"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SITENAME; ?></td>
    <td><input name="site_name" type="text" id="site_name" value="<? echo $conf["site_name"] ?>" size="50"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SITEKEYWORD; ?></td>
    <td><input name="site_keywords" type="text" id="site_keywords" value="<? echo $conf["keywords"] ?>" size="50"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_SITEDESC; ?></td>
    <td><input name="site_description" type="text" id="site_description" value="<? echo $conf["description"] ?>" size="50"></td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_DEFAULTLAN; ?></td>
    <td><SELECT NAME="site_language" id="site_language">
<option value="<? echo $conf['language']; ?>" SELECTED><? echo $conf['language']; ?>*</option>
<option DISABLED>-----------------</option>
<option value="en">English</option>
<option value="de">German Deutsch</option>
<option value="es">Spanish Español</option>
<option value="fr">French Française</option>
<option value="it">Italian Italiano</option>
<option value="nl">Dutch Nederlandse</option>
<option value="ko">Korean 한국인</option>
<option value="jp">Japanese 日本語</option>
</SELECT></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_DEFAULTCUR; ?></td>
    <td><SELECT NAME="site_currency" id="site_currency">
<option value="<? echo $conf['currency']; ?>" SELECTED><? echo $conf['currency']; ?>*</option>
<option DISABLED>-----------------</option>
<? if ($conf2["usd"] == "yes") { ?><option value="usd">USD US dollar</option><? } ?>
<? if ($conf2["eur"] == "yes") { ?><option value="eur">EUR Euro</option><? } ?>
<? if ($conf2["gbp"] == "yes") { ?><option value="gbp">GBP Pound sterling</option><? } ?>
<? if ($conf2["jpy"] == "yes") { ?><option value="jpy">JPY Japanese yen</option><? } ?>
<? if ($conf2["cad"] == "yes") { ?><option value="cad">CAD Canadian dollar</option><? } ?>
<? if ($conf2["aud"] == "yes") { ?><option value="aud">AUD Australian dollar</option><? } ?>
<? if ($conf2["chf"] == "yes") { ?><option value="chf">CHF Swiss franc</option><? } ?>
<? if ($conf2["inr"] == "yes") { ?><option value="inr">INR Indian rupee</option><? } ?>
<? if ($conf2["sek"] == "yes") { ?><option value="sek">SEK Swedish krona</option><? } ?>
<? if ($conf2["nok"] == "yes") { ?><option value="nok">NOK Norwegian krone</option><? } ?>
<? if ($conf2["mxn"] == "yes") { ?><option value="mxn">MXN Mexican peso</option><? } ?>
<? if ($conf2["hkd"] == "yes") { ?><option value="hkd">HKD Hong Kong dollar</option><? } ?>
<? if ($conf2["nzd"] == "yes") { ?><option value="nzd">NZD New Zealand dollar</option><? } ?>
<? if ($conf2["pln"] == "yes") { ?><option value="pln">PLN Polish złoty</option><? } ?>
<? if ($conf2["dkk"] == "yes") { ?><option value="dkk">DKK Danish krone</option><? } ?>
<? if ($conf2["czk"] == "yes") { ?><option value="czk">CZK Czech koruna</option><? } ?>
<? if ($conf2["sgd"] == "yes") { ?><option value="sgd">SGD Singapore dollar</option><? } ?>
<? if ($conf2["twd"] == "yes") { ?><option value="twd">TWD Taiwan dollar</option><? } ?>
<? if ($conf2["thb"] == "yes") { ?><option value="thb">THB Thai baht</option><? } ?>
<? if ($conf2["ils"] == "yes") { ?><option value="ils">ILS Israeli new shekel</option><? } ?>
<? if ($conf2["rub"] == "yes") { ?><option value="rub">RUB Russian ruble</option><? } ?>
<? if ($conf2["krw"] == "yes") { ?><option value="krw">KRW Korean won</option><? } ?>
<? if ($conf2["cny"] == "yes") { ?><option value="cny">CNY Chinese yuan</option><? } ?>
<? if ($conf2["php"] == "yes") { ?><option value="php">PHP Philippine peso</option><? } ?>
<option DISABLED>-----------------</option>
<? if ($conf2["btc"] == "yes") { ?><option value="btc">BTC Bitcoin</option><? } ?>
<? if ($conf2["ltc"] == "yes") { ?><option value="ltc">LTC Litecoin</option><? } ?>
<? if ($conf2["ftc"] == "yes") { ?><option value="ftc">FTC Feathercoin</option><? } ?>
<? if ($conf2["ppc"] == "yes") { ?><option value="ppc">PPC Peercoin</option><? } ?>
<? if ($conf2["nmc"] == "yes") { ?><option value="nmc">NMC Namecoin</option><? } ?>
</SELECT> <a href="index.php?action=cursettings"><?php echo LANG_AMENU_CURRSET; ?></a></td>
  </tr>
  <tr>
    <td><?php echo LANG_ADMIN_DEFAULTTEM; ?></td>
    <td><SELECT id=""site_theme" name="site_theme"> 
<option value="<? echo $conf['theme']; ?>" SELECTED><? echo $conf['theme']; ?>*</option>
<option DISABLED>-----------------</option>
<? foreach(glob('../themes/*', GLOB_ONLYDIR) as $dir) { $dir = str_replace('../themes/', '', $dir); echo '<option value="'.$dir.'">'.$dir.'</option>'; } ?> 
</SELECT></td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><a href="http://translate.google.com/manager/website/?hl=en" target="_blank"><img src="./img/google_translate.png" alt="Google translate" height="20" width="105" border="0" /></a></td>
    <td><?
	if ($conf['gtranslate'] == 'yes')
	{
		?>
		<input name='site_gtranslate' type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name='site_gtranslate' type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name='site_gtranslate' type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name='site_gtranslate' type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?>
<br /> <a href="http://translate.google.com/manager/website/?hl=en" target="_blank">Website Translator</a>
	</td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><a href="http://www.google.com/finance/converter" target="_blank"><img src="./img/google_currency.png" alt="Google Currency Conversion" height="20" width="105" border="0" /></a></td>
    <td><?
	if ($conf['gcurrency'] == 'yes')
	{
		?>
		<input name='site_gcurrency' type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name='site_gcurrency' type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name='site_gcurrency' type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name='site_gcurrency' type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?>
<br />* <?php echo LANG_ADMIN_GCURR1; ?> <font color="red"><?php echo LANG_ADMIN_GCURR2; ?></font> <a href="?action=howtos"><?php echo LANG_ADMIN_GCURR3; ?></a>.
	</td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td><i><?php echo LANG_FMENU_SCRIPTBY; ?>: Dijiteol?</i></td>
    <td><?
	if ($conf['scriptby'] == 'yes')
	{
		?>
		<input name='site_scriptby' type="radio" value="yes" checked><?php echo LANG_MAIN_YES; ?>
		<input name='site_scriptby' type="radio" value="no"><?php echo LANG_MAIN_NO; ?>
		<?
	}
	else
	{
		?>
		<input name='site_scriptby' type="radio" value="yes"><?php echo LANG_MAIN_YES; ?>
		<input name='site_scriptby' type="radio" value="no" checked><?php echo LANG_MAIN_NO; ?>
		<?
	}
	?></td>
  </tr>
  <tr>
    <td height="20" colspan="2"><hr width="100%" size="1"></td>
  </tr>
  <tr>
    <td height="20" colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td>
      <div align="right">
        <input name="update_settings" type="submit" id="update_settings" onClick="return confirm('<?php echo LANG_ADMIN_BUTTONALERT; ?>')" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>">
      </div></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</div>
	</div>
	<div class="column grid_1">
	</div>
</div>