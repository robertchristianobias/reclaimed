<style type="text/css">
fieldset
{
-moz-border-radius: 7px;
border: 1px #dddddd solid;
padding: 10px;
width: 550px;
margin-top: 10px;
}

fieldset legend
{
border: 1px #1a6f93 solid;
color: black; 

font-family: Verdana;
font-weight: none;
font-size: 13px;

padding-right: 5px;
padding-left: 5px;
padding-top: 2px;
padding-bottom: 2px;

-moz-border-radius: 3px;
}

/* Label */
label
{
width: 140px;
padding-left: 20px;
margin: 5px;
float: left;
text-align: left;
}
/* Input, Textarea */
input, textarea
{
margin: 1px;
padding: 0px;
}
br { 
clear: left; 
}
div#curopt {
width: 380px;
float: right;
}
div#update_but {
position:relative;
left:15px;
top:50px;
}
div#curnote {
border: 1px dotted red;
margin:0px auto;
}
</style>

<div class="row">
	<div class="column grid_2">
	</div>
	<div class="column grid_9">
<div id="bob" style="width:200px;"><?php echo LANG_ADMIN_PAYSETTINGS; ?></div>
<div id="bob3">

<form method="post" action="index.php?action=paysettings&p=<?=$_GET['p']?>">
<input type="button" onclick="location.href='index.php?action=paylist'" value="Back">
<?
switch ($_GET['p'])
{
case '2checkout':
?>
<fieldset><legend><a href="http://www.2checkout.com" target="_blank">2Checkout</a></legend>
<label><?php echo LANG_ADMIN_ACCOUNTNUM; ?></label> <input name="tco" type="text" id="tco" value="<? echo $conf2[tco] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_tco] == "yes") { ?> <input name="accept_tco" type="checkbox" id="accept_tco" value="yes" checked> <? } else { ?> <input name="accept_tco" type="checkbox" id="accept_tco" value="yes"> <? } ?> <br />
EUR (Euro) <? if ($conf2[accept_tcor] == "yes") { ?> <input name="accept_tcor" type="checkbox" id="accept_tcor" value="yes" checked> <? } else { ?> <input name="accept_tcor" type="checkbox" id="accept_tcor" value="yes"> <? } ?> <br />
GBP (Pound sterling) <? if ($conf2[accept_tcop] == "yes") { ?> <input name="accept_tcop" type="checkbox" id="accept_tcop" value="yes" checked> <? } else { ?> <input name="accept_tcop" type="checkbox" id="accept_tcop" value="yes"> <? } ?> <br />
JPY (Japanese Yen) <? if ($conf2[accept_tcoy] == "yes") { ?> <input name="accept_tcoy" type="checkbox" id="accept_tcoy" value="yes" checked> <? } else { ?> <input name="accept_tcoy" type="checkbox" id="accept_tcoy" value="yes"> <? } ?> <br />
CAD (Canadian dollar) <? if ($conf2[accept_tcoc] == "yes") { ?> <input name="accept_tcoc" type="checkbox" id="accept_tcoc" value="yes" checked> <? } else { ?> <input name="accept_tcoc" type="checkbox" id="accept_tcoc" value="yes"> <? } ?> <br />
AUD (Australian dollar) <? if ($conf2[accept_tcod] == "yes") { ?> <input name="accept_tcod" type="checkbox" id="accept_tcod" value="yes" checked> <? } else { ?> <input name="accept_tcod" type="checkbox" id="accept_tcod" value="yes"> <? } ?> <br />
CHF (Swiss franc) <? if ($conf2[accept_tcof] == "yes") { ?> <input name="accept_tcof" type="checkbox" id="accept_tcof" value="yes" checked> <? } else { ?> <input name="accept_tcof" type="checkbox" id="accept_tcof" value="yes"> <? } ?> <br />
INR (Indian rupee) <? if ($conf2[accept_tcoi] == "yes") { ?> <input name="accept_tcoi" type="checkbox" id="accept_tcoi" value="yes" checked> <? } else { ?> <input name="accept_tcoi" type="checkbox" id="accept_tcoi" value="yes"> <? } ?> <br />
SEK (Swedish krona) <? if ($conf2[accept_tcos] == "yes") { ?> <input name="accept_tcos" type="checkbox" id="accept_tcos" value="yes" checked> <? } else { ?> <input name="accept_tcos" type="checkbox" id="accept_tcos" value="yes"> <? } ?> <br />
NOK (Norwegian krone) <? if ($conf2[accept_tcon] == "yes") { ?> <input name="accept_tcon" type="checkbox" id="accept_tcon" value="yes" checked> <? } else { ?> <input name="accept_tcon" type="checkbox" id="accept_tcon" value="yes"> <? } ?> <br />
MXN (Mexican peso) <? if ($conf2[accept_tcom] == "yes") { ?> <input name="accept_tcom" type="checkbox" id="accept_tcom" value="yes" checked> <? } else { ?> <input name="accept_tcom" type="checkbox" id="accept_tcom" value="yes"> <? } ?> <br />
HKD (Hong Kong dollar) <? if ($conf2[accept_tcoh] == "yes") { ?> <input name="accept_tcoh" type="checkbox" id="accept_tcoh" value="yes" checked> <? } else { ?> <input name="accept_tcoh" type="checkbox" id="accept_tcoh" value="yes"> <? } ?> <br />
NZD (New Zealand dollar) <? if ($conf2[accept_tcoz] == "yes") { ?> <input name="accept_tcoz" type="checkbox" id="accept_tcoz" value="yes" checked> <? } else { ?> <input name="accept_tcoz" type="checkbox" id="accept_tcoz" value="yes"> <? } ?> <br />
DKK (Danish krone) <? if ($conf2[accept_tcok] == "yes") { ?> <input name="accept_tcok" type="checkbox" id="accept_tcok" value="yes" checked> <? } else { ?> <input name="accept_tcok" type="checkbox" id="accept_tcok" value="yes"> <? } ?> <br />
SGD (Singapore dollar) <? if ($conf2[accept_tcosd] == "yes") { ?> <input name="accept_tcosd" type="checkbox" id="accept_tcosd" value="yes" checked> <? } else { ?> <input name="accept_tcosd" type="checkbox" id="accept_tcosd" value="yes"> <? } ?> <br />
ILS (Israeli new shekel) <? if ($conf2[accept_tcois] == "yes") { ?> <input name="accept_tcois" type="checkbox" id="accept_tcois" value="yes" checked> <? } else { ?> <input name="accept_tcois" type="checkbox" id="accept_tcois" value="yes"> <? } ?> <br />
RUB (Russian ruble) <? if ($conf2[accept_tcorr] == "yes") { ?> <input name="accept_tcorr" type="checkbox" id="accept_tcorr" value="yes" checked> <? } else { ?> <input name="accept_tcorr" type="checkbox" id="accept_tcorr" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_2checkout" type="submit" id="update_2checkout" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'payza':
?>

<fieldset><legend><a href="http://www.payza.com/" target="_blank">Payza (AlertPay)</a></legend>
<label><?php echo LANG_ADMIN_EMAILADDRESS; ?></label> <input name="ap" type="text" id="ap" value="<? echo $conf2[ap] ?>" size="35"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_ap] == "yes") { ?> <input name="accept_ap" type="checkbox" id="accept_ap" value="yes" checked> <? } else { ?> <input name="accept_ap" type="checkbox" id="accept_ap" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_apr] == "yes") { ?> <input name="accept_apr" type="checkbox" id="accept_apr" value="yes" checked> <? } else { ?> <input name="accept_apr" type="checkbox" id="accept_apr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_app] == "yes") { ?> <input name="accept_app" type="checkbox" id="accept_app" value="yes" checked> <? } else { ?> <input name="accept_app" type="checkbox" id="accept_app" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_apc] == "yes") { ?> <input name="accept_apc" type="checkbox" id="accept_apc" value="yes" checked> <? } else { ?> <input name="accept_apc" type="checkbox" id="accept_apc" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_apd] == "yes") { ?> <input name="accept_apd" type="checkbox" id="accept_apd" value="yes" checked> <? } else { ?> <input name="accept_apd" type="checkbox" id="accept_apd" value="yes"> <? } ?> <br />
CHF (Swiss franc) <? if ($conf2[accept_apf] == "yes") { ?> <input name="accept_apf" type="checkbox" id="accept_apf" value="yes" checked> <? } else { ?> <input name="accept_apf" type="checkbox" id="accept_apf" value="yes"> <? } ?> <br />
INR (Indian rupee) <? if ($conf2[accept_api] == "yes") { ?> <input name="accept_api" type="checkbox" id="accept_api" value="yes" checked> <? } else { ?> <input name="accept_api" type="checkbox" id="accept_api" value="yes"> <? } ?> <br />
SEK (Swedish krona) <? if ($conf2[accept_aps] == "yes") { ?> <input name="accept_aps" type="checkbox" id="accept_aps" value="yes" checked> <? } else { ?> <input name="accept_aps" type="checkbox" id="accept_aps" value="yes"> <? } ?> <br />
NOK (Norwegian krone) <? if ($conf2[accept_apn] == "yes") { ?> <input name="accept_apn" type="checkbox" id="accept_apn" value="yes" checked> <? } else { ?> <input name="accept_apn" type="checkbox" id="accept_apn" value="yes"> <? } ?> <br />
HKD (Hong Kong dollar) <? if ($conf2[accept_aph] == "yes") { ?> <input name="accept_aph" type="checkbox" id="accept_aph" value="yes" checked> <? } else { ?> <input name="accept_aph" type="checkbox" id="accept_aph" value="yes"> <? } ?> <br />
NZD (New Zealand dollar) <? if ($conf2[accept_apz] == "yes") { ?> <input name="accept_apz" type="checkbox" id="accept_apz" value="yes" checked> <? } else { ?> <input name="accept_apz" type="checkbox" id="accept_apz" value="yes"> <? } ?> <br />
PLN (Polish zloty) <? if ($conf2[accept_apl] == "yes") { ?> <input name="accept_apl" type="checkbox" id="accept_apl" value="yes" checked> <? } else { ?> <input name="accept_apl" type="checkbox" id="accept_apl" value="yes"> <? } ?> <br />
DKK (Danish krone) <? if ($conf2[accept_apk] == "yes") { ?> <input name="accept_apk" type="checkbox" id="accept_apk" value="yes" checked> <? } else { ?> <input name="accept_apk" type="checkbox" id="accept_apk" value="yes"> <? } ?> <br />
CZK (Czech koruna) <? if ($conf2[accept_apczk] == "yes") { ?> <input name="accept_apczk" type="checkbox" id="accept_apczk" value="yes" checked> <? } else { ?> <input name="accept_apczk" type="checkbox" id="accept_apczk" value="yes"> <? } ?> <br />
SGD (Singapore dollar) <? if ($conf2[accept_apsd] == "yes") { ?> <input name="accept_apsd" type="checkbox" id="accept_apsd" value="yes" checked> <? } else { ?> <input name="accept_apsd" type="checkbox" id="accept_apsd" value="yes"> <? } ?> <br />

</div>
<br /><br />
<label><input name="update_payza" type="submit" id="update_payza" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'authorizenet':
?>

<fieldset><legend><a href="http://www.authorize.net" target="_blank">Authorize.Net</a></legend>
<label><?php echo LANG_ADMIN_LOGINID; ?></label> <input name="an" type="text" id="an" value="<? echo $conf2[an] ?>" size="15"> <br />
<label><?php echo LANG_ADMIN_TRANSKEY; ?></label> <input name="antk" type="text" id="antk" value="<? echo $conf2[antk] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_an] == "yes") { ?> <input name="accept_an" type="checkbox" id="accept_an" value="yes" checked> <? } else { ?> <input name="accept_an" type="checkbox" id="accept_an" value="yes"> <? } ?>
</div>
<br /><br />
<label><input name="update_authorizenet" type="submit" id="update_authorizenet" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'cgold':
?>

<fieldset><legend><a href="http://829.c-gold.com/" target="_blank">c-gold</a></legend>
<label><?php echo LANG_ADMIN_ACCOUNTNUM; ?></label> <input name="cg" type="text" id="cg" value="<? echo $conf2[cg] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_cg] == "yes") { ?> <input name="accept_cg" type="checkbox" id="accept_cg" value="yes" checked> <? } else { ?> <input name="accept_cg" type="checkbox" id="accept_cg" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_cgr] == "yes") { ?> <input name="accept_cgr" type="checkbox" id="accept_cgr" value="yes" checked> <? } else { ?> <input name="accept_cgr" type="checkbox" id="accept_cgr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_cgp] == "yes") { ?> <input name="accept_cgp" type="checkbox" id="accept_cgp" value="yes" checked> <? } else { ?> <input name="accept_cgp" type="checkbox" id="accept_cgp" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_cgy] == "yes") { ?> <input name="accept_cgy" type="checkbox" id="accept_cgy" value="yes" checked> <? } else { ?> <input name="accept_cgy" type="checkbox" id="accept_cgy" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_cgd] == "yes") { ?> <input name="accept_cgd" type="checkbox" id="accept_cgd" value="yes" checked> <? } else { ?> <input name="accept_cgd" type="checkbox" id="accept_cgd" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_cga] == "yes") { ?> <input name="accept_cga" type="checkbox" id="accept_cga" value="yes" checked> <? } else { ?> <input name="accept_cga" type="checkbox" id="accept_cga" value="yes"> <? } ?> <br />
CHF (Swiss franc) <? if ($conf2[accept_cgf] == "yes") { ?> <input name="accept_cgf" type="checkbox" id="accept_cgf" value="yes" checked> <? } else { ?> <input name="accept_cgf" type="checkbox" id="accept_cgf" value="yes"> <? } ?> <br />
INR (Indian rupee) <? if ($conf2[accept_cgi] == "yes") { ?> <input name="accept_cgi" type="checkbox" id="accept_cgi" value="yes" checked> <? } else { ?> <input name="accept_cgi" type="checkbox" id="accept_cgi" value="yes"> <? } ?> <br />
SEK (Swedish krona) <? if ($conf2[accept_cgs] == "yes") { ?> <input name="accept_cgs" type="checkbox" id="accept_cgs" value="yes" checked> <? } else { ?> <input name="accept_cgs" type="checkbox" id="accept_cgs" value="yes"> <? } ?> <br />
MXN (Mexican peso) <? if ($conf2[accept_cgm] == "yes") { ?> <input name="accept_cgm" type="checkbox" id="accept_cgm" value="yes" checked> <? } else { ?> <input name="accept_cgm" type="checkbox" id="accept_cgm" value="yes"> <? } ?> <br />
HKD (Hong Kong dollar) <? if ($conf2[accept_cgh] == "yes") { ?> <input name="accept_cgh" type="checkbox" id="accept_cgh" value="yes" checked> <? } else { ?> <input name="accept_cgh" type="checkbox" id="accept_cgh" value="yes"> <? } ?> <br />
CNY (Chinese Yuan Renminbi) <? if ($conf2[accept_cgyr] == "yes") { ?> <input name="accept_cgyr" type="checkbox" id="accept_cgyr" value="yes" checked> <? } else { ?> <input name="accept_cgyr" type="checkbox" id="accept_cgyr" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_cgold" type="submit" id="update_cgold" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'eurogoldcash':
?>

<fieldset><legend><a href="http://www.eurogoldcash.com" target="_blank">EuroGoldCash</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="egc" type="text" id="egc" value="<? echo $conf2[egc] ?>" size="25"> <br />
<label><?php echo LANG_ADMIN_STORENAME; ?></label> <input name="egcstore" type="text" id="egcstore" value="<? echo $conf2[egcstore] ?>" size="10"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_egc] == "yes") { ?> <input name="accept_egc" type="checkbox" id="accept_egc" value="yes" checked> <? } else { ?> <input name="accept_egc" type="checkbox" id="accept_egc" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_egcr] == "yes") { ?> <input name="accept_egcr" type="checkbox" id="accept_egcr" value="yes" checked> <? } else { ?> <input name="accept_egcr" type="checkbox" id="accept_egcr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_egcp] == "yes") { ?> <input name="accept_egcp" type="checkbox" id="accept_egcp" value="yes" checked> <? } else { ?> <input name="accept_egcp" type="checkbox" id="accept_egcp" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_eurogoldcash" type="submit" id="update_eurogoldcash" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'goldmoney':
?>

<fieldset><legend><a href="https://www.goldmoney.com" target="_blank">GoldMoney</a></legend>
<label><?php echo LANG_ADMIN_HOLDINGNUM; ?></label> <input name="gm" type="text" id="gm" value="<? echo $conf2[gm] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_gm] == "yes") { ?> <input name="accept_gm" type="checkbox" id="accept_gm" value="yes" checked> <? } else { ?> <input name="accept_gm" type="checkbox" id="accept_gm" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_gmr] == "yes") { ?> <input name="accept_gmr" type="checkbox" id="accept_gmr" value="yes" checked> <? } else { ?> <input name="accept_gmr" type="checkbox" id="accept_gmr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_gmp] == "yes") { ?> <input name="accept_gmp" type="checkbox" id="accept_gmp" value="yes" checked> <? } else { ?> <input name="accept_gmp" type="checkbox" id="accept_gmp" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_gmy] == "yes") { ?> <input name="accept_gmy" type="checkbox" id="accept_gmy" value="yes" checked> <? } else { ?> <input name="accept_gmy" type="checkbox" id="accept_gmy" value="yes"> <? } ?><br />
CAD (Canadian dollar) <? if ($conf2[accept_gmc] == "yes") { ?> <input name="accept_gmc" type="checkbox" id="accept_gmc" value="yes" checked> <? } else { ?> <input name="accept_gmc" type="checkbox" id="accept_gmc" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_gmd] == "yes") { ?> <input name="accept_gmd" type="checkbox" id="accept_gmd" value="yes" checked> <? } else { ?> <input name="accept_gmd" type="checkbox" id="accept_gmd" value="yes"> <? } ?> <br /> 
CHF (Swiss franc) <? if ($conf2[accept_gmf] == "yes") { ?> <input name="accept_gmf" type="checkbox" id="accept_gmf" value="yes" checked> <? } else { ?> <input name="accept_gmf" type="checkbox" id="accept_gmf" value="yes"> <? } ?> <br /> 
HKD (Hong Kong dollar) <? if ($conf2[accept_gmh] == "yes") { ?> <input name="accept_gmh" type="checkbox" id="accept_gmh" value="yes" checked> <? } else { ?> <input name="accept_gmh" type="checkbox" id="accept_gmh" value="yes"> <? } ?> <br />
NZD (New Zealand dollar) <? if ($conf2[accept_gmz] == "yes") { ?> <input name="accept_gmz" type="checkbox" id="accept_gmz" value="yes" checked> <? } else { ?> <input name="accept_gmz" type="checkbox" id="accept_gmz" value="yes"> <? } ?> <br />
RUB (Russian ruble) <? if ($conf2[accept_gmub] == "yes") { ?> <input name="accept_gmub" type="checkbox" id="accept_gmub" value="yes" checked> <? } else { ?> <input name="accept_gmub" type="checkbox" id="accept_gmub" value="yes"> <? } ?> <br />
CNY (Chinese Yuan Renminbi) <? if ($conf2[accept_gmny] == "yes") { ?> <input name="accept_gmny" type="checkbox" id="accept_gmny" value="yes" checked> <? } else { ?> <input name="accept_gmny" type="checkbox" id="accept_gmny" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_goldmoney" type="submit" id="update_goldmoney" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'googlecheckout':
?>

<fieldset><legend><a href="http://checkout.google.com" target="_blank">Google Checkout</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="gco" type="text" id="gco" value="<? echo $conf2[gco] ?>" size="25"> USD (U.S. dollar) <? if ($conf2[accept_gco] == "yes") { ?> <input name="accept_gco" type="checkbox" id="accept_gco" value="yes" checked> <? } else { ?> <input name="accept_gco" type="checkbox" id="accept_gco" value="yes"> <? } ?> (For US Merchant) <br />
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="gco2" type="text" id="gco2" value="<? echo $conf2[gco2] ?>" size="25"> GBP (Pound sterling) <? if ($conf2[accept_gcop] == "yes") { ?> <input name="accept_gcop" type="checkbox" id="accept_gcop" value="yes" checked> <? } else { ?> <input name="accept_gcop" type="checkbox" id="accept_gcop" value="yes"> <? } ?> (For UK Merchant) <br />
<br /><br />
<label><input name="update_googlecheckout" type="submit" id="update_googlecheckout" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'hdmoney':
?>

<fieldset><legend><a href="http://www.hd-money.com" target="_blank">HD-Money</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="hdm" type="text" id="hdm" value="<? echo $conf2[hdm] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_hdm] == "yes") { ?> <input name="accept_hdm" type="checkbox" id="accept_hdm" value="yes" checked> <? } else { ?> <input name="accept_hdm" type="checkbox" id="accept_hdm" value="yes"> <? } ?>
</div>
<br /><br />
<label><input name="update_hdmoney" type="submit" id="update_hdmoney" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'moneybookers':
?>

<fieldset><legend><a href="http://www.moneybookers.com" target="_blank">MoneyBookers | Skrill</a></legend>
<label><?php echo LANG_ADMIN_EMAILADDRESS; ?></label> <input name="mb" type="text" id="mb" value="<? echo $conf2[mb] ?>" size="35"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_mb] == "yes") { ?> <input name="accept_mb" type="checkbox" id="accept_mb" value="yes" checked> <? } else { ?> <input name="accept_mb" type="checkbox" id="accept_mb" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_mbr] == "yes") { ?> <input name="accept_mbr" type="checkbox" id="accept_mbr" value="yes" checked> <? } else { ?> <input name="accept_mbr" type="checkbox" id="accept_mbr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_mbp] == "yes") { ?> <input name="accept_mbp" type="checkbox" id="accept_mbp" value="yes" checked> <? } else { ?> <input name="accept_mbp" type="checkbox" id="accept_mbp" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_mby] == "yes") { ?> <input name="accept_mby" type="checkbox" id="accept_mby" value="yes" checked> <? } else { ?> <input name="accept_mby" type="checkbox" id="accept_mby" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_mbc] == "yes") { ?> <input name="accept_mbc" type="checkbox" id="accept_mbc" value="yes" checked> <? } else { ?> <input name="accept_mbc" type="checkbox" id="accept_mbc" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_mbd] == "yes") { ?> <input name="accept_mbd" type="checkbox" id="accept_mbd" value="yes" checked> <? } else { ?> <input name="accept_mbd" type="checkbox" id="accept_mbd" value="yes"> <? } ?> <br />
CHF (Swiss franc) <? if ($conf2[accept_mbf] == "yes") { ?> <input name="accept_mbf" type="checkbox" id="accept_mbf" value="yes" checked> <? } else { ?> <input name="accept_mbf" type="checkbox" id="accept_mbf" value="yes"> <? } ?> <br />
INR (Indian rupee) <? if ($conf2[accept_mbi] == "yes") { ?> <input name="accept_mbi" type="checkbox" id="accept_mbi" value="yes" checked> <? } else { ?> <input name="accept_mbi" type="checkbox" id="accept_mbi" value="yes"> <? } ?> <br />
SEK (Swedish krona) <? if ($conf2[accept_mbs] == "yes") { ?> <input name="accept_mbs" type="checkbox" id="accept_mbs" value="yes" checked> <? } else { ?> <input name="accept_mbs" type="checkbox" id="accept_mbs" value="yes"> <? } ?> <br />
NOK (Norwegian krone) <? if ($conf2[accept_mbn] == "yes") { ?> <input name="accept_mbn" type="checkbox" id="accept_mbn" value="yes" checked> <? } else { ?> <input name="accept_mbn" type="checkbox" id="accept_mbn" value="yes"> <? } ?> <br />
HKD (Hong Kong dollar) <? if ($conf2[accept_mbh] == "yes") { ?> <input name="accept_mbh" type="checkbox" id="accept_mbh" value="yes" checked> <? } else { ?> <input name="accept_mbh" type="checkbox" id="accept_mbh" value="yes"> <? } ?> <br />
NZD (New Zealand dollar) <? if ($conf2[accept_mbz] == "yes") { ?> <input name="accept_mbz" type="checkbox" id="accept_mbz" value="yes" checked> <? } else { ?> <input name="accept_mbz" type="checkbox" id="accept_mbz" value="yes"> <? } ?> <br />
SGD (Singapore dollar) <? if ($conf2[accept_mbsd] == "yes") { ?> <input name="accept_mbsd" type="checkbox" id="accept_mbsd" value="yes" checked> <? } else { ?> <input name="accept_mbsd" type="checkbox" id="accept_mbsd" value="yes"> <? } ?> <br />
ILS (Israeli new shekel) <? if ($conf2[accept_mbis] == "yes") { ?> <input name="accept_mbis" type="checkbox" id="accept_mbis" value="yes" checked> <? } else { ?> <input name="accept_mbis" type="checkbox" id="accept_mbis" value="yes"> <? } ?> <br />
TWD (New Taiwan dollar) <? if ($conf2[accept_mbt] == "yes") { ?> <input name="accept_mbt" type="checkbox" id="accept_mbt" value="yes" checked> <? } else { ?> <input name="accept_mbt" type="checkbox" id="accept_mbt" value="yes"> <? } ?> <br />
THB (Thailand baht) <? if ($conf2[accept_mbtb] == "yes") { ?> <input name="accept_mbtb" type="checkbox" id="accept_mbtb" value="yes" checked> <? } else { ?> <input name="accept_mbtb" type="checkbox" id="accept_mbtb" value="yes"> <? } ?> <br />
KRW (South Korean won) <? if ($conf2[accept_mbw] == "yes") { ?> <input name="accept_mbw" type="checkbox" id="accept_mbw" value="yes" checked> <? } else { ?> <input name="accept_mbw" type="checkbox" id="accept_mbw" value="yes"> <? } ?> <br />

</div>
<br /><br />
<label><input name="update_moneybookers" type="submit" id="update_moneybookers" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'nochex':
?>

<fieldset><legend><a href="http://www.nochex.com" target="_blank">NoChex</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="nc" type="text" id="nc" value="<? echo $conf2[nc] ?>" size="25"> <br />
<div id="curopt">
GBP (Pound sterling) <? if ($conf2[accept_nc] == "yes") { ?> <input name="accept_nc" type="checkbox" id="accept_nc" value="yes" checked> <? } else { ?> <input name="accept_nc" type="checkbox" id="accept_nc" value="yes"> <? } ?> 
</div>
<br /><br />
<label><input name="update_nochex" type="submit" id="update_nochex" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'paxum':
?>

<fieldset><legend><a href="http://www.paxum.com" target="_blank">Paxum</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="pax" type="text" id="pax" value="<? echo $conf2[pax] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_paxd] == "yes") { ?> <input name="accept_paxd" type="checkbox" id="accept_paxd" value="yes" checked> <? } else { ?> <input name="accept_paxd" type="checkbox" id="accept_paxd" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_paxr] == "yes") { ?> <input name="accept_paxr" type="checkbox" id="accept_paxr" value="yes" checked> <? } else { ?> <input name="accept_paxr" type="checkbox" id="accept_paxr" value="yes"> <? } ?> <br />
CAD (Canadian dollar) <? if ($conf2[accept_paxc] == "yes") { ?> <input name="accept_paxc" type="checkbox" id="accept_paxc" value="yes" checked> <? } else { ?> <input name="accept_paxc" type="checkbox" id="accept_paxc" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_paxum" type="submit" id="update_paxum" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'paymate':
?>

<fieldset><legend><a href="http://www.paymate.com" target="_blank">Paymate</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="paym" type="text" id="paym" value="<? echo $conf2[paym] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_paymd] == "yes") { ?> <input name="accept_paymd" type="checkbox" id="accept_paymd" value="yes" checked> <? } else { ?> <input name="accept_paymd" type="checkbox" id="accept_paymd" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_paymr] == "yes") { ?> <input name="accept_paymr" type="checkbox" id="accept_paymr" value="yes" checked> <? } else { ?> <input name="accept_paymr" type="checkbox" id="accept_paymr" value="yes"> <? } ?> <br />
GBP (Pound sterling) <? if ($conf2[accept_paymp] == "yes") { ?> <input name="accept_paymp" type="checkbox" id="accept_paymp" value="yes" checked> <? } else { ?> <input name="accept_paymp" type="checkbox" id="accept_paymp" value="yes"> <? } ?> <br />
AUD (Australian dollar) <? if ($conf2[accept_payma] == "yes") { ?> <input name="accept_payma" type="checkbox" id="accept_payma" value="yes" checked> <? } else { ?> <input name="accept_payma" type="checkbox" id="accept_payma" value="yes"> <? } ?> <br />
NZD (New Zealand dollar) <? if ($conf2[accept_paymz] == "yes") { ?> <input name="accept_paymz" type="checkbox" id="accept_paymz" value="yes" checked> <? } else { ?> <input name="accept_paymz" type="checkbox" id="accept_paymz" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_paymate" type="submit" id="update_paymate" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'paypal':
?>

<fieldset><legend><a href="https://www.paypal.com/us/mrb/pal=8BBNGBJULGBJE" target="_blank">PayPal</a></legend>
<label><?php echo LANG_ADMIN_EMAILADDRESS; ?></label> <input name="pp" type="text" id="pp" value="<? echo $conf2[pp] ?>" size="25"> <br />
<div id="curopt">
 USD (U.S. dollar) <? if ($conf2[accept_pp] == "yes") { ?> <input name="accept_pp" type="checkbox" id="accept_pp" value="yes" checked> <? } else { ?> <input name="accept_pp" type="checkbox" id="accept_pp" value="yes"> <? } ?> <br /> 
 EUR (Euro) <? if ($conf2[accept_ppr] == "yes") { ?> <input name="accept_ppr" type="checkbox" id="accept_ppr" value="yes" checked> <? } else { ?> <input name="accept_ppr" type="checkbox" id="accept_ppr" value="yes"> <? } ?> <br /> 
 GBP (Pound sterling) <? if ($conf2[accept_ppp] == "yes") { ?> <input name="accept_ppp" type="checkbox" id="accept_ppp" value="yes" checked> <? } else { ?> <input name="accept_ppp" type="checkbox" id="accept_ppp" value="yes"> <? } ?> <br /> 
 JPY (Japanese Yen) <? if ($conf2[accept_ppy] == "yes") { ?> <input name="accept_ppy" type="checkbox" id="accept_ppy" value="yes" checked> <? } else { ?> <input name="accept_ppy" type="checkbox" id="accept_ppy" value="yes"> <? } ?> <br /> 
 CAD (Canadian dollar) <? if ($conf2[accept_ppc] == "yes") { ?> <input name="accept_ppc" type="checkbox" id="accept_ppc" value="yes" checked> <? } else { ?> <input name="accept_ppc" type="checkbox" id="accept_ppc" value="yes"> <? } ?> <br /> 
 AUD (Australian dollar) <? if ($conf2[accept_ppd] == "yes") { ?> <input name="accept_ppd" type="checkbox" id="accept_ppd" value="yes" checked> <? } else { ?> <input name="accept_ppd" type="checkbox" id="accept_ppd" value="yes"> <? } ?><br />
 CHF (Swiss franc) <? if ($conf2[accept_ppf] == "yes") { ?> <input name="accept_ppf" type="checkbox" id="accept_ppf" value="yes" checked> <? } else { ?> <input name="accept_ppf" type="checkbox" id="accept_ppf" value="yes"> <? } ?> <br /> 
 SEK (Swedish krona) <? if ($conf2[accept_pps] == "yes") { ?> <input name="accept_pps" type="checkbox" id="accept_pps" value="yes" checked> <? } else { ?> <input name="accept_pps" type="checkbox" id="accept_pps" value="yes"> <? } ?> <br /> 
 NOK (Norwegian krone) <? if ($conf2[accept_ppn] == "yes") { ?> <input name="accept_ppn" type="checkbox" id="accept_ppn" value="yes" checked> <? } else { ?> <input name="accept_ppn" type="checkbox" id="accept_ppn" value="yes"> <? } ?> <br /> 
 MXN (Mexican peso) <? if ($conf2[accept_ppm] == "yes") { ?> <input name="accept_ppm" type="checkbox" id="accept_ppm" value="yes" checked> <? } else { ?> <input name="accept_ppm" type="checkbox" id="accept_ppm" value="yes"> <? } ?> <br /> 
 HKD (Hong Kong dollar) <? if ($conf2[accept_pph] == "yes") { ?> <input name="accept_pph" type="checkbox" id="accept_pph" value="yes" checked> <? } else { ?> <input name="accept_pph" type="checkbox" id="accept_pph" value="yes"> <? } ?> <br />
 NZD (New Zealand dollar) <? if ($conf2[accept_ppnz] == "yes") { ?> <input name="accept_ppnz" type="checkbox" id="accept_ppnz" value="yes" checked> <? } else { ?> <input name="accept_ppnz" type="checkbox" id="accept_ppnz" value="yes"> <? } ?> <br />
 PLN (Polish zloty) <? if ($conf2[accept_ppl] == "yes") { ?> <input name="accept_ppl" type="checkbox" id="accept_ppl" value="yes" checked> <? } else { ?> <input name="accept_ppl" type="checkbox" id="accept_ppl" value="yes"> <? } ?> <br />
 DKK (Danish krone) <? if ($conf2[accept_ppk] == "yes") { ?> <input name="accept_ppk" type="checkbox" id="accept_ppk" value="yes" checked> <? } else { ?> <input name="accept_ppk" type="checkbox" id="accept_ppk" value="yes"> <? } ?> <br /> 
 CZK (Czech koruna) <? if ($conf2[accept_ppz] == "yes") { ?> <input name="accept_ppz" type="checkbox" id="accept_ppz" value="yes" checked> <? } else { ?> <input name="accept_ppz" type="checkbox" id="accept_ppz" value="yes"> <? } ?> <br />
 SGD (Singapore dollar) <? if ($conf2[accept_ppg] == "yes") { ?> <input name="accept_ppg" type="checkbox" id="accept_ppg" value="yes" checked> <? } else { ?> <input name="accept_ppg" type="checkbox" id="accept_ppg" value="yes"> <? } ?> <br />
 TWD (New Taiwan dollar) <? if ($conf2[accept_ppt] == "yes") { ?> <input name="accept_ppt" type="checkbox" id="accept_ppt" value="yes" checked> <? } else { ?> <input name="accept_ppt" type="checkbox" id="accept_ppt" value="yes"> <? } ?> <br />
 THB (Thai Baht) <? if ($conf2[accept_ppb] == "yes") { ?> <input name="accept_ppb" type="checkbox" id="accept_ppb" value="yes" checked> <? } else { ?> <input name="accept_ppb" type="checkbox" id="accept_ppb" value="yes"> <? } ?> <br />
 ILS (Israeli New Shekel) <? if ($conf2[accept_ppi] == "yes") { ?> <input name="accept_ppi" type="checkbox" id="accept_ppi" value="yes" checked> <? } else { ?> <input name="accept_ppi" type="checkbox" id="accept_ppi" value="yes"> <? } ?> <br />
 RUB (Russian ruble) <? if ($conf2[accept_ppu] == "yes") { ?> <input name="accept_ppu" type="checkbox" id="accept_ppu" value="yes" checked> <? } else { ?> <input name="accept_ppu" type="checkbox" id="accept_ppu" value="yes"> <? } ?> <br />
 PHP (Philippine peso) <? if ($conf2[accept_ppo] == "yes") { ?> <input name="accept_ppo" type="checkbox" id="accept_ppo" value="yes" checked> <? } else { ?> <input name="accept_ppo" type="checkbox" id="accept_ppo" value="yes"> <? } ?> <br />
 </div>
<br /><br />
<label><input name="update_paypal" type="submit" id="update_paypal" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'pecunix':
?>

<fieldset><legend><a href="http://pecunix.com/" target="_blank">Pecunix</a></legend>
<label><?php echo LANG_ADMIN_EMAILADDRESS; ?></label> <input name="pu" type="text" id="pu" value="<? echo $conf2[pu] ?>" size="35"> <br />

<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_pu] == "yes") { ?> <input name="accept_pu" type="checkbox" id="accept_pu" value="yes" checked> <? } else { ?> <input name="accept_pu" type="checkbox" id="accept_pu" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_pur] == "yes") { ?> <input name="accept_pur" type="checkbox" id="accept_pur" value="yes" checked> <? } else { ?> <input name="accept_pur" type="checkbox" id="accept_pur" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_pecunix" type="submit" id="update_pecunix" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'perfectmoney':
?>

<fieldset><legend><a href="https://perfectmoney.is" target="_blank">PerfectMoney</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="pm" type="text" id="pm" value="<? echo $conf2[pm] ?>" size="25"> USD (U.S. dollar) <? if ($conf2[accept_pm] == "yes") { ?> <input name="accept_pm" type="checkbox" id="accept_pm" value="yes" checked> <? } else { ?> <input name="accept_pm" type="checkbox" id="accept_pm" value="yes"> <? } ?> <br />
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="pm2" type="text" id="pm2" value="<? echo $conf2[pm2] ?>" size="25"> EUR (Euro) <? if ($conf2[accept_pmr] == "yes") { ?> <input name="accept_pmr" type="checkbox" id="accept_pmr" value="yes" checked> <? } else { ?> <input name="accept_pmr" type="checkbox" id="accept_pmr" value="yes"> <? } ?> <br />

<br /><br />
<label><input name="update_perfectmoney" type="submit" id="update_perfectmoney" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'solidtrustpay':
?>

<fieldset><legend><a href="http://solidtrustpay.com/?r=5760" target="_blank">SolidTrustPay</a></legend>
<label><?php echo LANG_ADMIN_USER; ?></label> <input name="stp" type="text" id="stp" value="<? echo $conf2[stp] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_stp] == "yes") { ?> <input name="accept_stp" type="checkbox" id="accept_stp" value="yes" checked> <? } else { ?> <input name="accept_stp" type="checkbox" id="accept_stp" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_stpr] == "yes") { ?> <input name="accept_stpr" type="checkbox" id="accept_stpr" value="yes" checked> <? } else { ?> <input name="accept_stpr" type="checkbox" id="accept_stpr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_stpp] == "yes") { ?> <input name="accept_stpp" type="checkbox" id="accept_stpp" value="yes" checked> <? } else { ?> <input name="accept_stpp" type="checkbox" id="accept_stpp" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_stpy] == "yes") { ?> <input name="accept_stpy" type="checkbox" id="accept_stpy" value="yes" checked> <? } else { ?> <input name="accept_stpy" type="checkbox" id="accept_stpy" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_stpc] == "yes") { ?> <input name="accept_stpc" type="checkbox" id="accept_stpc" value="yes" checked> <? } else { ?> <input name="accept_stpc" type="checkbox" id="accept_stpc" value="yes"><? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_stpd] == "yes") { ?> <input name="accept_stpd" type="checkbox" id="accept_stpd" value="yes" checked> <? } else { ?> <input name="accept_stpd" type="checkbox" id="accept_stpd" value="yes"> <? } ?> <br /> 
CHF (Swiss franc) <? if ($conf2[accept_stpf] == "yes") { ?> <input name="accept_stpf" type="checkbox" id="accept_stpf" value="yes" checked> <? } else { ?> <input name="accept_stpf" type="checkbox" id="accept_stpf" value="yes"> <? } ?>
</div>
<br /><br />
<label><input name="update_solidtrustpay" type="submit" id="update_solidtrustpay" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'coinbin':
?>

<fieldset><legend><a href="http://coinb.in" target="_blank">coinb.in</a></legend>
<label><?php echo LANG_ADMIN_USER; ?></label> <input name="cb" type="text" id="cb" value="<? echo $conf2[cb] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_cb] == "yes") { ?> <input name="accept_cb" type="checkbox" id="accept_cb" value="yes" checked> <? } else { ?> <input name="accept_cb" type="checkbox" id="accept_cb" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_cbr] == "yes") { ?> <input name="accept_cbr" type="checkbox" id="accept_cbr" value="yes" checked> <? } else { ?> <input name="accept_cbr" type="checkbox" id="accept_cbr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_cbp] == "yes") { ?> <input name="accept_cbp" type="checkbox" id="accept_cbp" value="yes" checked> <? } else { ?> <input name="accept_cbp" type="checkbox" id="accept_cbp" value="yes"> <? } ?> <br /> 
BTC (Bitcoin) <? if ($conf2[accept_cbc] == "yes") { ?> <input name="accept_cbc" type="checkbox" id="accept_cbc" value="yes" checked> <? } else { ?> <input name="accept_cbc" type="checkbox" id="accept_cbc" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_coinbin" type="submit" id="update_coinbin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'webmoney':
?>

<fieldset><legend><a href="http://www.wmtransfer.com" target="_blank">WebMoney</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="wm" type="text" id="wm" value="<? echo $conf2[wm] ?>" size="25"> 
WMZ USD (U.S. dollar) <? if ($conf2[accept_wmz] == "yes") { ?> <input name="accept_wmz" type="checkbox" id="accept_wmz" value="yes" checked> <? } else { ?> <input name="accept_wmz" type="checkbox" id="accept_wmz" value="yes"> <? } ?> <br /> 
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="wm2" type="text" id="wm2" value="<? echo $conf2[wm2] ?>" size="25"> 
WME EUR (Euro) <? if ($conf2[accept_wme] == "yes") { ?> <input name="accept_wme" type="checkbox" id="accept_wme" value="yes" checked> <? } else { ?> <input name="accept_wme" type="checkbox" id="accept_wme" value="yes"> <? } ?> <br /> 

<br /><br />
<label><input name="update_webmoney" type="submit" id="update_webmoney" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'otherpayments':
?>
<script src="./includes/nicEdit/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function() {
	new nicEditor({fullPanel : true}).panelInstance('ops');
});
</script>

<fieldset><legend><?php echo LANG_ADMIN_OTHERPAY; ?></legend>
<?php echo LANG_ADMIN_OTHERPAY2; ?><br />
<input name="ops2" type="text" id="ops2" value="<? echo $conf2[ops2] ?>" size="70"> <br />
<?php echo LANG_ADMIN_OTHERPAY3; ?><br />
<textarea name="ops" cols="65" rows="10" id="ops"><? echo $conf2[ops] ?></textarea> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_ops] == "yes") { ?> <input name="accept_ops" type="checkbox" id="accept_ops" value="yes" checked> <? } else { ?> <input name="accept_ops" type="checkbox" id="accept_ops" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_ops2] == "yes") { ?> <input name="accept_ops2" type="checkbox" id="accept_ops2" value="yes" checked> <? } else { ?> <input name="accept_ops2" type="checkbox" id="accept_ops2" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_ops3] == "yes") { ?> <input name="accept_ops3" type="checkbox" id="accept_ops3" value="yes" checked> <? } else { ?> <input name="accept_ops3" type="checkbox" id="accept_ops3" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_ops4] == "yes") { ?> <input name="accept_ops4" type="checkbox" id="accept_ops4" value="yes" checked> <? } else { ?> <input name="accept_ops4" type="checkbox" id="accept_ops4" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_ops5] == "yes") { ?> <input name="accept_ops5" type="checkbox" id="accept_ops5" value="yes" checked> <? } else { ?> <input name="accept_ops5" type="checkbox" id="accept_ops5" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_ops6] == "yes") { ?> <input name="accept_ops6" type="checkbox" id="accept_ops6" value="yes" checked> <? } else { ?> <input name="accept_ops6" type="checkbox" id="accept_ops6" value="yes"> <? } ?> <br /> 
CHF (Swiss franc) <? if ($conf2[accept_ops7] == "yes") { ?> <input name="accept_ops7" type="checkbox" id="accept_ops7" value="yes" checked> <? } else { ?> <input name="accept_ops7" type="checkbox" id="accept_ops7" value="yes"> <? } ?> <br /> 
BTC (Bitcoin) <? if ($conf2[accept_ops8] == "yes") { ?> <input name="accept_ops8" type="checkbox" id="accept_ops8" value="yes" checked> <? } else { ?> <input name="accept_ops8" type="checkbox" id="accept_ops8" value="yes"> <? } ?> <br /> 
INR (Indian rupee) <? if ($conf2[accept_ops9] == "yes") { ?> <input name="accept_ops9" type="checkbox" id="accept_ops9" value="yes" checked> <? } else { ?> <input name="accept_ops9" type="checkbox" id="accept_ops9" value="yes"> <? } ?> <br /> 
SEK (Swedish krona) <? if ($conf2[accept_ops10] == "yes") { ?> <input name="accept_ops10" type="checkbox" id="accept_ops10" value="yes" checked> <? } else { ?> <input name="accept_ops10" type="checkbox" id="accept_ops10" value="yes"> <? } ?> <br /> 
NOK (Norwegian krone) <? if ($conf2[accept_ops11] == "yes") { ?> <input name="accept_ops11" type="checkbox" id="accept_ops11" value="yes" checked> <? } else { ?> <input name="accept_ops11" type="checkbox" id="accept_ops11" value="yes"> <? } ?> <br /> 
MXN (Mexican peso) <? if ($conf2[accept_ops12] == "yes") { ?> <input name="accept_ops12" type="checkbox" id="accept_ops12" value="yes" checked> <? } else { ?> <input name="accept_ops12" type="checkbox" id="accept_ops12" value="yes"> <? } ?> <br /> 
HKD (Hong Kong dollar) <? if ($conf2[accept_ops13] == "yes") { ?> <input name="accept_ops13" type="checkbox" id="accept_ops13" value="yes" checked> <? } else { ?> <input name="accept_ops13" type="checkbox" id="accept_ops13" value="yes"> <? } ?> <br /> 
NZD (New Zealand dollar) <? if ($conf2[accept_ops14] == "yes") { ?> <input name="accept_ops14" type="checkbox" id="accept_ops14" value="yes" checked> <? } else { ?> <input name="accept_ops14" type="checkbox" id="accept_ops14" value="yes"> <? } ?> <br /> 
PLN (Polish zloty) <? if ($conf2[accept_ops15] == "yes") { ?> <input name="accept_ops15" type="checkbox" id="accept_ops15" value="yes" checked> <? } else { ?> <input name="accept_ops15" type="checkbox" id="accept_ops15" value="yes"> <? } ?> <br /> 
DKK (Danish krone) <? if ($conf2[accept_ops16] == "yes") { ?> <input name="accept_ops16" type="checkbox" id="accept_ops16" value="yes" checked> <? } else { ?> <input name="accept_ops16" type="checkbox" id="accept_ops16" value="yes"> <? } ?> <br /> 
CZK (Czech koruna) <? if ($conf2[accept_ops17] == "yes") { ?> <input name="accept_ops17" type="checkbox" id="accept_ops17" value="yes" checked> <? } else { ?> <input name="accept_ops17" type="checkbox" id="accept_ops17" value="yes"> <? } ?> <br /> 
SGD (Singapore dollar) <? if ($conf2[accept_ops18] == "yes") { ?> <input name="accept_ops18" type="checkbox" id="accept_ops18" value="yes" checked> <? } else { ?> <input name="accept_ops18" type="checkbox" id="accept_ops18" value="yes"> <? } ?> <br /> 
TWD (New Taiwan dollar) <? if ($conf2[accept_ops19] == "yes") { ?> <input name="accept_ops19" type="checkbox" id="accept_ops19" value="yes" checked> <? } else { ?> <input name="accept_ops19" type="checkbox" id="accept_ops19" value="yes"> <? } ?> <br /> 
THB (Thailand baht) <? if ($conf2[accept_ops20] == "yes") { ?> <input name="accept_ops20" type="checkbox" id="accept_ops20" value="yes" checked> <? } else { ?> <input name="accept_ops20" type="checkbox" id="accept_ops20" value="yes"> <? } ?> <br /> 
ILS (Israeli new shekel) <? if ($conf2[accept_ops21] == "yes") { ?> <input name="accept_ops21" type="checkbox" id="accept_ops21" value="yes" checked> <? } else { ?> <input name="accept_ops21" type="checkbox" id="accept_ops21" value="yes"> <? } ?> <br /> 
RUB (Russian ruble) <? if ($conf2[accept_ops22] == "yes") { ?> <input name="accept_ops22" type="checkbox" id="accept_ops22" value="yes" checked> <? } else { ?> <input name="accept_ops22" type="checkbox" id="accept_ops22" value="yes"> <? } ?> <br /> 
KRW (South Korean won) <? if ($conf2[accept_ops23] == "yes") { ?> <input name="accept_ops23" type="checkbox" id="accept_ops23" value="yes" checked> <? } else { ?> <input name="accept_ops23" type="checkbox" id="accept_ops23" value="yes"> <? } ?> <br /> 
CNY (Chinese Yuan Renminbi) <? if ($conf2[accept_ops24] == "yes") { ?> <input name="accept_ops24" type="checkbox" id="accept_ops24" value="yes" checked> <? } else { ?> <input name="accept_ops24" type="checkbox" id="accept_ops24" value="yes"> <? } ?> <br /> 
PHP (Philippine peso) <? if ($conf2[accept_ops25] == "yes") { ?> <input name="accept_ops25" type="checkbox" id="accept_ops25" value="yes" checked> <? } else { ?> <input name="accept_ops25" type="checkbox" id="accept_ops25" value="yes"> <? } ?> <br /> 
LTC (Litecoin) <? if ($conf2[accept_ops26] == "yes") { ?> <input name="accept_ops26" type="checkbox" id="accept_ops26" value="yes" checked> <? } else { ?> <input name="accept_ops26" type="checkbox" id="accept_ops26" value="yes"> <? } ?> <br /> 
FTC (Feathercoin) <? if ($conf2[accept_ops27] == "yes") { ?> <input name="accept_ops27" type="checkbox" id="accept_ops27" value="yes" checked> <? } else { ?> <input name="accept_ops27" type="checkbox" id="accept_ops27" value="yes"> <? } ?> <br /> 
PPC (Peercoin) <? if ($conf2[accept_ops28] == "yes") { ?> <input name="accept_ops28" type="checkbox" id="accept_ops28" value="yes" checked> <? } else { ?> <input name="accept_ops28" type="checkbox" id="accept_ops28" value="yes"> <? } ?> <br /> 
NMC (Namecoin) <? if ($conf2[accept_ops29] == "yes") { ?> <input name="accept_ops29" type="checkbox" id="accept_ops29" value="yes" checked> <? } else { ?> <input name="accept_ops29" type="checkbox" id="accept_ops29" value="yes"> <? } ?> <br /> 

</div>
<br /><br />
<label><input name="update_otherpayments" type="submit" id="update_otherpayments" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'dwolla':
?>

<fieldset><legend><a href="http://refer.dwolla.com/a/clk/20l0Yn" target="_blank">Dwolla</a></legend>
<label>Application Key</label> <input name="dwok" type="text" id="dwok" value="<? echo $conf2[dwok] ?>" size="45"> <br />
<label>Application Secret</label> <input name="dwos" type="text" id="dwos" value="<? echo $conf2[dwos] ?>" size="45"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_dw1] == "yes") { ?> <input name="accept_dw1" type="checkbox" id="accept_dw1" value="yes" checked> <? } else { ?> <input name="accept_dw1" type="checkbox" id="accept_dw1" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_dwolla" type="submit" id="update_dwolla" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'okpay':
?>

<fieldset><legend><a href="http://www.okpay.com" target="_blank">OKPAY</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="okp" type="text" id="okp" value="<? echo $conf2[okp] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_okp1] == "yes") { ?> <input name="accept_okp1" type="checkbox" id="accept_okp1" value="yes" checked> <? } else { ?> <input name="accept_okp1" type="checkbox" id="accept_okp1" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_okp2] == "yes") { ?> <input name="accept_okp2" type="checkbox" id="accept_okp2" value="yes" checked> <? } else { ?> <input name="accept_okp2" type="checkbox" id="accept_okp2" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_okp3] == "yes") { ?> <input name="accept_okp3" type="checkbox" id="accept_okp3" value="yes" checked> <? } else { ?> <input name="accept_okp3" type="checkbox" id="accept_okp3" value="yes"> <? } ?> <br /> 
JPY (Japanese Yen) <? if ($conf2[accept_okp4] == "yes") { ?> <input name="accept_okp4" type="checkbox" id="accept_okp4" value="yes" checked> <? } else { ?> <input name="accept_okp4" type="checkbox" id="accept_okp4" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_okp5] == "yes") { ?> <input name="accept_okp5" type="checkbox" id="accept_okp5" value="yes" checked> <? } else { ?> <input name="accept_okp5" type="checkbox" id="accept_okp5" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_okp6] == "yes") { ?> <input name="accept_okp6" type="checkbox" id="accept_okp6" value="yes" checked> <? } else { ?> <input name="accept_okp6" type="checkbox" id="accept_okp6" value="yes"> <? } ?> <br /> 
CHF (Swiss franc) <? if ($conf2[accept_okp7] == "yes") { ?> <input name="accept_okp7" type="checkbox" id="accept_okp7" value="yes" checked> <? } else { ?> <input name="accept_okp7" type="checkbox" id="accept_okp7" value="yes"> <? } ?> <br /> 
NOK (Norwegian krone) <? if ($conf2[accept_okp11] == "yes") { ?> <input name="accept_okp11" type="checkbox" id="accept_okp11" value="yes" checked> <? } else { ?> <input name="accept_okp11" type="checkbox" id="accept_okp11" value="yes"> <? } ?> <br /> 
MXN (Mexican peso) <? if ($conf2[accept_okp12] == "yes") { ?> <input name="accept_okp12" type="checkbox" id="accept_okp12" value="yes" checked> <? } else { ?> <input name="accept_okp12" type="checkbox" id="accept_okp12" value="yes"> <? } ?> <br /> 
NZD (New Zealand dollar) <? if ($conf2[accept_okp14] == "yes") { ?> <input name="accept_okp14" type="checkbox" id="accept_okp14" value="yes" checked> <? } else { ?> <input name="accept_okp14" type="checkbox" id="accept_okp14" value="yes"> <? } ?> <br /> 
PLN (Polish zloty) <? if ($conf2[accept_okp15] == "yes") { ?> <input name="accept_okp15" type="checkbox" id="accept_okp15" value="yes" checked> <? } else { ?> <input name="accept_okp15" type="checkbox" id="accept_okp15" value="yes"> <? } ?> <br /> 
DKK (Danish krone) <? if ($conf2[accept_okp16] == "yes") { ?> <input name="accept_okp16" type="checkbox" id="accept_okp16" value="yes" checked> <? } else { ?> <input name="accept_okp16" type="checkbox" id="accept_okp16" value="yes"> <? } ?> <br /> 
CZK (Czech koruna) <? if ($conf2[accept_okp17] == "yes") { ?> <input name="accept_okp17" type="checkbox" id="accept_okp17" value="yes" checked> <? } else { ?> <input name="accept_okp17" type="checkbox" id="accept_okp17" value="yes"> <? } ?> <br /> 
SGD (Singapore dollar) <? if ($conf2[accept_okp18] == "yes") { ?> <input name="accept_okp18" type="checkbox" id="accept_okp18" value="yes" checked> <? } else { ?> <input name="accept_okp18" type="checkbox" id="accept_okp18" value="yes"> <? } ?> <br /> 
TWD (New Taiwan dollar) <? if ($conf2[accept_okp19] == "yes") { ?> <input name="accept_okp19" type="checkbox" id="accept_okp19" value="yes" checked> <? } else { ?> <input name="accept_okp19" type="checkbox" id="accept_okp19" value="yes"> <? } ?> <br /> 
ILS (Israeli new shekel) <? if ($conf2[accept_okp21] == "yes") { ?> <input name="accept_okp21" type="checkbox" id="accept_okp21" value="yes" checked> <? } else { ?> <input name="accept_okp21" type="checkbox" id="accept_okp21" value="yes"> <? } ?> <br /> 
RUB (Russian ruble) <? if ($conf2[accept_okp22] == "yes") { ?> <input name="accept_okp22" type="checkbox" id="accept_okp22" value="yes" checked> <? } else { ?> <input name="accept_okp22" type="checkbox" id="accept_okp22" value="yes"> <? } ?> <br /> 
CNY (Chinese Yuan Renminbi) <? if ($conf2[accept_okp24] == "yes") { ?> <input name="accept_okp24" type="checkbox" id="accept_okp24" value="yes" checked> <? } else { ?> <input name="accept_okp24" type="checkbox" id="accept_okp24" value="yes"> <? } ?> <br /> 
PHP (Philippine peso) <? if ($conf2[accept_okp25] == "yes") { ?> <input name="accept_okp25" type="checkbox" id="accept_okp25" value="yes" checked> <? } else { ?> <input name="accept_okp25" type="checkbox" id="accept_okp25" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_okpay" type="submit" id="update_okpay" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'egopay':
?>

<fieldset><legend><a href="https://www.egopay.com/?DYs4fZ" target="_blank">EgoPay</a></legend>
<label><?php echo LANG_ADMIN_STOREID; ?></label> <input name="ego" type="text" id="ego" value="<? echo $conf2[ego] ?>" size="25"> <br />
<label><?php echo LANG_MAIN_PASSWORD; ?></label> <input name="ego2" type="text" id="ego2" value="<? echo $conf2[ego2] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_egod] == "yes") { ?> <input name="accept_egod" type="checkbox" id="accept_egod" value="yes" checked> <? } else { ?> <input name="accept_egod" type="checkbox" id="accept_egod" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_egor] == "yes") { ?> <input name="accept_egor" type="checkbox" id="accept_egor" value="yes" checked> <? } else { ?> <input name="accept_egor" type="checkbox" id="accept_egor" value="yes"> <? } ?> <br />
</div>
<br /><br />
<label><input name="update_egopay" type="submit" id="update_egopay" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'sensipay':
?>

<fieldset><legend><a href="https://www.sensipay.com/" target="_blank">SensiPay</a></legend>
<label><?php echo LANG_ADMIN_USER; ?></label> <input name="se" type="text" id="se" value="<? echo $conf2[se] ?>" size="25"> <br />
<label><?php echo LANG_ADMIN_STORENAME; ?></label> <input name="sestore" type="text" id="sestore" value="<? echo $conf2[sestore] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_se] == "yes") { ?> <input name="accept_se" type="checkbox" id="accept_se" value="yes" checked> <? } else { ?> <input name="accept_se" type="checkbox" id="accept_se" value="yes"> <? } ?> <br /> 

</div>
<br /><br />
<label><input name="update_sensipay" type="submit" id="update_sensipay" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'worldpay':
?>

<fieldset><legend><a href="http://www.worldpay.com" target="_blank">WorldPay</a></legend>
<label><?php echo LANG_ADMIN_USER; ?></label> <input name="wp" type="text" id="wp" value="<? echo $conf2[wp] ?>" size="25"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_wp] == "yes") { ?> <input name="accept_wp" type="checkbox" id="accept_wp" value="yes" checked> <? } else { ?> <input name="accept_wp" type="checkbox" id="accept_wp" value="yes"> <? } ?> <br /> 
EUR (Euro) <? if ($conf2[accept_wpr] == "yes") { ?> <input name="accept_wpr" type="checkbox" id="accept_wpr" value="yes" checked> <? } else { ?> <input name="accept_wpr" type="checkbox" id="accept_wpr" value="yes"> <? } ?> <br /> 
GBP (Pound sterling) <? if ($conf2[accept_wpp] == "yes") { ?> <input name="accept_wpp" type="checkbox" id="accept_wpp" value="yes" checked> <? } else { ?> <input name="accept_wpp" type="checkbox" id="accept_wpp" value="yes"> <? } ?> <br /> 
CAD (Canadian dollar) <? if ($conf2[accept_wpc] == "yes") { ?> <input name="accept_wpc" type="checkbox" id="accept_wpc" value="yes" checked> <? } else { ?> <input name="accept_wpc" type="checkbox" id="accept_wpc" value="yes"> <? } ?> <br /> 
AUD (Australian dollar) <? if ($conf2[accept_wpa] == "yes") { ?> <input name="accept_wpa" type="checkbox" id="accept_wpa" value="yes" checked> <? } else { ?> <input name="accept_wpa" type="checkbox" id="accept_wpa" value="yes"> <? } ?> <br /> 
CHF (Swiss franc) <? if ($conf2[accept_wpf] == "yes") { ?> <input name="accept_wpf" type="checkbox" id="accept_wpf" value="yes" checked> <? } else { ?> <input name="accept_wpf" type="checkbox" id="accept_wpf" value="yes"> <? } ?> <br /> 
NOK (Norwegian krone) <? if ($conf2[accept_wpn] == "yes") { ?> <input name="accept_wpn" type="checkbox" id="accept_wpn" value="yes" checked> <? } else { ?> <input name="accept_wpn" type="checkbox" id="accept_wpn" value="yes"> <? } ?> <br /> 
HKD (Hong Kong dollar) <? if ($conf2[accept_wph] == "yes") { ?> <input name="accept_wph" type="checkbox" id="accept_wph" value="yes" checked> <? } else { ?> <input name="accept_wph" type="checkbox" id="accept_wph" value="yes"> <? } ?> <br /> 
NZD (New Zealand dollar) <? if ($conf2[accept_wpz] == "yes") { ?> <input name="accept_wpz" type="checkbox" id="accept_wpz" value="yes" checked> <? } else { ?> <input name="accept_wpz" type="checkbox" id="accept_wpz" value="yes"> <? } ?> <br /> 
DKK (Danish krone) <? if ($conf2[accept_wpd] == "yes") { ?> <input name="accept_wpd" type="checkbox" id="accept_wpd" value="yes" checked> <? } else { ?> <input name="accept_wpd" type="checkbox" id="accept_wpd" value="yes"> <? } ?> <br /> 
SGD (Singapore dollar) <? if ($conf2[accept_wps] == "yes") { ?> <input name="accept_wps" type="checkbox" id="accept_wps" value="yes" checked> <? } else { ?> <input name="accept_wps" type="checkbox" id="accept_wps" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_worldpay" type="submit" id="update_worldpay" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'bitcoin':
?>

<fieldset><legend><a href="http://bitcoin.org" target="_blank">Bitcoin</a></legend>
<label>Address</label> <input name="bc" type="text" id="bc" value="<? echo $conf2['bc'] ?>" size="45"> <br />
<div id="curopt">
BTC <? if ($conf2[accept_btc] == "yes") { ?> <input name="accept_btc" type="checkbox" id="accept_btc" value="yes" checked> <? } else { ?> <input name="accept_btc" type="checkbox" id="accept_btc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_bitcoin" type="submit" id="update_bitcoin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'litecoin':
?>

<fieldset><legend><a href="https://litecoin.org" target="_blank">Litecoin</a></legend>
<label>Address</label> <input name="lc" type="text" id="lc" value="<? echo $conf2['lc'] ?>" size="45"> <br />
<div id="curopt">
LTC <? if ($conf2[accept_ltc] == "yes") { ?> <input name="accept_ltc" type="checkbox" id="accept_ltc" value="yes" checked> <? } else { ?> <input name="accept_ltc" type="checkbox" id="accept_ltc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_litecoin" type="submit" id="update_litecoin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'feathercoin':
?>

<fieldset><legend><a href="http://www.feathercoin.com" target="_blank">Feathercoin</a></legend>
<label>Address</label> <input name="fc" type="text" id="fc" value="<? echo $conf2['fc'] ?>" size="45"> <br />
<div id="curopt">
FTC <? if ($conf2[accept_ftc] == "yes") { ?> <input name="accept_ftc" type="checkbox" id="accept_ftc" value="yes" checked> <? } else { ?> <input name="accept_ftc" type="checkbox" id="accept_ftc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_feathercoin" type="submit" id="update_feathercoin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'peercoin':
?>

<fieldset><legend><a href="http://www.peercoin.net" target="_blank">Peercoin</a></legend>
<label>Address</label> <input name="pc" type="text" id="pc" value="<? echo $conf2['pc'] ?>" size="45"> <br />
<div id="curopt">
PPC <? if ($conf2[accept_pppc] == "yes") { ?> <input name="accept_pppc" type="checkbox" id="accept_pppc" value="yes" checked> <? } else { ?> <input name="accept_pppc" type="checkbox" id="accept_pppc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_peercoin" type="submit" id="update_peercoin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'coinpayments':
?>

<fieldset><legend><a href="https://coinpayments.net" target="_blank">CoinPayments</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="cp" type="text" id="cp" value="<? echo $conf2['cp'] ?>" size="45"> <br />
<br />
*If you do not see your preferred currency here, then select one to enable to allow customer to see CoinPayments payment option.
You should have your preferred currency or more enabled in '<strong>My Account</strong>', '<strong><a href="https://www.coinpayments.net/index.php?cmd=acct_coins" target="_blank">What Coins to Accept</a></strong>'.
<br /><br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_cpusd] == "yes") { ?> <input name="accept_cpusd" type="checkbox" id="accept_cpusd" value="yes" checked> <? } else { ?> <input name="accept_cpusd" type="checkbox" id="accept_cpusd" value="yes"> <? } ?> <br />
EUR (Euro) <? if ($conf2[accept_cpeur] == "yes") { ?> <input name="accept_cpeur" type="checkbox" id="accept_cpeur" value="yes" checked> <? } else { ?> <input name="accept_cpeur" type="checkbox" id="accept_cpeur" value="yes"> <? } ?> <br />
GBP (Pound sterling) <? if ($conf2[accept_cpgbp] == "yes") { ?> <input name="accept_cpgbp" type="checkbox" id="accept_cpgbp" value="yes" checked> <? } else { ?> <input name="accept_cpgbp" type="checkbox" id="accept_cpgbp" value="yes"> <? } ?> <br />
CAD (Canadian dollar) <? if ($conf2[accept_cpcad] == "yes") { ?> <input name="accept_cpcad" type="checkbox" id="accept_cpcad" value="yes" checked> <? } else { ?> <input name="accept_cpcad" type="checkbox" id="accept_cpcad" value="yes"> <? } ?> <br />
AUD (Australian dollar) <? if ($conf2[accept_cpaud] == "yes") { ?> <input name="accept_cpaud" type="checkbox" id="accept_cpaud" value="yes" checked> <? } else { ?> <input name="accept_cpaud" type="checkbox" id="accept_cpaud" value="yes"> <? } ?> <br />
BTC (Bitcoin) <? if ($conf2[accept_cpbtc] == "yes") { ?> <input name="accept_cpbtc" type="checkbox" id="accept_cpbtc" value="yes" checked> <? } else { ?> <input name="accept_cpbtc" type="checkbox" id="accept_cpbtc" value="yes"> <? } ?> <br /> 
LTC (Litecoin) <? if ($conf2[accept_cpltc] == "yes") { ?> <input name="accept_cpltc" type="checkbox" id="accept_cpltc" value="yes" checked> <? } else { ?> <input name="accept_cpltc" type="checkbox" id="accept_cpltc" value="yes"> <? } ?> <br /> 
FTC (Feathercoin) <? if ($conf2[accept_cpftc] == "yes") { ?> <input name="accept_cpftc" type="checkbox" id="accept_cpftc" value="yes" checked> <? } else { ?> <input name="accept_cpftc" type="checkbox" id="accept_cpftc" value="yes"> <? } ?> <br /> 
PPC (Peercoin) <? if ($conf2[accept_cpppc] == "yes") { ?> <input name="accept_cpppc" type="checkbox" id="accept_cpppc" value="yes" checked> <? } else { ?> <input name="accept_cpppc" type="checkbox" id="accept_cpppc" value="yes"> <? } ?> <br /> 
NMC (Namecoin) <? if ($conf2[accept_cpnmc] == "yes") { ?> <input name="accept_cpnmc" type="checkbox" id="accept_cpnmc" value="yes" checked> <? } else { ?> <input name="accept_cpnmc" type="checkbox" id="accept_cpnmc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_coinpayments" type="submit" id="update_coinpayments" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'namecoin':
?>

<fieldset><legend><a href="http://namecoin.info" target="_blank">Namecoin</a></legend>
<label>Address</label> <input name="nmc2" type="text" id="nmc2" value="<? echo $conf2['nmc2'] ?>" size="45"> <br />
<div id="curopt">
NMC <? if ($conf2[accept_nmc] == "yes") { ?> <input name="accept_nmc" type="checkbox" id="accept_nmc" value="yes" checked> <? } else { ?> <input name="accept_nmc" type="checkbox" id="accept_nmc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_namecoin" type="submit" id="update_namecoin" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case 'btcmerch':
?>

<fieldset><legend><a href="https://www.btcmerch.com" target="_blank">BTCMerch</a></legend>
<label><?php echo LANG_ADMIN_USERID; ?></label> <input name="bm" type="text" id="bm" value="<? echo $conf2['bm'] ?>" size="45"> <br />
<div id="curopt">
USD (U.S. dollar) <? if ($conf2[accept_bmusd] == "yes") { ?> <input name="accept_bmusd" type="checkbox" id="accept_bmusd" value="yes" checked> <? } else { ?> <input name="accept_bmusd" type="checkbox" id="accept_bmusd" value="yes"> <? } ?> <br />
EUR (Euro) <? if ($conf2[accept_bmeur] == "yes") { ?> <input name="accept_bmeur" type="checkbox" id="accept_bmeur" value="yes" checked> <? } else { ?> <input name="accept_bmeur" type="checkbox" id="accept_bmeur" value="yes"> <? } ?> <br />
GBP (Pound sterling) <? if ($conf2[accept_bmgbp] == "yes") { ?> <input name="accept_bmgbp" type="checkbox" id="accept_bmgbp" value="yes" checked> <? } else { ?> <input name="accept_bmgbp" type="checkbox" id="accept_bmgbp" value="yes"> <? } ?> <br />
CAD (Canadian dollar) <? if ($conf2[accept_bmcad] == "yes") { ?> <input name="accept_bmcad" type="checkbox" id="accept_bmcad" value="yes" checked> <? } else { ?> <input name="accept_bmcad" type="checkbox" id="accept_bmcad" value="yes"> <? } ?> <br />
AUD (Australian dollar) <? if ($conf2[accept_bmaud] == "yes") { ?> <input name="accept_bmaud" type="checkbox" id="accept_bmaud" value="yes" checked> <? } else { ?> <input name="accept_bmaud" type="checkbox" id="accept_bmaud" value="yes"> <? } ?> <br />
JPY (Japanese yen) <? if ($conf2[accept_bmjpy] == "yes") { ?> <input name="accept_bmjpy" type="checkbox" id="accept_bmjpy" value="yes" checked> <? } else { ?> <input name="accept_bmjpy" type="checkbox" id="accept_bmjpy" value="yes"> <? } ?> <br />
CHF (Swiss franc) <? if ($conf2[accept_bmchf] == "yes") { ?> <input name="accept_bmchf" type="checkbox" id="accept_bmchf" value="yes" checked> <? } else { ?> <input name="accept_bmchf" type="checkbox" id="accept_bmchf" value="yes"> <? } ?> <br />
SEK (Swedish korna) <? if ($conf2[accept_bmsek] == "yes") { ?> <input name="accept_bmsek" type="checkbox" id="accept_bmsek" value="yes" checked> <? } else { ?> <input name="accept_bmsek" type="checkbox" id="accept_bmsek" value="yes"> <? } ?> <br />
NOK (Norwegian krone) <? if ($conf2[accept_bmnok] == "yes") { ?> <input name="accept_bmnok" type="checkbox" id="accept_bmnok" value="yes" checked> <? } else { ?> <input name="accept_bmnok" type="checkbox" id="accept_bmnok" value="yes"> <? } ?> <br /> 
NZD (New Zealand dollar) <? if ($conf2[accept_bmnzd] == "yes") { ?> <input name="accept_bmnzd" type="checkbox" id="accept_bmnzd" value="yes" checked> <? } else { ?> <input name="accept_bmnzd" type="checkbox" id="accept_bmnzd" value="yes"> <? } ?> <br /> 
PLN (Polish zloty) <? if ($conf2[accept_bmpln] == "yes") { ?> <input name="accept_bmpln" type="checkbox" id="accept_bmpln" value="yes" checked> <? } else { ?> <input name="accept_bmpln" type="checkbox" id="accept_bmpln" value="yes"> <? } ?> <br /> 
DKK (Danish krone) <? if ($conf2[accept_bmdkk] == "yes") { ?> <input name="accept_bmdkk" type="checkbox" id="accept_bmdkk" value="yes" checked> <? } else { ?> <input name="accept_bmdkk" type="checkbox" id="accept_bmdkk" value="yes"> <? } ?> <br /> 
SGD (Singapore dollar) <? if ($conf2[accept_bmsgd] == "yes") { ?> <input name="accept_bmsgd" type="checkbox" id="accept_bmsgd" value="yes" checked> <? } else { ?> <input name="accept_bmsgd" type="checkbox" id="accept_bmsgd" value="yes"> <? } ?> <br /> 
RUB (Russian ruble) <? if ($conf2[accept_bmrub] == "yes") { ?> <input name="accept_bmrub" type="checkbox" id="accept_bmrub" value="yes" checked> <? } else { ?> <input name="accept_bmrub" type="checkbox" id="accept_bmrub" value="yes"> <? } ?> <br /> 
CNY (Chinese Yuan Renminbi) <? if ($conf2[accept_bmcny] == "yes") { ?> <input name="accept_bmcny" type="checkbox" id="accept_bmcny" value="yes" checked> <? } else { ?> <input name="accept_bmcny" type="checkbox" id="accept_bmcny" value="yes"> <? } ?> <br /> 
THB (Thailand baht) <? if ($conf2[accept_bmthb] == "yes") { ?> <input name="accept_bmthb" type="checkbox" id="accept_bmthb" value="yes" checked> <? } else { ?> <input name="accept_bmthb" type="checkbox" id="accept_bmthb" value="yes"> <? } ?> <br />
HKD (Hong Kong dollar) <? if ($conf2[accept_bmhkd] == "yes") { ?> <input name="accept_bmhkd" type="checkbox" id="accept_bmhkd" value="yes" checked> <? } else { ?> <input name="accept_bmhkd" type="checkbox" id="accept_bmhkd" value="yes"> <? } ?> <br />
BTC (Bitcoin) <? if ($conf2[accept_bmbtc] == "yes") { ?> <input name="accept_bmbtc" type="checkbox" id="accept_bmbtc" value="yes" checked> <? } else { ?> <input name="accept_bmbtc" type="checkbox" id="accept_bmbtc" value="yes"> <? } ?> <br /> 
LTC (Litecoin) <? if ($conf2[accept_bmltc] == "yes") { ?> <input name="accept_bmltc" type="checkbox" id="accept_bmltc" value="yes" checked> <? } else { ?> <input name="accept_bmltc" type="checkbox" id="accept_bmltc" value="yes"> <? } ?> <br /> 
</div>
<br /><br />
<label><input name="update_btcmerch" type="submit" id="update_btcmerch" value="<?php echo LANG_ADMIN_UPDATEBUT; ?>"><label>
</fieldset>

<?
  break;
case '':
  echo '<div style=\'width:500px; height:250px;\'>?</div>';
  break;
default:
  echo '<div style=\'width:500px; height:250px;\'>?</div>';
}
?>
</form>

</div>
	</div>
	<div class="column grid_3">
	</div>
</div>