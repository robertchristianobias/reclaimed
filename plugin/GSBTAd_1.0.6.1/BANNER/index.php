<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
session_start();
$timeout=2100; // Number of seconds until it times out.
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
    // See if the number of seconds since the last visit is larger than the timeout period.
    $duration = time() - (int)$_SESSION['timeout'];
    if($duration > $timeout) {
        // Destroy the session and restart it.
		unset($_SESSION['email']);
		unset($_SESSION['password']);
		unset($_COOKIE['email']);
		unset($_COOKIE['password']);
		setcookie('email', '', time() - 3600);
		setcookie('password', '', time() - 3600);
		$_SESSION = array();
		session_destroy();
        session_start();
    }
}
// Update the timout field with the current time.
$_SESSION['timeout'] = time();
header('Cache-control: private'); // IE 6 FIX
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	ob_start("ob_gzhandler");
else
	ob_start();
header('Content-Type: text/html; charset=utf-8');
ini_set('default_charset', 'utf-8');
mb_internal_encoding('utf-8');
mb_detect_order('utf-8');
//---- Turn off all error reporting ---- 
error_reporting(0);
//---- display no errors ---- 
@ini_set(�display_errors�, 0);
//---- Turn on error reporting to all but notices ---- 
//error_reporting(E_ALL ^ E_NOTICE);
// *************************************************************
// QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK
// provided by AirCraft24.com / www.aircraft24.com
// version 1.8, released 2013-02-04 15:05 GMT+1
// *************************************************************
$phpbug_53632_vars='';
if (isset($_GET))    $phpbug_53632_vars.='|'.serialize($_GET);
if (isset($_POST))   $phpbug_53632_vars.='|'.serialize($_POST);
if (isset($_COOKIE)) $phpbug_53632_vars.='|'.serialize($_COOKIE);

if ($phpbug_53632_vars!='') 
{
  if (strpos(str_replace('.','',$phpbug_53632_vars), '22250738585072013')!== FALSE) 
  {
    if (!headers_sent()) header('Status: 422 Unprocessable Entity');
    die ('Script interrupted due to floating point DoS attack.');
  }
}
unset($phpbug_53632_vars); // cleanup
// *************************************************************
// END QUICK FIX / WORKAROUND FOR PHP FLOATING POINT DOS ATTACK	
// *************************************************************
function sanitize_output($buffer) {
	$search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
    );
    $replace = array(
        '>',
        '<',
        '\\1'
    );
$buffer = preg_replace($search, $replace, $buffer);
return $buffer;
}
ob_start("sanitize_output");
include ('includes/connect.php');
include_once ('includes/functions.php');
include_once ('includes/langswitcher.php');
include_once ('includes/currswitcher.php');
include_once ('includes/cssswitcher.php');
include_once ('includes/forms.php');
include_once ('pages/pricing3.php');
// Do the following silent crons
include_once ('includes/ads_cron.php');
//if ($conf['gcurrency'] == 'yes'){include_once ('includes/currs_cron.php');}
$_REQUEST['check'] = authenticate($_REQUEST['email'],$_REQUEST['password']);
if ($_GET['action'] == "")
{
	$include = "main.php";
	$view = "Welcome";	
}
if($_GET['action'] == "login")
{
	if($_REQUEST['check']>0)
	{
		$include = "members.php";
		$view = "Advertiser Area";
	}
	else
	{
		$include = "login.php";
		$view = "Login";
	}
}
if($_GET['action'] == "members")
{
	$include = "members.php";
	$view = "Advertiser Area";
}
if($_GET['action'] == "failed")
{
	$include = "failed.php";
	$view = "Login Failed";
}
if($_GET['action'] == "unauthorized")
{
	$include = "unauthorized.php";
	$view = "Illegal Access";
}
if ($_GET['action'] == "signup")
{
	$include = "signup.php";
	$view = "Register";	
}
if ($_GET['action'] == "buyad")
{
	$include = "buyad.php";
	$view = "Purchase Advert";	
}
if($_GET['action'] == "select")
{
	$include = "selectad.php";
	$view = "Select Advert";
}
if ($_GET['action'] == "createad")
{
	$include = "createad.php";
	$view = "Create New Advert";	
}
if ($_GET['action'] == "preview")
{
	$include = "preview.php";
	$view = "Preview Your Advert";	
}
if ($_GET['action'] == "confirm")
{
	$include = "confirm.php";
	$view = "Confirm Your Order";	
}
if($_GET['action'] == "complete")
{
	$include = "complete.php";
	$view = "Payment Completed";
}
if($_GET['action'] == "cancel")
{
	$include = "cancel.php";
	$view = "Payment Cancelled";
}
if($_GET['action'] == "declined")
{
	$include = "cancel.php";
	$view = "Purchase Declined";
}
if($_GET['action'] == "stats")
{
	$include = "stats.php";
	$view = "Detailed Stats";
}
if($_GET['action'] == "history")
{
	$include = "history.php";
	$view = "Purchase History";
}
if($_GET['action'] == "previewad")
{
	$include = "previewad.php";
	$view = "Preview Advert";
}
if($_GET['action'] == "contact")
{
   $include = "contact.php";
   $view = "Contact Us";
}
?>
<?php include_once ('includes/head.php') ?>
<?php echo $message; ?>
<?php include_once ("pages/".$include."") ?>
<?php include_once ('includes/foot.php') ?>