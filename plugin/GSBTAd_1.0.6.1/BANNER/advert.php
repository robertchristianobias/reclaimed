<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
error_reporting(0);
function getFileType($file) {
    //Deprecated, but still works if defined...
    if (function_exists("mime_content_type"))
        return mime_content_type($file);
	//New way to get file type, but not supported by all yet.
    else if (function_exists("finfo_open")) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $file);
        finfo_close($finfo);
        return $type;
    }
	//Otherwise...just use the file extension
    else {
        $types = array(
            'jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif', 'swf' => 'application/x-shockwave-flash'
        );
        $ext = substr($file, strrpos($file, '.') + 1);
        if (key_exists($ext, $types)) return $types[$ext];
        return "Unknown";
    }
}

function acceptableType($type) {
    $array = array("image/jpeg", "image/jpg", "image/png", "image/png", "image/gif", "application/x-shockwave-flash");
    if (in_array($type, $array))
        return true;
    return false;
}

$_GET['f'] = "aimg/" . $_GET['f'];
$type = getFileType($_GET['f']);
if (empty($_GET['f'])) { header("refresh:0;url=index.php"); exit; }
if (acceptableType($type)) {
    header("Content-type: $type");
	echo file_get_contents($_GET['f']);
    exit;
}
header("refresh:0;url=index.php");
exit;
?>