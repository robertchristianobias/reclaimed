<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
mysql_query("set names utf8");
if ($_GET['action'] == "logout")
{
	//setcookie("email","", time() - 3600);
	//setcookie("password","", time() - 3600);
	session_start();
	unset($_SESSION['email']);
	unset($_SESSION['password']);
	unset($_COOKIE['email']);
	unset($_COOKIE['password']);
	setcookie('email', '', time() - 3600);
	setcookie('password', '', time() - 3600);
	$_SESSION = array();
    session_destroy();
	$_GET['action'] = "";
	header("Location: index.php");
}
if(isset($_POST['login']))
{
	mysql_query("set names utf8");
	$email = stripslashes($_REQUEST['email']);
	$password = stripslashes($_REQUEST['password']);
	$email = mysql_real_escape_string($email);
	$password = mysql_real_escape_string($password);
	$password2 = hash('ripemd160', $password);
	$_REQUEST['check']=authenticate($email,$password);
	if($_REQUEST['check']>0)
	{ //pass
	// time()+3600 - 1 hour, time()+3600*24 - 24 hours
	//setcookie("email",$_REQUEST['email'], time()+3600);
	//setcookie("password",$_REQUEST['password'], time()+3600);
	session_regenerate_id();
	$_SESSION['email'] = $email;
	$_SESSION['password'] = $password;
	session_write_close();
		$_GET['action'] = "members";
	}
	else
	{ //fail
        // Destroy the session and restart it.
		unset($_SESSION['email']);
		unset($_SESSION['password']);
		unset($_COOKIE['email']);
		unset($_COOKIE['password']);
		setcookie('email', '', time() - 3600);
		setcookie('password', '', time() - 3600);
		$_SESSION = array();
		session_destroy();
        session_start();
		$_GET['action'] = "failed";
	}
}
if(isset($_POST['remind']))
{
	mysql_query("set names utf8");
	$query = mysql_query("SELECT * FROM users WHERE email = '".sql_quote($_POST['email'])."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$newp=generatePassword();
		$passhash = hash('ripemd160',$newp);
		$update = mysql_query("UPDATE users SET pass='$passhash' WHERE email = '$rows[email]'");
/*		$email_message = output_email ("password_remind");
		$email_message = str_replace("{user}",$rows[email],$email_message);
		//$email_message = str_replace("{pass}",$rows[pass],$email_message);
		$email_message = str_replace("{pass}",$newp,$email_message);*/
		$useremail=LANG_MAIN_EMAIL;
		$userpass=LANG_MAIN_PASSWORD;
		$remindemail1=LANG_FORM_HELLO;
		$remindemail2=LANG_FORM_REMIND2;
		$remindemail3=LANG_FORM_LOGIN;
		$remindemail4=LANG_FORM_REMIND4;
		$remindemail5=LANG_FORM_REMIND5;
		$remindemail6=LANG_FORM_REGARDS;
		$email_message = "$remindemail1,\r
\r
$remindemail2\r
\r
$useremail: $rows[email]\r
$userpass: $newp\r
\r
\r
$remindemail3:\r
$conf[site_url]index.php?action=login\r
\r
$remindemail4\r
\r
$remindemail5\r
\r
$remindemail6,\r
$conf[site_name]\r
$conf[site_url]\r";
		$email_message = wordwrap($email_message, 70, "\r\n");
		$LANGFORMREMINDESUBJ = LANG_FORM_REMINDESUBJ;
		$subject = "$conf[site_name] $LANGFORMREMINDESUBJ";
		$from = "From: $conf[site_name]<$conf[send_from_email]>";
		mb_send_mail($rows['email'],$subject,$email_message,$from);
		$LANGFORMFORGETEMAIL = LANG_FORM_FORGETEMAIL;
		$message = "<font color=green>$LANGFORMFORGETEMAIL</font>";
	}
	else
	{
		$LANGFORMNORECEMAIL = LANG_FORM_NORECEMAIL;
		$message = "<font color=red>$LANGFORMNORECEMAIL</font>";
	}
}
if(isset($_POST['signup']))
{
	mysql_query("set names utf8");
	$contact = mysql_real_escape_string($_POST['contact']);
	$email = mysql_real_escape_string($_POST['email']);
	$password = mysql_real_escape_string($_POST['password']);
	$check_email = check_if_email_exsits($_POST['email']);
	if (!trim($contact))
	{
		$LANGFORMNNONAME = LANG_FORM_NONAME;
		$message = "<font color=red>$LANGFORMNONAME</font>";
		$_GET['action'] = "signup";
	}
	else
	if (!trim($password))
	{
		$LANGFORMNNOPASS = LANG_FORM_NOPASS;
		$message = "<font color=red>$LANGFORMNNOPASS</font>";
		$_GET['action'] = "signup";
	}
	else
	if (!trim($_POST['email']))
	{
		$LANGFORMNNOEMAIL = LANG_FORM_NOEMAIL;
		$message = "<font color=red>$LANGFORMNNOEMAIL</font>";
		$_GET['action'] = "signup";
	}
	else
	if ($check_email > 0)
	{
		$LANGFORMEMAILUSED = LANG_FORM_EMAILUSED;
		$message = "<font color=red>$LANGFORMEMAILUSED</font>";
		$_GET['action'] = "signup";
	}
	else
	{	
		$email = mysql_real_escape_string($_POST['email']);
		$password = mysql_real_escape_string($_POST['password']);
		$passwordhashed = mysql_real_escape_string(hash('ripemd160', $_POST['password']));
		$adduser = mysql_query("INSERT INTO users values('','".sql_quote($passwordhashed)."','".sql_quote($contact)."','".sql_quote($_POST['email'])."',NOW())");
/*		$user_message = output_email("signup");
		$user_message = str_replace("{user}",$_POST['email'],$user_message);
		$user_message = str_replace("{pass}",$password,$user_message);*/
		$LANGFORMSIGNUPESUBJ = LANG_FORM_SIGNUPESUBJ;
		$subject = "$conf[site_name] $LANGFORMSIGNUPESUBJ";
		$from = "From: $conf[site_name]<$conf[send_from_email]>";
		$useremail=LANG_MAIN_EMAIL;
		$userpass=LANG_MAIN_PASSWORD;
		$signupemail1=LANG_FORM_HELLO;
		$signupemail2=LANG_FORM_SIGNUP2;
		$signupemail3=LANG_FORM_SIGNUP3;
		$signupemail4=LANG_FORM_LOGIN;
		$signupemail5=LANG_FORM_REGARDS;
		$user_message = "$signupemail1,\r
\r
$signupemail2\r
\r
$signupemail3:\r
$useremail: $email\r
$userpass: $password\r
\r
$signupemail4:\r
$conf[site_url]index.php?action=login\r
\r
$signupemail5,\r
$conf[site_name]\r
$conf[site_url]\r";
		$user_message = wordwrap($user_message, 70, "\r\n");
		mb_send_mail($_POST['email'],$subject,$user_message,$from);
		setcookie ("email",$_POST['email']);
		setcookie ("password",$_POST['password']);
		$_REQUEST['check'] = authenticate($_POST['email'],$passwordhashed);
		$LANGFORMACCCREATED = LANG_FORM_ACCCREATED;
		$message = "<a href=index.php?action=login>$LANGFORMACCCREATED</a>";
		$_GET['action'] = "signup";
	}
}
if(isset($_POST['preview_banner']))
{
	mysql_query("set names utf8");
	
if($conf['allowupload']=='yes'){
	/* UPLOAD START */
	$allowedExts = array("gif", "jpeg", "jpg", "png");
	$temp = explode(".", $_FILES["banner_url"]["name"]);
	$extension = end($temp);
	if ((($_FILES["banner_url"]["type"] == "image/gif")
	|| ($_FILES["banner_url"]["type"] == "image/jpeg")
	|| ($_FILES["banner_url"]["type"] == "image/jpg")
	|| ($_FILES["banner_url"]["type"] == "image/pjpeg")
	|| ($_FILES["banner_url"]["type"] == "image/x-png")
	|| ($_FILES["banner_url"]["type"] == "image/png"))
	&& ($_FILES["banner_url"]["size"] < $conf['uploadsize'])
	&& in_array($extension, $allowedExts))
	{
	if ($_FILES["banner_url"]["error"] > 0)
    {
     $_GET['action'] = "createad"; //echo "Return Code: " . $_FILES["banner_url"]["error"] . "<br />";
    }
	else
    {	 
	// Rename files with date stamp
      $randval = date("YmdHis");
      $newfilename = $randval . $_FILES["banner_url"]["name"];
	  $dir = $conf["absolute_path"];
	  ///echo $dir.'aimg/'.$newfilename;
	  chmod($dir.'aimg',0775);
      move_uploaded_file($_FILES["banner_url"]["tmp_name"],"aimg/" . $newfilename);
      //echo "Stored in: " . "aimg/" . $_FILES["banner_url"]["name"];
	  chmod($dir.'aimg',0555);
    }
	}
	else
	{ /*echo "Invalid file";*/ }
	/* UPLOAD END */
	if ($_FILES["banner_url"]["error"]>0) { $bannerurl=$_POST['banner_url']; }
	else { /* $bannerurl=$conf["site_url"].'aimg/'.$newfilename; */ $bannerurl=$conf["site_url"].'advert.php?f='.$newfilename; }
}else{$bannerurl=$_POST['banner_url'];}
	//echo '...<br />'.$bannerurl;
	
	$campaign_url=$_POST['campaign_url']; $id=$campaign_url;
	$campaign_info = get_campaign_info($id);
	$_POST['coupon_code']=stripslashes($_POST['coupon_code']);
	$_POST['campaign_price']=stripslashes($_POST['campaign_price']);
	$_POST['adtitle_url']=stripslashes($_POST['adtitle_url']);
	$_POST['addesc_url']=stripslashes($_POST['addesc_url']);
	$_POST['adtitle_url']=htmlentities($_POST['adtitle_url'], ENT_QUOTES);
	$_POST['addesc_url']=htmlentities($_POST['addesc_url'], ENT_QUOTES);
	
	$ext=array();
	$ext = explode(".",$bannerurl,strlen($bannerurl));
	$extn = end($ext);
	if (!trim($bannerurl))
	{
		$LANGFORMENTERGRPURL = LANG_FORM_ENTERGRPURL;
		$message = "<font color=red><strong>$LANGFORMENTERGRPURL</strong></font>";
		$_GET['action'] = "createad";
	}
	else
	if($extn !== "gif" AND $extn !== "GIF" AND $extn !== "jpeg" AND $extn !== "JPEG" AND $extn !== "jpg" AND $extn !== "JPG" AND $extn !== "png" AND $extn !== "PNG")
	{
		$LANGFORMGRAPICADFILE = LANG_FORM_GRAPICADFILE;
		$message = "<font color=red>$LANGFORMGRAPICADFILE</font>";
		$_GET['action'] = "createad";
	}
	else
	if (!trim($_POST['forward_url']))
	{
		$LANGFORMENTERFORURL = LANG_FORM_ENTERFORURL;
		$message = "<font color=red><strong>$LANGFORMENTERFORURL</strong></font>";
		$_GET['action'] = "createad";
	}
	else
	if($extn == "gif" || $extn == "GIF")
	{
		$image = @imagecreatefromgif($bannerurl);
		//if (imagesx($image) != $campaign_info["width"])
		  if (imagesx($image) > $campaign_info["width"])
		{
			$LANGFORMGRAPICMWIDTH = LANG_FORM_GRAPICMWIDTH;
			$message = "<font color=red><strong>$LANGFORMGRAPICMWIDTH ".$campaign_info["width"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		//if (imagesy($image) != $campaign_info["height"])
		  if (imagesy($image) > $campaign_info["height"])
		{
			$LANGFORMGRAPICMHEIGHT = LANG_FORM_GRAPICMHEIGHT;
			$message = "<font color=red><strong>$LANGFORMGRAPICMHEIGHT ".$campaign_info["height"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		else
		{
			$_GET['action'] = "preview";
		}
	}
	else
	if($extn == "jpeg" || $extn == "JPEG" || $extn == "jpg" || $extn == "JPG")
	{
		$image = @imagecreatefromjpeg($bannerurl);
		//if (imagesx($image) != $campaign_info["width"])
		  if (imagesx($image) > $campaign_info["width"])
		{
			$LANGFORMGRAPICMWIDTH = LANG_FORM_GRAPICMWIDTH;
			$message = "<font color=red><strong>$LANGFORMGRAPICMWIDTH ".$campaign_info["width"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		//if (imagesy($image) != $campaign_info["height"])
		  if (imagesy($image) > $campaign_info["height"])
		{
			$LANGFORMGRAPICMHEIGHT = LANG_FORM_GRAPICMHEIGHT;
			$message = "<font color=red><strong>$LANGFORMGRAPICMHEIGHT ".$campaign_info["height"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		else
		{
			$_GET['action'] = "preview";
		}
	}
	else
	if($extn == "png" || $extn == "PNG")
	{
		$image = @imagecreatefrompng($bannerurl);
		//if (imagesx($image) != $campaign_info["width"])
		  if (imagesx($image) > $campaign_info["width"])
		{
			$LANGFORMGRAPICMWIDTH = LANG_FORM_GRAPICMWIDTH;
			$message = "<font color=red><strong>$LANGFORMGRAPICMWIDTH ".$campaign_info["width"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		//if (imagesy($image) != $campaign_info["height"])
		  if (imagesy($image) > $campaign_info["height"])
		{
			$LANGFORMGRAPICMHEIGHT = LANG_FORM_GRAPICMHEIGHT;
			$message = "<font color=red><strong>$LANGFORMGRAPICMHEIGHT ".$campaign_info["height"]."</strong></font>";
			$_GET['action'] = "createad";
		}
		else
		{
			$_GET['action'] = "preview";
		}
	}
}
if(isset($_POST['go_to_preview2']))
{
	mysql_query("set names utf8");
	
if($conf['allowfupload']=='yes'){
	/* UPLOAD START */
	$allowedExts = array("swf");
	$temp = explode(".", $_FILES["banner_url"]["name"]);
	$extension = end($temp);
	if (($_FILES["banner_url"]["type"] == "application/x-shockwave-flash")
	&& ($_FILES["banner_url"]["size"] < $conf['uploadfsize'])
	&& in_array($extension, $allowedExts))
	{
	if ($_FILES["banner_url"]["error"] > 0)
    {
     $_GET['action'] = "createad"; //echo "Return Code: " . $_FILES["banner_url"]["error"] . "<br />";
    }
	else
    {	 
	// Rename files with date stamp
      $randval = date("YmdHis");
      $newfilename = $randval . $_FILES["banner_url"]["name"];
	  $dir = $conf["absolute_path"];
	  ///echo $dir.'aimg/'.$newfilename;
	  chmod($dir.'aimg',0775);
      move_uploaded_file($_FILES["banner_url"]["tmp_name"],"aimg/" . $newfilename);
      //echo "Stored in: " . "aimg/" . $_FILES["banner_url"]["name"];
	  chmod($dir.'aimg',0555);
    }
	}
	else
	{ /*echo "Invalid file";*/ }
	/* UPLOAD END */
	if ($_FILES["banner_url"]["error"]>0) { $bannerurl=$_POST['banner_url']; }
	else { /* $bannerurl=$conf["site_url"].'aimg/'.$newfilename; */ $bannerurl=$conf["site_url"].'advert.php?f='.$newfilename; }
}else{$bannerurl=$_POST['banner_url'];}
	//echo '<br />'.$bannerurl;
	
		$campaign=$_REQUEST['campaign'];
		$campaign_info = get_campaign_info($campaign);
		$_POST['coupon_code']= stripslashes($_POST['coupon_code']);
		$_POST['campaign_price']= stripslashes($_POST['campaign_price']);
		$_POST['adtitle_url']= stripslashes($_POST['adtitle_url']);
		$_POST['addesc_url']= stripslashes($_POST['addesc_url']);
		$_POST['adtitle_url']=htmlentities($_POST['adtitle_url'], ENT_QUOTES);
		$_POST['addesc_url']=htmlentities($_POST['addesc_url'], ENT_QUOTES);
	
	$ext=array();
	$ext = explode(".",$bannerurl,strlen($bannerurl));
	$extn = end($ext);
	if (!trim($bannerurl))
	{
		$LANGFORMENTERGRPURL = LANG_FORM_ENTERGRPURL;
		$message = "<font color=red><strong>$LANGFORMENTERGRPURL</strong></font>";
		$_GET['action'] = "createad";
	}
	else
	if($extn !== "swf")
	{
		$LANGFORMGRAPICADFILE = LANG_FORM_GRAPICADFILE;
		$message = "<font color=red>$LANGFORMGRAPICADFILE</font>";
		$_GET['action'] = "createad";
	}
	else
	if (!trim($_POST['forward_url']))
	{
		$LANGFORMENTERFORURL = LANG_FORM_ENTERFORURL;
		$message = "<font color=red><strong>$LANGFORMENTERFORURL</strong></font>";
		$_GET['action'] = "createad";
	}
	else
	if($extn == "swf")
	{

			$_GET['action'] = "preview";
	}
}
if(isset($_POST['go_to_payment']))
{
	mysql_query("set names utf8");
		$_POST['adtitle_url']=stripslashes($_POST['adtitle_url']);
		$_POST['addesc_url']=stripslashes($_POST['addesc_url']);
	if (!trim($_REQUEST['payment_method']))
	{
		$LANGFORMSELECTPAYMENT = LANG_FORM_SELECTPAYMENT;
		$message = "<font color=red><strong>$LANGFORMSELECTPAYMENT</strong></font>";
		$_GET['action'] = "preview";
	}
	else
	{
	$id = $_POST['campaign_url'];
	$campaign_info = get_campaign_info($id);
	$unlimited = $campaign_info["unlimited"];
		
		if($product == $product)
		{
		$_POST['adtitle_url']= stripslashes($_POST['adtitle_url']);
		$_POST['addesc_url']= stripslashes($_POST['addesc_url']);
		//$_POST['adtitle_url']= addslashes($_POST['adtitle_url']);
		//$_POST['addesc_url']= addslashes($_POST['addesc_url']);
			$add = mysql_query("INSERT INTO ads values('','".sql_quote($_REQUEST['transaction_url'])."','".sql_quote($_REQUEST['user_email'])."','inactive','".sql_quote($_POST['banner_url'])."','".sql_quote($_POST['forward_url'])."','".sql_quote($_POST['adurl_url'])."','".addslashes($_POST['adtitle_url'])."','".addslashes($_POST['addesc_url'])."','".sql_quote($_POST['credits_url'])."','".sql_quote($unlimited)."',NOW(),'".sql_quote($_REQUEST['adtype_url'])."','".$_POST['campaign_url']."','".sql_quote($_POST['adtyid_url'])."','".sql_quote($_POST['payment_method'])."','".sql_quote($_POST['coupon_code'])."')");
			$add = mysql_query("INSERT INTO history values('','".sql_quote($_REQUEST['transaction_url'])."','".sql_quote($_REQUEST['user_email'])."','0','".sql_quote($_POST['credits_url'])."',NOW(),'".$_POST['campaign_url']."','".sql_quote($_POST['payment_method'])."')");
			$banner_id = mysql_insert_id();
			$_GET['action'] = "confirm";
		}
	}
}
?>