<?php
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################

define('_VALID_INCLUDE', TRUE);

require_once("class.inputfilter_clean.php");

function clean ()
{
$tags = array('applet', 'body', 'bgsound', 'base', 'basefont', 'embed', 'frame', 'frameset', 'head', 'html', 'id', 'iframe', 'ilayer', 'layer', 'link', 'meta', 'name', 'object', 'script', 'style', 'title', 'xml','ScRiPt','%');
$attr = array('action', 'background', 'codebase', 'dynsrc', 'lowsrc');

$myFilter = new InputFilter($tags, $attr, 0, 0, 1);
$_GET = $myFilter->process($_GET);

$myFilter = new InputFilter($tags, $attr, 0, 0, 1);
$_POST = $myFilter->process($_POST);

$myFilter = new InputFilter($tags, $attr, 0, 0, 1);
$_SESSION = $myFilter->process($_SESSION);

$myFilter = new InputFilter($tags, $attr, 0, 0, 1);
$_COOKIE = $myFilter->process($_COOKIE); 
}

///// Call function
if (isset($_POST) || isset($_GET) || isset($_SESSION) || isset($_COOKIE))
{
clean();
}
///ends

function sql_quote( $value ) 
{ 
if( get_magic_quotes_gpc() ) 
{ 
      $value = stripslashes( $value ); 
} 
//check if this function exists 
if( function_exists( "mysql_real_escape_string" ) ) 
{ 
      $value = mysql_real_escape_string( $value ); 
} 
//for PHP version < 4.3.0 use htmlspecialchars 
else 
{ 
      $value = htmlspecialchars( $value ); 
} 
return $value; 
}

$GetConf = mysql_query("SELECT * FROM config WHERE id = 1");
$conf = mysql_fetch_array($GetConf);

$GetConf2 = mysql_query("SELECT * FROM payfig WHERE id = 1");
$conf2 = mysql_fetch_array($GetConf2);


function check_if_email_exsits($email)
{
	$query = mysql_query("SELECT * FROM users WHERE email = '".sql_quote($_POST['email'])."'");
	$numrows = mysql_num_rows($query);
	return $numrows;
}
//################START################ 
//ad.php/ad4.php - cost per impression
function per_view($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE `track` SET `exp` = `exp` + 1 WHERE `id` = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),1,0)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
	$subtract = mysql_query("SELECT * FROM ads WHERE id = '".sql_quote($banner_id)."' and unlimited = 'yes'");
	$numrows_subtract = mysql_num_rows($subtract);
	if ($numrows_subtract < 1)
	{
		$deduct = mysql_query("UPDATE ads SET credits = credits - '1' WHERE id = '".sql_quote($banner_id)."'");
	}
}

function add_click($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE track SET clicks = clicks + 1 WHERE id = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),0,1)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");
	}
}
//################END################ 
//################START################ 
//ad2.php/ad5.php - cost per click
function per_click($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE track SET exp = exp + '1' WHERE id = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),1,0)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
}

function add_click2($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE track SET clicks = clicks + '1' WHERE id = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");;
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),0,1)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");
	}
	$subtract = mysql_query("SELECT * FROM ads WHERE id = '".sql_quote($banner_id)."' and unlimited = 'yes'");
	$numrows_subtract = mysql_num_rows($subtract);
	if ($numrows_subtract < 1)
	{
		$deduct = mysql_query("UPDATE ads SET credits = credits - '1' WHERE id = '".sql_quote($banner_id)."'");
	}
}
//################END################ 
//################START################ 
//ad3.php/ad6.php - cost per day
function per_day($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE track SET exp = exp + '1' WHERE id = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),1,0)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','v')");
	}
}

function add_click3($banner_id)
{
	// Check if domain passed thru CloudFlare & Collect User IP Address
	if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { $getIP = $_SERVER['HTTP_CF_CONNECTING_IP']; } else { $getIP = $_SERVER['REMOTE_ADDR']; }
	$todayis = date("Y-m-d");
	$realtimedate = date("F j, Y, g:i a");
	$query = mysql_query("SELECT * FROM track WHERE banner_id = '".sql_quote($banner_id)."' AND date = '".$todayis."'");
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		$rows = mysql_fetch_array($query);
		$update = mysql_query("UPDATE track SET clicks = clicks + '1' WHERE id = '".$rows['id']."'");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");
	}
	else
	{
		$add = mysql_query("INSERT INTO track values ('','".sql_quote($banner_id)."',NOW(),0,1)");
		$add = mysql_query("INSERT INTO trackip values ('','".sql_quote($banner_id)."','".$realtimedate."','".$getIP."','c')");
	}
}
//################END################ 

function get_banner_info($id)
{
	$query = mysql_query("SELECT * FROM ads WHERE id = '".sql_quote($id)."'");
	$rows = mysql_fetch_array($query);
	return $rows;
}

function get_campaign_info($id)
{
	$query = mysql_query("SELECT * FROM campaigns WHERE id = '".sql_quote($id)."'");
	$rows = mysql_fetch_array($query);
	return $rows;
}

function get_user_info($email)
{
	$query = mysql_query("SELECT * FROM users WHERE email = '".sql_quote($_REQUEST['email'])."'");
	$rows = mysql_fetch_array($query);
	return $rows;
}

function generatePassword ($length = 10)
  {
    $password = "";
    $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
    $maxlength = strlen($possible);
    if ($length > $maxlength) {
      $length = $maxlength;
    }
    $i = 0; 
    while ($i < $length) { 
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
      if (!strstr($password, $char)) { 
        $password .= $char;
        $i++;
      }
    }
    return $password;
}

function authenticate($email,$password)
{
	$email = $_REQUEST['email'];
	$password = hash('ripemd160', $_REQUEST['password']);
	$sql = "SELECT * FROM users WHERE email='".sql_quote($email)."' AND pass='".sql_quote($password)."'";
	$query = mysql_query($sql) or die (mysql_error());
	$numrows = mysql_num_rows($query);
	if ($numrows > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function output_email($layout)
{
	$GetConfig = mysql_query("SELECT * FROM config WHERE id = 1");
	$conf = mysql_fetch_array($GetConfig);
	$fp = fopen("email_messages/".$layout.".txt", "r");
    $content = '';
    while(!feof($fp)) {
        $content .= fread($fp, 1024);
    }
    fclose($fp);
    $content = str_replace("%{site_name}", $conf["site_name"], $content);
	$content = str_replace("%{site_url}", $conf["site_url"], $content);
	return $content;
}
?>