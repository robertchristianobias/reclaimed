<style type="text/css">
<? if ($campaign_info['tlstyle']=='') { ?>
div#boxcpn<? echo $campaign_info['id']; ?> {
	margin: 0;
	width:<? echo $campaign_info['width']; ?>;
	height:auto;
	overflow:hidden;
	border:1px solid #ccc;
	padding:1px;
	line-height:14px;
}
li#spacecpn<? echo $campaign_info['id']; ?> {
	padding-right:0px;
	padding-bottom:0px;
	float: top; /* left=horizontal,top=vertical */
	width:<? echo $campaign_info['width']; ?>;
}
.ul1cpn<? echo $campaign_info['id']; ?> {
	margin: 0;
	padding: 1px 1px 1px 1px;
	text-align: left;
	float: left;
	list-style: none;
}
div#textcpn<? echo $campaign_info['id']; ?> {
	text-align:center;
	padding-top: 1px;
	padding-bottom: 2px;
}
div#adsbycpn<? echo $campaign_info['id']; ?> {
	float: right;
	width: auto;
	color: #808080;
	font-family: 'TrebuchetMS', 'Trebuchet MS', sans-serif;
	font-size: 12px;
	padding-top: 1px;
	padding-bottom: 2px;
	padding-left: 1px;
	padding-right: 5px;
}
.adsby2cpn<? echo $campaign_info['id']; ?> {
	color: #808080;
	text-decoration: underline;
}
.adsby2cpn<? echo $campaign_info['id']; ?> a {
	color: #808080;
	text-decoration: underline;
}
div#breakcpn<? echo $campaign_info['id']; ?> {
	clear: both;
}
<? } else { echo $campaign_info['tlstyle']; } ?>

<?php
if($campaign_info['addesc_type'] == 'teaser') 
	{ if ($campaign_info['allow_addesc']=='yes'){ ?>
.imgteaser<?php echo $campaign_info['id'] ?> { overflow: hidden; float: left; position: relative;
}
.imgteaser<?php echo $campaign_info['id'] ?> a { text-decoration: none; float: left;
}
.imgteaser<?php echo $campaign_info['id'] ?> a:hover { cursor: pointer;
}
.imgteaser<?php echo $campaign_info['id'] ?> a:hover .desc{ display: block; font:bold 11px verdana,arial,helvetica,sans-serif; background: #<?php if($campaign_info['teaserbgc']=='') echo '393C46'; else echo $campaign_info['teaserbgc']; ?>; background-image:url('<?php if($campaign_info['teaserbgi']=='') echo ''; else echo $campaign_info['teaserbgi']; ?>'); filter:alpha(opacity=<?php if($campaign_info['opacity']=='') echo '89'; else echo $campaign_info['opacity']; ?>); opacity:.<?php if($campaign_info['opacity']=='') echo '89'; else echo $campaign_info['opacity']; ?>; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=<?php if($campaign_info['opacity']=='') echo '89'; else echo $campaign_info['opacity']; ?>)"; /*--IE 8 Transparency--*/ color: #<?php if($campaign_info['teaserfc']=='') echo 'fff'; else echo $campaign_info['teaserfc']; ?>; position: absolute; bottom: 0px!important; <!--[if IE 5]> bottom: 0px; <![endif]--> left: 0px; padding: 0px; margin: 0; height: auto; width: <?php echo $campaign_info['width'] ?>; border-top: <?php if($campaign_info['teaserbt']=='') echo '1px solid #fff'; else echo $campaign_info['teaserbt']; ?>; border-top-left-radius: <?php if($campaign_info['teaserbtlr']=='') echo '30px 50px'; else echo $campaign_info['teaserbtlr']; ?>; border-top-right-radius: <?php if($campaign_info['teaserbtrr']=='') echo '30px 50px'; else echo $campaign_info['teaserbtrr']; ?>; 
}
.imgteaser<?php echo $campaign_info['id'] ?> a:hover .desc strong { display: block; margin-bottom: 1px;
}
.imgteaser<?php echo $campaign_info['id'] ?> a .desc {	display: none;
}
.adtext<?php echo $campaign_info['id'] ?> { <?php if($campaign_info['adtextcss']=='') echo 'color: black; font-family: helvetica; font-size: 11px; font-weight:bold;'; else echo $campaign_info['adtextcss']; ?>
}
<? } } elseif($campaign_info['addesc_type'] == 'text') { ?>
.adtext<?php echo $campaign_info['id'] ?> { <?php if($campaign_info['adtextcss']=='') echo 'color: black; font-family: helvetica; font-size: 11px; font-weight:bold;'; else echo $campaign_info['adtextcss']; ?>
}
<? } else { echo ''; } ?>
</style>
<?php if($campaign_info['adspeed']==' '){ ?><body onLoad="setTimeout('MyReload()', <?php echo $campaign_info['adspeed'] ?>)"><? } ?>