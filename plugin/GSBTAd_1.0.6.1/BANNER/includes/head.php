<? 
##############################################################
############ Pay Banner Textlink Ad Script v1.0.6 ############
############         Copyrights 2005-2014         ############
############        http://www.dijiteol.com       ############
##############################################################
//include_once ('includes/langswitcher.php');
?>
<!doctype html>
<html lang=<? echo LANG_HTML;?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo LANG_CHARSET_ISO; ?>">
<meta http-equiv="Content-Language" content="<?php echo LANG_CONT_HTML; ?>" />
<title><? echo $conf['site_name']; ?> - <? echo $view; ?></title>
<link rel="stylesheet" type="text/css" href="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/css/style.css" />
<meta content="<? echo $conf['keywords'] ?>" name="keywords">
<meta content="<? echo $conf['description'] ?>" name="description">
<? if ($conf['gtranslate'] == 'yes'){ ?><script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'auto',
    autoDisplay: false,
    floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><? } ?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
</head>
<body>

<div id="header">
<div id="logo"></div> 
<div id="nav"><ul id="navmenu">
<li><a href="<? echo $conf["site_url"]; ?>index.php"><span id="alink"><?php echo LANG_MENU_HOME; ?></span></a></li>
<li><a href="<? echo $conf["site_url"]; ?>index.php?action=signup"><span id="alink"><?php echo LANG_MENU_ADVERTISE; ?></span></a></li>
<? if(!isset($_SESSION['email']) || (trim($_SESSION['password'])=='')){ ?><li><a href="<? echo $conf["site_url"]; ?>index.php?action=login"><span id="alink"><?php echo LANG_MENU_LOGIN; ?></span></a></li>
<? }else{ ?><li><a href="<? echo $conf["site_url"]; ?>index.php?action=members"><span id="alink"><?php echo LANG_MENU_LOGGEDIN; ?></span></a></li><? } ?>
<li><a href="<? echo $conf["site_url"]; ?>index.php?action=contact"><span id="alink"><?php echo LANG_MENU_CONTACT; ?></span></a></li>
</ul></div>
<div id="languagemenu"><ul id="langnav">
<li><span title="Languages" id="lm"><img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/langs.gif" width="18" height="18" align="abmiddle" alt="Language" title="Language" border="0" /> Language</span><ul>
<li><a href="<? echo $conf["site_url"]; ?>?lang=en">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/en.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> English</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=es">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/es.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> Español</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=fr">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/fr.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> Française</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=de">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/de.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> Deutsch</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=it">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/it.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> Italiano</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=nl">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/nl.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> Nederlandse</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=ko">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/ko.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> 한국의</a></li>
<li><a href="<? echo $conf["site_url"]; ?>?lang=jp">&#8250; <img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/jp.png" width="18" height="18" align="top" alt="Language" title="Language" border="0" /> 日本語</a></li>
</ul></li>
</ul></div>
<div id="currencymenu"><ul id="currnav"><li>
<span title="Currency" id="cm"><img src="<? echo $conf["site_url"]; ?>themes/<?=$theme;?>/img/money.png" width="18" height="18" align="abmiddle" alt="Currency" title="Currency" border="0" /> Currency</span><ul>
<? if($conf2['usd'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=usd">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/usd.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> USD</a></li><? } ?>
<? if($conf2['eur'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=eur">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/eur.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> EUR</a></li><? } ?>
<? if($conf2['gbp'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=gbp">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/gbp.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> GBP</a></li><? } ?>
<? if($conf2['jpy'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=jpy">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/jpy.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> JPY</a></li><? } ?>
<? if($conf2['cad'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=cad">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/cad.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> CAD</a></li><? } ?>
<? if($conf2['aud'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=aud">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/aud.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> AUD</a></li><? } ?>
<? if($conf2['chf'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=chf">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/chf.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> CHF</a></li><? } ?>
<? if($conf2['inr'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=inr">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/inr.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> INR</a></li><? } ?>
<? if($conf2['sek'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=sek">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/sek.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> SEK</a></li><? } ?>
<? if($conf2['nok'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=nok">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/nok.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> NOK</a></li><? } ?>
<? if($conf2['mxn'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=mxn">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/mxn.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> MXN</a></li><? } ?>
<? if($conf2['hkd'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=hkd">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/hkd.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> HKD</a></li><? } ?>
<? if($conf2['nzd'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=nzd">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/nzd.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> NZD</a></li><? } ?>
<? if($conf2['pln'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=pln">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/pln.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> PLN</a></li><? } ?>
<? if($conf2['dkk'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=dkk">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/dkk.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> DKK</a></li><? } ?>
<? if($conf2['czk'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=czk">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/czk.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> CZK</a></li><? } ?>
<? if($conf2['sgd'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=sgd">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/sgd.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> SGD</a></li><? } ?>
<? if($conf2['twd'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=twd">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/twd.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> TWD</a></li><? } ?>
<? if($conf2['thb'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=thb">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/thb.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> THB</a></li><? } ?>
<? if($conf2['ils'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=ils">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/ils.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> ILS</a></li><? } ?>
<? if($conf2['rub'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=rub">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/rub.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> RUB</a></li><? } ?>
<? if($conf2['krw'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=krw">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/krw.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> KRW</a></li><? } ?>
<? if($conf2['cny'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=cny">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/cny.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> CNY</a></li><? } ?>
<? if($conf2['php'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=php">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/php.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> PHP</a></li><? } ?>
<? if($conf2['btc'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=btc">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/btc.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> BTC</a></li><? } ?>
<? if($conf2['ltc'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=ltc">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/ltc.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> LTC</a></li><? } ?>
<? if($conf2['ftc'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=ftc">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/ftc.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> FTC</a></li><? } ?>
<? if($conf2['ppc'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=ppc">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/ppc.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> PPC</a></li><? } ?>
<? if($conf2['nmc'] == 'yes') { ?><li><a href="<? echo $conf["site_url"]; ?>?curr=nmc">&#8250; <img src="<? echo $conf["site_url"]; ?>images/c/nmc.png" width="16" height="16" align="top" alt="Currency" title="Currency" border="0" /> NMC</a></li><? } ?>
</ul></li>
</ul></div>
</div>
<div id="spacing"></div>

<div id="wrapper">
<div id="main">
<div id="spacing"></div>