################################################################
############ Pay Banner Textlink Ad Script v1.0.6.1 ############
############         Copyrights 2005-2015           ############
############        http://www.dijiteol.com         ############
################################################################

################### UPGRADE TO v1.0.6 ###################
1. Please see \CHANGELOG and \UPGRADE folders.
***MAKE SURE YOU MAKE BACK-UP OF YOUR MYSQL DATABASE!!***
#########################################################

The php version required: 5.1.2 or higher
*The latest php / mysql versions tested: 5.2.9-5.3.4 / 5.0.91-5.0.92
*Tested on Godaddy and iPage


#################### FIRST TIME INSTALLATION v1.0.6.1 #####################

1. Create a Database name and assign user to it.

2. Upload all folders & files to host/server... you can rename the folder 'SCRIPT-UPLOAD' to 'banner', 'ad', 'advert' or whatever name you want.

3. CHMOD the includes/ directory permissions to 777 or 0777

4. Open the Web browser (IE, FF, Opera, etc) and point to http://www.yourdomain.com/*advertdir*/install/
	-Where yourdomain.com is you domain and *advertdir* is directory where the script was uploaded.
4a. Follow onscreen instructions.
4b. Ensure the following:
	-Site URL is URL to script and includes a trailing slash (/)
	-Absolute Path is directory path to script and includes a trailing slash (/)

5. CHMOD the includes/ directory permissions back to 755 or 0755 after installation finished 
*you'll get a warning in the Admin if not done*

6. Delete/remove the install directory - /install - you'll get a warning in the Admin Area if not removed/deleted.

7. You may now access admin by http://wwww.yourdomain.com/*advertdir*/admin/
                 ***YOU MUST CHANGE YOUR USER/PASSWORD AFTER YOU LOGIN!


8. You need to set-ups the following:
	-Site settings
	-Payment settings
	-Currency settings
	-Campaigns (must be created before displaying ads)

9. Start displaying banner ads on your site:
You can get ad codes in the Campaign Page.

########################################################################


############################## reCAPTCHA ##############################
I have decided not to include reCAPTCHA.
#######################################################################

########## JavaScript Graphical / Virtual Keyboard Interface ##########
Website: http://www.greywyvern.com/code/javascript/keyboard
#######################################################################




Enjoy!!!!

################################################################
############ Pay Banner Textlink Ad Script v1.0.6.1 ############
############         Copyrights 2005-2015           ############
############        http://www.dijiteol.com         ############
################################################################