<?php

/*
 * -------------------------------------------------------------------
 *  My Config Private
 * -------------------------------------------------------------------
 */
date_default_timezone_set('Asia/Ho_Chi_Minh');

define('FOLDER', '/');
define('ADMINCP', 'cms');
$flag_https = false;
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on'){
	define('PATH_URL', 'https://'.$_SERVER['HTTP_HOST'].FOLDER);
	define('PATH_URL_ADMIN', 'https://'.$_SERVER['HTTP_HOST'].FOLDER.ADMINCP.'/');
	$flag_https = true;
}else{
	define('PATH_URL', 'http://'.$_SERVER['HTTP_HOST'].FOLDER);
	define('PATH_URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].FOLDER.ADMINCP.'/');
}

define('PREFIX', '');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'reclaime_staging');

// define('DB_USER', 'dodeeg_redm');
// define('DB_PASS', '1GbZ3hV{Dc30');
// define('DB_NAME', 'dodeeg_redm');

define('IS_LOCALHOST', false);
if(!IS_LOCALHOST && !$flag_https){
?>
<html>
<head>
<script>
var current_url = location.href;
current_url = current_url.replace('http','https');
location.href = current_url;
</script>
</head>
</html>
<?php
exit();
}

define('DEBUG_LOG', true);
define('DEBUG_LOG_LAST_COMMAND', false);

define('WEBSITE_NAME', 'REDM');

define('LANG_CODE_DEFAULT', 'vn');
define('LANG_CODE_VIETNAM', 'vn');
define('LANG_CODE_ENGLISH', 'en');

define('PRODUCT_SIZE_SMALL', 1);
define('PRODUCT_SIZE_MEDIUM', 2);
define('PRODUCT_SIZE_LARGE', 3);

define('AJAX_PREFIX', 'ajax');
define('AJAX_SUBMIT_CONTACT_FORM', 'submit_contact');
define('AJAX_SUBMIT_BOOKING_FORM', 'submit_booking');
define('AJAX_SUBMIT_ORDER_FORM', 'submit_order');

define('HAS_PAGE', 1);
define('HAS_CATEGORY', 2);
define('HAS_DETAIL', 3);

define('USER_ROLE_READER', 1);
define('USER_ROLE_PRO', 2);
define('ACCOUNT_CREATED_BY_NORMAL', 1);
define('ACCOUNT_CREATED_BY_FACEBOOK', 2);
define('ACCOUNT_CREATED_BY_GMAIL', 3);

$size = array(
	's' => 'Small',
	'm' => 'medium',
	'l' => 'Large'
	);

define('SIZE_SERIALIZE', serialize($size));
define('AMAZON_PREFIX', 'aws_ses');
define('PAYMENT_PREFIX', 'payment');
define('REDMADS_FOLDER', 'redmads');
define('FEED_PREFIX', 'feed');


$slug_special = array(ADMINCP, AJAX_PREFIX, AMAZON_PREFIX, PAYMENT_PREFIX, REDMADS_FOLDER, FEED_PREFIX);
define('SLUGS_SPECIAL_SERIALIZE', serialize($slug_special));

//CONFIG SMTP SEND MAIL
define('SMTP_HOST', "email-smtp.us-east-1.amazonaws.com");
define('SMTP_PORT', "587");
define('SMTP_USERNAME', "AKIAIASNJYKG5KC4TRLA");
define('SMTP_PASSWORD', "AlxeKg+bxjF/bEyBMOnzHoDvRVe3mlRljwgR8ocTB+yn");
define('SMTP_ENCRYPTED', "TLS");
define('SMTP_SENDER_EMAIL', "guongvo.pix@gmail.com");
define('SMTP_SENDER_NAME', "REDM");



//CONFIG AMAZON SES
//PIX
define('AWS_ACCESS_KEY','AKIAJX5FIUES662ZCWAQ');
define('AWS_SECRET_KEY','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');
define('AWS_REGION','us-east-1'); 
define('AWS_SDK_VERSION', 'latest');
define('NO_REPLY_SENDER', 'nhatnguyen.pix@gmail.com');
define('FLATFORM_NAME', 'REDM');

// CONFIG GOOGLE
define('GG_CLIENT_ID','792368792522-jp3i36hmjvh7vj929juq1204uppn7ed0.apps.googleusercontent.com');
define('GG_CLIENT_SECRET','J43YUBRmeC7Bxb3aI0bOXwwX');
define('GCSE_ID', '009293338218675513167:huxtng0ebrw');
define('GG_API_KEY', 'AIzaSyDOIgH2GTu1-139naJ5mmobocWs6QjNILU');

// CONFIG FACEBOOK
define('FACEBOOK_APP_ID', '175490856387155');
define('FACEBOOK_APP_SECRET', '7227af520eb63f734acea0d41c404d8b');

define('ONESIGNAL_REST_API_KEY', 'NWQ5ZGM5MDEtZDIyYS00MzMwLTkzMWItOGI5MTA5MGRlMGZl');
define('ONESIGNAL_APP_ID', 'fe355421-d33c-4c52-92e9-34f2d60c9ede');
define('ONESIGNAL_SUBDOMAIN_NAME', 'redmdev');
define('ONESIGNAL_SAFARI_WEB_ID', 'web.onesignal.auto.0f4ef0fc-d020-4184-9066-253532c318fc');

#WIRECARD CONFIG
define('WIRECARD_CUSTOMER_ID', 'D200001');
define('WIRECARD_SHOP_ID', '');
define('WIRECARD_SECRET', 'B8AKTPWBRMNBV455FG6M2DANE99WU2');

#Google Analytics CONFIG
define('GA_VIEW_ID', '150026451');
define('GA_TABLE_ID', 'ga:150026451');
if(defined('BASEFOLDER')){
    define('GA_CERT_KEY_LOCATION', BASEFOLDER . '/include_files/redm_ga_cert.json');
}



